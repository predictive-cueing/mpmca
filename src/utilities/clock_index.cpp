/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/clock_index.hpp"

namespace mpmca::utilities {
ClockIndex::ClockIndex(int index)
    : m_clock_idx(index)
{
}

int ClockIndex::GetIndex() const
{
    return m_clock_idx;
}
}  //namespace mpmca