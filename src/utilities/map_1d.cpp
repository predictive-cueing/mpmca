/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/map_1d.hpp"

#include "mpmca/json_tools.hpp"

namespace mpmca::utilities {

Map1D::Map1D()
    : Map1D({0, 1}, {0, 1})
{
}

Map1D::Map1D(std::initializer_list<double> x_vector, std::initializer_list<double> value_vector)
    : Map1D(LinearAlgebra::InitDynamicRowVector(x_vector), LinearAlgebra::InitDynamicRowVector(value_vector))
{
}

Map1D::Map1D(DynamicRowVector x_vector, DynamicRowVector value_vector)
    : IParameter(ParameterType::kMap1D)
    , m_x_vector(x_vector)
    , m_value_vector(value_vector)
    , m_requires_process_call(true)
{
    ProcessNewValues();
}

Map1D::Map1D(ConfigurationPtr config)
    : Map1D(config->GetValue<DynamicRowVector>("x", {0, 1}), config->GetValue<DynamicMatrix>("Value", {0, 1}))
{
}

void Map1D::ProcessNewValues()
{
    if (m_x_vector.size() != m_value_vector.size())
        throw std::invalid_argument("Lengths of y and value should be equal.");

    m_d_value_dx = DynamicRowVector(m_x_vector.size() - 1);
    m_x_mid = DynamicRowVector(m_x_vector.size() - 1);

    for (int j = 0; j < m_x_vector.size() - 1; ++j) {
        m_d_value_dx[j] = (m_value_vector[j + 1] - m_value_vector[j]) / (m_x_vector[j + 1] - m_x_vector[j]);
        m_x_mid[j] = (m_x_vector[j + 1] + m_x_vector[j]) / 2.0;
    }

    m_requires_process_call = false;
}
void Map1D::SetXVector(const DynamicRowVector& x_vector)
{
    m_x_vector = x_vector;
    m_requires_process_call = true;
}
void Map1D::SetValueVector(const DynamicRowVector& value_vector)
{
    m_value_vector = value_vector;
    m_requires_process_call = true;
}
void Map1D::SetXVector(const std::vector<double>& x_vector)
{
    m_x_vector = LinearAlgebra::InitDynamicRowVector(x_vector);
    m_requires_process_call = true;
}
void Map1D::SetValueVector(const std::vector<double>& value_vector)
{
    m_value_vector = LinearAlgebra::InitDynamicRowVector(value_vector);
    m_requires_process_call = true;
}
double Map1D::GetValue(double at_x, InterpolationMethod method) const
{
    if (m_requires_process_call)
        throw std::runtime_error("GetValue called on Map1D object without calling ProcessNewValues first.");

    if (method == kNearestNeighbour)
        return Nearest(at_x);
    else
        return Interp1(at_x);
}

double Map1D::Interp1(double at_x) const
{
    int j = 0;
    while (j < m_x_vector.size() - 2 && at_x > m_x_vector[j + 1])
        ++j;

    return m_value_vector[j] + (at_x - m_x_vector[j]) * m_d_value_dx[j];
}

double Map1D::Nearest(double at_x) const
{
    int j = 0;
    while (j < m_x_mid.size() && at_x > m_x_mid[j])
        ++j;

    return m_value_vector[j];
}
}  //namespace mpmca::utilities