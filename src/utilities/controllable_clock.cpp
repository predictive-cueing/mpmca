/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/controllable_clock.hpp"

namespace mpmca::utilities {
ControllableClock::ControllableClock(const Clock& clock)
    : Clock(clock)
{
}
ControllableClock::ControllableClock(const std::chrono::milliseconds& dt, const std::chrono::milliseconds& current_time)
    : Clock(dt)
{
    SetCurrentTime(current_time);
}
void ControllableClock::SetCurrentTime(const std::chrono::milliseconds& current_time)
{
    SetCurrentTimeMs(current_time.count());
}
void ControllableClock::SetCurrentTimeMs(int64_t current_time_ms)
{
    if (current_time_ms % m_dt_ms != 0) {
        throw std::runtime_error(
            "The current time of a ControllableClock must be divisible by the time step. The time " +
            std::to_string(current_time_ms) + " is not divisible by " + std::to_string(m_dt_ms) + ".");
    }
    m_current_time_ms = current_time_ms;
}
void ControllableClock::Tick()
{
    TickClock();
}
void ControllableClock::Reset()
{
    SetCurrentTimeMs(0);
}
}  //namespace mpmca