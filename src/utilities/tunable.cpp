/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/tunable.hpp"

#include "mpmca/utilities/map_1d.hpp"
#include "mpmca/utilities/map_2d.hpp"
#include "mpmca/utilities/scalar.hpp"

namespace mpmca::utilities {
template <>
ParameterType Tunable<Scalar>::GetParameterType()
{
    return ParameterType::kScalar;
}
template <>
ParameterType Tunable<Map1D>::GetParameterType()
{
    return ParameterType::kMap1D;
}
template <>
ParameterType Tunable<Map2D>::GetParameterType()
{
    return ParameterType::kMap2D;
}

}  //namespace mpmca