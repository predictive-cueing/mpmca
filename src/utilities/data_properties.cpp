/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/data_properties.hpp"

#include <stdexcept>

namespace mpmca::utilities {
DataProperties::DataProperties(const std::string& frame_of_reference, const std::string& name, const std::string& units)
    : m_name(name)
    , m_frame_of_reference(frame_of_reference)
    , m_units(units)
{
    if (m_name.empty()) {
        throw std::runtime_error("Name may not be empty for DataProperties. ");
    }

    if (m_units.empty()) {
        throw std::runtime_error("Units may not be empty for DataProperties with name " + name);
    }

    if (m_frame_of_reference.empty()) {
        m_total_name = m_name + "_" + m_units;
    }
    else {
        m_total_name = m_frame_of_reference + "_" + m_name + "_" + m_units;
    }
}
}  //namespace mpmca::utilities