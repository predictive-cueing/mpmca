/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/mpmca_json_file_reader.hpp"

#include <algorithm>
#include <filesystem>
#include <fstream>

#include "mpmca/data_names.hpp"
#include "mpmca/utilities/file_tools.hpp"

namespace mpmca::utilities {

FileContainer MpmcaJsonFileReader::ContainerFromMpmcaJsonFile(const std::string& name, const std::string& filename,
                                                              const std::string& indexing, double dt)
{
    if (!std::filesystem::exists(filename))
        throw(std::invalid_argument("File does not exist : " + filename));

    nlohmann::json sample_file;

    try {
        std::ifstream(filename) >> sample_file;
    }
    catch (const std::exception& c) {
        throw(std::invalid_argument("Exception while reading json file " + filename + ".\nMessage: " + c.what()));
    }

    auto field_names_and_index = GetAvailableFieldIndices(sample_file, DN::all_signal_names());

    std::vector<std::string> field_names;
    std::for_each(field_names_and_index.cbegin(), field_names_and_index.cend(),
                  [&field_names](auto f_n_a_i) { field_names.push_back(f_n_a_i.first); });

    auto time = GetTime(sample_file);
    auto x = GetField(sample_file, DN::X_vehicle().GetName());
    auto y = GetField(sample_file, DN::Y_vehicle().GetName());
    auto z = GetField(sample_file, DN::Z_vehicle().GetName());
    auto data = GetData(sample_file, field_names_and_index);

    return FileContainer(name, filename, indexing, field_names, time, x, y, z, data, dt);
}

std::vector<int64_t> MpmcaJsonFileReader::GetTime(nlohmann::json& sample_file)
{
    const int data_idx = sample_file.at("column_ids").value("time", -1);
    const size_t n_data = data_idx >= 0 ? sample_file.at("data").size() : 0;
    std::vector<int64_t> res(n_data);

    for (int i = 0; i < n_data; ++i)
        res[i] = (int64_t)std::round((double)sample_file.at("data").at(i).at(data_idx));

    return res;
}

std::vector<double> MpmcaJsonFileReader::GetField(nlohmann::json& sample_file, const std::string& field_name)
{
    const int data_idx = sample_file.at("column_ids").value(field_name, -1);
    const size_t n_data = data_idx >= 0 ? sample_file.at("data").size() : 0;
    std::vector<double> res(n_data);

    for (int i = 0; i < n_data; ++i)
        res[i] = sample_file.at("data").at(i).at(data_idx);

    return res;
}

std::vector<std::pair<std::string, int>> MpmcaJsonFileReader::GetAvailableFieldIndices(
    nlohmann::json& data, const std::vector<std::string>& requested_field_names)
{
    std::vector<std::pair<std::string, int>> found_field_names;
    for (auto& pfn : requested_field_names) {
        int index = data.at("column_ids").value(pfn, -1);
        if (index >= 0)
            found_field_names.push_back(std::make_pair(pfn, index));
    }

    return found_field_names;
}

DynamicMatrix MpmcaJsonFileReader::GetData(nlohmann::json& sample_file,
                                           const std::vector<std::pair<std::string, int>>& names_and_index)
{
    const size_t n_data = sample_file.at("data").size();
    const size_t n_fields = names_and_index.size();

    DynamicMatrix data(n_data, n_fields);

    for (size_t i = 0; i < n_data; ++i) {
        for (size_t j = 0; j < n_fields; ++j) {
            if (sample_file.at("data").at(i).at(names_and_index[j].second).is_number())
                data(i, j) = sample_file.at("data").at(i).at(names_and_index[j].second);
            else
                data(i, j) = std::nan("");
        }
    }

    return data;
}
}  //namespace mpmca::utilities