/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/compare.hpp"

#include "mpmca/pipeline/task.hpp"
namespace mpmca::utilities {

bool Compare::CompareTasks(const std::shared_ptr<mpmca::pipeline::Task>& a,
                           const std::shared_ptr<mpmca::pipeline::Task>& b)
{
    if (a->GetClock().GetDtMs() == b->GetClock().GetDtMs()) {
        return a->GetName().compare(b->GetName());
    }
    else {
        return a->GetClock().GetDtMs() < b->GetClock().GetDtMs();
    }
}
}  //namespace mpmca::utilities