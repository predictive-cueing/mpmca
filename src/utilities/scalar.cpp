/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/scalar.hpp"

namespace mpmca::utilities {
Scalar::Scalar(double value)
    : IParameter(ParameterType::kScalar)
    , m_value(value)
{
}
double Scalar::GetValue() const
{
    return m_value;
}
void Scalar::SetValue(double i)
{
    m_value = i;
}
}  //namespace mpmca