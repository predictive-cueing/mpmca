/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/time_measurement.hpp"

#include <algorithm>
#include <iostream>

namespace mpmca::utilities {

TimeMeasurement::TimeMeasurement()
    : m_point_construction(clock::now())
    , m_point_cycle_start(m_point_construction)
    , m_point_previous_clock(m_point_construction)
{
}

ClockIndex TimeMeasurement::RegisterClock(const std::string& name)
{
    m_clock_names.push_back(name);
    m_time_points.push_back(clock::now());
    m_durations_since_previous.push_back(clock::duration(0));
    m_durations_since_cycle_start.push_back(clock::duration(0));

    m_average_durations_ns.push_back(0);
    m_minimum_durations_ns.push_back(UINT32_MAX);
    m_maximum_durations_ns.push_back(0);

    m_n_measurements.push_back(0);

    return ClockIndex(m_clock_names.size() - 1);
}

void TimeMeasurement::StartCycle()
{
    if (!m_prev_cycle_stopped)
        CalculateStatistics();

    m_prev_cycle_stopped = false;

    auto t_now = clock::now();
    m_duration_since_previous_cycle_start = t_now - m_point_cycle_start;
    m_point_cycle_start = t_now;
    m_point_previous_clock = m_point_cycle_start;

    for (size_t i = 0; i < m_time_points.size(); i++) {
        m_time_points[i] = m_point_cycle_start;
        m_durations_since_cycle_start[i] = clock::duration(0);
        m_durations_since_previous[i] = clock::duration(0);
    }
}

void TimeMeasurement::StopCycle()
{
    CalculateStatistics();
    m_prev_cycle_stopped = true;
}

void TimeMeasurement::CalculateStatistics()
{
    for (size_t i = 0; i < m_time_points.size(); i++) {
        uint32_t durations_ns =
            std::chrono::duration_cast<std::chrono::nanoseconds>(m_durations_since_previous[i]).count();

        if (m_n_measurements[i] > 0.5)
            m_minimum_durations_ns[i] = std::min(m_minimum_durations_ns[i], durations_ns);

        m_n_measurements[i] += 1.0;

        m_average_durations_ns[i] =
            m_average_durations_ns[i] * (1.0 - 1.0 / (double)m_n_measurements[i]) + durations_ns / m_n_measurements[i];

        m_maximum_durations_ns[i] = std::max(m_maximum_durations_ns[i], durations_ns);
    }
}

void TimeMeasurement::PrintStatisticsMs() const
{
    std::cout << GetStatisticsMs() << std::endl;
}

void TimeMeasurement::PrintStatisticsUs() const
{
    for (size_t i = 0; i < m_time_points.size(); i++) {
        std::cout << m_clock_names[i] << " [us] "
                  << " min: " << m_minimum_durations_ns[i] / 1000 << " max: " << m_maximum_durations_ns[i] / 1000
                  << " avg: " << m_average_durations_ns[i] / 1000 << std::endl;
    }
}

void TimeMeasurement::PrintStatisticsNs() const
{
    for (size_t i = 0; i < m_time_points.size(); i++) {
        std::cout << m_clock_names[i] << " [ns] "
                  << " min: " << m_minimum_durations_ns[i] << " max: " << m_maximum_durations_ns[i]
                  << " avg: " << m_average_durations_ns[i] << std::endl;
    }
}

std::string TimeMeasurement::GetStatisticsMs(const std::string& line_prefix, bool mark_nondeterministic_numbers) const
{
    std::string res;
    std::string esc_start = (mark_nondeterministic_numbers ? "{" : "");
    std::string esc_stop = (mark_nondeterministic_numbers ? "}" : "");

    for (size_t i = 0; i < m_time_points.size(); i++) {
        res += line_prefix + m_clock_names[i] + " [ms] " +  //
               " min: " + esc_start + std::to_string(m_minimum_durations_ns[i] / 1000 / 1000) + esc_stop +  //
               " max: " + esc_start + std::to_string(m_maximum_durations_ns[i] / 1000 / 1000) + esc_stop +  //
               " avg: " + esc_start + std::to_string(m_average_durations_ns[i] / 1000 / 1000) + esc_stop;

        if (i < m_time_points.size() - 1)
            res += "\n";
    }
    return res;
}
std::string TimeMeasurement::GetStatisticsUs(const std::string& line_prefix, bool mark_nondeterministic_numbers) const
{
    std::string res;
    std::string esc_start = (mark_nondeterministic_numbers ? "{" : "");
    std::string esc_stop = (mark_nondeterministic_numbers ? "}" : "");

    for (size_t i = 0; i < m_time_points.size(); i++) {
        res += line_prefix + m_clock_names[i] + " [us] " +  //
               " min: " + esc_start + std::to_string(m_minimum_durations_ns[i] / 1000) + esc_stop +  //
               " max: " + esc_start + std::to_string(m_maximum_durations_ns[i] / 1000) + esc_stop +  //
               " avg: " + esc_start + std::to_string(m_average_durations_ns[i] / 1000) + esc_stop;

        if (i < m_time_points.size() - 1)
            res += "\n";
    }
    return res;
}
std::string TimeMeasurement::GetStatisticsNs(const std::string& line_prefix, bool mark_nondeterministic_numbers) const
{
    std::string res;
    std::string esc_start = (mark_nondeterministic_numbers ? "{" : "");
    std::string esc_stop = (mark_nondeterministic_numbers ? "}" : "");

    for (size_t i = 0; i < m_time_points.size(); i++) {
        res += line_prefix + m_clock_names[i] + " [ns] " +  //
               " min: " + esc_start + std::to_string(m_minimum_durations_ns[i]) + esc_stop +  //
               " max: " + esc_start + std::to_string(m_maximum_durations_ns[i]) + esc_stop +  //
               " avg: " + esc_start + std::to_string(m_average_durations_ns[i]) + esc_stop;

        if (i < m_time_points.size() - 1)
            res += "\n";
    }
    return res;
}

void TimeMeasurement::ResetStatistics()
{
    for (size_t i = 0; i < m_time_points.size(); ++i) {
        m_average_durations_ns[i] = 0;
        m_minimum_durations_ns[i] = UINT32_MAX;
        m_maximum_durations_ns[i] = 0;
        m_n_measurements[i] = 0;
    }
}

void TimeMeasurement::Protect(const ClockIndex& index) const
{
    if (index.GetIndex() >= m_time_points.size())
        throw std::runtime_error("Requested to stop a clock not in my memory.");
}

void TimeMeasurement::StopClock(const ClockIndex& index)
{
    Protect(index);

    auto now = clock::now();

    m_time_points[index.GetIndex()] = now;

    m_durations_since_cycle_start[index.GetIndex()] = now - m_point_cycle_start;
    m_durations_since_previous[index.GetIndex()] = now - m_point_previous_clock;

    m_point_previous_clock = now;
}

TimeMeasurement::clock::time_point TimeMeasurement::GetClockTime(const ClockIndex& index) const
{
    Protect(index);
    return m_time_points[index.GetIndex()];
}

TimeMeasurement::clock::duration TimeMeasurement::GetClockDurationSinceCycleStart(const ClockIndex& index) const
{
    Protect(index);
    return m_durations_since_cycle_start[index.GetIndex()];
}

TimeMeasurement::clock::duration TimeMeasurement::GetClockDurationSincePrevious(const ClockIndex& index) const
{
    Protect(index);
    return m_durations_since_previous[index.GetIndex()];
}

uint32_t TimeMeasurement::GetClockDurationSinceCycleStartUs(const ClockIndex& index) const
{
    Protect(index);
    return std::chrono::duration_cast<std::chrono::microseconds>(m_durations_since_cycle_start[index.GetIndex()])
        .count();
}

uint32_t TimeMeasurement::GetClockDurationSinceCycleStartNs(const ClockIndex& index) const
{
    Protect(index);
    return std::chrono::duration_cast<std::chrono::nanoseconds>(m_durations_since_cycle_start[index.GetIndex()])
        .count();
}

uint32_t TimeMeasurement::GetClockDurationSinceCycleStartMs(const ClockIndex& index) const
{
    Protect(index);
    return std::chrono::duration_cast<std::chrono::milliseconds>(m_durations_since_cycle_start[index.GetIndex()])
        .count();
}

uint32_t TimeMeasurement::GetClockDurationSincePreviousUs(const ClockIndex& index) const
{
    Protect(index);
    return std::chrono::duration_cast<std::chrono::microseconds>(m_durations_since_previous[index.GetIndex()]).count();
}

uint32_t TimeMeasurement::GetClockDurationSincePreviousNs(const ClockIndex& index) const
{
    Protect(index);
    return std::chrono::duration_cast<std::chrono::nanoseconds>(m_durations_since_previous[index.GetIndex()]).count();
}

uint32_t TimeMeasurement::GetClockDurationSincePreviousMs(const ClockIndex& index) const
{
    Protect(index);
    return std::chrono::duration_cast<std::chrono::milliseconds>(m_durations_since_previous[index.GetIndex()]).count();
}

std::string TimeMeasurement::GetClockName(const ClockIndex& index) const
{
    Protect(index);
    return m_clock_names[index.GetIndex()];
}

std::string TimeMeasurement::GetClockName(size_t index) const
{
    return m_clock_names.at(index);
}

size_t TimeMeasurement::GetNumClocks() const
{
    return m_clock_names.size();
}

const std::vector<std::string>& TimeMeasurement::GetClockNames() const
{
    return m_clock_names;
}

uint32_t TimeMeasurement::GetDurationBetweenPreviousTwoCycleStartsMs() const
{
    return std::chrono::duration_cast<std::chrono::milliseconds>(m_duration_since_previous_cycle_start).count();
}
uint32_t TimeMeasurement::GetDurationBetweenPreviousTwoCycleStartsUs() const
{
    return std::chrono::duration_cast<std::chrono::microseconds>(m_duration_since_previous_cycle_start).count();
}
uint32_t TimeMeasurement::GetDurationBetweenPreviousTwoCycleStartsNs() const
{
    return std::chrono::duration_cast<std::chrono::nanoseconds>(m_duration_since_previous_cycle_start).count();
}

std::vector<uint32_t> TimeMeasurement::GetClockDurationsSinceCycleStartMs() const
{
    std::vector<uint32_t> res;
    for (size_t i = 0; i < m_durations_since_cycle_start.size(); i++)
        res.push_back(std::chrono::duration_cast<std::chrono::milliseconds>(m_durations_since_cycle_start[i]).count());
    return res;
}

std::vector<uint32_t> TimeMeasurement::GetClockDurationsSinceCycleStartUs() const
{
    std::vector<uint32_t> res;
    for (size_t i = 0; i < m_durations_since_cycle_start.size(); i++)
        res.push_back(std::chrono::duration_cast<std::chrono::microseconds>(m_durations_since_cycle_start[i]).count());
    return res;
}

std::vector<uint32_t> TimeMeasurement::GetClockDurationsSinceCycleStartNs() const
{
    std::vector<uint32_t> res;
    for (size_t i = 0; i < m_durations_since_cycle_start.size(); i++)
        res.push_back(std::chrono::duration_cast<std::chrono::nanoseconds>(m_durations_since_cycle_start[i]).count());
    return res;
}

std::vector<uint32_t> TimeMeasurement::GetClockDurationsSincePreviousNs() const
{
    std::vector<uint32_t> res;
    res.reserve(m_durations_since_previous.size());
    for (size_t i = 0; i < m_durations_since_previous.size(); i++)
        res.push_back(std::chrono::duration_cast<std::chrono::nanoseconds>(m_durations_since_previous[i]).count());

    return res;
}

std::vector<uint32_t> TimeMeasurement::GetClockDurationsSincePreviousUs() const
{
    std::vector<uint32_t> res;
    for (size_t i = 0; i < m_durations_since_previous.size(); i++)
        res.push_back(std::chrono::duration_cast<std::chrono::microseconds>(m_durations_since_previous[i]).count());

    return res;
}

std::vector<uint32_t> TimeMeasurement::GetClockDurationsSincePreviousMs() const
{
    std::vector<uint32_t> res;
    for (size_t i = 0; i < m_durations_since_previous.size(); i++)
        res.push_back(std::chrono::duration_cast<std::chrono::milliseconds>(m_durations_since_previous[i]).count());

    return res;
}

std::vector<double> TimeMeasurement::GetAverageDurationMs() const
{
    std::vector<double> res;
    for (size_t i = 0; i < m_average_durations_ns.size(); i++)
        res.push_back(m_average_durations_ns[i] / 1000 / 1000);

    return res;
}

std::vector<double> TimeMeasurement::GetAverageDurationUs() const
{
    std::vector<double> res;
    for (size_t i = 0; i < m_average_durations_ns.size(); i++)
        res.push_back(m_average_durations_ns[i] / 1000);

    return res;
}

std::vector<double> TimeMeasurement::GetAverageDurationNs() const
{
    std::vector<double> res;
    for (size_t i = 0; i < m_average_durations_ns.size(); i++)
        res.push_back(m_average_durations_ns[i]);

    return res;
}

std::vector<uint32_t> TimeMeasurement::GetMinDurationMs() const
{
    std::vector<uint32_t> res;
    for (size_t i = 0; i < m_minimum_durations_ns.size(); i++)
        res.push_back(m_minimum_durations_ns[i] / 1000 / 1000);

    return res;
}

std::vector<uint32_t> TimeMeasurement::GetMinDurationUs() const
{
    std::vector<uint32_t> res;
    for (size_t i = 0; i < m_minimum_durations_ns.size(); i++)
        res.push_back(m_minimum_durations_ns[i] / 1000);

    return res;
}

std::vector<uint32_t> TimeMeasurement::GetMinDurationNs() const
{
    std::vector<uint32_t> res;
    for (size_t i = 0; i < m_minimum_durations_ns.size(); i++)
        res.push_back(m_minimum_durations_ns[i]);

    return res;
}

std::vector<uint32_t> TimeMeasurement::GetMaxDurationMs() const
{
    std::vector<uint32_t> res;
    for (size_t i = 0; i < m_maximum_durations_ns.size(); i++)
        res.push_back(m_maximum_durations_ns[i] / 1000 / 1000);

    return res;
}

std::vector<uint32_t> TimeMeasurement::GetMaxDurationUs() const
{
    std::vector<uint32_t> res;
    for (size_t i = 0; i < m_maximum_durations_ns.size(); i++)
        res.push_back(m_maximum_durations_ns[i] / 1000);

    return res;
}

std::vector<uint32_t> TimeMeasurement::GetMaxDurationNs() const
{
    std::vector<uint32_t> res;
    for (size_t i = 0; i < m_maximum_durations_ns.size(); i++)
        res.push_back(m_maximum_durations_ns[i]);

    return res;
}
}  //namespace mpmca::utilities