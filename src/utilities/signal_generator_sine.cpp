/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/signal_generator_sine.hpp"

namespace mpmca::utilities {
SignalGeneratorSine::SignalGeneratorSine(ConfigurationPtr config)
    : SignalGeneratorSine(config->GetValue<double>("Amplitude", 1.0), config->GetValue<double>("Frequency", 1.0),
                          config->GetValue<double>("Phase", 1.0))
{
}

SignalGeneratorSine::SignalGeneratorSine(double amplitude, double frequency_rad, double phase)
    : m_amplitude(amplitude)
    , m_frequency(frequency_rad)
    , m_phase(phase)
{
}

double SignalGeneratorSine::CalculateValueAtTimeMs(long t_ms)
{
    double t = (double)t_ms / 1000.0;
    return m_amplitude * std::sin(m_frequency * t + m_phase);
}
}  //namespace mpmca