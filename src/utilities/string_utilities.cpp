/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/string_utilities.hpp"

namespace mpmca::utilities {

std::vector<std::string> StringUtilities::SplitString(const std::string &my_string, char c)
{
    std::string buff = "";
    std::vector<std::string> return_vector;

    for (auto my_character : my_string) {
        if (my_character != c)
            buff += my_character;
        else if (my_character == c && buff != "") {
            return_vector.push_back(buff);
            buff = "";
        }
    }

    if (buff != "")
        return_vector.push_back(buff);

    return return_vector;
}

}  //namespace mpmca