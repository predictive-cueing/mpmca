/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/file_container.hpp"

#include <algorithm>
#include <fstream>
#include <iostream>

#include "mpmca/utilities/math.hpp"

namespace mpmca::utilities {
FileContainer::FileContainer(const std::string& name, const std::string& filename, const std::string& indexing,
                             const std::vector<std::string>& field_names,  //
                             const std::vector<int64_t>& time,
                             const std::vector<double>& x,  //
                             const std::vector<double>& y,  //
                             const std::vector<double>& z,  //
                             const DynamicMatrix& data,  //
                             double dt)
    : m_name(name)
    , m_logger(name, "FileContainer")
    , m_filename(filename)
    , m_field_names(field_names)
    , m_time(time)
    , m_x(x)
    , m_y(y)
    , m_z(z)
    , m_data(data)
    , m_field_numbers(GetFieldNumbers(m_field_names))
    , m_n_samples(m_data.rows())
    , m_n_fields(m_data.cols())
    , m_start(0)
    , m_end(m_n_samples > 1 ? m_n_samples - 1 : 0)
    , m_t_min(m_time.size() > 0 ? time[0] : 0)
    , m_t_max(m_time.size() > 0 ? m_time[m_end] : 0)
    , m_time_indexing(GetTimeIndexing(indexing))
    , m_position_indexing(GetPositionIndexing(indexing))
    , m_dt(dt)
    , m_dt_ms((int64_t)std::round(dt * 1000.))
    , m_kk(0)
    , m_reached_end(false)
{
    if (m_time_indexing && m_time.size() != m_n_samples)
        throw(std::invalid_argument("Requested time indexing, but no time vector of adequate size was provided."));
    else {
        if (m_end > 1 && m_time_indexing) {
            for (size_t i = 1; i < m_n_samples; ++i) {
                if (m_time[i] - m_time[i - 1] != m_dt_ms)
                    throw(std::invalid_argument("In file " + m_filename + ", the time step at index " +
                                                std::to_string(i) + " and time " + std::to_string(m_time[i]) +
                                                " was not equal to dt (" + std::to_string(m_dt_ms) + " ms), but was " +
                                                std::to_string(m_time[i + 1] - m_time[i])));
            }
        }
    }

    if (m_position_indexing && m_x.size() != m_n_samples)
        throw(std::invalid_argument("Requested position indexing, but no x vector of adequate size was provided."));

    if (m_position_indexing && m_y.size() != m_n_samples)
        throw(std::invalid_argument("Requested position indexing, but no y vector of adequate size was provided."));

    if (m_position_indexing && m_z.size() != m_n_samples)
        throw(std::invalid_argument("Requested position indexing, but no z vector of adequate size was provided."));
}

std::unordered_map<std::string, int> FileContainer::GetFieldNumbers(const std::vector<std::string>& field_names)
{
    std::unordered_map<std::string, int> fn;

    for (size_t i = 0; i < field_names.size(); ++i) {
        if (fn.count(field_names[i]) != 0) {
            throw std::runtime_error("Field names must be unique! The field \"" + field_names[i] + "\" is duplicate.");
        }
        fn.insert({field_names[i], i});
    }

    return fn;
}

bool FileContainer::GetValidIndexingArgument(const std::string& indexing)
{
    std::vector<std::string> valid_arguments{"position based", "time based", "time and position based", "none"};

    if (!std::any_of(valid_arguments.cbegin(), valid_arguments.cend(),
                     [indexing](const std::string& i) { return indexing.compare(i) == 0; })) {
        std::string options;
        std::for_each(valid_arguments.cbegin(), valid_arguments.cend() - 1,
                      [&options](const std::string& o) { options += o + ", "; });
        options = options + valid_arguments.back();
        throw(std::invalid_argument("Indexing option " + indexing + " not understood. Valid options are: " + options +
                                    "."));
        return false;
    }
    else
        return true;
}

bool FileContainer::GetPositionIndexing(const std::string& indexing)
{
    return GetValidIndexingArgument(indexing) &&
           (indexing.compare("position based") == 0 || indexing.compare("time and position based") == 0);
}

bool FileContainer::GetTimeIndexing(const std::string& indexing)
{
    return GetValidIndexingArgument(indexing) &&
           (indexing.compare("time based") == 0 || indexing.compare("time and position based") == 0);
}

const std::string& FileContainer::GetName() const
{
    return m_name;
}
const std::string& FileContainer::GetFilename() const
{
    return m_filename;
}

int64_t FileContainer::GetCurrentIndex() const
{
    return m_kk;
}

int64_t FileContainer::SetIndex(int64_t k)
{
    m_kk = std::clamp(k, (int64_t)0, m_end);
    return m_kk;
}
double FileContainer::GetDataInField(int field_index)
{
    return GetDataInField(field_index, m_kk);
}

double FileContainer::GetDataInField(int field_index, int64_t data_idx)
{
    if (field_index >= 0 && field_index < m_n_fields) {
        if (data_idx >= m_n_samples)
            throw(std::invalid_argument("FileContainer " + m_name + ", index outside bounds: " +
                                        std::to_string(data_idx) + " / " + std::to_string(m_n_samples)));

        return m_data(data_idx, field_index);
    }
    else
        throw(std::invalid_argument("FileContainer " + m_name + ", field index " + std::to_string(field_index) +
                                    " requested, but only " + std::to_string(m_n_fields) + " fields stored."));

    return 0.0;
}

double FileContainer::GetDataInField(const std::string& name)
{
    return GetDataInField(name, m_kk);
}

int64_t FileContainer::GetNumSamples() const
{
    return m_n_samples;
}
int64_t FileContainer::GetNumFields() const
{
    return m_n_fields;
}

double FileContainer::GetDataInField(const std::string& name, int64_t index)
{
    if (m_field_numbers.count(name) == 1)
        return GetDataInField(m_field_numbers.at(name), index);
    else
        throw(std::invalid_argument("Field " + name + " was not loaded from file."));

    return 0.0;
}

double FileContainer::GetDataInFieldOrFallback(const std::string& name, double fallback_value)
{
    if (m_field_numbers.count(name) == 1)
        return GetDataInField(m_field_numbers.at(name), m_kk);
    else
        return fallback_value;
}

const std::string& FileContainer::GetFieldName(int field_id) const
{
    return m_field_names[field_id];
}

int FileContainer::GetFieldIndex(const std::string& name) const
{
    if (m_field_numbers.count(name) != 1) {
        throw std::runtime_error("Field name " + name + " does not exist in file " + GetFilename());
    }
    return m_field_numbers.at(name);
}

int FileContainer::GetFieldIndex(const std::string& name, int not_found_value) const
{
    if (m_field_numbers.count(name) == 1) {
        return m_field_numbers.at(name);
    }
    else {
        return not_found_value;
    }
}

int64_t FileContainer::SetIndexByPosition(double x_s, double y_s, double z_s, int64_t pre_samples)
{
    int64_t start_at = (m_kk + m_n_samples - pre_samples) % m_n_samples;
    int64_t ll = 0;
    int64_t iter = 0;

    double min_dist = Eigen::Infinity;

    for (iter = 0; iter < m_n_samples; ++iter) {
        int64_t jj = (start_at + iter + m_n_samples) % m_n_samples;
        double dist = ((m_x[jj] - x_s) * (m_x[jj] - x_s)) + ((m_y[jj] - y_s) * (m_y[jj] - y_s));

        if (dist < min_dist) {
            min_dist = dist;
            ll = jj;
        }

        if (min_dist < 1e-3) {
            break;
        }
    }

    m_kk = ll;

    return iter;
}

bool FileContainer::GetReachedEnd() const
{
    return m_reached_end;
}

int64_t FileContainer::SetIndexByTime(int64_t t)
{
    if (t > m_t_max) {
        if (!m_reached_end)
            m_logger.Warning("Requested " + std::to_string(t) + " but t_max = " + std::to_string(m_t_max));

        m_kk = m_end;
        m_reached_end = true;
    }
    else if (t < m_t_min) {
        m_kk = m_start;
        m_logger.Warning("Requesting data at time " + std::to_string(t) + " but t_min = " + std::to_string(m_t_min));
        m_reached_end = false || m_reached_end;
    }
    else {
        m_reached_end = false || m_reached_end;
        int64_t guess = std::clamp((int64_t)(t - m_time[0]) / m_dt_ms, (int64_t)0, m_n_samples);

        if ((m_time[guess] - t) < 1)
            m_kk = guess;
        else if (t < m_t_min)
            m_kk = 0;
        else if (t > m_t_max)
            m_kk = m_n_samples - 1;
        else {
            throw(std::runtime_error(
                "Did not find sample at expected location, something must be wrong with the data. We guessed " +
                std::to_string(guess) + ", which was equal to " + std::to_string(m_time[guess])));
        }
    }
    return m_kk;
}

int64_t FileContainer::GetTimeStartMs() const
{
    return m_t_min;
}

int64_t FileContainer::GetTimeEndMs() const
{
    return m_t_max;
}
}  //namespace mpmca::utilities
