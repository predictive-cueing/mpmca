/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/i_parameter.hpp"

#include <iostream>

namespace mpmca::utilities {
IParameter::IParameter(ParameterType type)
    : m_type(type)
{
}

}  //namespace mpmca