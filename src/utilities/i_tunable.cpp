/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/i_tunable.hpp"

namespace mpmca::utilities {
TunableRegister ITunable::s_tunable_register;

ITunable::ITunable(const std::string& basic_name, const std::string& unique_name, ParameterType type)
    : m_name_basic(basic_name)
    , m_name_unique(unique_name)
    , m_type(type)
{
    s_tunable_register.RegisterParameter(this);
}
ITunable::~ITunable()
{
    s_tunable_register.DeregisterParameter(this);
}
const std::string& ITunable::GetBasicName() const
{
    return m_name_basic;
}
const std::string& ITunable::GetUniqueName() const
{
    return m_name_unique;
}
ParameterType ITunable::GetParameterType() const
{
    return m_type;
}
TunableRegister& ITunable::GetTunableRegister()
{
    return s_tunable_register;
}
}  //namespace mpmca