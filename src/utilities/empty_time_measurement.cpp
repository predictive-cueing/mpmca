/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/empty_time_measurement.hpp"

namespace mpmca::utilities {

EmptyTimeMeasurement::EmptyTimeMeasurement()
{
}

ClockIndex EmptyTimeMeasurement::RegisterClock(const std::string& /* name */)
{
    return 0;
}

void EmptyTimeMeasurement::StartCycle()
{
}

void EmptyTimeMeasurement::CalculateStatistics()
{
}

void EmptyTimeMeasurement::PrintStatisticsMs()
{
}
void EmptyTimeMeasurement::PrintStatisticsUs()
{
}
void EmptyTimeMeasurement::PrintStatisticsNs()
{
}

void EmptyTimeMeasurement::ResetStatistics()
{
}

EmptyTimeMeasurement::clock::duration EmptyTimeMeasurement::StopClock(const ClockIndex& /*index*/)
{
    return clock::duration(0);
}

EmptyTimeMeasurement::clock::time_point EmptyTimeMeasurement::ClockTime(const ClockIndex& /*index*/)
{
    return clock::now();
}

EmptyTimeMeasurement::clock::duration EmptyTimeMeasurement::ClockDuration(const ClockIndex& /*index*/)
{
    return clock::duration(0);
}

uint32_t EmptyTimeMeasurement::ClockDurationSinceCycleStartUs(const ClockIndex& /*index*/) const
{
    return 0;
}

uint32_t EmptyTimeMeasurement::ClockDurationSinceCycleStartNs(const ClockIndex& /*index*/) const
{
    return 0;
}

uint32_t EmptyTimeMeasurement::ClockDurationSinceCycleStartMs(const ClockIndex& /*index*/) const
{
    return 0;
}

uint32_t EmptyTimeMeasurement::ClockDurationSinceCreationNs(const ClockIndex& /*index*/) const
{
    return 0;
}

uint32_t EmptyTimeMeasurement::ClockDurationSinceCreationUs(const ClockIndex& /*index*/) const
{
    return 0;
}

uint32_t EmptyTimeMeasurement::ClockDurationSinceCreationMs(const ClockIndex& /*index*/) const
{
    return 0;
}

uint32_t EmptyTimeMeasurement::ClockDurationNs(const ClockIndex& /*index*/) const
{
    return 0;
}

uint32_t EmptyTimeMeasurement::ClockDurationUs(const ClockIndex& /*index*/) const
{
    return 0;
}

uint32_t EmptyTimeMeasurement::ClockDurationMs(const ClockIndex& /*index*/) const
{
    return 0;
}

std::string EmptyTimeMeasurement::ClockName(const ClockIndex& /*index*/) const
{
    return "";
}

int EmptyTimeMeasurement::NumClocks() const
{
    return 0;
}

std::vector<std::string> EmptyTimeMeasurement::GetClockNames() const
{
    return std::vector<std::string>{};
}

std::vector<uint32_t> EmptyTimeMeasurement::ClockDurationsSinceCycleStartUs() const
{
    return std::vector<std::uint32_t>{};
}

std::vector<uint32_t> EmptyTimeMeasurement::ClockDurationsNs() const
{
    return std::vector<std::uint32_t>{};
}

std::vector<uint32_t> EmptyTimeMeasurement::ClockDurationsUs() const
{
    return std::vector<std::uint32_t>{};
}

std::vector<uint32_t> EmptyTimeMeasurement::ClockDurationsMs() const
{
    return std::vector<std::uint32_t>{};
}
}  //namespace mpmca::utilities