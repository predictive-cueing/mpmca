/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/file_tools.hpp"

namespace mpmca::utilities {

bool FileTools::GetFileExists(const std::string& filename)
{
    if (FILE* file = fopen(filename.c_str(), "r")) {
        fclose(file);
        return true;
    }
    else {
        return false;
    }
}

FileParts FileTools::GetFileParts(const std::string& fullpath)
{
    size_t idx_slash = fullpath.rfind("/");
    if (idx_slash == std::string::npos) {
        idx_slash = fullpath.rfind("\\");
    }
    size_t idx_dot = fullpath.rfind(".");

    FileParts fp;
    if (idx_slash != std::string::npos && idx_dot != std::string::npos) {
        fp.path = fullpath.substr(0, idx_slash + 1);
        fp.name = fullpath.substr(idx_slash + 1, idx_dot - idx_slash - 1);
        fp.ext = fullpath.substr(idx_dot);
    }
    else if (idx_slash == std::string::npos && idx_dot == std::string::npos) {
        fp.name = fullpath;
    }
    else if (/* only */ idx_slash == std::string::npos) {
        fp.name = fullpath.substr(0, idx_dot);
        fp.ext = fullpath.substr(idx_dot);
    }
    else {
        fp.path = fullpath.substr(0, idx_slash + 1);
        fp.name = fullpath.substr(idx_slash + 1);
    }
    return fp;
}

}  //namespace mpmca