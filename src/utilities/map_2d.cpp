/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/map_2d.hpp"

#include "mpmca/json_tools.hpp"

namespace mpmca::utilities {
Map2D::Map2D()
    : Map2D({0, 1}, {0, 1}, {{0, 1}, {0, 1}})
{
}

Map2D::Map2D(const std::initializer_list<double>& x, const std::initializer_list<double>& y,
             const std::initializer_list<std::vector<double>>& value)
    : Map2D(LinearAlgebra::InitDynamicRowVector(x), LinearAlgebra::InitDynamicRowVector(y),
            LinearAlgebra::InitDynamicMatrix(value))
{
}

Map2D::Map2D(const DynamicRowVector& x, const DynamicRowVector& y, const DynamicMatrix& value)
    : IParameter(ParameterType::kMap2D)
    , m_x_vector(x)
    , m_y_vector(y)
    , m_value_matrix(value)
    , m_requires_process_call(true)
{
    ProcessNewValues();
}

Map2D::Map2D(ConfigurationPtr config)
    : Map2D(config->GetValue<DynamicRowVector>("x", {0, 1}), config->GetValue<DynamicRowVector>("y", {0, 1}),
            config->GetValue<DynamicMatrix>("Value", {0, 1}))
{
}

void Map2D::SetXVector(const std::vector<double>& x_vector)
{
    m_x_vector = LinearAlgebra::InitDynamicRowVector(x_vector);
    m_requires_process_call = true;
}
void Map2D::SetYVector(const std::vector<double>& y_vector)
{
    m_y_vector = LinearAlgebra::InitDynamicRowVector(y_vector);
    m_requires_process_call = true;
}
void Map2D::SetValueMatrix(const std::vector<std::vector<double>>& value_matrix)
{
    m_value_matrix = LinearAlgebra::InitDynamicMatrix(value_matrix);
    m_requires_process_call = true;
}

const DynamicRowVector& Map2D::GetXVector() const
{
    return m_x_vector;
}
const DynamicRowVector& Map2D::GetYVector() const
{
    return m_y_vector;
}
const DynamicMatrix& Map2D::GetValueMatrix() const
{
    return m_value_matrix;
}

void Map2D::ProcessNewValues()
{
    if (m_x_vector.size() != m_value_matrix.rows())
        throw std::invalid_argument("Length of x and the number of rows in the matrix should be equal.");

    if (m_y_vector.size() != m_value_matrix.cols())
        throw std::invalid_argument("Length of y and the number of columns in the matrix should be equal.");

    m_x_min = 1e50;
    m_x_max = -1e50;
    m_y_min = 1e50;
    m_y_max = -1e50;

    for (int j = 0; j < m_y_vector.size(); ++j) {
        m_y_min = std::min(m_y_min, m_y_vector[j]);
        m_y_max = std::max(m_y_max, m_y_vector[j]);
    }

    for (int j = 0; j < m_x_vector.size(); ++j) {
        m_x_min = std::min(m_x_min, m_x_vector[j]);
        m_x_max = std::max(m_x_max, m_x_vector[j]);
    }

    m_x_mid_vector = DynamicRowVector(m_x_vector.size() - 1);
    m_y_mid_vector = DynamicRowVector(m_y_vector.size() - 1);

    for (int j = 0; j < m_x_vector.size() - 1; ++j)
        m_x_mid_vector[j] = (m_x_vector[j + 1] + m_x_vector[j]) / 2.0;

    for (int j = 0; j < m_y_vector.size() - 1; ++j)
        m_y_mid_vector[j] = (m_y_vector[j + 1] + m_y_vector[j]) / 2.0;

    m_dy_vector = DynamicRowVector(m_y_vector.size() - 1);
    for (int j = 0; j < m_y_vector.size() - 1; ++j)
        m_dy_vector[j] = m_y_vector[j + 1] - m_y_vector[j];

    m_d_value_dx_matrix = DynamicMatrix(m_x_vector.size() - 1, m_y_vector.size());
    for (int j = 0; j < m_y_vector.size(); ++j) {
        for (int i = 0; i < m_x_vector.size() - 1; ++i) {
            m_d_value_dx_matrix(i, j) =
                (m_value_matrix(i + 1, j) - m_value_matrix(i, j)) / (m_x_vector[i + 1] - m_x_vector[i]);
        }
    }

    m_d_value_dy_matrix = DynamicMatrix(m_x_vector.size(), m_y_vector.size() - 1);
    for (int j = 0; j < m_y_vector.size() - 1; ++j) {
        for (int i = 0; i < m_x_vector.size(); ++i) {
            m_d_value_dy_matrix(i, j) =
                (m_value_matrix(i, j + 1) - m_value_matrix(i, j)) / (m_y_vector[j + 1] - m_y_vector[j]);
        }
    }

    m_requires_process_call = false;
}

double Map2D::GetValue(double at_x, double at_y, InterpolationMethod method) const
{
    if (method == Map2D::kNearestNeighbour)
        return Nearest(at_x, at_y);
    else
        return Interp2(at_x, at_y);
}

double Map2D::GetValue(double at_x, double at_y, double outside, InterpolationMethod method) const
{
    if (at_x < m_x_min || at_x > m_x_max || at_y < m_y_min || at_y > m_y_max)
        return outside;
    else
        return GetValue(at_x, at_y, method);
}

double Map2D::Interp2(double at_x, double at_y) const
{
    int i = 0;
    while (i < m_x_vector.size() - 2 && at_x > m_x_vector[i + 1])
        ++i;

    int j = 0;
    while (j < m_y_vector.size() - 2 && at_y > m_y_vector[j + 1])
        ++j;

    double dx = (at_x - m_x_vector[i]);
    double R1 = m_value_matrix(i, j) + m_d_value_dx_matrix(i, j) * dx;
    double R2 = m_value_matrix(i, j + 1) + m_d_value_dx_matrix(i, j + 1) * dx;
    double P = R1 + (R2 - R1) * (at_y - m_y_vector[j]) / m_dy_vector[j];
    return P;
}

double Map2D::Nearest(double at_x, double at_y) const
{
    int i = 0;
    while (i < m_x_mid_vector.size() && at_x > m_x_mid_vector[i])
        ++i;

    int j = 0;
    while (j < m_y_mid_vector.size() && at_y > m_y_mid_vector[j])
        ++j;

    return m_value_matrix(i, j);
}
}  //namespace mpmca::utilities