/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/tunable_register.hpp"

#include <algorithm>

#include "mpmca/utilities/i_tunable.hpp"

namespace mpmca::utilities {
TunableRegister::TunableRegister()
    : m_logger("TunableRegister", "TunableRegister")
{
}

void TunableRegister::RegisterParameter(ITunable* parameter_ptr)
{
    if (!ExistsInRegister(parameter_ptr->GetUniqueName())) {
        m_register.insert(std::make_pair(parameter_ptr->GetUniqueName(), parameter_ptr));
        m_logger.Trace("Registered tunable parameter " + parameter_ptr->GetUniqueName());
    }
    else
        throw(std::runtime_error("A parameter with the name " + parameter_ptr->GetUniqueName() +
                                 " was already registered."));
}
bool TunableRegister::ExistsInRegister(const std::string& name) const
{
    return m_register.find(name) != m_register.end();
}
void TunableRegister::DeregisterParameter(ITunable* parameter_ptr)
{
    if (ExistsInRegister(parameter_ptr->GetUniqueName()))
        m_register.erase(parameter_ptr->GetUniqueName());
    else
        throw(std::runtime_error("Cannot deregister " + parameter_ptr->GetUniqueName() +
                                 ", because it does not exist in the register."));
}

ITunable* TunableRegister::GetParameterByUniqueName(const std::string& name) const
{
    if (ExistsInRegister(name))
        return m_register.at(name);
    else
        return nullptr;
}
std::vector<ITunable*> TunableRegister::GetAllParametersByBasicName(const std::string& name) const
{
    // This function search through the register and finds all parameters with basic name equal to name
    // and returns as a vector of ITunable pointers.
    std::vector<ITunable*> ret_value;
    std::for_each(m_register.cbegin(), m_register.cend(), [&ret_value, &name](std::pair<std::string, ITunable*> i) {
        if (i.second->GetBasicName().compare(name) == 0)
            ret_value.push_back(i.second);
    });
    return ret_value;
}
}  //namespace mpmca::utilities
