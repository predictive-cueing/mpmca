/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/a2d2_file_reader.hpp"

#include <filesystem>

#include "mpmca/data_names.hpp"
#include "mpmca/json.hpp"
#include "mpmca/utilities/math.hpp"

namespace mpmca::utilities {

namespace {
nlohmann::json ReadFile(const std::string& filename)
{
    if (!std::filesystem::exists(filename)) {
        throw(std::invalid_argument("File does not exist : " + filename));
    }
    nlohmann::json json_data;

    try {
        std::ifstream(filename) >> json_data;
    }
    catch (const std::exception& c) {
        throw(std::invalid_argument("Exception while reading json file " + filename + ".\nMessage: " + c.what()));
    }
    return json_data;
}
}  //namespace

FileContainer A2D2FileReader::ContainerFromA2D2JsonFile(const std::string& name, const std::string& filename)
{
    auto json_data = ReadFile(filename);

    size_t data_length = json_data.at("acceleration_x").at("values").size();
    std::vector<std::string> field_names = {
        DN::f_x_head().GetName(),     DN::f_y_head().GetName(),     DN::f_z_head().GetName(),
        DN::omega_x_head().GetName(), DN::omega_y_head().GetName(), DN::omega_z_head().GetName(),
    };

    std::vector<double> x_y_z_data;
    std::vector<int64_t> time_ms;
    DynamicMatrix data;

    data.resize(data_length, 9);
    time_ms.resize(data_length);

    for (size_t ii = 0; ii < data_length; ++ii) {
        // The data in the A2D2 set is not always sampled exactly 5 ms apart.
        // FileContainer will check the provided time vector and throw an
        // exception if it detects incorrect time stamps. Since the dataset is
        // used only for demonstration purposes, we simply construct an
        // artificial time vector based on the index, instead of resampling the
        // data. Obviously, for the best cueing results, you would have to
        // properly resample the data.
        time_ms[ii] = static_cast<int64_t>(ii * 5);
        data(ii, 0) = -1.0 * static_cast<double>(json_data.at("acceleration_x").at("values").at(ii).at(1));
        data(ii, 1) = static_cast<double>(json_data.at("acceleration_y").at("values").at(ii).at(1));
        data(ii, 2) = static_cast<double>(json_data.at("acceleration_z").at("values").at(ii).at(1));
        data(ii, 3) = utilities::Math::ConvertDegreesToRadians(
            json_data.at("angular_velocity_omega_x").at("values").at(ii).at(1));
        data(ii, 4) = utilities::Math::ConvertDegreesToRadians(
            json_data.at("angular_velocity_omega_y").at("values").at(ii).at(1));
        data(ii, 5) = -1.0 * static_cast<double>(utilities::Math::ConvertDegreesToRadians(
                                 json_data.at("angular_velocity_omega_z").at("values").at(ii).at(1)));
        data(ii, 6) = 0.0;
        data(ii, 7) = 0.0;
        data(ii, 8) = 0.0;
    }

    return FileContainer(name, filename, "time based",  //
                         field_names, time_ms, {}, {}, {},  //
                         data, 0.005);
}

}  //namespace mpmca::utilities