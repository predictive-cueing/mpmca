/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/tunable_change.hpp"

#include <algorithm>
#include <stdexcept>

namespace mpmca::utilities {
TunableChange::TunableChange(int64_t t_start_ms, int64_t t_change_duration_ms, ChangeMethod method)
    : m_t_start_ms(t_start_ms)
    , m_t_change_duration_ms(t_change_duration_ms)
    , m_t_start_ms_real((double)t_start_ms)
    , m_t_change_duration_ms_real((double)t_change_duration_ms)
    , m_change_method(method)
{
    if (method != ChangeMethod::kStep && m_t_change_duration_ms <= 0)
        throw(std::invalid_argument("Change duration should be larger than 0."));
}
double TunableChange::Weight(int64_t t_current_ms) const
{
    switch (m_change_method) {
        case ChangeMethod::kStep:
            return double(t_current_ms >= m_t_start_ms);
            break;
        case ChangeMethod::kLinear:
            return (double)(t_current_ms >= m_t_start_ms) *
                   (std::max((double)0,
                             std::min((double)t_current_ms - m_t_start_ms_real, m_t_change_duration_ms_real) /
                                 (m_t_change_duration_ms_real)));
            break;
        case ChangeMethod::kCosine:
            return 1.0;
            break;
    }
    return 1.0;
}
}  //namespace mpmca::utilities