/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/udp_sender_receiver.hpp"

namespace mpmca::utilities {

UdpSenderReceiver::UdpSenderReceiver(const std::string& name, ConfigurationPtr config)
    : UdpSenderReceiver(name, config->GetValue<std::string>("LocalEndpointHost", "127.0.0.1"),
                        config->GetValue<std::string>("LocalEndpointPort", "1234"),
                        config->GetValue<std::string>("RemoteEndpointHost", "127.0.0.1"),
                        config->GetValue<std::string>("RemoteEndpointPort", "2345"),
                        config->GetValue<bool>("ThrowOnError", true))
{
}

UdpSenderReceiver::UdpSenderReceiver(const std::string& name, const std::string& local_endpoint_host,
                                     const std::string& local_endpoint_port, const std::string& remote_endpoint_host,
                                     const std::string& remote_endpoint_port, bool throw_on_error)
    : m_initialized(false)
    , m_name(name)
    , m_logger(m_name, "UdpSenderReceiver")
    , m_local_endpoint_host(CheckEndpoint(local_endpoint_host, local_endpoint_port))
    , m_remote_endpoint_host(CheckEndpoint(remote_endpoint_host, remote_endpoint_port))
    , m_resolver(m_io_service)
    , m_local_endpoint(::asio::ip::address::from_string(local_endpoint_host), std::stoi(local_endpoint_port))
    , m_socket(m_io_service, m_local_endpoint)
{
    try {
        ::asio::ip::udp::resolver::query query(::asio::ip::udp::v4(), remote_endpoint_host, remote_endpoint_port,
                                               ::asio::ip::udp::resolver::query::canonical_name);
        m_iter = m_resolver.resolve(query);
        m_remote_endpoint = *m_iter;
        m_initialized = true;

        m_logger.Info("Will send and receive to/from remote endpoint: " + remote_endpoint_host + ":" +
                      remote_endpoint_port + " at local endpoint: " + local_endpoint_host + ":" + local_endpoint_port);
    }
    catch (std::exception& e) {
        std::string e_message = "Something wrong in UdpSenderReceiver " + name +
                                ", with  remote endpoint: " + remote_endpoint_host + ":" + remote_endpoint_port +
                                " at local endpoint: " + local_endpoint_host + ":" + local_endpoint_port + " " +
                                e.what();
        if (throw_on_error)
            throw(std::runtime_error(e_message));
        else
            m_logger.Warning(e_message);
    }
}

std::string UdpSenderReceiver::CheckEndpoint(const std::string& endpoint_hostname, const std::string& endpoint_port)
{
    try {
        ::asio::ip::udp::endpoint endpoint(::asio::ip::address::from_string(endpoint_hostname),
                                           std::stoi(endpoint_port));
    }
    catch (const std::exception& exception) {
        throw std::runtime_error("Error trying to create endpoint from endpoint hostname \"" + endpoint_hostname +
                                 "\" and port \"" + endpoint_port + "\".");
    }
    return endpoint_hostname;
}

std::string UdpSenderReceiver::GetName()
{
    return m_name;
}

bool UdpSenderReceiver::SendRawData(const std::vector<char>& buffer)
{
    ::asio::error_code err;
    m_socket.send_to(::asio::buffer(buffer), m_remote_endpoint, 0, err);

    if (m_sending_error_code && !err) {
        m_logger.Info("Error SendRawData: " + m_sending_error_code.message() + " cleared!");
        m_sending_error_code = err;
    }

    if (err) {
        if (m_sending_error_code != err) {
            m_sending_error_code = err;
            m_logger.Error("Error SendRawData: " + m_sending_error_code.message());
        }
        return false;
    }
    else
        return true;
}
void UdpSenderReceiver::ClearBuffer()
{
    if (!m_initialized)
        return;

    std::vector<char> buffer;
    try {
        int bytes_available = m_socket.available();

        if (bytes_available == 0)
            return;

        int packet_size = 0;

        while (bytes_available > packet_size) {
            bytes_available = m_socket.available();
            buffer.resize(bytes_available);
            ::asio::error_code err;
            packet_size = m_socket.receive_from(::asio::buffer(buffer), m_remote_endpoint, 0, err);
        }
    }
    catch (std::exception& e) {
        m_logger.Error("Error : " + std::string(e.what()));
    }
}
}  //namespace mpmca