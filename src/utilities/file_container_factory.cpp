/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */

#include "mpmca/utilities/file_container_factory.hpp"

#include <string>

#include "mpmca/utilities/a2d2_file_reader.hpp"
#include "mpmca/utilities/mpmca_json_file_reader.hpp"
namespace mpmca::utilities {

FileContainer FileContainerFactory::ConstructFileContainer(ConfigurationPtr config, double dt)
{
    auto file_config = config->GetConfigurationObject("FileReader");
    std::string file_type = file_config->GetValue<std::string>("FileType", "MpmcaJson");

    if (file_type == "MpmcaJson") {
        return utilities::MpmcaJsonFileReader::ContainerFromMpmcaJsonFile(
            "MpmcaJsonFile", file_config->GetValue<std::string>("Filename", "./path/to/file.json"),
            file_config->GetValue<std::string>("Indexing", "time-based"), dt);
    }
    else if (file_type == "A2D2Json") {
        return utilities::A2D2FileReader::ContainerFromA2D2JsonFile(
            "A2D2JsonFile", file_config->GetValue<std::string>("Filename", "./path/to/file.json"));
    }
    else {
        throw std::runtime_error("Unknown file type " + file_type);
    }
}
}  //namespace mpmca::utilities