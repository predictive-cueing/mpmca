/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/configuration.hpp"

#include <filesystem>
#include <fstream>

namespace mpmca::utilities {
ConfigurationPtr Configuration::CreateRootConfig(const std::string& name, bool use_default_values)
{
    return std::shared_ptr<Configuration>(new Configuration(name, use_default_values), Configuration::Deletor);
}
ConfigurationPtr Configuration::CreateRootConfig(const std::string& name, nlohmann::json js, bool use_default_values)
{
    return std::shared_ptr<Configuration>(new Configuration(name, js, use_default_values), Configuration::Deletor);
}

ConfigurationPtr Configuration::CreateChildConfig(const std::string& name, std::weak_ptr<Configuration> parent,
                                                  std::shared_ptr<Configuration> root, nlohmann::json js,
                                                  bool is_object_array, bool use_default_values)
{
    return std::shared_ptr<Configuration>(
        new Configuration(name, parent, root, js, is_object_array, use_default_values), Configuration::Deletor);
}
ConfigurationPtr Configuration::CreateChildConfig(size_t index, std::weak_ptr<Configuration> parent,
                                                  std::shared_ptr<Configuration> root, nlohmann::json js,
                                                  bool use_default_values)
{
    return std::shared_ptr<Configuration>(new Configuration(index, parent, root, js, use_default_values),
                                          Configuration::Deletor);
}

Configuration::Configuration(const std::string& name, bool use_default_values)
    : m_logger(name, "Configuration")
    , m_name(name)
    , m_is_object_array_element(false)
    , m_is_object_array(false)
    , m_use_default_values(use_default_values)
    , m_index(0)
    , m_child_index(-1)
    , m_is_root(true)
    , m_configuration_path(name)
{
}
Configuration::Configuration(const std::string& name, nlohmann::json js, bool use_default_values)
    : m_logger(name, "Configuration")
    , m_name(name)
    , m_is_object_array_element(false)
    , m_is_object_array(false)
    , m_use_default_values(use_default_values)
    , m_index(0)
    , m_child_index(-1)
    , m_is_root(true)
    , m_configuration_path(name)
{
    SetJson(js);
}
Configuration::Configuration(const std::string& name, std::weak_ptr<Configuration> parent,
                             std::shared_ptr<Configuration> root, nlohmann::json js, bool is_object_array,
                             bool use_default_values)
    : m_logger(name, "Configuration")
    , m_name(name)
    , m_is_object_array_element(false)
    , m_is_object_array(is_object_array)
    , m_use_default_values(use_default_values)
    , m_index(0)
    , m_child_index(-1)
    , m_is_root(false)
    , m_root(root)
    , m_parent(parent)
    , m_configuration_path(name)
{
    SetJson(js);
}
Configuration::Configuration(size_t index, std::weak_ptr<Configuration> parent, std::shared_ptr<Configuration> root,
                             nlohmann::json js, bool use_default_values)
    : m_logger("ConfigurationObject" + std::to_string(index), "Configuration")
    , m_name(std::to_string(index))
    , m_is_object_array_element(true)
    , m_is_object_array(false)
    , m_use_default_values(use_default_values)
    , m_index(index)
    , m_child_index(-1)
    , m_is_root(false)
    , m_root(root)
    , m_parent(parent)
    , m_configuration_path("[" + std::to_string(index) + "]")
{
    SetJson(js);
}
Configuration::~Configuration()
{
}

void Configuration::Deletor(Configuration* c)
{
    if (!c->m_parent.expired()) {
        auto p = c->m_parent.lock();
        if (p)
            p->SetChildJson(c->m_child_index, c->m_name, c->m_index, c->GetChildrenJson(),
                            c->m_is_object_array_element);
    }
    delete c;
}

ConfigurationPtr Configuration::CreateConfigFromString(const std::string& name, const std::string& json,
                                                       bool use_default_values)
{
    auto cfg = Configuration::CreateRootConfig(name, use_default_values);
    cfg->ReadJsonFromString(json);
    cfg->SetPath("[Not loaded from file.]");
    return cfg;
}

ConfigurationPtr Configuration::CreateConfigFromFile(const std::string& name, const std::string& filename,
                                                     bool use_default_values)
{
    auto cfg = Configuration::CreateRootConfig(name, use_default_values);
    cfg->ReadJsonFromFile(filename);
    cfg->ReadParentJsonFile();
    cfg->SetPath(filename);
    return cfg;
}

std::string Configuration::GetPath() const
{
    if (auto p = m_parent.lock())
        return p->GetPath() + ":" + m_path;
    else
        return m_path;
}
std::string Configuration::GetConfigurationPath() const
{
    if (auto p = m_parent.lock())
        return p->GetConfigurationPath() + ":" + m_configuration_path;
    else
        return m_configuration_path;
}
void Configuration::SetPath(const std::string& path)
{
    m_path = path;
}
void Configuration::SetConfigurationPath(const std::string& path)
{
    m_configuration_path = path;
}
std::string Configuration::GetName() const
{
    return m_name;
}
void Configuration::Update(nlohmann::json j)
{
    m_my_json = MergeJson(m_my_json, j);
}

size_t Configuration::GetNumObjectArrayElements() const
{
    if (!m_is_object_array || !m_my_json.is_array())
        throw(std::runtime_error(m_name + " is not an object array in the configuration object with path:\n" +
                                 GetConfigurationPath()));

    return m_my_json.size();
}

size_t Configuration::GetNumObjectElements() const
{
    if (m_is_object_array || m_my_json.is_array())
        throw(std::runtime_error(m_name + " is not an object in the configuration object with path:\n" +
                                 GetConfigurationPath()));

    return m_my_json.size();
}

std::vector<std::string> Configuration::GetObjectNames() const
{
    if (m_is_object_array || m_my_json.is_array())
        throw(std::runtime_error(m_name + " is not an object in the configuration object with path:\n" +
                                 GetConfigurationPath()));

    std::vector<std::string> names;
    for (auto it = m_my_json.begin(); it != m_my_json.end(); ++it)
        names.push_back(it.key());

    return names;
}

size_t Configuration::GetNumArrayElements() const
{
    if (m_is_object_array || !m_my_json.is_array())
        throw(std::runtime_error(m_name + " is not an array in the configuration object with path:\n" +
                                 GetConfigurationPath()));

    return m_my_json.size();
}

nlohmann::json Configuration::GetChildrenJson()
{
    for (size_t i = 0; i < m_children.size(); ++i)
        GetChildJson(i);

    return m_my_json;
}

void Configuration::ReadParentJsonFile()
{
    if (!GetKeyExists("ParentConfigFile")) {
        m_logger.Info("No ParentConfigFile specified in configuration file.");
        return;
    }
    else {
        std::string parent_path = GetValue<std::string>("ParentConfigFile", "parent_file.json");
        m_logger.Info("ParentConfigFile in configuration file was set to " + parent_path);
        MergeParentJsonFile(m_path, parent_path);
    }
}

void Configuration::MergeParentJsonFile(const std::string& current_path, const std::string& filename)
{
    nlohmann::json parent_json;
    std::string new_path;

    if (FileTools::GetFileExists(current_path + filename)) {
        std::ifstream(current_path + filename) >> parent_json;
        m_my_json = MergeJson(parent_json, m_my_json);
        new_path = FileTools::GetFileParts(current_path + filename).path;
    }
    else if (FileTools::GetFileExists(filename)) {
        std::ifstream(filename) >> parent_json;
        m_my_json = MergeJson(parent_json, m_my_json);
        new_path = FileTools::GetFileParts(filename).path;
    }
    else {
        throw(std::runtime_error("Could not find parent config file \"" + (current_path + filename) + "\" or \"" +
                                 filename + "\"."));
    }

    if (parent_json.count("ParentConfigFile") == 1 && parent_json["ParentConfigFile"].is_string())
        MergeParentJsonFile(new_path, parent_json["ParentConfigFile"]);
}

void Configuration::SetChildJson(size_t child_index, const std::string& child_name, size_t array_index,
                                 nlohmann::json child_json, bool is_object_array_element)
{
    if (child_index >= m_children.size()) {
        throw(std::runtime_error("Requesting child #" + std::to_string(child_index) + " which is outside the range " +
                                 std::to_string(m_children.size()) + " in the configuration object with path:\n" +
                                 GetConfigurationPath()));
    }

    if (IsObjectArray()) {
        if (!is_object_array_element) {
            throw(std::runtime_error(
                "A child (index: " + std::to_string(child_index) + ", name : " + child_name +
                ", array_index: " + std::to_string(array_index) +
                ") of an object array is not an object array element in the configuration object with path:\n" +
                GetConfigurationPath()));
        }

        m_my_json = MergeJsonArray(m_my_json, child_json, array_index);
    }
    else {
        if (is_object_array_element)
            throw(std::runtime_error(
                "An object array element should be child to an object array in the configuration object with path:\n" +
                GetConfigurationPath()));

        m_my_json[child_name] = MergeJson(m_my_json[child_name], child_json);
    }
}
nlohmann::json Configuration::MergeJsonArray(nlohmann::json j1, nlohmann::json j2, size_t array_index)
{
    nlohmann::json jm;

    if (!j1.is_array())
        throw(std::runtime_error("j1 is not an array."));

    if (array_index > j1.size())
        j1[array_index] = j2;
    else
        j1[array_index] = MergeJson(j1[array_index], j2);

    return j1;
}
nlohmann::json Configuration::MergeJson(nlohmann::json j1, nlohmann::json j2)
{
    nlohmann::json jm;

    if (j1.is_array() && j2.is_array()) {
        jm = j1;
        for (size_t i = 0; i < j2.size(); ++i) {
            jm = MergeJsonArray(jm, j2[i], i);
        }
    }
    else if (j1.is_object() && j2.is_object()) {
        jm = nlohmann::json::object();

        for (auto& el : j1.items()) {
            if (j2.count(el.key()) > 0) {
                jm[el.key()] = MergeJson(j1[el.key()], j2[el.key()]);
            }
            else {
                jm[el.key()] = j1[el.key()];
            }
        }

        for (auto& el : j2.items()) {
            if (jm.count(el.key()) == 0) {
                jm[el.key()] = j2[el.key()];
            }
        }
    }
    else if (j1.is_primitive() && j2.is_primitive()) {
        if (j1.is_null())
            jm = j2;
        else if (j2.is_null())
            jm = j1;
        else
            jm = j2;
    }
    else if (j1.is_null() && j2.is_null()) {
        throw(std::runtime_error("Two nulls, what to do?"));
    }
    else if (j1.is_null()) {
        jm = j2;
    }
    else if (j2.is_null()) {
        jm = j1;
    }
    else {
        throw(std::runtime_error("Cannot merge " + j1.dump() + " and " + j2.dump()));
    }

    return jm;
}

nlohmann::json Configuration::GetChildJson(size_t index)
{
    if (index >= m_children.size()) {
        throw(std::runtime_error("Requesting child #" + std::to_string(index) + " which is outside the range " +
                                 std::to_string(m_children.size()) + " in the configuration object with path:\n" +
                                 GetConfigurationPath()));
    }

    auto c = m_children[index].lock();
    if (c)
        SetChildJson(c->m_child_index, c->m_name, c->m_index, c->GetChildrenJson(), c->m_is_object_array_element);

    return m_my_json;
}
bool Configuration::IsObjectArray() const
{
    return m_is_object_array;
}
bool Configuration::IsObjectArrayElement() const
{
    return m_is_object_array_element;
}
size_t Configuration::GetIndex() const
{
    return m_index;
}
void Configuration::SetChildIndex(size_t index)
{
    m_child_index = index;
}
size_t Configuration::GetChildIndex() const
{
    return m_child_index;
}
bool Configuration::GetKeyExists(const std::string& name) const
{
    if (name.empty())
        throw(std::runtime_error("Name should not be empty."));

    if (m_my_json.is_null())
        return false;

    if (m_my_json.count(name) > 1) {
        throw(std::runtime_error("There is more than one key with the name " + name +
                                 " in the configuration object with path:\n" + GetConfigurationPath()));
    }

    return m_my_json.count(name) == 1;
}

bool Configuration::IsBoolean(const std::string& name) const
{
    return GetKeyExists(name) && m_my_json[name].is_boolean();
}
bool Configuration::IsString(const std::string& name) const
{
    return GetKeyExists(name) && m_my_json[name].is_string();
}
bool Configuration::IsNumber(const std::string& name) const
{
    return GetKeyExists(name) && m_my_json[name].is_number();
}
bool Configuration::IsArray(const std::string& name) const
{
    return GetKeyExists(name) && m_my_json[name].is_array();
}

bool Configuration::IsObject(const std::string& name) const
{
    return GetKeyExists(name) && m_my_json[name].is_object();
}

ConfigurationPtr Configuration::GetConfigurationObject(const std::string& name, bool create_if_does_not_exist)
{
    if (!GetKeyExists(name)) {
        if (m_use_default_values || create_if_does_not_exist)
            m_my_json[name] = nlohmann::json::object();
        else
            throw(std::runtime_error("Key " + name + " does not exist in the configuration object with path:\n" +
                                     GetConfigurationPath()));
    }

    return AddChild(CreateChildConfig(name, shared_from_this(), GetRootConfiguration(), m_my_json[name], false,
                                      m_use_default_values));
}

ConfigurationPtr Configuration::GetRootConfiguration()
{
    if (m_is_root)
        return shared_from_this();
    else
        return m_root;
}

ConfigurationPtr Configuration::GetConfigurationObjectArrayElement(size_t index)
{
    if (!m_my_json.is_array())
        throw(
            std::runtime_error("This configuration object does not contain an array of objects, for the configuration "
                               "object with path:\n" +
                               GetConfigurationPath()));

    for (size_t i = 0; i < std::max(index + 1, (size_t)m_my_json.size()); ++i) {
        if (!m_my_json[i].is_object() && !m_my_json[i].is_null()) {
            throw(std::runtime_error("Field #" + std::to_string(i) +
                                     " is not an object in the configuration object with path:\n" +
                                     GetConfigurationPath()));
        }
        else if (m_my_json[i].is_null()) {
            if (m_use_default_values)
                m_my_json[i] = nlohmann::json::object();
            else {
                throw(std::runtime_error("Index " + std::to_string(index) +
                                         " does not exist in the configuration object array with the path " +
                                         GetConfigurationPath()));
            }
        }
    }

    if (index >= m_my_json.size())
        throw(std::runtime_error("Index " + std::to_string(index) +
                                 " is outside the valid range in the configuration object array with the path:\n" +
                                 GetConfigurationPath()));

    return AddChild(
        CreateChildConfig(index, shared_from_this(), GetRootConfiguration(), m_my_json[index], m_use_default_values));
}

ConfigurationPtr Configuration::GetArray(const std::string& name)
{
    if (!GetKeyExists(name)) {
        if (m_use_default_values)
            m_my_json[name] = nlohmann::json::array();
        else {
            throw(std::runtime_error("Array with name " + name +
                                     " does not exist in the configuration object with the path:\n" +
                                     GetConfigurationPath()));
        }
    }

    if (!m_my_json[name].is_array()) {
        throw(std::runtime_error("Field " + name +
                                 " is not an array of values in the configuration object with the path:\n" +
                                 GetConfigurationPath()));
    }

    return AddChild(CreateChildConfig(name, shared_from_this(), GetRootConfiguration(), m_my_json[name], false,
                                      m_use_default_values));
}

ConfigurationPtr Configuration::AddChild(ConfigurationPtr cfg)
{
    m_children.push_back(cfg);
    cfg->SetChildIndex(m_children.size() - 1);
    return cfg;
}

ConfigurationPtr Configuration::GetConfigurationObjectArray(const std::string& name, size_t num_items)
{
    if (!GetKeyExists(name)) {
        if (m_use_default_values) {
            m_my_json[name] = nlohmann::json::array();
            for (size_t i = 0; i < num_items; ++i)
                m_my_json[name][i] = nlohmann::json::object();
        }
        else
            throw(std::runtime_error("Object array " + name +
                                     " is not present in the configuration object with the path:\n" +
                                     GetConfigurationPath()));
    }

    if (!m_my_json[name].is_array()) {
        throw(std::runtime_error("Field " + name + " is not an array in the configuration object with the path:\n" +
                                 GetConfigurationPath()));
    }

    for (size_t i = 0; i < m_my_json[name].size(); ++i) {
        if (!m_my_json[name][i].is_object())
            throw(std::runtime_error("Field #" + std::to_string(i) +
                                     " is not an object in the configuration object with the path:\n" +
                                     GetConfigurationPath()));
    }

    return AddChild(CreateChildConfig(name, shared_from_this(), GetRootConfiguration(), m_my_json[name], true,
                                      m_use_default_values));
}

void Configuration::SetJson(nlohmann::json j)
{
    m_my_json = j;
}
nlohmann::json Configuration::GetJson() const
{
    return m_my_json;
}
nlohmann::json Configuration::GetGlobalJson() const
{
    if (auto p = m_parent.lock())
        return p->GetGlobalJson();
    else
        return m_my_json;
}
void Configuration::SetFilename(const std::string& fn)
{
    auto fp_file_name = FileTools::GetFileParts(fn);

    if (fp_file_name.path.empty()) {
        std::string current_dir = std::filesystem::current_path().string() + "/";
        auto fp = FileTools::GetFileParts(current_dir + fn);
        m_path = fp.path;
    }
    else {
        m_path = fp_file_name.path;
    }
}
void Configuration::ReadJsonFromFile(const std::string& fn)
{
    if (FileTools::GetFileExists(fn)) {
        SetFilename(fn);
        std::ifstream(fn) >> m_my_json;
    }
    else {
        throw(std::runtime_error("Configuration file " + fn + " does not exist."));
    }
}
void Configuration::SaveJson(const std::string& filename, bool overwrite)
{
    if (!overwrite && FileTools::GetFileExists(filename))
        throw(std::invalid_argument("Cannot save to " + filename + " because it exists."));

    std::ofstream(filename) << m_my_json.dump(4);
}
void Configuration::ReadJsonFromString(const std::string& json_text)
{
    m_my_json = nlohmann::json::parse(json_text);
}
}  //namespace mpmca::utilities