/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/logger.hpp"

#include "mpmca/utilities/configuration.hpp"
#include "spdlog/sinks/basic_file_sink.h"
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/spdlog.h"

namespace mpmca::utilities {

class Logger::Impl
{
  protected:
    friend Logger;

    static LoggerType ConvertToLoggerType(const std::string& type)
    {
        if (type == "Console") {
            return LoggerType::kConsole;
        }
        else if (type == "SpdLog") {
            return LoggerType::kSpdLog;
        }
        else {
            throw std::runtime_error("The logger type " + type + " is unknown.");
            return LoggerType::kConsole;
        }
    }

  public:
    Impl() = default;
    Impl(Impl&) = delete;
    Impl(Impl&&) = delete;
    Impl& operator=(Impl&) = delete;
    Impl& operator=(Impl&&) = delete;

    virtual ~Impl() {};

    static std::string Prepend(const std::string& name, const std::string& type_name, const std::string& message)
    {
        return "[" + name + " <" + type_name + ">] : " + message;
    }

    static std::string MakeErrorString(const std::exception& c, const std::string& header)
    {
        return header + "\n" + std::string(c.what());
    }

    virtual void SetOutputLevel(Logger::LoggerOutputLevel output_level) = 0;
    virtual void Warning(const std::string& name, const std::string& type_name, const std::string& message) = 0;
    virtual void Info(const std::string& name, const std::string& type_name, const std::string& message) = 0;
    virtual void Debug(const std::string& name, const std::string& type_name, const std::string& message) = 0;
    virtual void Error(const std::string& name, const std::string& type_name, const std::string& message) = 0;
    virtual void Critical(const std::string& name, const std::string& type_name, const std::string& message) = 0;
    virtual void Trace(const std::string& name, const std::string& type_name, const std::string& message) = 0;
    virtual void Error(const std::string& name, const std::string& type_name, const std::exception& c) = 0;
    virtual void Critical(const std::string& name, const std::string& type_name, const std::exception& c) = 0;
};

class ConsoleLogger : public Logger::Impl
{
    Logger::LoggerOutputLevel m_logger_output_level;

    void SetOutputLevel(Logger::LoggerOutputLevel output_level) override { m_logger_output_level = output_level; }
    void SetOutputLevel(const std::string& output_level)
    {
        if (output_level == "Trace") {
            m_logger_output_level = Logger::LoggerOutputLevel::kTrace;
        }
        else if (output_level == "Debug") {
            m_logger_output_level = Logger::LoggerOutputLevel::kDebug;
        }
        else if (output_level == "Info") {
            m_logger_output_level = Logger::LoggerOutputLevel::kInfo;
        }
        else if (output_level == "Warning") {
            m_logger_output_level = Logger::LoggerOutputLevel::kWarning;
        }
        else if (output_level == "Error") {
            m_logger_output_level = Logger::LoggerOutputLevel::kError;
        }
        else if (output_level == "Critical") {
            m_logger_output_level = Logger::LoggerOutputLevel::kCritical;
        }
        else if (output_level == "None") {
            m_logger_output_level = Logger::LoggerOutputLevel::kNone;
        }
        else {
            throw std::runtime_error(
                "LoggerOutputLevel " + output_level +
                " is an unknown option. Valid options are: Trace, Debug, Info, Warning, Error, Critical, None.");
        }
    }

  public:
    ConsoleLogger() { SetOutputLevel("Trace"); }
    ConsoleLogger(ConfigurationPtr config) { SetOutputLevel(config->GetValue<std::string>("LoggerLevel", "Trace")); }
    ~ConsoleLogger() {}
    void Warning(const std::string& name, const std::string& type_name, const std::string& message) override
    {
        if (m_logger_output_level <= Logger::LoggerOutputLevel::kWarning)
            std::cout << Prepend(name, type_name, message) << std::endl;
    }
    void Info(const std::string& name, const std::string& type_name, const std::string& message) override
    {
        if (m_logger_output_level <= Logger::LoggerOutputLevel::kInfo)
            std::cout << Prepend(name, type_name, message) << std::endl;
    }
    void Debug(const std::string& name, const std::string& type_name, const std::string& message) override
    {
        if (m_logger_output_level <= Logger::LoggerOutputLevel::kDebug)
            std::cout << Prepend(name, type_name, message) << std::endl;
    }
    void Error(const std::string& name, const std::string& type_name, const std::string& message) override
    {
        if (m_logger_output_level <= Logger::LoggerOutputLevel::kError)
            std::cout << Prepend(name, type_name, message) << std::endl;
    }
    void Critical(const std::string& name, const std::string& type_name, const std::string& message) override
    {
        if (m_logger_output_level <= Logger::LoggerOutputLevel::kCritical)
            std::cout << Prepend(name, type_name, message) << std::endl;
    }
    void Trace(const std::string& name, const std::string& type_name, const std::string& message) override
    {
        if (m_logger_output_level <= Logger::LoggerOutputLevel::kTrace)
            std::cout << Prepend(name, type_name, message) << std::endl;
    }
    void Error(const std::string& name, const std::string& type_name, const std::exception& c) override
    {
        if (m_logger_output_level <= Logger::LoggerOutputLevel::kError)
            std::cout << Prepend(name, type_name, c.what()) << std::endl;
    }
    void Critical(const std::string& name, const std::string& type_name, const std::exception& c) override
    {
        if (m_logger_output_level <= Logger::LoggerOutputLevel::kCritical)
            std::cout << Prepend(name, type_name, c.what()) << std::endl;
    }
};

class SpdLogger : public Logger::Impl
{
  private:
    static std::shared_ptr<spdlog::sinks::stdout_color_sink_mt> s_global_console_sink;
    static std::shared_ptr<spdlog::sinks::basic_file_sink_mt> s_global_file_sink;
    static spdlog::logger s_global_logger;

    static spdlog::level::level_enum ConvertStringToSpdLogLevel(const std::string& string)
    {
        if (string == "Trace") {
            return spdlog::level::trace;
        }
        else if (string == "Debug") {
            return spdlog::level::debug;
        }
        else if (string == "Info") {
            return spdlog::level::info;
        }
        else if (string == "Warning") {
            return spdlog::level::warn;
        }
        else if (string == "Error") {
            return spdlog::level::err;
        }
        else if (string == "Critical") {
            return spdlog::level::critical;
        }
        else if (string == "None") {
            return spdlog::level::off;
        }
        else {
            throw std::runtime_error(
                "LoggerOutputLevel " + string +
                " is an unknown option. Valid options are: Trace, Debug, Info, Warning, Error, Critical, None.");
        }
    }

    static std::string ConvertSpdLogLevelToString(const spdlog::level::level_enum& level)
    {
        switch (level) {
            case spdlog::level::level_enum::trace:
                return "Trace";
                break;
            case spdlog::level::level_enum::debug:
                return "Debug";
                break;
            case spdlog::level::level_enum::info:
                return "Info";
                break;
            case spdlog::level::level_enum::warn:
                return "Warning";
                break;
            case spdlog::level::level_enum::err:
                return "Error";
                break;
            case spdlog::level::level_enum::critical:
                return "Critical";
                break;
            case spdlog::level::level_enum::off:
                return "None";
                break;
            default:
                return "Unknown level";
                break;
        }
        return "Unknown level";
    }

  public:
    void SetOutputLevel(Logger::LoggerOutputLevel output_level)
    {
        switch (output_level) {
            case Logger::LoggerOutputLevel::kCritical:
                s_global_logger.set_level(spdlog::level::level_enum::critical);
                break;
            case Logger::LoggerOutputLevel::kDebug:
                s_global_logger.set_level(spdlog::level::level_enum::debug);
                break;
            case Logger::LoggerOutputLevel::kError:
                s_global_logger.set_level(spdlog::level::level_enum::err);
                break;
            case Logger::LoggerOutputLevel::kInfo:
                s_global_logger.set_level(spdlog::level::level_enum::info);
                break;
            case Logger::LoggerOutputLevel::kNone:
                s_global_logger.set_level(spdlog::level::level_enum::trace);
                break;
            case Logger::LoggerOutputLevel::kTrace:
                s_global_logger.set_level(spdlog::level::level_enum::trace);
                break;
            case Logger::LoggerOutputLevel::kWarning:
                s_global_logger.set_level(spdlog::level::level_enum::warn);
                break;
        }
    }

    SpdLogger(ConfigurationPtr config)
    {
        if (s_global_logger.sinks().size() == 1) {
            s_global_file_sink = std::make_shared<spdlog::sinks::basic_file_sink_mt>(
                config->GetValue<std::string>("Filename", "mpmca.log"),  //
                config->GetValue<bool>("Truncate", false));
            s_global_file_sink->set_level(
                ConvertStringToSpdLogLevel(config->GetValue<std::string>("FileSinkLevel", "trace")));

            s_global_logger.sinks().push_back(s_global_file_sink);
            s_global_logger.set_level(
                ConvertStringToSpdLogLevel(config->GetValue<std::string>("LoggerLevel", "trace")));

            s_global_console_sink->set_level(
                ConvertStringToSpdLogLevel(config->GetValue<std::string>("ConsoleSinkLevel", "trace")));

            std::string truncate_message;
            if (config->GetValue<bool>("Truncate", false))
                truncate_message = "This file was truncated.";
            else
                truncate_message = "This file was not truncated.";

            s_global_logger.info("Initialized global loggers.");
            s_global_logger.info("The global logger will log messages from level \"" +
                                 ConvertSpdLogLevelToString(s_global_logger.level()) + "\" and more severe. ");

            s_global_logger.info("The global file sink will log to " + s_global_file_sink->filename() +
                                 ", logging messages from level \"" +
                                 ConvertSpdLogLevelToString(s_global_file_sink->level()) + "\" and more severe. " +
                                 truncate_message);

            s_global_logger.info("The global console sink will log messages from level \"" +
                                 ConvertSpdLogLevelToString(s_global_console_sink->level()) + "\" and more severe.");

            if (s_global_file_sink->level() < s_global_logger.level())
                s_global_logger.critical(
                    "The global file logger sink has a lower severity level than the global logger. "
                    "Certain "
                    "messages "
                    "might be hidden unexpectedly.");

            if (s_global_console_sink->level() < s_global_logger.level())
                s_global_logger.critical(
                    "The global console logger sink has a lower severity level than the global logger. "
                    "Certain "
                    "messages might be hidden unexpectedly.");
        }
        else
            s_global_logger.warn("Global logger already initialized.");
    }
    void Warning(const std::string& name, const std::string& type_name, const std::string& message) override
    {
        s_global_logger.warn(Prepend(name, type_name, message));
    }
    void Info(const std::string& name, const std::string& type_name, const std::string& message) override
    {
        s_global_logger.info(Prepend(name, type_name, message));
    }
    void Debug(const std::string& name, const std::string& type_name, const std::string& message) override
    {
        s_global_logger.debug(Prepend(name, type_name, message));
    }
    void Error(const std::string& name, const std::string& type_name, const std::string& message) override
    {
        s_global_logger.error(Prepend(name, type_name, message));
    }
    void Error(const std::string& name, const std::string& type_name, const std::exception& c) override
    {
        Error(name, type_name, c.what());
    }
    void Critical(const std::string& name, const std::string& type_name, const std::exception& c) override
    {
        Critical(name, type_name, c.what());
    }
    void Critical(const std::string& name, const std::string& type_name, const std::string& message) override
    {
        s_global_logger.critical(Prepend(name, type_name, message));
    }
    void Trace(const std::string& name, const std::string& type_name, const std::string& message) override
    {
        s_global_logger.trace(Prepend(name, type_name, message));
    }
};

std::shared_ptr<spdlog::sinks::stdout_color_sink_mt> SpdLogger::s_global_console_sink =
    std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
std::shared_ptr<spdlog::sinks::basic_file_sink_mt> SpdLogger::s_global_file_sink = nullptr;
spdlog::logger SpdLogger::s_global_logger("mpmca", s_global_console_sink);

std::unique_ptr<Logger::Impl> Logger::s_pointer_to_impl = nullptr;

/**
 * @brief Construct a new Logger:: Logger object.
 *
 * @param name Name of the logger.
 * @param type_name Type name of the logger.
 */
Logger::Logger(const std::string& name, const std::string& type_name)
    : m_name(name)
    , m_type_name(type_name)
{
    if (!s_pointer_to_impl)
        s_pointer_to_impl = std::make_unique<ConsoleLogger>();
}

Logger::Logger(ConfigurationPtr config, const std::string& name, const std::string& type_name)
    : Logger(name, type_name)
{
    Logger::LoggerType type = Logger::Impl::ConvertToLoggerType(config->GetValue<std::string>("LoggerType", "Console"));

    switch (type) {
        case Logger::LoggerType::kConsole:
            s_pointer_to_impl = std::make_unique<ConsoleLogger>(config);
            break;
        case Logger::LoggerType::kSpdLog:
            s_pointer_to_impl = std::make_unique<SpdLogger>(config);
            break;
    }
}
void Logger::Warning(const std::string& message) const
{
    s_pointer_to_impl->Warning(m_name, m_type_name, message);
}
void Logger::Debug(const std::string& message) const
{
    s_pointer_to_impl->Debug(m_name, m_type_name, message);
}
void Logger::Error(const std::string& message) const
{
    s_pointer_to_impl->Error(m_name, m_type_name, message);
}
void Logger::Error(const std::exception& c) const
{
    s_pointer_to_impl->Error(m_name, m_type_name, c);
}
void Logger::Critical(const std::exception& c) const
{
    s_pointer_to_impl->Critical(m_name, m_type_name, c);
}
void Logger::Critical(const std::string& message) const
{
    s_pointer_to_impl->Critical(m_name, m_type_name, message);
}
/**
 * @brief Log message of level 'trace'.
 *
 * @param message The message to log.
 */
void Logger::Trace(const std::string& message) const
{
    s_pointer_to_impl->Trace(m_name, m_type_name, message);
}
void Logger::Info(const std::string& message) const
{
    s_pointer_to_impl->Info(m_name, m_type_name, message);
}
void Logger::SetLoggerOutputLevel(Logger::LoggerOutputLevel output_level)
{
    s_pointer_to_impl->SetOutputLevel(output_level);
}
void Logger::SetName(const std::string& name)
{
    m_name = name;
}
void Logger::SetTypeName(const std::string& type_name)
{
    m_type_name = type_name;
}
const std::string& Logger::GetName() const
{
    return m_name;
}
const std::string& Logger::GetTypeName() const
{
    return m_type_name;
}

}  //namespace mpmca::utilities