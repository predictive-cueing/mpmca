/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/clock.hpp"

namespace mpmca::utilities {
Clock::Clock(const std::chrono::milliseconds& dt)
    : m_dt_ms(dt.count())
    , m_dt((double)(dt.count()) / 1000.0)
    , m_current_time_ms(0)
{
}
double Clock::GetCurrentTime() const
{
    return (double)m_current_time_ms / 1000.0;
}
int64_t Clock::GetCurrentTimeMs() const
{
    return m_current_time_ms;
}
void Clock::Tick()
{
    TickClock();
}
void Clock::TickClock()
{
    m_current_time_ms += m_dt_ms;
}
int Clock::GetDtMs() const
{
    return m_dt_ms;
}
double Clock::GetDt() const
{
    return m_dt;
}
}  //namespace mpmca