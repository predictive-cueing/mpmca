/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/math.hpp"

#include <algorithm>

#include "mpmca/constants.hpp"
#include "mpmca/types/pose_rot_mat_vector.hpp"
#include "mpmca/types/pose_vector.hpp"
#include "mpmca/types/transformation_matrix.hpp"

namespace mpmca::utilities {
// This sign function is not the same as the signum function below,
// it will return 1 if val is equal to 0. Some code apparently works
// with this function and we do not want to break it.
double Math::Sign(double val)
{
    return (val >= 0) - (val < 0);
}

double Math::GetCosineFade(double v1, double v2, double s_fade, double s_takeover)
{
    if (s_takeover < s_fade && s_fade > 0) {
        double sin_val = std::sin(constants::kPi_2 * std::clamp(s_takeover / s_fade, 0.0, 1.0));
        return v1 + (v2 - v1) * sin_val * sin_val;
    }
    else {
        return v2;
    }
}
Vector<4> Math::EulerToQuaternion(const Vector<3> euler_angles)
{
    double cos_roll = std::cos(euler_angles[0] * 0.5);
    double sin_roll = std::sin(euler_angles[0] * 0.5);
    double cos_pitch = std::cos(euler_angles[1] * 0.5);
    double sin_pitch = std::sin(euler_angles[1] * 0.5);
    double cos_yaw = std::cos(euler_angles[2] * 0.5);
    double sin_yaw = std::sin(euler_angles[2] * 0.5);

    Vector<4> quaternion;
    quaternion[0] = cos_roll * cos_pitch * cos_yaw + sin_roll * sin_pitch * sin_yaw;
    quaternion[1] = sin_roll * cos_pitch * cos_yaw - cos_roll * sin_pitch * sin_yaw;
    quaternion[2] = cos_roll * sin_pitch * cos_yaw + sin_roll * cos_pitch * sin_yaw;
    quaternion[3] = cos_roll * cos_pitch * sin_yaw - sin_roll * sin_pitch * cos_yaw;

    return quaternion;
}

Vector<4> Math::RotationMatrixToQuaternion(const Matrix<4, 4>& m_in)
{
    double t = 1;
    Vector<4> q = Vector<4>::Zero();
    auto m = m_in.transpose();
    // auto m = m_in;
    if (m(2, 2) < 0) {
        if (m(0, 0) > m(1, 1)) {
            t = 1 + m(0, 0) - m(1, 1) - m(2, 2);
            q = Vector<4>{t, m(0, 1) + m(1, 0), m(2, 0) + m(0, 2), m(1, 2) - m(2, 1)};
        }
        else {
            t = 1 - m(0, 0) + m(1, 1) - m(2, 2);
            q = Vector<4>{m(0, 1) + m(1, 0), t, m(1, 2) + m(2, 1), m(2, 0) - m(0, 2)};
        }
    }
    else {
        if (m(0, 0) < -m(1, 1)) {
            t = 1 - m(0, 0) - m(1, 1) + m(2, 2);
            q = Vector<4>{m(2, 0) + m(0, 2), m(1, 2) + m(2, 1), t, m(0, 1) - m(1, 0)};
        }
        else {
            t = 1 + m(0, 0) + m(1, 1) + m(2, 2);
            q = Vector<4>{m(1, 2) - m(2, 1), m(2, 0) - m(0, 2), m(0, 1) - m(1, 0), t};
        }
    }
    q *= 0.5 / std::sqrt(t);

    return {q[3], q[0], q[1], q[2]};
}

Vector<4> Math::EulerToQuaternion(double roll, double pitch, double yaw)
{
    return EulerToQuaternion({roll, pitch, yaw});
}

std::array<double, 3> Math::ConvertIso1151QuaternionToIso8855Euler(const std::array<double, 4>& quaternion)
{
    //https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles

    double w = quaternion[0];
    double x = quaternion[1];
    double y = quaternion[2];
    double z = quaternion[3];

    // roll
    double sin_r_cos_p = +2.0 * (w * x + y * z);
    double cos_r_cos_p = +1.0 - 2.0 * (x * x + y * y);
    double phi = std::atan2(sin_r_cos_p, cos_r_cos_p);

    // pitch
    double sin_p = +2.0 * (w * y - z * x);
    double theta;
    if (std::abs(sin_p) >= 1)
        theta = std::copysign(constants::kPi_2, sin_p);  // use 90 degrees if out of range
    else
        theta = std::asin(sin_p);

    // third rotation, around y, pitch
    double sin_y_cos_p = +2.0 * (w * z + x * y);
    double cos_y_cos_p = +1.0 - 2.0 * (y * y + z * z);
    double psi = std::atan2(sin_y_cos_p, cos_y_cos_p);

    // the quaternions were given in iso1151, so y-left and z-up, we need them in iso8855, so y-right, z-down
    std::array<double, 3> euler;
    euler[0] = phi;
    euler[1] = -theta;
    euler[2] = -psi;
    return euler;
}
double Math::GetCosineFadeOptimized(double v1, double v2, double sin_val2)
{
    return v1 + (v2 - v1) * sin_val2;
}

double Math::GetCosineFadeVal2(double s_fade, double s_takeover)
{
    if (s_takeover < s_fade && s_fade > 0) {
        double sin_val = std::sin(constants::kPi_2 * std::clamp(s_takeover / s_fade, 0.0, 1.0));
        return sin_val * sin_val;
    }
    else {
        return 1.0;
    }
}
unsigned Math::PositiveModulo(int value, unsigned m)
{
    int mod = value % (int)m;
    if (mod < 0) {
        mod += m;
    }
    return mod;
}
double Math::PositiveModuloDouble(double x, double y)
{
    return x - (int)(floor(x / y)) * y;
}
double Math::GetAngleDiff(double angle1, double angle2)
{
    double a = angle1 - angle2;
    a = std::fmod(a, 2 * constants::kPi);

    if (a > constants::kPi)
        a = a - 2 * constants::kPi;

    if (a < -constants::kPi)
        a = a + 2 * constants::kPi;

    return a;
}

double Math::ConvertDegreesToRadians(double angle_degrees)
{
    return angle_degrees * constants::kPi / 180.0;
}

double Math::ConvertRadiansToDegrees(double angle_radians)
{
    return angle_radians * 180.0 / constants::kPi;
}

double Math::Angle2Pi(double angle)
{
    return std::fmod(angle + constants::kPi, (2.0 * constants::kPi)) - constants::kPi;
}

Vector<3> Math::EulerRatesToAngularVelocities(const Vector<3>& body_euler_rates, const Vector<3>& world_rotation)
{
    Vector<3> angular_velocities;

    double sin_phi = std::sin(world_rotation[0]);
    double cos_phi = std::cos(world_rotation[0]);
    double sin_theta = std::sin(world_rotation[1]);
    double cos_theta = std::cos(world_rotation[1]);

    angular_velocities[0] = body_euler_rates[0] - sin_theta * body_euler_rates[2];
    angular_velocities[1] = cos_phi * body_euler_rates[1] + cos_theta * sin_phi * body_euler_rates[2];
    angular_velocities[2] = -sin_phi * body_euler_rates[1] + cos_theta * cos_phi * body_euler_rates[2];

    return angular_velocities;
}

}  //namespace mpmca::utilities