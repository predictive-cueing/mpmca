/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/signal_generator_constant.hpp"

namespace mpmca::utilities {
SignalGeneratorConstant::SignalGeneratorConstant(ConfigurationPtr config)
    : SignalGeneratorConstant(config->GetValue<double>("Constant", 1.0))
{
}

SignalGeneratorConstant::SignalGeneratorConstant(double constant)
    : m_constant(constant)
{
}

double SignalGeneratorConstant::CalculateValueAtTimeMs(long)
{
    return m_constant;
}
}  //namespace mpmca::utilities