/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/step_count.hpp"

#include <algorithm>
#include <exception>

namespace mpmca::utilities {

StepCount::StepCount(int step_ms, int count)
{
    m_step_ms = std::vector<int>{step_ms};
    m_count = std::vector<int>{count};
}
void StepCount::Add(int step, int count)
{
    m_step_ms.push_back(step);
    m_count.push_back(count);
}
int StepCount::GetNumStepCounts() const
{
    return m_step_ms.size();
}
int StepCount::GetCount(size_t index) const
{
    return m_count.at(index);
}
int StepCount::GetStepMs(size_t index) const
{
    return m_step_ms.at(index);
}
int StepCount::GetStep(size_t index) const
{
    return m_step_ms.at(index) / GetMinStepMs();
}
int StepCount::GetMinStepMs() const
{
    auto min_element = std::min_element(m_step_ms.cbegin(), m_step_ms.cend());
    if (std::end(m_step_ms) != min_element) {
        return *min_element;
    }
    else {
        throw std::runtime_error("GetMinStepMs called on empty StepCount object.");
        return 1;
    }
}
}  //namespace mpmca::utilities