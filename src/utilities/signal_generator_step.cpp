/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/signal_generator_step.hpp"

#include <cmath>
namespace mpmca::utilities {
SignalGeneratorStep::SignalGeneratorStep(ConfigurationPtr config)
    : SignalGeneratorStep(config->GetValue<double>("Amplitude", 1.0), config->GetValue<double>("Constant", 1.0),
                          config->GetValue<double>("StartIntervalMs", 1.0), config->GetValue<double>("DurationMs", 1.0))
{
}

SignalGeneratorStep::SignalGeneratorStep(double amplitude, double constant, double start_interval_ms,
                                         double duration_ms)
    : m_amplitude(amplitude)
    , m_constant(constant)
    , m_start_interval_ms(start_interval_ms)
    , m_duration_ms(duration_ms)
{
}

double SignalGeneratorStep::CalculateValueAtTimeMs(long t_ms)
{
    double period = std::fmod(t_ms + 2 * m_start_interval_ms, m_start_interval_ms);
    return m_constant + m_amplitude * (period < m_duration_ms);
}
}  //namespace mpmca::utilities