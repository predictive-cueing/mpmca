/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/command_line_options_parser.hpp"

namespace mpmca::utilities {

CommandLineOptionsParser::CommandLineOptionsParser(int& argc, char** argv)
{
    for (int i = 1; i < argc; ++i) {
        std::string argv_string = std::string(argv[i]);
        std::vector<std::string> argv_parts = StringUtilities::SplitString(argv_string, '=');

        for (auto& argv_part : argv_parts)
            m_tokens.push_back(argv_part);
    }
}

size_t CommandLineOptionsParser::GetNumTokens() const
{
    return m_tokens.size();
}

const std::vector<std::string>& CommandLineOptionsParser::GetTokens() const
{
    return m_tokens;
}

const std::string& CommandLineOptionsParser::GetCommandOption(const std::string& option) const
{
    std::vector<std::string>::const_iterator itr;
    itr = std::find(m_tokens.begin(), m_tokens.end(), option);
    if (itr != m_tokens.end() && ++itr != m_tokens.end()) {
        return *itr;
    }
    static const std::string m_empty_string("");
    return m_empty_string;
}

bool CommandLineOptionsParser::GetCommandOptionExists(const std::string& option) const
{
    return std::find(m_tokens.begin(), m_tokens.end(), option) != m_tokens.end();
}
}  //namespace mpmca::utilities