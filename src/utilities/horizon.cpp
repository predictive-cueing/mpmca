/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/horizon.hpp"

#include "mpmca/utilities/clock.hpp"
#include "mpmca/utilities/configuration.hpp"
#include "mpmca/utilities/step_count.hpp"

namespace mpmca::utilities {
Horizon::Horizon(ConfigurationPtr config)
    : Horizon(Horizon::JsonToStepCount(config->GetConfigurationObject("Horizon")->GetConfigurationObjectArray("Steps")),
              config->GetConfigurationObject("Horizon")->GetValue<int>("PredictionMaximumTimeStepMs", 1))
{
}
StepCount Horizon::JsonToStepCount(ConfigurationPtr config)
{
    StepCount step_count;

    for (unsigned j = 0; j < config->GetNumObjectArrayElements(); ++j) {
        auto config_item = config->GetConfigurationObjectArrayElement(j);

        if (config_item) {
            step_count.Add(config_item->GetValue<int>("StepMs", 1), config_item->GetValue<int>("Count", 1));
        }
        else
            throw(std::runtime_error("Object inside \"Steps\" is not an array element."));
    }
    return step_count;
}
Horizon::Horizon(int dt_ms, int n_steps)
    : Horizon(StepCount(dt_ms, n_steps), dt_ms)
{
}
Horizon::Horizon(const Clock& clock, int n_steps)
    : Horizon(StepCount(clock.GetDtMs(), n_steps), clock.GetDtMs())
{
}
Horizon::Horizon(StepCount steps_count, int prediction_max_step_ms)
try :  //
    m_dt((double)steps_count.GetMinStepMs() / 1000.0),  //
    m_dt_ms(steps_count.GetMinStepMs()),  //
    m_prediction_horizon_maximum_time_step(prediction_max_step_ms / steps_count.GetMinStepMs())  //
{
    if (m_dt_ms <= 0)
        throw(std::runtime_error("Horizon: dt should be larger than 0."));

    if (m_prediction_horizon_maximum_time_step <= 0)
        throw(std::runtime_error("Horizon: PredictionHorizonMaximumTimeStep should be larger than 0."));

    if (prediction_max_step_ms % m_dt_ms != 0) {
        throw(
            std::runtime_error("Horizon: PredictionHorizonMaximumTimeStepMs should be an integer multiple of the "
                               "smallest ControllerHorizon time step."));
    }

    for (int j = 0; j < steps_count.GetNumStepCounts(); ++j) {
        for (int i = 0; i < steps_count.GetCount(j); ++i) {
            m_controller_horizon_delta_time_steps.push_back(steps_count.GetStep(j));

            int min_prediction_chunks =
                (int)ceil((double)steps_count.GetStep(j) / (double)m_prediction_horizon_maximum_time_step);
            int evenly_prediction_steps = (int)ceil((double)steps_count.GetStep(j) / (double)min_prediction_chunks);
            int steps_taken = 0;
            int prediction_chunks = 0;

            while (steps_taken < steps_count.GetStep(j)) {
                const int m_steps_remaining = steps_count.GetStep(j) - steps_taken;
                evenly_prediction_steps =
                    (int)ceil((double)m_steps_remaining / (double)(min_prediction_chunks - prediction_chunks));

                m_prediction_horizon_delta_time_steps.push_back(std::min(m_steps_remaining, evenly_prediction_steps));
                steps_taken += m_prediction_horizon_delta_time_steps.back();
                ++prediction_chunks;
            }
        }
    }

    // add one extra delta time, because some Models rely on having a delta time on every step of the simulation,
    // including the last.
    m_prediction_horizon_delta_time_steps.push_back(m_prediction_horizon_maximum_time_step);

    m_n_mpc = m_controller_horizon_delta_time_steps.size();
    m_n_mpc_pred = m_controller_horizon_delta_time_steps.size() + 1;
    m_n_pred = m_prediction_horizon_delta_time_steps.size();

    SetPredictionHorizon();
    SetControllerHorizon();
}
catch (const std::exception& c) {
    throw(std::runtime_error("Error while initializing Horizon.\nMessage: " + std::string(c.what())));
}
void Horizon::SetPredictionHorizon()
{
    int prediction_time_steps_integrated = 0;
    for (size_t i = 0; i < m_prediction_horizon_delta_time_steps.size(); ++i) {
        m_prediction_horizon_time.push_back(prediction_time_steps_integrated * m_dt);
        m_prediction_horizon_time_steps.push_back(prediction_time_steps_integrated);
        m_prediction_horizon_time_ms.push_back(std::round(prediction_time_steps_integrated * m_dt * 1000.0));
        m_time_ms_prediction_horizon_index[m_prediction_horizon_time_ms.back()] = i;

        m_prediction_horizon_delta_time.push_back(m_prediction_horizon_delta_time_steps.at(i) * m_dt);
        m_prediction_horizon_delta_time_ms.push_back(
            std::round(m_prediction_horizon_delta_time_steps.at(i) * m_dt * 1000.0));
        prediction_time_steps_integrated += m_prediction_horizon_delta_time_steps.at(i);
    }
}
void Horizon::SetControllerHorizon()
{
    int controller_time_steps_integrated = 0;
    int n_fundamental = 0;

    for (size_t i = 0; i < m_controller_horizon_delta_time_steps.size() + 1; ++i) {
        m_controller_horizon_time.push_back(controller_time_steps_integrated * m_dt);
        m_controller_horizon_time_steps.push_back(controller_time_steps_integrated);
        m_controller_horizon_time_ms.push_back(std::round(controller_time_steps_integrated * m_dt * 1000.0));
        m_time_ms_horizon_index[m_controller_horizon_time_ms.back()] = i;

        // the delta vectors are length N_mpc, the time vectors are length N_pred
        if (i < m_controller_horizon_delta_time_steps.size()) {
            if (m_controller_horizon_delta_time_steps.at(i) == 1)
                ++n_fundamental;

            m_controller_horizon_delta_time.push_back(m_controller_horizon_delta_time_steps.at(i) * m_dt);
            m_controller_horizon_delta_time_ms.push_back(
                (int)(m_controller_horizon_delta_time_steps.at(i) * m_dt * 1000.0));
            controller_time_steps_integrated += m_controller_horizon_delta_time_steps.at(i);

            int pred_index = 0;

            while (pred_index < m_prediction_horizon_time_steps.size() &&
                   m_prediction_horizon_time_steps.at(pred_index) != m_controller_horizon_time_steps.back())
                ++pred_index;

            m_prediction_horizon_index.push_back(pred_index);
        }
    }

    m_n_mpc_fundamental = n_fundamental;
}
void Horizon::PrintHorizon() const
{
    size_t controller_index = 0;
    size_t prediction_index = 0;
    size_t full_horizon_time_steps = m_controller_horizon_time_steps.back();

    for (size_t i = 0; i < full_horizon_time_steps; ++i) {
        std::cout << i << "\t";

        if (m_prediction_horizon_time_steps.at(prediction_index) == i) {
            std::cout << "(" << prediction_index << ")\t" << m_prediction_horizon_time_steps.at(prediction_index)
                      << "\t" << m_prediction_horizon_time.at(prediction_index) << "\t";

            if (prediction_index < m_prediction_horizon_delta_time.size())
                std::cout << m_prediction_horizon_delta_time.at(prediction_index) << "\t"
                          << m_prediction_horizon_delta_time_steps.at(prediction_index);
            else
                std::cout << "\t";

            std::cout << "\t";
            ++prediction_index;
        }
        else
            std::cout << "\t\t\t";

        if (m_controller_horizon_time_steps.at(controller_index) == i) {
            std::cout << "(" << controller_index << ")\t" << m_controller_horizon_time_steps.at(controller_index)
                      << "\t" << m_controller_horizon_time.at(controller_index) << "\t";

            if (controller_index < m_controller_horizon_delta_time.size())
                std::cout << m_controller_horizon_delta_time.at(controller_index) << "\t"
                          << m_controller_horizon_delta_time_steps.at(controller_index);
            else
                std::cout << "\t";

            std::cout << "\t[" << i << ", " << m_prediction_horizon_index.at(controller_index) << "]";

            ++controller_index;
        }
        std::cout << std::endl;
    }
}
int Horizon::GetTimeMsToPredictionHorizonIndex(int time) const
{
    if (m_time_ms_prediction_horizon_index.find(time) == m_time_ms_prediction_horizon_index.end())
        throw(std::invalid_argument("Time " + std::to_string(time) +
                                    " does not correspond with a time on the prediction horizon."));

    return m_time_ms_prediction_horizon_index.at(time);
}
int Horizon::GetTimeMsToHorizonIndex(int time) const
{
    if (m_time_ms_horizon_index.find(time) == m_time_ms_horizon_index.end())
        throw(std::invalid_argument("Time " + std::to_string(time) +
                                    " does not correspond with a time on the prediction horizon."));

    return m_time_ms_horizon_index.at(time);
}
std::vector<int> Horizon::GetPredictionHorizonIndex() const
{
    return m_prediction_horizon_index;
}
int Horizon::GetPredictionHorizonIndex(int mpc_horizon_index) const
{
    return m_prediction_horizon_index.at(mpc_horizon_index);
}
int Horizon::GetNMpcFundamental() const
{
    return m_n_mpc_fundamental;
}
int Horizon::GetNMpc() const
{
    return m_n_mpc;
}
int Horizon::GetNMpcPred() const
{
    return m_n_mpc_pred;
}
int Horizon::GetNPrediction() const
{
    return m_n_pred;
}
double Horizon::GetDt() const
{
    return m_dt;
}
int Horizon::GetDtMs() const
{
    return m_dt_ms;
}
std::vector<double> Horizon::GetHorizonTime() const
{
    return m_controller_horizon_time;
}
int Horizon::GetFinalControllerHorizonTimeMs() const
{
    return m_controller_horizon_time_ms.back();
}
int Horizon::GetFinalPredictionHorizonTimeMs() const
{
    return m_prediction_horizon_time_ms.back();
}
std::vector<double> Horizon::GetPredictionHorizonTime() const
{
    return m_prediction_horizon_time;
}
std::vector<double> Horizon::GetHorizonDeltaTime() const
{
    return m_controller_horizon_delta_time;
}
std::vector<int> Horizon::GetHorizonTimeSteps() const
{
    return m_controller_horizon_time_steps;
}
std::vector<int> Horizon::GetHorizonDeltaTimeSteps() const
{
    return m_controller_horizon_delta_time_steps;
}
std::vector<unsigned> Horizon::GetHorizonDeltaTimeStepsUnsigned() const
{
    std::vector<unsigned> st;
    for (int i = 0; i < (int)m_controller_horizon_delta_time_steps.size(); ++i)
        st.push_back((unsigned)m_controller_horizon_delta_time_steps.at(i));

    return st;
}
std::vector<int> Horizon::GetHorizonTimeMs() const
{
    return m_controller_horizon_time_ms;
}
std::vector<int> Horizon::GetPredictionHorizonTimeMs() const
{
    return m_prediction_horizon_time_ms;
}
std::vector<int> Horizon::GetHorizonDeltaTimeMs() const
{
    return m_controller_horizon_delta_time_ms;
}
int Horizon::GetHorizonDeltaTimeMs(int i) const
{
    return m_controller_horizon_delta_time_ms.at(i);
}
std::vector<int> Horizon::GetPredictionHorizonDeltaTimeMs() const
{
    return m_prediction_horizon_delta_time_ms;
}
int Horizon::GetPredictionHorizonDeltaTimeMs(int i) const
{
    return m_prediction_horizon_delta_time_ms.at(i);
}
double Horizon::GetHorizonTime(int i) const
{
    return m_controller_horizon_time.at(i);
}
double Horizon::GetPredictionHorizonTime(int i) const
{
    return m_prediction_horizon_time.at(i);
}
int Horizon::GetHorizonTimeMs(int i) const
{
    return m_controller_horizon_time_ms.at(i);
}
int Horizon::GetPredictionHorizonTimeMs(int i) const
{
    return m_prediction_horizon_time_ms.at(i);
}
double Horizon::GetHorizonDeltaTime(int i) const
{
    return m_controller_horizon_delta_time.at(i);
}
double Horizon::GetPredictionHorizonDeltaTime(int i) const
{
    return m_prediction_horizon_delta_time.at(i);
}
std::vector<double> Horizon::GetPredictionHorizonDeltaTime() const
{
    return m_prediction_horizon_delta_time;
}
std::vector<int> Horizon::GetPredictionHorizonDeltaTimeSteps() const
{
    return m_prediction_horizon_delta_time_steps;
}
int Horizon::GetPredictionHorizonDeltaTimeSteps(int j) const
{
    return m_prediction_horizon_delta_time_steps.at(j);
}
int Horizon::GetHorizonTimeSteps(int i) const
{
    return m_controller_horizon_time_steps.at(i);
}
int Horizon::GetHorizonDeltaTimeSteps(int i) const
{
    return m_controller_horizon_delta_time_steps.at(i);
}
}  //namespace mpmca