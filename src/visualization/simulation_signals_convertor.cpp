/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/visualization/simulation_signals_convertor.hpp"

#include "mpmca/messages/automotive_signals.hpp"
#include "mpmca/messages/inertial_head_signals.hpp"
#include "mpmca/messages/vehicle_state_signals.hpp"
#include "simulation_signals.pb.h"
#define MCAP_IMPLEMENTATION
#include "mpmca/utilities/mpmca_mcap.hpp"

namespace mpmca::visualization {

SimulationSignalsConvertor::SimulationSignalsConvertor(mcap::McapWriter& writer)
    : m_channel_id(AddSchema<proto::SimulationSignals>(writer, "/simulation"))
{
}
void SimulationSignalsConvertor::ProcessDataBucket(mcap::McapWriter& writer, const pipeline::DataBucket& data_bucket)
{
    proto::SimulationSignals* sample = new proto::SimulationSignals();

    data_bucket.GetTaskSignalBus().ReadMessageIfUpdated<messages::InertialHeadSignals>(
        [&](const messages::InertialHeadSignals& message) {
            auto* proto_ptr = sample->mutable_inertial_head_signals();

            proto_ptr->set_head_f_x_head_m_s2(message.head_f_x_head_m_s2);
            proto_ptr->set_head_f_y_head_m_s2(message.head_f_y_head_m_s2);
            proto_ptr->set_head_f_z_head_m_s2(message.head_f_z_head_m_s2);
            proto_ptr->set_head_omega_x_head_rad_s(message.head_omega_x_head_rad_s);
            proto_ptr->set_head_omega_y_head_rad_s(message.head_omega_y_head_rad_s);
            proto_ptr->set_head_omega_z_head_rad_s(message.head_omega_z_head_rad_s);
            proto_ptr->set_head_alpha_x_head_rad_s2(message.head_alpha_x_head_rad_s2);
            proto_ptr->set_head_alpha_y_head_rad_s2(message.head_alpha_y_head_rad_s2);
            proto_ptr->set_head_alpha_z_head_rad_s2(message.head_alpha_z_head_rad_s2);
        });

    data_bucket.GetTaskSignalBus().ReadMessageIfUpdated<messages::VehicleStateSignals>(
        [&](const messages::VehicleStateSignals& message) {
            auto* proto_ptr = sample->mutable_vehicle_state_signals();
            proto_ptr->set_world_x_vehicle_m(message.world_X_vehicle_m);
            proto_ptr->set_world_y_vehicle_m(message.world_Y_vehicle_m);
            proto_ptr->set_world_z_vehicle_m(message.world_Z_vehicle_m);
            proto_ptr->set_world_phi_vehicle_rad(message.world_phi_vehicle_rad);
            proto_ptr->set_world_theta_vehicle_rad(message.world_theta_vehicle_rad);
            proto_ptr->set_world_psi_vehicle_rad(message.world_psi_vehicle_rad);
            proto_ptr->set_body_u_vehicle_m_s(message.body_u_vehicle_m_s);
            proto_ptr->set_body_v_vehicle_m_s(message.body_v_vehicle_m_s);
            proto_ptr->set_body_w_vehicle_m_s(message.body_w_vehicle_m_s);
            proto_ptr->set_body_p_vehicle_rad_s(message.body_p_vehicle_rad_s);
            proto_ptr->set_body_q_vehicle_rad_s(message.body_q_vehicle_rad_s);
            proto_ptr->set_body_r_vehicle_rad_s(message.body_r_vehicle_rad_s);
            proto_ptr->set_body_a_x_vehicle_m_s2(message.body_a_x_vehicle_m_s2);
            proto_ptr->set_body_a_y_vehicle_m_s2(message.body_a_y_vehicle_m_s2);
            proto_ptr->set_body_a_z_vehicle_m_s2(message.body_a_z_vehicle_m_s2);
            proto_ptr->set_body_p_dot_vehicle_rad_s2(message.body_p_dot_vehicle_rad_s2);
            proto_ptr->set_body_q_dot_vehicle_rad_s2(message.body_q_dot_vehicle_rad_s2);
            proto_ptr->set_body_r_dot_vehicle_rad_s2(message.body_r_dot_vehicle_rad_s2);
        });

    data_bucket.GetTaskSignalBus().ReadMessageIfUpdated<messages::AutomotiveSignals>(
        [&](const messages::AutomotiveSignals& message) {
            auto* proto_ptr = sample->mutable_automotive_signals();
            proto_ptr->set_car_throttle_fraction(message.car_throttle_fraction);
            proto_ptr->set_car_brake_fraction(message.car_brake_fraction);
            proto_ptr->set_car_throttle_dot_fraction_s(message.car_throttle_dot_fraction_s);
            proto_ptr->set_car_brake_dot_fraction_s(message.car_brake_dot_fraction_s);
            proto_ptr->set_car_clutch_fraction(message.car_clutch_fraction);
            proto_ptr->set_car_engine_rotational_velocity_rad_s(message.car_engine_rotational_velocity_rad_s);
            proto_ptr->set_car_engine_torque_nm(message.car_engine_torque_Nm);
            proto_ptr->set_car_transmission_gear_integer(message.car_transmission_gear_integer);
            proto_ptr->set_car_steer_rad(message.car_steer_rad);
            proto_ptr->set_car_shift_integer(message.car_shift_integer);
            proto_ptr->set_car_automatic_gear_change_boolean(message.car_automatic_gear_change_boolean);
        });
    WriteSerializedString(writer, sample->SerializeAsString(), data_bucket, m_channel_id);
}
}  //namespace mpmca::visualization