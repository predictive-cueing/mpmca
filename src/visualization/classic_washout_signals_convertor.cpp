/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/visualization/classic_washout_signals_convertor.hpp"

#include "classic_washout_signals.pb.h"
#include "mpmca/messages/classic_washout_signals.hpp"

#define MCAP_IMPLEMENTATION
#include "mpmca/utilities/mpmca_mcap.hpp"

namespace mpmca::visualization {

ClassicWashoutSignalsConvertor::ClassicWashoutSignalsConvertor(mcap::McapWriter& writer)
    : m_channel_id(AddSchema<proto::ClassicWashoutSignals>(writer, "/classic_washout_algorithm"))
{
}
void ClassicWashoutSignalsConvertor::ProcessDataBucket(mcap::McapWriter& writer,
                                                       const pipeline::DataBucket& data_bucket)
{
    data_bucket.GetTaskSignalBus().ReadMessageIfUpdated<messages::ClassicWashoutSignals>(
        [&](const messages::ClassicWashoutSignals& message) {
            proto::ClassicWashoutSignals* sample = new proto::ClassicWashoutSignals();
            {
                auto* proto_ptr = sample->mutable_head_inertial_cwa();

                proto_ptr->set_head_f_x_cwa_m_s2(message.head_inertial_cwa.GetFx());
                proto_ptr->set_head_f_y_cwa_m_s2(message.head_inertial_cwa.GetFy());
                proto_ptr->set_head_f_z_cwa_m_s2(message.head_inertial_cwa.GetFz());
                proto_ptr->set_head_omega_x_cwa_rad_s(message.head_inertial_cwa.GetOmegaX());
                proto_ptr->set_head_omega_y_cwa_rad_s(message.head_inertial_cwa.GetOmegaY());
                proto_ptr->set_head_omega_z_cwa_rad_s(message.head_inertial_cwa.GetOmegaZ());
                proto_ptr->set_head_alpha_x_cwa_rad_s2(message.head_inertial_cwa.GetAlphaX());
                proto_ptr->set_head_alpha_y_cwa_rad_s2(message.head_inertial_cwa.GetAlphaY());
                proto_ptr->set_head_alpha_z_cwa_rad_s2(message.head_inertial_cwa.GetAlphaZ());
            }

            {
                auto* proto_ptr = sample->mutable_simulator_state_reference_cwa();

                proto_ptr->set_world_x_cwa_m(message.simulator_state_reference_cwa.GetX());
                proto_ptr->set_world_y_cwa_m(message.simulator_state_reference_cwa.GetY());
                proto_ptr->set_world_z_cwa_m(message.simulator_state_reference_cwa.GetZ());
                proto_ptr->set_world_phi_cwa_rad(message.simulator_state_reference_cwa.GetPhi());
                proto_ptr->set_world_theta_cwa_rad(message.simulator_state_reference_cwa.GetTheta());
                proto_ptr->set_world_psi_cwa_rad(message.simulator_state_reference_cwa.GetPsi());
                proto_ptr->set_body_u_cwa_m_s(message.simulator_state_reference_cwa.GetXd());
                proto_ptr->set_body_v_cwa_m_s(message.simulator_state_reference_cwa.GetYd());
                proto_ptr->set_body_w_cwa_m_s(message.simulator_state_reference_cwa.GetZd());
                proto_ptr->set_body_p_cwa_rad_s(message.simulator_state_reference_cwa.GetP());
                proto_ptr->set_body_q_cwa_rad_s(message.simulator_state_reference_cwa.GetQ());
                proto_ptr->set_body_r_cwa_rad_s(message.simulator_state_reference_cwa.GetR());
                proto_ptr->set_body_a_x_cwa_m_s2(message.simulator_state_reference_cwa.GetXdd());
                proto_ptr->set_body_a_y_cwa_m_s2(message.simulator_state_reference_cwa.GetYdd());
                proto_ptr->set_body_a_z_cwa_m_s2(message.simulator_state_reference_cwa.GetZdd());
                proto_ptr->set_body_p_dot_cwa_rad_s2(message.simulator_state_reference_cwa.GetPd());
                proto_ptr->set_body_q_dot_cwa_rad_s2(message.simulator_state_reference_cwa.GetQd());
                proto_ptr->set_body_r_dot_cwa_rad_s2(message.simulator_state_reference_cwa.GetRd());
            }
            WriteSerializedString(writer, sample->SerializeAsString(), data_bucket, m_channel_id);
        });
}
}  //namespace mpmca::visualization