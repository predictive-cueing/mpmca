/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/visualization/build_file_descriptor_set.hpp"

#include <queue>
#include <unordered_set>

namespace foxglove {

/// Builds a FileDescriptorSet of this descriptor and all transitive dependencies, for use as a
/// channel schema.
google::protobuf::FileDescriptorSet BuildFileDescriptorSet(const google::protobuf::Descriptor* toplevel_descriptor)
{
    google::protobuf::FileDescriptorSet fd_set;
    std::queue<const google::protobuf::FileDescriptor*> to_add;
    to_add.push(toplevel_descriptor->file());
    std::unordered_set<std::string> seen_dependencies;
    while (!to_add.empty()) {
        const google::protobuf::FileDescriptor* next = to_add.front();
        to_add.pop();
        next->CopyTo(fd_set.add_file());
        for (int i = 0; i < next->dependency_count(); ++i) {
            const auto& dep = next->dependency(i);
            if (seen_dependencies.find(dep->name()) == seen_dependencies.end()) {
                seen_dependencies.insert(dep->name());
                to_add.push(dep);
            }
        }
    }
    return fd_set;
}

}  // namespace foxglove
