/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <cstddef>

#include "mpmca/visualization/foxglove_message_convertor.hpp"

namespace mpmca::visualization {

void FoxgloveMessageConvertor::WriteSerializedString(mcap::McapWriter& writer, const std::string& string,
                                                     const pipeline::DataBucket& data_bucket,
                                                     mcap::ChannelId channel_id)
{
    WriteSerializedString(writer, string, data_bucket.GetMainTickCycle(), data_bucket.GetPipelineTimeMs() * 1000 * 1000,
                          channel_id);
}

void FoxgloveMessageConvertor::WriteSerializedString(mcap::McapWriter& writer, const std::string& string,
                                                     uint32_t sequence, mcap::Timestamp log_time,
                                                     mcap::ChannelId channel_id)
{
    mcap::Message mcap_message;
    mcap_message.channelId = channel_id;
    mcap_message.sequence = sequence;
    mcap_message.publishTime = log_time;
    mcap_message.logTime = log_time;
    mcap_message.data = reinterpret_cast<const std::byte*>(string.data());
    mcap_message.dataSize = string.size();
    const auto map_res = writer.write(mcap_message);
}
}  //namespace mpmca::visualization