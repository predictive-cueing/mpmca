/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/visualization/automotive_prediction_horizon_convertor.hpp"

#include "automotive_prediction_horizon.pb.h"
#include "foxglove/FrameTransform.pb.h"
#include "foxglove/SceneEntity.pb.h"
#include "foxglove/SceneUpdate.pb.h"
#include "mpmca/utilities/math.hpp"
#include "mpmca/visualization/build_file_descriptor_set.hpp"
#include "mpmca/visualization/proto_tools.hpp"

namespace mpmca::visualization {

AutomotivePredictionHorizonConvertor::AutomotivePredictionHorizonConvertor(mcap::McapWriter& writer)
    : m_logger("AutomotivePredictionHorizonConvertor", "AutomotivePredictionHorizonConvertor")
    , m_path_channel_id(AddSchema<foxglove::SceneUpdate>(writer, "/prediction/path"))
    , m_car_coordinate_transform_id(AddSchema<foxglove::FrameTransform>(writer, "/prediction/tf"))
    , m_signals_channel_id(AddSchema<proto::AutomotivePredictionHorizon>(writer, "/prediction/signals"))
{
}
void AutomotivePredictionHorizonConvertor::ProcessDataBucket(mcap::McapWriter& writer,
                                                             const pipeline::DataBucket& data_bucket)
{
    data_bucket.GetTaskSignalBus().ReadMessageIfUpdated<messages::AutomotivePredictionHorizon>(
        [&](const messages::AutomotivePredictionHorizon& message) {
            WritePath(writer, message, data_bucket);
            WriteCarCoordinateTransform(writer, message, data_bucket);
            WriteSignals(writer, message, data_bucket);
        });
}
int64_t AutomotivePredictionHorizonConvertor::GetSeconds(int64_t pipeline_time_ms)
{
    return (pipeline_time_ms - (pipeline_time_ms % 1000)) / 1000;
}
int64_t AutomotivePredictionHorizonConvertor::GetNanos(int64_t pipeline_time_ms)
{
    return (pipeline_time_ms % 1000) * 1000 * 1000;
}

void AutomotivePredictionHorizonConvertor::WriteSignals(mcap::McapWriter& writer,
                                                        const messages::AutomotivePredictionHorizon& message,
                                                        const pipeline::DataBucket& data_bucket)
{
    auto* horizon_signals = new proto::AutomotivePredictionHorizon();

    ProtoTools::FillField(horizon_signals->mutable_horizon_time_grid(), data_bucket.GetHorizon().GetHorizonTime());

    ProtoTools::FillField(horizon_signals->mutable_world_x_vehicle_m(), message.world_X_vehicle_m);
    ProtoTools::FillField(horizon_signals->mutable_world_y_vehicle_m(), message.world_Y_vehicle_m);
    ProtoTools::FillField(horizon_signals->mutable_world_z_vehicle_m(), message.world_Z_vehicle_m);
    ProtoTools::FillField(horizon_signals->mutable_world_phi_vehicle_rad(), message.world_phi_vehicle_rad);
    ProtoTools::FillField(horizon_signals->mutable_world_theta_vehicle_rad(), message.world_theta_vehicle_rad);
    ProtoTools::FillField(horizon_signals->mutable_world_psi_vehicle_rad(), message.world_psi_vehicle_rad);
    ProtoTools::FillField(horizon_signals->mutable_body_u_vehicle_m_s(), message.body_u_vehicle_m_s);
    ProtoTools::FillField(horizon_signals->mutable_body_v_vehicle_m_s(), message.body_v_vehicle_m_s);
    ProtoTools::FillField(horizon_signals->mutable_body_w_vehicle_m_s(), message.body_w_vehicle_m_s);
    ProtoTools::FillField(horizon_signals->mutable_body_p_vehicle_rad_s(), message.body_p_vehicle_rad_s);
    ProtoTools::FillField(horizon_signals->mutable_body_q_vehicle_rad_s(), message.body_q_vehicle_rad_s);
    ProtoTools::FillField(horizon_signals->mutable_body_r_vehicle_rad_s(), message.body_r_vehicle_rad_s);
    ProtoTools::FillField(horizon_signals->mutable_body_a_x_vehicle_m_s2(), message.body_a_x_vehicle_m_s2);
    ProtoTools::FillField(horizon_signals->mutable_body_a_y_vehicle_m_s2(), message.body_a_y_vehicle_m_s2);
    ProtoTools::FillField(horizon_signals->mutable_body_a_z_vehicle_m_s2(), message.body_a_z_vehicle_m_s2);
    ProtoTools::FillField(horizon_signals->mutable_body_p_dot_vehicle_rad_s2(), message.body_p_dot_vehicle_rad_s2);
    ProtoTools::FillField(horizon_signals->mutable_body_q_dot_vehicle_rad_s2(), message.body_q_dot_vehicle_rad_s2);
    ProtoTools::FillField(horizon_signals->mutable_body_r_dot_vehicle_rad_s2(), message.body_r_dot_vehicle_rad_s2);
    ProtoTools::FillField(horizon_signals->mutable_head_f_x_head_m_s2(), message.head_f_x_head_m_s2);
    ProtoTools::FillField(horizon_signals->mutable_head_f_y_head_m_s2(), message.head_f_y_head_m_s2);
    ProtoTools::FillField(horizon_signals->mutable_head_f_z_head_m_s2(), message.head_f_z_head_m_s2);
    ProtoTools::FillField(horizon_signals->mutable_head_omega_x_head_rad_s(), message.head_omega_x_head_rad_s);
    ProtoTools::FillField(horizon_signals->mutable_head_omega_y_head_rad_s(), message.head_omega_y_head_rad_s);
    ProtoTools::FillField(horizon_signals->mutable_head_omega_z_head_rad_s(), message.head_omega_z_head_rad_s);
    ProtoTools::FillField(horizon_signals->mutable_head_alpha_x_head_rad_s2(), message.head_alpha_x_head_rad_s2);
    ProtoTools::FillField(horizon_signals->mutable_head_alpha_y_head_rad_s2(), message.head_alpha_y_head_rad_s2);
    ProtoTools::FillField(horizon_signals->mutable_head_alpha_z_head_rad_s2(), message.head_alpha_z_head_rad_s2);
    ProtoTools::FillField(horizon_signals->mutable_world_x_path_m(), message.world_X_path_m);
    ProtoTools::FillField(horizon_signals->mutable_world_y_path_m(), message.world_Y_path_m);
    ProtoTools::FillField(horizon_signals->mutable_world_z_path_m(), message.world_Z_path_m);
    ProtoTools::FillField(horizon_signals->mutable_world_phi_path_rad(), message.world_phi_path_rad);
    ProtoTools::FillField(horizon_signals->mutable_world_theta_path_rad(), message.world_theta_path_rad);
    ProtoTools::FillField(horizon_signals->mutable_world_psi_path_rad(), message.world_psi_path_rad);
    ProtoTools::FillField(horizon_signals->mutable_car_steer_rad(), message.car_steer_rad);
    ProtoTools::FillField(horizon_signals->mutable_car_throttle_fraction(), message.car_throttle_fraction);
    ProtoTools::FillField(horizon_signals->mutable_car_brake_fraction(), message.car_brake_fraction);
    ProtoTools::FillField(horizon_signals->mutable_car_clutch_fraction(), message.car_clutch_fraction);
    ProtoTools::FillField(horizon_signals->mutable_car_shift_integer(), message.car_shift_integer);
    ProtoTools::FillField(horizon_signals->mutable_human_car_u_t_m_s(), message.human_car_u_t_m_s);
    ProtoTools::FillField(horizon_signals->mutable_human_car_u_e_m_s(), message.human_car_u_e_m_s);
    ProtoTools::FillField(horizon_signals->mutable_human_car_lat_e_m(), message.human_car_lat_e_m);
    ProtoTools::FillField(horizon_signals->mutable_human_car_lat_e_dot_m_s(), message.human_car_lat_e_dot_m_s);
    ProtoTools::FillField(horizon_signals->mutable_human_car_psi_t_rad(), message.human_car_psi_t_rad);
    ProtoTools::FillField(horizon_signals->mutable_human_car_psi_e_rad(), message.human_car_psi_e_rad);
    ProtoTools::FillField(horizon_signals->mutable_car_transmission_gear_integer(),
                          message.car_transmission_gear_integer);
    ProtoTools::FillField(horizon_signals->mutable_car_beta_vehicle_rad(), message.car_beta_vehicle_rad);
    ProtoTools::FillField(horizon_signals->mutable_car_delta_f_rad(), message.car_delta_f_rad);

    std::string serialized_data = horizon_signals->SerializeAsString();

    mcap::Message mcap_message;
    mcap_message.channelId = m_signals_channel_id;
    mcap_message.sequence = data_bucket.GetMainTickCycle();
    mcap_message.publishTime = data_bucket.GetPipelineTimeMs() * 1000 * 1000;
    mcap_message.logTime = data_bucket.GetPipelineTimeMs() * 1000 * 1000;
    mcap_message.data = reinterpret_cast<const std::byte*>(serialized_data.data());
    mcap_message.dataSize = serialized_data.size();
    const auto map_res = writer.write(mcap_message);
}

void AutomotivePredictionHorizonConvertor::WriteCarCoordinateTransform(
    mcap::McapWriter& writer, const messages::AutomotivePredictionHorizon& message,
    const pipeline::DataBucket& data_bucket)
{
    auto* frame_transform = new foxglove::FrameTransform();

    frame_transform->set_parent_frame_id("<root>");
    frame_transform->set_child_frame_id("car_follow_frame");

    frame_transform->mutable_translation()->set_x(message.world_X_path_m.at(0));
    frame_transform->mutable_translation()->set_y(-message.world_Y_path_m.at(0));
    frame_transform->mutable_translation()->set_z(-message.world_Z_path_m.at(0));

    Vector<4> quaternion = utilities::Math::EulerToQuaternion(
        message.world_phi_path_rad.at(0), -message.world_theta_path_rad.at(0), -message.world_psi_path_rad.at(0));

    frame_transform->mutable_rotation()->set_w(quaternion[0]);
    frame_transform->mutable_rotation()->set_x(quaternion[1]);
    frame_transform->mutable_rotation()->set_y(quaternion[2]);
    frame_transform->mutable_rotation()->set_z(quaternion[3]);

    auto* timestamp = new google::protobuf::Timestamp();
    timestamp->set_seconds(GetSeconds(data_bucket.GetPipelineTimeMs()));
    timestamp->set_nanos(GetNanos(data_bucket.GetPipelineTimeMs()));

    frame_transform->set_allocated_timestamp(timestamp);

    std::string serialized_data = frame_transform->SerializeAsString();

    mcap::Message mcap_message;
    mcap_message.channelId = m_car_coordinate_transform_id;
    mcap_message.sequence = data_bucket.GetMainTickCycle();
    mcap_message.publishTime = data_bucket.GetPipelineTimeMs() * 1000 * 1000;
    mcap_message.logTime = data_bucket.GetPipelineTimeMs() * 1000 * 1000;
    mcap_message.data = reinterpret_cast<const std::byte*>(serialized_data.data());
    mcap_message.dataSize = serialized_data.size();
    const auto map_res = writer.write(mcap_message);
}

void AutomotivePredictionHorizonConvertor::WritePath(mcap::McapWriter& writer,
                                                     const messages::AutomotivePredictionHorizon& message,
                                                     const pipeline::DataBucket& data_bucket)
{
    auto* scene_update = new foxglove::SceneUpdate();

    auto* scene_entity = scene_update->add_entities();
    scene_entity->set_frame_id("world_frame");
    scene_entity->set_id("predictor_path");

    auto* timestamp = new google::protobuf::Timestamp();
    timestamp->set_seconds(GetSeconds(data_bucket.GetPipelineTimeMs()));
    timestamp->set_nanos(GetNanos(data_bucket.GetPipelineTimeMs()));

    scene_entity->mutable_cubes()->Reserve(message.world_X_path_m.size());
    scene_entity->set_allocated_timestamp(timestamp);

    for (size_t i = 0; i < message.world_X_path_m.size(); ++i) {
        foxglove::CubePrimitive* cube = new foxglove::CubePrimitive();
        foxglove::ArrowPrimitive* arrow = new foxglove::ArrowPrimitive();

        foxglove::Vector3* position = new foxglove::Vector3();
        position->set_x(message.world_X_path_m.at(i));
        position->set_y(message.world_Y_path_m.at(i));
        position->set_z(message.world_Z_path_m.at(i));

        foxglove::Quaternion* orientation = new foxglove::Quaternion();
        auto quat = utilities::Math::EulerToQuaternion(
            message.world_phi_path_rad.at(i), message.world_theta_path_rad.at(i), message.world_psi_path_rad.at(i));
        orientation->set_w(quat[0]);
        orientation->set_x(quat[1]);
        orientation->set_y(quat[2]);
        orientation->set_z(quat[3]);

        foxglove::Pose* pose = new foxglove::Pose();
        pose->set_allocated_position(position);
        pose->set_allocated_orientation(orientation);
        cube->set_allocated_pose(pose);
        arrow->set_allocated_pose(pose);

        foxglove::Color* color = new foxglove::Color();
        color->set_a(0.5);
        color->set_r(1.0);
        color->set_g(0);
        color->set_b(0);
        cube->set_allocated_color(color);

        foxglove::Vector3* size = new foxglove::Vector3();
        size->set_x(1);
        size->set_y(1);
        size->set_z(1);
        cube->set_allocated_size(size);

        scene_entity->mutable_cubes()->AddAllocated(cube);
    }

    std::string serialized = scene_update->SerializeAsString();

    mcap::Message mcap_message;
    mcap_message.channelId = m_path_channel_id;
    mcap_message.sequence = data_bucket.GetMainTickCycle();
    mcap_message.publishTime = data_bucket.GetPipelineTimeMs() * 1000 * 1000;
    mcap_message.logTime = data_bucket.GetPipelineTimeMs() * 1000 * 1000;
    mcap_message.data = reinterpret_cast<const std::byte*>(serialized.data());
    mcap_message.dataSize = serialized.size();
    const auto res = writer.write(mcap_message);
}
}  //namespace mpmca::visualization