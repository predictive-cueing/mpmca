/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/visualization/automotive_prediction_path_convertor.hpp"
#define MCAP_IMPLEMENTATION
#include "mpmca/constants.hpp"
#include "mpmca/utilities/mpmca_mcap.hpp"
namespace mpmca::visualization {

std::string AutomotivePredictionPathConvertor::SerializePath(predict::inertial::PathPtr path_ptr)
{
    auto* scene_update = new foxglove::SceneUpdate();

    auto* scene_entity = scene_update->add_entities();
    scene_entity->set_frame_id("world_frame");
    scene_entity->set_id("path");

    scene_entity->mutable_cubes()->Reserve(path_ptr->GetNumPathPoints());

    for (size_t i = 0; i < path_ptr->GetNumPathPoints(); ++i) {
        auto& path_point = path_ptr->GetPathPointAt(i);

        foxglove::CubePrimitive* cube = new foxglove::CubePrimitive();

        foxglove::Vector3* position = new foxglove::Vector3();
        position->set_x(path_point.GetX());
        position->set_y(path_point.GetY());
        position->set_z(path_point.GetZ());

        foxglove::Quaternion* orientation = new foxglove::Quaternion();
        auto quat = path_point.GetOrientationQuaternion();
        orientation->set_w(quat[0]);
        orientation->set_x(quat[1]);
        orientation->set_y(quat[2]);
        orientation->set_z(quat[3]);

        foxglove::Pose* pose = new foxglove::Pose();
        pose->set_allocated_position(position);
        pose->set_allocated_orientation(orientation);
        cube->set_allocated_pose(pose);

        foxglove::Color* color = new foxglove::Color();
        color->set_a(0.5);
        color->set_r(1.0);
        color->set_g(0);
        color->set_b(0);
        cube->set_allocated_color(color);

        foxglove::Vector3* size = new foxglove::Vector3();
        size->set_x(1);
        size->set_y(path_point.GetRoadWidth());
        size->set_z(1);
        cube->set_allocated_size(size);

        scene_entity->mutable_cubes()->AddAllocated(cube);
    }

    return scene_update->SerializeAsString();
}

std::string AutomotivePredictionPathConvertor::SerializeFrameTransform(mcap::McapWriter& writer)
{
    foxglove::FrameTransform* root_to_world_frame = new foxglove::FrameTransform();
    root_to_world_frame->mutable_timestamp()->set_seconds(0);
    root_to_world_frame->mutable_timestamp()->set_nanos(0);
    root_to_world_frame->set_parent_frame_id("<root>");
    root_to_world_frame->set_child_frame_id("world_frame");
    root_to_world_frame->mutable_translation()->set_x(0);
    root_to_world_frame->mutable_translation()->set_y(0);
    root_to_world_frame->mutable_translation()->set_z(0);

    Vector<4> quaternion = utilities::Math::EulerToQuaternion(constants::kPi, 0, 0);

    root_to_world_frame->mutable_rotation()->set_w(quaternion[0]);
    root_to_world_frame->mutable_rotation()->set_x(quaternion[1]);
    root_to_world_frame->mutable_rotation()->set_y(quaternion[2]);
    root_to_world_frame->mutable_rotation()->set_z(quaternion[3]);

    return root_to_world_frame->SerializeAsString();
}

AutomotivePredictionPathConvertor::AutomotivePredictionPathConvertor(mcap::McapWriter& writer)
    : m_path_channel_id(AddSchema<foxglove::SceneUpdate>(writer, "/path"))
    , m_frame_transform_channel_id(AddSchema<foxglove::FrameTransform>(writer, "/world_frame"))
{
    WriteSerializedString(writer, SerializeFrameTransform(writer), 0, 0, m_frame_transform_channel_id);
}
void AutomotivePredictionPathConvertor::ProcessDataBucket(mcap::McapWriter& writer,
                                                          const pipeline::DataBucket& data_bucket)
{
    data_bucket.GetTaskSignalBus().ReadMessageIfUpdated<messages::AutomotivePredictionPath>(
        [&](const messages::AutomotivePredictionPath& message) {
            WriteSerializedString(writer, SerializePath(message.path_ptr), data_bucket, m_path_channel_id);
        });
}
}  //namespace mpmca::visualization