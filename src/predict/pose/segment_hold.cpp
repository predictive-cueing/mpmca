/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/pose/segment_hold.hpp"

#include "mpmca/json_tools.hpp"
#include "mpmca/predict/pose/trajectory.hpp"
namespace mpmca::predict::pose {

SegmentHold::SegmentHold(const Trajectory& trajectory, utilities::ConfigurationPtr config)
    : SegmentHold(trajectory, config->GetValue<std::string>("Name", "some_name"),
                  config->GetValue<int64_t>("TimeHoldMs", 100),
                  config->GetValue<PoseRotMatVector>("OutputErrorWeight", PoseRotMatVector::Zero()))
{
}
SegmentHold::SegmentHold(const Trajectory& trajectory, const std::string& name, int64_t t_hold_ms,
                         const PoseRotMatVector& output_error_weight)
    : Segment(trajectory, name, t_hold_ms)
    , m_t_hold_ms(CheckTimeIsDivisible(t_hold_ms))
    , m_output_error_weight(output_error_weight)
{
    m_type = std::string("Hold");
}
void SegmentHold::Calculate(Segment const* previous_segment)
{
    int N = m_t_hold_ms / m_dt_ms;

    if (!previous_segment)
        throw std::runtime_error("A hold segment requires the previous segment to be set.");

    m_reference_pose_plan.resize(N, previous_segment->GetFinalReferencePose());
    m_output_error_weight_plan.resize(N, m_output_error_weight);
}
}  //namespace mpmca::predict::pose