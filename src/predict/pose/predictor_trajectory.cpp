/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/pose/predictor_trajectory.hpp"

#include "mpmca/predict/pose/segment_end.hpp"
#include "mpmca/predict/pose/segment_free_move.hpp"
#include "mpmca/predict/pose/segment_hold.hpp"
#include "mpmca/predict/pose/segment_linear_move.hpp"
#include "mpmca/predict/pose/segment_start.hpp"
#include "mpmca/utilities/math.hpp"
namespace mpmca::predict::pose {
PredictorTrajectory::PredictorTrajectory(utilities::ConfigurationPtr config, const utilities::Horizon& horizon,
                                         const std::string& name)
    : PosePredictor(config, horizon, name)
    , m_trajectory(config, horizon)
    , m_cycle(0)
{
}

void PredictorTrajectory::Predict()
{
    m_trajectory.Tick();

    for (int i = 0; i < m_horizon.GetNPrediction(); ++i) {
        m_output_error_weight_plan[i] =
            m_trajectory.GetOutputErrorWeightAtHorizonTimeMs(m_horizon.GetPredictionHorizonTimeMs(i));
        m_reference_output_plan[i] =
            m_trajectory.GetReferencePoseAtHorizonTimeMs(m_horizon.GetPredictionHorizonTimeMs(i)).ToPoseRotMatVector();
    }
}
PredictorTrajectory::~PredictorTrajectory()
{
}
}  //namespace mpmca::predict::pose