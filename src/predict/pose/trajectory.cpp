/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/pose/trajectory.hpp"

#include "mpmca/json_tools.hpp"
#include "mpmca/predict/pose/trajectory_t.hpp"
#include "mpmca/utilities/math.hpp"

namespace mpmca::predict::pose {
Trajectory::Trajectory(utilities::ConfigurationPtr config, const utilities::Horizon& horizon)
    : Trajectory(horizon)
{
    if (config->GetKeyExists("Waypoints")) {
        auto waypoint_config = config->GetConfigurationObjectArray("Waypoints", 1);

        for (size_t i = 0; i < waypoint_config->GetNumObjectArrayElements(); ++i) {
            auto local_config = waypoint_config->GetConfigurationObjectArrayElement(i);
            Vector<6> pose_position = local_config->GetValue<Vector<6>>("PoseVector", Vector<6>::Zero());
            PoseVector pose_complete = PoseVector::Zero();
            pose_complete.GetX() = pose_position[0];
            pose_complete.GetY() = pose_position[1];
            pose_complete.GetZ() = pose_position[2];
            pose_complete.GetPhi() = pose_position[3];
            pose_complete.GetTheta() = pose_position[4];
            pose_complete.GetPsi() = pose_position[5];
            AddWaypoint(local_config->GetValue<std::string>("Name", "test"), pose_complete);
        }
    }

    auto segments_config = config->GetConfigurationObjectArray("Segments", 1);

    for (size_t i = 0; i < segments_config->GetNumObjectArrayElements(); ++i) {
        auto local_config = segments_config->GetConfigurationObjectArrayElement(i);

        std::string type_name = local_config->GetValue<std::string>("SegmentType", "Start");
        SegmentType type = segment_type_strings.find(type_name) != segment_type_strings.end()
                               ? segment_type_strings.find(type_name)->second
                               : SegmentType::kInvalid;

        try {
            switch (type) {
                case SegmentType::kStart:
                    AddSegment<SegmentStart>(local_config);
                    break;
                case SegmentType::kFreeMove:
                    AddSegment<SegmentFreeMove>(local_config);
                    break;
                case SegmentType::kHold:
                    AddSegment<SegmentHold>(local_config);
                    break;
                case SegmentType::kLinearMove:
                    AddSegment<SegmentLinearMove>(local_config);
                    break;
                case SegmentType::kLinearMovePose:
                    AddSegment<SegmentLinearMove>(local_config);
                    break;
                case SegmentType::kEnd:
                    AddSegment<SegmentEnd>(local_config);
                    break;
                case SegmentType::kGotoSegment:
                    AddSegment<SegmentGoto>(local_config);
                    break;
                case SegmentType::kInvalid:
                    throw std::invalid_argument("Did not recognize segment type " + type_name);
            }
        }
        catch (const std::exception& e) {
            throw std::invalid_argument("Error while initializing trajectory on step " + std::to_string(i) +
                                        " of type " + type_name + ":\n" + e.what());
        }
    }
}
Trajectory::Trajectory(const utilities::Horizon& horizon)
    : m_horizon(horizon)
    , m_has_end_segment(false)
    , m_reference_pose_plan(horizon.GetNPrediction(), PoseVector::Zero())
    , m_output_error_weight_plan(horizon.GetNPrediction(), PoseRotMatVector::Zero())
    , m_current_segment_local_time(0)
    , m_current_segment(nullptr)
    , m_previous_segment(nullptr)
{
}
Segment* Trajectory::GetCurrentSegment()
{
    if (!m_current_segment && m_segments.size() > 0)
        m_current_segment = m_segments.begin()->get();

    return m_current_segment;
}
Segment* Trajectory::GetPreviousSegment()
{
    return m_previous_segment;
}
Segment* Trajectory::GetNextSegmentInDeque(Segment* current_segment)
{
    auto it = std::find_if(m_segments.cbegin(), m_segments.cend(),
                           [current_segment](const std::unique_ptr<Segment>& i) { return i.get() == current_segment; });

    auto index = std::distance(m_segments.cbegin(), it);

    if (index + 1 >= m_segments.size())
        throw std::runtime_error("Cannot get next in deque, because is located at end.");

    return m_segments[index + 1].get();
}
void Trajectory::SetNextSegment(Segment* next_segment)
{
    m_previous_segment = m_current_segment;
    m_current_segment = next_segment;
    m_current_segment_local_time = 0;
}
int64_t Trajectory::GetFundamentalDtMs() const
{
    return m_horizon.GetDtMs();
}
void Trajectory::AddWaypoint(const std::string& name, const PoseVector& pose)
{
    m_waypoint_register.MakePoint(name, pose);
}
PoseVector Trajectory::GetPoseFromRegisterOrConfig(utilities::ConfigurationPtr config, const std::string& name) const
{
    if (!config->IsString(name))
        return config->GetValue<PoseVector>(name, PoseVector::Zero());
    else
        return m_waypoint_register.GetPoseVector(config->GetValue<std::string>(name, "boo"));
}
PoseRotMatVector Trajectory::GetPoseFromRegisterOrConfigToOutputVector(utilities::ConfigurationPtr config,
                                                                       const std::string& name) const
{
    if (!config->IsString(name))
        return config->GetValue<PoseVector>(name, PoseVector::Zero()).ToPoseRotMatVector();
    else
        return m_waypoint_register.GetOutputVector(config->GetValue<std::string>(name, "boo"));
}
size_t Trajectory::GetNumSegments() const
{
    return m_segments.size();
}
Segment* Trajectory::GetSegmentByIndex(size_t i) const
{
    return m_segments.at(i).get();
}
const PoseRotMatVector& Trajectory::GetOutputErrorWeightAtIndex(size_t i) const
{
    return m_output_error_weight_plan.at(i);
}
const PoseVector& Trajectory::GetReferencePoseAtIndex(size_t i) const
{
    return m_reference_pose_plan.at(i);
}
const PoseRotMatVector& Trajectory::GetOutputErrorWeightAtHorizonTimeMs(int64_t t_get) const
{
    return m_output_error_weight_plan.at(m_horizon.GetTimeMsToPredictionHorizonIndex(t_get));
}
const PoseVector& Trajectory::GetReferencePoseAtHorizonTimeMs(int64_t t_get) const
{
    return m_reference_pose_plan.at(m_horizon.GetTimeMsToPredictionHorizonIndex(t_get));
}
bool Trajectory::HasStartSegment() const
{
    return m_segments.size() > 0;
}
bool Trajectory::HasEndSegment() const
{
    return m_has_end_segment;
}
void Trajectory::Tick()
{
    auto current_segment_ptr = GetCurrentSegment();

    if (m_current_segment_local_time > current_segment_ptr->GetTimeEndMs()) {
        current_segment_ptr->DecideNextSegment(*this);

        if (current_segment_ptr->GetNextSegment())
            SetNextSegment(current_segment_ptr->GetNextSegment());
        else
            SetNextSegment(GetNextSegmentInDeque(current_segment_ptr));
    }
    Calculate();
    m_current_segment_local_time += m_horizon.GetDtMs();
}
void Trajectory::Calculate()
{
    auto h_current_segment_ptr = GetCurrentSegment();
    auto h_previous_segment_ptr = GetPreviousSegment();
    auto h_current_segment_local_time = m_current_segment_local_time;

    if (!h_current_segment_ptr)
        throw std::runtime_error("Something wrong.");

    h_current_segment_ptr->Calculate(h_previous_segment_ptr);

    for (int i = 0; i < m_horizon.GetNPrediction(); ++i) {
        if (h_current_segment_local_time > h_current_segment_ptr->GetTimeEndMs()) {
            h_current_segment_ptr->PredictNextSegment(*this);

            h_previous_segment_ptr = h_current_segment_ptr;
            h_current_segment_local_time = 0;

            if (h_current_segment_ptr->GetPredictedNextSegment())
                h_current_segment_ptr = h_current_segment_ptr->GetPredictedNextSegment();
            else
                h_current_segment_ptr = GetNextSegmentInDeque(h_current_segment_ptr);

            if (!h_current_segment_ptr)
                throw std::runtime_error("There are not enough segments available to fill the entire horizon.");

            h_current_segment_ptr->Calculate(h_previous_segment_ptr);
        }

        m_output_error_weight_plan.at(i) =
            h_current_segment_ptr->GetOutputErrorWeightAtLocalTimeMs(h_current_segment_local_time);
        m_reference_pose_plan.at(i) =
            h_current_segment_ptr->GetReferencePoseAtLocalTimeMs(h_current_segment_local_time);

        h_current_segment_local_time += m_horizon.GetPredictionHorizonDeltaTimeMs(i);
    }
}
}  //namespace mpmca::predict::pose