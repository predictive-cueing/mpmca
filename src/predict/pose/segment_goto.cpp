/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/pose/segment_goto.hpp"

#include "mpmca/predict/pose/trajectory.hpp"

namespace mpmca::predict::pose {

SegmentGoto::SegmentGoto(const Trajectory& trajectory, utilities::ConfigurationPtr config)
    : SegmentGoto(trajectory, config->GetValue<std::string>("Name", "some_name"),
                  config->GetValue<std::string>("GoToSegmentName", "empty"),
                  config->GetValue<int64_t>("TimeMoveMs", 100))
{
}
SegmentGoto::SegmentGoto(const Trajectory& trajectory, const std::string& name, const std::string& goto_segment_name,
                         int64_t t_free_move_ms)
    : Segment(trajectory, name, t_free_move_ms)
    , m_t_free_move_ms(CheckTimeIsDivisible(t_free_move_ms))
    , m_goto_segment_name(goto_segment_name)
{
    m_type = std::string("GoTo");
}
void SegmentGoto::Calculate(Segment const* previous_segment)
{
    int N = m_t_free_move_ms / m_dt_ms;

    if (!previous_segment)
        throw std::runtime_error("A hold segment requires the previous segment to be set.");

    m_reference_pose_plan.resize(N, previous_segment->GetFinalReferencePose());
    m_output_error_weight_plan.resize(N, PoseRotMatVector::Zero());
}
void SegmentGoto::DecideNextSegment(const Trajectory& trajectory)
{
    PredictNextSegment(trajectory);
    m_next_segment = m_predicted_next_segment;
}
void SegmentGoto::PredictNextSegment(const Trajectory& trajectory)
{
    size_t n = trajectory.GetNumSegments();

    for (size_t i = 0; i < n; ++i) {
        if (trajectory.GetSegmentByIndex(i)->GetName().compare(m_goto_segment_name) == 0) {
            m_predicted_next_segment = trajectory.GetSegmentByIndex(i);
            break;
        }
    }
}
}  //namespace mpmca::predict::pose