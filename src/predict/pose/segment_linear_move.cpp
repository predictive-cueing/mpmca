/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/pose/segment_linear_move.hpp"

#include "mpmca/json_tools.hpp"
#include "mpmca/predict/pose/trajectory.hpp"

namespace mpmca::predict::pose {
SegmentLinearMove SegmentLinearMove::InitTrajectorySegmentLinearMove(const Trajectory& trajectory,
                                                                     utilities::ConfigurationPtr config)
{
    if (config->GetKeyExists("ReferencePoseStart")) {
        return std::move(SegmentLinearMove(
            trajectory, config->GetValue<std::string>("Name", "some_name"), config->GetValue<int64_t>("TimeMoveMs", 100),
            trajectory.GetPoseFromRegisterOrConfig(config, "ReferencePoseStart"),
            trajectory.GetPoseFromRegisterOrConfig(config, "ReferencePoseEnd"),
            config->GetValue<PoseRotMatVector>("OutputErrorWeight", PoseRotMatVector::Zero())));
    }
    else {
        return std::move(SegmentLinearMove(
            trajectory, config->GetValue<std::string>("Name", "some_name"), config->GetValue<int64_t>("TimeMoveMs", 100),
            trajectory.GetPoseFromRegisterOrConfig(config, "ReferencePoseEnd"),
            config->GetValue<PoseRotMatVector>("OutputErrorWeight", PoseRotMatVector::Zero())));
    }
}
SegmentLinearMove::SegmentLinearMove(const Trajectory& trajectory, utilities::ConfigurationPtr config)
    : SegmentLinearMove(InitTrajectorySegmentLinearMove(trajectory, config))
{
}

SegmentLinearMove::SegmentLinearMove(const Trajectory& trajectory, const std::string& name, int64_t t_move_ms,
                                     const PoseVector& end_reference_pose, const PoseRotMatVector& output_error_weight)
    : SegmentLinearMove(trajectory, name, t_move_ms, PoseVector::Zero(), end_reference_pose, output_error_weight, false)
{
}

SegmentLinearMove::SegmentLinearMove(const Trajectory& trajectory, const std::string& name, int64_t t_move_ms,
                                     const PoseVector& start_reference_pose, const PoseVector& end_reference_pose,
                                     const PoseRotMatVector& output_error_weight)
    : SegmentLinearMove(trajectory, name, t_move_ms, start_reference_pose, end_reference_pose, output_error_weight,
                        true)
{
}

SegmentLinearMove::SegmentLinearMove(const Trajectory& trajectory, const std::string& name, int64_t t_move_ms,
                                     const PoseVector& start_reference_pose, const PoseVector& end_reference_pose,
                                     const PoseRotMatVector& output_error_weight, bool fixed_start_reference)
    : Segment(trajectory, name, t_move_ms)
    , m_t_move_ms(CheckTimeIsDivisible(t_move_ms))
    , m_fixed_start_reference(fixed_start_reference)
    , m_start_reference_pose(start_reference_pose)
    , m_end_reference_pose(end_reference_pose)
    , m_output_error_weight(output_error_weight)
{
    m_type = std::string("LinearMove");
}

const PoseVector& SegmentLinearMove::GetStartReferencePose() const
{
    return m_start_reference_pose;
}
const PoseVector& SegmentLinearMove::GetEndReferencePose() const
{
    return m_end_reference_pose;
}
const PoseRotMatVector& SegmentLinearMove::GetOutputErrorWeight() const
{
    return m_output_error_weight;
}
void SegmentLinearMove::Calculate(Segment const* previous_segment)
{
    int N = m_t_move_ms / m_dt_ms;

    if (N <= 1)
        throw std::runtime_error("Moving time (" + std::to_string(m_t_move_ms) +
                                 " ms) should be at least 2 times the sample time (" + std::to_string(m_dt_ms) +
                                 " ms).");

    auto start_reference_pose =
        m_fixed_start_reference ? m_start_reference_pose : previous_segment->GetFinalReferencePose();

    m_reference_pose_plan.resize(N, PoseVector::Zero());
    m_output_error_weight_plan.resize(N, m_output_error_weight);

    for (int i = 0; i < N; ++i) {
        double alpha = (double)i / (double)(N - 1);
        m_reference_pose_plan[i] = (1 - alpha) * start_reference_pose + alpha * m_end_reference_pose;
    }
}
}  //namespace mpmca::predict::pose