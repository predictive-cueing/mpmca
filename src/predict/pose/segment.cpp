/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/pose/segment.hpp"

#include "mpmca/predict/pose/trajectory.hpp"

namespace mpmca::predict::pose {
Segment::Segment(const Trajectory& trajectory, const std::string& name, int64_t t_duration_ms)
    : m_name(name)
    , m_dt_ms(trajectory.GetFundamentalDtMs())
    , m_t_duration_ms(CheckTimeIsDivisible(t_duration_ms))
    , m_t_end_ms(m_t_duration_ms - m_dt_ms)
    , m_next_segment(nullptr)
    , m_predicted_next_segment(nullptr)
{
}
const std::string& Segment::GetType() const
{
    return m_type;
}
const PoseVector& Segment::GetFinalReferencePose() const
{
    if (m_reference_pose_plan.size() == 0)
        throw std::runtime_error("Cannot return final reference output, because no reference output plan was set.");

    return m_reference_pose_plan.back();
}
const PoseRotMatVector& Segment::GetOutputErrorWeightAtLocalTimeMs(int64_t tLocalMs) const
{
    return m_output_error_weight_plan.at(CheckTimeIsWithinBounds(CheckTimeIsDivisible(tLocalMs)) / m_dt_ms);
}
const PoseVector& Segment::GetReferencePoseAtLocalTimeMs(int64_t tLocalMs) const
{
    return m_reference_pose_plan.at(CheckTimeIsWithinBounds(CheckTimeIsDivisible(tLocalMs)) / m_dt_ms);
}
const std::string& Segment::GetName() const
{
    return m_name;
}
int64_t Segment::GetDtMs() const
{
    return m_dt_ms;
}
int64_t Segment::GetTimeDurationMs() const
{
    return m_t_duration_ms;
}
int64_t Segment::GetTimeEndMs() const
{
    return m_t_end_ms;
}
int64_t Segment::CheckTimeIsDivisible(int64_t tCheck) const
{
    if (!TimeIsDivisible(tCheck))
        throw std::invalid_argument("Time (" + std::to_string(tCheck) +
                                    " is not divisible by the fundamental time step (" + std::to_string(GetDtMs()) +
                                    ").");

    return tCheck;
}
int64_t Segment::CheckTimeIsWithinBounds(int64_t tCheck) const
{
    if (!TimeIsWithinBounds(tCheck))
        throw std::invalid_argument("Time (" + std::to_string(tCheck) + ") must be between 0 and " +
                                    std::to_string(m_t_duration_ms) + ").");

    return tCheck;
}
bool Segment::TimeIsDivisible(int64_t tCheck) const
{
    return (double)(tCheck / m_dt_ms) == ((double)tCheck / (double)m_dt_ms);
}
bool Segment::TimeIsWithinBounds(int64_t tCheck) const
{
    return (m_reference_pose_plan.size() > 0) && (m_output_error_weight_plan.size() == m_reference_pose_plan.size()) &&
           (tCheck >= 0) && (tCheck < m_t_duration_ms);
}
Segment* Segment::GetNextSegment() const
{
    return m_next_segment;
}
Segment* Segment::GetPredictedNextSegment() const
{
    return m_predicted_next_segment;
}
void Segment::SetNextSegment(Segment* next_segment)
{
    m_next_segment = next_segment;
}
}  //namespace mpmca::predict::pose