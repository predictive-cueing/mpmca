/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/pose/pose_predictor.hpp"

#include "mpmca/json_tools.hpp"
#include "mpmca/messages/controller_output_error_weight_plan.hpp"
#include "mpmca/messages/controller_output_reference_plan.hpp"

namespace mpmca::predict::pose {

PosePredictor::PosePredictor(utilities::ConfigurationPtr config, const utilities::Horizon& horizon,
                             const std::string& name)
    : m_name(name)
    , m_y_simulator(PoseRotMatVector::Zero())
    , m_t_current_ms(0)
    , m_horizon(horizon)
    , m_logger("Predictor", "Predictor")
{
    m_reference_output_plan.resize(horizon.GetNPrediction(), PoseRotMatVector::Zero());
    m_output_error_weight_plan.resize(horizon.GetNPrediction(), PoseRotMatVector::Zero());
}

std::vector<PoseRotMatVector> PosePredictor::GetReferencePointsFromConfig(utilities::ConfigurationPtr config)
{
    if (!config->IsArray("ReferencePoints"))
        throw std::invalid_argument("ReferencePoints should be an array.");

    if (config->GetArray("ReferencePoints")->GetNumArrayElements() < 1)
        throw std::invalid_argument("ReferencePoints cannot be an empty array.");

    std::vector<PoseRotMatVector> reference_points;

    for (int i = 0; i < config->GetArray("ReferencePoints")->GetNumArrayElements(); ++i) {
        auto reference_pose = config->GetArray("ReferencePoints")->GetValueFromArray<PoseVector>(i, PoseVector::Zero());
        auto reference_output = reference_pose.ToPoseRotMatVector();
        reference_points.push_back(reference_output);
    }

    return reference_points;
}
void PosePredictor::PrepareTaskMessageBus(pipeline::MessageBus& message_bus)
{
    message_bus.MakeMessage<mpmca::messages::ControllerOutputErrorWeightPlan<24>>(m_horizon);
    message_bus.MakeMessage<mpmca::messages::ControllerOutputReferencePlan<24>>(m_horizon);
}
void PosePredictor::CheckTaskMessageBusPrepared(const pipeline::MessageBus&)
{
}
void PosePredictor::UpdateMessageBus(pipeline::MessageBus& message_bus)
{
    message_bus.UpdateMessage<mpmca::messages::ControllerOutputErrorWeightPlan<24>>(
        [&](mpmca::messages::ControllerOutputErrorWeightPlan<24>& message) {
            const auto& predictor_output_error_weight_plan = GetOutputErrorWeightPlan();

            for (size_t i = 0; i < m_horizon.GetNMpc(); ++i)
                message.w_y_plan.at(i) = predictor_output_error_weight_plan.at(m_horizon.GetPredictionHorizonIndex(i));
        });

    message_bus.UpdateMessage<mpmca::messages::ControllerOutputReferencePlan<24>>(
        [&](mpmca::messages::ControllerOutputReferencePlan<24>& message) {
            const auto& predictor_output_reference_plan = GetPredictionOutputVectorPlan();

            for (size_t i = 0; i < m_horizon.GetNMpc(); ++i)
                message.y_ref_plan.at(i) = predictor_output_reference_plan.at(m_horizon.GetPredictionHorizonIndex(i));
        });
}
void PosePredictor::ReadMessageBus(pipeline::MessageBus& message_bus)
{
}
const std::vector<std::string>& PosePredictor::GetPredictorSignalNames() const
{
    return m_all_signal_names;
}
const std::vector<std::string>& PosePredictor::GetOutputSignalNames() const
{
    return m_output_signal_names;
}
const std::vector<PoseRotMatVector>& PosePredictor::GetPredictionOutputVectorPlan() const
{
    return m_reference_output_plan;
}
const std::vector<PoseRotMatVector>& PosePredictor::GetOutputErrorWeightPlan() const
{
    return m_output_error_weight_plan;
}
void PosePredictor::SetActualSimulatorOutput(PoseRotMatVector const& y_simulator)
{
    m_y_simulator = y_simulator;
}
void PosePredictor::SetCurrentTimeMs(int64_t t)
{
    m_t_current_ms = t;
}
void PosePredictor::Prepare()
{
}
void PosePredictor::PrepareTick()
{
}
bool PosePredictor::GetTerminate() const
{
    return false;
}
PosePredictor::~PosePredictor()
{
}
}  //namespace mpmca::predict::pose