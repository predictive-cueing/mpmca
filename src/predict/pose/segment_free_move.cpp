/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/pose/segment_free_move.hpp"

#include "mpmca/predict/pose/trajectory.hpp"

namespace mpmca::predict::pose {
SegmentFreeMove::SegmentFreeMove(const Trajectory& trajectory, utilities::ConfigurationPtr config)
    : SegmentFreeMove(trajectory, config->GetValue<std::string>("Name", "some_name"),
                      config->GetValue<int64_t>("TimeMoveMs", 1000))
{
}

SegmentFreeMove::SegmentFreeMove(const Trajectory& trajectory, const std::string& name, int64_t t_move_ms)
    : Segment(trajectory, name, t_move_ms)
    , m_t_move_ms(CheckTimeIsDivisible(t_move_ms))
{
    m_type = std::string("FreeMove");
}

void SegmentFreeMove::Calculate(Segment const* previous_segment)
{
    int N = m_t_move_ms / m_dt_ms;

    if (!previous_segment)
        throw std::runtime_error("A FreeMove segment requires the previous segment to be set.");

    m_reference_pose_plan.resize(N, previous_segment->GetFinalReferencePose());
    m_output_error_weight_plan.resize(N, PoseRotMatVector::Zero());
}
}  //namespace mpmca::predict::pose