/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/pose/waypoint.hpp"

namespace mpmca::predict::pose {
Waypoint::Waypoint(std::string name, const PoseVector& pose)
    : m_name(name)
    , m_output_vector(pose.ToPoseRotMatVector())
    , m_pose_vector(pose)
{
}
const std::string& Waypoint::GetName() const
{
    return m_name;
}
const PoseRotMatVector& Waypoint::GetOutputVector() const
{
    return m_output_vector;
}
const PoseVector& Waypoint::GetPoseVector() const
{
    return m_pose_vector;
}
}  //namespace mpmca::predict::pose