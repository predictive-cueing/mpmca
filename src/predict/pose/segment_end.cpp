/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/pose/segment_end.hpp"

#include <limits>

#include "mpmca/json_tools.hpp"
#include "mpmca/predict/pose/trajectory.hpp"

namespace mpmca::predict::pose {
SegmentEnd::SegmentEnd(const Trajectory& trajectory, utilities::ConfigurationPtr config)
    : SegmentEnd(trajectory, config->GetValue<std::string>("Name", "some_name"),
                 config->GetValue<int64_t>("TimeFadeMs", 100),
                 trajectory.GetPoseFromRegisterOrConfig(config, "ReferencePose"),
                 config->GetValue<PoseRotMatVector>("OutputErrorWeight", PoseRotMatVector::Zero()))
{
}
SegmentEnd::SegmentEnd(const Trajectory& trajectory, const std::string& name, int64_t t_fade_ms,
                       PoseVector reference_pose_end, PoseRotMatVector output_error_weight_end)
    : Segment(trajectory, name, t_fade_ms)
    , m_t_fade_ms(CheckTimeIsDivisible(t_fade_ms))
    , m_reference_pose_end(reference_pose_end)
    , m_output_error_weight_end(output_error_weight_end)
{
    m_type = std::string("End");
}
void SegmentEnd::Calculate(Segment const* previous_segment)
{
    int N = m_t_fade_ms / m_dt_ms;

    if (N <= 1)
        throw std::runtime_error("Moving time (" + std::to_string(m_t_fade_ms) +
                                 " ms) should be at least 2 times the sample time (" + std::to_string(m_dt_ms) +
                                 " ms).");

    m_reference_pose_plan.resize(N, m_reference_pose_end);
    m_output_error_weight_plan.resize(N, PoseRotMatVector::Zero());

    for (int i = 0; i < N; ++i)
        m_output_error_weight_plan[i] = ((double)i / (double)(N - 1)) * m_output_error_weight_end;
}
const PoseRotMatVector& SegmentEnd::GetOutputErrorWeightAtLocalTimeMs(int64_t t_local_ms) const
{
    Segment::CheckTimeIsDivisible(t_local_ms);

    if (Segment::TimeIsWithinBounds(t_local_ms))
        return Segment::GetOutputErrorWeightAtLocalTimeMs(t_local_ms);
    else
        return m_output_error_weight_end;
}
const PoseVector& SegmentEnd::GetReferencePoseAtLocalTimeMs(int64_t t_local_ms) const
{
    Segment::CheckTimeIsDivisible(t_local_ms);

    if (Segment::TimeIsWithinBounds(t_local_ms))
        return Segment::GetReferencePoseAtLocalTimeMs(t_local_ms);
    else
        return m_reference_pose_end;
}
bool SegmentEnd::TimeIsWithinBounds(int64_t t_check) const
{
    return t_check >= 0;
}
int64_t SegmentEnd::GetTimeEndMs() const
{
    return std::numeric_limits<int64_t>::max();
}
}  //namespace mpmca::predict::pose