/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/pose/segment_start.hpp"

#include "mpmca/json_tools.hpp"
#include "mpmca/predict/pose/trajectory.hpp"

namespace mpmca::predict::pose {
SegmentStart::SegmentStart(const Trajectory& trajectory, utilities::ConfigurationPtr config)
    : SegmentStart(trajectory, config->GetValue<std::string>("Name", "some_name"),
                   config->GetValue<int64_t>("TimeMoveMs", 1000),
                   trajectory.GetPoseFromRegisterOrConfig(config, "ReferencePose"),
                   config->GetValue<PoseRotMatVector>("OutputErrorWeight", PoseRotMatVector::Zero()))
{
}

SegmentStart::SegmentStart(const Trajectory& trajectory, const std::string& name, int64_t t_move_ms,
                           const PoseVector& reference_pose, const PoseRotMatVector& output_error_weight)
    : Segment(trajectory, name, t_move_ms)
    , m_t_move_ms(CheckTimeIsDivisible(t_move_ms))
    , m_reference_pose(reference_pose)
    , m_output_error_weight(output_error_weight)
{
    m_type = std::string("Start");
}

void SegmentStart::Calculate(Segment const* previous_segment)
{
    if (previous_segment != nullptr)
        throw std::invalid_argument("A start trajectory segment cannot follow another segment.");

    int N = m_t_move_ms / m_dt_ms;

    if (N <= 1)
        throw std::runtime_error("Moving time (" + std::to_string(m_t_move_ms) +
                                 " ms) should be at least 2 times the sample time (" + std::to_string(m_dt_ms) +
                                 " ms).");

    auto zero_output_error_weight = PoseRotMatVector::Zero();

    m_reference_pose_plan.resize(N, m_reference_pose);
    m_output_error_weight_plan.resize(N, zero_output_error_weight);

    for (int i = 0; i < N; ++i) {
        double alpha = ((double)i / (double)(N - 1));
        m_output_error_weight_plan[i] = alpha * m_output_error_weight;
    }
}
const PoseVector& SegmentStart::GetReferencePose() const
{
    return m_reference_pose;
}
}  //namespace mpmca::predict::pose