/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/pose/waypoint_register.hpp"

namespace mpmca::predict::pose {
WaypointRegister::WaypointRegister()
{
}
void WaypointRegister::MakePoint(const std::string& name, const PoseVector& point)
{
    if (GetPointExists(name))
        throw std::invalid_argument("A point named " + name + " was already registered.");

    m_register[name] = std::make_shared<Waypoint>(name, point);
}
const PoseVector& WaypointRegister::GetPoseVector(const std::string& name) const
{
    if (!GetPointExists(name))
        throw std::invalid_argument("A point named " + name + " was not registered.");

    return m_register.at(name)->GetPoseVector();
}
const PoseRotMatVector& WaypointRegister::GetOutputVector(const std::string& name) const
{
    if (!GetPointExists(name))
        throw std::invalid_argument("A point named " + name + " was not registered.");

    return m_register.at(name)->GetOutputVector();
}
bool WaypointRegister::GetPointExists(const std::string& name) const
{
    return m_register.find(name) != m_register.end();
}
}  //namespace mpmca::predict::pose