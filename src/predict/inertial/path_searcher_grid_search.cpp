/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/path_searcher_grid_search.hpp"

namespace mpmca::predict::inertial {
const PathIndex& PathSearcherGridSearch::FindIndexLocal(PathPtr path, const Vector6& p)
{
    m_iterations = 0;
    double alpha = 2;
    double distance_traveled = alpha * m_last_query_position.Vector2::GetDistance(p);
    int distance_traveled_index =
        std::max(5, (int)(distance_traveled / path->GetSmallestApproximatePointToPointDistance()));

    m_last_path_index.GetIndex(SearchAllFromTo(path, p, m_last_path_index.GetIndex() - distance_traveled_index,
                                               m_last_path_index.GetIndex() + distance_traveled_index));
    m_last_path_index.GetIndex(path->GetPathPointAt(m_last_path_index.GetIndex()).GetIndex());
    m_last_path_index.GetFraction(path->GetPathPointAt(m_last_path_index.GetIndex()).GetFraction(p));
    m_last_query_position = p;

    return m_last_path_index;
}
const PathIndex& PathSearcherGridSearch::FindIndexGlobal(PathPtr path, const Vector6& p)
{
    m_iterations = 0;
    m_last_path_index.GetIndex(SearchAllFromTo(path, p, path->GetStartIndex(), path->GetEndIndex()));
    m_last_path_index.GetIndex(path->GetPathPointAt(m_last_path_index.GetIndex()).GetIndex());
    m_last_path_index.GetFraction(path->GetPathPointAt(m_last_path_index.GetIndex()).GetFraction(p));
    m_last_query_position = p;

    return m_last_path_index;
}
}  //namespace mpmca::predict::inertial