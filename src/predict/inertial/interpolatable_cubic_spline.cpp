/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/interpolatable_cubic_spline.hpp"

#include <algorithm>
#include <stdexcept>

namespace mpmca::predict::inertial {
InterpolatableCubicSpline::InterpolatableCubicSpline(double value)
    : Interpolatable(value)
    , m_has_previous(false)
    , m_has_next(false)
{
    m_values.fill(value);
    m_dss.fill(0);
    m_was_set.fill(false);
}
void InterpolatableCubicSpline::SetValue(double value)
{
    m_value = value;
    m_values = {{value}};
    m_dss = {{0}};
    m_was_set = {{false}};
    m_behind.p0 = value;
    m_behind.p1 = value;
    m_ahead.p0 = value;
    m_ahead.p1 = value;
    m_ready = false;
}
double InterpolatableCubicSpline::GetInterpolated(double f) const
{
    SplineParameters p;
    if (f < 0.0) {
        f = 1.0 + f;
        p = m_behind;
    }
    else
        p = m_ahead;

    /**
     * h00  =  2*t^3 - 3*t^2 +   + 1;
     * h10  =    t^3 - 2*t^2 + t
     * h01  = -2*t^3 + 3*t^2
     * h11  =    t^3 -   t^2
     * v    = p0*h00 + m0*h10 + p1*h01 + m1*h11;
     */

    const double f2 = f * f;
    const double f3 = f2 * f;
    const double h00 = 2.0 * f3 - 3.0 * f2 + 1.0;
    const double h10 = f3 - 2.0 * f2 + f;
    const double h01 = -2.0 * f3 + 3.0 * f2;
    const double h11 = f3 - f2;

    return p.p0 * h00 + p.m0 * h10 + p.p1 * h01 + p.m1 * h11;
}
double InterpolatableCubicSpline::Derivative(double f) const
{
    SplineParameters p;
    if (f < 0.0) {
        f = 1.0 + f;
        p = m_behind;
    }
    else
        p = m_ahead;

    const double f2 = f * f;

    const double h00 = 6.0 * f2 - 6.0 * f;
    const double h10 = 3.0 * f2 - 4.0 * f + 1.0;
    const double h01 = -6.0 * f2 + 6.0 * f;
    const double h11 = 3.0 * f2 - 2.0 * f;

    return (p.p0 * h00 + p.m0 * h10 + p.p1 * h01 + p.m1 * h11) / p.ds;
}
void InterpolatableCubicSpline::GetAdjacentPoint(int index_difference, double ds, const Interpolatable& other)
{
    if (index_difference < -2)
        return;

    if (index_difference > 2)
        return;

    if (index_difference == 0)
        throw std::runtime_error("A point with index_different == 0 is not an GetAdjacentPoint(). Use self().");

    if (index_difference > 0)
        --index_difference;

    if (m_was_set[index_difference + 2])
        throw std::runtime_error("The data for point " + std::to_string(index_difference) + " was already provided.");

    m_values[index_difference + 2] = other.GetValue();
    m_dss[index_difference + 2] = ds;
    m_was_set[index_difference + 2] = true;

    m_ready = false;
}
void InterpolatableCubicSpline::Finalize(std::function<double(const double&, const double&)> difference_function)
{
    const int far_behind = 0;
    const int previous = 1;
    const int next = 2;
    const int far_ahead = 3;

    if (m_was_set[far_behind] && !m_was_set[previous])
        throw std::runtime_error("GetAdjacentPoint() was called for this-2, but not for this-1.");

    if (m_was_set[far_ahead] && !m_was_set[next])
        throw std::runtime_error("GetAdjacentPoint() was called for this+2, but not for this+1.");

    m_has_previous = m_was_set[previous];
    m_has_next = m_was_set[next];

    double p_A, p_B, p_C;
    double m_A, m_B1, m_B2, m_C;
    double ds_A, ds_C;

    p_B = m_value;

    if (m_was_set[previous])
        p_A = m_value + difference_function(m_values[previous], m_value);
    else
        p_A = m_value;

    if (m_was_set[next])
        p_C = m_value + difference_function(m_values[next], m_value);
    else
        p_C = m_value;

    if (m_was_set[far_behind] && m_was_set[previous]) {
        m_A = difference_function(m_value, m_values[far_behind]) / m_dss[far_behind] * m_dss[previous];
        ds_A = m_dss[previous];
    }
    else if (!m_was_set[far_behind] && m_was_set[previous]) {
        m_A = difference_function(m_value, m_values[previous]) / m_dss[previous] * m_dss[previous];
        ds_A = m_dss[previous];
    }
    else if (!m_was_set[far_behind] && !m_was_set[previous]) {
        m_A = 0;
        ds_A = 1;
    }
    else
        throw std::runtime_error("I do not know how to compute m_A.");

    if (m_was_set[far_ahead] && m_was_set[next]) {
        m_C = difference_function(m_values[far_ahead], m_value) / m_dss[far_ahead] * m_dss[next];
        ds_C = m_dss[next];
    }
    else if (!m_was_set[far_ahead] && m_was_set[next]) {
        m_C = difference_function(m_values[next], m_value) / m_dss[next] * m_dss[next];
        ds_C = m_dss[next];
    }
    else if (!m_was_set[far_ahead] && !m_was_set[next]) {
        m_C = 0;
        ds_C = 1;
    }
    else
        throw std::runtime_error("I do not know how to compute m_C.");

    if (m_was_set[previous] && m_was_set[next]) {
        m_B1 =
            difference_function(m_values[next], m_values[previous]) / (m_dss[next] + m_dss[previous]) * m_dss[previous];
        m_B2 = difference_function(m_values[next], m_values[previous]) / (m_dss[next] + m_dss[previous]) * m_dss[next];
    }
    else if (!m_was_set[previous] && m_was_set[next]) {
        m_B1 = 0;
        m_B2 = difference_function(m_values[next], m_value) / m_dss[next] * m_dss[next];
    }
    else if (m_was_set[previous] && !m_was_set[next]) {
        m_B1 = difference_function(m_value, m_values[previous]) / m_dss[previous] * m_dss[previous];
        m_B2 = 0;
    }
    else
        throw std::runtime_error("I do not know how to compute m_B.");

    m_behind.p0 = p_A;
    m_behind.p1 = p_B;
    m_behind.m0 = m_A;
    m_behind.m1 = m_B1;
    m_behind.ds = ds_A;

    m_ahead.p0 = p_B;
    m_ahead.p1 = p_C;
    m_ahead.m0 = m_B2;
    m_ahead.m1 = m_C;
    m_ahead.ds = ds_C;

    m_ready = true;
}
}  //namespace mpmca::predict::inertial