/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/car_minimal.hpp"

#include <iostream>

namespace mpmca::predict::inertial {

CarMinimal::CarMinimal(utilities::ConfigurationPtr config, const utilities::Horizon& horizon, const std::string& name,
                       bool is_for_control)
    : CarMinimal(horizon, name, is_for_control)
{
}

CarMinimal::CarMinimal(const utilities::Horizon& t, const std::string& name, bool is_for_control)
    : Model(name, t, is_for_control)
    , x(this, "world_X_vehicle_m")
    , y(this, "world_Y_vehicle_m")
    , z(this, "world_Z_vehicle_m")
    , phi(this, "world_phi_vehicle_rad")
    , theta(this, "world_theta_vehicle_rad")
    , psi(this, "world_psi_vehicle_rad")
    , p(this, "body_p_vehicle_rad_s")
    , q(this, "body_q_vehicle_rad_s")
    , r(this, "body_r_vehicle_rad_s")
    , pd(this, "body_p_dot_vehicle_rad_s2")
    , qd(this, "body_q_dot_vehicle_rad_s2")
    , rd(this, "body_r_dot_vehicle_rad_s2")
    , a_x(this, "body_a_x_vehicle_m_s2")
    , a_y(this, "body_a_y_vehicle_m_s2")
    , a_z(this, "body_a_z_vehicle_m_s2")
    , u(this, "body_u_vehicle_m_s")
    , v(this, "body_v_vehicle_m_s")
    , w(this, "body_w_vehicle_m_s")
    , beta(this, "car_beta_vehicle_rad")
    , gear(this, "car_transmission_gear_integer")
    , delta_f(this, "car_delta_f_rad")
    , a_x_dot(this, "body_a_x_dot_vehicle_m_s3")
    , omega_engine(this, "car_engine_rotational_velocity_rad_s")
    , T_engine(this, "car_engine_torque_Nm")
    , disturbance_f_z(this, "disturbance_f_z_m_s2")
    , car_C_distance_rear_axle_m("car_C_distance_rear_axle_m", Model::GetName() + ":car_C_distance_rear_axle_m")
{
}

void CarMinimal::CalculateBeta(int j)
{
    // because u could potentially become 0 and we need to avoid division by 0, we max u at 1 m/s
    // this should not be a problem because if u is smaller than 1, then probably also r is very small and the beta
    // component in the World integration step is rather small.
    beta.SetValue(j, std::asin(std::min(
                         1., std::max(-1., r.GetValue(j) * car_C_distance_rear_axle_m.GetValue(GetCurrentTimeMs()) /
                                               std::max(1., u.GetValue(j))))));
}

void CarMinimal::ExtrapolateDisturbances(int j)
{
    if (!IsLastStepInHorizon(j))
        disturbance_f_z.SetValue(j + 1, disturbance_f_z.GetValue(j));
}
}  //namespace mpmca::predict::inertial
