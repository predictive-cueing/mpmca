/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/path_searcher.hpp"

#include "mpmca/predict/inertial/path_segment.hpp"
#include "mpmca/utilities/math.hpp"
namespace mpmca::predict::inertial {
const PathIndex& PathSearcher::GetLastPathIndex() const
{
    return m_last_path_index;
}
void PathSearcher::CopyState(const PathSearcher& other)
{
    m_last_path_index = other.m_last_path_index;
    m_last_query_position = other.m_last_query_position;
}
int PathSearcher::SearchAllFromTo(PathPtr path, const Vector6& p, int from, int to)
{
    from = path->ProtectIndexSilent(from);
    to = path->ProtectIndexSilent(to);

    int index = from;
    double min_dist = p.Vector2::GetDistanceSquared(path->GetPathPointAt(from));

    for (int i = from + 1; i <= to; ++i) {
        double distance = p.Vector2::GetDistanceSquared(path->GetPathPointAt(i));

        if (distance < min_dist) {
            index = i;
            min_dist = distance;
        }
    }

    m_iterations += to - from;
    return path->GetPathPointAt(index).GetIndex();
}
int PathSearcher::GetNumIterations() const
{
    return m_iterations;
}
}  //namespace mpmca::predict::inertial