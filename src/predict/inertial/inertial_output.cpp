/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/inertial_output.hpp"

#include <iostream>

#include "mpmca/data_names.hpp"

namespace mpmca::predict::inertial {
InertialOutput::InertialOutput(utilities::ConfigurationPtr config, const utilities::Horizon& horizon,
                               const std::string& name, bool is_for_control)
    : InertialOutput(horizon, name, is_for_control)
{
}

InertialOutput::InertialOutput(const utilities::Horizon& horizon, const std::string& name, bool is_for_control)
    : Model(name, horizon, is_for_control)
    , gravity_m_s2(DN::gravity().GetName(), Model::GetName() + ":" + DN::gravity().GetName(), 9.81)
    , f_x(this, DN::f_x_head().GetName())
    , f_y(this, DN::f_y_head().GetName())
    , f_z(this, DN::f_z_head().GetName(), gravity_m_s2.GetValue(0))
    , omega_x(this, DN::omega_x_head().GetName())
    , omega_y(this, DN::omega_y_head().GetName())
    , omega_z(this, DN::omega_z_head().GetName())
    , alpha_x(this, DN::alpha_x_head().GetName())
    , alpha_y(this, DN::alpha_y_head().GetName())
    , alpha_z(this, DN::alpha_z_head().GetName())
{
}

}  //namespace mpmca::predict::inertial
