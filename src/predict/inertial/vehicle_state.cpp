/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/vehicle_state.hpp"

#include <cmath>

namespace mpmca::predict::inertial {
VehicleState::VehicleState(double x, double y, double z, double phi, double theta, double psi, double u, double v,
                           double w, double p, double q, double r, double a_x, double a_y, double a_z, double pd,
                           double qd, double rd, double j_x, double j_y, double j_z, double pdd, double qdd, double rdd)
    : Vector6(x, y, z, phi, theta, psi)
    , m_u(u)
    , m_v(v)
    , m_w(w)
    , m_p(p)
    , m_q(q)
    , m_r(r)
    , m_a_x(a_x)
    , m_a_y(a_y)
    , m_a_z(a_z)
    , m_pd(pd)
    , m_qd(qd)
    , m_rd(rd)
    , m_j_x(j_x)
    , m_j_y(j_y)
    , m_j_z(j_z)
    , m_pdd(pdd)
    , m_qdd(qdd)
    , m_rdd(rdd)
{
}

std::ostream &operator<<(std::ostream &out, const VehicleState &c)  //
{
    out << " x: " << c.GetX() << " y: " << c.GetY() << " z: " << c.GetZ() << " phi: " << c.GetPhi()
        << " theta: " << c.GetTheta() << " psi: " << c.GetPsi() << " u: " << c.GetU() << " v: " << c.GetV()
        << " w: " << c.GetW() << " p: " << c.GetP() << " q: " << c.GetQ() << " r: " << c.GetR() << " a_x: " << c.GetAx()
        << " a_y: " << c.GetAy() << " a_z: " << c.GetAz() << " pd: " << c.GetPd() << " qd: " << c.GetQd()
        << " rd: " << c.GetRd() << " j_x: " << c.GetJx() << " j_y: " << c.GetJy() << " j_z: " << c.GetJz()
        << " pdd: " << c.GetPdd() << " qdd: " << c.GetQdd() << " rdd: " << c.GetRdd();
    return out;
}

VehicleState VehicleState::PredictPosition(double tau) const
{
    double u_end = m_u + m_a_x * tau;
    double v_end = m_v + m_a_y * tau;
    double w_end = m_w + m_a_z * tau;

    double p_end = m_p + m_pd * tau;
    double q_end = m_q + m_qd * tau;
    double r_end = m_r + m_rd * tau;

    double u_mean = m_u + m_a_x * tau / 2.;
    double v_mean = m_v + m_a_y * tau / 2.;
    double w_mean = m_w + m_a_z * tau / 2.;
    double r_mean = m_r + m_rd * tau / 2.;

    double phi_end = m_phi + m_p * tau;
    double theta_end = m_theta + m_q * tau;
    double psi_end = m_psi + r_mean * tau;

    double psi_mean = (psi_end + m_psi) / 2.;

    return VehicleState(m_x + u_mean * tau * std::cos(psi_mean) - v_mean * tau * std::sin(psi_mean),
                        m_y + u_mean * tau * std::sin(psi_mean) + v_mean * tau * std::cos(psi_mean), m_z + w_mean * tau,
                        phi_end, theta_end, psi_end, u_end, v_end, w_end, p_end, q_end, r_end, m_a_x, m_a_y, m_a_z,
                        m_pd, m_qd, m_rd, m_j_x, m_j_y, m_j_z, m_pdd, m_qdd, m_rdd);
}

double VehicleState::GetU() const
{
    return m_u;
}
double VehicleState::GetV() const
{
    return m_v;
}
double VehicleState::GetW() const
{
    return m_w;
}
double VehicleState::GetP() const
{
    return m_p;
}
double VehicleState::GetQ() const
{
    return m_q;
}
double VehicleState::GetR() const
{
    return m_r;
}
double VehicleState::GetAx() const
{
    return m_a_x;
}
double VehicleState::GetAy() const
{
    return m_a_y;
}
double VehicleState::GetAz() const
{
    return m_a_z;
}
double VehicleState::GetPd() const
{
    return m_pd;
}
double VehicleState::GetQd() const
{
    return m_qd;
}
double VehicleState::GetRd() const
{
    return m_rd;
}
double VehicleState::GetJx() const
{
    return m_j_x;
}
double VehicleState::GetJy() const
{
    return m_j_y;
}
double VehicleState::GetJz() const
{
    return m_j_z;
}
double VehicleState::GetPdd() const
{
    return m_pdd;
}
double VehicleState::GetQdd() const
{
    return m_qdd;
}
double VehicleState::GetRdd() const
{
    return m_rdd;
}
void VehicleState::SetU(double i)
{
    m_u = i;
}
void VehicleState::SetV(double i)
{
    m_v = i;
}
void VehicleState::SetW(double i)
{
    m_w = i;
}
void VehicleState::SetP(double i)
{
    m_p = i;
}
void VehicleState::SetQ(double i)
{
    m_q = i;
}
void VehicleState::SetR(double i)
{
    m_r = i;
}
void VehicleState::SetAx(double i)
{
    m_a_x = i;
}
void VehicleState::SetAy(double i)
{
    m_a_y = i;
}
void VehicleState::SetAz(double i)
{
    m_a_z = i;
}
void VehicleState::SetPd(double i)
{
    m_pd = i;
}
void VehicleState::SetQd(double i)
{
    m_qd = i;
}
void VehicleState::SetRd(double i)
{
    m_rd = i;
}
void VehicleState::SetJx(double i)
{
    m_j_x = i;
}
void VehicleState::SetJy(double i)
{
    m_j_y = i;
}
void VehicleState::SetJz(double i)
{
    m_j_z = i;
}
void VehicleState::SetPdd(double i)
{
    m_pdd = i;
}
void VehicleState::SetQdd(double i)
{
    m_qdd = i;
}
void VehicleState::SetRdd(double i)
{
    m_rdd = i;
}
}  //namespace mpmca::predict::inertial