/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/fraction_circle.hpp"

#include <cmath>

#include "mpmca/utilities/math.hpp"

namespace mpmca::predict::inertial {
FractionCircle::FractionCircle(const PathPoint& p)
    : PathPointInterpolatable<InterpolatableCubicSpline>(p)
    , m_circle_center(0, 0)
    , m_vector_prev(0, 0)
    , m_vector_next(0, 0)
    , m_has_next(false)
    , m_has_previous(false)
    , m_has_circle(false)
    , m_psi_next(0)
    , m_psi_prev(0)
    , m_psi_mid(0)
    , m_psi_next_prev_diff(0)
    , m_ds_prev(0)
    , m_ds_next(0)
    , m_s_circle(0)
{
    m_points.fill({0, 0});
    m_was_set.fill(false);
}

double FractionCircle::GetFraction(const Vector2& p) const
{
    if (m_has_circle) {
        double psi_here = std::atan2(p.GetY() - m_circle_center.GetY(), p.GetX() - m_circle_center.GetX());

        if (GetAngleIsBetween(m_psi_prev, m_psi_mid, psi_here)) {
            return CheckFraction(utilities::Math::GetAngleDiff(psi_here, m_psi_mid) / (m_psi_next_prev_diff / 2.0) *
                                 m_s_circle / m_ds_prev);
        }
        else if (GetAngleIsBetween(m_psi_mid, m_psi_next, psi_here)) {
            return CheckFraction(utilities::Math::GetAngleDiff(psi_here, m_psi_mid) / (m_psi_next_prev_diff / 2.0) *
                                 m_s_circle / m_ds_next);
        }
        else {
            return CheckFraction(GetFractionNoCircle(p));
        }
    }
    else
        return CheckFraction(GetFractionNoCircle(p));
}
double FractionCircle::CheckFraction(double fraction) const
{
    if (!m_has_previous && fraction < 0)
        throw std::runtime_error("The vehicle is not located within the range of the path. Fraction would be " +
                                 std::to_string(fraction) + " and should be positive.");

    if (!m_has_next && fraction > 0)
        throw std::runtime_error("The vehicle is not located within the range of the path. Fraction would be " +
                                 std::to_string(fraction) + " and should be negative.");

    return fraction;
}
double FractionCircle::GetFractionNoCircle(const Vector2& p) const
{
    if (m_has_previous && m_has_next) {
        double projection_prev = m_vector_prev.Vector2::GetDotProductWith(p - m_points[1]) / m_ds_prev;

        if (projection_prev > 0)
            return -projection_prev / m_ds_prev;
        else
            return m_vector_next.Vector2::GetDotProductWith(p - m_points[1]) / m_ds_next / m_ds_next;
    }
    else if (m_has_previous) {
        return -1 * m_vector_prev.Vector2::GetDotProductWith(p - m_points[1]) / m_ds_prev / m_ds_prev;
    }
    else {
        return m_vector_next.Vector2::GetDotProductWith(p - m_points[1]) / m_ds_next / m_ds_next;
    }
}
bool FractionCircle::GetAngleIsBetween(double psi1, double psi2, double psi_test)
{
    double to2 = utilities::Math::GetAngleDiff(psi2, psi1);
    double to_test = utilities::Math::GetAngleDiff(psi_test, psi1);

    if (to2 >= 0)
        return to_test <= to2 && to_test >= 0.0;
    else
        return to_test >= to2 && to_test <= 0.0;
}
void FractionCircle::CalculateCircle()
{
    m_has_previous = m_was_set[0];
    m_has_next = m_was_set[2];

    if (m_has_previous) {
        m_ds_prev = m_points[1].Vector2::GetDistance(m_points[0]);
        m_vector_prev = m_points[0] - m_points[1];
    }

    if (m_has_next) {
        m_vector_next = m_points[2] - m_points[1];
        m_ds_next = m_points[1].Vector2::GetDistance(m_points[2]);
    }

    if (!std::all_of(m_was_set.cbegin(), m_was_set.cend(), [](const bool& r) { return r; })) {
        m_has_circle = false;
        return;
    }

    m_s_circle = std::min(m_ds_prev / 2.0, m_ds_next / 2.0);

    Vector2 mid_prev = m_points[1].MoveDistance(m_points[0], m_s_circle);
    Vector2 mid_next = m_points[1].MoveDistance(m_points[2], m_s_circle);

    double alpha =
        utilities::Math::GetAngleDiff(m_points[1].GetHeadingFrom(m_points[2]), m_points[0].GetHeadingFrom(m_points[1]));

    m_has_circle = std::abs(alpha) > 1e-8;

    if (!m_has_circle)
        return;

    double beta = (m_points[1] - m_points[0]).GetHeading();
    double R = std::abs(m_s_circle / std::tan(alpha / 2.0));

    m_circle_center = mid_prev + Vector2(-sin(beta) * R * utilities::Math::Sign(alpha),
                                         std::cos(beta) * R * utilities::Math::Sign(alpha));

    m_psi_next = (mid_next - m_circle_center).GetHeading();
    m_psi_prev = (mid_prev - m_circle_center).GetHeading();
    m_psi_next_prev_diff = utilities::Math::GetAngleDiff(m_psi_next, m_psi_prev);
    m_psi_mid = m_psi_prev + m_psi_next_prev_diff / 2.0;
}
void FractionCircle::Finalize()
{
    CalculateCircle();

    if (!m_has_next && !m_has_previous)
        throw std::runtime_error("This point has no previous and no next points. This cannot work.");
}
}  //namespace mpmca::predict::inertial