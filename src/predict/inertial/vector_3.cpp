/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/vector_3.hpp"

#include <cmath>

#include "mpmca/utilities/math.hpp"

namespace mpmca::predict::inertial {
Vector3::Vector3()
    : Vector3(0, 0, 0)
{
}
Vector3::Vector3(double x, double y, double z)
    : Vector2(x, y)
    , m_z(z)
{
}
double Vector3::GetZ() const
{
    return m_z;
}
void Vector3::SetZ(double i)
{
    m_z = i;
}
double Vector3::GetDistance(const Vector3& other) const
{
    return std::sqrt(GetDistanceSquared(other));
}
double Vector3::GetDistanceSquared(const Vector3& other) const
{
    double dx = (m_x - other.GetX());
    double dy = (m_y - other.GetY());
    double dz = (m_z - other.GetZ());
    return dx * dx + dy * dy + dz * dz;
}
Vector3 Vector3::operator-(const Vector3& other) const
{
    return Vector3(m_x - other.GetX(), m_y - other.GetY(), m_z - other.GetZ());
}
Vector3 Vector3::operator+(const Vector3& other) const
{
    return Vector3(m_x + other.GetX(), m_y + other.GetY(), m_z + other.GetZ());
}
Vector3 Vector3::operator*(double mult) const
{
    return Vector3(m_x * mult, m_y * mult, m_z * mult);
}
Vector3 Vector3::GetMeanWith(const Vector3& other) const
{
    return Vector3(m_x / 2.0 + other.GetX() / 2.0, m_y / 2.0 + other.GetY() / 2.0, m_z / 2.0 + other.GetZ() / 2.0);
}
std::ostream& operator<<(std::ostream& out, const Vector3& c)  //
{
    out << " x: " << c.GetX() << " y: " << c.GetY() << " z: " << c.GetZ();
    return out;
}
}  //namespace mpmca::predict::inertial