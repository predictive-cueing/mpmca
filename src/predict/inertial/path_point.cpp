/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/path_point.hpp"

#include <cmath>

#include "mpmca/utilities/math.hpp"
#include "mpmca/utilities/value_checking.hpp"

namespace mpmca::predict::inertial {
PathPoint::PathPoint()
    : PathPoint(0, 0, 0, 0, 0, 0)
{
}
PathPoint::PathPoint(double x, double y)
    : PathPoint(x, y, 0, 0, 0, 0)
{
}
PathPoint::PathPoint(double x, double y, double z, double phi, double theta, double psi)
    : Vector6(x, y, z, phi, theta, psi)
{
}
PathPoint::PathPoint(const std::array<double, 6> &mpc_vector)
    : Vector6(mpc_vector[0], mpc_vector[1], mpc_vector[2], mpc_vector[3], mpc_vector[4], mpc_vector[5])
{
}
double PathPoint::GetDxDs() const
{
    return m_dx_ds;
}
double PathPoint::GetDyDs() const
{
    return m_dy_ds;
}
double PathPoint::GetDzDs() const
{
    return m_dz_ds;
}
double PathPoint::GetDPhiDs() const
{
    return m_d_phi_ds;
}
double PathPoint::GetDThetaDs() const
{
    return m_d_theta_ds;
}
double PathPoint::GetDPsiDs() const
{
    return m_d_psi_ds;
}
double PathPoint::GetUMean() const
{
    return m_u_mean;
}
double PathPoint::GetUStandardDeviation() const
{
    return m_u_standard_deviation;
}
double PathPoint::GetRoadWidth() const
{
    return m_road_width;
}
/**
 * 0 : w
 * 1 : x
 * 2 : y
 * 3 : z
 */
Vector<4> PathPoint::GetOrientationQuaternion() const
{
    return utilities::Math::EulerToQuaternion(m_phi, m_theta, m_psi);
}
double PathPoint::GetDuMeanDs() const
{
    return m_du_mean_ds;
}
double PathPoint::GetDuStandardDeviationDs() const
{
    return m_du_standard_deviation_ds;
}
int PathPoint::GetIndex() const
{
    return m_idx;
}
std::string PathPoint::ToString() const
{
    return std::string(" Idx: " + std::to_string(GetIndex()) + " x: " + std::to_string(GetX()) +
                       " y: " + std::to_string(GetY()) + " z: " + std::to_string(GetZ()) +
                       " phi: " + std::to_string(GetPhi()) + " theta: " + std::to_string(GetTheta()) +
                       " psi: " + std::to_string(GetPsi()) + " u_mean: " + std::to_string(GetUMean()) +
                       " u_standard_deviation: " + std::to_string(GetUStandardDeviation()));
}
std::ostream &operator<<(std::ostream &out, const PathPoint &c)  //
{
    out << c.ToString();
    return out;
}
void PathPoint::CheckValues() const
{
    utilities::ValueChecking::CheckAll(m_x, "PathPoint.x,");
    utilities::ValueChecking::CheckAll(m_y, "PathPoint.y,");
    utilities::ValueChecking::CheckAll(m_z, "PathPoint.z,");
    utilities::ValueChecking::CheckAll(m_phi, "PathPoint.phi");
    utilities::ValueChecking::CheckAll(m_theta, "PathPoint.theta");
    utilities::ValueChecking::CheckAll(m_psi, "PathPoint.psi");
    utilities::ValueChecking::CheckAll(m_dx_ds, "PathPoint.dx_ds");
    utilities::ValueChecking::CheckAll(m_dy_ds, "PathPoint.dy_ds");
    utilities::ValueChecking::CheckAll(m_dz_ds, "PathPoint.dz_ds");
    utilities::ValueChecking::CheckAll(m_d_phi_ds, "PathPoint.d_phi_ds");
    utilities::ValueChecking::CheckAll(m_d_theta_ds, "PathPoint.d_theta_ds");
    utilities::ValueChecking::CheckAll(m_d_psi_ds, "PathPoint.d_psi_ds");

    utilities::ValueChecking::CheckAll(m_u_mean, "PathPoint.u_mean");
    utilities::ValueChecking::CheckAll(m_u_standard_deviation, "PathPoint.u_standard_deviation");

    utilities::ValueChecking::CheckAll(m_du_mean_ds, "PathPoint.du_mean_ds");
    utilities::ValueChecking::CheckAll(m_du_standard_deviation_ds, "PathPoint.du_standard_deviation_ds");

    if (m_u_standard_deviation <= 0)
        throw std::invalid_argument("PathPoint.u_standard_deviation <= 0.");
}
void PathPoint::SetDxDs(double i)
{
    m_dx_ds = i;
}
void PathPoint::SetDyDs(double i)
{
    m_dy_ds = i;
}
void PathPoint::SetDzDs(double i)
{
    m_dz_ds = i;
}
void PathPoint::SetDPhiDs(double i)
{
    m_d_phi_ds = i;
}
void PathPoint::SetDThetaDs(double i)
{
    m_d_theta_ds = i;
}
void PathPoint::SetDPsiDs(double i)
{
    m_d_psi_ds = i;
}
void PathPoint::SetUMean(double i)
{
    m_u_mean = i;
}
void PathPoint::SetUStandardDeviation(double i)
{
    m_u_standard_deviation = i;
}
void PathPoint::SetRoadWidth(double i)
{
    m_road_width = i;
}
void PathPoint::SetIndex(int i)
{
    m_idx = i;
}
void PathPoint::SetDuMeanDs(double i)
{
    m_du_mean_ds = i;
}
void PathPoint::SetDuStandardDeviationDs(double i)
{
    m_du_standard_deviation_ds = i;
}
void PathPoint::SetDRoadWidthDs(double i)
{
    m_d_road_width_ds = i;
}
}  //namespace mpmca::predict::inertial