/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/automotive_predictor.hpp"

#include "mpmca/messages/automotive_prediction_horizon.hpp"
#include "mpmca/messages/automotive_prediction_parameters.hpp"
#include "mpmca/messages/automotive_prediction_path.hpp"
#include "mpmca/messages/automotive_signals.hpp"
#include "mpmca/messages/controller_output_reference_plan.hpp"
#include "mpmca/messages/inertial_head_signals.hpp"
#include "mpmca/messages/vehicle_state_signals.hpp"
#include "mpmca/predict/inertial/path_reader.hpp"

namespace mpmca::predict::inertial {

AutomotivePredictor::AutomotivePredictor(utilities::ConfigurationPtr config, const utilities::Horizon& horizon,
                                         const std::string& name)
    : m_logger(name, "Predictor" + name)
    , m_sim(config, horizon, name, SimulationControlOption::kForPrediction)
    , m_simulate_called(false)
{
}
void AutomotivePredictor::PrepareTaskMessageBus(pipeline::MessageBus& message_bus)
{
    message_bus.MakeMessage<messages::ControllerOutputReferencePlan<9>>(m_sim.GetHorizon());
    message_bus.MakeMessage<messages::AutomotivePredictionHorizon>(m_sim.GetHorizon());
}
void AutomotivePredictor::CheckTaskMessageBusPrepared(const pipeline::MessageBus& message_bus)
{
    message_bus.RequireMessage<messages::AutomotiveSignals>();
    message_bus.RequireMessage<messages::InertialHeadSignals>();
    message_bus.RequireMessage<messages::VehicleStateSignals>();
    message_bus.RequireMessage<messages::AutomotivePredictionParameters>();
    message_bus.RequireMessage<messages::AutomotivePredictionPath>();
}
void AutomotivePredictor::SetCurrentTimeMs(int64_t t)
{
    m_sim.InitTime(t);
}
void AutomotivePredictor::ReadMessageBus(pipeline::MessageBus& message_bus)
{
    message_bus.ReadUpdatedMessage<messages::AutomotiveSignals>(
        [&](const messages::AutomotiveSignals& message) { m_sim.ReadMessage(message); });
    message_bus.ReadUpdatedMessage<messages::InertialHeadSignals>(
        [&](const messages::InertialHeadSignals& message) { m_sim.ReadMessage(message); });
    message_bus.ReadUpdatedMessage<messages::VehicleStateSignals>(
        [&](const messages::VehicleStateSignals& message) { m_sim.ReadMessage(message); });

    message_bus.ReadMessageIfUpdated<messages::AutomotivePredictionParameters>(
        [&](const messages::AutomotivePredictionParameters& message) { m_sim.ReadMessage(message); });

    message_bus.ReadMessageIfUpdated<messages::AutomotivePredictionPath>(
        //
        [&](const messages::AutomotivePredictionPath& message) {
            if (message.path_ptr == nullptr) {
                throw std::runtime_error("The AutomotivePredictionPath message contained a nullptr path pointer.");
            }
            m_sim.SetPathPtr(message.path_ptr);
        });
}
void AutomotivePredictor::UpdateMessageBus(pipeline::MessageBus& message_bus)
{
    message_bus.UpdateMessage<messages::ControllerOutputReferencePlan<9>>(
        //
        [&](messages::ControllerOutputReferencePlan<9>& message) {
            if (m_simulate_called) {
                for (size_t i = 0; i < m_sim.GetHorizon().GetNMpc(); ++i) {
                    message.y_ref_plan.at(i) =
                        m_sim.GetOutputModel().GetPredictionPlanVector(m_sim.GetHorizon().GetPredictionHorizonIndex(i));
                }
            }
            else {
                for (size_t i = 0; i < m_sim.GetHorizon().GetNMpc(); ++i) {
                    message.y_ref_plan.at(i) = m_sim.GetOutputModel().GetNominalSignalVector();
                }
            }
        });

    message_bus.UpdateMessage<messages::AutomotivePredictionHorizon>(
        [&](messages::AutomotivePredictionHorizon& message) { m_sim.UpdateMessage(message); });
}
void AutomotivePredictor::PrepareTick()
{
    m_sim.PrepareTick();
}
void AutomotivePredictor::Predict()
{
    m_sim.Simulate();
    m_simulate_called = true;
}
bool AutomotivePredictor::GetTerminate() const
{
    return m_sim.GetEndOfPathReached();
}
}  //namespace mpmca::predict::inertial