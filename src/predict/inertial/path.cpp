/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/path.hpp"

#include "mpmca/predict/inertial/path_factory.hpp"
#include "mpmca/utilities/math.hpp"
namespace mpmca::predict::inertial {
Path::Path(const std::string& name, const std::vector<PathPointE>& path, bool closed)
    : m_name(name)
    , m_closed(closed)
    , m_path(path)
    , m_unique_id(GetNextId())
    , m_smallest_approximate_point_to_point_distance(0.0001)
{
}
Path::~Path()
{
}
double Path::GetSmallestApproximatePointToPointDistance() const
{
    return m_smallest_approximate_point_to_point_distance;
}
void Path::SetSmallestApproximatePointToPointDistance(double i)
{
    m_smallest_approximate_point_to_point_distance = i;
}
bool Path::IsClosed() const
{
    return m_closed;
}
int Path::GetUniqueId() const
{
    return m_unique_id;
}
int Path::GetStartIndex() const
{
    return 0;
}
size_t Path::GetNumPathPoints() const
{
    return m_path.size();
}
int Path::GetEndIndex() const
{
    return m_path.size() - 1;
}
int Path::ProtectIndex(int index) const
{
    int n_idx = ProtectIndexSilent(index);

    if (index != n_idx)
        throw std::runtime_error("The code requested a point at an index (" + std::to_string(index) +
                                 ") that does not exist in a path with " + std::to_string(GetNumPathPoints()) +
                                 " points.");

    return n_idx;
}
int Path::ProtectIndexSilent(int index) const
{
    if (m_closed)
        return index;
    else
        return std::clamp(index, GetStartIndex(), GetEndIndex());
}
const std::string& Path::GetName() const
{
    return m_name;
}
const PathPointE& Path::GetPathPointAt(int index) const
{
    if (m_closed)
        return m_path[utilities::Math::PositiveModulo(index, m_path.size())];
    else
        return m_path[ProtectIndex(index)];
}
const PathPointE& Path::GetPathPointAt(const PathIndex& p_index) const
{
    return GetPathPointAt(p_index.GetIndex());
}
PathPoint Path::GetInterpolated(const PathIndex& p_index) const
{
    return GetPathPointAt(p_index.GetIndex()).GetInterpolated(p_index.GetFraction());
}
bool Path::SetEndOfPathReached(const PathIndex& p) const
{
    return !m_closed && p.GetIndex() >= GetEndIndex();
}
}  //namespace mpmca::predict::inertial