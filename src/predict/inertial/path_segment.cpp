/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/path_segment.hpp"

#include <stdexcept>

namespace mpmca::predict::inertial {
PathSegment::PathSegment(int from, int to)
    : m_from(from)
    , m_to(to)
    , m_minimum_point_found(false)
    , m_all_points_evaluated(false)
    , m_minimum_index(-1)
    , m_minimum_distance(1e10)
    , m_distance_at_center(1e10)
{
    if (m_from > m_to)
        throw std::runtime_error("From should be smaller than to.");
}
bool PathSegment::GetAllPointsAreEvaluated() const
{
    return m_all_points_evaluated;
}
void PathSegment::SetAllPointsAreEvaluated(bool eval)
{
    m_all_points_evaluated = eval;
}
bool PathSegment::operator<(const PathSegment& other) const
{
    return m_minimum_distance < other.GetMinimumDistance();
}
int PathSegment::GetFrom() const
{
    return m_from;
}
int PathSegment::GetTo() const
{
    return m_to;
}
int PathSegment::GetCenter() const
{
    return m_from + (m_to - m_from) / 2;
}
int PathSegment::GetNumberOfPoints() const
{
    return m_to - m_from + 1;
}
double PathSegment::GetMinimumDistance() const
{
    return m_minimum_distance;
}
int PathSegment::GetMinimumIndex() const
{
    return m_minimum_index;
}
bool PathSegment::IsMinimumPointFound() const
{
    return m_minimum_point_found;
}
void PathSegment::IsMinimumPointFound(int min_index, double min_dist)
{
    m_minimum_point_found = true;
    m_minimum_index = min_index;
    m_minimum_distance = min_dist;
}
double PathSegment::GetDistanceAtCenter() const
{
    return m_distance_at_center;
}
void PathSegment::SetDistanceAtCenter(double dist_center)
{
    m_distance_at_center = dist_center;
}
}  //namespace mpmca::predict::inertial