/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/lateral_error.hpp"

namespace mpmca::predict::inertial {
double LateralError::GetSignOfSide(const Vector6& p) const
{
    double cross = m_dx_ds * (p.GetY() - m_y) - m_dy_ds * (p.GetX() - m_x);
    double res = (cross >= 0) - (cross < 0);
    return res;
}
double LateralError::GetSignOfSide(const PathPoint& path_point, const Vector6& point)
{
    double cross = path_point.GetDxDs() * (point.GetY() - path_point.GetY()) -
                   path_point.GetDyDs() * (point.GetX() - path_point.GetX());
    double res = (cross >= 0) - (cross < 0);
    return res;
}
double LateralError::GetLateralError(const Vector6& point) const
{
    return GetLateralError(GetFraction(point), point);
}
double LateralError::GetLateralError(double fraction, const Vector6& point) const
{
    auto pi = GetInterpolated(fraction);
    return GetSignOfSide(pi, point) * pi.Vector2::GetDistance(point);
}
double LateralError::GetLateralErrorDot(const VehicleState& point) const
{
    return GetLateralErrorDot(GetFraction(point), point);
}
double LateralError::GetLateralErrorDot(double fraction, const VehicleState& state) const
{
    auto pi = GetInterpolated(fraction);
    Vector2 path_vector{-pi.GetDyDs(), pi.GetDxDs()};
    Vector2 velocity_vector_world{state.GetU() * std::cos(state.GetPsi()) - state.GetV() * std::sin(state.GetPsi()),
                                  state.GetU() * std::sin(state.GetPsi()) + state.GetV() * std::cos(state.GetPsi())};

    return velocity_vector_world.GetDotProductWith(path_vector.GetUnitVector());
}
}  //namespace mpmca::predict::inertial