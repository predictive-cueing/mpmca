/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/path_loader.hpp"

#include "mpmca/predict/inertial/path.hpp"
#include "mpmca/predict/inertial/path_reader.hpp"

namespace mpmca::predict::inertial {

template <>
void PathLoader::GenerateOrLoadPath<Path>(const std::string& name, utilities::ConfigurationPtr config)
{
    auto path_source = config->GetValue<std::string>("PathSource", "File");

    if (path_source.compare("File") != 0)
        throw std::runtime_error("PathLoader::GenerateOrLoadPath does not support other options than 'file'.");

    PathReader::ReadPath<Path>(name, config->GetValue<std::string>("Filename", "data/test_path.json"));
}

PathLoader::PathLoader(utilities::ConfigurationPtr config)
{
    if (config->IsObject("PathLoader")) {
        auto loader_config = config->GetConfigurationObject("PathLoader");
        std::vector<std::string> names = loader_config->GetObjectNames();

        for (auto& name : names) {
            GenerateOrLoadPath<Path>(name, loader_config->GetConfigurationObject(name));
        }
    }
}
}  //namespace mpmca::predict::inertial