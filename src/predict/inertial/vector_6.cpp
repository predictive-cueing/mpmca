/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/vector_6.hpp"

#include <cmath>

#include "mpmca/utilities/math.hpp"

namespace mpmca::predict::inertial {
Vector6::Vector6()
    : Vector6(0, 0, 0, 0, 0, 0)
{
}
Vector6::Vector6(double x, double y)
    : Vector6(x, y, 0, 0, 0, 0)
{
}
Vector6::Vector6(double x, double y, double z, double phi, double theta, double psi)
    : Vector3(x, y, z)
    , m_phi(phi)
    , m_theta(theta)
    , m_psi(psi)
{
}
double Vector6::GetPhi() const
{
    return m_phi;
}
double Vector6::GetTheta() const
{
    return m_theta;
}
double Vector6::GetPsi() const
{
    return m_psi;
}
void Vector6::SetPhi(double i)
{
    m_phi = i;
}
void Vector6::SetTheta(double i)
{
    m_theta = i;
}
void Vector6::SetPsi(double i)
{
    m_psi = i;
}
Vector6 Vector6::operator*(double mult) const
{
    return Vector6(m_x * mult, m_y * mult, m_z * mult, m_phi * mult, m_theta * mult, m_psi * mult);
}
Vector6 Vector6::operator-(const Vector6& other) const
{
    return Vector6(m_x - other.GetX(), m_y - other.GetY(), m_z - other.GetZ(), m_phi - other.GetPhi(),
                   m_theta - other.GetTheta(), m_psi - other.GetPsi());
}
bool Vector6::IsClose(const Vector6& other, double translational_tolerance, double angular_tolerance) const
{
    return std::abs(other.GetX() - m_x) < translational_tolerance &&
           std::abs(other.GetY() - m_y) < translational_tolerance &&
           std::abs(other.GetZ() - m_z) < translational_tolerance &&
           std::abs(utilities::Math::GetAngleDiff(other.GetPhi(), m_phi)) < angular_tolerance &&
           std::abs(utilities::Math::GetAngleDiff(other.GetTheta(), m_theta)) < angular_tolerance &&
           std::abs(utilities::Math::GetAngleDiff(other.GetPsi(), m_psi)) < angular_tolerance;
}

Vector6 Vector6::operator+(const Vector6& other) const
{
    return Vector6(m_x + other.GetX(), m_y + other.GetY(), m_z + other.GetZ(), m_phi + other.GetPhi(),
                   m_theta + other.GetTheta(), m_psi + other.GetPsi());
}

Vector6 Vector6::GetMeanWith(const Vector6& other) const
{
    return Vector6(m_x / 2.0 + other.GetX() / 2.0, m_y / 2.0 + other.GetY() / 2.0, m_z / 2.0 + other.GetZ() / 2.0,
                   m_phi / 2.0 + other.GetPhi() / 2.0, m_theta / 2.0 + other.GetTheta() / 2.0,
                   m_psi / 2.0 + other.GetPsi() / 2.0);
}

std::ostream& operator<<(std::ostream& out, const Vector6& c)  //
{
    out << " x: " << c.GetX() << " y: " << c.GetY() << " z: " << c.GetZ() << " phi: " << c.GetPhi()
        << " theta: " << c.GetTheta() << " psi: " << c.GetPsi();
    return out;
}
}  //namespace mpmca::predict::inertial