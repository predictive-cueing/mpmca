/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/path_index.hpp"

namespace mpmca::predict::inertial {
PathIndex::PathIndex()
    : PathIndex(0, 0)
{
}
PathIndex::PathIndex(int64_t index, double fraction)
    : m_index(index)
    , m_fraction(fraction)
{
}

void PathIndex::operator()(const PathIndex& other)
{
    m_index = other.GetIndex();
    m_fraction = other.GetFraction();
}
void PathIndex::GetIndex(int64_t index)
{
    m_index = index;
}
void PathIndex::GetFraction(double fraction)
{
    m_fraction = fraction;
}
int64_t PathIndex::GetIndex() const
{
    return m_index;
}
double PathIndex::GetFraction() const
{
    return m_fraction;
}
}  //namespace mpmca::predict::inertial