/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/driver_minimal.hpp"

#include <algorithm>
#include <iostream>

#include "mpmca/constants.hpp"
#include "mpmca/data_names.hpp"
#include "mpmca/predict/inertial/car_minimal.hpp"
namespace mpmca::predict::inertial {
DriverMinimal::DriverMinimal(utilities::ConfigurationPtr config, const utilities::Horizon& t, const std::string& name,
                             bool is_for_control)
    : DriverMinimal(t, name, is_for_control)
{
}
DriverMinimal::DriverMinimal(const utilities::Horizon& t, const std::string& name, bool is_for_control)
    : Model(name, t, is_for_control)
    , u_t(this, "human_car_u_t_m_s")
    , u_e(this, "human_car_u_e_m_s")
    , lat_e(this, "human_car_lat_e_m")
    , lat_e_dot(this, "human_car_lat_e_dot_m_s")
    , psi_t(this, "human_car_psi_t_rad")
    , psi_e(this, "human_car_psi_e_rad")
    , r_c(this, "human_car_r_c_rad_s")
    , a_x_c(this, "human_car_a_x_c_m_s2")
    , steer(this, "car_steer_rad")
    , throttle(this, "car_throttle_fraction")
    , brake(this, "car_brake_fraction")
    , clutch(this, "car_clutch_fraction")
    , shift(this, "car_shift_integer")
    , throttle_dot(this, "car_throttle_dot_fraction_s")
    , brake_dot(this, "car_brake_dot_fraction_s")
    , x_path(this, "world_X_path_m")
    , y_path(this, "world_Y_path_m")
    , z_path(this, "world_Z_path_m")
    , phi_path(this, "world_phi_path_rad")
    , theta_path(this, "world_theta_path_rad")
    , psi_path(this, "world_psi_path_rad")
    , u_standard_deviation(this, "human_car_u_stddev_m_s")
    , u_standard_deviation_t(this, "human_car_u_stddev_t_m_s")
    , disturbance_r_c(this, "disturbance_r_c_rad_s", 0, true)
    , disturbance_a_x(this, "disturbance_a_x_m_s2", 0, true)
    , gear_change(this, "car_automatic_gear_change_boolean")
    , a_x_model(this, "car_a_x_model_m_s2")
    , K_u_e_throttle(0.3)
    , K_u_e_brake(0.3)
    , K_lat_e(-0.4)
    , T_lat_e(-1.0)
    , K_psi_t(1.0)
    , K_psi_e(0.4)
    , initial_world_X_vehicle_m(DN::X_vehicle_0().GetName(), Model::GetName() + ":" + DN::X_vehicle_0().GetName())
    , initial_world_Y_vehicle_m(DN::Y_vehicle_0().GetName(), Model::GetName() + ":" + DN::Y_vehicle_0().GetName())
    , initial_world_Z_vehicle_m(DN::Z_vehicle_0().GetName(), Model::GetName() + ":" + DN::Z_vehicle_0().GetName())
    , initial_world_psi_vehicle_rad(DN::psi_vehicle_0().GetName(),
                                    Model::GetName() + ":" + DN::psi_vehicle_0().GetName())
    , gravity_m_s2(DN::gravity().GetName(), Model::GetName() + ":" + DN::gravity().GetName())
    , vehicle_mass_kg(DN::vehicle_mass().GetName(), Model::GetName() + ":" + DN::vehicle_mass().GetName())
    , car_S_aero_force_kg_m("car_S_aero_force_kg_m", Model::GetName() + ":car_S_aero_force_kg_m")
    , car_S_brake_force_N_fraction("car_S_brake_force_N_fraction", Model::GetName() + ":car_S_brake_force_N_fraction")
    , car_M1D_gear_ratio_1_m("car_M1D_gear_ratio_1_m", Model::GetName() + ":car_M1D_gear_ratio_1_m")
    , car_M2D_engine_torque_Nm("car_M2D_engine_torque_Nm", Model::GetName() + ":car_M2D_engine_torque_Nm")
    , m_end_of_path_reached(false)
    , m_max_path_distance(5.0)
    , m_cycle(0)
{
}

void DriverMinimal::PrepareTick(PathPtr path)
{
    Vector6 initial_position = {initial_world_X_vehicle_m.GetValue(0),
                                initial_world_Y_vehicle_m.GetValue(0),
                                initial_world_Z_vehicle_m.GetValue(0),
                                0,
                                0,
                                initial_world_psi_vehicle_rad.GetValue(0)};

    m_path_index_current = m_searcher_global.FindIndexGlobal(path, initial_position);

    m_cycle = 0;
    m_logger.Debug("Closest index is " + std::to_string(m_path_index_current.GetIndex()));
}
void DriverMinimal::Extrapolate(int j)
{
    if (!IsLastStepInHorizon(j))
        gear_change.SetValue(j + 1, gear_change.GetValue(j));
}
void DriverMinimal::ExtrapolateDisturbances(int j)
{
    if (!IsLastStepInHorizon(j)) {
        disturbance_a_x.SetValue(j + 1, disturbance_a_x.GetValue(j));
        disturbance_r_c.SetValue(j + 1, disturbance_r_c.GetValue(j));
    }
}
template <>
void DriverMinimal::SpeedCurves<CarMinimal>(CarMinimal& car, int j)
{
    // calculate how many standard deviations we are currently away from the mean
    double u_standard_deviation_now =
        (car.u.GetValue(j) - m_path_point_current.GetUMean()) / m_path_point_current.GetUStandardDeviation();
    double u_standard_deviation_target = 0;

    if (!IsForControl())  // || s_fade() > 0)
    {
        // if there is a non-zero fade distance, we calculate the target stddev
        // as the weighted average over the fade distance.

        if (IsFirstStepInHorizon(j)) {
            m_u_standard_deviation_w = 0;
            m_u_standard_deviation_w_t = 0;
        }
        m_u_standard_deviation_w += GetTakeoverFade(1, 0);
        m_u_standard_deviation_w_t += GetTakeoverFade(1, 0) * u_standard_deviation_now;
        u_standard_deviation_target = m_u_standard_deviation_w_t / m_u_standard_deviation_w;
    }

    u_standard_deviation.SetValue(j, u_standard_deviation_now);
    u_standard_deviation_t.SetValue(j, u_standard_deviation_target);
}
template <>
void DriverMinimal::ControlInputs<CarMinimal>(CarMinimal& car, int j)
{
    if (IsFirstStepInHorizon(j)) {
        m_d_r_c = car.r.GetValue(j);
        m_rd_first = car.rd.GetValue(j);
        m_d_throttle_c = throttle.GetValue(j);
        m_throttle_dot_first = throttle_dot.GetValue(j);
        m_d_brake_c = brake.GetValue(j);
        m_brake_dot_first = brake_dot.GetValue(j);

        m_d_a_x = car.a_x.GetValue(j);
        m_d_a_x_dot_first = car.a_x_dot.GetValue(j);
    }
    else {
        m_d_r_c = m_d_r_c + m_rd_first * m_horizon.GetPredictionHorizonDeltaTime(ToHorizonIndex(j));
        m_d_throttle_c =
            m_d_throttle_c + m_throttle_dot_first * m_horizon.GetPredictionHorizonDeltaTime(ToHorizonIndex(j));
        m_d_brake_c = m_d_brake_c + m_brake_dot_first * m_horizon.GetPredictionHorizonDeltaTime(ToHorizonIndex(j));
    }
}
template <>
void DriverMinimal::Derivatives<CarMinimal>(CarMinimal& car, int j)
{
    if (IsFirstStepInHorizon(j)) {
        car.rd.SetValue(j, (car.r.GetValue(j) - car.r.GetValue(j - 1)) / m_horizon.GetDt());
        car.a_x_dot.SetValue(j, (car.a_x.GetValue(j) - car.a_x.GetValue(j - 1)) / m_horizon.GetDt());
        throttle_dot.SetValue(j, (throttle.GetValue(j) - throttle.GetValue(j - 1)) / m_horizon.GetDt());
        brake_dot.SetValue(j, (brake.GetValue(j) - brake.GetValue(j - 1)) / m_horizon.GetDt());
    }
    else {
        car.rd.SetValue(j, (car.r.GetValue(j) - car.r.GetValue(j - 1)) /
                               m_horizon.GetPredictionHorizonDeltaTime(ToHorizonIndex(j - 1)));
        car.a_x_dot.SetValue(j, (car.a_x.GetValue(j) - car.a_x.GetValue(j - 1)) /
                                    m_horizon.GetPredictionHorizonDeltaTime(ToHorizonIndex(j - 1)));
        throttle_dot.SetValue(j, (throttle.GetValue(j) - throttle.GetValue(j - 1)) /
                                     m_horizon.GetPredictionHorizonDeltaTime(ToHorizonIndex(j - 1)));
        brake_dot.SetValue(j, (brake.GetValue(j) - brake.GetValue(j - 1)) /
                                  m_horizon.GetPredictionHorizonDeltaTime(ToHorizonIndex(j - 1)));
    }
}
template <>
void DriverMinimal::HorizonDerivatives<CarMinimal>(CarMinimal& car, int j)
{
    if (!IsFirstStepInHorizon(j)) {
        car.rd.SetValue(j, (car.r.GetValue(j) - car.r.GetValue(j - 1)) /
                               m_horizon.GetPredictionHorizonDeltaTime(ToHorizonIndex(j)));
        car.a_x_dot.SetValue(j, (car.a_x.GetValue(j) - car.a_x.GetValue(j - 1)) /
                                    m_horizon.GetPredictionHorizonDeltaTime(ToHorizonIndex(j - 1)));
        throttle_dot.SetValue(j, (throttle.GetValue(j) - throttle.GetValue(j - 1)) /
                                     m_horizon.GetPredictionHorizonDeltaTime(ToHorizonIndex(j - 1)));
        brake_dot.SetValue(j, (brake.GetValue(j) - brake.GetValue(j - 1)) /
                                  m_horizon.GetPredictionHorizonDeltaTime(ToHorizonIndex(j - 1)));
    }
}

template <>
void DriverMinimal::VehicleStates<CarMinimal>(CarMinimal& car, int j)
{
    m_vehicle_state_current.SetX(car.x.GetValue(j));
    m_vehicle_state_current.SetY(car.y.GetValue(j));
    m_vehicle_state_current.SetZ(car.z.GetValue(j));
    m_vehicle_state_current.SetPhi(car.phi.GetValue(j));
    m_vehicle_state_current.SetTheta(car.theta.GetValue(j));
    m_vehicle_state_current.SetPsi(car.psi.GetValue(j));

    m_vehicle_state_current.SetU(car.u.GetValue(j));
    m_vehicle_state_current.SetV(car.v.GetValue(j));
    m_vehicle_state_current.SetW(car.w.GetValue(j));

    m_vehicle_state_current.SetAx(car.a_x.GetValue(j));
    m_vehicle_state_current.SetAy(car.a_y.GetValue(j));
    m_vehicle_state_current.SetAz(car.a_z.GetValue(j));

    m_vehicle_state_current.SetP(car.p.GetValue(j));
    m_vehicle_state_current.SetQ(car.q.GetValue(j));
    m_vehicle_state_current.SetR(car.r.GetValue(j));
}
template <>
void DriverMinimal::CalculateModelLongitudinalAcceleration<CarMinimal>(CarMinimal& car, int j)
{
    if (IsFirstStepInHorizon(j)) {
        double T_engine_model = car_M2D_engine_torque_Nm.GetValue(GetCurrentTimeMs(), throttle.GetValue(j),
                                                                  car.omega_engine.GetValue(j), 0.0);
        double Fx_engine_raw =
            T_engine_model * car_M1D_gear_ratio_1_m.GetValue(GetCurrentTimeMs(), std::round(car.gear.GetValue(j)),
                                                             utilities::Map1D::InterpolationMethod::kNearestNeighbour);
        double Fx_engine_tractive = std::max(0., Fx_engine_raw);
        double Fx_engine_brake = utilities::Math::Sign(car.u.GetValue(j)) * std::min(0., Fx_engine_raw);
        double Fx_brake = -1. * utilities::Math::Sign(car.u.GetValue(j)) * brake.GetValue(j) *
                          car_S_brake_force_N_fraction.GetValue(GetCurrentTimeMs());
        double Fx_aero = -1. * utilities::Math::Sign(car.u.GetValue(j)) *
                         car_S_aero_force_kg_m.GetValue(GetCurrentTimeMs()) * car.u.GetValue(j) * car.u.GetValue(j);
        double Fx_gravity = -1. * std::sin(car.theta.GetValue(j)) * gravity_m_s2.GetValue(GetCurrentTimeMs()) *
                            vehicle_mass_kg.GetValue(GetCurrentTimeMs());

        double Fx_tractive = Fx_engine_tractive + Fx_gravity;
        double u0 = (double)(abs(car.u.GetValue(j)) < 0.5);
        double Fx_total_u0 =
            Fx_tractive - utilities::Math::Sign(Fx_tractive) * std::min(std::abs(Fx_tractive), std::abs(Fx_brake));
        double Fx_total_u = Fx_engine_tractive + Fx_gravity + Fx_engine_brake + Fx_aero + Fx_brake;
        double Fx_total = u0 * Fx_total_u0 + (1 - u0) * Fx_total_u;
        double a_x_calculated = Fx_total / vehicle_mass_kg.GetValue(GetCurrentTimeMs());

        a_x_model.SetValue(j, a_x_calculated);
        a_x_model.SetValue(j + 1, a_x_calculated);
    }
    else if (!IsLastStepInHorizon(j))
        a_x_model.SetValue(j + 1, a_x_model.GetValue(j));
}
void DriverMinimal::CheckDistance(PathPtr path, const PathIndex& path_index, const Vector6& point) const
{
    double current_distance = path->GetPathPointAt(path_index).GetDistance(point);

    if (current_distance > m_max_path_distance) {
        throw std::runtime_error(
            "Car is " + std::to_string(current_distance) + " m from path, which is more than allowed (" +
            std::to_string(m_max_path_distance) + ")." + "\nCar location is " +
            std::to_string(m_vehicle_state_current.GetX()) + ", " + std::to_string(m_vehicle_state_current.GetY()) +
            ", " + std::to_string(m_vehicle_state_current.GetZ()) + "\nClosest point is " +
            std::to_string(m_path_index_current.GetIndex()) + ":" + std::to_string(m_path_index_current.GetFraction()) +
            " at " + path->GetPathPointAt(m_path_index_current.GetIndex()).ToString() + "\nPath has " +
            std::to_string(path->GetNumPathPoints()) + " points.");
    }
}
template <>
void DriverMinimal::LookAtPath<CarMinimal>(const CarMinimal& car, PathPtr path, int j)
{
    if (IsFirstStepInHorizon(j)) {
        if (GetTimeJumped())
            m_path_index_current = m_searcher_global.FindIndexGlobal(path, m_vehicle_state_current);
        else
            m_path_index_current = m_searcher_global.FindIndexLocal(path, m_vehicle_state_current);

        m_searcher_horizon.CopyState(m_searcher_global);

        SetEndOfPathReached(path->SetEndOfPathReached(m_path_index_current));
    }
    else {
        m_path_index_current = m_searcher_horizon.FindIndexLocal(path, m_vehicle_state_current);
        UpdateEndOfPathReached(path->SetEndOfPathReached(m_path_index_current));
    }

    if (m_path_index_current.GetFraction() < -0.5 || m_path_index_current.GetFraction() > 0.5)
        throw std::runtime_error(
            "On predictor step " + std::to_string(j) + ". Fraction should be between -0.5 and 0.5. It is " +
            std::to_string(m_path_index_current.GetFraction()) + ". Searched from state:\n" +
            std::to_string(m_vehicle_state_current.GetX()) + ", " + std::to_string(m_vehicle_state_current.GetY()) +
            ", " + std::to_string(m_vehicle_state_current.GetZ()) + ".");

    m_path_point_current = path->GetInterpolated(m_path_index_current);

    x_path.SetValue(j, m_path_point_current.GetX());
    y_path.SetValue(j, m_path_point_current.GetY());
    z_path.SetValue(j, m_path_point_current.GetZ());
    phi_path.SetValue(j, m_path_point_current.GetPhi());
    theta_path.SetValue(j, m_path_point_current.GetTheta());
    psi_path.SetValue(j, m_path_point_current.GetPsi());
}
template <>
void DriverMinimal::LateralControl<CarMinimal>(CarMinimal& car, PathPtr path, int j)
{
    double lat_error = 0.;
    double lateral_error_dot = 0.;

    lat_error = path->GetPathPointAt(m_path_index_current)
                    .GetLateralError(m_path_index_current.GetFraction(), m_vehicle_state_current);
    lateral_error_dot = path->GetPathPointAt(m_path_index_current)
                            .GetLateralErrorDot(m_path_index_current.GetFraction(), m_vehicle_state_current);

    lat_e.SetValue(j, lat_error);
    lat_e_dot.SetValue(j, lateral_error_dot);

    double psi_t_lat;
    if (IsForControl())
        psi_t_lat = K_lat_e.GetValue() * (lat_e.GetValue(j) - T_lat_e.GetValue() * lat_e_dot.GetValue(j));
    else
        psi_t_lat = std::clamp(K_lat_e.GetValue() * GetTakeoverFade(0, 1, t_fade.GetValue() * 5) *
                                   (lat_e.GetValue(j) - T_lat_e.GetValue() * lat_e_dot.GetValue(j)),
                               -20 * constants::kPi / 180.0, 20 * constants::kPi / 180.0);

    psi_t.SetValue(j, m_path_point_current.GetPsi() + psi_t_lat);
    psi_e.SetValue(j, utilities::Math::GetAngleDiff(psi_t.GetValue(j), car.psi.GetValue(j)));

    double r_c_t = K_psi_t.GetValue() * m_path_point_current.GetDPsiDs() * car.u.GetValue(j);
    double r_c_e = K_psi_e.GetValue() * psi_e.GetValue(j);

    r_c.SetValue(j, r_c_t + r_c_e);

    // OUTPUTS
    if (IsForControl()) {
        double t_start = std::min(1.0, m_cycle * m_horizon.GetPredictionHorizonDeltaTime(ToHorizonIndex(j)));
        double k_fade = utilities::Math::GetCosineFade(0.0, 1.0, 1.0, t_start);
        car.r.SetValue(j, r_c.GetValue(j) * k_fade);
    }
    else
        car.r.SetValue(j, GetTakeoverFade(m_d_r_c, r_c.GetValue(j)));

    if (!std::isnan(disturbance_r_c.GetValue(j)) && IsForControl())
        car.r.SetValue(j, car.r.GetValue(j) + disturbance_r_c.GetValue(j));

    car.r.SetValue(j, car.r.GetValue(j) * (double)(car.u.GetValue(j) > 1.0));
}
template <>
void DriverMinimal::LongitudinalControl<CarMinimal>(CarMinimal& car, int j)
{
    CalculateModelLongitudinalAcceleration(car, j);

    u_t.SetValue(j, m_path_point_current.GetUMean() +
                        u_standard_deviation_t.GetValue(j) * m_path_point_current.GetUStandardDeviation());
    double u_e_max = 15;
    u_e.SetValue(j, std::clamp(u_t.GetValue(j) - car.u.GetValue(j), -u_e_max, u_e_max));

    double a_x_fb = K_u_e_throttle.GetValue() * u_e.GetValue(j) * (double)(u_e.GetValue(j) > 0) +
                    K_u_e_brake.GetValue() * u_e.GetValue(j) * (double)(u_e.GetValue(j) < 0);
    double a_x_ff = m_path_point_current.GetDuMeanDs() * car.u.GetValue(j);

    if (IsForControl()) {
        a_x_c.SetValue(j, a_x_ff + a_x_fb);

        if (!std::isnan(disturbance_a_x.GetValue(j)))
            a_x_c.SetValue(j, disturbance_a_x.GetValue(j));

        car.a_x.SetValue(j, a_x_c.GetValue(j));
    }
    else {
        a_x_c.SetValue(j, a_x_ff);
        double a_x_calm = GetTakeoverFade(
            m_d_a_x, a_x_model.GetValue(j) * gear_change.GetValue(j) + m_d_a_x * (1. - gear_change.GetValue(j)), 0.3);
        car.a_x.SetValue(j, GetTakeoverFade(a_x_calm, a_x_c.GetValue(j), 1., 1.));
    }
}
template <>
void DriverMinimal::Step<CarMinimal>(CarMinimal& car, PathPtr path, int j)
{
    m_time_measurement.StartCycle();

    Derivatives<CarMinimal>(car, j);
    Extrapolate(j);
    ExtrapolateDisturbances(j);
    CalculateTakeoverTime(j);
    VehicleStates<CarMinimal>(car, j);
    LookAtPath<CarMinimal>(car, path, j);
    ControlInputs<CarMinimal>(car, j);
    SpeedCurves<CarMinimal>(car, j);
    LongitudinalControl<CarMinimal>(car, j);
    LateralControl<CarMinimal>(car, path, j);
    HorizonDerivatives<CarMinimal>(car, j);

    m_time_measurement.StopCycle();

    ++m_cycle;
}
}  //namespace mpmca::predict::inertial