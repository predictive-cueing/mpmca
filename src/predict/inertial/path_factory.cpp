/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/path_factory.hpp"

#include "mpmca/constants.hpp"
#include "mpmca/predict/inertial/path_register.hpp"
#include "mpmca/utilities/math.hpp"

namespace mpmca::predict::inertial {
PathFactory::PathFactory(const std::vector<PathPoint>& path, bool closed)
    : PathFactory("UnnamedPathFactory", path, closed)
{
}
PathFactory::PathFactory(const std::string& factory_name, const std::vector<PathPoint>& path, bool closed)
    : m_factory_name(factory_name)
    , m_closed(closed)
    , m_omit_heading_check(false)
    , m_prepared(false)
    , m_smallest_distance(1e9)
    , m_unprepared_path_point_vector(path)
{
}
void PathFactory::SetHeadingLimit(double psi_limit)
{
    m_heading_limit = psi_limit;
}
void PathFactory::SetOmitHeadingCheck(bool omit)
{
    m_omit_heading_check = omit;
}
PathPtr PathFactory::GetPreparedPath(const std::string& path_name)
{
    Prepare();
    PathPtr ret = std::make_shared<Path>(path_name, m_prepared_path_point_vector, m_closed);
    ret->SetSmallestApproximatePointToPointDistance(m_smallest_distance);

    return PathRegister::Instance()->StorePath(ret);
}
void PathFactory::CheckHeading()
{
    /**
     * @brief Assume that the car moves along a circle with radius 1 m, centroid at (0,0) and that the
     * m_unprepared_path_point_vector has a ds of 0.1 m. So, each step on the m_unprepared_path_point_vector has a d_psi
     * = ds / radius = 0.1 / 1 = 0.1 rad at psi = 0, the heading is +pi/2 radians
     *
     * The coordinates and heading of the first two points are:
     * #    x           y           std::atan2(dy,dx)    psi
     * 0    1           0           1.620796        pi/2 + 0 * 0.1 = 1.570796
     * 1    0.9950      0.09983     1.720796        pi/2 + 1 * 0.1 = 1.670796
     * 2    0.9801      0.19867     1.820796        pi/2 + 2 * 0.1 = 1.770796
     *
     * So, we notice that the heading that is calculated based on derivatives of x and y is exactly in between the
     * actual heading and the heading of the next point.
     *
     */

    if (m_omit_heading_check)
        return;

    for (int i = 0; i < m_unprepared_path_point_vector.size() - 1 + (int)m_closed; ++i) {
        double dy = GetPathPointAt(m_unprepared_path_point_vector, i + 1).GetY() -
                    GetPathPointAt(m_unprepared_path_point_vector, i).GetY();
        double dx = GetPathPointAt(m_unprepared_path_point_vector, i + 1).GetX() -
                    GetPathPointAt(m_unprepared_path_point_vector, i).GetX();
        double psi_derivative = std::atan2(dy, dx);
        CheckHeading(GetPathPointAt(m_unprepared_path_point_vector, i).GetPsi(), psi_derivative, i);
    }
}
void PathFactory::CheckHeading(double actual_psi, double derivative_psi, int i) const
{
    if (abs(utilities::Math::GetAngleDiff(actual_psi, derivative_psi)) > m_heading_limit)
        throw std::runtime_error(
            "The heading (psi) provided for point " + std::to_string(i) + ", equal to " + std::to_string(actual_psi) +
            " deviates " + std::to_string(utilities::Math::GetAngleDiff(actual_psi, derivative_psi)) +
            " rad from the heading calculated based on the location of the next point, which is equal to " +
            std::to_string(derivative_psi) + ". The difference should be smaller than " +
            std::to_string(m_heading_limit) + " rad.");
}
void PathFactory::SetIndices()
{
    for (int i = 0; i < m_unprepared_path_point_vector.size(); ++i)
        m_unprepared_path_point_vector[i].SetIndex(i);
}
void PathFactory::Prepare()
{
    if (m_prepared)
        return;

    if (m_unprepared_path_point_vector.size() < 3)
        throw std::runtime_error("This unprepared path point vector contains " +
                                 std::to_string(m_unprepared_path_point_vector.size()) +
                                 " points. A m_unprepared_path_point_vector should contain at least 3 points "
                                 "(but typically contains many more...).");

    CheckClosed();
    SetIndices();
    CheckHeading();
    DetermineSmallestApproximatePointToPointDistance();

    for (auto& p : m_unprepared_path_point_vector)
        m_prepared_path_point_vector.push_back(PathPointE(p));

    ProcessVector(m_prepared_path_point_vector, m_closed);

    m_prepared = true;
}
void PathFactory::DetermineSmallestApproximatePointToPointDistance()
{
    for (int i = 0; i < m_unprepared_path_point_vector.size() - 1 + (int)m_closed; ++i)
        m_smallest_distance = std::min(
            m_smallest_distance, GetPathPointAt(m_unprepared_path_point_vector, i)
                                     .Vector2::GetDistance(GetPathPointAt(m_unprepared_path_point_vector, i + 1)));
}
void PathFactory::CheckClosed()
{
    if (!m_closed)
        return;

    if (!m_unprepared_path_point_vector.front().IsClose(m_unprepared_path_point_vector.back(), 0.001, 0.001)) {
        throw std::runtime_error(
            "The first and last elements of a closed unprepared path point vector should be identical. First and last "
            "points:\n" +
            m_unprepared_path_point_vector.front().ToString() + "\n" +
            m_unprepared_path_point_vector.back().ToString());
    }
    else
        m_unprepared_path_point_vector.pop_back();
}
std::vector<PathPoint> PathFactory::CreatePathPointVectorCircle(double ds, double psi_start, double psi_end,
                                                                double radius, double x0, double y0)
{
    double arc_length = radius * std::abs(psi_end - psi_start);
    int N = (int)ceil(arc_length / ds);
    double d_psi = std::abs(psi_end - psi_start) / (double)(N);
    double d_psi_sign = utilities::Math::Sign(psi_end - psi_start);

    double xc = x0 - radius * std::cos(psi_start);
    double yc = y0 - radius * std::sin(psi_start);

    std::vector<PathPoint> path_vector;
    for (int i = 0; i <= N; ++i) {
        path_vector.push_back(PathPoint(xc + radius * std::cos(psi_start + d_psi_sign * d_psi * i),
                                        yc + radius * std::sin(psi_start + d_psi_sign * d_psi * i), 0., 0., 0.,
                                        psi_start + d_psi_sign * d_psi * i + constants::kPi_2));
    }
    return path_vector;
}
PathPtr PathFactory::CreatePathCircle(double ds, double psi_start, double psi_end, double radius, double x0, double y0,
                                      bool closed, const std::string& name)
{
    return PathFactory("CirclePathFactory", CreatePathPointVectorCircle(ds, psi_start, psi_end, radius, x0, y0), closed)
        .GetPreparedPath(name);
}
PathPtr PathFactory::CreatePathLine(double ds, double x0, double y0, double x1, double y1, double u_mean,
                                    double u_standard_deviation, const std::string& name)
{
    Vector6 p0(x0, y0);
    Vector6 p1(x1, y1);

    double line_length = p0.GetDistance(p1);
    double psi = p0.GetHeadingFrom(p1);

    int N = (int)(ceil(line_length / ds));

    double dx = (x1 - x0) / (double)N;
    double dy = (y1 - y0) / (double)N;

    std::vector<PathPoint> test_path;

    for (int i = 0; i < N; ++i) {
        test_path.push_back(PathPoint(x0 + i * dx, y0 + i * dy, 0.0, 0.0, 0.0, psi));
        test_path.back().SetUMean(u_mean);
        test_path.back().SetUStandardDeviation(u_standard_deviation);
    }

    return PathFactory("LinePathFactory", test_path, false).GetPreparedPath(name);
}
std::vector<PathPoint> PathFactory::GetCirclePathPointVector(int num_points_whole_circle, int num_points_vector,
                                                             double radius, double x_center, double y_center)
{
    std::vector<PathPoint> path_points;

    for (int i = 0; i < num_points_vector; ++i) {
        double fraction = (double)i / (double)num_points_whole_circle;
        path_points.push_back(PathPoint(radius * std::cos(fraction * 2 * constants::kPi) + x_center,
                                        radius * std::sin(fraction * 2 * constants::kPi) + y_center, 0.0, 0.0, 0.0,
                                        fraction * 2 * constants::kPi + constants::kPi_2));
    }

    return path_points;
}
std::vector<PathPoint> PathFactory::GetStraightPathPointVector(const Vector6& start, const Vector6& end, double ds)
{
    std::vector<PathPoint> path_points;

    double psi = start.GetHeadingFrom(end);
    double distance = start.Vector2::GetDistance(end);
    int num_points = std::ceil(distance / ds) + 1;
    double dx = std::cos(psi) * ds;
    double dy = std::sin(psi) * ds;

    for (int i = 0; i < num_points; ++i)
        path_points.push_back(PathPoint(start.GetX() + i * dx, start.GetY() + i * dy, 0.0, 0.0, 0.0, psi));

    return path_points;
}
std::vector<PathPoint> PathFactory::GetCircularPathPointVector(int num_points, double ds, double radius,
                                                               double x_center, double y_center)
{
    std::vector<PathPoint> path_points;

    double d_psi = ds / radius;

    for (int i = 0; i < num_points; ++i) {
        path_points.push_back(PathPoint(radius * std::cos(i * d_psi) + x_center,
                                        radius * std::sin(i * d_psi) + y_center, 0.0, 0.0, 0.0,
                                        i * d_psi + constants::kPi_2));
    }

    return path_points;
}
}  //namespace mpmca::predict::inertial