/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/interpolatable.hpp"

#include <stdexcept>

namespace mpmca::predict::inertial {
Interpolatable::Interpolatable(double value)
    : m_value(value)
{
}
double Interpolatable::GetValue() const
{
    return m_value;
}
void Interpolatable::SetValue(double value)
{
    m_value = value;
}
int Interpolatable::GetNumPointsRequiredBehind() const
{
    return 0;
}
int Interpolatable::GetNumPointsRequiredAhead() const
{
    return 0;
}
double Interpolatable::GetInterpolated(double fraction) const
{
    throw std::runtime_error("Cannot interpolate a bare Interpolatable.");
    return 0;
}
double Interpolatable::Derivative(double fraction) const
{
    throw std::runtime_error("Cannot calculate the derivative of a bare Interpolatable.");
    return 0;
}
void Interpolatable::GetAdjacentPoint(int index_difference, double ds, const Interpolatable& other)
{
    throw std::runtime_error("The Interpolatable base class does not take adjacent points.");
}
void Interpolatable::Finalize(std::function<double(const double&, const double&)> difference_function)
{
    throw std::runtime_error("The Interpolatable base class cannot be finalized.");
}
}  //namespace mpmca::predict::inertial