/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/vector_2.hpp"

#include <cmath>

#include "mpmca/utilities/math.hpp"

namespace mpmca::predict::inertial {
Vector2::Vector2()
    : Vector2(0, 0)
{
}
Vector2::Vector2(double x, double y)
    : m_x(x)
    , m_y(y)
{
}
double Vector2::GetLength() const
{
    return std::sqrt(m_x * m_x + m_y * m_y);
}
Vector2 Vector2::GetUnitVector() const
{
    double l = GetLength();
    return Vector2(m_x / l, m_y / l);
}
Vector2 Vector2::MoveDistance(const Vector2& towards, double distance) const
{
    return (towards - (*this)).GetUnitVector() * distance + (*this);
}
double Vector2::GetX() const
{
    return m_x;
}
double Vector2::GetY() const
{
    return m_y;
}
void Vector2::SetX(double i)
{
    m_x = i;
}
void Vector2::SetY(double i)
{
    m_y = i;
}
double Vector2::GetHeading() const
{
    return std::atan2(m_y, m_x);
}
double Vector2::GetHeadingFrom(const Vector2& other) const
{
    return std::atan2(other.GetY() - m_y, other.GetX() - m_x);
}
double Vector2::GetDistance(const Vector2& other) const
{
    return std::sqrt(GetDistanceSquared(other));
}
double Vector2::GetDistanceSquared(const Vector2& other) const
{
    double dx = (m_x - other.GetX());
    double dy = (m_y - other.GetY());
    return dx * dx + dy * dy;
}
Vector2 Vector2::operator*(double mult) const
{
    return Vector2(m_x * mult, m_y * mult);
}
Vector2 Vector2::operator-(const Vector2& other) const
{
    return Vector2(m_x - other.GetX(), m_y - other.GetY());
}
Vector2 Vector2::operator+(const Vector2& other) const
{
    return Vector2(m_x + other.GetX(), m_y + other.GetY());
}
Vector2 Vector2::GetMeanWith(const Vector2& other) const
{
    return Vector2(m_x / 2.0 + other.GetX() / 2.0, m_y / 2.0 + other.GetY() / 2.0);
}
double Vector2::GetDotProductWith(const Vector2& other) const
{
    return m_x * other.GetX() + m_y * other.GetY();
}
std::ostream& operator<<(std::ostream& out, const Vector2& c)  //
{
    out << " x: " << c.GetX() << " y: " << c.GetY();
    return out;
}
}  //namespace mpmca::predict::inertial