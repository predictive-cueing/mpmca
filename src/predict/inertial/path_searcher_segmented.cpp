/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/path_searcher_segmented.hpp"

#include "mpmca/utilities/math.hpp"
namespace mpmca::predict::inertial {
const PathIndex& PathSearcherSegmented::FindIndexLocal(PathPtr path, const Vector6& p)
{
    // basically the same as the grid searcher
    m_iterations = 0;
    double alpha = 2;
    double distance_traveled = alpha * m_last_query_position.Vector2::GetDistance(p);
    int distance_traveled_index =
        std::max(5, (int)(distance_traveled / path->GetSmallestApproximatePointToPointDistance()));

    if (2 * distance_traveled < m_maximum_points_local_search) {
        m_last_path_index.GetIndex(SearchAllFromTo(path, p, m_last_path_index.GetIndex() - distance_traveled_index,
                                                   m_last_path_index.GetIndex() + distance_traveled_index));
        m_last_path_index.GetIndex(path->GetPathPointAt(m_last_path_index.GetIndex()).GetIndex());
        m_last_path_index.GetFraction(path->GetPathPointAt(m_last_path_index.GetIndex()).GetFraction(p));
        m_last_query_position = p;
        return m_last_path_index;
    }
    else {
        return FindIndexGlobal(path, p);
    }
}
const PathIndex& PathSearcherSegmented::FindIndexGlobal(PathPtr path, const Vector6& p)
{
    m_iterations = 0;
    m_last_path_index.GetIndex(SearchSegmented(path, p, path->GetStartIndex(), path->GetEndIndex()));
    m_last_path_index.GetIndex(path->GetPathPointAt(m_last_path_index.GetIndex()).GetIndex());
    m_last_path_index.GetFraction(path->GetPathPointAt(m_last_path_index.GetIndex()).GetFraction(p));
    m_last_query_position = p;

    return m_last_path_index;
}
int PathSearcherSegmented::SearchSegmented(PathPtr path, const Vector6& p, int from, int to)
{
    std::vector<PathSegment> segments;
    segments.push_back(PathSegment(from, to));
    SearchSegmented(path, p, segments);

    if (segments.size() == 0)
        throw std::runtime_error("No segments remain...");

    double min_dist = segments[0].GetMinimumDistance();
    int min_index = 0;

    for (int i = 1; i < segments.size(); ++i) {
        if (segments[i].GetMinimumDistance() < min_dist) {
            min_dist = segments[i].GetMinimumDistance();
            min_index = i;
        }
    }
    return path->GetPathPointAt(segments[min_index].GetMinimumIndex()).GetIndex();
}
void PathSearcherSegmented::SearchSegmented(PathPtr path, const Vector6& p, std::vector<PathSegment>& segments,
                                            int ident)
{
    std::string tabs;

    if (m_verbose) {
        for (int i = 0; i < ident; ++i)
            tabs += "\t";
        std::cout << tabs << "SearchSegmented, with " << segments.size() << " segments." << std::endl;
    }

    for (auto& segment : segments)
        EvaluateSegment(path, p, segment);

    sort(segments.begin(), segments.end());

    std::vector<PathSegment> refined_segments;
    bool all_segments_all_points_evaluated = true;

    for (int i = 0; i < std::min(m_num_segments_advance, (int)segments.size()); ++i) {
        if (segments[i].GetAllPointsAreEvaluated()) {
            refined_segments.push_back(segments[i]);
        }
        else {
            all_segments_all_points_evaluated = false;

            int num_sub_segments = std::clamp(segments[i].GetNumberOfPoints() / m_num_points_search_max,
                                              m_minimum_number_of_sub_segments, m_maximum_number_of_sub_segments);

            refined_segments.reserve(refined_segments.size() + num_sub_segments);
            int points_per_segment = (int)ceil((double)segments[i].GetNumberOfPoints() / (double)num_sub_segments);

            if (m_verbose)
                std::cout << tabs << "Going to break up segment " << i << " into " << num_sub_segments
                          << " sub-segments, with " << points_per_segment << " points per segment." << std::endl;

            for (int j = 0; j < num_sub_segments; ++j) {
                int from;
                int to;

                if (path->IsClosed() || j < num_sub_segments - 1) {
                    from = segments[i].GetFrom() + (j + 0) * points_per_segment;
                    to = segments[i].GetFrom() + (j + 1) * points_per_segment;
                }
                else {
                    from = segments[i].GetFrom() + (j + 0) * points_per_segment;
                    to = std::min(segments[i].GetTo(), segments[i].GetFrom() + (j + 1) * points_per_segment);
                }

                if (m_verbose)
                    std::cout << tabs << j << " [" << from << ", " << to << "]" << std::endl;

                refined_segments.push_back(PathSegment(from, to));
            }
        }
    }

    if (!all_segments_all_points_evaluated)
        SearchSegmented(path, p, refined_segments, ident + 1);

    segments = refined_segments;
}
void PathSearcherSegmented::EvaluateSegment(PathPtr path, const Vector6& p, PathSegment& segment)
{
    if (segment.GetNumberOfPoints() > m_num_points_search_max) {
        segment.IsMinimumPointFound(segment.GetCenter(),
                                    p.Vector2::GetDistance(path->GetPathPointAt(segment.GetCenter())));
        ++m_iterations;
    }
    else
        SearchEntireSegment(path, p, segment);
}
void PathSearcherSegmented::SearchEntireSegment(PathPtr path, const Vector6& p, PathSegment& segment)
{
    int index = SearchAllFromTo(path, p, segment.GetFrom(), segment.GetTo());
    segment.IsMinimumPointFound(index, p.Vector2::GetDistance(path->GetPathPointAt(index)));
    segment.SetAllPointsAreEvaluated(true);
    ++m_iterations;
}
}  //namespace mpmca::predict::inertial