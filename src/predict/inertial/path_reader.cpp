/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/path_reader.hpp"

#include <algorithm>
#include <filesystem>
#include <fstream>

#include "mpmca/json_tools.hpp"
#include "mpmca/predict/inertial/path.hpp"
#include "mpmca/predict/inertial/path_factory.hpp"
#include "mpmca/predict/inertial/path_point.hpp"
#include "mpmca/predict/inertial/path_register.hpp"

namespace mpmca::predict::inertial {
template <>
PathPtr PathReader::ReadPath<Path>(utilities::ConfigurationPtr config, const std::string& name)
{
    auto cfg = config->GetConfigurationObject("PathReader");

    std::string path_name;
    if (cfg->GetKeyExists("Name"))
        path_name = cfg->GetValue<std::string>("Name", "PathFactory");
    else
        path_name = name;

    bool closed;
    auto path_point_vector =
        ReadPathPointVector(path_name, cfg->GetValue<std::string>("Filename", "data/test_path.json"), closed);

    PathFactory factory(path_name + "Factory", path_point_vector, closed);
    factory.SetOmitHeadingCheck(cfg->GetValue<bool>("OmitHeadingCheck", false));
    factory.SetHeadingLimit(cfg->GetValue<double>("HeadingCheckThreshold", 0.15));
    return factory.GetPreparedPath(path_name);
}

template <>
PathPtr PathReader::ReadPath<Path>(const std::string& name, const std::string& filename, double ds_tol)
{
    bool closed;
    auto path_point_vector = ReadPathPointVector(name, filename, closed);
    return PathFactory(name + "Factory", path_point_vector, closed).GetPreparedPath(name);
}

std::vector<PathPoint> PathReader::ReadPathPointVector(const std::string& name, const std::string& filename,
                                                       bool& closed)
{
    if (!std::filesystem::exists(filename))
        throw std::invalid_argument("File does not exist : " + filename);

    nlohmann::json sample_file;
    std::ifstream(filename) >> sample_file;

    // bool
    closed = false;

    std::vector<PathPoint> path_vector;

    if (sample_file.count("options") == 1) {
        if (sample_file.at("options").count("circular") == 1) {
            if (sample_file.at("options").at("circular").is_boolean())
                closed = sample_file.at("options").value("circular", false);
            else if (sample_file.at("options").at("circular").is_number_integer())
                closed = sample_file.at("options").value("circular", 0) == 1;
            else
                throw std::runtime_error(
                    "The path definition file " + filename +
                    " contains an option field called 'circular', but it is neither a boolean nor an integer.");
        }

        if (sample_file.at("options").count("closed") == 1) {
            if (sample_file.at("options").at("closed").is_boolean())
                closed = sample_file.at("options").value("closed", false);
            else if (sample_file.at("options").at("closed").is_number_integer())
                closed = sample_file.at("options").value("closed", 0) == 1;
            else
                throw std::runtime_error(
                    "The path definition file " + filename +
                    " contains an option field called 'closed', but it is neither a boolean nor an integer.");
        }
    }
    else if (sample_file.count("options") > 1)
        throw std::runtime_error("The path definition file " + filename + " contains more than one 'options' field.");

    int idx_x_path = sample_file.at("column_ids").value("x_path", -1);
    int idx_y_path = sample_file.at("column_ids").value("y_path", -1);
    int idx_z_path = sample_file.at("column_ids").value("z_path", -1);
    int idx_phi_path = sample_file.at("column_ids").value("phi_path", -1);
    int idx_theta_path = sample_file.at("column_ids").value("theta_path", -1);
    int idx_psi_path = sample_file.at("column_ids").value("psi_path", -1);
    int idx_uMean = sample_file.at("column_ids").value("u_mean", -1);
    int idx_uStandardDeviation = sample_file.at("column_ids").value("u_standard_deviation", -1);

    if (idx_x_path == -1)
        throw std::invalid_argument("PathReader " + name +
                                    ", data file does not contain signal \"x_path\", which is necessary.");

    if (idx_y_path == -1)
        throw std::invalid_argument("PathReader " + name +
                                    ", data file does not contain signal \"y_path\", which is necessary.");

    if (idx_z_path == -1)
        throw std::invalid_argument("PathReader " + name +
                                    ", data file does not contain signal \"z_path\", which is necessary.");

    if (idx_phi_path == -1)
        throw std::invalid_argument("PathReader " + name +
                                    ", data file does not contain signal \"phi_path\", which is necessary.");

    if (idx_theta_path == -1)
        throw std::invalid_argument("PathReader " + name +
                                    ", data file does not contain signal \"theta_path\", which is necessary.");

    if (idx_psi_path == -1)
        throw std::invalid_argument("PathReader " + name +
                                    ", data file does not contain signal \"psi_path\", which is necessary.");

    size_t N = sample_file.at("data").size();
    path_vector.reserve(N);

    for (size_t i = 0; i < N; ++i) {
        double x = sample_file.at("data").at(i).at(idx_x_path);
        double y = sample_file.at("data").at(i).at(idx_y_path);
        double z = sample_file.at("data").at(i).at(idx_z_path);
        double phi = sample_file.at("data").at(i).at(idx_phi_path);
        double theta = sample_file.at("data").at(i).at(idx_theta_path);
        double psi = sample_file.at("data").at(i).at(idx_psi_path);

        path_vector.push_back(PathPoint(x, y, z, phi, theta, psi));

        if (idx_uMean != -1 && idx_uStandardDeviation != -1) {
            path_vector.back().SetUMean(sample_file.at("data").at(i).at(idx_uMean));
            path_vector.back().SetUStandardDeviation(sample_file.at("data").at(i).at(idx_uStandardDeviation));
        }
    }
    return path_vector;
}

}  //namespace mpmca::predict::inertial