/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/interpolatable_linear.hpp"

#include <algorithm>
#include <stdexcept>

namespace mpmca::predict::inertial {
InterpolatableLinear::InterpolatableLinear(double value)
    : Interpolatable(value)
    , m_dy_behind(0)
    , m_dy_ahead(0)
    , m_dy_ds_behind(0)
    , m_dy_ds_ahead(0)
{
    m_values.fill(0);
    m_dss.fill(0);
    m_was_set.fill(false);
}
void InterpolatableLinear::SetValue(double value)
{
    m_value = value;
    m_dy_behind = 0;
    m_dy_ahead = 0;
    m_ready = false;
}
double InterpolatableLinear::GetInterpolated(double fraction) const
{
    return (fraction < 0.0) * fraction * m_dy_behind + (fraction > 0.0) * fraction * m_dy_ahead + m_value;
}
double InterpolatableLinear::Derivative(double fraction) const
{
    if (fraction >= 0)
        return m_dy_ds_ahead;
    else
        return m_dy_ds_behind;
}
void InterpolatableLinear::GetAdjacentPoint(int index_difference, double ds, const Interpolatable& other)
{
    if (index_difference < -1)
        return;

    if (index_difference > 1)
        return;

    if (index_difference == 0)
        throw std::runtime_error("A point with index_different == 0 is not an GetAdjacentPoint(). Use self().");

    if (index_difference > 0)
        --index_difference;

    m_values[index_difference + 1] = other.GetValue();
    m_dss[index_difference + 1] = ds;
    m_was_set[index_difference + 1] = true;

    m_ready = false;
}
void InterpolatableLinear::Finalize(std::function<double(const double&, const double&)> difference_function)
{
    if (m_was_set[0]) {
        m_dy_behind = difference_function(m_value, m_values[0]);
        m_dy_ds_behind = m_dy_behind / m_dss[0];
    }

    if (m_was_set[1]) {
        m_dy_ahead = difference_function(m_values[1], m_value);
        m_dy_ds_ahead = m_dy_ahead / m_dss[1];
    }

    m_ready = true;
}
}  //namespace mpmca::predict::inertial