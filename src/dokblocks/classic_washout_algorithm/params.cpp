/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/dokblocks/classic_washout_algorithm/params.hpp"

#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>

#include "mpmca/json_tools.hpp"

namespace mpmca::dokblocks::classic_washout_algorithm {
nlohmann::json Params::ReadJson(std::string const& filename)
{
    nlohmann::json j;
    std::ifstream(filename) >> j;
    return j;
}

Params::Params(utilities::ConfigurationPtr config)
    : m_gravity_world(config->GetValue<Vector<3>>("GravityWorldVector", Vector<3>::Zero()))
    , m_acceleration_gain(config->GetValue<Vector<3>>("AccelerationGain", Vector<3>::Zero()))
    , m_angular_velocity_gain(config->GetValue<Vector<3>>("AngularVelocityGain", Vector<3>::Zero()))
    , m_acceleration_split_omega_x(config->GetValue<Real>("AccelerationSplitHighPassXOmega", 0.7))
    , m_acceleration_split_omega_y(config->GetValue<Real>("AccelerationSplitHighPassYOmega", 0.7))
    , m_acceleration_split_omega_z(config->GetValue<Real>("AccelerationSplitHighPassZOmega", 0.7))
    , m_acceleration_washout_high_pass_omega_x(config->GetValue<Real>("AccelerationWashoutHighPassOmegaX", 0.7))
    , m_acceleration_washout_high_pass_omega_y(config->GetValue<Real>("AccelerationWashoutHighPassOmegaY", 0.7))
    , m_acceleration_washout_high_pass_omega_z(config->GetValue<Real>("AccelerationWashoutHighPassOmegaZ", 0.7))
    , m_acceleration_washout_high_pass_zeta_x(config->GetValue<Real>("AccelerationWashoutHighPassZetaX", 0.7))
    , m_acceleration_washout_high_pass_zeta_y(config->GetValue<Real>("AccelerationWashoutHighPassZetaY", 0.7))
    , m_acceleration_washout_high_pass_zeta_z(config->GetValue<Real>("AccelerationWashoutHighPassZetaZ", 0.7))
    , m_tilt_coordination_maximum_roll(config->GetValue<Real>("TiltCoordinationMaximumRoll", 0.7))
    , m_tilt_coordination_maximum_pitch(config->GetValue<Real>("TiltCoordinationMaximumPitch", 0.7))
    , m_tilt_coordination_maximum_roll_rate(config->GetValue<Real>("TiltCoordinationMaximumRollRate", 0.7))
    , m_tilt_coordination_maximum_pitch_rate(config->GetValue<Real>("TiltCoordinationMaximumPitchRate", 0.7))
    , m_euler_rates_washout_high_pass_omega_roll(config->GetValue<Real>("EulerRatesWashoutHighPassOmegaRoll", 0.7))
    , m_euler_rates_washout_high_pass_omega_pitch(config->GetValue<Real>("EulerRatesWashoutHighPassOmegaPitch", 0.7))
    , m_euler_rates_washout_high_pass_omega_yaw(config->GetValue<Real>("EulerRatesWashoutHighPassOmegaYaw", 0.7))
    , m_tilt_coordination_low_pass_omega(config->GetValue<Real>("TiltCoordinationLowPassOmega", 0.7))
    , m_tilt_coordination_low_pass_zeta(config->GetValue<Real>("TiltCoordinationLowPassZeta", 0.7))
{
}

Params::Params()
    : m_gravity_world(Vector<3>::Zero())
    , m_acceleration_gain(Vector<3>::Ones())
    , m_angular_velocity_gain(Vector<3>::Ones())
    , m_acceleration_split_omega_x(1.5)
    , m_acceleration_split_omega_y(1.5)
    , m_acceleration_split_omega_z(1.5)
    , m_acceleration_washout_high_pass_omega_x(2.0)
    , m_acceleration_washout_high_pass_omega_y(2.0)
    , m_acceleration_washout_high_pass_omega_z(2.0)
    , m_acceleration_washout_high_pass_zeta_x(1.0)
    , m_acceleration_washout_high_pass_zeta_y(1.0)
    , m_acceleration_washout_high_pass_zeta_z(1.0)
    , m_tilt_coordination_maximum_roll(1.3)
    , m_tilt_coordination_maximum_pitch(1.3)
    , m_tilt_coordination_maximum_roll_rate(10.1)
    , m_tilt_coordination_maximum_pitch_rate(10.1)
    , m_euler_rates_washout_high_pass_omega_roll(2.0)
    , m_euler_rates_washout_high_pass_omega_pitch(2.0)
    , m_euler_rates_washout_high_pass_omega_yaw(2.0)
    , m_tilt_coordination_low_pass_omega(10)
    , m_tilt_coordination_low_pass_zeta(1.)
{
    m_gravity_world[2] = 9.81;
}

Params::Params(std::string const& filename)
    : Params(ReadJson(filename))
{
}

Params::Params(nlohmann::json const& j)
    : m_gravity_world(j["g"].get<Vector<3>>())
    , m_acceleration_gain(j["Ka"].get<Vector<3>>())
    , m_angular_velocity_gain(j["Kw"].get<Vector<3>>())
    , m_acceleration_split_omega_x(j["w_high_x"])
    , m_acceleration_split_omega_y(j["w_high_y"])
    , m_acceleration_split_omega_z(j["w_high_z"])
    , m_acceleration_washout_high_pass_omega_x(j["w0_x"])
    , m_acceleration_washout_high_pass_omega_y(j["w0_y"])
    , m_acceleration_washout_high_pass_omega_z(j["w0_z"])
    , m_acceleration_washout_high_pass_zeta_x(j["eps_x"])
    , m_acceleration_washout_high_pass_zeta_y(j["eps_y"])
    , m_acceleration_washout_high_pass_zeta_z(j["eps_z"])
    , m_tilt_coordination_maximum_roll(j["beta_max_tc"])
    , m_tilt_coordination_maximum_pitch(j["beta_max_tc"])
    , m_tilt_coordination_maximum_roll_rate(j["beta_dot_max_tc"])
    , m_tilt_coordination_maximum_pitch_rate(j["beta_dot_max_tc"])
    , m_euler_rates_washout_high_pass_omega_roll(j["w0_roll"])
    , m_euler_rates_washout_high_pass_omega_pitch(j["w0_pitch"])
    , m_euler_rates_washout_high_pass_omega_yaw(j["w0_yaw"])
    , m_tilt_coordination_low_pass_omega(j["w_tc"])
    , m_tilt_coordination_low_pass_zeta(j["eps_tc"])
{
}

Vector<3> const& Params::GetGravityVector() const noexcept
{
    return m_gravity_world;
}

Vector<3> const& Params::GetAccelerationGain() const noexcept
{
    return m_acceleration_gain;
}

Vector<3> const& Params::GetAngularVelocityGain() const noexcept
{
    return m_angular_velocity_gain;
}

Real Params::GetAccelerationSplitOmegaX() const noexcept
{
    return m_acceleration_split_omega_x;
}

Real Params::GetAccelerationSplitOmegaY() const noexcept
{
    return m_acceleration_split_omega_y;
}

Real Params::GetAccelerationSplitOmegaZ() const noexcept
{
    return m_acceleration_split_omega_z;
}

Real Params::GetAccelerationWashoutHighPassZetaX() const noexcept
{
    return m_acceleration_washout_high_pass_zeta_x;
}

Real Params::GetAccelerationWashoutHighPassZetaY() const noexcept
{
    return m_acceleration_washout_high_pass_zeta_y;
}

Real Params::GetAccelerationWashoutHighPassZetaZ() const noexcept
{
    return m_acceleration_washout_high_pass_zeta_z;
}

Real Params::GetAccelerationWashoutHighPassOmegaX() const noexcept
{
    return m_acceleration_washout_high_pass_omega_x;
}

Real Params::GetAccelerationWashoutHighPassOmegaY() const noexcept
{
    return m_acceleration_washout_high_pass_omega_y;
}

Real Params::GetAccelerationWashoutHighPassOmegaZ() const noexcept
{
    return m_acceleration_washout_high_pass_omega_z;
}

Real Params::GetTiltCoordinationMaximumRoll() const noexcept
{
    return m_tilt_coordination_maximum_roll;
}

Real Params::GetTiltCoordinationMaximumPitch() const noexcept
{
    return m_tilt_coordination_maximum_pitch;
}

Real Params::GetTiltCoordinationMaximumRollRate() const noexcept
{
    return m_tilt_coordination_maximum_roll_rate;
}

Real Params::GetTiltCoordinationMaximumPitchRate() const noexcept
{
    return m_tilt_coordination_maximum_pitch_rate;
}

Real Params::GetEulerRatesWashoutHighPassOmegaRoll() const noexcept
{
    return m_euler_rates_washout_high_pass_omega_roll;
}

Real Params::GetEulerRatesWashoutHighPassOmegaPitch() const noexcept
{
    return m_euler_rates_washout_high_pass_omega_pitch;
}

Real Params::GetEulerRatesWashoutHighPassOmegaYaw() const noexcept
{
    return m_euler_rates_washout_high_pass_omega_yaw;
}

Real Params::GetTiltCoordinationLowPassOmegaRoll() const noexcept
{
    return m_tilt_coordination_low_pass_omega;
}

Real Params::GetTiltCoordinationLowPassZetaRoll() const noexcept
{
    return m_tilt_coordination_low_pass_zeta;
}

Real Params::GetTiltCoordinationLowPassOmegaPitch() const noexcept
{
    return m_tilt_coordination_low_pass_omega;
}

Real Params::GetTiltCoordinationLowPassZetaPitch() const noexcept
{
    return m_tilt_coordination_low_pass_zeta;
}
}  //namespace mpmca::dokblocks::classic_washout_algorithm