/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/dokblocks/core/time_series.hpp"

#include <stdexcept>

namespace mpmca::dokblocks {

TimeSeries::TimeSeries(const std::string& name)
    : m_name(name)
{
}
double TimeSeries::GetTimeAtIndex(size_t index) const
{
    try {
        return m_time_vector.at(index);
    }
    catch (const std::exception& exception) {
        throw std::runtime_error("Signal at index " + std::to_string(index) + " was not set for TimeSeries with name " +
                                 m_name);
    }
    return 0;
}
double TimeSeries::GetSignalAtIndex(size_t index) const
{
    try {
        return m_data_vector.at(index);
    }
    catch (const std::exception& exception) {
        throw std::runtime_error("Signal at index " + std::to_string(index) + " was not set for TimeSeries with name " +
                                 m_name);
    }
    return 0;
}
void TimeSeries::SetSignalAtIndex(size_t index, double time, double value)
{
    m_time_vector.resize(index + 1);
    m_time_vector.at(index) = time;

    m_data_vector.resize(index + 1);
    m_data_vector.at(index) = value;
}
size_t TimeSeries::GetSignalLength() const
{
    return m_time_vector.size();
}
const std::vector<double>& TimeSeries::GetTimeVector() const
{
    return m_time_vector;
}
const std::vector<double>& TimeSeries::GetDataVector() const
{
    return m_data_vector;
}
}  //namespace mpmca::dokblocks