/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/dokblocks/components/low_pass_second_order.hpp"

#include "mpmca/dokblocks/core/conversions.hpp"

namespace mpmca::dokblocks {

LowPassSecondOrder::LowPassSecondOrder(const std::string& name, Real zeta, Real omega)
    : StateSpace<2, 1, 1>(
          tf2ss(name, std::array<Real, 1>{{omega * omega}}, std::array<Real, 3>{{1, 2. * omega * zeta, omega * omega}}))
{
}

}  //namespace mpmca::dokblocks