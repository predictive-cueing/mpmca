/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/dokblocks/components/high_pass_first_order.hpp"

#include "mpmca/dokblocks/core/conversions.hpp"

namespace mpmca::dokblocks {

HighPassFirstOrder::HighPassFirstOrder(const std::string& name, Real omega)
    : StateSpace<1, 1, 1>(tf2ss(name, std::array<Real, 2>{{1, 0}}, std::array<Real, 2>{{1, omega}}))
{
}

}  //namespace mpmca::dokblocks