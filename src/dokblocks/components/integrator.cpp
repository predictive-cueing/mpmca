/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/dokblocks/components/integrator.hpp"

#include "mpmca/dokblocks/core/conversions.hpp"

namespace mpmca::dokblocks {
Integrator::Integrator(const std::string& name)
    : StateSpace<1, 1, 1>(tf2ss(name, std::array<Real, 1>{{1}}, std::array<Real, 2>{{1, 0}}))
{
}

}  //namespace mpmca::dokblocks