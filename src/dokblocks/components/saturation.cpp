/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/dokblocks/components/saturation.hpp"

namespace mpmca::dokblocks {
Saturation::Saturation(const std::string& name, Real lower_limit, Real upper_limit)
    : AlgebraicFunction<1, 1>(name, [lower_limit = lower_limit, upper_limit = upper_limit](const Vector<1>& u) {
        return u.cwiseMin(upper_limit).cwiseMax(lower_limit);
    })
{
}

}  //namespace mpmca::dokblocks