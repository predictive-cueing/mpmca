/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/dokblocks/components/constant.hpp"

namespace mpmca::dokblocks {
Constant::Constant(const std::string& name, Real k) noexcept
    : BASE(name)
    , m_constant(k)
{
    this->SetOutputSignalNames({{name}});
}

typename Constant::StateVector Constant::GetStateImpl() const
{
    return StateVector::Zero();
}

void Constant::SetSimulationStepImpl(Real dt)
{
}

void Constant::UpdateOutputImpl(InputVector const& u)
{
    this->m_output = m_constant;
}

void Constant::UpdateStateImpl(InputVector const& u)
{
}

bool Constant::IsDirectFeedThroughImpl() const
{
    return false;
}
std::vector<std::string> Constant::GetMermaidDiagramStringImpl() const
{
    return {this->GetUniqueId() + "[" + this->GetName() + "]"};
}
}  //namespace mpmca::dokblocks