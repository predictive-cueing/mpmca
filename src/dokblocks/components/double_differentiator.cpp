/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/dokblocks/components/double_differentiator.hpp"

#include "mpmca/dokblocks/core/conversions.hpp"
namespace mpmca::dokblocks {

DoubleDifferentiator::DoubleDifferentiator(const std::string& name, Real omega)
    : StateSpace<2, 1, 1>(
          tf2ss(name, std::array<Real, 3>{{omega * omega, 0, 0}}, std::array<Real, 3>{{1, 2 * omega, omega * omega}}))
{
}

}  //namespace mpmca::dokblocks