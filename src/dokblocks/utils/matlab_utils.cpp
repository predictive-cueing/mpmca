/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/dokblocks/utils/matlab_utils.hpp"

#include <fstream>
#include <iostream>
#include <numeric>
#include <vector>

#include "mpmca/dokblocks/core/matrix_types.hpp"

namespace mpmca::dokblocks::utils {

namespace {

uint16_t ReadNumberOfDimensions(std::ifstream& infile)
{
    uint16_t* uint16_ptr = (uint16_t*)malloc(sizeof(uint16_t));
    infile.read((char*)uint16_ptr, sizeof(uint16_t));
    uint16_t N_dims = uint16_ptr[0];

    return N_dims;
}

std::vector<uint64_t> ReadDimensions(std::ifstream& infile, const uint16_t N_dims)
{
    uint64_t* uint64_ptr = (uint64_t*)malloc(N_dims * sizeof(uint64_t));
    infile.read((char*)uint64_ptr, N_dims * sizeof(uint64_t));

    std::vector<uint64_t> dims;
    for (auto k = 0; k < N_dims; ++k)
        dims.emplace_back(uint64_ptr[k]);

    return dims;
}

DynamicMatrix ReadData(std::ifstream& infile, const uint16_t N_dims, std::vector<uint64_t> const& dims)
{
    uint64_t N_rows, N_cols;

    if (N_dims == 1) {
        N_rows = dims[0];
        N_cols = 1;
    }
    else if (N_dims == 2) {
        N_rows = dims[0];
        N_cols = dims[1];
    }
    else
        throw std::logic_error(
            "Not implemented functionality: the specified file contains data with more than 2 dimensions.");

    uint64_t Ndata = std::accumulate(dims.cbegin(), dims.cend(), 1, std::multiplies<uint64_t>());
    double* double_ptr = (double*)malloc(Ndata * sizeof(double));
    infile.read((char*)double_ptr, Ndata * sizeof(double));

    DynamicMatrix mtx(N_rows, N_cols);
    for (auto col_k = 0; col_k < dims[1]; ++col_k)
        for (auto row_k = 0; row_k < dims[0]; ++row_k)
            mtx(row_k, col_k) = double_ptr[col_k * dims[0] + row_k];

    return mtx;
}

std::vector<std::vector<double>> ReadDataStdVector(std::ifstream& infile, const uint16_t N_dims,
                                                   std::vector<uint64_t> const& dims)
{
    uint64_t Ndata = std::accumulate(dims.cbegin(), dims.cend(), 1, std::multiplies<uint64_t>());
    double* double_ptr = (double*)malloc(Ndata * sizeof(double));
    infile.read((char*)double_ptr, Ndata * sizeof(double));

    std::vector<std::vector<double>> mtx;  //(N_rows, N_cols);
    for (auto col_k = 0; col_k < dims[1]; ++col_k) {
        mtx.push_back(std::vector<double>{});
        for (auto row_k = 0; row_k < dims[0]; ++row_k) {
            mtx.back().push_back(double_ptr[col_k * dims[0] + row_k]);
        }
    }

    return mtx;
}
}  //namespace

DynamicMatrix ReadBinary(std::string const& fname)
{
    std::ifstream infile;
    infile.exceptions(std::ifstream::failbit);
    infile.open(fname.c_str(), std::ios::in | std::ios::binary);

    const auto N_dims = ReadNumberOfDimensions(infile);
    const auto dims = ReadDimensions(infile, N_dims);
    auto data = ReadData(infile, N_dims, dims);

    infile.close();

    return data;
}

std::vector<std::vector<double>> ReadBinaryStdVector(std::string const& fname)
{
    std::ifstream infile;
    infile.exceptions(std::ifstream::failbit);
    infile.open(fname.c_str(), std::ios::in | std::ios::binary);

    const uint16_t N_dims = ReadNumberOfDimensions(infile);
    const std::vector<uint64_t> dims = ReadDimensions(infile, N_dims);

    std::cout << "N_dims: " << N_dims << std::endl;

    for (auto dim : dims) {
        std::cout << "dim: " << dim << std::endl;
    }

    auto data = ReadDataStdVector(infile, N_dims, dims);

    infile.close();

    return data;
}

}  //namespace mpmca::dokblocks::utils