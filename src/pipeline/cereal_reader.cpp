/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/cereal_reader.hpp"

namespace mpmca::pipeline {

CerealReader::CerealReader(const std::string& filename)
    : m_filename(filename)
    , m_os(filename, std::ios::binary | std::ios::ate)
    , m_file_size(m_os.tellg())
    , m_archive(m_os)
{
    m_os.seekg(0, std::ios::beg);
    ReadFile();
}
void CerealReader::ReadFile()
{
    while (!m_os.eof() && m_os.tellg() < m_file_size) {
        cereal_types::bus_message_type_id message_type_id;
        cereal_types::time_ms_type time_stamp_ms;
        uint64_t message_length;
        m_archive(message_type_id, time_stamp_ms, message_length);
        m_message_start_address[message_type_id][time_stamp_ms] = m_os.tellg();
        m_os.ignore(message_length);
    }
}
void CerealReader::AddMessageAddress(cereal_types::bus_message_type_id message_type_id,
                                     cereal_types::time_ms_type time_ms, cereal_types::memory_address_type address)
{
    m_message_start_address[message_type_id].insert({time_ms, address});
}
const std::string& CerealReader::GetFilename() const
{
    return m_filename;
}
bool CerealReader::HasMessageType(cereal_types::bus_message_type_id message_type_id) const
{
    return m_message_start_address.count(message_type_id) == 1;
}
bool CerealReader::HasMessageOfTypeAtTime(cereal_types::bus_message_type_id message_type_id,
                                          cereal_types::time_ms_type time_ms) const
{
    return HasMessageType(message_type_id) && m_message_start_address.at(message_type_id).count(time_ms) == 1;
}
}  //namespace mpmca