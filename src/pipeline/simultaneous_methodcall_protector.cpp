/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/simultaneous_method_call_protector.hpp"

namespace mpmca::pipeline {

template <>
SimultaneousMethodCallProtector<SafetyLevel, SafetyLevel::kFail>::~SimultaneousMethodCallProtector()
{
    if (m_bool_switch.load())
        m_safety.Fail();
}

template <>
SimultaneousMethodCallProtector<SafetyLevel, SafetyLevel::kCritical>::~SimultaneousMethodCallProtector()
{
    if (m_bool_switch.load())
        m_safety.Critical();
}
}  //namespace mpmca