/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/safety.hpp"

#include <string>
namespace mpmca::pipeline {
class Safety::Impl
{
  public:
    Safety MakeChild(const std::string& name, Safety* parent);
    Safety* MakeChildPtr(const std::string& name, Safety* parent);
    bool HasParent() const;

    void Fail();
    void Critical();
    void Prepared();

    void PrintChildren(const std::string& prefix) const;
    void ListChildren(std::string& ret_string, const std::string& prefix) const;
    SafetyLevel GetLevel() const;
    std::string GetName() const;
    bool GetGuarded() const;

    Impl(const std::string& name, Safety* me, bool guarded);
    Impl(const std::string& name, Safety* parent, Safety* me, bool guarded);
    ~Impl();

    void AddChild(Safety* child);
    void DeleteChild(Safety* child);

    Safety* m_me;
    std::string m_name;
    SafetyLevel m_level;
    Safety* m_parent;
    std::vector<Safety*> m_children;
    const bool m_guarded;

    void ChildLevel(SafetyLevel level);
    void ParentLevel(SafetyLevel level);
    void SetLevel(SafetyLevel level, bool send_to_parent, bool send_to_children);
    size_t NumChildren() const;
    bool AllChildrenSafe() const;
    void DeleteChildren();
    void CleanUp(Safety* me);
    void SendToChildren(SafetyLevel level);
};

Safety::Impl::Impl(const std::string& name, Safety* me, bool guarded)
    : Impl(name, nullptr, me, guarded)
{
}

void Safety::Impl::SendToChildren(SafetyLevel level)
{
    for (auto& c : m_children)
        c->m_pimpl->ParentLevel(level);
}

void Safety::Impl::SetLevel(SafetyLevel level, bool send_to_parent, bool send_to_child)
{
    if (level == SafetyLevel::kCritical) {
        m_level = SafetyLevel::kCritical;
        if (send_to_child)
            SendToChildren(SafetyLevel::kCritical);
    }
    else if (level == SafetyLevel::kFail && m_level != SafetyLevel::kCritical) {
        m_level = SafetyLevel::kFail;
        if (send_to_child)
            SendToChildren(SafetyLevel::kFail);
    }
    else if (level == SafetyLevel::kUnprepared && m_level != SafetyLevel::kFail) {
        m_level = SafetyLevel::kUnprepared;
    }
    else if (level == SafetyLevel::kSafe && m_level == SafetyLevel::kUnprepared && AllChildrenSafe()) {
        m_level = SafetyLevel::kSafe;
    }

    if (m_parent && send_to_parent)
        m_parent->m_pimpl->ChildLevel(m_level);
}

Safety::Impl::Impl(const std::string& name, Safety* parent, Safety* me, bool guarded)
    : m_me(me)
    , m_name(name)
    , m_level(SafetyLevel::kUnprepared)
    , m_parent(parent)
    , m_guarded(guarded)
{
    if (parent)
        parent->m_pimpl->AddChild(me);
}
bool Safety::Impl::HasParent() const
{
    return m_parent != nullptr;
}
bool Safety::Impl::GetGuarded() const
{
    return m_guarded;
}

void Safety::Impl::Critical()
{
    SetLevel(SafetyLevel::kCritical, true, true);
}
void Safety::Impl::Fail()
{
    SetLevel(SafetyLevel::kFail, true, true);
}
void Safety::Impl::Prepared()
{
    SetLevel(SafetyLevel::kSafe, false, false);
}
size_t Safety::Impl::NumChildren() const
{
    return m_children.size();
}
SafetyLevel Safety::Impl::GetLevel() const
{
    return m_level;
}
void Safety::Impl::ListChildren(std::string& total_string, const std::string& prefix) const
{
    total_string = total_string + prefix + " " + m_name + " : " + ToString(m_level) + "\n";

    for (auto& c : m_children)
        c->m_pimpl->ListChildren(total_string, prefix + "\t");
}
void Safety::Impl::PrintChildren(const std::string& prefix) const
{
    std::cout << prefix.c_str() << " " << m_name << " : " << m_level << std::endl;
    for (auto& c : m_children)
        c->m_pimpl->PrintChildren(prefix + "\t");
}
std::string Safety::Impl::GetName() const
{
    return m_name;
}

void Safety::Impl::AddChild(Safety* child)
{
    m_children.push_back(child);
}

void Safety::Impl::DeleteChild(Safety* child)
{
    m_children.erase(std::remove_if(m_children.begin(), m_children.end(), [child](Safety* i) { return i == child; }),
                     m_children.end());
}

void Safety::Impl::ChildLevel(SafetyLevel level)
{
    if (level == SafetyLevel::kCritical) {
        SetLevel(SafetyLevel::kCritical, true, true);
    }
    else if (level == SafetyLevel::kFail) {
        SetLevel(SafetyLevel::kFail, true, true);
    }
    else if (level == SafetyLevel::kUnprepared) {
        if (level == SafetyLevel::kSafe)
            SetLevel(SafetyLevel::kUnprepared, false, false);
    }
    else if (level == SafetyLevel::kSafe) {
        // do nothing
    }
}

void Safety::Impl::ParentLevel(SafetyLevel level)
{
    if (level == SafetyLevel::kFail) {
        SetLevel(SafetyLevel::kFail, false, true);
    }
}

Safety::Impl::~Impl()
{
    if (m_parent)
        m_parent->m_pimpl->DeleteChild(m_me);
}

bool Safety::Impl::AllChildrenSafe() const
{
    return std::all_of(m_children.cbegin(), m_children.cend(),
                       [](Safety* i) { return i->GetLevel() == SafetyLevel::kSafe; });
}

Safety Safety::Impl::MakeChild(const std::string& name, Safety* parent)
{
    return Safety(name, parent, m_guarded);
}
Safety* Safety::Impl::MakeChildPtr(const std::string& name, Safety* parent)
{
    return new Safety(name, parent, m_guarded);
}

/*
 * Safety::Safety
 *
 * */

Safety::~Safety() = default;
Safety::Safety(Safety&&) = default;
Safety& Safety::operator=(Safety&&) = default;

Safety::Safety(const std::string& name, bool guarded)
    : m_pimpl(new Impl(name, this, guarded))
{
}
Safety::Safety(const std::string& name, Safety* parent, bool guarded)
    : m_pimpl(new Impl(name, parent, this, guarded))
{
}
Safety Safety::MakeChild(const std::string& name) &
{
    return m_pimpl->MakeChild(name, this);
}
Safety* Safety::MakeChildPtr(const std::string& name) &
{
    return m_pimpl->MakeChildPtr(name, this);
}
Safety Safety::DoSomethingUnsafe(const std::string& name, bool guarded)
{
    return Safety(name, guarded);
}
void Safety::PrintChildren() const
{
    m_pimpl->PrintChildren(std::string(""));
}
std::string Safety::ListChildren() const
{
    std::string ret_string;
    m_pimpl->ListChildren(ret_string, std::string(""));
    return ret_string;
}
std::string Safety::GetName() const
{
    return m_pimpl->GetName();
}
bool Safety::GetGuarded() const
{
    return m_pimpl->GetGuarded();
}
void Safety::Fail()
{
    m_pimpl->Fail();
}
void Safety::Critical()
{
    m_pimpl->Critical();
}
void Safety::Prepared()
{
    m_pimpl->Prepared();
}
bool Safety::HasParent() const
{
    return m_pimpl->HasParent();
}
SafetyLevel Safety::GetLevel() const
{
    return m_pimpl->GetLevel();
}
std::string Safety::GetLevelName() const
{
    return ToString(m_pimpl->GetLevel());
}
}  //namespace mpmca