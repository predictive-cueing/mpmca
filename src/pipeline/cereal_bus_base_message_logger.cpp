/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/cereal_bus_base_message_logger.hpp"

namespace mpmca::pipeline {

CerealBusBaseMessageLogger::CerealBusBaseMessageLogger(const std::string& filename, bool log_all_messages)
    : CerealLogger(filename)
    , m_log_all_messages(log_all_messages)
{
}

void CerealBusBaseMessageLogger::WriteBusBaseMessage(int64_t current_time_ms, const BusBaseMessage& bus_base_message)
{
    bus_base_message.SerializeToArchive(m_buffered_archive);
    DoWriteMessage(current_time_ms, bus_base_message.GetTypeId());
}

void CerealBusBaseMessageLogger::WriteBusBaseMessages(
    cereal_types::time_ms_type current_time_ms,
    const std::map<cereal_types::bus_message_type_id, std::shared_ptr<BusBaseMessage>> bus_base_messages)
{
    for (auto& [message_type_id, bus_base_message_ptr] : bus_base_messages) {
        if (IsMessageToLog(message_type_id, bus_base_message_ptr->GetTypeName())) {
            WriteBusBaseMessage(current_time_ms, *bus_base_message_ptr);
        }
    }
}

bool CerealBusBaseMessageLogger::IsMessageToLog(cereal_types::bus_message_type_id message_type_id,
                                                const std::string& message_type_name)
{
    if (m_include_message_type_id.count(message_type_id) == 1) {
        return m_include_message_type_id[message_type_id];
    }
    else {
        // do something slow, but only once
        if (m_log_all_messages) {
            m_include_message_type_id.insert(
                {message_type_id, m_message_type_name_to_exclude.count(message_type_name) == 0});
        }
        else {
            m_include_message_type_id.insert(
                {message_type_id, m_message_type_name_to_include.count(message_type_name) == 1});
        }

        return IsMessageToLog(message_type_id, message_type_name);
    }
}
void CerealBusBaseMessageLogger::AddMessageTypeToInclude(uint32_t message_type_id)
{
    m_include_message_type_id.insert({message_type_id, true});
}
void CerealBusBaseMessageLogger::AddMessageTypeToExclude(uint32_t message_type_id)
{
    m_include_message_type_id.insert({message_type_id, false});
}

void CerealBusBaseMessageLogger::AddMessageNameToInclude(const std::string& message_name)
{
    m_message_type_name_to_include.insert(message_name);
}
void CerealBusBaseMessageLogger::AddMessageNameToExclude(const std::string& message_name)
{
    m_message_type_name_to_exclude.insert(message_name);
}
}  //namespace mpmca