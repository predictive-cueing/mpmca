/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/state_machine_client.hpp"

#include <stdexcept>

#include "mpmca/pipeline/state_machine.hpp"
namespace mpmca::pipeline {
StateMachineClient::StateMachineClient(const std::string& name, StateMachine* machine)
    : m_name(name)
    , m_current_state(State::kNone)
    , m_go_to_state(State::kNone)
    , m_state_machine(machine)
{
}
void StateMachineClient::SetCurrentState(const State& current_state)
{
    m_current_state = current_state;
    m_go_to_state = State::kNone;
}
const std::string& StateMachineClient::GetName() const
{
    return m_name;
}
const State& StateMachineClient::GetCurrentState() const
{
    return m_current_state;
}
bool StateMachineClient::IsCurrentStateTransitionState() const
{
    return m_current_state == State::kBooting ||  //
           m_current_state == State::kFailing ||  //
           m_current_state == State::kFailTerminating ||  //
           m_current_state == State::kPreparing ||  //
           m_current_state == State::kRecovering ||  //
           m_current_state == State::kResuming ||  //
           m_current_state == State::kStarting ||  //
           m_current_state == State::kStopping ||  //
           m_current_state == State::kTerminating;
}
bool StateMachineClient::IsCurrentStateFailState() const
{
    return m_current_state == State::kFail ||  //
           m_current_state == State::kFailed ||  //
           m_current_state == State::kFailing ||  //
           m_current_state == State::kFailTerminating ||  //
           m_current_state == State::kFailTerminated ||  //
           m_current_state == State::kFailTerminate;
}
const State& StateMachineClient::GetGoToState() const
{
    return m_go_to_state;
}
void StateMachineClient::SetGoToState(const State& go_to_state)
{
    m_go_to_state = go_to_state;
}
int StateMachineClient::GetNumberClientsNotInState(const State& state) const
{
    if (!m_state_machine)
        throw(std::runtime_error("The pointer to my parent state machine was not set..."));

    return m_state_machine->GetNumberClientsNotInState(state);
}
}  //namespace mpmca::pipeline