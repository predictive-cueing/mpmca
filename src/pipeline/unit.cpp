/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/unit.hpp"

#include "mpmca/pipeline/data_bucket.hpp"
#include "mpmca/pipeline/pipeline.hpp"
#include "mpmca/pipeline/task.hpp"

namespace mpmca::pipeline {
Unit::Unit(const std::string& name, const std::string& type_name, Safety& parent_safety,
           StateMachine& parent_state_machine, int execution_order, int maximum_computation_time_us)
    : m_logger(name, type_name)
    , m_name(name)
    , m_type_name(type_name)
    , m_execution_order(execution_order)
    , m_maximum_computation_time_us(1000)
    , m_safety_ptr(parent_safety.MakeChildPtr(name))
    , m_state_machine_client_ptr(parent_state_machine.MakeClientPtr(name))
{
}
Unit::Unit(const std::string& name, const std::string& type_name, Safety& parent_safety,
           StateMachine& parent_state_machine, utilities::ConfigurationPtr config)
    : Unit(name, type_name, parent_safety, parent_state_machine,  //
           config->GetValue<int>("ExecutionOrder", 1),  //
           config->GetKeyExists("MaximumComputationTimeUs") ?  //
               config->GetValue<int>("MaximumComputationTimeUs", 1000)
                                                            : 1000)
{
}
Unit::~Unit()
{
}
void Unit::GoToIdleAndTerminate(bool condition)
{
    if (condition && GetStateMachineClient().GetCurrentState() == State::kRun)
        GetStateMachineClient().SetGoToState(State::kStopping);

    if (condition && GetStateMachineClient().GetCurrentState() == State::kIdle)
        GetStateMachineClient().SetGoToState(State::kTerminating);
}
void Unit::TransitionStateMachine()
{
    if (m_state_machine_client_ptr)
        StateMachine::TransitionStateMachineClient(*m_state_machine_client_ptr);
}
const Safety& Unit::GetSafety() const
{
    if (m_safety_ptr) {
        return *m_safety_ptr;
    }
    else {
        throw std::runtime_error("Safety pointer not set yet!");
    }
}
Safety& Unit::GetSafety()
{
    if (m_safety_ptr) {
        return *m_safety_ptr;
    }
    else {
        throw std::runtime_error("Safety pointer not set yet!");
    }
}
bool Unit::operator<(const Unit& other) const
{
    return (m_execution_order < other.GetExecutionOrder());
}
void Unit::SetExecutionOrder(int priority)
{
    if (GetSafety().GetLevel() != SafetyLevel::kUnprepared)
        throw std::runtime_error("You can only change the priority of an Unit before Pipeline::Prepare() is called.");

    m_execution_order = priority;
}
int Unit::GetExecutionOrder() const
{
    return m_execution_order;
}
const std::string& Unit::GetName() const
{
    return m_name;
}
const std::string& Unit::GetTypeName() const
{
    return m_type_name;
}
void Unit::SetStateMachineClient(StateMachineClient* state_machine_client)
{
    m_state_machine_client_ptr = state_machine_client;
}
void Unit::SetSafety(Safety* safety)
{
    m_safety_ptr = safety;
}
const StateMachineClient& Unit::GetStateMachineClient() const
{
    if (m_state_machine_client_ptr) {
        return *m_state_machine_client_ptr;
    }
    else {
        throw std::runtime_error("State machine client pointer not set yet!");
    }
}
StateMachineClient& Unit::GetStateMachineClient()
{
    if (m_state_machine_client_ptr) {
        return *m_state_machine_client_ptr;
    }
    else {
        throw std::runtime_error("State machine client pointer not set yet!");
    }
}
void Unit::GuardedPrepare()
{
    const std::lock_guard<std::mutex> lock(m_data_mutex);

    if (m_safety_ptr->GetGuarded()) {
        if (m_safety_ptr->GetLevel() == SafetyLevel::kUnprepared) {
            try {
                Prepare();
            }
            catch (const std::exception& c) {
                m_safety_ptr->Fail();
                m_logger.Error(c);
            }
        }
    }
    else {
        Prepare();
    }
}
void Unit::GuardedSafeTick()
{
    try {
        SafeTick();
    }
    catch (const std::exception& c) {
        m_safety_ptr->Fail();
        m_logger.Error(c);
    }
}
void Unit::GuardedTick(MessageBus& in_output_message_bus)
{
    const std::lock_guard<std::mutex> lock(m_data_mutex);

    if (m_safety_ptr->GetGuarded()) {
        if (m_safety_ptr->GetLevel() == SafetyLevel::kSafe) {
            try {
                Tick(in_output_message_bus);
            }
            catch (const std::exception& c) {
                m_safety_ptr->Fail();
                m_logger.Error(c);
                GuardedSafeTick();
            }
        }
        else if (m_safety_ptr->GetLevel() == SafetyLevel::kFail) {
            GuardedSafeTick();
        }
    }
    else {
        Tick(in_output_message_bus);
    }
}
void Unit::GuardedTaskCompleted(const DataBucket& data_bucket)
{
    const std::lock_guard<std::mutex> lock(m_data_mutex);

    if (m_safety_ptr->GetGuarded()) {
        if (m_safety_ptr->GetLevel() == SafetyLevel::kSafe) {
            try {
                TaskCompleted(data_bucket);
            }
            catch (const std::exception& c) {
                m_safety_ptr->Fail();
                m_logger.Error(c);
            }
        }
    }
    else {
        TaskCompleted(data_bucket);
    }
}
void Unit::GuardedMainTick(DataBucket& data_bucket)
{
    const std::lock_guard<std::mutex> lock(m_data_mutex);

    if (m_safety_ptr->GetGuarded()) {
        if (m_safety_ptr->GetLevel() == SafetyLevel::kSafe) {
            try {
                MainTick(data_bucket);
            }
            catch (const std::exception& c) {
                m_safety_ptr->Fail();
                m_logger.Error(c);
            }
        }
    }
    else {
        MainTick(data_bucket);
    }
}
void Unit::SetMaximumComputationTimeUs(int time_us)
{
    m_maximum_computation_time_us = std::chrono::microseconds{time_us};
}

const std::chrono::microseconds& Unit::GetMaximumComputationTimeUs() const
{
    return m_maximum_computation_time_us;
}
}  //namespace mpmca::pipeline