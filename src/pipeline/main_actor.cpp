/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/main_actor.hpp"

namespace mpmca::pipeline {

MainActor::MainActor(int argc, char** argv)
    : m_logger("MainLogger", "MainActor")
    , m_print_help(false)
    , m_config_generator(false)
    , m_free_run(false)
    , m_overwrite(false)
    , m_max_iterations(std::numeric_limits<int64_t>::max())
{
    if (argc >= 0) {
        m_executable_name = std::string(argv[0]);
    }
    else {
        m_executable_name = "mpmca";
    }
    ProcessInputs(argc, argv);
}

int MainActor::Run()
{
    if (m_print_help) {
        return 0;
    }

    if (m_config_generator) {
        return ConfigGenerator();
    }
    else {
        return RunPipeline();
    }
}

int MainActor::RunPipeline()
{
    int return_code = 0;

    if (m_input_configuration_filename.empty()) {
        throw(std::invalid_argument("Argument -c is required if you do not use --config."));
        PrintUsage();
    }

    std::cout << "MPMCA Pipeline, with configuration file: " << m_input_configuration_filename << std::endl;

    try {
        utilities::ConfigurationPtr config =
            utilities::Configuration::CreateConfigFromFile("root", m_input_configuration_filename);
        std::shared_ptr<Pipeline> pipeline = std::make_shared<Pipeline>(config);
        pipeline->Prepare();

        if (pipeline->GetSafety().GetLevel() != SafetyLevel::kSafe)
            m_logger.Error(pipeline->GetSafety().ListChildren());

        Trigger trigger(pipeline, m_max_iterations);

        if (m_free_run) {
            return_code = trigger.FreeRun();
        }
        else {
            return_code = trigger.ThreadedRun();
        }
    }
    catch (const std::exception& e) {
        std::cerr << "Caught error: " << e.what() << std::endl;
        return_code = 1;
    }

    return return_code;
}

int MainActor::ConfigGenerator()
{
    utilities::ConfigurationPtr config;

    std::cout << "Overwrite " << m_overwrite << " input file: " << m_input_configuration_filename
              << " output file: " << m_output_configuration_filename << std::endl;
    if (m_input_configuration_filename.empty())
        config = utilities::Configuration::CreateRootConfig("Main", true);
    else
        config = utilities::Configuration::CreateConfigFromFile("Main", m_input_configuration_filename, true);

    Pipeline pipeline(config);
    config->GetChildrenJson();
    config->SaveJson(m_output_configuration_filename, m_overwrite);
    return 0;
}

void MainActor::ProcessInputs(int argc, char** argv)
{
    utilities::CommandLineOptionsParser parser(argc, argv);

    if (parser.GetNumTokens() == 0) {
        PrintUsage();
        throw std::invalid_argument(
            "If you do not provide an input configuration file through the argument -c, then you have to specify "
            "--config to use the configuration file generator.");
    }

    if (parser.GetCommandOptionExists("--help")) {
        PrintUsage();
        m_print_help = true;
        return;
    }

    if (!parser.GetCommandOptionExists("--config") && !parser.GetCommandOptionExists("-c")) {
        PrintUsage();
        throw std::invalid_argument(
            "If you do not provide an input configuration file through the argument -c, then you have to specify "
            "--config to use the configuration file generator.");
    }

    if (parser.GetCommandOptionExists("--config") && parser.GetCommandOptionExists("--free")) {
        PrintUsage();
        throw std::invalid_argument("Cannot give --config and --free as arguments.");
    }

    if (parser.GetCommandOptionExists("--config"))
        m_config_generator = true;

    if (parser.GetCommandOptionExists("--free"))
        m_free_run = true;

    if (parser.GetCommandOptionExists("--overwrite_output_config"))
        m_overwrite = true;

    if (parser.GetCommandOptionExists("-c")) {
        if (parser.GetCommandOption("-c") == "") {
            PrintUsage();
            throw std::invalid_argument("Argument -c was not followed by a configuration filename.");
        }
        else {
            m_input_configuration_filename = parser.GetCommandOption("-c");
        }
    }

    if (parser.GetCommandOptionExists("--output_config")) {
        if (parser.GetCommandOption("--output_config") == "") {
            PrintUsage();
            throw std::invalid_argument(
                "Argument --output_config was not followed by an output configuration filename.");
        }
        else {
            m_output_configuration_filename = parser.GetCommandOption("--output_config");
        }
    }

    if (parser.GetCommandOptionExists("--max_iter")) {
        if (parser.GetCommandOption("--max_iter") == "") {
            PrintUsage();
            throw std::invalid_argument("Argument --max_iter was not followed by an integer number.");
        }
        else {
            try {
                m_max_iterations = std::max(0, std::stoi(parser.GetCommandOption("--max_iter")));
            }
            catch (const std::exception& e) {
                PrintUsage();
                throw std::invalid_argument(
                    "Argument --max_iter was not followed by a string that could be converted to an integer.");
            }
        }
    }
}
void MainActor::PrintUsage()
{
    std::cout
        << "usage: " << m_executable_name
        << " [-c <path/to/input_config.json>] [--free] [--max_iter N]\n\t\t[--config] [--overwrite_output_config] "
           "[--output_config </path/to/output_config.json>]\n\t\t[--help]"
        << std::endl;
    std::cout << "[optional] [-c input_config.json] : Path to input configuration json file. Required if not using the "
                 "configuration file generator."
              << std::endl;
    std::cout << "[optional] [--help] : Print this text" << std::endl;
    std::cout << "[optional] [--free] : Free running mode (no realtime pacing)." << std::endl;
    std::cout << "[optional] [--max_iter N] : maximum number of iterations (ticks) to run. N must be a positive "
                 "integer. For example: --max_iter 1000"
              << std::endl;
    std::cout << "[optional] [--config] : Configuration file generation. Use this to generate a configuration file "
                 "with default options if you do not know what options are required."
              << std::endl;
    std::cout << "[optional] [--overwrite_output_config] : Overwrite output configuration file if it exists. Only in "
                 "combination with --config."
              << std::endl;
    std::cout << "[optional] [--output_config </path/to/output_config.json>] : Path to the generated configuration "
                 "json file. Only in combination with --config."
              << std::endl;
    std::cout << "Default values when using the configuration file generator: no overwrite, no input file, output file "
              << m_output_configuration_filename << "." << std::endl
              << std::endl;
}
}  //namespace mpmca::pipeline