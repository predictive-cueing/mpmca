/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/step.hpp"

#include "mpmca/pipeline/pipeline.hpp"
#include "mpmca/pipeline/task.hpp"

namespace mpmca::pipeline {
Step::Step(Task& task, const std::string& name, const std::string& type_name, utilities::ConfigurationPtr config)
    : Unit(name, type_name, task.GetSafety(), task.GetStateMachine(), config)
{
}
Step::Step(Task& task, const std::string& name, const std::string& type_name)
    : Unit(name, type_name, task.GetSafety(), task.GetStateMachine(), task.GetNextExecutionOrder(), 1000)
{
}
Step::~Step()
{
}
void Step::Tick(MessageBus& in_output_message_bus)
{
}
void Step::SafeTick()
{
}
void Step::PrepareTaskMessageBus(const Task& task, MessageBus&)
{
}
void Step::CheckTaskMessageBusPrepared(const Task& task, const MessageBus&)
{
}
void Step::PrepareInOutputMessageBus(MessageBus&)
{
}
void Step::CheckInOutputMessageBusPrepared(const MessageBus&)
{
}
void Step::TaskCompleted(const DataBucket&)
{
}
}  //namespace mpmca::pipeline