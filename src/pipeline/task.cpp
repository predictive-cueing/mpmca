/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/task.hpp"

#include "mpmca/pipeline/in_output.hpp"
#include "mpmca/pipeline/pipeline.hpp"
#include "mpmca/pipeline/simultaneous_method_call_protector.hpp"
#include "mpmca/utilities/compare.hpp"

namespace mpmca::pipeline {

Task::Task(Pipeline& pipeline, const std::string& name, utilities::ConfigurationPtr config)
try :  //
    Task(pipeline, name, config->GetValue<int>("TimeStepMs", 10),
         utilities::Horizon(config))  //
{
    AddSteps(config->GetConfigurationObject("Steps"));
}
catch (const std::exception& c) {
    throw(std::runtime_error("Error while initializing Task.\nMessage: " + std::string(c.what())));
}

void Task::InitializeDataBuckets(const Pipeline& pipeline)
{
    m_data_bucket_mutex.resize(pipeline.GetMainTickBufferSize());
    for (size_t i = 0; i < pipeline.GetMainTickBufferSize(); ++i)
        m_data_buckets.push_back(DataBucket(m_horizon, m_time_measurement));
}

Task::Task(Pipeline& pipeline, const std::string& name, const utilities::Horizon& horizon)
    : Task(pipeline, name, horizon.GetDtMs(), horizon)
{
}

Task::Task(Pipeline& pipeline, const std::string& name, int dt_ms, const utilities::Horizon& horizon)
    : m_pipeline(pipeline)
    , m_safety(pipeline.GetSafety().MakeChild(name))
    , m_logger(name, "Task")
    , m_name(name)
    , m_main_tick_protector(false)
    , m_horizon(horizon)
    , m_clock(std::chrono::milliseconds(dt_ms))
    , m_state_clock(m_clock)
    , m_main_tick_multiple(std::max(1, dt_ms / pipeline.GetBaseStepMs()))
    , m_mark_nondeterministic_log_output(pipeline.GetMarkNondeterministicLogOutput())
    , m_actual_main_tick_cycle(-1)
    , m_main_tick_running(false)
{
    InitializeDataBuckets(pipeline);
    m_logger.Info("This Task will tick once every " + std::to_string(m_main_tick_multiple) + " base ticks.");
    m_logger.Info("The total horizon length of this Task is " +
                  std::to_string(m_horizon.GetFinalControllerHorizonTimeMs()) + " ms and has " +
                  std::to_string(m_horizon.GetNMpc()) + " steps.");
}
void Task::SortSteps()
{
    sort(m_steps.begin(), m_steps.end(), utilities::Compare::CompareUnits<Step>);
}
int Task::GetMainTickMultiple() const
{
    return m_main_tick_multiple;
}
void Task::AddSteps(utilities::ConfigurationPtr config)
{
    std::vector<std::string> names = config->GetObjectNames();

    m_logger.Info("The configuration file contains a definition for " + std::to_string(names.size()) +
                  " Step(s). Initializing Steps...");

    for (const std::string& step_name : names)
        AddStep(step_name, config->GetConfigurationObject(step_name));

    SortSteps();

    m_logger.Info("Order of " + std::to_string(m_steps.size()) + " steps:");
    for (size_t i = 0; i < m_steps.size(); i++) {
        m_logger.Info("Step " + std::to_string(i) + ". " + m_steps[i]->GetName());
    }
}
void Task::AddStep(const std::string& step_name, utilities::ConfigurationPtr config)
{
    auto step_type_name = config->GetValue<std::string>("Type", "EmptyControlStep");

    if (config->GetKeyExists("Disabled") && config->GetValue<bool>("Disabled", false)) {
        m_logger.Info("Skipping the creation of Step " + step_name + " of type " + step_type_name +
                      ", because the configuration file specified it should be disabled.");
        return;
    }

    if (StepFactory::Instance()->Exists(step_type_name)) {
        m_logger.Info("Initializing step " + step_type_name + ".");
        try {
            m_steps.push_back(StepFactory::Instance()->Create(step_type_name, *this, step_name, config));
        }
        catch (const std::exception& e) {
            m_logger.Error("Error when initializing step " + step_type_name + ".\n" + e.what());
            throw(e);
        }
    }
    else
        throw std::runtime_error("The Step type \"" + step_type_name + "\" does not exist.");
}
std::mutex& Task::GetDataBucketMutex(int64_t cycle_main)
{
    return m_data_bucket_mutex[cycle_main % m_data_buckets.size()];
}
DataBucket& Task::GetDataBucket(int64_t cycle_main)
{
    return m_data_buckets[cycle_main % m_data_buckets.size()];
}
const DataBucket& Task::GetDataBucket(int64_t cycle_main) const
{
    return m_data_buckets[cycle_main % m_data_buckets.size()];
}
utilities::TimeMeasurement& Task::GetTimeMeasurement()
{
    return m_time_measurement;
}
const utilities::Horizon& Task::GetHorizon() const
{
    return m_horizon;
}
bool Task::IsEmpty() const
{
    return m_steps.size() == 0;
}
const std::string& Task::GetName() const
{
    return m_name;
}
const utilities::TimeMeasurement& Task::GetTimeMeasurement() const
{
    return m_time_measurement;
}
void Task::PrepareTimeMeasurement()
{
    for (size_t i = 0; i < m_steps.size(); i++) {
        m_step_clocks.push_back(m_step_measurement.RegisterClock(m_name + ":MainTick:" + m_steps[i]->GetName()));
    }
}
void Task::Prepare()
{
    SortSteps();
    PrepareTimeMeasurement();

    for (size_t i = 0; i < m_steps.size(); ++i)
        m_steps[i]->GuardedPrepare();

    m_safety.Prepared();
}
const Safety& Task::GetSafety() const
{
    return m_safety;
}
Safety& Task::GetSafety()
{
    return m_safety;
}
const utilities::ControllableClock& Task::GetStateClock() const
{
    return m_state_clock;
}
const utilities::Clock& Task::GetClock() const
{
    return m_clock;
}
void Task::TickClocks()
{
    m_clock.Tick();
    m_state_clock.Tick();
}
void Task::MainTick(DataBucket& data_bucket)
{
    try {
        if (!data_bucket.IsPipelineReady())
            throw std::runtime_error(
                "The DataBucket that I should process was not declared ready by the Pipeline yet.");

        if (data_bucket.GetMainStateChangedOnThisMainTick()) {
            m_state_clock.Reset();
        }

        data_bucket.InitFromTask(*this);
        data_bucket.DeclareTaskReady();

        if (!data_bucket.IsTaskReady()) {
            throw std::runtime_error("The DataBucket that I should process was not declared ready by the Task yet.");
        }

        SimultaneousMethodCallProtector<SafetyLevel, SafetyLevel::kCritical> prot(
            m_main_tick_protector, m_safety, "Error preventing simultaneous calls of Task::MainTick()");

        data_bucket.GetTimeMeasurement().StartCycle();

        m_step_measurement.StartCycle();
        for (size_t i = 0; i < m_steps.size(); ++i) {
            m_steps[i]->GuardedMainTick(data_bucket);
            m_step_measurement.StopClock(m_step_clocks[i]);
        }
        m_step_measurement.StopCycle();
        data_bucket.GetTimeMeasurement().StopCycle();

        prot.success();
    }
    catch (const std::exception& c) {
        m_safety.Critical();
        m_logger.Critical(c);
    }

    TickClocks();
}

void Task::PrepareTaskMessageBus(std::vector<std::shared_ptr<InOutput>>& in_outputs)
{
    for (size_t d = 0; d < m_data_buckets.size(); ++d) {
        for (size_t i = 0; i < in_outputs.size(); ++i) {
            try {
                in_outputs[i]->PrepareTaskMessageBus(*this, m_data_buckets[d].GetTaskSignalBus());
            }
            catch (const std::exception& exception) {
                throw std::runtime_error("Error while preparing the MessageBus (PrepareTaskMessageBus) for InOutput " +
                                         in_outputs[i]->GetName() + ":\n" + exception.what());
            }
        }
        for (size_t s = 0; s < m_steps.size(); ++s) {
            try {
                m_steps[s]->PrepareTaskMessageBus(*this, m_data_buckets[d].GetTaskSignalBus());
            }
            catch (const std::exception& exception) {
                throw std::runtime_error("Error while preparing the MessageBus (PrepareTaskMessageBus) for Step " +
                                         m_steps[s]->GetName() + ":\n" + exception.what());
            }
        }
    }

    for (size_t d = 0; d < m_data_buckets.size(); ++d) {
        for (size_t i = 0; i < in_outputs.size(); ++i) {
            try {
                in_outputs[i]->CheckTaskMessageBusPrepared(*this, m_data_buckets[d].GetTaskSignalBus());
            }
            catch (const std::exception& exception) {
                throw std::runtime_error("The TaskMessageBus does not meet all requirements imposed by InOutput " +
                                         in_outputs[i]->GetName() + ":\n" + exception.what());
            }
        }
        for (size_t s = 0; s < m_steps.size(); ++s) {
            try {
                m_steps[s]->CheckTaskMessageBusPrepared(*this, m_data_buckets[d].GetTaskSignalBus());
            }
            catch (const std::exception& exception) {
                throw std::runtime_error("The TaskMessageBus does not meet all requirements imposed by Step " +
                                         m_steps[s]->GetName() + ":\n" + exception.what());
            }
        }
    }
}

Task::~Task()
{
    if (m_steps.size() > 0)
        m_logger.Info("Time measurement information for task " + m_name + ":\n" +
                      m_step_measurement.GetStatisticsMs("\t", m_mark_nondeterministic_log_output));
    else
        m_logger.Info("No time measurement information to show for task " + m_name + ", because it contains 0 steps.");
}
StateMachine& Task::GetStateMachine()
{
    return m_pipeline.GetStateMachine();
}
int Task::GetNextExecutionOrder() const
{
    return m_steps.size();
}
std::atomic<int64_t>& Task::GetActualMainTickCycle()
{
    return m_actual_main_tick_cycle;
}
std::mutex& Task::GetMainTickMutex()
{
    return m_main_tick_mutex;
}
std::atomic<bool>& Task::GetMainTickRunning()
{
    return m_main_tick_running;
}
size_t Task::GetNumSteps() const
{
    return m_steps.size();
}
std::shared_ptr<Step> Task::GetStepPtr(size_t step_index)
{
    return m_steps.at(step_index);
}
void Task::SetTaskIndex(size_t task_index)
{
    m_task_index = task_index;
}
size_t Task::GetTaskIndex() const
{
    return m_task_index;
}
}  //namespace mpmca::pipeline