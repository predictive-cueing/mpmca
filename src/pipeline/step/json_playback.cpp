/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/step/json_playback.hpp"

#include "mpmca/data_names.hpp"
#include "mpmca/messages/automotive_prediction_parameters.hpp"
#include "mpmca/messages/automotive_prediction_path.hpp"
#include "mpmca/messages/automotive_signals.hpp"
#include "mpmca/messages/controller_output_reference_plan.hpp"
#include "mpmca/messages/inertial_head_signals.hpp"
#include "mpmca/messages/vehicle_state_signals.hpp"
#include "mpmca/pipeline/pipeline.hpp"
#include "mpmca/predict/inertial/path_reader.hpp"
#include "mpmca/types/inertial_vector.hpp"
#include "mpmca/utilities/file_container_factory.hpp"

namespace mpmca::pipeline::step {
JsonPlayback::JsonPlayback(Task& task, const std::string& name, utilities::ConfigurationPtr config)
    : Step(task, name, "JsonPlayback", config)
    , m_read_path(config->GetKeyExists("PathReader"))
    , m_update_automotive_prediction_parameters(config->GetValue<bool>("UpdateAutomotivePredictionParameters", false))
    , m_update_inertial_head_signals(config->GetValue<bool>("UpdateInertialHeadSignals", false))
    , m_update_vehicle_state_signals(config->GetValue<bool>("UpdateVehicleStateSignals", false))
    , m_update_automotive_signals(config->GetValue<bool>("UpdateAutomotiveSignals", false))
    , m_update_controller_output_reference_plan_9(
          config->GetValue<bool>("UpdateControllerOutputReferencePlan<9>", false))
    , m_oracle_prediction(config->GetValue<std::string>("PredictionMethod", "oracle") == "oracle")
    , m_file_container(
          mpmca::utilities::FileContainerFactory::ConstructFileContainer(config, task.GetHorizon().GetDt()))
    , m_path_ptr(m_read_path ? mpmca::predict::inertial::PathReader::ReadPath<predict::inertial::Path>(config)
                             : nullptr)
    , m_start_time_ms(config->GetValue<uint64_t>("StartTimeMs", 0))
    , m_stop_time_ms(config->GetValue<uint64_t>("StopTimeMs", 0))
    , m_go_to_idle(config->GetValue<bool>("GoToIdleAtStopTimeOrEndOfData", true))
    , m_go_to_terminate(config->GetValue<bool>("TerminateAtStopTimeOrEndOfData", true))
    , m_automotive_prediction_parameters(m_update_automotive_prediction_parameters
                                             ? messages::AutomotivePredictionParameters(
                                                   config->GetConfigurationObject("AutomotivePredictionParameters"))
                                             : messages::AutomotivePredictionParameters())
{
}
void JsonPlayback::PrepareTaskMessageBus(const Task& task, MessageBus& message_bus)
{
    if (m_update_automotive_prediction_parameters) {
        message_bus.MakeMessage<messages::AutomotivePredictionParameters>();
    }
    if (m_update_inertial_head_signals) {
        message_bus.MakeMessage<messages::InertialHeadSignals>();
    }
    if (m_update_vehicle_state_signals) {
        message_bus.MakeMessage<messages::VehicleStateSignals>();
    }
    if (m_update_automotive_signals) {
        message_bus.MakeMessage<messages::AutomotiveSignals>();
    }
    if (m_read_path) {
        message_bus.MakeMessage<messages::AutomotivePredictionPath>();
    }
    if (m_update_controller_output_reference_plan_9) {
        message_bus.MakeMessage<messages::ControllerOutputReferencePlan<9>>(task.GetHorizon());
    }
}
void JsonPlayback::CheckTaskMessageBusPrepared(const Task& task, const MessageBus& message_bus)
{
    if (m_update_automotive_prediction_parameters) {
        message_bus.RequireMessage<messages::AutomotivePredictionParameters>();
    }
    if (m_update_inertial_head_signals) {
        message_bus.RequireMessage<messages::InertialHeadSignals>();
    }
    if (m_update_vehicle_state_signals) {
        message_bus.RequireMessage<messages::VehicleStateSignals>();
    }
    if (m_update_automotive_signals) {
        message_bus.RequireMessage<messages::AutomotiveSignals>();
    }
    if (m_read_path) {
        message_bus.RequireMessage<messages::AutomotivePredictionPath>();
    }
    if (m_update_controller_output_reference_plan_9) {
        message_bus.RequireMessage<messages::ControllerOutputReferencePlan<9>>();
    }
}
void JsonPlayback::MainTick(DataBucket& data_bucket)
{
    // State logic
    TransitionStateMachine();

    int64_t current_time = data_bucket.GetPipelineStateTimeMs();

    if (m_go_to_idle && GetStateMachineClient().GetCurrentState() == State::kRun &&
        (m_file_container.GetReachedEnd() || (current_time >= m_stop_time_ms))) {
        //
        GetStateMachineClient().SetGoToState(State::kStopping);
    }

    if (m_go_to_terminate && GetStateMachineClient().GetCurrentState() == State::kIdle &&
        (m_file_container.GetReachedEnd() || (current_time >= m_stop_time_ms))) {
        //
        GetStateMachineClient().SetGoToState(State::kTerminating);
    }

    // Update messages

    if (data_bucket.GetTickCycle() == 0) {
        if (m_update_automotive_prediction_parameters) {
            data_bucket.GetTaskSignalBus().UpdateMessage<messages::AutomotivePredictionParameters>(
                [&](messages::AutomotivePredictionParameters& message) {
                    message = m_automotive_prediction_parameters;
                });
        }

        if (m_read_path) {
            data_bucket.GetTaskSignalBus().UpdateMessage<messages::AutomotivePredictionPath>(
                {[&](messages::AutomotivePredictionPath& message) { message.path_ptr = m_path_ptr; }});
        }
    }

    if (m_update_inertial_head_signals) {
        m_file_container.SetIndexByTime(current_time + m_start_time_ms);
        data_bucket.GetTaskSignalBus().UpdateMessage<messages::InertialHeadSignals>(
            [&](messages::InertialHeadSignals& message) {
                // clang-format off
                message.head_f_x_head_m_s2 = m_file_container.GetDataInFieldOrFallback(DN::f_x_head().GetName());
                message.head_f_y_head_m_s2 = m_file_container.GetDataInFieldOrFallback(DN::f_y_head().GetName());
                message.head_f_z_head_m_s2 = m_file_container.GetDataInFieldOrFallback(DN::f_z_head().GetName());
                message.head_omega_x_head_rad_s = m_file_container.GetDataInFieldOrFallback(DN::omega_x_head().GetName());
                message.head_omega_y_head_rad_s = m_file_container.GetDataInFieldOrFallback(DN::omega_y_head().GetName());
                message.head_omega_z_head_rad_s = m_file_container.GetDataInFieldOrFallback(DN::omega_z_head().GetName());
                message.head_alpha_x_head_rad_s2 = m_file_container.GetDataInFieldOrFallback(DN::alpha_x_head().GetName());
                message.head_alpha_y_head_rad_s2 = m_file_container.GetDataInFieldOrFallback(DN::alpha_y_head().GetName());
                message.head_alpha_z_head_rad_s2 = m_file_container.GetDataInFieldOrFallback(DN::alpha_z_head().GetName());
                // clang-format on
            });
    }

    if (m_update_vehicle_state_signals) {
        m_file_container.SetIndexByTime(current_time + m_start_time_ms);
        data_bucket.GetTaskSignalBus().UpdateMessage<messages::VehicleStateSignals>(
            [&](messages::VehicleStateSignals& message) {
                // clang-format off
                message.world_X_vehicle_m = m_file_container.GetDataInFieldOrFallback(DN::X_vehicle().GetName());
                message.world_Y_vehicle_m = m_file_container.GetDataInFieldOrFallback(DN::Y_vehicle().GetName());
                message.world_Z_vehicle_m = m_file_container.GetDataInFieldOrFallback(DN::Z_vehicle().GetName());
                message.world_phi_vehicle_rad = m_file_container.GetDataInFieldOrFallback(DN::phi_vehicle().GetName());
                message.world_theta_vehicle_rad = m_file_container.GetDataInFieldOrFallback(DN::theta_vehicle().GetName());
                message.world_psi_vehicle_rad = m_file_container.GetDataInFieldOrFallback(DN::psi_vehicle().GetName());
                message.body_u_vehicle_m_s = m_file_container.GetDataInFieldOrFallback(DN::u_vehicle().GetName());
                message.body_v_vehicle_m_s = m_file_container.GetDataInFieldOrFallback(DN::v_vehicle().GetName());
                message.body_w_vehicle_m_s = m_file_container.GetDataInFieldOrFallback(DN::w_vehicle().GetName());
                message.body_p_vehicle_rad_s = m_file_container.GetDataInFieldOrFallback(DN::p_vehicle().GetName());
                message.body_q_vehicle_rad_s = m_file_container.GetDataInFieldOrFallback(DN::q_vehicle().GetName());
                message.body_r_vehicle_rad_s = m_file_container.GetDataInFieldOrFallback(DN::r_vehicle().GetName());
                message.body_a_x_vehicle_m_s2 = m_file_container.GetDataInFieldOrFallback(DN::a_x_vehicle().GetName());
                message.body_a_y_vehicle_m_s2 = m_file_container.GetDataInFieldOrFallback(DN::a_y_vehicle().GetName());
                message.body_a_z_vehicle_m_s2 = m_file_container.GetDataInFieldOrFallback(DN::a_z_vehicle().GetName());
                message.body_p_dot_vehicle_rad_s2 = m_file_container.GetDataInFieldOrFallback(DN::p_dot_vehicle().GetName());
                message.body_q_dot_vehicle_rad_s2 = m_file_container.GetDataInFieldOrFallback(DN::q_dot_vehicle().GetName());
                message.body_r_dot_vehicle_rad_s2 = m_file_container.GetDataInFieldOrFallback(DN::r_dot_vehicle().GetName());
                // clang-format on
            });
    }

    if (m_update_automotive_signals) {
        m_file_container.SetIndexByTime(current_time + m_start_time_ms);
        data_bucket.GetTaskSignalBus().UpdateMessage<messages::AutomotiveSignals>(
            [&](messages::AutomotiveSignals& message) {
                // clang-format off
                message.car_throttle_fraction = m_file_container.GetDataInFieldOrFallback(DN::car_throttle().GetName());
                message.car_brake_fraction = m_file_container.GetDataInFieldOrFallback(DN::car_brake().GetName());
                message.car_throttle_dot_fraction_s = m_file_container.GetDataInFieldOrFallback(DN::car_throttle_dot().GetName());
                message.car_brake_dot_fraction_s = m_file_container.GetDataInFieldOrFallback(DN::car_brake_dot().GetName());
                message.car_clutch_fraction = m_file_container.GetDataInFieldOrFallback(DN::car_clutch().GetName());
                message.car_engine_rotational_velocity_rad_s = m_file_container.GetDataInFieldOrFallback(DN::car_engine_rotational_velocity().GetName());
                message.car_engine_torque_Nm = m_file_container.GetDataInFieldOrFallback(DN::car_engine_torque().GetName());
                message.car_transmission_gear_integer = m_file_container.GetDataInFieldOrFallback(DN::car_transmission_gear().GetName());
                message.car_steer_rad = m_file_container.GetDataInFieldOrFallback(DN::car_steer().GetName());
                message.car_shift_integer = m_file_container.GetDataInFieldOrFallback(DN::car_shift().GetName());
                message.car_automatic_gear_change_boolean = m_file_container.GetDataInFieldOrFallback(DN::car_automatic_gear_change().GetName());
                // clang-format on
            });
    }

    if (m_update_controller_output_reference_plan_9) {
        data_bucket.GetTaskSignalBus().UpdateMessage<messages::ControllerOutputReferencePlan<9>>(
            [&](messages::ControllerOutputReferencePlan<9>& message) {
                for (int i = 0; i < data_bucket.GetHorizon().GetNMpc(); ++i) {
                    if (m_oracle_prediction) {
                        m_file_container.SetIndexByTime(m_start_time_ms + current_time +
                                                        data_bucket.GetHorizon().GetPredictionHorizonTimeMs(i));
                    }
                    else if (i == 0) {  // only set the time on the first step in the horizon
                        m_file_container.SetIndexByTime(m_start_time_ms + current_time);
                    }

                    InertialVector y_ref_i = InertialVector::Zero();
                    // clang-format off
                    y_ref_i.GetFx() = m_file_container.GetDataInFieldOrFallback(DN::f_x_head().GetName());
                    y_ref_i.GetFy() = m_file_container.GetDataInFieldOrFallback(DN::f_y_head().GetName());
                    y_ref_i.GetFz() = m_file_container.GetDataInFieldOrFallback(DN::f_z_head().GetName());
                    y_ref_i.GetOmegaX() = m_file_container.GetDataInFieldOrFallback(DN::omega_x_head().GetName());
                    y_ref_i.GetOmegaY() = m_file_container.GetDataInFieldOrFallback(DN::omega_y_head().GetName());
                    y_ref_i.GetOmegaZ() = m_file_container.GetDataInFieldOrFallback(DN::omega_z_head().GetName());
                    y_ref_i.GetAlphaX() = m_file_container.GetDataInFieldOrFallback(DN::alpha_x_head().GetName());
                    y_ref_i.GetAlphaY() = m_file_container.GetDataInFieldOrFallback(DN::alpha_y_head().GetName());
                    y_ref_i.GetAlphaZ() = m_file_container.GetDataInFieldOrFallback(DN::alpha_z_head().GetName());
                    // clang-format on
                    message.y_ref_plan.at(i) = y_ref_i;
                }
            });
    }
}

}  //namespace mpmca::pipeline::step