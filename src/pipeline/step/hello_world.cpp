/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/step/hello_world.hpp"

namespace mpmca::pipeline::step {

HelloWorld::HelloWorld(Task& task, const std::string& name, bool print_messages)
    : Step(task, name, "HelloWorld")
    , m_print_messages(print_messages)
{
    if (m_print_messages)
        m_logger.Info("Constructor");
}

HelloWorld::HelloWorld(Task& task, const std::string& name, utilities::ConfigurationPtr config)
    : HelloWorld(task, name, config->GetValue<bool>("PrintMessages", true))
{
}

HelloWorld::~HelloWorld()
{
    if (m_print_messages)
        m_logger.Info("Destructor");
}

void HelloWorld::PrepareTaskMessageBus(const Task& task, MessageBus& message_bus)
{
    if (m_print_messages)
        m_logger.Info("PrepareTaskMessageBus(const Task& task, MessageBus&)");
}

void HelloWorld::CheckTaskMessageBusPrepared(const Task& task, const MessageBus& message_bus)
{
    if (m_print_messages)
        m_logger.Info("CheckTaskMessageBusPrepared(const Task& task, const MessageBus&)");
}

void HelloWorld::MainTick(DataBucket& data_bucket)
{
    TransitionStateMachine();
    if (m_print_messages)
        m_logger.Info("MainTick(DataBucket&)");
}

void HelloWorld::Prepare()
{
    GetSafety().Prepared();
    if (m_print_messages)
        m_logger.Info("Prepare(Task&)");
}

}  //namespace mpmca::pipeline::step