/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/step/scale_inertial_reference.hpp"

#include "mpmca/types/inertial_vector.hpp"
namespace mpmca::pipeline::step {
ScaleInertialReference::ScaleInertialReference(Task& task, const std::string& name, utilities::ConfigurationPtr config)
    : Step(task, name, "ScaleInertialReference", config)
    , K_f_x(config->GetValue<double>("K_f_x", 0.))
    , K_f_y(config->GetValue<double>("K_f_y", 0.))
    , K_f_z(config->GetValue<double>("K_f_z", 0.))
    , K_omega_x(config->GetValue<double>("K_omega_x", 0.))
    , K_omega_y(config->GetValue<double>("K_omega_y", 0.))
    , K_omega_z(config->GetValue<double>("K_omega_z", 0.))
    , K_alpha_x(config->GetValue<double>("K_alpha_x", 0.))
    , K_alpha_y(config->GetValue<double>("K_alpha_y", 0.))
    , K_alpha_z(config->GetValue<double>("K_alpha_z", 0.))
{
}

void ScaleInertialReference::PrepareTaskMessageBus(const Task& task, MessageBus& message_bus)
{
    //
}

void ScaleInertialReference::CheckTaskMessageBusPrepared(const Task& task, const MessageBus& message_bus)
{
    message_bus.RequireMessage<messages::ControllerOutputReferencePlan<9>>();
}

void ScaleInertialReference::ScaleReferenceOutputPlan(messages::ControllerOutputReferencePlan<9>& message) const
{
    for (int i = 0; i < message.y_ref_plan.size(); ++i) {
        InertialVector y_ref = message.y_ref_plan.at(i);

        y_ref.GetFx() = y_ref.GetFx() * K_f_x;
        y_ref.GetFy() = y_ref.GetFy() * K_f_y;
        y_ref.GetFz() = (y_ref.GetFz() - 9.81) * K_f_z + 9.81;
        y_ref.GetOmegaX() = y_ref.GetOmegaX() * K_omega_x;
        y_ref.GetOmegaY() = y_ref.GetOmegaY() * K_omega_y;
        y_ref.GetOmegaZ() = y_ref.GetOmegaZ() * K_omega_z;
        y_ref.GetAlphaX() = y_ref.GetAlphaX() * K_alpha_x;
        y_ref.GetAlphaY() = y_ref.GetAlphaY() * K_alpha_y;
        y_ref.GetAlphaZ() = y_ref.GetAlphaZ() * K_alpha_z;

        message.y_ref_plan.at(i) = y_ref;
    }
}

void ScaleInertialReference::MainTick(DataBucket& data_bucket)
{
    TransitionStateMachine();

    data_bucket.GetTaskSignalBus().UpdateMessage<messages::ControllerOutputReferencePlan<9>>(
        [&](auto& message) { ScaleReferenceOutputPlan(message); });
}

void ScaleInertialReference::Prepare()
{
    GetSafety().Prepared();
}

}  //namespace mpmca::pipeline::step