/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/step/binary_logger.hpp"

namespace mpmca::pipeline::step {

BinaryLogger::BinaryLogger(Task& task, const std::string& name, utilities::ConfigurationPtr config)
    : Step(task, name, "BinaryLogger", config)
    , m_binary_logger(config->GetValue<std::string>("Filename", "out.dat"),
                      config->GetValue<bool>("LogAllMessageTypes", false))
{
    auto include_messages = config->GetArray("IncludeMessageTypes");
    for (size_t i = 0; i < include_messages->GetNumArrayElements(); ++i) {
        m_binary_logger.AddMessageNameToInclude(
            include_messages->GetValueFromArray<std::string>(i, "SomeMessageTypeName"));
    }

    auto exclude_messages = config->GetArray("IncludeMessageTypes");
    for (size_t i = 0; i < exclude_messages->GetNumArrayElements(); ++i) {
        m_binary_logger.AddMessageNameToExclude(
            exclude_messages->GetValueFromArray<std::string>(i, "SomeMessageTypeName"));
    }
}

void BinaryLogger::PrepareTaskMessageBus(const Task& task, MessageBus& message_bus)
{
}

void BinaryLogger::CheckTaskMessageBusPrepared(const Task& task, const MessageBus& message_bus)
{
    // The purpose of this for loop is to increase the performance of the first call to
    // WriteBusBaseMessages in MainTick a bit.
    for (auto& [message_type_id, bus_base_message] : message_bus.GetBusBaseMessages()) {
        m_binary_logger.IsMessageToLog(message_type_id, bus_base_message->GetTypeName());
    }
}

void BinaryLogger::MainTick(DataBucket& data_bucket)
{
    TransitionStateMachine();

    if (GetStateMachineClient().GetCurrentState() == State::kRun) {
        m_binary_logger.WriteBusBaseMessages(data_bucket.GetTaskStateTimeMs(),
                                             data_bucket.GetTaskSignalBus().GetBusBaseMessages());
    }
}

void BinaryLogger::Prepare()
{
    GetSafety().Prepared();
}
}  //namespace mpmca