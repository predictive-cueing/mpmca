/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/step/binary_generic_playback.hpp"

namespace mpmca::pipeline::step {

BinaryGenericPlayback::BinaryGenericPlayback(Task& task, const std::string& name, utilities::ConfigurationPtr config)
    : Step(task, name, "BinaryGenericPlayback", config)
    , m_binary_reader(config->GetValue<std::string>("Filename", "out.dat"))
    , m_final_sample_ms(CheckTimestampRequirementsAndReturnFinalSample())
    , m_last_time_step_in_run(0)
{
}
void BinaryGenericPlayback::PrepareTaskMessageBus(const Task& task, MessageBus& message_bus)
{
}
void BinaryGenericPlayback::CheckTaskMessageBusPrepared(const Task& task, const MessageBus& message_bus)
{
}
void BinaryGenericPlayback::MainTick(DataBucket& data_bucket)
{
    TransitionStateMachine();
}
void BinaryGenericPlayback::UpdateMessageBus(MessageBus& message_bus, int64_t current_time)
{
}
void BinaryGenericPlayback::Prepare()
{
    GetSafety().Prepared();
}

}  //namespace mpmca::pipeline::step