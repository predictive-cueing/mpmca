/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/step/timing_printer.hpp"

namespace mpmca::pipeline::step {

TimingPrinter::TimingPrinter(Task& task, const std::string& name, utilities::ConfigurationPtr config)
    : Step(task, name, "TimingPrinter", config)
    , m_test_clock_index(m_time_measurement.RegisterClock("TestClock"))
    , m_counter(1)
{
}
void TimingPrinter::Prepare()
{
    GetSafety().Prepared();
    m_time_measurement.StartCycle();
}
void TimingPrinter::MainTick(DataBucket& /* data_bucket */)
{
    m_time_measurement.StopClock(m_test_clock_index);
    m_time_measurement.StopCycle();

    std::string cached_statistics;
    if (m_counter % 100 == 0) {
        cached_statistics = m_time_measurement.GetStatisticsNs();
        m_time_measurement.ResetStatistics();
    }

    m_time_measurement.StartCycle();

    if (m_counter % 100 == 0)
        m_logger.Trace(cached_statistics);

    ++m_counter;
}
void TimingPrinter::PrepareTaskMessageBus(const Task&, MessageBus&)
{
}
void TimingPrinter::CheckTaskMessageBusPrepared(const Task&, const MessageBus&)
{
}
}  //namespace mpmca::pipeline::step