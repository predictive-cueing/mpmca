/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/step/binary_inertial_oracle_playback.hpp"

#include "mpmca/messages/controller_output_reference_plan.hpp"
#include "mpmca/messages/inertial_head_signals.hpp"
#include "mpmca/types/inertial_vector.hpp"

namespace mpmca::pipeline::step {

BinaryInertialOraclePlayback::BinaryInertialOraclePlayback(Task& task, const std::string& name,
                                                           utilities::ConfigurationPtr config)
    : Step(task, name, "BinaryInertialOraclePlayback", config)
    , m_binary_reader(config->GetValue<std::string>("Filename", "out.dat"))
    , m_final_sample_ms(CheckTimestampRequirementsAndReturnFinalSample(task.GetHorizon()))
    , m_last_time_step_in_run(0)
{
}

int64_t BinaryInertialOraclePlayback::CheckTimestampRequirementsAndReturnFinalSample(
    const utilities::Horizon& horizon) const
{
    if (!m_binary_reader.HasMessageType<messages::InertialHeadSignals>()) {
        throw std::runtime_error(
            "The provided binary file does not contain any messages of type InertialHeadSignals. I can only play back "
            "messages of that type");
    }

    const std::set<cereal_types::time_ms_type> samples_in_file =
        m_binary_reader.GetSetOfValidTimestamps<messages::InertialHeadSignals>();

    bool first_element = true;
    uint64_t previous_sample = 0;

    for (auto& sample : samples_in_file) {
        if (first_element) {
            if (sample != 0) {
                throw std::runtime_error("The timestamp of the first sample in file \"" +
                                         m_binary_reader.GetFilename() + "\" must be equal to 0. It is " +
                                         std::to_string(sample) + ".");
            }
            first_element = false;
        }
        else {
            if (sample != (previous_sample + horizon.GetDtMs())) {
                throw std::runtime_error("The data in file \"" + m_binary_reader.GetFilename() +
                                         "\" is not continuous. The sample at time step " +
                                         std::to_string(previous_sample) + " is followed by a sample at time step " +
                                         std::to_string(sample) + ".");
            }
        }

        previous_sample = sample;
    }

    m_logger.Info("The binary data file " + m_binary_reader.GetFilename() +
                  " contains valid data starting at 0, ending at " + std::to_string(previous_sample) + ".");

    if (previous_sample < horizon.GetFinalControllerHorizonTimeMs()) {
        throw std::runtime_error("The data in file \"" + m_binary_reader.GetFilename() +
                                 "\" is shorter than the length of the controller horizon. This will fail on the first "
                                 "time step in state RUN.");
    }

    return previous_sample;
}

void BinaryInertialOraclePlayback::PrepareTaskMessageBus(const Task& task, MessageBus& message_bus)
{
    message_bus.MakeMessage<messages::ControllerOutputReferencePlan<9>>(task.GetHorizon());
}

void BinaryInertialOraclePlayback::CheckTaskMessageBusPrepared(const Task& task, const MessageBus& message_bus)
{
    message_bus.RequireMessage<messages::ControllerOutputReferencePlan<9>>();
}
bool BinaryInertialOraclePlayback::WillNextMainTickFail(const DataBucket& data_bucket) const
{
    return (data_bucket.GetTaskStateTimeMs() + data_bucket.GetMainTickDtMs() +
            data_bucket.GetHorizon().GetFinalControllerHorizonTimeMs()) > m_final_sample_ms;
}
void BinaryInertialOraclePlayback::MainTick(DataBucket& data_bucket)
{
    TransitionStateMachine();

    if (GetStateMachineClient().GetCurrentState() == State::kRun) {
        UpdateMessageBus(data_bucket.GetHorizon(), data_bucket.GetTaskSignalBus(), data_bucket.GetTaskStateTimeMs());

        if (WillNextMainTickFail(data_bucket)) {
            GetStateMachineClient().SetGoToState(State::kStopping);
            m_last_time_step_in_run = data_bucket.GetTaskStateTimeMs();
            m_logger.Info("Reached end of data in file \"" + m_binary_reader.GetFilename() +
                          "\". Sending state machine to STOPPING.");
        }
    }
    else if (GetStateMachineClient().GetCurrentState() == State::kStopping ||
             GetStateMachineClient().GetCurrentState() == State::kStopped) {
        UpdateMessageBus(data_bucket.GetHorizon(), data_bucket.GetTaskSignalBus(), m_last_time_step_in_run);
    }
    else {
        UpdateMessageBus(data_bucket.GetHorizon(), data_bucket.GetTaskSignalBus(), 0);
        m_last_time_step_in_run = 0;
    }
}
void BinaryInertialOraclePlayback::UpdateMessageBus(const utilities::Horizon& horizon, MessageBus& message_bus,
                                                    int64_t current_time)
{
    message_bus.UpdateMessage<messages::ControllerOutputReferencePlan<9>>(
        [&](messages::ControllerOutputReferencePlan<9>& message) {
            messages::InertialHeadSignals inertial_head_signals;

            for (size_t i = 0; i < horizon.GetNMpc(); ++i) {
                uint64_t horizon_time = current_time + horizon.GetHorizonTimeMs(i);
                m_binary_reader.UpdateMessage(inertial_head_signals, horizon_time);

                InertialVector y_ref;
                y_ref.GetFx() = inertial_head_signals.head_f_x_head_m_s2;
                y_ref.GetFy() = inertial_head_signals.head_f_y_head_m_s2;
                y_ref.GetFz() = inertial_head_signals.head_f_z_head_m_s2;
                y_ref.GetOmegaX() = inertial_head_signals.head_omega_x_head_rad_s;
                y_ref.GetOmegaY() = inertial_head_signals.head_omega_y_head_rad_s;
                y_ref.GetOmegaZ() = inertial_head_signals.head_omega_z_head_rad_s;
                y_ref.GetAlphaX() = inertial_head_signals.head_alpha_x_head_rad_s2;
                y_ref.GetAlphaY() = inertial_head_signals.head_alpha_y_head_rad_s2;
                y_ref.GetAlphaZ() = inertial_head_signals.head_alpha_z_head_rad_s2;

                message.y_ref_plan.at(i) = y_ref;
            }
        });
}
void BinaryInertialOraclePlayback::Prepare()
{
    GetSafety().Prepared();
}
}  //namespace mpmca::pipeline::step