/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/step/empty_step.hpp"

namespace mpmca::pipeline::step {

EmptyStep::EmptyStep(Task& task, const std::string& name, utilities::ConfigurationPtr config)
    : Step(task, name, "EmptyStep", config)
{
}

EmptyStep::~EmptyStep()
{
}

void EmptyStep::PrepareTaskMessageBus(const Task& task, MessageBus& message_bus)
{
}

void EmptyStep::CheckTaskMessageBusPrepared(const Task& task, const MessageBus& message_bus)
{
}

void EmptyStep::MainTick(DataBucket& data_bucket)
{
    TransitionStateMachine();
}

void EmptyStep::Prepare()
{
    GetSafety().Prepared();
}

}  //namespace mpmca