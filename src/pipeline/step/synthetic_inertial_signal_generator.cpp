/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/step/synthetic_inertial_signal_generator.hpp"

#include "mpmca/messages/inertial_head_signals.hpp"
#include "mpmca/pipeline/pipeline.hpp"
#include "mpmca/pipeline/task.hpp"

namespace mpmca::pipeline::step {
SyntheticInertialSignalGenerator::SyntheticInertialSignalGenerator(Task& task, const std::string& name,
                                                                   utilities::ConfigurationPtr config)
    : Step(task, name, "SyntheticInertialSignalGenerator", config)
    , m_f_x_sine_generator(config->GetConfigurationObject("SpecificForceX"))
    , m_f_y_sine_generator(config->GetConfigurationObject("SpecificForceY"))
    , m_f_z_sine_generator(config->GetConfigurationObject("SpecificForceZ"))
    , m_omega_x_sine_generator(config->GetConfigurationObject("OmegaX"))
    , m_omega_y_sine_generator(config->GetConfigurationObject("OmegaY"))
    , m_omega_z_sine_generator(config->GetConfigurationObject("OmegaZ"))
    , m_alpha_x_sine_generator(config->GetConfigurationObject("AlphaX"))
    , m_alpha_y_sine_generator(config->GetConfigurationObject("AlphaY"))
    , m_alpha_z_sine_generator(config->GetConfigurationObject("AlphaZ"))
{
}

void SyntheticInertialSignalGenerator::MainTick(DataBucket& data_bucket)
{
    TransitionStateMachine();

    if (GetStateMachineClient().GetCurrentState() == State::kRun) {
        double t = (double)(data_bucket.GetTaskStateTimeMs()) / 1000.0;
        data_bucket.GetTaskSignalBus().UpdateMessage<messages::InertialHeadSignals>(
            [&](messages::InertialHeadSignals& message) {
                message.head_f_x_head_m_s2 = m_f_x_sine_generator.GetValue(t);
                message.head_f_y_head_m_s2 = m_f_y_sine_generator.GetValue(t);
                message.head_f_z_head_m_s2 = m_f_z_sine_generator.GetValue(t);
                message.head_omega_x_head_rad_s = m_omega_x_sine_generator.GetValue(t);
                message.head_omega_y_head_rad_s = m_omega_y_sine_generator.GetValue(t);
                message.head_omega_z_head_rad_s = m_omega_z_sine_generator.GetValue(t);
                message.head_alpha_x_head_rad_s2 = m_alpha_x_sine_generator.GetValue(t);
                message.head_alpha_y_head_rad_s2 = m_alpha_y_sine_generator.GetValue(t);
                message.head_alpha_z_head_rad_s2 = m_alpha_z_sine_generator.GetValue(t);
            });
    }
}

void SyntheticInertialSignalGenerator::PrepareTaskMessageBus(const Task& task, MessageBus& message_bus)
{
    message_bus.MakeMessage<messages::InertialHeadSignals>();
}
void SyntheticInertialSignalGenerator::CheckTaskMessageBusPrepared(const Task& task, const MessageBus& message_bus)
{
    message_bus.RequireMessage<messages::InertialHeadSignals>();
}
}  //namespace mpmca