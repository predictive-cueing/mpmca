/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/step/mcap_test_producer.hpp"

#include "mpmca/messages/automotive_signals.hpp"
#include "mpmca/messages/mcap_test_message.hpp"

namespace mpmca::pipeline::step {

McapTestProducer::McapTestProducer(Task& task, const std::string& name, utilities::ConfigurationPtr config)
    : Step(task, name, "McapTestProducer", config)
{
}

McapTestProducer::~McapTestProducer()
{
}

void McapTestProducer::PrepareTaskMessageBus(const Task& task, MessageBus& message_bus)
{
    message_bus.MakeMessage<messages::testing::McapTestMessage>();
    message_bus.MakeMessage<messages::AutomotiveSignals>();
}
void McapTestProducer::CheckTaskMessageBusPrepared(const Task& task, const MessageBus& message_bus)
{
    message_bus.RequireMessage<messages::testing::McapTestMessage>();
    message_bus.RequireMessage<messages::AutomotiveSignals>();
}
void McapTestProducer::MainTick(DataBucket& data_bucket)
{
    TransitionStateMachine();

    data_bucket.GetTaskSignalBus().UpdateMessage<messages::testing::McapTestMessage>(
        [&](messages::testing::McapTestMessage& message) {
            message.variable_bool = std::sin(data_bucket.GetPipelineTimeSecond()) > 0;
            message.variable_double = std::sin(data_bucket.GetPipelineTimeSecond());
            message.variable_int = std::round(std::sin(data_bucket.GetPipelineTimeSecond()));
            message.variable_string = std::to_string(message.variable_double);
            message.variable_double_vector.clear();

            for (double t = 0; t < 10; t += 0.1) {
                message.variable_double_vector.push_back(std::sin(data_bucket.GetPipelineTimeSecond() + t));
            }
        });
}
void McapTestProducer::Prepare()
{
    GetSafety().Prepared();
}

}  //namespace mpmca::pipeline