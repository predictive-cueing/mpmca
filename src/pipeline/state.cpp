/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/state.hpp"

#include <iostream>

namespace mpmca::pipeline {

std::string ToString(State state)
{
    switch (state) {
        case State::kNone:
            return std::string("NONE");
            break;
        case State::kIdle:
            return std::string("IDLE");
            break;
        case State::kPreparing:
            return std::string("PREPARING");
            break;
        case State::kPrepared:
            return std::string("PREPARED");
            break;
        case State::kReady:
            return std::string("READY");
            break;
        case State::kStarting:
            return std::string("STARTING");
            break;
        case State::kStarted:
            return std::string("STARTED");
            break;
        case State::kRun:
            return std::string("RUN");
            break;
        case State::kPausing:
            return std::string("PAUSING");
            break;
        case State::kPaused:
            return std::string("PAUSED");
            break;
        case State::kPause:
            return std::string("PAUSE");
            break;
        case State::kStopping:
            return std::string("STOPPING");
            break;
        case State::kStopped:
            return std::string("STOPPED");
            break;
        case State::kResuming:
            return std::string("RESUMING");
            break;
        case State::kResumed:
            return std::string("RESUMED");
            break;
        case State::kTerminating:
            return std::string("TERMINATING");
            break;
        case State::kTerminated:
            return std::string("TERMINATED");
            break;
        case State::kTerminate:
            return std::string("TERMINATE");
            break;
        case State::kFailing:
            return std::string("FAILING");
            break;
        case State::kFailed:
            return std::string("FAILED");
            break;
        case State::kFail:
            return std::string("FAIL");
            break;
        case State::kRecovering:
            return std::string("RECOVERING");
            break;
        case State::kRecovered:
            return std::string("RECOVERED");
            break;
        case State::kFailTerminating:
            return std::string("FAIL_TERMINATING");
            break;
        case State::kFailTerminated:
            return std::string("FAIL_TERMINATED");
            break;
        case State::kFailTerminate:
            return std::string("FAIL_TERMINATE");
            break;
        case State::kBooting:
            return std::string("BOOTING");
            break;
        case State::kBooted:
            return std::string("BOOTED");
            break;
    }
    return "UNDEFINED_STATE";
}

State FromString(const std::string& string)
{
    if (string == "NONE") {
        return State::kNone;
    }
    else if (string == "IDLE") {
        return State::kIdle;
    }
    else if (string == "PREPARING") {
        return State::kPreparing;
    }
    else if (string == "PREPARED") {
        return State::kPrepared;
    }
    else if (string == "READY") {
        return State::kReady;
    }
    else if (string == "STARTING") {
        return State::kStarting;
    }
    else if (string == "STARTED") {
        return State::kStarted;
    }
    else if (string == "RUN") {
        return State::kRun;
    }
    else if (string == "PAUSING") {
        return State::kPausing;
    }
    else if (string == "PAUSED") {
        return State::kPaused;
    }
    else if (string == "PAUSE") {
        return State::kPause;
    }
    else if (string == "STOPPING") {
        return State::kStopping;
    }
    else if (string == "STOPPED") {
        return State::kStopped;
    }
    else if (string == "RESUMING") {
        return State::kResuming;
    }
    else if (string == "RESUMED") {
        return State::kResumed;
    }
    else if (string == "TERMINATING") {
        return State::kTerminating;
    }
    else if (string == "TERMINATED") {
        return State::kTerminated;
    }
    else if (string == "TERMINATE") {
        return State::kTerminate;
    }
    else if (string == "FAILING") {
        return State::kFailing;
    }
    else if (string == "FAILED") {
        return State::kFailed;
    }
    else if (string == "FAIL") {
        return State::kFail;
    }
    else if (string == "RECOVERING") {
        return State::kRecovering;
    }
    else if (string == "RECOVERED") {
        return State::kRecovered;
    }
    else if (string == "FAIL_TERMINATING") {
        return State::kFailTerminating;
    }
    else if (string == "FAIL_TERMINATED") {
        return State::kFailTerminated;
    }
    else if (string == "FAIL_TERMINATE") {
        return State::kFailTerminate;
    }
    else if (string == "BOOTING") {
        return State::kBooting;
    }
    else if (string == "BOOTED") {
        return State::kBooted;
    }
    else {
        return State::kNone;
    }
}

std::ostream& operator<<(std::ostream& os, const State& state)
{
    os << ToString(state);
    return os;
}
}  //namespace mpmca::pipeline