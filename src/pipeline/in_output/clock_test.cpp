/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/in_output/clock_test.hpp"

#include "mpmca/pipeline/data_bucket.hpp"

namespace mpmca::pipeline::in_output {
ClockTest::ClockTest(Pipeline& pipeline, const std::string& name, utilities::ConfigurationPtr config)
    : InOutput(pipeline, name, "ClockTest", config)
{
}
void ClockTest::Tick(MessageBus& /* in_output_message_bus */)
{
    TransitionStateMachine();
}
void ClockTest::MainTick(DataBucket& data_bucket)
{
    m_logger.Debug("On MainTick " + std::to_string(data_bucket.GetMainTickCycle()) +
                   " TickCycle : " + std::to_string(data_bucket.GetTickCycle()) +
                   " PipelineTimeMs: " + std::to_string(data_bucket.GetPipelineTimeMs()) +
                   " PipelineStateTimeMs: " + std::to_string(data_bucket.GetPipelineStateTimeMs()));
}
}  //namespace mpmca