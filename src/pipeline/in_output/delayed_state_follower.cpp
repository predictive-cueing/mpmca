/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/in_output/delayed_state_follower.hpp"

#include "mpmca/pipeline/pipeline.hpp"
#include "mpmca/pipeline/state_machine_client.hpp"

namespace mpmca::pipeline::in_output {
DelayedStateFollower::DelayedStateFollower(Pipeline& pipeline, const std::string& name, int delay_samples)
    : InOutput(pipeline, name, "DelayedStateFollower")
    , m_delay_samples(delay_samples)
    , m_samples_in_transition_state(0)
{
}
DelayedStateFollower::DelayedStateFollower(Pipeline& pipeline, const std::string& name,
                                           utilities::ConfigurationPtr config)
    : InOutput(pipeline, name, "DelayedStateFollower", config)
    , m_delay_samples(config->GetValue<int>("DelaySamples", 3))
    , m_samples_in_transition_state(0)
{
}
void DelayedStateFollower::Tick(MessageBus&)
{
    if (GetStateMachineClient().IsCurrentStateTransitionState())
        ++m_samples_in_transition_state;

    if (m_samples_in_transition_state >= m_delay_samples) {
        TransitionStateMachine();
        m_samples_in_transition_state = 0;
    }
}
void DelayedStateFollower::TaskCompleted(const DataBucket&)
{
}
void DelayedStateFollower::SafeTick()
{
}
void DelayedStateFollower::MainTick(DataBucket& data_bucket)
{
}
}  //namespace mpmca::pipeline::in_output