/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/in_output/go_to_run.hpp"

#include "mpmca/pipeline/pipeline.hpp"
#include "mpmca/pipeline/state_machine_client.hpp"

namespace mpmca::pipeline::in_output {
GoToRun::GoToRun(Pipeline& pipeline, const std::string& name)
    : InOutput(pipeline, name, "GoToRun")
    , m_target_state(TargetState::kRun)
    , m_was_in_target_state(false)
{
}
GoToRun::GoToRun(Pipeline& pipeline, const std::string& name, utilities::ConfigurationPtr config)
    : InOutput(pipeline, name, "GoToRun", config)
    , m_target_state(TargetState::kRun)
    , m_was_in_target_state(false)
{
}
void GoToRun::Tick(MessageBus&)
{
    TransitionStateMachine();
}
void GoToRun::TaskCompleted(const DataBucket&)
{
}
void GoToRun::SafeTick()
{
}
void GoToRun::SetTargetState(TargetState target_state)
{
    m_target_state = target_state;
    m_was_in_target_state = false;
}
void GoToRun::MainTick(DataBucket& data_bucket)
{
    // Once the global StateMachine reached the target state (Idle or Run)
    // we just follow the states as they are requested by other Units.
    if (m_was_in_target_state) {
        TransitionStateMachine();
        return;
    }

    if (m_target_state == TargetState::kRun) {
        // If the target state is Run, then this InOutput will try to reach Run from the Idle state.
        if (GetStateMachineClient().GetCurrentState() == State::kIdle) {
            GetStateMachineClient().SetGoToState(State::kPreparing);
        }
        else if (GetStateMachineClient().GetCurrentState() == State::kReady) {
            GetStateMachineClient().SetGoToState(State::kStarting);
        }
        else {
            TransitionStateMachine();
        }
        m_was_in_target_state |= GetStateMachineClient().GetCurrentState() == State::kRun;
    }
    else if (m_target_state == TargetState::kIdle) {
        if (GetStateMachineClient().GetCurrentState() == State::kRun) {
            GetStateMachineClient().SetGoToState(State::kStopping);
        }
        else {
            TransitionStateMachine();
        }
        m_was_in_target_state |= GetStateMachineClient().GetCurrentState() == State::kIdle;
    }
}
}  //namespace mpmca::pipeline::in_output