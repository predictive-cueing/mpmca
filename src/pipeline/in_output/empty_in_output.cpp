/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/in_output/empty_in_output.hpp"

namespace mpmca::pipeline::in_output {

EmptyInOutput::EmptyInOutput(Pipeline& pipeline, const std::string& name)
    : InOutput(pipeline, name, "EmptyInOutput")
{
}

EmptyInOutput::EmptyInOutput(Pipeline& pipeline, const std::string& name, utilities::ConfigurationPtr config)
    : InOutput(pipeline, name, "EmptyInOutput", config)
{
}
void EmptyInOutput::Prepare()
{
    GetSafety().Prepared();
}
void EmptyInOutput::Tick(MessageBus&)
{
    TransitionStateMachine();
}
void EmptyInOutput::TaskCompleted(const DataBucket&)
{
}
void EmptyInOutput::MainTick(DataBucket&)
{
}
void EmptyInOutput::SafeTick()
{
}
EmptyInOutput::~EmptyInOutput()
{
}
}  //namespace mpmca::pipeline::in_output