/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/in_output/hello_world.hpp"

namespace mpmca::pipeline::in_output {

HelloWorld::HelloWorld(Pipeline& pipeline, const std::string& name, bool print_messages)
    : InOutput(pipeline, name, "HelloWorld")
    , m_print_messages(print_messages)
{
    if (m_print_messages)
        m_logger.Info("Constructor");
}

HelloWorld::HelloWorld(Pipeline& pipeline, const std::string& name, utilities::ConfigurationPtr config)
    : InOutput(pipeline, name, "HelloWorld", config)
    , m_print_messages(config->GetValue<bool>("PrintMessages", false))
{
    if (m_print_messages)
        m_logger.Info("Constructor");
}

void HelloWorld::Prepare()
{
    if (m_print_messages)
        m_logger.Info("Prepare()");

    GetSafety().Prepared();
}
void HelloWorld::Tick(MessageBus&)
{
    TransitionStateMachine();

    if (m_print_messages)
        m_logger.Info("Tick()");
}
void HelloWorld::TaskCompleted(const DataBucket&)
{
    if (m_print_messages)
        m_logger.Info("TaskCompleted()");
}
void HelloWorld::MainTick(DataBucket&)
{
    if (m_print_messages)
        m_logger.Info("MainTick()");
}
void HelloWorld::SafeTick()
{
    if (m_print_messages)
        m_logger.Info("SafeTick()");
}
void HelloWorld::PrepareTaskMessageBus(const Task& task, MessageBus&)
{
    if (m_print_messages)
        m_logger.Info("PrepareTaskMessageBus(const Task& task, MessageBus&)");
}
void HelloWorld::CheckTaskMessageBusPrepared(const Task& task, const MessageBus&)
{
    if (m_print_messages)
        m_logger.Info("CheckTaskMessageBusPrepared(const Task& task, const MessageBus&)");
}

void HelloWorld::PrepareInOutputMessageBus(MessageBus&)
{
    if (m_print_messages)
        m_logger.Info("PrepareInOutputMessageBus(MessageBus&)");
}
void HelloWorld::CheckInOutputMessageBusPrepared(const MessageBus&)
{
    if (m_print_messages)
        m_logger.Info("CheckInOutputMessageBusPrepared(const MessageBus&)");
}
HelloWorld::~HelloWorld()
{
    if (m_print_messages)
        m_logger.Info("~HelloWorld()");
}
}  //namespace mpmca::pipeline::in_output