/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/cereal_logger.hpp"

namespace mpmca::pipeline {
CerealLogger::CerealLogger(const std::string& filename)
    : m_output_file_stream(filename, std::ios::binary)
    , m_archive(m_output_file_stream)
    , m_buffered_archive(m_buffer)
{
}

void CerealLogger::DoWriteMessage(cereal_types::time_ms_type current_time_ms,
                                  cereal_types::bus_message_type_id message_type_id)
{
    uint64_t message_length = m_buffer.tellp();
    m_archive(message_type_id, current_time_ms, message_length);
    m_output_file_stream << m_buffer.str();
    m_buffer.clear();
    m_buffer.seekp(0, std::ios::beg);
}
}  //namespace mpmca