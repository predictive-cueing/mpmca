/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/bus_base_message.hpp"

#include <algorithm>

namespace mpmca::pipeline {
BusBaseMessage::BusBaseMessage(uint64_t type_id, const std::string& type_name)
    : m_type_id(type_id)
    , m_type_name(type_name)
    , m_is_updated(false)
{
}
uint64_t BusBaseMessage::GetTypeId() const
{
    return m_type_id;
}
const std::string& BusBaseMessage::GetTypeName() const
{
    return m_type_name;
}
bool BusBaseMessage::IsUpdated() const
{
    return m_is_updated;
}
void BusBaseMessage::SetIsUpdated(bool is_updated)
{
    m_is_updated = is_updated;
}
}  //namespace mpmca