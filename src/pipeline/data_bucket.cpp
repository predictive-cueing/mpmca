/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/data_bucket.hpp"

#include "mpmca/pipeline/pipeline.hpp"
#include "mpmca/pipeline/task.hpp"

namespace mpmca::pipeline {
int DataBucket::s_instances = 0;

DataBucket::DataBucket(const utilities::Horizon& horizon, utilities::TimeMeasurement& time_measurement)
    : m_id(s_instances++)
    , m_horizon(horizon)
    , m_time_measurement(time_measurement)
    , m_tick_cycle(0)
    , m_main_tick_cycle(0)
    , m_pipeline_time_ms(0)
    , m_pipeline_state_time_ms(0)
    , m_pipeline_initialized(false)
    , m_pipeline_ready_for_processing(false)
    , m_task_initialized(false)
    , m_task_ready_for_processing(false)
    , m_done(true)
    , m_main_state_changed_on_this_main_tick(false)
    , m_task_time_ms(0)
    , m_task_state_time_ms(0)
    , m_task_index(0)
{
}

int DataBucket::GetId() const
{
    return m_id;
}
const utilities::Horizon& DataBucket::GetHorizon() const
{
    return m_horizon;
}
void DataBucket::InitFromPipeline(const Pipeline& pipeline, int64_t theoretical_main_tick_cycle, size_t task_index)
{
    if (!m_done)
        throw(std::runtime_error("This DataBucket cannot be initialized, because it was not declared done."));

    m_pipeline_ready_for_processing = false;
    m_pipeline_time_ms = pipeline.GetClock().GetCurrentTimeMs();
    m_pipeline_state_time_ms = pipeline.GetStateClock().GetCurrentTimeMs();
    m_tick_cycle = pipeline.GetTickCycle();
    m_main_tick_cycle = theoretical_main_tick_cycle;
    m_task_message_bus.ResetUpdateStatusOnAllMessages();
    m_main_state_changed_on_this_main_tick = pipeline.GetStateMachine().GetMainStateChangedOnThisTick();
    m_pipeline_initialized = true;
    m_task_index = task_index;
}
void DataBucket::InitFromTask(const Task& task)
{
    if (!m_done)
        throw(std::runtime_error("This DataBucket cannot be initialized, because it was not declared done."));

    m_task_ready_for_processing = false;
    m_main_tick_dt_ms = task.GetClock().GetDtMs();
    m_task_state_time_ms = task.GetStateClock().GetCurrentTimeMs();
    m_task_time_ms = task.GetClock().GetCurrentTimeMs();
    m_task_initialized = true;
    m_task_index = task.GetTaskIndex();
    m_task_name = task.GetName();
}
bool DataBucket::GetMainStateChangedOnThisMainTick() const
{
    return m_main_state_changed_on_this_main_tick;
}
int64_t DataBucket::GetMainTickDtMs() const
{
    return m_main_tick_dt_ms;
}
void DataBucket::DeclarePipelineReady()
{
    if (!m_pipeline_initialized)
        throw(
            std::runtime_error("This DataBucket cannot be declared ready by the Pipeline, because it was never "
                               "initialized by the Pipeline."));

    m_pipeline_ready_for_processing = true;
}
void DataBucket::DeclareTaskReady()
{
    if (!m_pipeline_ready_for_processing)
        throw(
            std::runtime_error("This DataBucket cannot be declared ready by the Task, because it was never "
                               "declared ready by the Pipeline."));

    if (!m_task_initialized)
        throw(std::runtime_error(
            "This DataBucket cannot be declared ready by the Task, because it was never initialized by the Task."));

    m_task_ready_for_processing = true;
}
bool DataBucket::IsDone() const
{
    return m_done;
}
bool DataBucket::IsPipelineReady() const
{
    return m_pipeline_ready_for_processing;
}
bool DataBucket::IsTaskReady() const
{
    return m_pipeline_ready_for_processing && m_task_ready_for_processing;
}
void DataBucket::DeclareDone()
{
    if (!m_pipeline_initialized || !m_pipeline_ready_for_processing)
        throw(
            std::runtime_error("This DataBucket cannot be declared done, because it was not initialized or not "
                               "declared ready by the Pipeline yet."));

    if (!m_task_initialized || !m_task_ready_for_processing)
        throw(
            std::runtime_error("This DataBucket cannot be declared done, because it was not initialized or not "
                               "declared ready by the Task yet."));

    m_done = true;
    m_pipeline_initialized = false;
    m_task_initialized = false;
    m_pipeline_ready_for_processing = false;
    m_task_ready_for_processing = false;
}
MessageBus& DataBucket::GetTaskSignalBus()
{
    return m_task_message_bus;
}
const MessageBus& DataBucket::GetTaskSignalBus() const
{
    return m_task_message_bus;
}
utilities::TimeMeasurement& DataBucket::GetTimeMeasurement()
{
    return m_time_measurement;
}
const utilities::TimeMeasurement& DataBucket::GetTimeMeasurement() const
{
    return m_time_measurement;
}
int64_t DataBucket::GetPipelineTimeMs() const
{
    return m_pipeline_time_ms;
}
double DataBucket::GetPipelineTimeSecond() const
{
    return ((double)m_pipeline_time_ms) / 1000.0;
}
int64_t DataBucket::GetTaskTimeMs() const
{
    return m_task_time_ms;
}
int64_t DataBucket::GetPipelineStateTimeMs() const
{
    return m_pipeline_state_time_ms;
}
double DataBucket::GetPipelineStateTime() const
{
    return (double)m_pipeline_state_time_ms / 1000.0;
}
int64_t DataBucket::GetTaskStateTimeMs() const
{
    return m_task_state_time_ms;
}
int64_t DataBucket::GetMainTickCycle() const
{
    return m_main_tick_cycle;
}
int64_t DataBucket::GetTickCycle() const
{
    return m_tick_cycle;
}
void DataBucket::SetMainTickCycle(int64_t cycle)
{
    m_main_tick_cycle = cycle;
}
size_t DataBucket::GetTaskIndex() const
{
    return m_task_index;
}
const std::string& DataBucket::GetTaskName() const
{
    return m_task_name;
}
}  //namespace mpmca::pipeline