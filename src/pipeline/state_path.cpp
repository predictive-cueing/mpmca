/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/state_path.hpp"

namespace mpmca::pipeline {
StatePath::StatePath(State start, State transition, State transition_done, State next, Type type)
    : m_start(start)
    , m_transition(transition)
    , m_transition_done(transition_done)
    , m_next(next)
    , m_type(type)
{
}
const State& StatePath::GetStartState() const
{
    return m_start;
}
const State& StatePath::GetTransitionState() const
{
    return m_transition;
}
const State& StatePath::GetTransitionDoneState() const
{
    return m_transition_done;
}
const State& StatePath::GetNextState() const
{
    return m_next;
}
StatePath::Type StatePath::GetType() const
{
    return m_type;
}
}  //namespace mpmca