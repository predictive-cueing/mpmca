/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/pipeline.hpp"

#include <utility>

#include "mpmca/pipeline/data_bucket.hpp"
#include "mpmca/pipeline/in_output.hpp"
#include "mpmca/pipeline/in_output_factory.hpp"
#include "mpmca/pipeline/simultaneous_method_call_protector.hpp"
#include "mpmca/pipeline/task.hpp"
#include "mpmca/utilities/compare.hpp"

namespace mpmca::pipeline {
Pipeline::Pipeline(const std::string& json_string)
    : Pipeline(utilities::Configuration::CreateConfigFromString("PipelineConfig", json_string))
{
}

Pipeline::Pipeline(utilities::ConfigurationPtr config)
    : m_command_buffer_length(config->GetConfigurationObject("Pipeline")->GetValue<int>("MainTickBufferSize", 3))
    , m_base_step_ms(config->GetConfigurationObject("Pipeline")->GetValue<int>("BaseStepMs", 1))
    , m_print_state_changes(config->GetConfigurationObject("Pipeline")->GetValue<bool>("PrintStateChanges", false))
    , m_print_client_state_changes(
          config->GetConfigurationObject("Pipeline")->GetValue<bool>("PrintClientStateChanges", false))
    , m_mark_nondeterministic_log_output(
          config->GetConfigurationObject("Pipeline")->GetValue<bool>("MarkNondeterministicLogOutput", false))
    , m_safety("Pipeline", config->GetConfigurationObject("Pipeline")->GetValue<bool>("Guarded", true))
    , m_logger(config->GetConfigurationObject("Pipeline")->GetConfigurationObject("Logger"), "Pipeline", "Pipeline")
    , m_clock(std::chrono::milliseconds(m_base_step_ms))
    , m_state_clock(std::chrono::milliseconds(m_base_step_ms), std::chrono::milliseconds(m_clock.GetCurrentTimeMs()))
    , m_previous_safety_level(m_safety.GetLevel())
    , m_tick_running(false)
    , m_tick_cycle(-1)
    , m_pipeline_return_code(0)
{
    PerformOptionsCheck();
    auto local_config = config->GetConfigurationObject("Pipeline");
    InitTasks(local_config);
    AddInOutputs(local_config);
}

Pipeline::Pipeline(const Options& options)
    : m_command_buffer_length(options.command_buffer_length)
    , m_base_step_ms(options.base_step_ms)
    , m_print_state_changes(options.print_state_changes)
    , m_print_client_state_changes(options.print_client_state_changes)
    , m_mark_nondeterministic_log_output(options.mark_nondeterministic_log_output)
    , m_safety("Pipeline", options.guarded)
    , m_logger("Pipeline", "Pipeline")
    , m_clock(std::chrono::milliseconds(m_base_step_ms))
    , m_state_clock(std::chrono::milliseconds(m_base_step_ms), std::chrono::milliseconds(m_clock.GetCurrentTimeMs()))
    , m_previous_safety_level(m_safety.GetLevel())
    , m_tick_running(false)
    , m_tick_cycle(-1)
    , m_pipeline_return_code(0)
{
    PerformOptionsCheck();
}

void Pipeline::PerformOptionsCheck()
{
    if (m_command_buffer_length < 2)
        throw(std::invalid_argument("Option \"MainTickBufferSize\" should be 2 or larger."));

    if (!m_safety.GetGuarded())
        m_logger.Warning(
            "This instance of Pipeline is not guarded! Exceptions thrown anywhere inside this instance will not be "
            "handled. Use with care.");
}

int Pipeline::GetPipelineReturnCode() const
{
    State current_state = m_state_machine.GetCurrentState();
    if (current_state == State::kFail || current_state == State::kFailed || current_state == State::kFailing ||
        current_state == State::kFailTerminating || current_state == State::kFailTerminated ||
        current_state == State::kFailTerminate) {
        return 1;
    }

    return 0;
}

void Pipeline::PrepareInOutputMessageBus()
{
    for (size_t i = 0; i < m_in_outputs.size(); ++i) {
        try {
            m_in_outputs[i]->PrepareInOutputMessageBus(m_in_output_message_bus);
        }
        catch (const std::exception& exception) {
            throw std::runtime_error(
                "Error while preparing the InOutput MessageBus (PrepareInOutputMessageBus) for InOutput " +
                m_in_outputs[i]->GetName() + ":\n" + exception.what());
        }
    }

    for (size_t i = 0; i < m_in_outputs.size(); ++i) {
        try {
            m_in_outputs[i]->CheckInOutputMessageBusPrepared(m_in_output_message_bus);
        }
        catch (const std::exception& exception) {
            throw std::runtime_error("The InOutput MessageBus does not meet all requirements imposed by InOutput " +
                                     m_in_outputs[i]->GetName() + ":\n" + exception.what());
        }
    }
}

void Pipeline::PrepareTimeMeasurement()
{
    for (int i = 0; i < m_in_outputs.size(); ++i) {
        m_in_output_tick_clocks.push_back(
            m_in_output_tick_measurement.RegisterClock("tick:" + m_in_outputs[i]->GetName()));

        m_in_output_main_tick_clocks.push_back(
            m_in_output_main_tick_measurement.RegisterClock("MainTick:" + m_in_outputs[i]->GetName()));

        m_in_output_task_completed_clocks.push_back(
            m_in_output_task_completed_measurement.RegisterClock("taskCompleted:" + m_in_outputs[i]->GetName()));
    }
}
void Pipeline::Prepare()
{
    SortInOutputs();
    SortTasks();
    PrepareTimeMeasurement();
    PrepareInOutputMessageBus();

    for (auto task_ptr : m_tasks) {
        task_ptr->PrepareTaskMessageBus(m_in_outputs);
        task_ptr->Prepare();
    }

    for (size_t i = 0; i < m_in_outputs.size(); ++i) {
        m_in_outputs[i]->GuardedPrepare();
    }

    m_safety.Prepared();
    m_logger.Info("Prepared pipeline. Safety status is " + m_safety.GetLevelName() + ".");
    m_previous_safety_level = m_safety.GetLevel();
}
bool Pipeline::GetNextTickIsMainTick(size_t task_index) const
{
    if (task_index < m_tasks.size())
        return (((m_tick_cycle.load() + 1) % m_tasks[task_index]->GetMainTickMultiple()) == 0);
    else
        return true;
}
bool Pipeline::GetCurrentTickIsMainTick(size_t task_index) const
{
    if (task_index < m_tasks.size())
        return ((m_tick_cycle.load() % m_tasks[task_index]->GetMainTickMultiple()) == 0);
    else
        return true;
}

int Pipeline::GetBaseStepMs() const
{
    return m_base_step_ms;
}
int64_t Pipeline::GetTickCycle() const
{
    return m_tick_cycle.load();
}
int64_t Pipeline::GetTheoreticalMainTickCycle(size_t task_index) const
{
    if (task_index < m_tasks.size()) {
        if (m_tick_cycle.load() < 0) {
            return -1;
        }
        else {
            return m_tick_cycle.load() / m_tasks[task_index]->GetMainTickMultiple();
        }
    }
    else
        return m_tick_cycle.load();
}
const utilities::TimeMeasurement& Pipeline::GetInOutputTickMeasurement() const
{
    return m_in_output_tick_measurement;
}
const utilities::TimeMeasurement& Pipeline::GetInOutputMainTickMeasurement() const
{
    return m_in_output_main_tick_measurement;
}
const utilities::TimeMeasurement& Pipeline::GetInOutputTaskCompletedMeasurement() const
{
    return m_in_output_task_completed_measurement;
}
const StateMachine& Pipeline::GetStateMachine() const
{
    return m_state_machine;
}
StateMachine& Pipeline::GetStateMachine()
{
    return m_state_machine;
}
const Safety& Pipeline::GetSafety() const
{
    return m_safety;
}
Safety& Pipeline::GetSafety()
{
    return m_safety;
}
std::shared_ptr<Task> Pipeline::GetTaskPtr(size_t task_index)
{
    if (task_index < m_tasks.size()) {
        return m_tasks[task_index];
    }
    else {
        return nullptr;
    }
}
std::shared_ptr<const Task> Pipeline::GetConstTaskPtr(size_t task_index) const
{
    if (task_index < m_tasks.size()) {
        return m_tasks[task_index];
    }
    else {
        return nullptr;
    }
}
const utilities::Clock& Pipeline::GetClock() const
{
    return m_clock;
}
const utilities::ControllableClock& Pipeline::GetStateClock() const
{
    return m_state_clock;
}
utilities::Clock& Pipeline::GetClock()
{
    return m_clock;
}
bool Pipeline::HasTaskSteps(size_t task_index) const
{
    if (task_index < m_tasks.size()) {
        return !m_tasks[task_index]->IsEmpty();
    }
    else {
        return false;
    }
}
size_t Pipeline::GetNumInOutputs() const
{
    return m_in_outputs.size();
}
void Pipeline::ThrowSafetyLevelUnpreparedError() const
{
    throw(std::runtime_error(
        "The SafetyLevel of the pipeline is UNPREPARED. You cannot call Tick() or MainTick(). The status of all "
        "safety clients is:\n" +
        m_safety.ListChildren()));
}
void Pipeline::TickClocks()
{
    m_clock.Tick();
    m_state_clock.Tick();
}

void Pipeline::CheckDataBucketIsDone(int64_t theoretical_main_tick_cycle, size_t task_index) const
{
    if (m_tasks.size() == 0) {
        return;
    }

    if (!m_tasks[task_index]->GetDataBucket(theoretical_main_tick_cycle).IsDone())
        throw(std::runtime_error("I cannot start MainTick #" + std::to_string(theoretical_main_tick_cycle) +
                                 ", because MainTick #" + std::to_string(theoretical_main_tick_cycle) +
                                 " is not done yet, and I have a buffer of length " +
                                 std::to_string(m_command_buffer_length) + "."));
}
void Pipeline::MainTickStateMachine()
{
    TickStateMachine();
    m_state_machine.MainTick();

    if (m_state_machine.GetMainStateChangedOnThisTick()) {
        m_state_clock.Reset();
    }

    if (m_print_state_changes && (m_state_machine.GetMainStateChangedOnThisTick() || m_tick_cycle == 0))
        m_logger.Info("Main state changed.\nState machine state: " + ToString(m_state_machine.GetCurrentState()) +
                      "\n" + m_state_machine.ListClients());
}

void Pipeline::TickStateMachine()
{
    m_state_machine.Tick();

    if (m_print_client_state_changes && (m_state_machine.GetStatesChangedOnThisTick() && m_tick_cycle != 0))
        m_logger.Info("Client state(s) changed.\nState machine state: " + ToString(m_state_machine.GetCurrentState()) +
                      "\n" + m_state_machine.ListClients());
}

void Pipeline::TickSafeLogic()
{
    if (m_tasks.size() == 0) {
        MainTickStateMachine();
        TickAllInOutputs();
        return;
    }
    if (GetCurrentTickIsMainTick(m_tasks.size() - 1)) {
        MainTickStateMachine();
    }

    TickAllInOutputs();

    for (size_t task_index = 0; task_index < m_tasks.size(); ++task_index) {
        if (GetCurrentTickIsMainTick(task_index)) {
            int64_t theoretical_main_tick_cycle = GetTheoreticalMainTickCycle(task_index);
            CheckDataBucketIsDone(theoretical_main_tick_cycle, task_index);

            const std::lock_guard<std::mutex> lock(
                m_tasks[task_index]->GetDataBucketMutex(theoretical_main_tick_cycle));

            m_tasks[task_index]
                ->GetDataBucket(theoretical_main_tick_cycle)
                .InitFromPipeline(*this, theoretical_main_tick_cycle, task_index);
            MainTickAllInOutputs(m_tasks[task_index]->GetDataBucket(theoretical_main_tick_cycle));
            m_tasks[task_index]->GetDataBucket(theoretical_main_tick_cycle).DeclarePipelineReady();
        }
    }
}

void Pipeline::TickFailLogic()
{
    for (int j = 0; j < GetNumInOutputs(); ++j)
        m_in_outputs[j]->GuardedSafeTick();
}
void Pipeline::PrintSafetyLevelChange()
{
    if (m_previous_safety_level != m_safety.GetLevel()) {
        m_logger.Info("Safety level changed to " + m_safety.GetLevelName());
        m_previous_safety_level = m_safety.GetLevel();
    }
}
void Pipeline::Tick()
{
    switch (m_safety.GetLevel()) {
        case SafetyLevel::kUnprepared:
            ThrowSafetyLevelUnpreparedError();
            break;
        case SafetyLevel::kSafe:
            try {
                SimultaneousMethodCallProtector<SafetyLevel, SafetyLevel::kCritical> prot(
                    m_tick_running, m_safety, "Error preventing simultaneous calls of Pipeline::Tick()");
                ++m_tick_cycle;
                TickSafeLogic();
                TickClocks();
                prot.success();
            }
            catch (const std::exception& e) {
                m_safety.Critical();
                m_logger.Critical(e);
            }
            break;
        case SafetyLevel::kFail:
            try {
                SimultaneousMethodCallProtector<SafetyLevel, SafetyLevel::kCritical> prot(
                    m_tick_running, m_safety, "Error preventing simultaneous calls of Pipeline::Tick()");
                ++m_tick_cycle;
                TickFailLogic();
                prot.success();
            }
            catch (const std::exception& e) {
                m_safety.Critical();
                m_logger.Critical(e);
            }
            break;
        case SafetyLevel::kCritical:
            // do nothing
            if (!m_safety.GetGuarded()) {
                throw std::runtime_error("The Pipeline reached SafetyLevel CRITICAL in unguarded mode. Terminating.");
            }
            break;
    }
}

void Pipeline::TickAllInOutputs()
{
    m_in_output_tick_measurement.StartCycle();
    for (int j = 0; j < GetNumInOutputs(); ++j) {
        m_in_outputs[j]->GuardedTick(m_in_output_message_bus);
        m_in_output_tick_measurement.StopClock(m_in_output_tick_clocks[j]);
    }
    m_in_output_tick_measurement.StopCycle();
}

void Pipeline::MainTickAllInOutputs(DataBucket& data_bucket)
{
    m_in_output_main_tick_measurement.StartCycle();
    for (int j = 0; j < GetNumInOutputs(); ++j) {
        m_in_outputs[j]->GuardedMainTick(data_bucket);
        m_in_output_main_tick_measurement.StopClock(m_in_output_main_tick_clocks[j]);
    }
    m_in_output_main_tick_measurement.StopCycle();
}

void Pipeline::TaskCompletedAllInOutputs(const DataBucket& data_bucket)
{
    m_in_output_task_completed_measurement.StartCycle();
    for (int j = 0; j < GetNumInOutputs(); ++j) {
        m_in_outputs[j]->GuardedTaskCompleted(data_bucket);
        m_in_output_task_completed_measurement.StopClock(m_in_output_task_completed_clocks[j]);
    }
    m_in_output_task_completed_measurement.StopCycle();
}

void Pipeline::MainTick(size_t task_index)
{
    if (task_index >= m_tasks.size())
        return;

    switch (m_safety.GetLevel()) {
        case SafetyLevel::kUnprepared:
            ThrowSafetyLevelUnpreparedError();
            break;
        case SafetyLevel::kSafe:
            try {
                SimultaneousMethodCallProtector<SafetyLevel, SafetyLevel::kCritical> prot(
                    m_tasks[task_index]->GetMainTickRunning(), m_safety,
                    "Error preventing simultaneous calls of Pipeline::MainTick()");

                const std::lock_guard<std::mutex> main_tick_lock(m_tasks[task_index]->GetMainTickMutex());

                // there are two main_cycle counters: the theoretical value (based on the m_tick_cycle value)
                // and the actual value (which is really the number of times taskMainTick is called)

                ++(m_tasks[task_index]->GetActualMainTickCycle());

                if ((m_tasks[task_index]->GetActualMainTickCycle() + m_command_buffer_length - 1) <
                    GetTheoreticalMainTickCycle(task_index)) {
                    m_safety.Fail();
                    m_logger.Error("We are too far behind.");
                }

                // Then run the Task MainTick, passing the next in line data_bucket
                const std::lock_guard<std::mutex> data_bucket_lock(
                    m_tasks[task_index]->GetDataBucketMutex(m_tasks[task_index]->GetActualMainTickCycle()));

                DataBucket& this_bucket =
                    m_tasks[task_index]->GetDataBucket(m_tasks[task_index]->GetActualMainTickCycle());
                m_tasks[task_index]->MainTick(this_bucket);

                TaskCompletedAllInOutputs(this_bucket);
                this_bucket.DeclareDone();

                prot.success();
            }
            catch (const std::exception& e) {
                m_safety.Critical();
                m_logger.Critical(e);
            }
            break;
        case SafetyLevel::kFail:
            // Nothing is done when SafetyLevel is Fail.
            break;
        case SafetyLevel::kCritical:
            // Nothing is done when SafetyLevel is Critical.
            break;
    }
}

void Pipeline::InitTasks(utilities::ConfigurationPtr config)
{
    if (config->GetKeyExists("Tasks")) {
        auto tasks_config = config->GetConfigurationObject("Tasks");
        std::vector<std::string> names = tasks_config->GetObjectNames();

        for (const auto& name : names) {
            auto task_config = tasks_config->GetConfigurationObject(name);
            if (task_config->GetKeyExists("Disabled") && task_config->GetValue<bool>("Disabled", false)) {
                continue;
            }
            else {
                MakeTask(name, tasks_config->GetConfigurationObject(name));
            }
        }
    }
    else if (config->GetKeyExists("Task")) {
        MakeTask("Task", config->GetConfigurationObject("Task"));
    }
}

void Pipeline::SortInOutputs()
{
    sort(m_in_outputs.begin(), m_in_outputs.end(), utilities::Compare::CompareUnits<InOutput>);
}

void Pipeline::AddInOutputs(utilities::ConfigurationPtr config)
{
    auto local_config = config->GetConfigurationObject("InOutputs");

    m_logger.Info("The configuration file contains a definition for " +
                  std::to_string(local_config->GetNumObjectElements()) + " InOutput(s).");

    std::vector<std::string> names = local_config->GetObjectNames();

    for (const auto& name : names)
        AddInOutput(name, local_config->GetConfigurationObject(name));

    SortInOutputs();

    m_logger.Info("Order of InOutputs:");
    for (size_t i = 0; i < m_in_outputs.size(); i++) {
        m_logger.Info("InOutput " + std::to_string(i) + ". " + m_in_outputs[i]->GetName());
    }
}

int Pipeline::GetMainTickBufferSize() const
{
    return m_command_buffer_length;
}
Pipeline::~Pipeline()
{
    m_logger.Info("Time measurement results for InOutput tick calls:\n" +
                  m_in_output_tick_measurement.GetStatisticsUs("\t", m_mark_nondeterministic_log_output));

    if (m_tasks.size() > 0) {
        m_logger.Info("Time measurement results for InOutput MainTick calls:\n" +
                      m_in_output_main_tick_measurement.GetStatisticsUs("\t", m_mark_nondeterministic_log_output));
        m_logger.Info("Time measurement results for InOutput taskCompleted calls:\n" +
                      m_in_output_task_completed_measurement.GetStatisticsUs("\t", m_mark_nondeterministic_log_output));
    }
}

void Pipeline::AddInOutput(const std::string& in_output_name, utilities::ConfigurationPtr config)
{
    const std::string& in_output_type_name = config->GetValue<std::string>("Type", "InOutputTypeName");

    if (config->GetKeyExists("Disabled") && config->GetValue<bool>("Disabled", false)) {
        m_logger.Info("Skipping the creation of InOutput " + in_output_name + " of type " + in_output_type_name +
                      ", because the configuration file specified it should be disabled.");
        return;
    }

    if (InOutputFactory::Instance()->Exists(in_output_type_name)) {
        try {
            m_in_outputs.push_back(
                InOutputFactory::Instance()->Create(in_output_type_name, *this, in_output_name, config));
        }
        catch (const std::exception& e) {
            m_logger.Error("Exception raised while constructing InOutput of type \"" + in_output_type_name +
                           "\" with name \"" + in_output_name + "\": " + e.what());
            throw;
        }

        m_logger.Info("Successfully created InOutput of type \"" + in_output_type_name + "\" with name \"" +
                      in_output_name + "\" and execution order " +
                      std::to_string(m_in_outputs.back()->GetExecutionOrder()) + ".");
    }
    else
        throw std::runtime_error("The InOutput type \"" + in_output_type_name + "\" does not exist.");
}
void Pipeline::MakeTaskChecks()
{
    if (m_safety.GetLevel() != SafetyLevel::kUnprepared) {
        throw std::runtime_error(
            "It is only possible to add a Task to the Pipeline before calling Pipeline::Prepare().");
    }
}
int Pipeline::GetNextExecutionOrder() const
{
    return m_in_outputs.size();
}
size_t Pipeline::GetNumTasks() const
{
    return m_tasks.size();
}

void Pipeline::SortTasks()
{
    sort(m_tasks.begin(), m_tasks.end(), utilities::Compare::CompareTasks);
}

bool Pipeline::GetMarkNondeterministicLogOutput() const
{
    return m_mark_nondeterministic_log_output;
}

}  //namespace mpmca::pipeline