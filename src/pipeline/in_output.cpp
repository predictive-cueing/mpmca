/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/in_output.hpp"

#include "mpmca/pipeline/data_bucket.hpp"
#include "mpmca/pipeline/pipeline.hpp"
#include "mpmca/pipeline/task.hpp"

namespace mpmca::pipeline {
InOutput::InOutput(Pipeline& pipeline, const std::string& name, const std::string& type_name,
                   utilities::ConfigurationPtr config)
    : Unit(name, type_name, pipeline.GetSafety(), pipeline.GetStateMachine(), config)
{
}
InOutput::InOutput(Pipeline& pipeline, const std::string& name, const std::string& type_name)
    : Unit(name, type_name, pipeline.GetSafety(), pipeline.GetStateMachine(), pipeline.GetNextExecutionOrder(), 1000)
{
}
InOutput::~InOutput()
{
}
void InOutput::PrepareTaskMessageBus(const Task& task, MessageBus& message_bus)
{
}
void InOutput::CheckTaskMessageBusPrepared(const Task& task, const MessageBus&)
{
}
void InOutput::PrepareInOutputMessageBus(MessageBus&)
{
}
void InOutput::CheckInOutputMessageBusPrepared(const MessageBus&)
{
}
}  //namespace mpmca::pipeline