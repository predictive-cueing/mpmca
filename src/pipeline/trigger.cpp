/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/trigger.hpp"

namespace mpmca::pipeline {

Trigger::Trigger(const std::shared_ptr<Pipeline>& pipeline_ptr, int64_t max_iterations)
    : m_logger("Trigger", "Trigger")
    , m_pipeline_ptr(pipeline_ptr)
    , m_base_step_ms(m_pipeline_ptr->GetBaseStepMs())
    , m_start_time(std::chrono::steady_clock::now())
    , m_finished(true)
    , m_in_output_thread_running(false)
    , m_max_iterations(max_iterations)
{
    s_terminate_clean.store(false);

    for (size_t i = 0; i < pipeline_ptr->GetNumTasks(); ++i) {
        m_start_task_tick.emplace_back(0);
        m_task_thread_running.emplace_back(false);
    }

    // handle ctrl+c event
    struct sigaction sa;
    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = GotSignalStd;
    sigfillset(&sa.sa_mask);
    sigaction(SIGINT, &sa, NULL);
}

Trigger::~Trigger()
{
    s_terminate_clean.store(true);
    m_logger.Info("The Pipeline was signalled to terminate. Now waiting for all Pipeline threads to stop.");
    while (std::any_of(m_task_thread_running.begin(), m_task_thread_running.end(),
                       [](std::atomic<bool>& c) { return c.load(); }) ||
           m_in_output_thread_running.load()) {
        std::this_thread::sleep_for(std::chrono::microseconds(100));
    }
    m_logger.Info("All Pipeline threads stopped.");
}

int Trigger::ThreadedRun()
{
    m_logger.Info("Starting threaded run.");

    std::thread in_output_thread(&Trigger::InOutputLoop, this);
    std::vector<std::thread> task_threads;

    for (size_t i = 0; i < m_pipeline_ptr->GetNumTasks(); ++i) {
        task_threads.emplace_back(&Trigger::TaskLoop, this, i);
    }

    in_output_thread.join();

    for (size_t i = 0; i < task_threads.size(); ++i) {
        task_threads[i].join();
    }

    return m_pipeline_ptr->GetPipelineReturnCode();
}

int Trigger::FreeRun()
{
    m_logger.Info("Starting free run.");
    while (!s_terminate_clean.load()) {
        DoLogicBlocking();
        if (m_pipeline_ptr->GetTickCycle() % 5000 == 0) {
            m_logger.Info("At tick " + std::to_string(m_pipeline_ptr->GetTickCycle()) + ", simulation time is " +
                          std::to_string(m_pipeline_ptr->GetTickCycle() * m_pipeline_ptr->GetClock().GetDt()) + " s.");
        }

        SetTerminate(m_pipeline_ptr->GetTickCycle() >= m_max_iterations);
    }
    return m_pipeline_ptr->GetPipelineReturnCode();
}

void Trigger::InOutputLoop()
{
    bool expected = false;
    if (!m_in_output_thread_running.compare_exchange_strong(expected, true))
        throw(std::runtime_error("The InOutput thread loop was already running."));

    auto sleep_until = m_start_time;
    while (!s_terminate_clean.load()) {
        DoLogicNonblock();
        sleep_until += m_base_step_ms;
        std::this_thread::sleep_until(sleep_until);
        SetTerminate(m_pipeline_ptr->GetTickCycle() >= m_max_iterations);
    }

    m_in_output_thread_running.store(false);
}

void Trigger::TaskLoop(size_t task_index)
{
    bool expected = false;
    if (!m_task_thread_running[task_index].compare_exchange_strong(expected, true))
        throw(std::runtime_error("The task thread loop was already running."));

    auto sleep_time = std::chrono::microseconds(50);
    while (!s_terminate_clean.load() && m_pipeline_ptr->HasTaskSteps(task_index)) {
        while (m_start_task_tick[task_index].load() > 0) {
            if (m_start_task_tick[task_index].load() != 1)
                m_logger.Warning("StartTaskTick is now " + std::to_string(m_start_task_tick[task_index].load()));

            m_pipeline_ptr->MainTick(task_index);
            --m_start_task_tick[task_index];
        }
        std::this_thread::sleep_for(sleep_time);
    }

    m_task_thread_running[task_index].store(false);
}

void Trigger::SetTerminate(bool terminate)
{
    s_terminate_clean.store(s_terminate_clean.load() || terminate);
}

void Trigger::DoLogicBlocking()
{
    m_pipeline_ptr->Tick();

    for (size_t i = 0; i < m_pipeline_ptr->GetNumTasks(); ++i) {
        if (m_pipeline_ptr->GetCurrentTickIsMainTick(i)) {
            m_pipeline_ptr->MainTick(i);
        }
    }

    SetTerminate(GetTerminateFromPipeline());
}

void Trigger::DoLogicNonblock()
{
    bool expected = true;
    if (!m_finished.compare_exchange_strong(expected, false))
        throw(std::runtime_error("The previous DoLogic call was not finished yet."));

    m_pipeline_ptr->Tick();

    for (size_t i = 0; i < m_pipeline_ptr->GetNumTasks(); ++i) {
        if (m_pipeline_ptr->GetCurrentTickIsMainTick(i))
            ++m_start_task_tick[i];
    }

    SetTerminate(GetTerminateFromPipeline());

    m_finished.store(true);
}

bool Trigger::GetTerminateFromPipeline() const
{
    return m_pipeline_ptr->GetStateMachine().GetCurrentState() == State::kTerminate ||
           m_pipeline_ptr->GetStateMachine().GetCurrentState() == State::kFailTerminate;
}
}  //namespace mpmca::pipeline