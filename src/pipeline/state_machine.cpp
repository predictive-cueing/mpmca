/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/state_machine.hpp"

#include <algorithm>
#include <iostream>
#include <stdexcept>

#include "mpmca/pipeline/state_machine_client.hpp"

namespace mpmca::pipeline {
void StateMachine::AddStatePath(const StatePath& path)
{
    m_paths.push_back(path);
}
void StateMachine::GotoTransitionState(State transition_state)
{
    m_current_state_path = nullptr;

    if (StateIsAlwaysReachableState(transition_state)) {
        for (auto& p : m_paths) {
            if (p.GetTransitionState() == transition_state) {
                m_current_state_path = &p;
                break;
            }
        }
    }
    else {
        for (auto& p : m_paths) {
            if (p.GetStartState() == m_current_state && p.GetTransitionState() == transition_state) {
                m_current_state_path = &p;
                break;
            }
        }

        if (!m_current_state_path)
            throw(std::runtime_error("There is no StatePath starting at " + ToString(m_current_state) +
                                     " whose transition state is " + ToString(transition_state)));
    }

    m_current_state = m_current_state_path->GetTransitionState();
    m_main_state_changed_on_this_tick = true;

    for (auto& c : m_clients) {
        c->SetCurrentState(m_current_state);
    }
}

StateMachine::StateMachine()
    : m_current_state(State::kNone)
    , m_states_changed_on_this_tick(false)
    , m_main_state_changed_on_this_tick(false)
    , m_entry_state(State::kBooting)
{
    AddStatePath({State::kNone, State::kBooting, State::kBooted, State::kIdle, StatePath::Type::kRegular});
    AddStatePath({State::kIdle, State::kPreparing, State::kPrepared, State::kReady, StatePath::Type::kRegular});
    AddStatePath({State::kReady, State::kStarting, State::kStarted, State::kRun, StatePath::Type::kRegular});
    AddStatePath({State::kRun, State::kPausing, State::kPaused, State::kPause, StatePath::Type::kRegular});
    AddStatePath({State::kRun, State::kStopping, State::kStopped, State::kIdle, StatePath::Type::kRegular});
    AddStatePath({State::kPause, State::kResuming, State::kResumed, State::kRun, StatePath::Type::kRegular});
    AddStatePath({State::kIdle, State::kTerminating, State::kTerminated, State::kTerminate, StatePath::Type::kRegular});
    AddStatePath({State::kFail, State::kRecovering, State::kRecovered, State::kIdle, StatePath::Type::kFailure});
    AddStatePath({State::kNone, State::kFailing, State::kFailed, State::kFail, StatePath::Type::kFailure});
    AddStatePath({State::kFail, State::kFailTerminating, State::kFailTerminated, State::kFailTerminate,
                  StatePath::Type::kFailure});

    m_always_reachable_transition_states.push_back(State::kFailing);
    m_always_reachable_transition_states.push_back(State::kFailTerminating);

    m_exit_states.push_back(State::kTerminate);
    m_exit_states.push_back(State::kFailTerminate);

    GotoTransitionState(m_entry_state);

    m_main_state_changed_on_this_tick = false;
}
StateMachineClient StateMachine::MakeClient(const std::string& name)
{
    StateMachineClient ret(name, this);
    ret.SetCurrentState(m_current_state);
    m_clients.push_back(&ret);
    return ret;
}
StateMachineClient* StateMachine::MakeClientPtr(const std::string& name)
{
    StateMachineClient* ret = new StateMachineClient(name, this);
    ret->SetCurrentState(m_current_state);
    m_clients.push_back(ret);
    return ret;
}
StateMachineClient StateMachineTester::MakeClient(const std::string& name)
{
    return StateMachine::MakeClient(name);
}
StateMachineClient* StateMachineTester::MakeClientPtr(const std::string& name)
{
    return StateMachine::MakeClientPtr(name);
}
std::string StateMachine::GetMermaidDiagram() const
{
    std::string result = "```mermaid\nstateDiagram\n";

    std::set<std::string> transitions;

    std::set<std::string> failure_states;
    std::set<std::string> regular_states;

    for (StatePath path : m_paths) {
        std::string start_state_string = ToString(path.GetStartState());
        std::string transition_states_string =
            ToString(path.GetTransitionState());  // + "\\n" + ToString(path.GetTransitionDoneState());
        std::string transition_states_string_elaborate =
            ToString(path.GetTransitionState()) + "\\n" + ToString(path.GetTransitionDoneState());
        std::string next_state_string = ToString(path.GetNextState());

        if (start_state_string != "NONE" && next_state_string != "NONE")
            transitions.emplace(start_state_string + " --> " + next_state_string + " : " +
                                transition_states_string_elaborate);

        if (path.GetType() == StatePath::Type::kRegular) {
            regular_states.emplace(start_state_string);
            // regular_states.emplace(transition_states_string + " : " + transition_states_string_elaborate);
            regular_states.emplace(next_state_string);
        }
        else {
            failure_states.emplace(start_state_string);
            // failure_states.emplace(transition_states_string + " : " + transition_states_string_elaborate);
            failure_states.emplace(next_state_string);
        }
    }

    failure_states.erase("NONE");
    regular_states.erase("NONE");

    result += "state Failure {\n";

    for (const std::string& state_string : failure_states)
        result += "\t" + state_string + "\n";

    result += "\t[*] --> FAIL : FAILING\n";

    result += "}\n";

    result += "state Regular {\n";
    for (const std::string& state_string : regular_states)
        result += "\t" + state_string + "\n";

    result += "}\n";

    for (const std::string& transition_string : transitions)
        result += "\t" + transition_string + "\n";

    result += "\tRegular --> Failure\n";
    result += "\t[*] --> IDLE : BOOTING\\nBOOTED\n";
    result += "\tTERMINATE --> [*] : TERMINATING\\nTERMINATED\n";
    result += "\tFAIL_TERMINATE --> [*] : FAIL_TERMINATING\\nFAIL_TERMINATED\n";

    result += "```";

    return result;
}
std::string StateMachine::ListClients() const
{
    std::string ret = "";
    for (auto& c : m_clients) {
        std::string current_state = ToString(c->GetCurrentState());
        std::string goto_state = ToString(c->GetGoToState());

        ret += "\t" + std::string(std::max(1, 30 - (int)c->GetName().length()), ' ') + c->GetName() + "\t" +
               std::string(std::max(1, 10 - (int)current_state.length()), ' ') + current_state + "\t" +
               std::string(std::max(1, 10 - (int)goto_state.length()), ' ') + goto_state + "\n";
    }
    return ret;
}
int StateMachine::GetNumberClientsNotInState(const State& state) const
{
    return std::count_if(m_clients.cbegin(), m_clients.cend(),
                         [state](StateMachineClient* c) { return c->GetCurrentState() != state; });
}
bool StateMachine::GetStatesChangedOnThisTick() const
{
    return m_states_changed_on_this_tick;
}
bool StateMachine::GetMainStateChangedOnThisTick() const
{
    return m_main_state_changed_on_this_tick;
}
void StateMachine::PrintClients() const
{
    std::cout << ListClients() << std::endl;
}
const State& StateMachine::GetCurrentState() const
{
    return m_current_state;
}
void StateMachine::DetermineStatesChangedOnThisTick()
{
    if (m_clients.size() == 0) {
        m_states_changed_on_this_tick = false;
    }
    else {
        m_states_changed_on_this_tick = std::any_of(m_clients.cbegin(), m_clients.cend(), [](StateMachineClient* c) {
            return c->GetGoToState() != State::kNone;
        });
    }
}
bool StateMachine::ClientRequestedAlwaysReachableState(StateMachineClient* state_machine_client)
{
    if (state_machine_client->GetGoToState() == State::kNone) {
        return false;
    }
    else {
        return StateIsAlwaysReachableState(state_machine_client->GetGoToState());
    }
}
bool StateMachine::AnyClientRequestedAlwaysReachableState()
{
    return std::any_of(m_clients.cbegin(), m_clients.cend(),
                       [&](StateMachineClient* c) { return ClientRequestedAlwaysReachableState(c); });
}
bool StateMachine::StateIsAlwaysReachableState(State state) const
{
    return std::any_of(m_always_reachable_transition_states.cbegin(), m_always_reachable_transition_states.cend(),
                       [&](State always_reachable_state) { return state == always_reachable_state; });
}
void StateMachine::ResetCurrentStatePath()
{
    m_current_state_path = nullptr;
}
void StateMachine::Tick()
{
    DetermineStatesChangedOnThisTick();
}
void StateMachine::MainTick()
{
    m_main_state_changed_on_this_tick = false;
    DetermineStatesChangedOnThisTick();

    if (AnyClientRequestedAlwaysReachableState())
        ResetCurrentStatePath();

    if (m_current_state_path) {
        // while we are on a path

        for (auto& c : m_clients) {
            // check that a client is either in the transition or the TransitionDone state.
            if (c->GetCurrentState() != m_current_state_path->GetTransitionState() &&
                c->GetCurrentState() != m_current_state_path->GetTransitionDoneState())
                throw(std::runtime_error("StateMachineClient " + c->GetName() + " should be in " +
                                         ToString(m_current_state_path->GetTransitionState()) + " or in " +
                                         ToString(m_current_state_path->GetTransitionDoneState()) + " but is in " +
                                         ToString(c->GetCurrentState())));
        }

        // We are on a path state, so clients can only request to go to their TransitionDone state
        for (auto& c : m_clients) {
            if (c->GetGoToState() != State::kNone) {
                if (c->GetGoToState() != m_current_state_path->GetTransitionDoneState())
                    throw(std::runtime_error("While in state " + ToString(c->GetCurrentState()) +
                                             " the StateMachineClient " + c->GetName() + " wants to go to " +
                                             ToString(c->GetGoToState()) + ", but it can only go to (or stay in) " +
                                             ToString(m_current_state_path->GetTransitionDoneState())));
                else
                    c->SetCurrentState(c->GetGoToState());
            }
        }

        if (std::all_of(m_clients.begin(), m_clients.end(), [this](auto c) {
                return c->GetCurrentState() == m_current_state_path->GetTransitionDoneState();
            })) {
            // the state transition is done, because all clients declared they are done.
            for (auto& c : m_clients)
                c->SetCurrentState(m_current_state_path->GetNextState());

            m_current_state = m_current_state_path->GetNextState();
            m_main_state_changed_on_this_tick = true;
            m_current_state_path = nullptr;
        }
    }
    else {
        HandleRequestForTransitionState();
    }
}
void StateMachine::CheckAllClientsAreInCurrentState()
{
    // if we are currently not following a StatePath, then all clients should be in the same state as the main current
    // state. This function performs that assertion.
    for (auto& c : m_clients) {
        if (c->GetCurrentState() != m_current_state)
            throw(std::runtime_error("StateMachineClient " + c->GetName() + " should be in " +
                                     ToString(m_current_state) + " but is in " + ToString(c->GetCurrentState())));
    }
}
void StateMachine::HandleRequestForTransitionState()
{
    // We are not on a path state, so if a client has a goto state it must be a request to initiate a transition.
    State requested_transition = State::kNone;

    for (auto& c : m_clients) {
        if (c->GetGoToState() != State::kNone) {
            if (requested_transition != State::kNone && requested_transition != c->GetGoToState() &&
                !StateIsAlwaysReachableState(c->GetGoToState())) {
                throw(std::runtime_error("Another StateMachineClient already requested a transition to " +
                                         ToString(requested_transition)));
            }
            else {
                requested_transition = c->GetGoToState();
            }
        }
    }

    if (requested_transition != State::kNone)
        GotoTransitionState(requested_transition);
}

void StateMachine::TransitionStateMachineClient(StateMachineClient& state_machine_client)
{
    if (state_machine_client.GetCurrentState() == State::kBooting)
        state_machine_client.SetGoToState(State::kBooted);

    if (state_machine_client.GetCurrentState() == State::kPreparing)
        state_machine_client.SetGoToState(State::kPrepared);

    if (state_machine_client.GetCurrentState() == State::kStarting)
        state_machine_client.SetGoToState(State::kStarted);

    if (state_machine_client.GetCurrentState() == State::kPausing)
        state_machine_client.SetGoToState(State::kPaused);

    if (state_machine_client.GetCurrentState() == State::kStopping)
        state_machine_client.SetGoToState(State::kStopped);

    if (state_machine_client.GetCurrentState() == State::kResuming)
        state_machine_client.SetGoToState(State::kResumed);

    if (state_machine_client.GetCurrentState() == State::kTerminating)
        state_machine_client.SetGoToState(State::kTerminated);

    if (state_machine_client.GetCurrentState() == State::kFailing)
        state_machine_client.SetGoToState(State::kFailed);

    if (state_machine_client.GetCurrentState() == State::kRecovering)
        state_machine_client.SetGoToState(State::kRecovered);

    if (state_machine_client.GetCurrentState() == State::kFailTerminating)
        state_machine_client.SetGoToState(State::kFailTerminated);
}

}  //namespace mpmca::pipeline