/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/data_names.hpp"

namespace mpmca {

const utilities::DataProperties& DN::f_x_head()
{
    static utilities::DataProperties res("head", "f_x_head", "m/s2");
    return res;
}
const utilities::DataProperties& DN::f_y_head()
{
    static utilities::DataProperties res("head", "f_y_head", "m/s2");
    return res;
}
const utilities::DataProperties& DN::f_z_head()
{
    static utilities::DataProperties res("head", "f_z_head", "m/s2");
    return res;
}
const utilities::DataProperties& DN::omega_x_head()
{
    static utilities::DataProperties res("head", "omega_x_head", "rad/s");
    return res;
}
const utilities::DataProperties& DN::omega_y_head()
{
    static utilities::DataProperties res("head", "omega_y_head", "rad/s");
    return res;
}
const utilities::DataProperties& DN::omega_z_head()
{
    static utilities::DataProperties res("head", "omega_z_head", "rad/s");
    return res;
}
const utilities::DataProperties& DN::alpha_x_head()
{
    static utilities::DataProperties res("head", "alpha_x_head", "rad/s2");
    return res;
}
const utilities::DataProperties& DN::alpha_y_head()
{
    static utilities::DataProperties res("head", "alpha_y_head", "rad/s2");
    return res;
}
const utilities::DataProperties& DN::alpha_z_head()
{
    static utilities::DataProperties res("head", "alpha_z_head", "rad/s2");
    return res;
}
const utilities::DataProperties& DN::X_vehicle()
{
    static utilities::DataProperties res("world", "X_vehicle", "m");
    return res;
}
const utilities::DataProperties& DN::Y_vehicle()
{
    static utilities::DataProperties res("world", "Y_vehicle", "m");
    return res;
}
const utilities::DataProperties& DN::Z_vehicle()
{
    static utilities::DataProperties res("world", "Z_vehicle", "m");
    return res;
}
const utilities::DataProperties& DN::phi_vehicle()
{
    static utilities::DataProperties res("world", "phi_vehicle", "rad");
    return res;
}
const utilities::DataProperties& DN::theta_vehicle()
{
    static utilities::DataProperties res("world", "theta_vehicle", "rad");
    return res;
}
const utilities::DataProperties& DN::psi_vehicle()
{
    static utilities::DataProperties res("world", "psi_vehicle", "rad");
    return res;
}
const utilities::DataProperties& DN::u_vehicle()
{
    static utilities::DataProperties res("body", "u_vehicle", "m_s");
    return res;
}
const utilities::DataProperties& DN::v_vehicle()
{
    static utilities::DataProperties res("body", "v_vehicle", "m_s");
    return res;
}
const utilities::DataProperties& DN::w_vehicle()
{
    static utilities::DataProperties res("body", "w_vehicle", "m_s");
    return res;
}
const utilities::DataProperties& DN::p_vehicle()
{
    static utilities::DataProperties res("body", "p_vehicle", "rad/s");
    return res;
}
const utilities::DataProperties& DN::q_vehicle()
{
    static utilities::DataProperties res("body", "q_vehicle", "rad/s");
    return res;
}
const utilities::DataProperties& DN::r_vehicle()
{
    static utilities::DataProperties res("body", "r_vehicle", "rad/s");
    return res;
}
const utilities::DataProperties& DN::a_x_vehicle()
{
    static utilities::DataProperties res("body", "a_x_vehicle", "m/s2");
    return res;
}
const utilities::DataProperties& DN::a_y_vehicle()
{
    static utilities::DataProperties res("body", "a_y_vehicle", "m/s2");
    return res;
}
const utilities::DataProperties& DN::a_z_vehicle()
{
    static utilities::DataProperties res("body", "a_z_vehicle", "m/s2");
    return res;
}
const utilities::DataProperties& DN::p_dot_vehicle()
{
    static utilities::DataProperties res("body", "p_dot_vehicle", "rad/s2");
    return res;
}
const utilities::DataProperties& DN::q_dot_vehicle()
{
    static utilities::DataProperties res("body", "q_dot_vehicle", "rad/s2");
    return res;
}
const utilities::DataProperties& DN::r_dot_vehicle()
{
    static utilities::DataProperties res("body", "r_dot_vehicle", "rad/s2");
    return res;
}
const utilities::DataProperties& DN::car_throttle()
{
    static utilities::DataProperties res("", "car_throttle", "fraction");
    return res;
}
const utilities::DataProperties& DN::car_brake()
{
    static utilities::DataProperties res("", "car_brake", "fraction");
    return res;
}
const utilities::DataProperties& DN::car_throttle_dot()
{
    static utilities::DataProperties res("", "car_throttle_dot", "fraction/s");
    return res;
}
const utilities::DataProperties& DN::car_brake_dot()
{
    static utilities::DataProperties res("", "car_brake_dot", "fraction/s");
    return res;
}
const utilities::DataProperties& DN::car_clutch()
{
    static utilities::DataProperties res("car_clutch_fraction", "car_clutch", "fraction");
    return res;
}
const utilities::DataProperties& DN::car_engine_rotational_velocity()
{
    static utilities::DataProperties res("", "car_engine_rotational_velocity", "rad/s");
    return res;
}
const utilities::DataProperties& DN::car_engine_torque()
{
    static utilities::DataProperties res("", "car_engine_torque", "Nm");
    return res;
}
const utilities::DataProperties& DN::car_transmission_gear()
{
    static utilities::DataProperties res("", "car_transmission_gear", "integer");
    return res;
}
const utilities::DataProperties& DN::car_steer()
{
    static utilities::DataProperties res("car_steer_rad", "car_steer", "rad");
    return res;
}
const utilities::DataProperties& DN::car_shift()
{
    static utilities::DataProperties res("", "car_shift", "integer");
    return res;
}
const utilities::DataProperties& DN::car_automatic_gear_change()
{
    static utilities::DataProperties res("", "car_automatic_gear_change", "boolean");
    return res;
}
std::vector<std::string> DN::all_signal_names()
{
    return {f_x_head().GetName(),
            f_y_head().GetName(),
            f_z_head().GetName(),
            omega_x_head().GetName(),
            omega_y_head().GetName(),
            omega_z_head().GetName(),
            alpha_x_head().GetName(),
            alpha_y_head().GetName(),
            alpha_z_head().GetName(),
            X_vehicle().GetName(),
            Y_vehicle().GetName(),
            Z_vehicle().GetName(),
            phi_vehicle().GetName(),
            theta_vehicle().GetName(),
            psi_vehicle().GetName(),
            u_vehicle().GetName(),
            v_vehicle().GetName(),
            w_vehicle().GetName(),
            p_vehicle().GetName(),
            q_vehicle().GetName(),
            r_vehicle().GetName(),
            a_x_vehicle().GetName(),
            a_y_vehicle().GetName(),
            a_z_vehicle().GetName(),
            p_dot_vehicle().GetName(),
            q_dot_vehicle().GetName(),
            r_dot_vehicle().GetName(),
            car_throttle().GetName(),
            car_brake().GetName(),
            car_throttle_dot().GetName(),
            car_brake_dot().GetName(),
            car_clutch().GetName(),
            car_engine_rotational_velocity().GetName(),
            car_engine_torque().GetName(),
            car_transmission_gear().GetName(),
            car_steer().GetName(),
            car_shift().GetName(),
            car_automatic_gear_change().GetName()

    };
}
const utilities::DataProperties& DN::X_vehicle_0()
{
    static utilities::DataProperties res("world", "X_vehicle_0", "m");
    return res;
}
const utilities::DataProperties& DN::Y_vehicle_0()
{
    static utilities::DataProperties res("world", "Y_vehicle_0", "m");
    return res;
}
const utilities::DataProperties& DN::Z_vehicle_0()
{
    static utilities::DataProperties res("world", "Z_vehicle_0", "m");
    return res;
}
const utilities::DataProperties& DN::phi_vehicle_0()
{
    static utilities::DataProperties res("world", "phi_vehicle_0", "rad");
    return res;
}
const utilities::DataProperties& DN::theta_vehicle_0()
{
    static utilities::DataProperties res("world", "theta_vehicle_0", "rad");
    return res;
}
const utilities::DataProperties& DN::psi_vehicle_0()
{
    static utilities::DataProperties res("world", "psi_vehicle_0", "rad");
    return res;
}
const utilities::DataProperties& DN::gravity()
{
    static utilities::DataProperties res("world", "gravity", "m/s2");
    return res;
}
const utilities::DataProperties& DN::vehicle_mass()
{
    static utilities::DataProperties res("", "vehicle_mass", "kg");
    return res;
}
const utilities::DataProperties& DN::car_C_distance_rear_axle()
{
    static utilities::DataProperties res("body", "car_C_distance_rear_axle", "m");
    return res;
}
const utilities::DataProperties& DN::car_S_aero_force()
{
    static utilities::DataProperties res("body", "car_S_aero_force", "kg/m");
    return res;
}
const utilities::DataProperties& DN::car_S_brake_force()
{
    static utilities::DataProperties res("", "car_S_brake_force", "N/fraction");
    return res;
}
const utilities::DataProperties& DN::car_M1D_gear_ratio()
{
    static utilities::DataProperties res("", "car_M1D_gear_ratio", "1/m");
    return res;
}
const utilities::DataProperties& DN::car_M2D_engine_torque()
{
    static utilities::DataProperties res("", "car_M2D_engine_torque", "Nm");
    return res;
}
}  //namespace mpmca