#include "mpmca/types/rotation_matrix.hpp"
namespace mpmca {

RotationMatrix RotationMatrix::FromEulerAngles(double phi, double theta, double psi)
{
    // z-y′-x″ (intrinsic rotations) or x-y-z (extrinsic rotations): the intrinsic rotations are known as: yaw, pitch
    // and roll

    RotationMatrix T_roll, T_pitch, T_yaw;
    T_roll << 1., 0., 0., 0., std::cos(phi), -std::sin(phi), 0., std::sin(phi), std::cos(phi);
    T_pitch << std::cos(theta), 0., std::sin(theta), 0, 1., 0., -std::sin(theta), 0., std::cos(theta);
    T_yaw << std::cos(psi), -std::sin(psi), 0., std::sin(psi), std::cos(psi), 0., 0., 0., 1.;

    return (T_yaw * T_pitch * T_roll).transpose();
}
}  //namespace mpmca