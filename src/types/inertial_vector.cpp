/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/types/inertial_vector.hpp"
namespace mpmca {

InertialVector InertialVector::GetNeutral()
{
    InertialVector neutral = InertialVector::Zero();
    neutral.GetFz() = 9.81;
    return neutral;
}
}  //namespace mpmca