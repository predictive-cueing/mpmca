/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/types/pose_rot_mat_vector.hpp"

#include "mpmca/types/pose_vector.hpp"
#include "mpmca/types/transformation_matrix.hpp"
namespace mpmca {

TransformationMatrix PoseRotMatVector::GetTransformationMatrix() const
{
    TransformationMatrix T;
    T << GetT00(), GetT01(), GetT02(), GetT03(),  //
        GetT10(), GetT11(), GetT12(), GetT13(),  //
        GetT20(), GetT21(), GetT22(), GetT23(),  //
        0, 0, 0, 1;
    return T;
}
PoseVector PoseRotMatVector::ToPoseVector() const
{
    PoseVector ret;
    ret.GetX() = GetX();
    ret.GetY() = GetY();
    ret.GetZ() = GetZ();
    ret.GetPhi() = std::atan2(GetT12(), GetT22());
    ret.GetTheta() = std::atan2(-GetT02(), std::sqrt(GetT12() * GetT12() + GetT22() * GetT22()));
    ret.GetPsi() = std::atan2(GetT01(), GetT00());
    ret.GetXd() = GetXd();
    ret.GetYd() = GetYd();
    ret.GetZd() = GetZd();
    ret.GetP() = GetP();
    ret.GetQ() = GetQ();
    ret.GetR() = GetR();
    ret.GetXdd() = GetXdd();
    ret.GetYdd() = GetYdd();
    ret.GetZdd() = GetZdd();
    ret.GetPd() = GetPd();
    ret.GetQd() = GetQd();
    ret.GetRd() = GetRd();
    return ret;
}
}  //namespace mpmca