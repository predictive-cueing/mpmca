/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/types/pose_vector.hpp"

#include <iostream>

#include "mpmca/types/pose_rot_mat_vector.hpp"
#include "mpmca/types/rotation_matrix.hpp"
#include "mpmca/types/transformation_matrix.hpp"
namespace mpmca {

Vector<3> PoseVector::EulerRatesToAngularVelocities() const
{
    Vector<3> angular_velocities;

    double sin_phi = std::sin(GetPhi());
    double cos_phi = std::cos(GetPhi());
    double sin_theta = std::sin(GetTheta());
    double cos_theta = std::cos(GetTheta());

    angular_velocities[0] = GetP() - sin_theta * GetR();
    angular_velocities[1] = cos_phi * GetQ() + cos_theta * sin_phi * GetR();
    angular_velocities[2] = -sin_phi * GetQ() + cos_theta * cos_phi * GetR();

    return angular_velocities;
}

Vector<3> PoseVector::AccelerationsToSpecificForces(const Vector<3>& gravity_world) const
{
    RotationMatrix rotation_world_to_body = GetRotationMatrix();
    Vector<3> accelerations_world = Vector<3>::Zero();
    accelerations_world[0] = GetXdd();
    accelerations_world[1] = GetYdd();
    accelerations_world[2] = GetZdd();

    Vector<3> accelerations_body = rotation_world_to_body * accelerations_world;

    double cos_phi = std::cos(GetPhi());
    double sin_phi = std::sin(GetPhi());
    double cos_theta = std::cos(GetTheta());
    double sin_theta = std::sin(GetTheta());
    double cos_psi = std::cos(GetPsi());
    double sin_psi = std::sin(GetPsi());

    double gravity_x = (cos_theta * cos_psi) * gravity_world[0] +  // = 0
                       (cos_theta * sin_psi) * gravity_world[1] +  // = 0
                       (-sin_theta) * gravity_world[2];
    double gravity_y = (sin_phi * sin_theta * cos_psi - cos_phi * sin_psi) * gravity_world[0] +  // = 0
                       (sin_phi * sin_theta * sin_psi + cos_phi * cos_psi) * gravity_world[1] +  // = 0
                       (sin_phi * cos_theta) * gravity_world[2];
    double gravity_z = (cos_phi * sin_theta * cos_psi + sin_phi * sin_psi) * gravity_world[0] +  // = 0
                       (cos_phi * sin_theta * sin_psi - sin_phi * cos_psi) * gravity_world[1] +  // = 0
                       (cos_phi * cos_theta) * gravity_world[2];
    Vector<3> gravity_body;
    gravity_body << gravity_x, gravity_y, gravity_z;

    return gravity_body - accelerations_body;
}

Vector<3> PoseVector::EulerAccelerationsToAngularAccelerations() const
{
    Vector<3> angular_accelerations;

    double sin_phi = std::sin(GetPhi());
    double cos_phi = std::cos(GetPhi());
    double sin_theta = std::sin(GetTheta());
    double cos_theta = std::cos(GetTheta());

    angular_accelerations[0] = GetPd() - sin_theta * GetRd();
    angular_accelerations[1] = cos_phi * GetQd() + cos_theta * sin_phi * GetRd();
    angular_accelerations[2] = -sin_phi * GetQd() + cos_theta * cos_phi * GetRd();

    return angular_accelerations;
}

PoseVector PoseVector::Extrapolate(double t) const
{
    PoseVector pose_i = PoseVector::Zero();

    pose_i.GetX() = GetX() + GetXd() * t + 0.5 * t * t * GetXdd();
    pose_i.GetY() = GetY() + GetYd() * t + 0.5 * t * t * GetYdd();
    pose_i.GetZ() = GetZ() + GetZd() * t + 0.5 * t * t * GetZdd();
    pose_i.GetPhi() = GetPhi() + GetPhi() * t + 0.5 * t * t * GetPd();
    pose_i.GetTheta() = GetTheta() + GetTheta() * t + 0.5 * t * t * GetQd();
    pose_i.GetPsi() = GetPsi() + GetPsi() * t + 0.5 * t * t * GetRd();

    pose_i.GetXd() = GetXd() + t * GetXdd();
    pose_i.GetYd() = GetYd() + t * GetYdd();
    pose_i.GetZd() = GetZd() + t * GetZdd();
    pose_i.GetP() = GetP() + t * GetPd();
    pose_i.GetQ() = GetQ() + t * GetQd();
    pose_i.GetR() = GetR() + t * GetRd();

    pose_i.GetXdd() = GetXdd();
    pose_i.GetYdd() = GetYdd();
    pose_i.GetZdd() = GetZdd();
    pose_i.GetPd() = GetPd();
    pose_i.GetQd() = GetQd();
    pose_i.GetRd() = GetRd();

    return pose_i;
}

RotationMatrix PoseVector::GetRotationMatrix() const
{
    return RotationMatrix::FromEulerAngles(GetPhi(), GetTheta(), GetPsi());
}

TransformationMatrix PoseVector::GetTransformationMatrix() const
{
    TransformationMatrix T_roll, T_pitch, T_yaw, T_xyz, T;
    T_roll << 1., 0., 0., 0., 0, std::cos(GetPhi()), -std::sin(GetPhi()), 0., 0., std::sin(GetPhi()),
        std::cos(GetPhi()), 0., 0., 0., 0., 1.;

    T_pitch << std::cos(GetTheta()), 0., std::sin(GetTheta()), 0., 0, 1., 0., 0., -std::sin(GetTheta()), 0.,
        std::cos(GetTheta()), 0., 0., 0., 0., 1.;

    T_yaw << std::cos(GetPsi()), -std::sin(GetPsi()), 0., 0., std::sin(GetPsi()), std::cos(GetPsi()), 0., 0., 0., 0.,
        1., 0., 0., 0., 0., 1.;

    T_xyz << 1., 0., 0., GetX(),  //
        0., 1., 0., GetY(),  //
        0., 0., 1., GetZ(),  //
        0., 0., 0., 1.;

    T = T_xyz * (T_yaw * T_pitch * T_roll).transpose();
    return T;
}

PoseRotMatVector PoseVector::ToPoseRotMatVector() const
{
    RotationMatrix R = GetRotationMatrix();

    PoseRotMatVector ret;

    ret.GetX() = GetX();
    ret.GetY() = GetY();
    ret.GetZ() = GetZ();

    ret.GetT00() = R(0, 0);
    ret.GetT01() = R(0, 1);
    ret.GetT02() = R(0, 2);
    ret.GetT10() = R(1, 0);
    ret.GetT11() = R(1, 1);
    ret.GetT12() = R(1, 2);
    ret.GetT20() = R(2, 0);
    ret.GetT21() = R(2, 1);
    ret.GetT22() = R(2, 2);

    ret.GetXd() = GetXd();
    ret.GetYd() = GetYd();
    ret.GetZd() = GetZd();

    ret.GetP() = GetP();
    ret.GetQ() = GetQ();
    ret.GetR() = GetR();

    ret.GetXdd() = GetXdd();
    ret.GetYdd() = GetYdd();
    ret.GetZdd() = GetZdd();

    ret.GetPd() = GetPd();
    ret.GetQd() = GetQd();
    ret.GetRd() = GetRd();
    return ret;
}

}  //namespace mpmca