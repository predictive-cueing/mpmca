/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/types/transformation_matrix.hpp"

namespace mpmca {

TransformationMatrix TransformationMatrix::FromDenavitHartenbergParameters(double theta, double d, double alpha,
                                                                           double a)
{
    TransformationMatrix dh_matrix;
    // clang-format off
    dh_matrix
        <<  std::cos(theta), -std::sin(theta) * std::cos(alpha),  std::sin(theta) * std::sin(alpha), a * std::cos(theta),
            std::sin(theta),  std::cos(theta) * std::cos(alpha), -std::cos(theta) * std::sin(alpha), a * std::sin(theta),
            0.0,              std::sin(alpha),                    std::cos(alpha)                  , d,
            0.0,              0 , 0 , 1;
    // clang-format on
    return dh_matrix;
}

TransformationMatrix TransformationMatrix::FromPoseParameters(double x, double y, double z, double roll, double pitch,
                                                              double yaw)
{
    TransformationMatrix T_roll, T_pitch, T_yaw, T_xyz, T;
    // clang-format off
    T_roll << 1., 0., 0., 0., 0, std::cos(roll), -std::sin(roll), 0., 0., std::sin(roll), std::cos(roll), 0., 0., 0., 0., 1.;
    T_pitch << std::cos(pitch), 0., std::sin(pitch), 0., 0, 1., 0., 0., -std::sin(pitch), 0., std::cos(pitch), 0., 0., 0., 0., 1.;
    T_yaw << std::cos(yaw), -std::sin(yaw), 0., 0., std::sin(yaw), std::cos(yaw), 0., 0., 0., 0., 1., 0., 0., 0., 0., 1.;
    T_xyz << 1., 0., 0., x, 0., 1., 0., y, 0., 0., 1., z, 0., 0., 0., 1.;
    // clang-format on
    T = T_xyz * T_yaw * T_pitch * T_roll;

    return T;
}
}  //namespace mpmca