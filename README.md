# Model Predictive Motion Cueing Algorithm (MPMCA)

Documentation at [predictivecueing.com](https://www.predictivecueing.com).

Copyright Frank Drop and Mario Olivari ("Predictive cueing").
Please read carefully the terms and conditions (root/LICENCE.md) and
any accompanying documentation before you download and/or use
the Model Predictive Motion Cueing Algorithm (MPMCA) software.

## Dependencies, Docker and .devcontainer

Note that MPMCA relies on many dependencies, which can be quite a hassle to install correctly on a local machine.
A Docker container project providing a development and runtime environment is provided to get you started quickly.
You can either pull the container directly using `docker pull frankdrop/mpmca-dev` or see [this](https://hub.docker.com/r/frankdrop/mpmca-dev) page.
If you'd like to know what is inside the container or build from source, see this [project](https://gitlab.com/predictive-cueing/docker-containers) and [this](https://predictivecueing.com/docker/) documentation page.

If you are a user of an IDE that supports [dev containers](https://containers.dev/) (such as vscode), you can get going even quicker!
For vscode: simply open this repository inside your IDE, make sure the [dev containers extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) is installed, click the little blue button at the bottom left of the screen, and select the option to 'Reopen inside a container'.
For other IDEs, look for their specific documentation on how to use dev containers.

If you are building the Docker container or the MPMCA source on WSL (Windows), read [this](https://code.visualstudio.com/remote/advancedcontainers/improve-performance) page for hints on how to improve disk performance, for much faster builds and better performance of the MPMCA applications.

## Git submodules

Note that the project contains git submodules in the `extern` folder.
Check these submodules out before building, using:

`git submodules update --init --recursive`

## Building

For an out of source build, create a build folder, run `cmake ..`, then `make` (add the `-j` option to perform a parallel build).

## Documentation

The MPMCA documentation is included in this repository. You can find it in the `mkdocs/docs` directory.
To read the documentation, you can either read the Markdown files themselves or run a local instance of `predictivecueing.com`.
To do so, go to the mkdocs directory, and issue the following commands.
1. Install all required dependencies, using `pip install -r requirements.txt`.
2. Run the mkdocs server, using `mkdocs serve`.

## Release notes

### Version 1.0.0

First public release of the MPMCA software.