*[MPMCA]: Model Predictive Motion Cueing Algorithm
*[MCA]: Motion Cueing Algorithm
*[MPC]: Model Predictive Control
*[Step]: An instance of a class deriving from mpmca::pipeline::Step.
*[InOutput]: An instance of a class deriving from mpmca::pipeline::InOutput.
*[Pipeline]: The MPMCA Pipeline class mpmca::pipeline::Pipeline.