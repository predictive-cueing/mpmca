# Utilities

The functionality provided by the classes inside the `utilities` namespace is very diverse, so please head over to the doxygen generated (or in-source) documentation to learn more about them.