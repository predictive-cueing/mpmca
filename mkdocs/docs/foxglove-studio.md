# Foxglove Studio

The MPMCA contains functionality to save data to the MCAP format for visualization using [Foxglove Studio](https://foxglove.dev/studio).

For installation and usage instructions, head over to the [download page at foxglove.dev](https://foxglove.dev/download).

To get you started quicker, import the MPMCA dashboard layout file at `data/foxglove_studio/dashboard.json`.
See [this](https://foxglove.dev/docs/studio/layouts) page for instructions.