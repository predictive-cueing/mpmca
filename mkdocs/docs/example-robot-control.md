# Robot control

The application `demo_industrial_robot_pose` demonstrates that the MPMCA can also be used for robot pose control.
In this case, the MPC does not take the inertial signals (specific forces and rotational rates) as reference output, but the end-effector pose (position and orientation).
You can run the application using the command

`./build/bin/demo_industrial_robot_pose -c applications/demo_industrial_robot_pose/config.json`

from the root directory. Then use [Foxglove Studio](foxglove-studio.md) to visualize the output data file *robot_pose.mcap*.

--8<-- "../includes/sorry-incomplete.md"
