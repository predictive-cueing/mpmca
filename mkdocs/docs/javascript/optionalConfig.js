window.mermaidConfig = {
    startOnLoad: false,
    theme: "default",
    flowchart: {
        htmlLabels: false
    },
    er: {
        useMaxWidth: false
    },
    sequence: {
        useMaxWidth: false,
        noteFontWeight: "14px",
        actorFontSize: "14px",
        messageFontSize: "16px"
    },
    journey: {
        useMaxWidth: false
    },
    gitGraph: {
        useMaxWidth: false
    },
    pie: {
        useMaxWidth: false
    },
    gantt: {
        useMaxWidth: false
    }
}