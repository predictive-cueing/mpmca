# Welcome
 
Welcome to predictivecueing.com, home of the Model Predictive Motion Cueing Algorithm (MPMCA) software library.
The MPMCA is a realtime implementation of a Motion Cueing Algorithm (MCA) for human-in-the-loop motion simulators based on [Model Predictive Control](https://en.wikipedia.org/wiki/Model_predictive_control) (MPC), but it can also be used as a more generic MPC for robots or other dynamic systems.

--8<-- "../includes/get-the-source.md"

The MPMCA will attempt to reproduce the inertial signals of the simulated vehicle (specific forces, rotational rates and rotational accelerations) as accurately as possible on the simulator, while keeping it nicely within its (possibly non-linear) limits.
The MPMCA library contains four common simulator models that can be used as a starting point for your specific simulator:

1. a hexapod platform (sometimes referred to as a Gough-Stewart platform, see [this](https://en.wikipedia.org/wiki/Stewart_platform) page),
2. a hexapod platform placed on top of an "xy table" (providing motion in the horizontal plane) with a yaw platform on top of the hexapod (providing additional yaw motion space),
3. a generic six degree-of-freedom simulator for which the mechanical actuation system is not explicitly modelled, and 
4. a simulator based on a six degree-of-freedom industrial robot.

In addition to these four simulators, the MPMCA also contains an industrial robot whose six degree-of-freedom end-effector pose can be controlled using the same MPC.

The source-code of the MPMCA, mostly written in `c++`, is released under a license permitting you to use the software for the sole purpose of performing **non-commercial** scientific research, education, or artistic projects.
To obtain a license permitting commercial use, please contact frank@predictivecueing.com and see the [licensing](licensing.md) page.

The code contains a set of example applications that demonstrate the use of the MPMCA and can be used as a starting point for your own implementation.
A development and runtime [Docker container](docker.md) is provided to build a ready-to-go development environment that can be used to compile the MPMCA and applications using the MPMCA, and generate the model-specific code using the provided Python library.