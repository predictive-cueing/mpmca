# MPMCA literature

Scientific literature written by MPMCA authors on the topic of motion cueing or related topics.

<!-- [@cp:Katliar2018;@jp:Katliar2019;@cp:Drop2018a;@cp:Drop2018b;@jp:Mulder2018;@jp:Drop2013;@jp:Drop2018;@thesis:Drop2016;@cp:Olivari2018a] -->

\bibliography