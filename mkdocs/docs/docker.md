# Docker 

A Docker container project providing a development and runtime environment is provided to get you started quickly.
You can either pull the container directly using `docker pull frankdrop/mpmca-dev` or see [this](https://hub.docker.com/r/frankdrop/mpmca-dev) page.
If you'd like to know what is inside the container or build from source, see this [project](https://gitlab.com/predictive-cueing/docker-containers) and read on.

If you are a user of an IDE that supports [dev containers](https://containers.dev/) (such as vscode), you can get going even quicker!
For vscode: simply open this repository inside your IDE, make sure the [dev containers extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) is installed, click the little blue button at the bottom left of the screen, and select the option to 'Reopen inside a container'.
For other IDEs, look for their specific documentation on how to use dev containers.

If you are building the Docker container or the MPMCA source on WSL (Windows), read [this](https://code.visualstudio.com/remote/advancedcontainers/improve-performance) page for hints on how to improve disk performance, for much faster builds and better performance of the MPMCA applications.

## Build from source

[This](https://gitlab.com/predictive-cueing/docker-containers) project contains the `Dockerfile` for the development and runtime environment. To build it yourself, follow these steps.

1. Install Docker Engine on your system, see [this](https://docs.docker.com/engine/install/) page for instructions.
2. Execute the following command from the root directory of this repository to build the MPMCA docker container: `docker build . -t mpmca-dev`.
3. Create a new volume called `mpmca-develop-volume` using `docker volume create mpmca-develop-volume`. This volume will ensure your development efforts are persistent between sessions.
4. After building, start the container using `docker run -ti --mount 'type=volume,src=mpmca-develop-volume,dst=/home/mpmca-user/projects/' mpmca-dev`.
5. Clone the MPMCA repository into the `/home/mpmca-user/projects/` directory and you're good to go!

You can copy the built binaries to an Ubuntu 20.04 system and run them with minimal dependencies.
The MPMCA applications will dynamically link against some libraries that are installed in the first `apt-get` call, such as protobuf and libzstd.
If you cannot (or do not want to) install all packages that are installed through apt in the Docker image, use the `ldd` command to find out what libraries are required for the particular executable you want to run.

## Local installation

To install all dependencies locally on an Ubuntu installation, follow the commands in the `Dockerfile`.
Note that you should check out the exact same commits of all dependencies as the ones in the Docker project.
