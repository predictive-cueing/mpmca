# Introduction

To get you started, a few simple example applications are provided in the applications subfolder of the MPMCA repository.
Some of those applications are presented in more detail in this section of the documentation.