# Command line interface options

All MPMCA executables that make use of the `MainActor` class (i.e., most demo applications) provide certain options to be set from the command line interface. This functionality is documented by the executable itself. Append `--help` to get the documentation. For example: `./demo_oracle_predictor --help`.