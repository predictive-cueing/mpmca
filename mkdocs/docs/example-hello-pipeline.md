# Hello Pipeline!

The examples presented on this page try to be as simple as possible.
The resulting applications will not be particularly useful, but introduce a few basic concepts of the `Pipeline` in a practical manner.
These examples will not involve any predictor or controller.

The examples presented on this page are snippets from the file `applications/demo_hello_pipeline/main.cpp`.

## Example 1 - InOutputs only

In example 1, we create a `Pipeline` object, add a few `InOutput` instance and perform three periodic updates (ticks).

```c++
--8<-- "../../applications/demo_hello_pipeline/main.cpp:Example1"
```
Run this example, by running `./demo_hello_pipeline --run_example1`.
The expected output is:

```txt
--8<-- "../../data/application_output/demo_hello_pipeline/example1.txt"
```

Observe the following:

1. The two `HelloWorld` instances print debug output in their constructors, `Prepare()`, `Tick()` and `MainTick()` functions. The `EmptyInOutput` does not.
2. The `Pipeline` does not contain a `Task` with `Step` instances, and therefor, only the `Tick()` functions are called. The `MainTick()` functions are **not** called.
3. At the start of the first sample, all `InOutput` instances are in the `BOOTING` state. During this sample, they all report that they have completed booting (i.e., they are booted) and can advance to the `IDLE` state. They declare themselves `BOOTED`.
4. At the start of the second sample, the `StateMachine` is still in `BOOTING`, but detects that all `InOutput` instances have reported themselves `BOOTED`, and therefore moves the overall state to `IDLE`. The `InOutput` instances will be in `IDLE` when their `Tick()` function is called.
5. At the third sample, nothing happens to the `StateMachine` and therefor it produces no output.
6. When `Pipeline` object is destructed, it prints some time measurement related statistics.


## Example 2 - InOutputs and Steps

In example 2, we extend Example 1 by also adding a `Task` that will contain two `Step` instances. The update loop now performs 7 ticks.

```cpp
--8<-- "../../applications/demo_hello_pipeline/main.cpp:Example2"
```

Run this example, by running `./demo_hello_pipeline --run_example2`.
The expected output is as follows:

```txt
--8<-- "../../data/application_output/demo_hello_pipeline/example2.txt"
```

Observe the following:

1. Directly following the constructor calls, note that a lot of calls to `PrepareTaskMessageBus` and `CheckTaskMessageBusPrepared` are performed. This is not by mistake! That is, for each 'buffer element', a separate `TaskMessageBus` needs to be initialized and prepared. The option `command_buffer_length` is set to 3, and so three `TaskMessageBus` instances need to be prepared, once by each `InOutput` and `Step` instance.
2. The first sample is immediately both a `Tick` and a `MainTick`!
3. At the start of the program, the `InOutput` and `Step` instances are all in `BOOTING`, and declare themselves `BOOTED`.
4. In this example, a `Task` with `Step` instances is added to the `Pipeline`, therefore, the `MainTick()` function on the `InOutput` instances is called.
5. The global `StateMachine` will only change states on a `MainTick`! This prevents (under normal circumstances), that the global state changes while some expensive computations are performed in the `Task::MainTick()`.


## Example 3 - StateMachine demo

In the third example, we also add an `InOutput` of type `GoToRun`, which will move the `StateMachine` to the `RUN` state and back to `IDLE`, see [this](state-machine.md) page.

```cpp
--8<-- "../../applications/demo_hello_pipeline/main.cpp:Example3"
```

Run this example, by running `./demo_hello_pipeline --run_example3`.
The expected output is as follows:

```txt
--8<-- "../../data/application_output/demo_hello_pipeline/example3.txt"
```

Observe that:

1. The states follow the state flow as depicted [here](state-machine.md).
2. The global state only changes on `MainTick`s.