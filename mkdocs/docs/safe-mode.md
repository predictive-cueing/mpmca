# Safe mode

During running, the `InOutput` and `Step` instances can signal unrecoverable errors by throwing an exception.
The `Pipeline` can catch these exceptions and enter *safe mode*, if desired.
Recoverable failures should be handled using the [state machine](state-machine.md).

!!! danger "Do not use *safe mode* for safety critical functions!"

    The *safe mode* provided by the `Pipeline` provides a way to handle exceptions in the code gracefully. However, it should **not** be used to implement safety critical functions! Your hardware (simulator, robot, etc.) is responsible for keeping all occupants and bystanders safe, even if the code running in the `Pipeline` suddenly terminates, misbehaves, or communicates erroneous values.


## Configuration option

The "guarded" option of the `Pipeline` controls the behavior of the `Pipeline` in response to exceptions.

* If `guarded` is `true`, the `Pipeline` will catch exceptions and enter *safe mode*.
* If `guarded` is `false`, neither the `Pipeline` nor the `MainActor` will catch the exception, and thus the `MainActor::Run()` function will propagate the exception. The `Pipeline` is now in an unrecoverable state and should not be used anymore.

The guarded option is set through the `Pipeline::Options::guarded` boolean, or the `Pipeline.Guarded` option in a configuration file.

## Behavior

In *safe mode*, the `SafeTick()` methods of all `InOutput` instances are called only.
That is, in *safe mode*, the `Pipeline` will **not** call the `Tick()`, `MainTick()` and `TaskCompleted()` functions on `InOutput` and `Step` instances.
It will *only* call the `SafeTick()` method on `InOutput` instances.

The `SafeTick()` method does not allow you to communicate to other `InOutput` instances through a `MessageBus`.
In *safe mode*, each `InOutput` should function autonomously and should not rely on information obtained from other `InOutput` instances, because they might be the ones that threw the exception.

Be aware that *safe mode* should **not** be used to attempt to recover or continue operation.
It is meant to gracefully shut down communication with other systems (such as a simulator or robot) as quickly as possible.
