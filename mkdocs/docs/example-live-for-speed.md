# Driving simulation integration

The application `demo_live_for_speed` shows an example integration between the Pipeline and an existing driving simulation application.
Here, the driving simulation game [Live for Speed](https://www.lfs.net) (LFS) is used, but the general pattern should be the same.
A free demo version of LFS can be downloaded from the website and installed on a Windows PC.
Alternatively, you can install (a slightly older version of) LFS on Linux using `snap`, see [this](https://snapcraft.io/liveforspeed) page.
A powerful PC should be able to run LFS and the MPMCA simultaneously, but the best performance is obtained when both applications run on a separate system.

When running the application, you should use the configuration file `config_hexapod.json` or `config_xysim.json`.

--8<-- "../includes/sorry-incomplete.md"