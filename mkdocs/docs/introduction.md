# Introduction

Welcome to the documentation pages of the Model Predictive Motion Cueing Algorithm (MPMCA), developed by [Frank Drop](https://www.linkedin.com/in/frank-drop-115a972b/) and [Mario Olivari](https://www.linkedin.com/in/mario-olivari/).
The provided license governs the use of MPMCA in academic and educational environments.
Commercial use requires a commercial license.

--8<-- "../includes/get-the-source.md"

## What is the MPMCA?

The MPMCA is primarily a realtime implementation of a Motion Cueing Algorithm (MCA) for human-in-the-loop motion simulators based on [Model Predictive Control](https://en.wikipedia.org/wiki/Model_predictive_control).
An MCA is an algorithm that converts the inertial signals (accelerations and rotational rates) of a simulated vehicle into motions of a simulator, such that the person inside the simulator perceives inertial signals that are as accurate as possible.
It consists of a very fast (realtime capable) implementation of a non-linear Model Predictive Controller for typical motion simulator designs (such as a [hexapod Gough-Stewart platform](https://en.wikipedia.org/wiki/Stewart_platform)), and a framework for predicting the inertial reference signals for a vehicle following a predefined path.

But, the MPMCA can do a lot more than motion cueing!
As demonstrated in one of the example applications, the MPMCA can also be used to control a serial robot to let the end-effector follow a trajectory through space.
And with a bit of imagination, the MPMCA framework can be used for realtime Model Predictive Control of just *any* system.


## Main components of the MPMCA

The MPMCA consists of three main components: the Controller, the Predictor, and the Pipeline.

The **Controller** is a realtime implementation of a Non-Linear Model Predictive Controller.
The Controller will calculate the control inputs that will make the output of the system follow the provided reference output as optimally as possible, given the (non-linear) constraints and cost-function weights.
The constraints of the system can be simple 'box constraints', i.e., minimum and maximum values for a particular state or input, or more complex constraints, such as the constraints (position, velocity, and acceleration) imposed by the six actuators of a hexapod motion simulator.
The cost-function weights can be used to 'tune' the behavior of the controller. For example, the importance of tracking longitudinal accelerations with respect to tracking lateral accelerations can be influenced by increasing or decreasing the weights on the tracking errors for those outputs.
Typical performance metrics of the Controller on a fast, modern CPU: calculating the optimal control inputs for a hexapod simulator with complex constraints, with a horizon length of approximately 75 samples (giving a horizon time length of approx 10 seconds), takes between 3 and 10 milliseconds.

The **Predictor** calculates a *prediction* of the reference output signals, based (for example) on the current state of the simulation.
The MPMCA currently provides two types of predictors.

* The purpose of the **Inertial predictor** is to predict the inertial output signals of a vehicle (e.g., a car, aircraft, bicycle, boat, etc.) following a path through space (e.g., a road, a Tunnel-in-the-Sky, a narrow river, etc.), by simulating a simplified model of the vehicle, the human, and the environment, for the full duration of the controller horizon. Typical performance metrics of the Inertial predictor on a fast, modern CPU: simulating a car driven by a human following a road for a horizon length of 10 seconds takes approximately 1 millisecond (typically less).
* The purpose of the **Pose predictor** is to 'predict' (or rather: prescribe) the desired future pose (position and attitude) of the end-effector of the simulator (in this case the term robot is probably better than simulator).

The **Pipeline** is a framework aimed at integrating the Predictor and Controller with each other, and integrating them with an actual human-in-the-loop simulator running within a (distributed) simulation environment.
It is possible to use the Controller and Predictor without the Pipeline, but it can be very beneficial to use the Pipeline, as it provides a solution to a few fundamental challenges related to using an MPC controller for motion cueing. Some features of the Pipeline: 

- it consists of highly configurable components (Step and InOutput) that typically serve one single task and can be reused easily in separate projects,
- it uses standardized interfaces for the communication of data (providing modularity and promoting reuse of components);
- it provides functionality to align the state of the MPMCA with the state of the simulator through a state machine (e.g., initialization, run, pause and stop states);
- it provides functionality for dealing with occasional computational overruns of the iterative solver used by the MPC; and
- it provides basic functionality to detect and handle failures and errors in the software gracefully.


## Before you start (some disclaimers)

Here are a few things to consider before you start working with the MPMCA:

1. The software is being developed and tested on modern versions of Ubuntu (20.04 and 22.04). Variants of the MPMCA have been compiled successfully on Windows, WSL, and several Linux distributions other than Ubuntu. However, do note that the software is not actively tested on these platforms!
2. The version released to the public (for academic use) should be considered a beta version. That is, depending on initial feedback, we might decide to make structural changes to the code that might break any code you write against the MPMCA. Obviously, we will try to keep such changes to a minimum. If you're planning on larger developments with the MPMCA, get in touch!
3. The software relies on quite a few third party libraries. To get you going quicker, a Docker container is available that can be used for evaluation or more permanent development purposes.
4. Read the license file carefully! Essentially, the license permits you to use the MPMCA for academic purposes and requires you to obtain a commercial license from [predictive cueing](https://www.predictivecueing.com) for any commercial use.
5. The MPMCA can only work properly if the internal model of the simulator (or robot) is identical to the simulator it is actually controlling. It is your responsibility to make sure that you use the correct model for your simulator. The MPMCA comes with a number of example simulators that can be used as a basis for defining your own simulator model.
6. The MPMCA is intended to be used as a Motion Cueing Algorithm or high-level controller providing setpoints for individual degrees-of-freedom or actuators. Its use assumes that lower-level control loops and/or dedicated safety systems provide *all* safety functions. That is, the control inputs calculated by the MPMCA must always be considered as 'unsafe' and should be checked by independent systems before providing them to the actual hardware. The MPMCA is explicitly **not** intended to be used as a low-level controller that directly controls hardware without a dedicated safety system.

## Documentation and literature

Besides these documentation pages, some other sources of information are available:

1. [Doxygen generated pages](https://www.predictivecueing.com/doxy) compiled from in-source documentation.
1. The scientific papers written by the authors of the MPMCA, listed on [this](literature.md) page.
1. [Youtube video](https://www.youtube.com/watch?v=4OFMcHZ4fQc) related to [@cp:Drop2018a].
1. [Youtube video](https://www.youtube.com/watch?v=uSRo6o1Wy5s) related to [@cp:Drop2018b].

\bibliography