# State machine

--8<-- "../includes/sorry-incomplete.md"

```mermaid
stateDiagram
state Failure {
        FAIL
        FAIL_TERMINATE
        IDLE
        [*] --> FAIL : FAILING
}
state Regular {
        IDLE
        PAUSE
        READY
        RUN
        TERMINATE
}
        FAIL --> FAIL_TERMINATE : FAIL_TERMINATING\nFAIL_TERMINATED
        FAIL --> IDLE : RECOVERING\nRECOVERED
        IDLE --> READY : PREPARING\nPREPARED
        IDLE --> TERMINATE : TERMINATING\nTERMINATED
        PAUSE --> RUN : RESUMING\nRESUMED
        READY --> RUN : STARTING\nSTARTED
        RUN --> IDLE : STOPPING\nSTOPPED
        RUN --> PAUSE : PAUSING\nPAUSED
        Regular --> Failure
        [*] --> IDLE : BOOTING\nBOOTED
        TERMINATE --> [*] : TERMINATING\nTERMINATED
        FAIL_TERMINATE --> [*] : FAIL_TERMINATING\nFAIL_TERMINATED
```