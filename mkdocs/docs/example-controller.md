# Minimal Controller 

The `demo_minimal_controller` application provides a minimalistic example of the code in the controller namespace for Model Predictive Control of a simple simulator.
The Constraint Free Simulator (CFS) is simply a 'point' moving through space, with 6 degrees of freedom, constrained by minimum and maximum values for each degree of freedom.

The example shows:

1. how to create a Controller object for the type of simulator to be controlled,
2. the basic configuration options,
3. how to call the update methods of the controller, and
4. the way in which the state of the simulator needs to be updated for the next control cycle.

The example is documented in comments in the code.

```cpp
--8<-- "../../applications/demo_minimal_controller/main.cpp"
```

