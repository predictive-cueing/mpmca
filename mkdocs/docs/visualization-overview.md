# Visualization

The MPMCA involves many different components that are rather complex by themselves and interact with each other in many different ways.
Understanding what is going on inside the algorithm is difficult.
The `visualization` namespace contains a number of tools to convert the data stored in certain *Messages* on a `MessageBus` into the [MCAP data format](https://mcap.dev/) that can be visualized using [Foxglove Studio](https://foxglove.dev/).
For instructions on how to use Foxglove Studio with the MPMCA, see [this](foxglove-studio.md) page.

The Step `McapLogger` is able to convert and log certain types of *Messages* (or specific data fields inside those *Messages*) on the *TaskMessageBus* to a file on disk for offline visualization.

The conversion from internal representation of the data into MCAP format cannot be 'automatic' and needs to be manually implemented for every type of data, even if you only want to 'plot' the data.
That is, a specific implementation that derives from `FoxgloveMessageConvertor` needs to be developed for every type of Message.
A limited number of (incomplete) convertors are provided with the MPMCA:

- The `AutomotivePredictionPathConvertor` will convert the `Path` used by the `AutomotivePredictor` into a set of cubes that resemble the outer limits of the `Path`.
- The `AutomotivePredictionHorizonConvertor` convert the signals used inside the `AutomotivePredictor` into data arrays for plotting.
- The `ControllerDataConvertor` will convert important signals provided to or by the Controller into data arrays for plotting and into data structures that allow 3-dimensional visualization of the current pose of the simulator.

The provided visualizations are rather schematic, but can be 'dressed-up' by the user (e.g. using 3D models of the actual simulator) to be more informative or aesthetic.
Please refer to the Foxglove Studio documentation for more information.

