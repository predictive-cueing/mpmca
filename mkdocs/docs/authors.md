# Authors

## Frank Drop

Entrepreneurial [Aerospace engineer](https://www.tudelft.nl/lr), with a strong background in automatic control and human-machine interaction ([Control & Simulation group](https://cs.lr.tudelft.nl/)).
He obtained a PhD ([thesis](https://research.tudelft.nl/en/publications/control-theoretic-models-of-feedforward-in-manual-control)) from the [Max Planck Institute (MPI) for Biological Cybernetics](https://www.kyb.tuebingen.mpg.de/en) in Tübingen, Germany, and Delft University of Technology, Delft, the Netherlands in 2016.
Frank is working on Model Predictive Motion Cueing since 2016.
At first, as a scientist at MPI.
In 2019, he founded DOK Predictive Motion Cueing GmbH as a spinoff of the Max Planck Institute for Biological Cybernetics.
The GmbH unfortunately did not make it through the Corona pandemic, and Frank joined [Cruden BV](https://www.cruden.com) mid-2020.
At Cruden, Frank worked as an engineer on automotive driving simulators and led the software team developing the Panthera software suite.
In January 2023, Frank joined [TNO](https://www.tno.nl/en/) (the Dutch Applied Research Institute), taking roles as System Engineer, Project Manager and Software developer.

## Mario Olivari

I am currently a Software Engineer at the Airbus Defence and Space GmbH. Furthermore I am a co-founder and the CTO at Predictive Cueing.