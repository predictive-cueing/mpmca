# Motion cueing literature

This page contains a list of motion cueing related literature not published by any of the MPMCA authors, but very much worth reading.
Do you want to get your publication listed? Send us a message.

<!-- 2023 -->
<!-- [@jp:Pham2023] -->
<!-- [@cp:Chadha2023] -->

<!-- 2022 -->

<!-- [@jp:Lamprecht2022]  -->
<!-- [@cp:Kolff2022] -->

<!-- 2020 -->
<!-- [@cp:Biemelt2020] -->
<!-- [@cp:Kolff2020] -->
<!-- [@jp:Ellensohn2020] -->
<!-- [@cp:Koyoncu2020] -->
<!-- [@thesis:Cleij2020] -->
<!-- [@thesis:Erler2020] -->

<!-- 2019 -->
<!-- [@jp:Cleij2019] -->
<!-- [@jp:Ellensohn2019a] -->
<!-- [@jp:Ellensohn2019b] -->
<!-- [@cp:Lamprecht2019a] -->
<!-- [@cp:Lamprecht2019b] -->
<!-- [@cp:Mulder2019a] -->
<!-- [@jp:Mohammadi2019] -->


<!-- 2018 -->
<!-- [@cp:Asadi2018] -->
<!-- [@cp:Biemelt2018] -->
<!-- [@jp:Cleij2018] -->
<!-- [@cp:Lamprecht2018] -->

<!-- 2017 -->
<!-- [@cp:Fang2017] -->

<!-- 2016 -->
<!-- [@cp:Venrooij2016] -->
<!-- [@cp:Mohammadi2016] -->

<!-- 2013 -->
<!-- [jp:Garrett2013] -->

<!-- 2012 -->
<!-- [@cp:Beghi2012] -->

<!-- 2009 -->
<!-- [@jp:Dagdelen2009] -->

<!-- 2004 -->
<!-- [@cp:Dagdelen2004] -->

\bibliography