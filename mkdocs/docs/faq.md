# Frequently asked questions

## I get compiler errors related to missing Foxglove files.

The compiler error looks similar to:

```bash
In file included from mpmca/src/visualization/automotive_prediction_path_convertor.cpp:7:
/mpmca/include/mpmca/visualization/automotive_prediction_path_convertor.hpp:8:10: fatal error: foxglove/FrameTransform.pb.h: No such file or directory
    8 | #include "foxglove/FrameTransform.pb.h"
      |          ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
compilation terminated.
```

You probably forgot to initialize all submodules before running `cmake ..`. Remove the contents of the build folder (`rm -rf build/*`), run `git submodules update --init`, then `cd build` and `cmake ..`.
Running `make` should now run without errors.

## I try to use the OMSF inside the devcontainer, but it does not work.

Although the OMSF is a dependency of MPMCA (and therefore contained within the development container image), it is not supposed to work itself inside the container. The OMSF has a few dependencies to run offline optimizations which are very large and therefore not installed. MPMCA uses parts of the OMSF for generating model code (using CasADi), but never actually runs optimizations using OMSF.
If you want to use the OMSF directly, follow the instructions in the OMSF readme at their [GitHub page](https://github.com/mkatliar/omsf).