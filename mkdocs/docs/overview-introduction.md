# Introduction

The MPMCA consists of several modules (recognizable in the code as a separate namespace) that each have their own function within the framework:

- [Pipeline](pipeline-overview.md). The Pipeline provides functionality to integrate the predictor and controller together, and integrate the cueing algorithm with the outside world.
- [Control](control-overview.md). All functionality related to the Model Predictive Controller used to calculate the set of inputs to optimally track the reference output signals.
- [Prediction](prediction-overview.md). All functionality related to calculating a prediction of the future (inertial or pose) reference output signals, based on the current state of the simulation.
- [Messages](messages-overview.md). Standardized interfaces for communication of specific information between elements in the Pipeline.
- [Utilities](utilities-overview.md). Several utility classes used throughout the Pipeline, providing functionality such as a configuration file reader, UDP senders and receivers, etc.
- [Visualization](visualization-overview.md). All functionality related to visualizing the behavior of the MPMCA using the [Foxglove Studio](https://foxglove.dev/) data visualizer.