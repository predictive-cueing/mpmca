/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <cereal/types/vector.hpp>
#include <vector>

#include "mpmca/cereal_tools.hpp"
#include "mpmca/messages/message_id_macro_definition.hpp"
#include "mpmca/types/inertial_vector.hpp"
#include "mpmca/utilities/horizon.hpp"
namespace mpmca::messages {

template <typename TSIM>
struct ControllerInertialPlan
{
    DEFINE_MPMCA_PIPELINE_MESSAGE_ID("ControllerInertialPlan<", TSIM::GetName(), ">");

    ControllerInertialPlan(const utilities::Horizon& horizon)
    {
        i_plan.resize(horizon.GetNMpc(), InertialVector::Zero());
    }

    template <class Archive>
    void serialize(Archive& archive)
    {
        archive(i_plan);
    }

    std::vector<InertialVector> i_plan;
};
}  //namespace mpmca::messages