/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <vector>

#include "mpmca/messages/message_id_macro_definition.hpp"
#include "mpmca/utilities/horizon.hpp"

namespace mpmca::messages {

template <typename TSIM>
struct ControllerStatePlan
{
    DEFINE_MPMCA_PIPELINE_MESSAGE_ID("ControllerStatePlan<", TSIM::GetName(), ">");
    ControllerStatePlan(const utilities::Horizon& horizon,
                        const typename TSIM::StateVector& x_initial = TSIM::StateVector::Zero())
    {
        x_plan.resize(horizon.GetNMpcPred(), x_initial);
    }
    template <class Archive>
    void serialize(Archive& archive)
    {
        archive(x_plan);
    }

    std::vector<typename TSIM::StateVector> x_plan;
};
}  //namespace mpmca::messages