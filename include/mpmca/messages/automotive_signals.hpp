/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/messages/message_id_macro_definition.hpp"

namespace mpmca::messages {

struct AutomotiveSignals
{
    DEFINE_MPMCA_PIPELINE_MESSAGE_ID("AutomotiveSignals");

    double car_throttle_fraction = 0;
    double car_brake_fraction = 0;
    double car_throttle_dot_fraction_s = 0;
    double car_brake_dot_fraction_s = 0;
    double car_clutch_fraction = 0;
    double car_engine_rotational_velocity_rad_s = 0;
    double car_engine_torque_Nm = 0;
    double car_transmission_gear_integer = 0;
    double car_steer_rad = 0;
    double car_shift_integer = 0;
    double car_automatic_gear_change_boolean = 0;

    template <class Archive>
    void serialize(Archive& archive)
    {
    }
};
}  //namespace mpmca::messages
