/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <cereal/types/string.hpp>
#include <cereal/types/vector.hpp>
#include <vector>

#include "mpmca/messages/message_id_macro_definition.hpp"

namespace mpmca::messages::testing {

struct McapTestMessage
{
    DEFINE_MPMCA_PIPELINE_MESSAGE_ID("McapTestMessage");

    std::string variable_string;
    double variable_double;
    int variable_int;
    bool variable_bool;
    std::vector<double> variable_double_vector;

    template <class Archive>
    void serialize(Archive& archive)
    {
        archive(variable_string, variable_double, variable_int, variable_bool, variable_double_vector);
    }
};

}  //namespace mpmca::messages::testing
