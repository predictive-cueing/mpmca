/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <memory>

#include "mpmca/messages/message_id_macro_definition.hpp"

namespace mpmca::predict::inertial {
class Path;
}

namespace mpmca::messages {

struct AutomotivePredictionPath
{
    DEFINE_MPMCA_PIPELINE_MESSAGE_ID("AutomotivePredictionPath");

    std::shared_ptr<mpmca::predict::inertial::Path> path_ptr;

    template <class Archive>
    void serialize(Archive& archive)
    {
    }
};
}  //namespace mpmca::messages
