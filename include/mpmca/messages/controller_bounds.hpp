/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <cereal/types/vector.hpp>

#include "mpmca/cereal_tools.hpp"
#include "mpmca/messages/message_id_macro_definition.hpp"

namespace mpmca::messages {

template <typename TSIM>
struct ControllerBounds
{
    DEFINE_MPMCA_PIPELINE_MESSAGE_ID("ControllerBounds<", TSIM::GetName(), ">");

    typename TSIM::StateVector x_lower_bound;
    typename TSIM::StateVector x_upper_bound;
    typename TSIM::InputVector u_lower_bound;
    typename TSIM::InputVector u_upper_bound;
    typename TSIM::ConstraintVector c_lower_bound;
    typename TSIM::ConstraintVector c_upper_bound;

    template <class Archive>
    void serialize(Archive& archive)
    {
        archive(x_lower_bound, x_upper_bound, u_lower_bound, u_upper_bound, c_lower_bound, c_upper_bound);
    }
};
}  //namespace mpmca::messages