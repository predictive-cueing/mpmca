/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <vector>

#include "mpmca/messages/message_id_macro_definition.hpp"
#include "mpmca/utilities/horizon.hpp"

namespace mpmca::messages {

struct AutomotivePredictionHorizon
{
    DEFINE_MPMCA_PIPELINE_MESSAGE_ID("AutomotivePredictionHorizon");

    AutomotivePredictionHorizon(const utilities::Horizon& horizon)
        : world_X_vehicle_m(horizon.GetNPrediction(), 0)
        , world_Y_vehicle_m(horizon.GetNPrediction(), 0)
        , world_Z_vehicle_m(horizon.GetNPrediction(), 0)
        , world_phi_vehicle_rad(horizon.GetNPrediction(), 0)
        , world_theta_vehicle_rad(horizon.GetNPrediction(), 0)
        , world_psi_vehicle_rad(horizon.GetNPrediction(), 0)
        , body_u_vehicle_m_s(horizon.GetNPrediction(), 0)
        , body_v_vehicle_m_s(horizon.GetNPrediction(), 0)
        , body_w_vehicle_m_s(horizon.GetNPrediction(), 0)
        , body_p_vehicle_rad_s(horizon.GetNPrediction(), 0)
        , body_q_vehicle_rad_s(horizon.GetNPrediction(), 0)
        , body_r_vehicle_rad_s(horizon.GetNPrediction(), 0)
        , body_a_x_vehicle_m_s2(horizon.GetNPrediction(), 0)
        , body_a_y_vehicle_m_s2(horizon.GetNPrediction(), 0)
        , body_a_z_vehicle_m_s2(horizon.GetNPrediction(), 0)
        , body_p_dot_vehicle_rad_s2(horizon.GetNPrediction(), 0)
        , body_q_dot_vehicle_rad_s2(horizon.GetNPrediction(), 0)
        , body_r_dot_vehicle_rad_s2(horizon.GetNPrediction(), 0)
        , head_f_x_head_m_s2(horizon.GetNPrediction(), 0)
        , head_f_y_head_m_s2(horizon.GetNPrediction(), 0)
        , head_f_z_head_m_s2(horizon.GetNPrediction(), 0)
        , head_omega_x_head_rad_s(horizon.GetNPrediction(), 0)
        , head_omega_y_head_rad_s(horizon.GetNPrediction(), 0)
        , head_omega_z_head_rad_s(horizon.GetNPrediction(), 0)
        , head_alpha_x_head_rad_s2(horizon.GetNPrediction(), 0)
        , head_alpha_y_head_rad_s2(horizon.GetNPrediction(), 0)
        , head_alpha_z_head_rad_s2(horizon.GetNPrediction(), 0)
        , world_X_path_m(horizon.GetNPrediction(), 0)
        , world_Y_path_m(horizon.GetNPrediction(), 0)
        , world_Z_path_m(horizon.GetNPrediction(), 0)
        , world_phi_path_rad(horizon.GetNPrediction(), 0)
        , world_theta_path_rad(horizon.GetNPrediction(), 0)
        , world_psi_path_rad(horizon.GetNPrediction(), 0)
        , car_steer_rad(horizon.GetNPrediction(), 0)
        , car_throttle_fraction(horizon.GetNPrediction(), 0)
        , car_brake_fraction(horizon.GetNPrediction(), 0)
        , car_clutch_fraction(horizon.GetNPrediction(), 0)
        , car_shift_integer(horizon.GetNPrediction(), 0)
        , human_car_u_t_m_s(horizon.GetNPrediction(), 0)
        , human_car_u_e_m_s(horizon.GetNPrediction(), 0)
        , human_car_lat_e_m(horizon.GetNPrediction(), 0)
        , human_car_lat_e_dot_m_s(horizon.GetNPrediction(), 0)
        , human_car_psi_t_rad(horizon.GetNPrediction(), 0)
        , human_car_psi_e_rad(horizon.GetNPrediction(), 0)
        , car_transmission_gear_integer(horizon.GetNPrediction(), 0)
        , car_beta_vehicle_rad(horizon.GetNPrediction(), 0)
        , car_delta_f_rad(horizon.GetNPrediction(), 0)
    {
    }

    template <class Archive>
    void serialize(Archive& archive)
    {
    }

    std::vector<double> world_X_vehicle_m;
    std::vector<double> world_Y_vehicle_m;
    std::vector<double> world_Z_vehicle_m;
    std::vector<double> world_phi_vehicle_rad;
    std::vector<double> world_theta_vehicle_rad;
    std::vector<double> world_psi_vehicle_rad;
    std::vector<double> body_u_vehicle_m_s;
    std::vector<double> body_v_vehicle_m_s;
    std::vector<double> body_w_vehicle_m_s;
    std::vector<double> body_p_vehicle_rad_s;
    std::vector<double> body_q_vehicle_rad_s;
    std::vector<double> body_r_vehicle_rad_s;
    std::vector<double> body_a_x_vehicle_m_s2;
    std::vector<double> body_a_y_vehicle_m_s2;
    std::vector<double> body_a_z_vehicle_m_s2;
    std::vector<double> body_p_dot_vehicle_rad_s2;
    std::vector<double> body_q_dot_vehicle_rad_s2;
    std::vector<double> body_r_dot_vehicle_rad_s2;
    std::vector<double> head_f_x_head_m_s2;
    std::vector<double> head_f_y_head_m_s2;
    std::vector<double> head_f_z_head_m_s2;
    std::vector<double> head_omega_x_head_rad_s;
    std::vector<double> head_omega_y_head_rad_s;
    std::vector<double> head_omega_z_head_rad_s;
    std::vector<double> head_alpha_x_head_rad_s2;
    std::vector<double> head_alpha_y_head_rad_s2;
    std::vector<double> head_alpha_z_head_rad_s2;
    std::vector<double> world_X_path_m;
    std::vector<double> world_Y_path_m;
    std::vector<double> world_Z_path_m;
    std::vector<double> world_phi_path_rad;
    std::vector<double> world_theta_path_rad;
    std::vector<double> world_psi_path_rad;
    std::vector<double> car_steer_rad;
    std::vector<double> car_throttle_fraction;
    std::vector<double> car_brake_fraction;
    std::vector<double> car_clutch_fraction;
    std::vector<double> car_shift_integer;
    std::vector<double> human_car_u_t_m_s;
    std::vector<double> human_car_u_e_m_s;
    std::vector<double> human_car_lat_e_m;
    std::vector<double> human_car_lat_e_dot_m_s;
    std::vector<double> human_car_psi_t_rad;
    std::vector<double> human_car_psi_e_rad;
    std::vector<double> car_transmission_gear_integer;
    std::vector<double> car_beta_vehicle_rad;
    std::vector<double> car_delta_f_rad;
};
}  //namespace mpmca::messages
