/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <cereal/types/vector.hpp>
#include <vector>

#include "mpmca/cereal_tools.hpp"
#include "mpmca/linear_algebra.hpp"
#include "mpmca/messages/message_id_macro_definition.hpp"
#include "mpmca/types/transformation_matrix.hpp"

namespace mpmca::messages {

struct ControllerFinalTransform
{
    DEFINE_MPMCA_PIPELINE_MESSAGE_ID("ControllerFinalTransform");
    TransformationMatrix final_transform;

    template <class Archive>
    void serialize(Archive& archive)
    {
        archive(final_transform);
    }
};
}  //namespace mpmca::messages