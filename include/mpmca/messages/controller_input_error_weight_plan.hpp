/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <cereal/types/vector.hpp>
#include <vector>

#include "mpmca/cereal_tools.hpp"
#include "mpmca/messages/message_id_macro_definition.hpp"
#include "mpmca/utilities/horizon.hpp"

namespace mpmca::messages {

template <typename TSIM>
struct ControllerInputErrorWeightPlan
{
    DEFINE_MPMCA_PIPELINE_MESSAGE_ID("ControllerInputErrorWeightPlan<", TSIM::GetName(), ">");

    ControllerInputErrorWeightPlan(const utilities::Horizon& horizon)
    {
        w_u_plan.resize(horizon.GetNMpc(), TSIM::InputVector::Zero());  //
    }

    template <class Archive>
    void serialize(Archive& archive)
    {
        archive(w_u_plan);
    }

    std::vector<typename TSIM::InputVector> w_u_plan;
};
}  //namespace mpmca::messages