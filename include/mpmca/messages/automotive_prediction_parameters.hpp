/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <vector>

#include "mpmca/data_names.hpp"
#include "mpmca/messages/message_id_macro_definition.hpp"
#include "mpmca/utilities/configuration.hpp"
#include "mpmca/utilities/map_1d.hpp"
#include "mpmca/utilities/map_2d.hpp"
namespace mpmca::messages {

struct AutomotivePredictionParameters
{
    DEFINE_MPMCA_PIPELINE_MESSAGE_ID("AutomotivePredictionParameters");

    double initial_world_X_vehicle_m = 0;
    double initial_world_Y_vehicle_m = 0;
    double initial_world_Z_vehicle_m = 0;
    double initial_world_psi_vehicle_rad = 0;
    double gravity_m_s2 = 9.81;
    double vehicle_mass_kg = 1000;
    double car_C_distance_rear_axle_m = 1.5;
    double car_S_aero_force_kg_m = 0;
    double car_S_brake_force_N_fraction = 0;
    utilities::Map1D car_M1D_gear_ratio_1_m;
    utilities::Map2D car_M2D_engine_torque_Nm;

    AutomotivePredictionParameters() = default;

    AutomotivePredictionParameters(utilities::ConfigurationPtr config)
        : initial_world_X_vehicle_m(config->GetValue(DN::X_vehicle().GetTotalName(), 0))
        , initial_world_Y_vehicle_m(config->GetValue(DN::Y_vehicle().GetTotalName(), 0))
        , initial_world_Z_vehicle_m(config->GetValue(DN::Z_vehicle().GetTotalName(), 0))
        , initial_world_psi_vehicle_rad(config->GetValue(DN::psi_vehicle().GetTotalName(), 0))
        , gravity_m_s2(config->GetValue(DN::gravity().GetTotalName(), 0))
        , vehicle_mass_kg(config->GetValue(DN::vehicle_mass().GetTotalName(), 0))
        , car_C_distance_rear_axle_m(config->GetValue(DN::car_C_distance_rear_axle().GetTotalName(), 0))
        , car_S_aero_force_kg_m(config->GetValue(DN::car_S_aero_force().GetTotalName(), 0))
        , car_S_brake_force_N_fraction(config->GetValue(DN::car_S_brake_force().GetTotalName(), 0))
        , car_M1D_gear_ratio_1_m(config->GetConfigurationObject(DN::car_M1D_gear_ratio().GetTotalName()))
        , car_M2D_engine_torque_Nm(config->GetConfigurationObject(DN::car_M2D_engine_torque().GetTotalName()))
    {
    }

    template <class Archive>
    void serialize(Archive& archive)
    {
    }
};
}  //namespace mpmca::messages