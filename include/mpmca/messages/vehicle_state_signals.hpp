/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/messages/message_id_macro_definition.hpp"

namespace mpmca::messages {

struct VehicleStateSignals
{
    DEFINE_MPMCA_PIPELINE_MESSAGE_ID("VehicleStateSignals");

    double world_X_vehicle_m = 0;
    double world_Y_vehicle_m = 0;
    double world_Z_vehicle_m = 0;
    double world_phi_vehicle_rad = 0;
    double world_theta_vehicle_rad = 0;
    double world_psi_vehicle_rad = 0;
    double body_u_vehicle_m_s = 0;
    double body_v_vehicle_m_s = 0;
    double body_w_vehicle_m_s = 0;
    double body_p_vehicle_rad_s = 0;
    double body_q_vehicle_rad_s = 0;
    double body_r_vehicle_rad_s = 0;
    double body_a_x_vehicle_m_s2 = 0;
    double body_a_y_vehicle_m_s2 = 0;
    double body_a_z_vehicle_m_s2 = 0;
    double body_p_dot_vehicle_rad_s2 = 0;
    double body_q_dot_vehicle_rad_s2 = 0;
    double body_r_dot_vehicle_rad_s2 = 0;

    template <class Archive>
    void serialize(Archive& archive)
    {
        archive(world_X_vehicle_m, world_Y_vehicle_m, world_Z_vehicle_m, world_phi_vehicle_rad, world_theta_vehicle_rad,
                world_psi_vehicle_rad, body_u_vehicle_m_s, body_v_vehicle_m_s, body_w_vehicle_m_s, body_p_vehicle_rad_s,
                body_q_vehicle_rad_s, body_r_vehicle_rad_s, body_a_x_vehicle_m_s2, body_a_y_vehicle_m_s2,
                body_a_z_vehicle_m_s2, body_p_dot_vehicle_rad_s2, body_q_dot_vehicle_rad_s2, body_r_dot_vehicle_rad_s2);
    }
};
}  //namespace mpmca::messages
