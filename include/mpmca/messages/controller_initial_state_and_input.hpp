/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <cereal/types/vector.hpp>
#include <vector>

#include "mpmca/cereal_tools.hpp"
#include "mpmca/messages/message_id_macro_definition.hpp"

namespace mpmca::messages {

template <typename TSIM>
struct ControllerInitialStateAndInput
{
    DEFINE_MPMCA_PIPELINE_MESSAGE_ID("ControllerInitialStateAndInput<", TSIM::GetName(), ">");
    typename TSIM::StateVector x_initial;
    typename TSIM::InputVector u_initial;

    template <class Archive>
    void serialize(Archive& archive)
    {
        archive(x_initial, u_initial);
    }
};
}  //namespace mpmca::messages