/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/messages/message_id_macro_definition.hpp"

namespace mpmca::messages {

struct InertialHeadSignals
{
    DEFINE_MPMCA_PIPELINE_MESSAGE_ID("InertialHeadSignals");

    double head_f_x_head_m_s2 = 0;
    double head_f_y_head_m_s2 = 0;
    double head_f_z_head_m_s2 = 9.81;
    double head_omega_x_head_rad_s = 0;
    double head_omega_y_head_rad_s = 0;
    double head_omega_z_head_rad_s = 0;
    double head_alpha_x_head_rad_s2 = 0;
    double head_alpha_y_head_rad_s2 = 0;
    double head_alpha_z_head_rad_s2 = 0;

    template <class Archive>
    void serialize(Archive& archive)
    {
        archive(head_f_x_head_m_s2, head_f_y_head_m_s2, head_f_z_head_m_s2, head_omega_x_head_rad_s,
                head_omega_y_head_rad_s, head_omega_z_head_rad_s, head_alpha_x_head_rad_s2, head_alpha_y_head_rad_s2,
                head_alpha_z_head_rad_s2);
    }
};
}  //namespace mpmca::messages
