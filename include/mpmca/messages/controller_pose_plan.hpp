/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <cereal/types/vector.hpp>
#include <vector>

#include "mpmca/cereal_tools.hpp"
#include "mpmca/messages/message_id_macro_definition.hpp"
#include "mpmca/types/pose_rot_mat_vector.hpp"
#include "mpmca/utilities/horizon.hpp"

namespace mpmca::messages {

template <typename TSIM>
struct ControllerPosePlan
{
    DEFINE_MPMCA_PIPELINE_MESSAGE_ID("ControllerPosePlan<", TSIM::GetName(), ">");

    ControllerPosePlan(const utilities::Horizon& horizon)
    {
        pose_plan.resize(horizon.GetNMpc(), PoseRotMatVector::Zero());
    }

    template <class Archive>
    void serialize(Archive& archive)
    {
        archive(pose_plan);
    }

    std::vector<PoseRotMatVector> pose_plan;
};
}  //namespace mpmca::messages