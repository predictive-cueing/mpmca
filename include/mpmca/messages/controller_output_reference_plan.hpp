/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <cereal/types/vector.hpp>
#include <vector>

#include "mpmca/cereal_tools.hpp"
#include "mpmca/linear_algebra.hpp"
#include "mpmca/messages/message_id_macro_definition.hpp"
#include "mpmca/utilities/horizon.hpp"

namespace mpmca::messages {

template <std::size_t NY>
struct ControllerOutputReferencePlan
{
    DEFINE_MPMCA_PIPELINE_MESSAGE_ID("ControllerOutputReferencePlan<", NY, ">");

    ControllerOutputReferencePlan(const utilities::Horizon& horizon, const Vector<NY> y_initial = Vector<NY>::Zero())
    {
        y_ref_plan.resize(horizon.GetNMpc(), y_initial);
    }

    template <class Archive>
    void serialize(Archive& archive)
    {
        archive(y_ref_plan);
    }

    std::vector<Vector<NY>> y_ref_plan;
};
}  //namespace mpmca::messages