/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <vector>

#include "mpmca/linear_algebra.hpp"
#include "mpmca/messages/message_id_macro_definition.hpp"

namespace mpmca::messages {

struct PredictionInertialOutputPlan
{
    DEFINE_MPMCA_PIPELINE_MESSAGE_ID("PredictionInertialOutputPlan");
    std::vector<mpmca::Vector<9>> y_pred_plan;
};
}  //namespace mpmca::messages