/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/messages/message_id_macro_definition.hpp"
#include "mpmca/types/inertial_vector.hpp"
#include "mpmca/types/pose_vector.hpp"

namespace mpmca::messages {

struct ClassicWashoutSignals
{
    DEFINE_MPMCA_PIPELINE_MESSAGE_ID("ClassicWashoutSignals");

    InertialVector head_inertial_cwa = InertialVector::GetNeutral();
    PoseVector simulator_state_reference_cwa = PoseVector::Zero();

    template <class Archive>
    void serialize(Archive&)
    {
    }
};
}  //namespace mpmca::messages
