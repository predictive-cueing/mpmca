/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <type_traits>

namespace mpmca::dokblocks::utils {
// Forward declaration of AbstractBlock
template <int _NX, int _NU, int _NY>
class AbstractBlock;

// General functions for type traits
// https://www.fluentcpp.com/2017/06/02/write-template-metaprogramming-expressively/
template <typename...>
using trying_to_instantiate = void;

using disregard_this = void;

template <typename, template <typename...> typename Expression, typename... Ts>
struct is_detected_impl : std::false_type
{
};

template <template <typename...> typename Expression, typename... Ts>
struct is_detected_impl<trying_to_instantiate<Expression<Ts...>>, Expression, Ts...> : std::true_type
{
};

template <template <typename...> typename Expression, typename... Ts>
using is_detected = is_detected_impl<disregard_this, Expression, Ts...>;

// Detect if a type is derived from AbstractBlock
template <class T>
using is_derived_from_abstract_block_expr = std::is_base_of<AbstractBlock<T::NX, T::NU, T::NY>, T>;
//
template <class T>
using is_derived_from_abstract_block = is_detected<is_derived_from_abstract_block_expr, T>;

}  //namespace mpmca::dokblocks::utils
