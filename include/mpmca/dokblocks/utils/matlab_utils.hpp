/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <string>
#include <vector>

#include "mpmca/dokblocks/core/matrix_types.hpp"

namespace mpmca::dokblocks::utils {
DynamicMatrix ReadBinary(std::string const& fname);
std::vector<std::vector<double>> ReadBinaryStdVector(std::string const& fname);

}  //namespace mpmca::dokblocks::utils