/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/dokblocks/core/matrix_types.hpp"

namespace mpmca::dokblocks::utils {
template <int N>
bool DifferenceIsWithinPrecision(const Vector<N>& matrix_a, const Vector<N>& matrix_b, double tol)
{
    return ((matrix_a - matrix_b).template cwiseAbs().array() < tol).template all();
}
}  //namespace mpmca::dokblocks::utils