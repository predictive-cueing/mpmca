/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <string>
#include <vector>

#include "mpmca/dokblocks/core/abstract_block.hpp"

namespace mpmca::dokblocks {
class Constant : public AbstractBlock<0, 0, 1, Constant>
{
  private:
    using BASE = AbstractBlock<0, 0, 1, Constant>;

  public:
    using typename BASE::InputVector;
    using typename BASE::OutputVector;
    using typename BASE::StateVector;

  private:
    OutputVector m_constant;

  public:
    Constant(const std::string& name, Real k) noexcept;

  protected:
    friend BASE;
    StateVector GetStateImpl() const;
    void SetSimulationStepImpl(Real dt);

    void UpdateOutputImpl(InputVector const& u);
    void UpdateStateImpl(InputVector const& u);
    bool IsDirectFeedThroughImpl() const;
    std::vector<std::string> GetMermaidDiagramStringImpl() const;
    std::string GetMermaidEndConnectorIdImpl() const { return this->GetUniqueId(); }
    std::string GetMermaidStartConnectorIdImpl() const { return this->GetUniqueId(); }
};
}  //namespace mpmca::dokblocks