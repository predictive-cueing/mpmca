/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/dokblocks/core/abstract_block.hpp"

namespace mpmca::dokblocks {
template <int IDX, int SIZE, int _NU>
class SignalSelect : public AbstractBlock<0, _NU, SIZE, SignalSelect<IDX, SIZE, _NU>>
{
    using BASE = AbstractBlock<0, _NU, SIZE, SignalSelect<IDX, SIZE, _NU>>;

  public:
    static constexpr int NY = BASE::NY;
    static constexpr int NU = BASE::NU;
    static constexpr int NX = BASE::NX;
    using typename BASE::InputVector;
    using typename BASE::OutputVector;
    using typename BASE::StateVector;

    explicit SignalSelect(const std::string& name);
    SignalSelect(const std::string& name, const std::array<std::string, _NU>& input_signal_names);

  protected:
    friend BASE;
    StateVector GetStateImpl() const;
    void SetSimulationStepImpl(Real dt);

    void UpdateOutputImpl(InputVector const& u);
    void UpdateStateImpl(InputVector const& u);
    bool IsDirectFeedThroughImpl() const;
    std::vector<std::string> GetMermaidDiagramStringImpl() const;
    std::string GetMermaidEndConnectorIdImpl() const { return this->GetUniqueId(); }
    std::string GetMermaidStartConnectorIdImpl() const { return this->GetUniqueId(); }
};

template <int IDX, int SIZE, int _NU>
SignalSelect<IDX, SIZE, _NU>::SignalSelect(const std::string& name)
    : BASE(name)
{
}

template <int IDX, int SIZE, int _NU>
SignalSelect<IDX, SIZE, _NU>::SignalSelect(const std::string& name,
                                           const std::array<std::string, _NU>& input_signal_names)
    : BASE(name)
{
    this->SetInputSignalNames(input_signal_names);
    std::copy(input_signal_names.begin() + IDX, input_signal_names.begin() + IDX + SIZE,
              this->m_output_signal_names.begin());
}

template <int IDX, int SIZE, int _NU>
typename SignalSelect<IDX, SIZE, _NU>::StateVector SignalSelect<IDX, SIZE, _NU>::GetStateImpl() const
{
    return StateVector::Zero();
}

template <int IDX, int SIZE, int _NU>
void SignalSelect<IDX, SIZE, _NU>::SetSimulationStepImpl(Real dt)
{
}

template <int IDX, int SIZE, int _NU>
void SignalSelect<IDX, SIZE, _NU>::UpdateOutputImpl(InputVector const& u)
{
    this->m_output = u.template segment<SIZE>(IDX);
}

template <int IDX, int SIZE, int _NU>
void SignalSelect<IDX, SIZE, _NU>::UpdateStateImpl(InputVector const& u)
{
}

template <int IDX, int SIZE, int _NU>
bool SignalSelect<IDX, SIZE, _NU>::IsDirectFeedThroughImpl() const
{
    return true;
}

template <int IDX, int SIZE, int _NU>
std::vector<std::string> SignalSelect<IDX, SIZE, _NU>::GetMermaidDiagramStringImpl() const
{
    return {this->GetUniqueId() + "[" + this->GetName() + "]"};
}
}  //namespace mpmca::dokblocks