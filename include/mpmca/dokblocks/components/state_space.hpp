/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <string>

#include "mpmca/dokblocks/core/abstract_block.hpp"
#include "mpmca/dokblocks/core/state_space_discretize.hpp"

namespace mpmca::dokblocks {
template <int _NX, int _NU, int _NY>
class StateSpace : public AbstractBlock<_NX, _NU, _NY, StateSpace<_NX, _NU, _NY>>
{
    using BASE = AbstractBlock<_NX, _NU, _NY, StateSpace<_NX, _NU, _NY>>;

  public:
    using typename BASE::InputVector;
    using typename BASE::OutputVector;
    using typename BASE::StateVector;

    using StateStateMatrix = Matrix<_NX, _NX>;
    using StateInputMatrix = Matrix<_NX, _NU>;
    using OutputStateMatrix = Matrix<_NY, _NX>;
    using OutputInputMatrix = Matrix<_NY, _NU>;

  private:
    StateStateMatrix m_A, m_A_sim;
    StateInputMatrix m_B, m_B_sim;
    OutputStateMatrix m_C, m_C_sim;
    OutputInputMatrix m_D, m_D_sim;
    Real m_dt, m_dt_sim;
    StateVector m_xk;

    void SetSimulationMatrices(Real dt_sim) noexcept;

  public:
    /**
     * Constructors
     */
    StateSpace(const std::string& name) noexcept;
    StateSpace(const std::string& name, StateStateMatrix const& A, StateInputMatrix const& B,
               OutputStateMatrix const& C, OutputInputMatrix const& D) noexcept;
    StateSpace(const std::string& name, StateStateMatrix const& A, StateInputMatrix const& B,
               OutputStateMatrix const& C, OutputInputMatrix const& D, StateVector const& x0) noexcept;
    StateSpace(const std::string& name, StateStateMatrix const& A, StateInputMatrix const& B,
               OutputStateMatrix const& C, OutputInputMatrix const& D, StateVector const& x0, Real dt);
    StateSpace(StateSpace const& other) noexcept;

    /**
     * Getters
     */
    StateStateMatrix const& GetA() const noexcept;
    StateInputMatrix const& GetB() const noexcept;
    OutputStateMatrix const& GetC() const noexcept;
    OutputInputMatrix const& GetD() const noexcept;
    Real GetDt() const noexcept;

    /**
     * Setters
     */
    void SetMatrices(StateStateMatrix const& A, StateInputMatrix const& B, OutputStateMatrix const& C,
                     OutputInputMatrix const& D) noexcept;
    void SetState(StateVector const& xk) noexcept;

    /**
     * Compare
     */
    bool IsApprox(StateSpace const& other) const noexcept;
    bool IsApprox(StateSpace const& other, Real tol) const noexcept;

  protected:
    friend BASE;
    StateVector GetStateImpl() const;
    void SetSimulationStepImpl(Real dt);
    void UpdateOutputImpl(InputVector const& u);
    void UpdateStateImpl(InputVector const& u);

    bool IsDirectFeedThroughImpl() const;
    std::vector<std::string> GetMermaidDiagramStringImpl() const;
    std::string GetMermaidEndConnectorIdImpl() const { return this->GetUniqueId(); }
    std::string GetMermaidStartConnectorIdImpl() const { return this->GetUniqueId(); }
};

template <int NX, int NU, int NY>
void StateSpace<NX, NU, NY>::SetSimulationMatrices(Real dt_sim) noexcept
{
    m_dt_sim = dt_sim;
    if (m_dt == 0 && dt_sim > 0) {
        Discretize<DiscretizationMethod::kRungeKutta4>::c2d(m_A, m_B, m_C, m_D, dt_sim, m_A_sim, m_B_sim, m_C_sim,
                                                            m_D_sim);
    }
    else if (m_dt > 0 && m_dt != dt_sim) {
        Discretize<DiscretizationMethod::kTustin>::d2d(m_A, m_B, m_C, m_D, m_dt, dt_sim, m_A_sim, m_B_sim, m_C_sim,
                                                       m_D_sim);
    }
    else {
        m_A_sim = m_A;
        m_B_sim = m_B;
        m_C_sim = m_C;
        m_D_sim = m_D;
    }
}

template <int NX, int NU, int NY>
StateSpace<NX, NU, NY>::StateSpace(const std::string& name) noexcept
    : BASE(name)
{
}

template <int NX, int NU, int NY>
StateSpace<NX, NU, NY>::StateSpace(const std::string& name, StateStateMatrix const& A, StateInputMatrix const& B,
                                   OutputStateMatrix const& C, OutputInputMatrix const& D) noexcept

    : BASE(name)
    , m_A{A}
    , m_B{B}
    , m_C{C}
    , m_D{D}
    , m_dt{0.}
    , m_xk{StateVector::Zero()}
{
    SetSimulationMatrices(0.);
}

template <int NX, int NU, int NY>
StateSpace<NX, NU, NY>::StateSpace(const std::string& name, StateStateMatrix const& A, StateInputMatrix const& B,
                                   OutputStateMatrix const& C, OutputInputMatrix const& D,
                                   StateVector const& x) noexcept
    : BASE(name)
    , m_A{A}
    , m_B{B}
    , m_C{C}
    , m_D{D}
    , m_dt{0.}
    , m_xk{x}
{
    SetSimulationMatrices(m_dt);
}

template <int NX, int NU, int NY>
StateSpace<NX, NU, NY>::StateSpace(const std::string& name, StateStateMatrix const& A, StateInputMatrix const& B,
                                   OutputStateMatrix const& C, OutputInputMatrix const& D, StateVector const& x,
                                   Real dt)
    : BASE(name)
    , m_A{A}
    , m_B{B}
    , m_C{C}
    , m_D{D}
    , m_dt{dt}
    , m_xk{x}
{
    if (dt < 0)
        throw std::invalid_argument("The sampling time must be larger or equal to zero.");

    SetSimulationMatrices(m_dt);
}

template <int NX, int NU, int NY>
StateSpace<NX, NU, NY>::StateSpace(StateSpace const& other) noexcept
    : BASE(other)
    , m_A{other.m_A}
    , m_A_sim{other.m_A_sim}
    , m_B{other.m_B}
    , m_B_sim{other.m_B_sim}
    , m_C{other.m_C}
    , m_C_sim{other.m_C_sim}
    , m_D{other.m_D}
    , m_D_sim{other.m_D_sim}
    , m_dt{other.m_dt}
    , m_dt_sim{other.m_dt_sim}
    , m_xk{other.m_xk}
{
}

template <int NX, int NU, int NY>
typename StateSpace<NX, NU, NY>::StateStateMatrix const& StateSpace<NX, NU, NY>::GetA() const noexcept
{
    return m_A;
}

template <int NX, int NU, int NY>
typename StateSpace<NX, NU, NY>::StateInputMatrix const& StateSpace<NX, NU, NY>::GetB() const noexcept
{
    return m_B;
}

template <int NX, int NU, int NY>
typename StateSpace<NX, NU, NY>::OutputStateMatrix const& StateSpace<NX, NU, NY>::GetC() const noexcept
{
    return m_C;
}

template <int NX, int NU, int NY>
typename StateSpace<NX, NU, NY>::OutputInputMatrix const& StateSpace<NX, NU, NY>::GetD() const noexcept
{
    return m_D;
}

template <int NX, int NU, int NY>
typename StateSpace<NX, NU, NY>::StateVector StateSpace<NX, NU, NY>::GetStateImpl() const
{
    return m_xk;
}

template <int NX, int NU, int NY>
Real StateSpace<NX, NU, NY>::GetDt() const noexcept
{
    return m_dt;
}

template <int NX, int NU, int NY>
void StateSpace<NX, NU, NY>::SetMatrices(StateStateMatrix const& A, StateInputMatrix const& B,
                                         OutputStateMatrix const& C, OutputInputMatrix const& D) noexcept
{
    m_A = A;
    m_B = m_B;
    m_C = C;
    m_D = D;

    SetSimulationMatrices(m_dt_sim);
}

template <int NX, int NU, int NY>
void StateSpace<NX, NU, NY>::SetState(StateVector const& xk) noexcept
{
    m_xk = xk;
}

template <int NX, int NU, int NY>
bool StateSpace<NX, NU, NY>::IsApprox(StateSpace const& other) const noexcept
{
    return m_A.isApprox(other.m_A) && m_B.isApprox(other.m_B) && m_C.isApprox(other.m_C) && m_D.isApprox(other.m_D) &&
           m_xk.isApprox(other.m_xk) && (m_dt == other.m_dt);
}

template <int NX, int NU, int NY>
bool StateSpace<NX, NU, NY>::IsApprox(StateSpace const& other, Real tol) const noexcept
{
    return m_A.isApprox(other.m_A, tol) && m_B.isApprox(other.m_B, tol) && m_C.isApprox(other.m_C, tol) &&
           m_D.isApprox(other.m_D, tol) && m_xk.isApprox(other.m_xk, tol) && (m_dt == other.m_dt);
}

/**
 * Overrides
 */

template <int NX, int NU, int NY>
void StateSpace<NX, NU, NY>::SetSimulationStepImpl(Real dt)
{
    SetSimulationMatrices(dt);
}

template <int NX, int NU, int NY>
void StateSpace<NX, NU, NY>::UpdateOutputImpl(InputVector const& u)
{
    this->m_output = m_C_sim * m_xk + m_D_sim * u;
}

template <int NX, int NU, int NY>
void StateSpace<NX, NU, NY>::UpdateStateImpl(InputVector const& u)
{
    m_xk = m_A_sim * m_xk + m_B_sim * u;
}

template <int NX, int NU, int NY>
bool StateSpace<NX, NU, NY>::IsDirectFeedThroughImpl() const
{
    return !m_D_sim.isApprox(GetZero(m_D_sim), 1e-6);
}

template <int NX, int NU, int NY>
std::vector<std::string> StateSpace<NX, NU, NY>::GetMermaidDiagramStringImpl() const
{
    return {this->GetUniqueId() + "[" + this->GetName() + "]"};
}

}  //namespace mpmca::dokblocks