/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/dokblocks/core/abstract_block.hpp"

namespace mpmca::dokblocks {
/*
 * Declaration
 */
template <int N, int _NU>
class SignalRepeat : public AbstractBlock<0, _NU, _NU * N, SignalRepeat<N, _NU>>
{
  private:
    using BASE = AbstractBlock<0, _NU, _NU * N, SignalRepeat<N, _NU>>;

  public:
    static constexpr int NY = BASE::NY;
    static constexpr int NU = BASE::NU;
    static constexpr int NX = BASE::NX;

    using typename BASE::InputVector;
    using typename BASE::OutputVector;
    using typename BASE::StateVector;

    explicit SignalRepeat(const std::string& name);
    SignalRepeat(const std::string& name, const std::array<std::string, _NU>& input_signal_names);

  protected:
    friend BASE;
    StateVector GetStateImpl() const;
    void SetSimulationStepImpl(Real dt);

    void UpdateOutputImpl(InputVector const& u);
    void UpdateStateImpl(InputVector const& u);
    bool IsDirectFeedThroughImpl() const;
    std::vector<std::string> GetMermaidDiagramStringImpl() const;
    std::string GetMermaidEndConnectorIdImpl() const { return this->GetUniqueId(); }
    std::string GetMermaidStartConnectorIdImpl() const { return this->GetUniqueId(); }
};

template <int N, int _NU>
SignalRepeat<N, _NU>::SignalRepeat(const std::string& name)
    : BASE(name)
{
}

template <int N, int _NU>
SignalRepeat<N, _NU>::SignalRepeat(const std::string& name, const std::array<std::string, _NU>& input_signal_names)
    : BASE(name)
{
    this->SetInputSignalNames(input_signal_names);

    for (int i = 0; i < N; ++i) {
        std::copy(input_signal_names.begin(), input_signal_names.end(), this->m_output_signal_names.begin() + i * _NU);
    }
}

template <int N, int _NU>
typename SignalRepeat<N, _NU>::StateVector SignalRepeat<N, _NU>::GetStateImpl() const
{
    return StateVector::Zero();
}

template <int N, int _NU>
void SignalRepeat<N, _NU>::SetSimulationStepImpl(Real dt)
{
}

template <int N, int _NU>
void SignalRepeat<N, _NU>::UpdateOutputImpl(InputVector const& u)
{
    this->m_output = u.template replicate<N, 1>();
}

template <int N, int _NU>
void SignalRepeat<N, _NU>::UpdateStateImpl(InputVector const& u)
{
}

template <int N, int _NU>
bool SignalRepeat<N, _NU>::IsDirectFeedThroughImpl() const
{
    return true;
}

template <int N, int _NU>
std::vector<std::string> SignalRepeat<N, _NU>::GetMermaidDiagramStringImpl() const
{
    return {this->GetUniqueId() + "[" + this->GetName() + "]"};
}
}  //namespace mpmca::dokblocks