/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/dokblocks/components/state_space.hpp"
#include "mpmca/dokblocks/core/matrix_types.hpp"

namespace mpmca::dokblocks {
/*
 * Declaration
 */
template <int N>
class GainCoefficientWise : public AbstractBlock<0, N, N, GainCoefficientWise<N>>
{
    using BASE = AbstractBlock<0, N, N, GainCoefficientWise<N>>;

  public:
    using typename BASE::InputVector;
    using typename BASE::OutputVector;
    using typename BASE::StateVector;

  private:
    InputVector m_gain;

  public:
    GainCoefficientWise(const std::string& name, Real k) noexcept;
    GainCoefficientWise(const std::string& name, InputVector const& K) noexcept;

  protected:
    friend BASE;
    StateVector GetStateImpl() const;
    void SetSimulationStepImpl(Real dt);

    void UpdateOutputImpl(InputVector const& u);
    void UpdateStateImpl(InputVector const& u);
    bool IsDirectFeedThroughImpl() const;
    std::vector<std::string> GetMermaidDiagramStringImpl() const;
    std::string GetMermaidEndConnectorIdImpl() const { return this->GetUniqueId(); }
    std::string GetMermaidStartConnectorIdImpl() const { return this->GetUniqueId(); }
};

template <int N>
GainCoefficientWise<N>::GainCoefficientWise(const std::string& name, Real k) noexcept
    : BASE(name)
    , m_gain{InputVector::Constant(k)}
{
}

template <int N>
GainCoefficientWise<N>::GainCoefficientWise(const std::string& name, InputVector const& K) noexcept
    : BASE(name)
    , m_gain(K)
{
}

template <int N>
typename GainCoefficientWise<N>::StateVector GainCoefficientWise<N>::GetStateImpl() const
{
    return StateVector::Zero();
}

template <int N>
void GainCoefficientWise<N>::SetSimulationStepImpl(Real dt)
{
}

template <int N>
void GainCoefficientWise<N>::UpdateOutputImpl(InputVector const& u)
{
    this->m_output = m_gain.cwiseProduct(u);
}

template <int N>
void GainCoefficientWise<N>::UpdateStateImpl(InputVector const& u)
{
}

template <int N>
bool GainCoefficientWise<N>::IsDirectFeedThroughImpl() const
{
    return true;
}

template <int N>
std::vector<std::string> GainCoefficientWise<N>::GetMermaidDiagramStringImpl() const
{
    return {this->GetUniqueId() + "[" + this->GetName() + "]"};
}

}  //namespace mpmca::dokblocks