/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/dokblocks/components/state_space.hpp"

namespace mpmca::dokblocks {
class DoubleDifferentiator : public StateSpace<2, 1, 1>
{
  public:
    DoubleDifferentiator(const std::string& name, Real omega);
};

}  //namespace mpmca::dokblocks