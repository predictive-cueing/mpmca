/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <memory>

#include "mpmca/dokblocks/core/abstract_block.hpp"
#include "mpmca/dokblocks/core/matrix_types.hpp"

namespace mpmca::dokblocks {

template <int _NU, int _NY>
class AlgebraicFunction : public AbstractBlock<0, _NU, _NY, AlgebraicFunction<_NU, _NY>>
{
  private:
    using BASE = AbstractBlock<0, _NU, _NY, AlgebraicFunction<_NU, _NY>>;

  public:
    using typename BASE::InputVector;
    using typename BASE::OutputVector;
    using typename BASE::StateVector;

    using Fun = std::function<OutputVector(const InputVector&)>;

  private:
    Fun m_output_function;

  public:
    AlgebraicFunction(const std::string& name, Fun const& output_function) noexcept;
    void SetOutputFunction(Fun const& output_function) noexcept;

  private:
    friend BASE;
    StateVector GetStateImpl() const;
    void SetSimulationStepImpl(Real dt);
    void UpdateOutputImpl(InputVector const& u);
    void UpdateStateImpl(InputVector const& u);
    bool IsDirectFeedThroughImpl() const;
    std::vector<std::string> GetMermaidDiagramStringImpl() const;
    std::string GetMermaidEndConnectorIdImpl() const { return this->GetUniqueId(); }
    std::string GetMermaidStartConnectorIdImpl() const { return this->GetUniqueId(); }
};

template <int _NU, int _NY>
AlgebraicFunction<_NU, _NY>::AlgebraicFunction(const std::string& name, const Fun& output_function) noexcept
    : BASE(name)
{
    SetOutputFunction(output_function);
}

template <int _NU, int _NY>
void AlgebraicFunction<_NU, _NY>::SetOutputFunction(Fun const& output_function) noexcept
{
    m_output_function = output_function;
}

template <int _NU, int _NY>
typename AlgebraicFunction<_NU, _NY>::StateVector AlgebraicFunction<_NU, _NY>::GetStateImpl() const
{
    return StateVector::Zero();
}

template <int _NU, int _NY>
void AlgebraicFunction<_NU, _NY>::SetSimulationStepImpl(Real dt)
{
}

template <int NU, int NY>
void AlgebraicFunction<NU, NY>::UpdateOutputImpl(InputVector const& u)
{
    this->m_output = m_output_function(u);
}

template <int NU, int NY>
void AlgebraicFunction<NU, NY>::UpdateStateImpl(InputVector const& u)
{
}

template <int NU, int NY>
bool AlgebraicFunction<NU, NY>::IsDirectFeedThroughImpl() const
{
    return true;
}

template <int NU, int NY>
std::vector<std::string> AlgebraicFunction<NU, NY>::GetMermaidDiagramStringImpl() const
{
    return {this->GetUniqueId() + "[" + this->GetName() + "]"};
}

}  //namespace mpmca::dokblocks