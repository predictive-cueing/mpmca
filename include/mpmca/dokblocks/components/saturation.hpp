/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/dokblocks/components/algebraic_function.hpp"

namespace mpmca::dokblocks {
class Saturation : public AlgebraicFunction<1, 1>
{
  public:
    Saturation(const std::string& name, Real lower_limit, Real upper_limit);
};

}  //namespace mpmca::dokblocks