/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <memory>

#include "mpmca/dokblocks/components/differentiator.hpp"
#include "mpmca/dokblocks/components/integrator.hpp"
#include "mpmca/dokblocks/components/saturation.hpp"
#include "mpmca/dokblocks/core/block_diagram.hpp"

namespace mpmca::dokblocks {
inline auto RateLimiter(const std::string& name, Real lower_limit, Real upper_limit)
{
    auto differentiator = Differentiator(name + std::string("_diff"), 100.);
    auto saturation = Saturation(name + std::string("_saturation"), lower_limit, upper_limit);
    auto integrator = Integrator(name + std::string("_int"));
    auto rate_limiter = MakeSeries(name, differentiator, saturation, integrator);
    return rate_limiter;
}

}  //namespace mpmca::dokblocks