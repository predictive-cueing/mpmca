/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <string>

namespace mpmca::dokblocks {

template <class M>
class ModelOutput
{
  private:
    std::string m_name;
    size_t m_index;

  public:
    ModelOutput(const M& model, const std::string& name)
        : m_name(name)
    {
        auto output_signal_names = model.GetOutputSignalNames();

        m_index =
            std::find(output_signal_names.cbegin(), output_signal_names.cend(), name) - output_signal_names.cbegin();
    }
    double GetValue(const M& model) const { return model.GetOutput()[m_index]; }
};

}  //namespace mpmca::dokblocks