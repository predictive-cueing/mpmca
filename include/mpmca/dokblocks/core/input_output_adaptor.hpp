/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <exception>
#include <string>
#include <unordered_map>

#include "mpmca/dokblocks/core/abstract_block.hpp"
#include "mpmca/dokblocks/core/model_input.hpp"
#include "mpmca/dokblocks/core/model_output.hpp"

namespace mpmca::dokblocks {

template <class M>
class InputOutputAdaptor : public AbstractBlock<0, 0, 0, InputOutputAdaptor<M>>
{
  private:
    using BASE = AbstractBlock<0, 0, 0, InputOutputAdaptor<M>>;

    M m_model;
    std::unordered_map<std::string, ModelInput<M>> m_input_map;
    std::unordered_map<std::string, ModelOutput<M>> m_output_map;

    typename M::InputVector m_input_vector;

  public:
    const std::unordered_map<std::string, ModelInput<M>>& GetInputMap() const { return m_input_map; }
    const std::unordered_map<std::string, ModelOutput<M>>& GetOutputMap() const { return m_output_map; }

    InputOutputAdaptor(const std::string& name, const M& model)
        : BASE(name)
        , m_model(model)
    {
        auto unique_inputs = m_model.GetUniqueInputSignalNames();

        for (auto& unique_input : unique_inputs) {
            m_input_map.emplace(unique_input, ModelInput(m_model, unique_input));
        }

        auto output_names = m_model.GetOutputSignalNames();
        for (auto& output_name : output_names) {
            m_output_map.emplace(output_name, ModelOutput(m_model, output_name));
        }
    }
    void SetSimulationStepImpl(Real dt) { m_model.SetSimulationStep(dt); }
    void Simulate() { m_model.Simulate(m_input_vector); }
    void SetInput(const std::string& input_name, double value)
    {
        m_input_map.at(input_name).SetValue(m_input_vector, value);
    }
    double GetOutput(const std::string& output_name) const
    {
        try {
            return m_output_map.at(output_name).GetValue(m_model);
        }
        catch (const std::out_of_range& exception) {
            std::string message = "Signal \"" + output_name + "\" is not an output signal of model " +
                                  m_model.GetName() + ". Available signals are: ";
            for (auto& [key, value] : m_output_map) {
                message += key + ", ";
            }
            throw std::runtime_error(message);
        }
    }
    std::vector<std::string> GetMermaidDiagramStringImpl() const
    {
        std::vector<std::string> result = m_model.GetMermaidDiagramString();

        result.push_back(this->GetMermaidStartConnectorId() + "([" +
                         BASE::ConvertStringContainerToMultilineMermaidString(m_model.GetUniqueInputSignalNames()) +
                         "])");
        result.push_back(this->GetMermaidEndConnectorId() + "([" +
                         BASE::ConvertStringContainerToMultilineMermaidString(m_model.GetOutputSignalNames()) + "])");

        result.push_back(this->GetMermaidStartConnectorId() + " --> " + m_model.GetMermaidStartConnectorId());
        result.push_back(m_model.GetMermaidEndConnectorId() + " --> " + this->GetMermaidEndConnectorId());
        return result;
    }
    std::string GetMermaidStartConnectorIdImpl() const { return this->GetUniqueId() + "_input"; }
    std::string GetMermaidEndConnectorIdImpl() const { return this->GetUniqueId() + "_output"; }
    const M& GetModel() const { return m_model; }
};
}  //namespace mpmca::dokblocks