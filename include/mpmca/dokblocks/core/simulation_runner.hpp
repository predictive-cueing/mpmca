/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <string>
#include <unordered_map>
#include <vector>

#include "mpmca/dokblocks/core/input_output_adaptor.hpp"
#include "mpmca/dokblocks/core/time_series.hpp"
#include "mpmca/dokblocks/utils/block_traits.hpp"

namespace mpmca::dokblocks {

template <class M>
class SimulationRunner
{
    static_assert(utils::is_derived_from_abstract_block<M>::value,
                  "The template parameter of SimulationRunner must "
                  "derive from AbstractBlock.");

    double m_dt;
    InputOutputAdaptor<M> m_input_output_adaptor;
    std::unordered_map<std::string, TimeSeries> m_input_signals;
    std::unordered_map<std::string, TimeSeries> m_output_signals;
    std::vector<std::string> m_connected_outputs_to_inputs;

    void DetermineConnectOutputsToInputs();

  public:
    SimulationRunner(const std::string& name, const M& model, double dt, bool connect_outputs_to_inputs);
    void RunSimulation(size_t N);
    const TimeSeries& GetOutput(const std::string& name) const;
    const TimeSeries& GetInput(const std::string& name) const;
    TimeSeries& GetInput(const std::string& name);
    bool IsSignalConnected(const std::string& name) const;
    void SetInitialInput(const std::string& name, double value);
    const M& GetModel() const;
};

template <class M>
SimulationRunner<M>::SimulationRunner(const std::string& name, const M& model, double dt,
                                      bool connect_outputs_to_inputs)
    : m_dt(dt)
    , m_input_output_adaptor{name, model}
{
    m_input_output_adaptor.SetSimulationStep(dt);

    const auto& adaptor_input_map = m_input_output_adaptor.GetInputMap();

    for (const auto& [key, value] : adaptor_input_map) {
        m_input_signals.emplace(key, TimeSeries(key));
    }

    const auto& adaptor_output_map = m_input_output_adaptor.GetOutputMap();
    for (const auto& [key, value] : adaptor_output_map) {
        m_output_signals.emplace(key, TimeSeries(key));
    }

    if (connect_outputs_to_inputs) {
        DetermineConnectOutputsToInputs();
    }
}

template <class M>
const M& SimulationRunner<M>::GetModel() const
{
    return m_input_output_adaptor.GetModel();
}

template <class M>
bool SimulationRunner<M>::IsSignalConnected(const std::string& name) const
{
    return std::find(m_connected_outputs_to_inputs.cbegin(), m_connected_outputs_to_inputs.cend(), name) !=
           m_connected_outputs_to_inputs.cend();
}

template <class M>
void SimulationRunner<M>::DetermineConnectOutputsToInputs()
{
    for (const auto& [key, value] : m_input_signals) {
        if (m_output_signals.find(key) != m_output_signals.end()) {
            m_connected_outputs_to_inputs.push_back(key);
        }
    }
}

template <class M>
const TimeSeries& SimulationRunner<M>::GetOutput(const std::string& name) const
{
    return m_output_signals.at(name);
}

template <class M>
const TimeSeries& SimulationRunner<M>::GetInput(const std::string& name) const
{
    return m_input_signals.at(name);
}

template <class M>
void SimulationRunner<M>::SetInitialInput(const std::string& name, double value)
{
    GetInput(name).SetSignalAtIndex(0, 0, value);
}

template <class M>
TimeSeries& SimulationRunner<M>::GetInput(const std::string& name)
{
    return m_input_signals.at(name);
}

template <class M>
void SimulationRunner<M>::RunSimulation(size_t N)
{
    for (size_t i = 0; i < N; ++i) {
        double time = i * m_dt;

        if (i > 0) {
            for (const std::string& connected_output_to_input : m_connected_outputs_to_inputs) {
                m_input_signals.at(connected_output_to_input)
                    .SetSignalAtIndex(i, time, m_input_output_adaptor.GetOutput(connected_output_to_input));
            }
        }

        for (const auto& [key, value] : m_input_output_adaptor.GetInputMap()) {
            m_input_output_adaptor.SetInput(key, m_input_signals.at(key).GetSignalAtIndex(i));
        }

        m_input_output_adaptor.Simulate();

        for (const auto& [key, value] : m_input_output_adaptor.GetOutputMap()) {
            m_output_signals.at(key).SetSignalAtIndex(i, time, m_input_output_adaptor.GetOutput(key));
        }
    }
}
}  //namespace mpmca::dokblocks