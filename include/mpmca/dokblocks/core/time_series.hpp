/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <string>
#include <vector>

namespace mpmca::dokblocks {
class TimeSeries
{
  private:
    std::string m_name;
    std::vector<double> m_time_vector;
    std::vector<double> m_data_vector;

  public:
    TimeSeries(const std::string& name);
    double GetTimeAtIndex(size_t index) const;
    double GetSignalAtIndex(size_t index) const;
    void SetSignalAtIndex(size_t index, double time, double value);
    size_t GetSignalLength() const;

    const std::vector<double>& GetTimeVector() const;
    const std::vector<double>& GetDataVector() const;
};
}  //namespace mpmca::dokblocks