/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <eigen3/Eigen/Dense>

namespace mpmca::dokblocks {
using Real = double;

using Eigen::ColMajor;
using Eigen::RowMajor;

template <int M, int Options = ColMajor>
using Vector = Eigen::Matrix<Real, M, 1, Options>;

template <int M, int N>
using Matrix = Eigen::Matrix<Real, M, N>;

template <int M>
using DiagonalMatrix = Eigen::DiagonalMatrix<Real, M>;

using DynamicVector = Eigen::Matrix<Real, Eigen::Dynamic, 1>;

using DynamicMatrix = Eigen::Matrix<Real, Eigen::Dynamic, Eigen::Dynamic>;

template <int N>
inline auto GetIdentity(Matrix<N, N> const& A)
{
    return Matrix<N, N>::Identity();
}

template <>
inline auto GetIdentity(Matrix<Eigen::Dynamic, Eigen::Dynamic> const& A)
{
    return DynamicMatrix::Identity(A.rows(), A.cols());
}

// TODO: What happens if only one of NR, NC is Eigen::Dynamic
template <int NR, int NC>
inline auto GetZero(Matrix<NR, NC> const& A)
{
    return Matrix<NR, NC>::Zero();
}

template <>
inline auto GetZero(Matrix<Eigen::Dynamic, Eigen::Dynamic> const& A)
{
    return DynamicMatrix::Zero(A.rows(), A.cols());
}
}  //namespace mpmca::dokblocks