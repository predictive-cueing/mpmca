/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/dokblocks/core/abstract_block.hpp"

namespace mpmca::dokblocks {

template <class TOP, class BOTTOM>
class Parallel : public AbstractBlock<TOP::NX + BOTTOM::NX, TOP::NU, TOP::NY, Parallel<TOP, BOTTOM>>
{
    static_assert(TOP::NU == BOTTOM::NU);
    static_assert(TOP::NY == BOTTOM::NY);

    TOP m_top;
    BOTTOM m_bottom;
    using BASE = AbstractBlock<TOP::NX + BOTTOM::NX, TOP::NU, TOP::NY, Parallel<TOP, BOTTOM>>;

  public:
    static constexpr int NY = BASE::NY;
    static constexpr int NU = BASE::NU;
    static constexpr int NX = BASE::NX;
    using typename BASE::InputVector;
    using typename BASE::OutputVector;
    using typename BASE::StateVector;

    Parallel(const std::string& name, TOP const& top, BOTTOM const& bottom);
    Parallel(Parallel const& other) = default;
    Parallel(Parallel&& other) = default;
    Parallel& operator=(Parallel const& other) = default;
    Parallel& operator=(Parallel&& other) = default;
    ~Parallel() = default;

  protected:
    friend BASE;
    StateVector GetStateImpl() const;
    void SetSimulationStepImpl(Real dt);

    void UpdateOutputImpl(InputVector const& u);
    void UpdateStateImpl(InputVector const& u);
    bool IsDirectFeedThroughImpl() const;
    std::vector<std::string> GetMermaidDiagramStringImpl() const
    {
        std::vector<std::string> result;
        auto top_vector = m_top.GetMermaidDiagramString();
        auto bottom_vector = m_bottom.GetMermaidDiagramString();

        std::copy(top_vector.begin(), top_vector.end(), std::back_inserter(result));
        std::copy(bottom_vector.begin(), bottom_vector.end(), std::back_inserter(result));

        result.push_back(this->GetUniqueId() + "((+))");
        result.push_back(m_top.GetMermaidEndConnectorId() + " --> " + this->GetMermaidStartConnectorId());
        result.push_back(m_bottom.GetMermaidEndConnectorId() + " --> " + this->GetMermaidStartConnectorId());

        return result;
    }
    std::string GetMermaidEndConnectorIdImpl() const { return this->GetUniqueId(); }
    std::string GetMermaidStartConnectorIdImpl() const { return this->GetUniqueId(); }
};

template <class TOP, class BOTTOM>
Parallel<TOP, BOTTOM>::Parallel(const std::string& name, TOP const& top, BOTTOM const& bottom)
    : BASE(name)
    , m_top{top}
    , m_bottom{bottom}
{
}

template <class TOP, class BOTTOM>
typename Parallel<TOP, BOTTOM>::StateVector Parallel<TOP, BOTTOM>::GetStateImpl() const
{
    return (StateVector() << m_top.GetState(), m_bottom.GetState()).finished();
}

template <class TOP, class BOTTOM>
void Parallel<TOP, BOTTOM>::SetSimulationStepImpl(Real dt)
{
    m_top.SetSimulationStep(dt);
    m_bottom.SetSimulationStep(dt);
}

template <class TOP, class BOTTOM>
void Parallel<TOP, BOTTOM>::UpdateOutputImpl(InputVector const& u)
{
    m_top.UpdateOutput(u);
    m_bottom.UpdateOutput(u);

    this->m_output = m_top.GetOutput() + m_bottom.GetOutput();
}

template <class TOP, class BOTTOM>
void Parallel<TOP, BOTTOM>::UpdateStateImpl(InputVector const& u)
{
    m_top.UpdateState(u);
    m_bottom.UpdateState(u);
}

template <class TOP, class BOTTOM>
bool Parallel<TOP, BOTTOM>::IsDirectFeedThroughImpl() const
{
    return m_top.IsDirectFeedThrough() || m_bottom.IsDirectFeedThrough();
}

template <class TOP, class BOTTOM, class... OTHERS>
auto MakeParallel(const std::string& name, TOP const& top, BOTTOM const& bottom, OTHERS const&... others)
{
    auto parallel = Parallel(name, top, bottom);
    return MakeParallel(name, parallel, others...);
}

template <class TOP, class BOTTOM>
auto MakeParallel(const std::string& name, TOP const& top, BOTTOM const& bottom)
{
    auto parallel = Parallel(name, top, bottom);
    return parallel;
}
}  //namespace mpmca::dokblocks