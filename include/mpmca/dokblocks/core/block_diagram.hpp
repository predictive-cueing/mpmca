/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/dokblocks/core/append.hpp"
#include "mpmca/dokblocks/core/feedback.hpp"
#include "mpmca/dokblocks/core/parallel.hpp"
#include "mpmca/dokblocks/core/series.hpp"
#include "mpmca/dokblocks/core/sum.hpp"