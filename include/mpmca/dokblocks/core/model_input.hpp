/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <string>
#include <vector>

namespace mpmca::dokblocks {

template <class M>
class ModelInput
{
  private:
    std::string m_name;
    std::vector<size_t> m_indices;

  public:
    ModelInput(const M& model, const std::string& name)
        : m_name(name)
    {
        auto input_signal_names = model.GetInputSignalNames();

        for (size_t i = 0; i < input_signal_names.size(); ++i) {
            if (input_signal_names[i].compare(name) == 0) {
                m_indices.push_back(i);
            }
        }
    }

    const std::vector<size_t> GetIndices() const { return m_indices; }

    void SetValue(typename M::InputVector& input_vector, double value)
    {
        for (size_t index : m_indices) {
            input_vector[index] = value;
        }
    }
};
}  //namespace mpmca::dokblocks