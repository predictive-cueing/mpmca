/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/dokblocks/core/abstract_block.hpp"
#include "mpmca/dokblocks/core/mermaid_tools.hpp"

namespace mpmca::dokblocks {

template <class LEFT, class RIGHT>
class Series : public AbstractBlock<LEFT::NX + RIGHT::NX, LEFT::NU, RIGHT::NY, Series<LEFT, RIGHT>>
{
    static_assert(LEFT::NY == RIGHT::NU);

    LEFT m_left;
    RIGHT m_right;
    using BASE = AbstractBlock<LEFT::NX + RIGHT::NX, LEFT::NU, RIGHT::NY, Series<LEFT, RIGHT>>;

  public:
    static constexpr int NY = BASE::NY;
    static constexpr int NU = BASE::NU;
    static constexpr int NX = BASE::NX;
    using typename BASE::InputVector;
    using typename BASE::OutputVector;
    using typename BASE::StateVector;

    Series(const std::string& name, LEFT const& left, RIGHT const& right);
    Series(Series const& other) = default;
    Series(Series&& other) = default;
    Series& operator=(Series const& other) = default;
    Series& operator=(Series&& other) = default;
    ~Series() = default;

  protected:
    friend BASE;
    StateVector GetStateImpl() const;
    void SetSimulationStepImpl(Real dt);

    void UpdateOutputImpl(InputVector const& u);
    void UpdateStateImpl(InputVector const& u);
    bool IsDirectFeedThroughImpl() const;
    std::vector<std::string> GetMermaidDiagramStringImpl() const
    {
        std::vector<std::string> result;
        auto left_vector = m_left.GetMermaidDiagramString();
        auto right_vector = m_right.GetMermaidDiagramString();

        std::copy(left_vector.begin(), left_vector.end(), std::back_inserter(result));
        std::copy(right_vector.begin(), right_vector.end(), std::back_inserter(result));

        result.push_back(
            m_left.GetMermaidEndConnectorId() + " -- " +
            MermaidTools::MatchedInputOutputSignalNames(m_left.GetOutputSignalNames(), m_right.GetInputSignalNames()) +
            " --> " + m_right.GetMermaidStartConnectorId());
        return result;
    }
    std::string GetMermaidStartConnectorIdImpl() const { return m_left.GetMermaidStartConnectorId(); }
    std::string GetMermaidEndConnectorIdImpl() const { return m_right.GetMermaidEndConnectorId(); }
};

template <class LEFT, class RIGHT>
Series<LEFT, RIGHT>::Series(const std::string& name, LEFT const& left, RIGHT const& right)
    : BASE(name)
    , m_left{left}
    , m_right{right}
{
    this->SetInputSignalNames(m_left.GetInputSignalNames());
    this->SetOutputSignalNames(m_right.GetOutputSignalNames());
}

template <class LEFT, class RIGHT>
typename Series<LEFT, RIGHT>::StateVector Series<LEFT, RIGHT>::GetStateImpl() const
{
    return (StateVector() << m_right.GetState(), m_left.GetState()).finished();
}

template <class LEFT, class RIGHT>
void Series<LEFT, RIGHT>::SetSimulationStepImpl(Real dt)
{
    m_left.SetSimulationStep(dt);
    m_right.SetSimulationStep(dt);
}

template <class LEFT, class RIGHT>
void Series<LEFT, RIGHT>::UpdateOutputImpl(InputVector const& u)
{
    m_left.UpdateOutput(u);
    m_right.UpdateOutput(m_left.GetOutput());
    this->m_output = m_right.GetOutput();
}

template <class LEFT, class RIGHT>
void Series<LEFT, RIGHT>::UpdateStateImpl(InputVector const& u)
{
    m_left.UpdateState(u);
    m_right.UpdateState(m_left.GetOutput());
}

template <class LEFT, class RIGHT>
bool Series<LEFT, RIGHT>::IsDirectFeedThroughImpl() const
{
    return m_left.IsDirectFeedThrough() && m_right.IsDirectFeedThrough();
}

template <class LEFT, class RIGHT, class... OTHERS>
auto MakeSeries(const std::string& name, LEFT const& left, RIGHT const& right, OTHERS const&... others)
{
    auto series_block = Series(name, left, right);
    return MakeSeries(name, series_block, others...);
}

template <class LEFT, class RIGHT>
auto MakeSeries(const std::string& name, LEFT const& left, RIGHT const& right)
{
    auto series_block = Series(name, left, right);
    return series_block;
}

}  //namespace mpmca::dokblocks