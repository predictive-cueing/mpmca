/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <array>
#include <string>
#include <vector>

#include "mpmca/dokblocks/core/matrix_types.hpp"
#include "mpmca/dokblocks/utils/block_traits.hpp"

namespace mpmca::dokblocks {

class UniqueIdGenerator
{
  public:
    static std::string GenerateUniqueId()
    {
        static int id = 0;
        return std::string("block_") + std::to_string(id++);
    }
};

template <int _NX, int _NU, int _NY, class T>
class AbstractBlock
{
    static_assert(_NX != Eigen::Dynamic, "Dynamically sized vectors (Eigen) are not supported.");
    static_assert(_NU != Eigen::Dynamic, "Dynamically sized vectors (Eigen) are not supported.");
    static_assert(_NY != Eigen::Dynamic, "Dynamically sized vectors (Eigen) are not supported.");

  public:
    static constexpr std::size_t NX = _NX;
    static constexpr std::size_t NU = _NU;
    static constexpr std::size_t NY = _NY;

    using StateVector = Vector<_NX>;
    using InputVector = Vector<_NU>;
    using OutputVector = Vector<_NY>;

  private:
    std::string m_name;
    std::string m_unique_id;

  protected:
    OutputVector m_output;

    std::array<std::string, _NU> m_input_signal_names;
    std::array<std::string, _NY> m_output_signal_names;

  public:
    AbstractBlock(const std::string& name)
        : m_name(name)
        , m_unique_id(UniqueIdGenerator::GenerateUniqueId())
    {
    }

    AbstractBlock(const AbstractBlock<_NX, _NU, _NY, T>& other)
        : m_name(other.m_name)
        , m_unique_id(UniqueIdGenerator::GenerateUniqueId())
        , m_output(OutputVector::Zero())
        , m_input_signal_names(other.m_input_signal_names)
        , m_output_signal_names(other.m_output_signal_names)
    {
    }

    const std::string& GetName() const { return m_name; }
    const std::string& GetUniqueId() const { return m_unique_id; }

    void SetInputSignalNames(const std::array<std::string, _NU>& input_signal_names)
    {
        m_input_signal_names = input_signal_names;
    }

    void SetOutputSignalNames(const std::array<std::string, _NY>& output_signal_names)
    {
        m_output_signal_names = output_signal_names;
    }

    const std::array<std::string, _NU>& GetInputSignalNames() const { return m_input_signal_names; }
    const std::array<std::string, _NY>& GetOutputSignalNames() const { return m_output_signal_names; }

    const std::vector<std::string> GetUniqueInputSignalNames() const
    {
        auto input_signal_names_cpy = m_input_signal_names;
        std::sort(input_signal_names_cpy.begin(), input_signal_names_cpy.end());

        std::vector<std::string> result;
        std::unique_copy(input_signal_names_cpy.begin(), input_signal_names_cpy.end(), std::back_inserter(result));
        return result;
    }
    template <class C>
    static std::string ConvertStringContainerToMultilineMermaidString(const C& string_array)
    {
        std::string result = "\"`";
        for (int i = 0; i < string_array.size(); ++i) {
            result += string_array[i];

            if (i < string_array.size() - 1) {
                result += "\n";
            }
        }
        result += " `\"";
        return result;
    }
    std::string GetInputSignalNamesString() const
    {
        return ConvertStringContainerToMultilineMermaidString(m_input_signal_names);
    }
    std::string GetOutputSignalNamesString() const
    {
        return ConvertStringContainerToMultilineMermaidString(m_output_signal_names);
    }

    bool IsDirectFeedThrough() const { return static_cast<const T*>(this)->IsDirectFeedThroughImpl(); }
    void SetSimulationStep(Real dt) { static_cast<T*>(this)->SetSimulationStepImpl(dt); }

    StateVector GetState() const { return static_cast<const T*>(this)->GetStateImpl(); }
    OutputVector Simulate(InputVector const& u)
    {
        static_cast<T*>(this)->UpdateOutputImpl(u);
        static_cast<T*>(this)->UpdateStateImpl(u);

        return m_output;
    }
    OutputVector GetOutput() const { return m_output; }
    void UpdateState(InputVector const& u) { static_cast<T*>(this)->UpdateStateImpl(u); }
    void UpdateOutput(InputVector const& u) { static_cast<T*>(this)->UpdateOutputImpl(u); }

  public:
    std::vector<std::string> GetMermaidDiagramString() const
    {
        return static_cast<const T*>(this)->GetMermaidDiagramStringImpl();
    }
    std::string GetMermaidEndConnectorId() const { return static_cast<const T*>(this)->GetMermaidEndConnectorIdImpl(); }
    std::string GetMermaidStartConnectorId() const
    {
        return static_cast<const T*>(this)->GetMermaidStartConnectorIdImpl();
    }
};

}  //namespace mpmca::dokblocks