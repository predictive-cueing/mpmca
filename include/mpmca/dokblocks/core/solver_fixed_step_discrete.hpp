/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <memory>
#include <stdexcept>

#include "mpmca/dokblocks/core/abstract_block.hpp"
#include "mpmca/dokblocks/core/block_diagram.hpp"
#include "mpmca/dokblocks/core/matrix_types.hpp"

namespace mpmca::dokblocks {
template <class BLOCK>
class SolverFixedStepDiscrete
{
    static_assert(utils::is_derived_from_abstract_block<BLOCK>::value,
                  "The template parameter of SolverFixedStepDiscrete must derive from AbstractBlock.");

    BLOCK m_block_diagram;

  public:
    using OutputVector = typename BLOCK::OutputVector;
    using InputVector = typename BLOCK::InputVector;
    SolverFixedStepDiscrete(BLOCK const& block_diagram, double dt);
    void SetSimulationStep(Real dt);
    OutputVector Simulate(InputVector const& u);
};

template <class BLOCK>
SolverFixedStepDiscrete<BLOCK>::SolverFixedStepDiscrete(const BLOCK& block_diagram, double dt)
    : m_block_diagram{block_diagram}
{
    m_block_diagram.SetSimulationStep(dt);
}

template <class BLOCK>
void SolverFixedStepDiscrete<BLOCK>::SetSimulationStep(Real step_size)
{
    m_block_diagram.SetSimulationStep(step_size);
}

template <class BLOCK>
typename SolverFixedStepDiscrete<BLOCK>::OutputVector SolverFixedStepDiscrete<BLOCK>::Simulate(InputVector const& u)
{
    return m_block_diagram.Simulate(u);
}
}  //namespace mpmca::dokblocks