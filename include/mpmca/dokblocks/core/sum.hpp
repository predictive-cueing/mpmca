/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <memory>

#include "mpmca/dokblocks/core/abstract_block.hpp"
#include "mpmca/dokblocks/core/matrix_types.hpp"
#include "mpmca/dokblocks/core/mermaid_tools.hpp"

namespace mpmca::dokblocks {
// TODO: Operator + for abstract block
template <class TOP, class BOTTOM>
class Sum : public AbstractBlock<TOP::NX + BOTTOM::NX, TOP::NU + BOTTOM::NU, TOP::NY, Sum<TOP, BOTTOM>>
{
    static_assert(TOP::NY == BOTTOM::NY);

    TOP m_top;
    BOTTOM m_bottom;
    using BASE = AbstractBlock<TOP::NX + BOTTOM::NX, TOP::NU + BOTTOM::NU, TOP::NY, Sum<TOP, BOTTOM>>;

  public:
    static constexpr int NY = BASE::NY;
    static constexpr int NU = BASE::NU;
    static constexpr int NX = BASE::NX;
    using typename BASE::InputVector;
    using typename BASE::OutputVector;
    using typename BASE::StateVector;

    Sum(const std::string& name, TOP const& top, BOTTOM const& bottom);
    Sum(Sum const& other) = default;
    Sum(Sum&& other) = default;
    Sum& operator=(Sum const& other) = default;
    Sum& operator=(Sum&& other) = default;
    ~Sum() = default;

  protected:
    friend BASE;
    StateVector GetStateImpl() const;
    void SetSimulationStepImpl(Real dt);

    void UpdateOutputImpl(InputVector const& u);
    void UpdateStateImpl(InputVector const& u);
    bool IsDirectFeedThroughImpl() const;
    std::vector<std::string> GetMermaidDiagramStringImpl() const
    {
        std::vector<std::string> result;
        auto top_vector = m_top.GetMermaidDiagramString();
        auto bottom_vector = m_bottom.GetMermaidDiagramString();

        std::copy(top_vector.begin(), top_vector.end(), std::back_inserter(result));
        std::copy(bottom_vector.begin(), bottom_vector.end(), std::back_inserter(result));

        result.push_back(this->GetMermaidStartConnectorId() + "{ }");
        result.push_back(this->GetMermaidEndConnectorId() + "((+))");

        result.push_back(this->GetMermaidStartConnectorId() + " -- " + m_top.GetInputSignalNamesString() + " --> " +
                         m_top.GetMermaidStartConnectorId());
        result.push_back(this->GetMermaidStartConnectorId() + " -- " + m_bottom.GetInputSignalNamesString() + " --> " +
                         m_bottom.GetMermaidStartConnectorId());

        result.push_back(m_top.GetMermaidEndConnectorId() + " -- " + m_top.GetOutputSignalNamesString() + " --> " +
                         this->GetMermaidEndConnectorId());
        result.push_back(m_bottom.GetMermaidEndConnectorId() + " -- " + m_bottom.GetOutputSignalNamesString() +
                         " --> " + this->GetMermaidEndConnectorId());

        return result;
    }
    std::string GetMermaidStartConnectorIdImpl() const { return this->GetUniqueId() + "_start"; }
    std::string GetMermaidEndConnectorIdImpl() const { return this->GetUniqueId() + "_end"; }
};

template <class TOP, class BOTTOM>
Sum<TOP, BOTTOM>::Sum(const std::string& name, TOP const& top, BOTTOM const& bottom)
    : BASE(name)
    , m_top{top}
    , m_bottom{bottom}
{
    auto top_input_signal_names = m_top.GetInputSignalNames();
    auto bottom_input_signal_names = m_bottom.GetInputSignalNames();
    std::copy(top_input_signal_names.begin(), top_input_signal_names.end(), this->m_input_signal_names.begin());
    std::copy(bottom_input_signal_names.begin(), bottom_input_signal_names.end(),
              this->m_input_signal_names.begin() + TOP::NU);

    for (int i = 0; i < this->NY; ++i) {
        this->m_output_signal_names[i] = m_top.GetOutputSignalNames()[i] + " + " + m_bottom.GetOutputSignalNames()[i];
    }
}

template <class TOP, class BOTTOM>
typename Sum<TOP, BOTTOM>::StateVector Sum<TOP, BOTTOM>::GetStateImpl() const
{
    return (StateVector() << m_top.GetState(), m_bottom.GetState()).finished();
}

template <class TOP, class BOTTOM>
void Sum<TOP, BOTTOM>::SetSimulationStepImpl(Real dt)
{
    m_top.SetSimulationStep(dt);
    m_bottom.SetSimulationStep(dt);
}

template <class TOP, class BOTTOM>
void Sum<TOP, BOTTOM>::UpdateOutputImpl(InputVector const& u)
{
    m_top.UpdateOutput(u.template topRows<TOP::NU>());
    m_bottom.UpdateOutput(u.template bottomRows<BOTTOM::NU>());

    this->m_output = m_top.GetOutput() + m_bottom.GetOutput();
}

template <class TOP, class BOTTOM>
void Sum<TOP, BOTTOM>::UpdateStateImpl(InputVector const& u)
{
    m_top.UpdateState(u.template topRows<TOP::NU>());
    m_bottom.UpdateState(u.template bottomRows<BOTTOM::NU>());
}

template <class TOP, class BOTTOM>
bool Sum<TOP, BOTTOM>::IsDirectFeedThroughImpl() const
{
    return m_top.IsDirectFeedThrough() || m_bottom.IsDirectFeedThrough();
}

template <class TOP, class BOTTOM, class... OTHERS>
auto MakeSum(const std::string& name, TOP const& top, BOTTOM const& bottom, OTHERS const&... others)
{
    auto sum = Sum(name, top, bottom);
    return MakeSum(name, sum, others...);
}

template <class TOP, class BOTTOM>
auto MakeSum(const std::string& name, TOP const& top, BOTTOM const& bottom)
{
    auto sum = Sum(name, top, bottom);
    return sum;
}

}  //namespace mpmca::dokblocks