/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <exception>

#include "mpmca/dokblocks/core/abstract_block.hpp"

namespace mpmca::dokblocks {

template <class DIRECT, class FEEDBACK>
class Feedback : public AbstractBlock<DIRECT::NX + FEEDBACK::NX, DIRECT::NU, DIRECT::NY, Feedback<DIRECT, FEEDBACK>>
{
    static_assert(FEEDBACK::NY == DIRECT::NU);
    static_assert(FEEDBACK::NU == DIRECT::NY);

    DIRECT m_direct;
    FEEDBACK m_feedback;
    using BASE = AbstractBlock<DIRECT::NX + FEEDBACK::NX, DIRECT::NU, DIRECT::NY, Feedback<DIRECT, FEEDBACK>>;

  public:
    static constexpr int NY = BASE::NY;
    static constexpr int NU = BASE::NU;
    static constexpr int NX = BASE::NX;
    using typename BASE::InputVector;
    using typename BASE::OutputVector;
    using typename BASE::StateVector;

    Feedback(const std::string& name, DIRECT const& direct, FEEDBACK const& feedback);
    Feedback(Feedback const& other) = default;
    Feedback(Feedback&& other) = default;
    Feedback& operator=(Feedback const& other) = default;
    Feedback& operator=(Feedback&& other) = default;
    ~Feedback() = default;

  protected:
    friend BASE;
    StateVector GetStateImpl() const;
    void SetSimulationStepImpl(Real dt);
    void UpdateOutputImpl(InputVector const& u);
    void UpdateStateImpl(InputVector const& u);

    bool IsDirectFeedThroughImpl() const;
    std::string GetMermaidEndConnectorIdImpl() const { return m_direct->GetUniqueId(); }
    std::string GetMermaidStartConnectorIdImpl() const { return this->GetUniqueId(); }
};

template <class DIRECT, class FEEDBACK>
Feedback<DIRECT, FEEDBACK>::Feedback(const std::string& name, DIRECT const& direct, FEEDBACK const& feedback)
    : BASE(name)
    , m_direct{direct}
    , m_feedback{feedback}
{
}

template <class DIRECT, class FEEDBACK>
typename Feedback<DIRECT, FEEDBACK>::StateVector Feedback<DIRECT, FEEDBACK>::GetStateImpl() const
{
    return (StateVector() << m_direct.GetState(), m_feedback.GetState()).finished();
}

template <class DIRECT, class FEEDBACK>
void Feedback<DIRECT, FEEDBACK>::SetSimulationStepImpl(Real dt)
{
    m_direct.SetSimulationStep(dt);
    m_feedback.SetSimulationStep(dt);
}

template <class DIRECT, class FEEDBACK>
void Feedback<DIRECT, FEEDBACK>::UpdateOutputImpl(InputVector const& u)
{
    OutputVector y_direct;
    if (!m_direct.IsDirectFeedThrough()) {
        m_direct.UpdateOutput(FEEDBACK::OutputVector::Zero());
    }
    else if (!m_feedback.IsDirectFeedThrough()) {
        m_feedback.UpdateOutput(DIRECT::OutputVector::Zero());
        m_direct.UpdateOutput(u - m_feedback.GetOutput());
    }
    else {
        // Not implemented error
        throw std::runtime_error("Not implemented");
    }

    this->m_output = m_direct.GetOutput();
}

template <class DIRECT, class FEEDBACK>
void Feedback<DIRECT, FEEDBACK>::UpdateStateImpl(InputVector const& u)
{
    if (!m_direct.IsDirectFeedThrough()) {
        auto e_dummy = FEEDBACK::OutputVector::Zero();
        m_direct.UpdateOutput(e_dummy);
        auto y_feedback = m_feedback.Simulate(m_direct.GetOutput());
        m_direct.UpdateState(u - y_feedback);
    }
    else if (!m_feedback.IsDirectFeedThrough()) {
        auto y_direct_dummy = DIRECT::OutputVector::Zero();
        m_feedback.UpdateOutput(y_direct_dummy);
        auto y_direct = m_direct.Simulate(u - m_feedback.GetOutput());
        m_feedback.UpdateState(y_direct);
    }
    else {
        // Not implemented error
        throw std::runtime_error("Not implemented");
    }
}

template <class DIRECT, class FEEDBACK>
bool Feedback<DIRECT, FEEDBACK>::IsDirectFeedThroughImpl() const
{
    //TODO
    return m_direct.IsDirectFeedThrough() || m_feedback.IsDirectFeedThrough();
}

template <class DIRECT, class FEEDBACK>
auto MakeFeedback(const std::string& name, DIRECT const& direct, FEEDBACK const& feedback)
{
    auto diagram = Feedback(name, direct, feedback);
    return diagram;
}

}  //namespace mpmca::dokblocks