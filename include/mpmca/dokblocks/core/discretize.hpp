/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <exception>

#include "mpmca/dokblocks/components/state_space.hpp"
#include "mpmca/dokblocks/core/matrix_types.hpp"
#include "mpmca/dokblocks/core/state_space_discretize.hpp"

namespace mpmca::dokblocks {
template <int NX, int NU, int NY>
StateSpace<NX, NU, NY> c2d(StateSpace<NX, NU, NY> const& ss, Real dt, DiscretizationMethod DM)
{
    if (ss.GetDt() != 0)
        throw std::runtime_error("The function c2d only accepts continuous state spaces.");

    Matrix<NX, NX> Ad;
    Matrix<NX, NU> Bd;
    Matrix<NY, NX> Cd;
    Matrix<NY, NU> Dd;

    switch (DM) {
        case DiscretizationMethod::kRungeKutta4:
            Discretize<DiscretizationMethod::kRungeKutta4>::c2d(ss.GetA(), ss.GetB(), ss.GetC(), ss.GetD(), dt, Ad, Bd,
                                                                Cd, Dd);
            break;

        case DiscretizationMethod::kTustin:
            Discretize<DiscretizationMethod::kTustin>::c2d(ss.GetA(), ss.GetB(), ss.GetC(), ss.GetD(), dt, Ad, Bd, Cd,
                                                           Dd);
            break;

        default:
            break;
    }

    return StateSpace(ss.GetName(), Ad, Bd, Cd, Dd, ss.GetState(), dt);
}

template <int NX, int NU, int NY>
StateSpace<NX, NU, NY> d2d(StateSpace<NX, NU, NY> const& ss, Real dt, DiscretizationMethod method)
{
    Matrix<NX, NX> Ad;
    Matrix<NX, NU> Bd;
    Matrix<NY, NX> Cd;
    Matrix<NY, NU> Dd;
    switch (method) {
        case DiscretizationMethod::kTustin:
            Discretize<DiscretizationMethod::kTustin>::d2d(ss.GetA(), ss.GetB(), ss.GetC(), ss.GetD(), ss.GetDt(), dt,
                                                           Ad, Bd, Cd, Dd);
            break;
        default:
            break;
    }

    return StateSpace(ss.GetName(), Ad, Bd, Cd, Dd, ss.GetState(), dt);
}

template <int NX, int NU, int NY>
StateSpace<NX, NU, NY> d2c(StateSpace<NX, NU, NY> const& ssd, DiscretizationMethod method)
{
    Matrix<NX, NX> A;
    Matrix<NX, NU> B;
    Matrix<NY, NX> C;
    Matrix<NY, NU> D;

    switch (method) {
        case DiscretizationMethod::kTustin:
            Discretize<DiscretizationMethod::kTustin>::d2c(ssd.GetA(), ssd.GetB(), ssd.GetC(), ssd.GetD(), ssd.GetDt(),
                                                           A, B, C, D);
            break;

        default:
            break;
    }

    return StateSpace(ssd.GetName(), A, B, C, D, ssd.GetState());
}
}  //namespace mpmca::dokblocks