/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <algorithm>
#include <functional>

#include "mpmca/dokblocks/components/state_space.hpp"
#include "mpmca/dokblocks/core/matrix_types.hpp"

namespace mpmca::dokblocks {

template <std::size_t N_NUM, std::size_t N_DEN>
void NormalizeAndFillWithZeros(std::array<Real, N_NUM> const& num, std::array<Real, N_DEN> const& den,
                               std::array<Real, N_DEN>& num_normalized_and_filled,
                               std::array<Real, N_DEN>& den_filled) noexcept
{
    Real den0 = den.front();

    std::transform(den.cbegin(), den.cend(), den_filled.begin(),
                   std::bind(std::divides<Real>(), std::placeholders::_1, den0));

    num_normalized_and_filled.fill(0);
    std::transform(num.crbegin(), num.crend(), num_normalized_and_filled.rbegin(),
                   std::bind(std::divides<Real>(), std::placeholders::_1, den0));
}

// Set controllable canonical form from SISO TF
namespace controllable_canonical {
template <std::size_t N_NUM, std::size_t N_DEN>
auto GetA(std::array<Real, N_NUM> const& num, std::array<Real, N_DEN> const& den) noexcept
{
    constexpr int NP = static_cast<int>(N_DEN - 1);

    Matrix<NP - 1, NP - 1> Aid = Matrix<NP - 1, NP - 1>::Identity();
    Matrix<NP - 1, 1> Az = Matrix<NP - 1, 1>::Zero();

    std::array<Real, N_DEN - 1> den_reversed_and_negated;
    std::transform(den.crbegin(), den.crend() - 1, den_reversed_and_negated.begin(), std::negate<Real>());
    Matrix<1, NP> A_den{den_reversed_and_negated.data()};

    Matrix<NP, NP> A;
    A << Az, Aid, A_den;

    return A;
}

template <std::size_t N_NUM, std::size_t N_DEN>
auto GetB(std::array<Real, N_NUM> const& num, std::array<Real, N_DEN> const& den) noexcept
{
    constexpr int NP = static_cast<int>(N_DEN - 1);

    Matrix<NP, 1> B;
    B << Matrix<NP - 1, 1>::Zero(), 1;

    return B;
}

template <std::size_t N_NUM, std::size_t N_DEN>
auto GetC(std::array<Real, N_NUM> const& num, std::array<Real, N_DEN> const& den) noexcept
{
    constexpr int NP = static_cast<int>(N_DEN - 1);

    Real num0 = num.front();

    std::array<Real, NP> CArray;
    auto it_num = num.cbegin() + 1;
    auto it_den = den.cbegin() + 1;
    auto it_c_rev = CArray.rbegin();
    for (; it_den != den.cend(); it_den++, it_num++, it_c_rev++)
        *it_c_rev = (*it_num) - (*it_den) * num0;

    return Matrix<1, NP>(CArray.data());
}

template <std::size_t N_NUM, std::size_t N_DEN>
auto GetD(std::array<Real, N_NUM> const& num, std::array<Real, N_DEN> const& den) noexcept
{
    Matrix<1, 1> D = Matrix<1, 1>::Constant(num.front());
    return D;
}
}  //namespace controllable_canonical

template <std::size_t N_NUM, std::size_t N_DEN>
auto tf2ss(const std::string& name, std::array<Real, N_NUM> num, std::array<Real, N_DEN> den) noexcept
{
    static_assert(N_DEN >= N_NUM);

    std::array<Real, N_DEN> num_post, den_post;
    NormalizeAndFillWithZeros(num, den, num_post, den_post);

    auto A = controllable_canonical::GetA(num_post, den_post);
    auto B = controllable_canonical::GetB(num_post, den_post);
    auto C = controllable_canonical::GetC(num_post, den_post);
    auto D = controllable_canonical::GetD(num_post, den_post);

    return StateSpace(name, A, B, C, D);
}
}  //namespace mpmca::dokblocks