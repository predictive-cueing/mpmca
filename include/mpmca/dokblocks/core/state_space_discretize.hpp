/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/dokblocks/core/matrix_types.hpp"

namespace mpmca::dokblocks {
enum class DiscretizationMethod
{
    kRungeKutta4,
    kTustin
};

template <DiscretizationMethod DM>
struct Discretize;

template <>
struct Discretize<DiscretizationMethod::kRungeKutta4>
{
    template <int NX, int NU, int NY>
    static void c2d(Matrix<NX, NX> const& A, Matrix<NX, NU> const& B, Matrix<NY, NX> const& C, Matrix<NY, NU> const& D,
                    Real dt, Matrix<NX, NX>& Ad, Matrix<NX, NU>& Bd, Matrix<NY, NX>& Cd, Matrix<NY, NU>& Dd)
    {
        Matrix<NX, NX> A2 = A * A;
        Matrix<NX, NX> A3 = A2 * A;
        Matrix<NX, NX> A4 = A3 * A;

        auto I = GetIdentity(A);

        Ad = I + (dt * A + dt * dt * A2 / 2. + dt * dt * dt * A3 / 6. + dt * dt * dt * dt * A4 / 24.);
        Bd = (dt * B + dt * dt * A * B / 2. + dt * dt * dt * A2 * B / 6. + dt * dt * dt * dt * A3 * B / 24.);
        Cd = C;
        Dd = D;
    }
};

// http://cse.lab.imtlucca.it/~bemporad/teaching/ac/pdf/04a-TD_sys.pdf
// http://arrow.utias.utoronto.ca/~damaren/s9.pdf
// http://techteach.no/publications/discretetime_signals_systems/discrete.pdf
// https://math.aalto.fi/~jmalinen/MyPSFilesInWeb/milano-talk.pdf
template <>
struct Discretize<DiscretizationMethod::kTustin>
{
    template <int NX, int NU, int NY>
    static void c2d(Matrix<NX, NX> const& A, Matrix<NX, NU> const& B, Matrix<NY, NX> const& C, Matrix<NY, NU> const& D,
                    Real dt, Matrix<NX, NX>& Ad, Matrix<NX, NU>& Bd, Matrix<NY, NX>& Cd, Matrix<NY, NU>& Dd)
    {
        auto I = GetIdentity(A);
        Matrix<NX, NX> P_inv = (I - A * dt / 2.).inverse();
        Matrix<NX, NX> Q = (I + A * dt / 2.);

        Ad = P_inv * Q;
        Bd = P_inv * B * dt;
        Cd = C * P_inv;
        Dd = C * Bd / 2. + D;
    }

    template <int NX, int NU, int NY>
    static void d2c(Matrix<NX, NX> const& Ad, Matrix<NX, NU> const& Bd, Matrix<NY, NX> const& Cd,
                    Matrix<NY, NU> const& Dd, Real dt, Matrix<NX, NX>& A, Matrix<NX, NU>& B, Matrix<NY, NX>& C,
                    Matrix<NY, NU>& D)
    {
        auto I = GetIdentity(Ad);

        Matrix<NX, NX> PdInv = ((Ad + I) * dt / 2.).inverse();
        Matrix<NX, NX> Qd = (Ad - I);

        A = PdInv * Qd;
        B = PdInv * Bd;
        C = Cd * PdInv * dt;
        D = -C * Bd / 2. + Dd;
    }

    template <int NX, int NU, int NY>
    static void d2d(Matrix<NX, NX> const& Ad1, Matrix<NX, NU> const& Bd1, Matrix<NY, NX> const& Cd1,
                    Matrix<NY, NU> const& Dd1, Real dt1, Real dt2, Matrix<NX, NX>& Ad2, Matrix<NX, NU>& Bd2,
                    Matrix<NY, NX>& Cd2, Matrix<NY, NU>& Dd2)
    {
        auto I = GetIdentity(Ad1);

        Matrix<NX, NX> PdInv1 = (((Ad1 + I) - (Ad1 - I) * dt2 / dt1).inverse());
        Matrix<NX, NX> Qd1 = ((Ad1 + I) + (Ad1 - I) * dt2 / dt1);

        Ad2 = PdInv1 * Qd1;
        Bd2 = PdInv1 * Bd1 * 2. * dt2 / dt1;
        Cd2 = 2. * Cd1 * PdInv1;
        //D = -C * Bd / 2. + Dd;

        Dd2 = Cd1 * (Ad1 + I).inverse() * (PdInv1 * 2. * dt2 / dt1 - I) * Bd1 + Dd1;
    }
};
}  //namespace mpmca::dokblocks
