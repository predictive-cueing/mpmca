/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <string>

#include "mpmca/dokblocks/core/abstract_block.hpp"

namespace mpmca::dokblocks {

template <class TOP, class BOTTOM>
class Append
    : public AbstractBlock<TOP::NX + BOTTOM::NX, TOP::NU + BOTTOM::NU, TOP::NY + BOTTOM::NY, Append<TOP, BOTTOM>>
{
    TOP m_top;
    BOTTOM m_bottom;
    using BASE = AbstractBlock<TOP::NX + BOTTOM::NX, TOP::NU + BOTTOM::NU, TOP::NY + BOTTOM::NY, Append<TOP, BOTTOM>>;

  public:
    static constexpr int NY = BASE::NY;
    static constexpr int NU = BASE::NU;
    static constexpr int NX = BASE::NX;
    using typename BASE::InputVector;
    using typename BASE::OutputVector;
    using typename BASE::StateVector;

    Append(const std::string& name, TOP const& top, BOTTOM const& bottom);
    Append(Append const& other) = default;
    Append(Append&& other) = default;

    Append& operator=(Append const& other) = default;
    Append& operator=(Append&& other) = default;
    ~Append() = default;

  protected:
    friend BASE;
    StateVector GetStateImpl() const;
    void SetSimulationStepImpl(Real dt);

    void UpdateOutputImpl(InputVector const& u);
    void UpdateStateImpl(InputVector const& u);
    bool IsDirectFeedThroughImpl() const;
    std::vector<std::string> GetMermaidDiagramStringImpl() const
    {
        std::vector<std::string> result;
        auto top_vector = m_top.GetMermaidDiagramString();
        auto bottom_vector = m_bottom.GetMermaidDiagramString();

        std::copy(top_vector.begin(), top_vector.end(), std::back_inserter(result));
        std::copy(bottom_vector.begin(), bottom_vector.end(), std::back_inserter(result));
        result.push_back(this->GetMermaidStartConnectorId() + "{{ " + std::to_string(this->NU) + " }}");
        result.push_back(this->GetMermaidEndConnectorId() + "{{ " + std::to_string(this->NY) + " }}");

        if (m_top.NU > 0) {
            result.push_back(this->GetMermaidStartConnectorId() + " -- " + m_top.GetInputSignalNamesString() + " --> " +
                             m_top.GetMermaidStartConnectorId());
        }

        if (m_bottom.NU > 0) {
            result.push_back(this->GetMermaidStartConnectorId() + " -- " + m_bottom.GetInputSignalNamesString() +
                             " --> " + m_bottom.GetMermaidStartConnectorId());
        }

        result.push_back(m_top.GetMermaidEndConnectorId() + "-- " + m_top.GetOutputSignalNamesString() + " --> " +
                         this->GetMermaidEndConnectorId());
        result.push_back(m_bottom.GetMermaidEndConnectorId() + "-- " + m_bottom.GetOutputSignalNamesString() + " --> " +
                         this->GetMermaidEndConnectorId());

        return result;
    }
    std::string GetMermaidEndConnectorIdImpl() const { return this->GetUniqueId() + "_end"; }
    std::string GetMermaidStartConnectorIdImpl() const { return this->GetUniqueId() + "_start"; }
};

template <class TOP, class BOTTOM>
Append<TOP, BOTTOM>::Append(const std::string& name, TOP const& top, BOTTOM const& bottom)
    : BASE(name)
    , m_top{top}
    , m_bottom{bottom}
{
    auto top_input_signal_names = m_top.GetInputSignalNames();
    auto bottom_input_signal_names = m_bottom.GetInputSignalNames();
    std::copy(top_input_signal_names.begin(), top_input_signal_names.end(), this->m_input_signal_names.begin());
    std::copy(bottom_input_signal_names.begin(), bottom_input_signal_names.end(),
              this->m_input_signal_names.begin() + TOP::NU);

    auto top_output_signal_names = m_top.GetOutputSignalNames();
    auto bottom_output_signal_names = m_bottom.GetOutputSignalNames();
    std::copy(top_output_signal_names.begin(), top_output_signal_names.end(), this->m_output_signal_names.begin());
    std::copy(bottom_output_signal_names.begin(), bottom_output_signal_names.end(),
              this->m_output_signal_names.begin() + TOP::NY);
}

template <class TOP, class BOTTOM>
typename Append<TOP, BOTTOM>::StateVector Append<TOP, BOTTOM>::GetStateImpl() const
{
    return (StateVector() << m_top.GetState(), m_bottom.GetState()).finished();
}

template <class TOP, class BOTTOM>
void Append<TOP, BOTTOM>::SetSimulationStepImpl(Real dt)
{
    m_top.SetSimulationStep(dt);
    m_bottom.SetSimulationStep(dt);
}

template <class TOP, class BOTTOM>
void Append<TOP, BOTTOM>::UpdateOutputImpl(InputVector const& u)
{
    m_top.UpdateOutput(u.template topRows<TOP::NU>());
    m_bottom.UpdateOutput(u.template bottomRows<BOTTOM::NU>());
    this->m_output << m_top.GetOutput(), m_bottom.GetOutput();
}

template <class TOP, class BOTTOM>
void Append<TOP, BOTTOM>::UpdateStateImpl(InputVector const& u)
{
    m_top.UpdateState(u.template topRows<TOP::NU>());
    m_bottom.UpdateState(u.template bottomRows<BOTTOM::NU>());
}

template <class TOP, class BOTTOM>
bool Append<TOP, BOTTOM>::IsDirectFeedThroughImpl() const
{
    return m_top.IsDirectFeedThrough() || m_bottom.IsDirectFeedThrough();
}

template <class TOP, class BOTTOM, class... OTHERS>
auto MakeAppend(const std::string& name, TOP const& top, BOTTOM const& bottom, OTHERS const&... others)
{
    auto append = Append(name, top, bottom);
    return MakeAppend(name, append, others...);
}

template <class TOP, class BOTTOM>
auto MakeAppend(const std::string& name, TOP const& top, BOTTOM const& bottom)
{
    auto append = Append(name, top, bottom);
    return append;
}
}  //namespace mpmca::dokblocks