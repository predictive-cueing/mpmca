/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <array>
#include <string>

namespace mpmca::dokblocks {
class MermaidTools
{
  public:
    template <size_t N>
    static std::string MatchedInputOutputSignalNames(const std::array<std::string, N>& output_names,
                                                     const std::array<std::string, N>& input_names)
    {
        std::string result = "\"`";

        for (int i = 0; i < N; ++i) {
            if (output_names[i].compare(input_names[i]) == 0) {
                result += output_names[i];
            }
            else {
                result += output_names[i] + " > " + input_names[i];
            }

            if (i < N - 1) {
                result += "\n";
            }
        }

        result += " `\"";

        return result;
    }
};
}  //namespace mpmca::dokblocks