/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <iostream>
#include <stdexcept>

#include "mpmca/dokblocks/components/state_space.hpp"

namespace mpmca::dokblocks {
/**
 * Declarations
 */

// Series connection of two input/output models.
// Similarly to Octave, the state of the resulting series system is:
//      x_series = [x_rhs, x_lhs]
template <int NX_L, int NU_L, int NY_L, int NX_R, int NU_R, int NY_R>
StateSpace<NX_L + NX_R, NU_L, NY_R> MakeSeries(StateSpace<NX_L, NU_L, NY_L> const& lhs,
                                               StateSpace<NX_R, NU_R, NY_R> const& rhs);

// Parallel connection of two input/output models.
template <int NX_TOP, int NU_TOP, int NY_TOP, int NX_BOTTOM, int NU_BOTTOM, int NY_BOTTOM>
StateSpace<NX_TOP + NX_BOTTOM, NU_TOP, NY_TOP> MakeParallel(StateSpace<NX_TOP, NU_TOP, NY_TOP> const& top,
                                                            StateSpace<NX_BOTTOM, NU_BOTTOM, NY_BOTTOM> const& bottom);

// Sum outputs
template <int NX_TOP, int NU_TOP, int NY_TOP, int NX_BOTTOM, int NU_BOTTOM, int NY_BOTTOM>
StateSpace<NX_TOP + NX_BOTTOM, NU_BOTTOM + NU_TOP, NY_TOP> MakeSumOutputs(
    StateSpace<NX_TOP, NU_TOP, NY_TOP> const& lhs, StateSpace<NX_BOTTOM, NU_BOTTOM, NY_BOTTOM> const& rhs);

// Feedback connection of two input/output models.
template <int NX_DIRECT, int NU_DIRECT, int NY_DIRECT, int NX_FEEDBACK, int NU_FEEDBACK, int NY_FEEDBACK>
StateSpace<NX_DIRECT + NX_FEEDBACK, NU_DIRECT, NY_DIRECT> MakeFeedback(
    StateSpace<NX_DIRECT, NU_DIRECT, NY_DIRECT> const& direct_path,
    StateSpace<NX_FEEDBACK, NU_FEEDBACK, NY_FEEDBACK> const& feedback_path);

// Append multiple input/output models.
template <int NX_TOP, int NU_TOP, int NY_TOP, int NX_BOTTOM, int NU_BOTTOM, int NY_BOTTOM, int... NX, int... NU,
          int... NY>
auto MakeAppend(StateSpace<NX_TOP, NU_TOP, NY_TOP> const& top,
                StateSpace<NX_BOTTOM, NU_BOTTOM, NY_BOTTOM> const& bottom, StateSpace<NX, NU, NY> const&... others);

template <int NX_TOP, int NU_TOP, int NY_TOP, int NX_BOTTOM, int NU_BOTTOM, int NY_BOTTOM>
StateSpace<NX_TOP + NX_BOTTOM, NU_TOP + NU_BOTTOM, NY_TOP + NY_BOTTOM> MakeAppend(
    StateSpace<NX_TOP, NU_TOP, NY_TOP> const& top, StateSpace<NX_BOTTOM, NU_BOTTOM, NY_BOTTOM> const& bottom);

// Make same inputs for multiple input/output models.
template <int NX_TOP, int NU_TOP, int NY_TOP, int NX_BOTTOM, int NU_BOTTOM, int NY_BOTTOM, int... NX, int... NU,
          int... NY>
auto MakeSameInputs(StateSpace<NX_TOP, NU_TOP, NY_TOP> const& top,
                    StateSpace<NX_BOTTOM, NU_BOTTOM, NY_BOTTOM> const& bottom, StateSpace<NX, NU, NY> const&... others);

template <int NX_TOP, int NU_TOP, int NY_TOP, int NX_BOTTOM, int NU_BOTTOM, int NY_BOTTOM>
StateSpace<NX_TOP + NX_BOTTOM, NU_TOP, NY_TOP + NY_BOTTOM> MakeSameInputs(
    StateSpace<NX_TOP, NU_TOP, NY_TOP> const& top, StateSpace<NX_BOTTOM, NU_BOTTOM, NY_BOTTOM> const& bottom);

// Operators

// Multiply output with number
template <std::size_t NX, std::size_t NU, std::size_t NY>
StateSpace<NX, NU, NY> operator*(StateSpace<NX, NU, NY> const& ss, Real gain);

// Same as series
template <int NX_L, int NU_L, int NY_L, int NX_R, int NU_R, int NY_R>
StateSpace<NX_L + NX_R, NU_L, NY_R> operator*(StateSpace<NX_L, NU_L, NY_L> const& lhs,
                                              StateSpace<NX_R, NU_R, NY_R> const& rhs);

// Same as parallel
template <int NX_TOP, int NU_TOP, int NY_TOP, int NX_BOTTOM, int NU_BOTTOM, int NY_BOTTOM>
StateSpace<NX_TOP + NX_BOTTOM, NU_TOP, NY_TOP> operator+(StateSpace<NX_TOP, NU_TOP, NY_TOP> const& top,
                                                         StateSpace<NX_BOTTOM, NU_BOTTOM, NY_BOTTOM> const& bottom);

/**
 * Definitions
 */
template <int NX_TOP, int NU_TOP, int NY_TOP, int NX_BOTTOM, int NU_BOTTOM, int NY_BOTTOM>
StateSpace<NX_TOP + NX_BOTTOM, NU_BOTTOM + NU_TOP, NY_TOP> MakeSumOutputs(
    StateSpace<NX_TOP, NU_TOP, NY_TOP> const& lhs, StateSpace<NX_BOTTOM, NU_BOTTOM, NY_BOTTOM> const& rhs)
{
    static_assert(NY_TOP == NY_BOTTOM);
    if (lhs.GetDt() != rhs.GetDt())
        throw std::runtime_error("Sample time must be the same for all StateSpaces objs to connect.");

    constexpr int NX_PLUS = NX_TOP + NX_BOTTOM;
    constexpr int NU_PLUS = NU_BOTTOM + NU_TOP;
    constexpr int NY_PLUS = NY_TOP;

    Matrix<NX_PLUS, NX_PLUS> A;
    A << lhs.GetA(), Matrix<NX_TOP, NX_BOTTOM>::Zero(), Matrix<NX_BOTTOM, NX_TOP>::Zero(), rhs.GetA();

    Matrix<NX_PLUS, NU_PLUS> B;
    B << lhs.GetB(), Matrix<NX_TOP, NU_BOTTOM>::Zero(), Matrix<NX_BOTTOM, NU_TOP>::Zero(), rhs.GetB();

    Matrix<NY_PLUS, NX_PLUS> C;
    C << lhs.GetC(), rhs.GetC();

    Matrix<NY_PLUS, NU_PLUS> D;
    D << lhs.GetD(), rhs.GetD();

    Vector<NX_PLUS> x0;
    x0 << lhs.GetState(), rhs.GetState();

    return StateSpace("empty_string", A, B, C, D, x0, lhs.GetDt());
}

template <int NX_L, int NU_L, int NY_L, int NX_R, int NU_R, int NY_R>
StateSpace<NX_L + NX_R, NU_L, NY_R> MakeSeries(StateSpace<NX_L, NU_L, NY_L> const& lhs,
                                               StateSpace<NX_R, NU_R, NY_R> const& rhs)
{
    static_assert(NY_L == NU_R);
    if (lhs.GetDt() != rhs.GetDt())
        throw std::runtime_error("Sample time must be the same for all StateSpaces objs to connect.");

    constexpr int NX_SERIES = NX_L + NX_R;
    constexpr int NU_SERIES = NU_L;
    constexpr int NY_SERIES = NY_R;

    Matrix<NX_SERIES, NX_SERIES> A;
    A << rhs.GetA(), rhs.GetB() * lhs.GetC(), Matrix<NX_L, NX_R>::Zero(), lhs.GetA();

    Matrix<NX_SERIES, NU_SERIES> B;
    B << rhs.GetB() * lhs.GetD(), lhs.GetB();

    Matrix<NY_SERIES, NX_SERIES> C;
    C << rhs.GetC(), rhs.GetD() * lhs.GetC();

    Matrix<NY_SERIES, NU_SERIES> D;
    D << rhs.GetD() * lhs.GetD();

    Vector<NX_SERIES> x0;
    x0 << rhs.GetState(), lhs.GetState();

    return StateSpace("empty_string", A, B, C, D, x0, lhs.GetDt());
}

template <int NX_TOP, int NU_TOP, int NY_TOP, int NX_BOTTOM, int NU_BOTTOM, int NY_BOTTOM>
StateSpace<NX_TOP + NX_BOTTOM, NU_TOP, NY_TOP> MakeParallel(StateSpace<NX_TOP, NU_TOP, NY_TOP> const& top,
                                                            StateSpace<NX_BOTTOM, NU_BOTTOM, NY_BOTTOM> const& bottom)
{
    static_assert(NU_TOP == NU_BOTTOM);
    static_assert(NY_TOP == NY_BOTTOM);
    if (top.GetDt() != bottom.GetDt())
        throw std::runtime_error("Sample time must be the same for all StateSpaces objs to connect.");

    constexpr int NX_PARALLEL = NX_TOP + NX_BOTTOM;
    constexpr int NU_PARALLEL = NU_TOP;
    constexpr int NY_PARALLEL = NY_TOP;

    Matrix<NX_PARALLEL, NX_PARALLEL> A;
    A << top.GetA(), Matrix<NX_TOP, NX_BOTTOM>::Zero(), Matrix<NX_BOTTOM, NX_TOP>::Zero(), bottom.GetA();

    Matrix<NX_PARALLEL, NU_PARALLEL> B;
    B << top.GetB(), bottom.GetB();

    Matrix<NY_PARALLEL, NX_PARALLEL> C;
    C << top.GetC(), bottom.GetC();

    Matrix<NY_PARALLEL, NU_PARALLEL> D;
    D << top.GetD() + bottom.GetD();

    Vector<NX_PARALLEL> x0;
    x0 << top.GetState(), bottom.GetState();

    return StateSpace("empty_string", A, B, C, D, x0, top.GetDt());
}

template <int NX_DIRECT, int NU_DIRECT, int NY_DIRECT, int NX_FEEDBACK, int NU_FEEDBACK, int NY_FEEDBACK>
StateSpace<NX_DIRECT + NX_FEEDBACK, NU_DIRECT, NY_DIRECT> MakeFeedback(
    StateSpace<NX_DIRECT, NU_DIRECT, NY_DIRECT> const& direct_path,
    StateSpace<NX_FEEDBACK, NU_FEEDBACK, NY_FEEDBACK> const& feedback_path)
{
    static_assert(NU_DIRECT == NY_FEEDBACK);
    static_assert(NY_DIRECT == NU_FEEDBACK);
    if (direct_path.GetDt() != feedback_path.GetDt())
        throw std::runtime_error("Sample time must be the same for all StateSpaces objs to connect.");

    constexpr int NX_CL = NX_DIRECT + NX_FEEDBACK;
    constexpr int NU_CL = NU_DIRECT;
    constexpr int NY_CL = NY_DIRECT;

    auto Q = (Matrix<NY_FEEDBACK, NY_DIRECT>::Identity() + feedback_path.GetD() * direct_path.GetD()).inverse();
    auto Q_C_feedback = Q * feedback_path.GetC();
    auto D_direct_Q_C_feedback = direct_path.GetD() * Q_C_feedback;
    auto Q_D_feedback_C_direct = Q * feedback_path.GetD() * direct_path.GetC();
    auto C_direct_minus_D_direct_Q_D_feedback_C_direct =
        direct_path.GetC() - direct_path.GetD() * Q_D_feedback_C_direct;

    Matrix<NX_CL, NX_CL> A;
    A << direct_path.GetA() - direct_path.GetB() * Q_D_feedback_C_direct, -direct_path.GetB() * Q_C_feedback,
        feedback_path.GetB() * C_direct_minus_D_direct_Q_D_feedback_C_direct,
        feedback_path.GetA() - feedback_path.GetB() * D_direct_Q_C_feedback;

    Matrix<NX_CL, NU_CL> B;
    B << direct_path.GetB() * Q, feedback_path.GetB() * direct_path.GetD() * Q;

    Matrix<NY_CL, NX_CL> C;
    C << C_direct_minus_D_direct_Q_D_feedback_C_direct, -D_direct_Q_C_feedback;

    Matrix<NY_CL, NU_CL> D;
    D << direct_path.GetD() * Q;

    Vector<NX_CL> x0;
    x0 << direct_path.GetState(), feedback_path.GetState();

    return StateSpace("empty_string", A, B, C, D, x0, direct_path.GetDt());
}

template <int NX_TOP, int NU_TOP, int NY_TOP, int NX_BOTTOM, int NU_BOTTOM, int NY_BOTTOM, int... NX, int... NU,
          int... NY>
auto MakeAppend(StateSpace<NX_TOP, NU_TOP, NY_TOP> const& top,
                StateSpace<NX_BOTTOM, NU_BOTTOM, NY_BOTTOM> const& bottom, StateSpace<NX, NU, NY> const&... others)
{
    auto ss_append = MakeAppend(top, bottom);
    return MakeAppend(ss_append, others...);
}

template <int NX_TOP, int NU_TOP, int NY_TOP, int NX_BOTTOM, int NU_BOTTOM, int NY_BOTTOM>
StateSpace<NX_TOP + NX_BOTTOM, NU_TOP + NU_BOTTOM, NY_TOP + NY_BOTTOM> MakeAppend(
    StateSpace<NX_TOP, NU_TOP, NY_TOP> const& top, StateSpace<NX_BOTTOM, NU_BOTTOM, NY_BOTTOM> const& bottom)
{
    constexpr int NX_APPEND = NX_TOP + NX_BOTTOM;
    constexpr int NU_APPEND = NU_TOP + NU_BOTTOM;
    constexpr int NY_APPEND = NY_TOP + NY_BOTTOM;
    if (top.GetDt() != bottom.GetDt())
        throw std::runtime_error("Sample time must be the same for all StateSpaces objs to connect.");

    Matrix<NX_APPEND, NX_APPEND> A;
    A << top.GetA(), Matrix<NX_TOP, NX_BOTTOM>::Zero(), Matrix<NX_BOTTOM, NX_TOP>::Zero(), bottom.GetA();

    Matrix<NX_APPEND, NU_APPEND> B;
    B << top.GetB(), Matrix<NX_TOP, NU_BOTTOM>::Zero(), Matrix<NX_BOTTOM, NU_TOP>::Zero(), bottom.GetB();

    Matrix<NY_APPEND, NX_APPEND> C;
    C << top.GetC(), Matrix<NY_TOP, NX_BOTTOM>::Zero(), Matrix<NY_BOTTOM, NX_TOP>::Zero(), bottom.GetC();

    Matrix<NY_APPEND, NU_APPEND> D;
    D << top.GetD(), Matrix<NY_TOP, NU_BOTTOM>::Zero(), Matrix<NY_BOTTOM, NU_TOP>::Zero(), bottom.GetD();

    Vector<NX_APPEND> x0;
    x0 << top.GetState(), bottom.GetState();

    return StateSpace("empty_string", A, B, C, D, x0, top.GetDt());
}

template <int NX_TOP, int NU_TOP, int NY_TOP, int NX_BOTTOM, int NU_BOTTOM, int NY_BOTTOM, int... NX, int... NU,
          int... NY>
auto MakeSameInputs(StateSpace<NX_TOP, NU_TOP, NY_TOP> const& top,
                    StateSpace<NX_BOTTOM, NU_BOTTOM, NY_BOTTOM> const& bottom, StateSpace<NX, NU, NY> const&... others)
{
    auto ss_same_inputs = MakeSameInputs(top, bottom);
    return MakeSameInputs(ss_same_inputs, others...);
}

template <int NX_TOP, int NU_TOP, int NY_TOP, int NX_BOTTOM, int NU_BOTTOM, int NY_BOTTOM>
StateSpace<NX_TOP + NX_BOTTOM, NU_TOP, NY_TOP + NY_BOTTOM> MakeSameInputs(
    StateSpace<NX_TOP, NU_TOP, NY_TOP> const& top, StateSpace<NX_BOTTOM, NU_BOTTOM, NY_BOTTOM> const& bottom)
{
    static_assert(NU_TOP == NU_BOTTOM);
    if (top.GetDt() != bottom.GetDt())
        throw std::runtime_error("Sample time must be the same for all StateSpaces objs to connect.");

    constexpr int NX_SAME_INPUTS = NX_TOP + NX_BOTTOM;
    constexpr int NU_SAME_INPUTS = NU_TOP;
    constexpr int NY_SAME_INPUTS = NY_TOP + NY_BOTTOM;

    Matrix<NX_SAME_INPUTS, NX_SAME_INPUTS> A;
    A << top.GetA(), Matrix<NX_TOP, NX_BOTTOM>::Zero(), Matrix<NX_BOTTOM, NX_TOP>::Zero(), bottom.GetA();

    Matrix<NX_SAME_INPUTS, NU_SAME_INPUTS> B;
    B << top.GetB(), bottom.GetB();

    Matrix<NY_SAME_INPUTS, NX_SAME_INPUTS> C;
    C << top.GetC(), Matrix<NY_TOP, NX_BOTTOM>::Zero(), Matrix<NY_BOTTOM, NX_TOP>::Zero(), bottom.GetC();

    Matrix<NY_SAME_INPUTS, NU_SAME_INPUTS> D;
    D << top.GetD(), bottom.GetD();

    Vector<NX_SAME_INPUTS> x0;
    x0 << top.GetState(), bottom.GetState();

    return StateSpace("empty_string", A, B, C, D, x0, top.GetDt());
}

template <int NX_L, int NU_L, int NY_L, int NX_R, int NU_R, int NY_R>
StateSpace<NX_L + NX_R, NU_L, NY_R> operator*(StateSpace<NX_L, NU_L, NY_L> const& lhs,
                                              StateSpace<NX_R, NU_R, NY_R> const& rhs)
{
    return MakeSeries(lhs, rhs);
}

template <std::size_t NX, std::size_t NU, std::size_t NY>
StateSpace<NX, NU, NY> operator*(StateSpace<NX, NU, NY> const& ss, Real gain)
{
    Matrix<NY, NX> C = ss.GetC() * gain;
    Matrix<NY, NU> D = ss.GetD() * gain;

    return StateSpace(ss.GetA(), ss.GetB(), C, D, ss.GetState(), ss.GetDt());
}

template <int NX_TOP, int NU_TOP, int NY_TOP, int NX_BOTTOM, int NU_BOTTOM, int NY_BOTTOM>
StateSpace<NX_TOP + NX_BOTTOM, NU_TOP, NY_TOP> operator+(StateSpace<NX_TOP, NU_TOP, NY_TOP> const& top,
                                                         StateSpace<NX_BOTTOM, NU_BOTTOM, NY_BOTTOM> const& bottom)
{
    return MakeParallel(top, bottom);
}
}  //namespace mpmca::dokblocks