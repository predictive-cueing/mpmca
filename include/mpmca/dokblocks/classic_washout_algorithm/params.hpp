/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <nlohmann/json.hpp>

#include "mpmca/dokblocks/core/matrix_types.hpp"
#include "mpmca/utilities/configuration.hpp"

namespace mpmca::dokblocks::classic_washout_algorithm {

class Params
{
    Vector<3> m_gravity_world;
    Vector<3> m_acceleration_gain;
    Vector<3> m_angular_velocity_gain;
    Real m_acceleration_split_omega_x;
    Real m_acceleration_split_omega_y;
    Real m_acceleration_split_omega_z;
    Real m_acceleration_washout_high_pass_omega_x;
    Real m_acceleration_washout_high_pass_omega_y;
    Real m_acceleration_washout_high_pass_omega_z;
    Real m_acceleration_washout_high_pass_zeta_x;
    Real m_acceleration_washout_high_pass_zeta_y;
    Real m_acceleration_washout_high_pass_zeta_z;
    Real m_tilt_coordination_maximum_roll;
    Real m_tilt_coordination_maximum_pitch;
    Real m_tilt_coordination_maximum_roll_rate;
    Real m_tilt_coordination_maximum_pitch_rate;
    Real m_euler_rates_washout_high_pass_omega_roll;
    Real m_euler_rates_washout_high_pass_omega_pitch;
    Real m_euler_rates_washout_high_pass_omega_yaw;
    Real m_tilt_coordination_low_pass_omega;
    Real m_tilt_coordination_low_pass_zeta;

  public:
    nlohmann::json ReadJson(std::string const& filename);

    explicit Params();
    explicit Params(utilities::ConfigurationPtr config);
    explicit Params(std::string const& filename);
    explicit Params(nlohmann::json const& j);

    Vector<3> const& GetGravityVector() const noexcept;
    Vector<3> const& GetAccelerationGain() const noexcept;
    Vector<3> const& GetAngularVelocityGain() const noexcept;
    Real GetAccelerationSplitOmegaX() const noexcept;
    Real GetAccelerationSplitOmegaY() const noexcept;
    Real GetAccelerationSplitOmegaZ() const noexcept;
    Real GetAccelerationWashoutHighPassOmegaX() const noexcept;
    Real GetAccelerationWashoutHighPassZetaX() const noexcept;
    Real GetAccelerationWashoutHighPassOmegaY() const noexcept;
    Real GetAccelerationWashoutHighPassZetaY() const noexcept;
    Real GetAccelerationWashoutHighPassOmegaZ() const noexcept;
    Real GetAccelerationWashoutHighPassZetaZ() const noexcept;
    Real GetTiltCoordinationMaximumRoll() const noexcept;
    Real GetTiltCoordinationMaximumPitch() const noexcept;
    Real GetTiltCoordinationMaximumRollRate() const noexcept;
    Real GetTiltCoordinationMaximumPitchRate() const noexcept;
    Real GetEulerRatesWashoutHighPassOmegaRoll() const noexcept;
    Real GetEulerRatesWashoutHighPassOmegaPitch() const noexcept;
    Real GetEulerRatesWashoutHighPassOmegaYaw() const noexcept;
    Real GetTiltCoordinationLowPassOmegaRoll() const noexcept;
    Real GetTiltCoordinationLowPassZetaRoll() const noexcept;
    Real GetTiltCoordinationLowPassOmegaPitch() const noexcept;
    Real GetTiltCoordinationLowPassZetaPitch() const noexcept;
};
}  //namespace mpmca::dokblocks::classic_washout_algorithm