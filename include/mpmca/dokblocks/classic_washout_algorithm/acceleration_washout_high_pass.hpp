/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/dokblocks/classic_washout_algorithm/params.hpp"
#include "mpmca/dokblocks/components/high_pass_second_order.hpp"

namespace mpmca::dokblocks::classic_washout_algorithm::acceleration_washout_high_pass {
inline auto GetBlock(const Params& params)
{
    auto acceleration_washout_high_pass_x =
        HighPassSecondOrder("acceleration_washout_high_pass_x", params.GetAccelerationWashoutHighPassZetaX(),
                            params.GetAccelerationWashoutHighPassOmegaX());
    acceleration_washout_high_pass_x.SetInputSignalNames({"a_x_W"});
    acceleration_washout_high_pass_x.SetOutputSignalNames({"a_x_sim"});

    auto acceleration_washout_high_pass_y =
        HighPassSecondOrder("acceleration_washout_high_pass_y", params.GetAccelerationWashoutHighPassZetaY(),
                            params.GetAccelerationWashoutHighPassOmegaY());
    acceleration_washout_high_pass_y.SetInputSignalNames({"a_y_W"});
    acceleration_washout_high_pass_y.SetOutputSignalNames({"a_y_sim"});

    auto acceleration_washout_high_pass_z =
        HighPassSecondOrder("acceleration_washout_high_pass_z", params.GetAccelerationWashoutHighPassZetaZ(),
                            params.GetAccelerationWashoutHighPassOmegaZ());
    acceleration_washout_high_pass_z.SetInputSignalNames({"a_z_W"});
    acceleration_washout_high_pass_z.SetOutputSignalNames({"a_z_sim"});

    auto HP2 = MakeAppend("f_washout_filter", acceleration_washout_high_pass_x, acceleration_washout_high_pass_y,
                          acceleration_washout_high_pass_z);

    return HP2;
}
}  //namespace mpmca::dokblocks::classic_washout_algorithm::acceleration_washout_high_pass