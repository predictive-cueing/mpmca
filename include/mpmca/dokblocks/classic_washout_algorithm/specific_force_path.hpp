/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/dokblocks/classic_washout_algorithm/acceleration_body_to_world.hpp"
#include "mpmca/dokblocks/classic_washout_algorithm/acceleration_gain.hpp"
#include "mpmca/dokblocks/classic_washout_algorithm/acceleration_split_high_pass.hpp"
#include "mpmca/dokblocks/classic_washout_algorithm/acceleration_washout_high_pass.hpp"
#include "mpmca/dokblocks/classic_washout_algorithm/params.hpp"
#include "mpmca/dokblocks/classic_washout_algorithm/specific_force_to_acceleration.hpp"
#include "mpmca/dokblocks/components/double_integrator.hpp"
#include "mpmca/dokblocks/components/gain_coefficient_wise.hpp"
#include "mpmca/dokblocks/components/integrator.hpp"
#include "mpmca/dokblocks/components/signal_repeat.hpp"
#include "mpmca/dokblocks/components/signal_select.hpp"
#include "mpmca/dokblocks/core/block_diagram.hpp"

namespace mpmca::dokblocks::classic_washout_algorithm::specific_force_path {
inline auto GetBlock(const Params& params)
{
    auto specific_force_to_acceleration = specific_force_to_acceleration::GetBlock(params);
    auto acceleration_gain = acceleration_gain::GetBlock(params);
    auto acceleration_split_high_pass = acceleration_split_high_pass::GetBlock(params);
    auto acceleration_gain_and_split = MakeSeries("acceleration_gain_and_split", specific_force_to_acceleration,
                                                  acceleration_gain, acceleration_split_high_pass);

    auto euler_sim = GainCoefficientWise<3>("euler_sim", 1.);
    euler_sim.SetInputSignalNames({"phi_sim", "theta_sim", "psi_sim"});
    euler_sim.SetOutputSignalNames({"phi_sim", "theta_sim", "psi_sim"});

    auto euler_sim_acceleration_split =
        MakeAppend("euler_sim_acceleration_split", acceleration_gain_and_split, euler_sim);

    auto acceleration_body_to_world = acceleration_body_to_world::GetBlock(params);
    auto acceleration_washout_high_pass = acceleration_washout_high_pass::GetBlock(params);

    auto acceleration_split_and_washout = MakeSeries("acceleration_split_and_washout", euler_sim_acceleration_split,
                                                     acceleration_body_to_world, acceleration_washout_high_pass);

    auto unitary_gain = GainCoefficientWise<3>("unitary_gain", 1.);
    unitary_gain.SetInputSignalNames({"a_x_sim", "a_y_sim", "a_z_sim"});
    unitary_gain.SetOutputSignalNames({"a_x_sim", "a_y_sim", "a_z_sim"});

    auto a_x_integrator = Integrator("a_x_integrator");
    a_x_integrator.SetInputSignalNames({"a_x_sim"});
    a_x_integrator.SetOutputSignalNames({"xd_sim"});
    auto a_y_integrator = Integrator("a_y_integrator");
    a_y_integrator.SetInputSignalNames({"a_y_sim"});
    a_y_integrator.SetOutputSignalNames({"yd_sim"});
    auto a_z_integrator = Integrator("a_z_integrator");
    a_z_integrator.SetInputSignalNames({"a_z_sim"});
    a_z_integrator.SetOutputSignalNames({"zd_sim"});

    auto a_x_double_integrator = DoubleIntegrator("a_x_double_integrator");
    a_x_double_integrator.SetInputSignalNames({"a_x_sim"});
    a_x_double_integrator.SetOutputSignalNames({"x_sim"});

    auto a_y_double_integrator = DoubleIntegrator("a_y_double_integrator");
    a_y_double_integrator.SetInputSignalNames({"a_y_sim"});
    a_y_double_integrator.SetOutputSignalNames({"y_sim"});

    auto a_z_double_integrator = DoubleIntegrator("a_z_double_integrator");
    a_z_double_integrator.SetInputSignalNames({"a_z_sim"});
    a_z_double_integrator.SetOutputSignalNames({"z_sim"});

    auto acceleration_integrators =
        MakeAppend("acceleration_integrators", unitary_gain, a_x_integrator, a_y_integrator, a_z_integrator,
                   a_x_double_integrator, a_y_double_integrator, a_z_double_integrator);

    SignalRepeat<3, acceleration_split_and_washout.NY> acceleration_washout_high_pass_repeat(
        "acceleration_washout_high_pass_repeat", acceleration_washout_high_pass.GetOutputSignalNames());

    auto specific_force_path = MakeSeries("specific_force_path", acceleration_split_and_washout,
                                          acceleration_washout_high_pass_repeat, acceleration_integrators);

    return specific_force_path;
}
}  //namespace mpmca::dokblocks::classic_washout_algorithm::specific_force_path