/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <cmath>
#include <iostream>

#include "mpmca/dokblocks/classic_washout_algorithm/params.hpp"
#include "mpmca/dokblocks/components/algebraic_function.hpp"
#include "mpmca/dokblocks/core/matrix_types.hpp"
#include "mpmca/types/pose_vector.hpp"

namespace mpmca::dokblocks::classic_washout_algorithm::simulator_commands_to_inertial_signals {
inline auto GetBlock(const Params& params)
{
    auto fun = [g = params.GetGravityVector()](const Vector<18>& in) {
        Vector<27> res = Vector<27>::Zero();

        PoseVector pose;
        pose.GetX() = in[6];
        pose.GetY() = in[7];
        pose.GetZ() = in[8];

        pose.GetXd() = in[3];
        pose.GetYd() = in[4];
        pose.GetZd() = in[5];

        pose.GetXdd() = in[0];
        pose.GetYdd() = in[1];
        pose.GetZdd() = in[2];

        pose.GetPhi() = in[9];
        pose.GetTheta() = in[10];
        pose.GetPsi() = in[11];

        pose.GetP() = in[12];
        pose.GetQ() = in[13];
        pose.GetR() = in[14];

        pose.GetPd() = in[15];
        pose.GetQd() = in[16];
        pose.GetRd() = in[17];

        res.segment<18>(0) = pose;
        res.segment<3>(18 + 0) = pose.AccelerationsToSpecificForces(g);
        res.segment<3>(18 + 3) = pose.EulerRatesToAngularVelocities();
        res.segment<3>(18 + 6) = pose.EulerAccelerationsToAngularAccelerations();

        return res;
    };

    auto result = AlgebraicFunction<18, 27>("simulator_commands_to_inertial_signals", fun);

    result.SetInputSignalNames({
        "a_x_sim",  // 0
        "a_y_sim",  // 1
        "a_z_sim",  // 2
        "xd_sim",  // 3
        "yd_sim",  // 4
        "zd_sim",  // 5
        "x_sim",  // 6
        "y_sim",  // 7
        "z_sim",  // 8
        "phi_sim",  // 9
        "theta_sim",  // 10
        "psi_sim",  // 11
        "p_sim",  // 12
        "q_sim",  // 13
        "r_sim",  // 14
        "pd_sim",  // 15
        "qd_sim",  // 16
        "rd_sim"  // 17
    });
    result.SetOutputSignalNames({
        "x_sim",  // 0
        "y_sim",  // 1
        "z_sim",  // 2
        "phi_sim",  // 3
        "theta_sim",  // 4
        "psi_sim",  // 5
        "xd_sim",  // 6
        "yd_sim",  // 7
        "zd_sim",  // 8
        "p_sim",  // 9
        "q_sim",  // 10
        "r_sim",  // 11
        "a_x_sim",  // 12
        "a_y_sim",  // 13
        "a_z_sim",  // 14
        "pd_sim",  // 15
        "qd_sim",  // 16
        "rd_sim",  // 17
        "f_x_cwa",  // 18
        "f_y_cwa",  // 19
        "f_z_cwa",  // 20
        "omega_x_cwa",  // 21
        "omega_y_cwa",  // 22
        "omega_z_cwa",  // 23
        "alpha_x_cwa",  // 24
        "alpha_y_cwa",  // 25
        "alpha_z_cwa"  // 26
    });

    return result;
}
}  //namespace mpmca::dokblocks::classic_washout_algorithm::simulator_commands_to_inertial_signals