/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/dokblocks/classic_washout_algorithm/params.hpp"
#include "mpmca/dokblocks/components/low_pass_second_order.hpp"

namespace mpmca::dokblocks::classic_washout_algorithm::tilt_coordination_low_pass {
inline auto GetBlock(const Params& params)
{
    auto tilt_coordination_low_pass_roll =
        LowPassSecondOrder("tilt_coordination_low_pass_roll", params.GetTiltCoordinationLowPassZetaRoll(),
                           params.GetTiltCoordinationLowPassOmegaRoll());
    tilt_coordination_low_pass_roll.SetOutputSignalNames({"phi_tc"});

    auto tilt_coordination_low_pass_pitch =
        LowPassSecondOrder("tilt_coordination_low_pass_pitch", params.GetTiltCoordinationLowPassZetaPitch(),
                           params.GetTiltCoordinationLowPassOmegaPitch());
    tilt_coordination_low_pass_pitch.SetOutputSignalNames({"theta_tc"});

    auto tilt_coordination_low_pass =
        MakeAppend("tilt_coordination_low_pass", tilt_coordination_low_pass_roll, tilt_coordination_low_pass_pitch);

    return tilt_coordination_low_pass;
}
}  //namespace mpmca::dokblocks::classic_washout_algorithm::tilt_coordination_low_pass