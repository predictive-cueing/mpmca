/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/dokblocks/classic_washout_algorithm/angular_velocity_path.hpp"
#include "mpmca/dokblocks/classic_washout_algorithm/params.hpp"
#include "mpmca/dokblocks/classic_washout_algorithm/simulator_commands_to_inertial_signals.hpp"
#include "mpmca/dokblocks/classic_washout_algorithm/specific_force_path.hpp"
#include "mpmca/dokblocks/classic_washout_algorithm/tilt_coordination_path.hpp"
#include "mpmca/dokblocks/components/differentiator.hpp"
#include "mpmca/dokblocks/components/double_differentiator.hpp"
#include "mpmca/dokblocks/components/signal_repeat.hpp"
#include "mpmca/dokblocks/core/block_diagram.hpp"

namespace mpmca::dokblocks::classic_washout_algorithm::cwa {
inline auto GetBlock(const Params& params)
{
    auto specific_force_path = specific_force_path::GetBlock(params);
    auto tilt_coordination_path = tilt_coordination_path::GetBlock(params);
    auto angular_velocity_path = angular_velocity_path::GetBlock(params);

    auto sum_tilt_and_angular_velocity =
        MakeSum("sum_tilt_and_angular_velocity", tilt_coordination_path, angular_velocity_path);
    sum_tilt_and_angular_velocity.SetOutputSignalNames({"phi_sim", "theta_sim", "psi_sim"});

    auto unitary_gain = GainCoefficientWise<3>("unitary_gain", 1.);
    unitary_gain.SetInputSignalNames({"phi_sim", "theta_sim", "psi_sim"});
    unitary_gain.SetOutputSignalNames({"phi_sim", "theta_sim", "psi_sim"});

    auto diff_phi = Differentiator("diff_phi", 100.);
    diff_phi.SetInputSignalNames({"phi_sim"});
    diff_phi.SetOutputSignalNames({"p_sim"});
    auto diff_phi_2 = Differentiator("diff_phi_2", 100.);
    auto double_diff_phi = MakeSeries("double_diff_phi", diff_phi_2, diff_phi_2);
    double_diff_phi.SetInputSignalNames({"phi_sim"});
    double_diff_phi.SetOutputSignalNames({"pd_sim"});

    auto diff_theta = Differentiator("diff_theta", 100.);
    diff_theta.SetInputSignalNames({"theta_sim"});
    diff_theta.SetOutputSignalNames({"q_sim"});
    auto diff_theta_2 = Differentiator("diff_theta_2", 100.);
    auto double_diff_theta = MakeSeries("double_diff_psi", diff_theta_2, diff_theta_2);
    double_diff_theta.SetInputSignalNames({"theta_sim"});
    double_diff_theta.SetOutputSignalNames({"qd_sim"});

    auto diff_psi = Differentiator("diff_psi", 100.);
    diff_psi.SetInputSignalNames({"psi_sim"});
    diff_psi.SetOutputSignalNames({"r_sim"});
    auto diff_psi_2 = Differentiator("diff_theta_2", 100.);
    auto double_diff_psi = MakeSeries("double_diff_psi", diff_psi_2, diff_psi_2);
    double_diff_psi.SetInputSignalNames({"psi_sim"});
    double_diff_psi.SetOutputSignalNames({"rd_sim"});

    auto euler_angle_differentiators = MakeAppend("euler_angle_differentiators", unitary_gain, diff_phi, diff_theta,
                                                  diff_psi, double_diff_phi, double_diff_theta, double_diff_psi);

    auto tilt_and_angular_velocity_and_differentiators =
        MakeSeries("tilt_and_angular_velocity_and_differentiators", sum_tilt_and_angular_velocity,
                   SignalRepeat<3, sum_tilt_and_angular_velocity.NY>(
                       "sum_tilt_and_angular_velocity_repeat", sum_tilt_and_angular_velocity.GetOutputSignalNames()),
                   euler_angle_differentiators);

    auto classic_washout_algorithm =
        MakeAppend("classic_washout_algorithm", specific_force_path, tilt_and_angular_velocity_and_differentiators);

    auto inertial_output_signals = simulator_commands_to_inertial_signals::GetBlock(params);

    return MakeSeries("classic_washout_algorithm", classic_washout_algorithm, inertial_output_signals);
}
}  //namespace mpmca::dokblocks::classic_washout_algorithm::cwa