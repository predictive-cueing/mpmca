/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <cmath>

#include "mpmca/dokblocks/classic_washout_algorithm/params.hpp"
#include "mpmca/dokblocks/components/algebraic_function.hpp"
#include "mpmca/dokblocks/core/matrix_types.hpp"

namespace mpmca::dokblocks::classic_washout_algorithm::acceleration_body_to_world {
inline auto GetBlock(const Params& params)
{
    auto fun = [g = params.GetGravityVector()](const Vector<6>& in) {
        Vector<3> acceleration_body = in.topRows<3>();
        Vector<3> euler_angles = in.bottomRows<3>();

        Real phi = euler_angles[0];
        Real theta = euler_angles[1];
        Real psi = euler_angles[2];
        Real cos_phi = std::cos(phi);
        Real sin_phi = std::sin(phi);
        Real cos_theta = std::cos(theta);
        Real sin_theta = std::sin(theta);
        Real cos_psi = std::cos(psi);
        Real sin_psi = std::sin(psi);

        Real a0 = (cos_theta * cos_psi) * acceleration_body[0] +
                  (sin_phi * sin_theta * cos_psi - cos_phi * sin_psi) * acceleration_body[1] +
                  (cos_phi * sin_theta * cos_psi + sin_phi * sin_psi) * acceleration_body[2];
        Real a1 = (cos_theta * sin_psi) * acceleration_body[0] +
                  (sin_phi * sin_theta * sin_psi + cos_phi * cos_psi) * acceleration_body[1] +
                  (cos_phi * sin_theta * sin_psi - sin_phi * cos_psi) * acceleration_body[2];
        Real a2 = (-sin_theta) * acceleration_body[0] + (sin_phi * cos_theta) * acceleration_body[1] +
                  (cos_phi * cos_theta) * acceleration_body[2];

        Vector<3> acceleration_world;
        acceleration_world << a0, a1, a2;
        return acceleration_world;
    };

    auto result = AlgebraicFunction<6, 3>("acceleration_body_to_world", fun);

    result.SetInputSignalNames({"a_x_B", "a_y_B", "a_z_B", "phi_sim", "theta_sim", "psi_sim"});
    result.SetOutputSignalNames({"a_x_W", "a_y_W", "a_z_W"});

    return result;
}
}  //namespace mpmca::dokblocks::classic_washout_algorithm::acceleration_body_to_world