/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/dokblocks/classic_washout_algorithm/params.hpp"
#include "mpmca/dokblocks/components/low_pass_first_order.hpp"

namespace mpmca::dokblocks::classic_washout_algorithm::acceleration_split_low_pass {
inline auto GetBlock(const Params& params)
{
    auto acceleration_split_low_pass_x =
        LowPassFirstOrder("acceleration_split_low_pass_x", params.GetAccelerationSplitOmegaX());
    auto acceleration_split_low_pass_y =
        LowPassFirstOrder("acceleration_split_low_pass_y", params.GetAccelerationSplitOmegaY());

    return MakeAppend("acceleration_split_low_pass", acceleration_split_low_pass_x, acceleration_split_low_pass_y);
}
}  //namespace mpmca::dokblocks::classic_washout_algorithm::acceleration_split_low_pass