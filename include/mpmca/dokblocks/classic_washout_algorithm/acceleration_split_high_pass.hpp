/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/dokblocks/classic_washout_algorithm/params.hpp"
#include "mpmca/dokblocks/components/high_pass_first_order.hpp"

namespace mpmca::dokblocks::classic_washout_algorithm::acceleration_split_high_pass {
inline auto GetBlock(const Params& params)
{
    auto acceleration_split_high_pass_x =
        HighPassFirstOrder("acceleration_split_high_pass_x", params.GetAccelerationSplitOmegaX());
    acceleration_split_high_pass_x.SetInputSignalNames({"a_x_K"});
    acceleration_split_high_pass_x.SetOutputSignalNames({"a_x_B"});

    auto acceleration_split_high_pass_y =
        HighPassFirstOrder("acceleration_split_high_pass_y", params.GetAccelerationSplitOmegaY());
    acceleration_split_high_pass_y.SetInputSignalNames({"a_y_K"});
    acceleration_split_high_pass_y.SetOutputSignalNames({"a_y_B"});

    auto acceleration_split_high_pass_z =
        HighPassFirstOrder("acceleration_split_high_pass_z", params.GetAccelerationSplitOmegaZ());

    acceleration_split_high_pass_z.SetInputSignalNames({"a_z_K"});
    acceleration_split_high_pass_z.SetOutputSignalNames({"a_z_B"});

    auto acceleration_split_high_pass = MakeAppend("acceleration_split_high_pass", acceleration_split_high_pass_x,
                                                   acceleration_split_high_pass_y, acceleration_split_high_pass_z);

    return acceleration_split_high_pass;
}
}  //namespace mpmca::dokblocks::classic_washout_algorithm::acceleration_split_high_pass