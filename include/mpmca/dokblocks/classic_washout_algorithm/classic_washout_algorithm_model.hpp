/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/dokblocks/classic_washout_algorithm/classic_washout_algorithm.hpp"
#include "mpmca/dokblocks/classic_washout_algorithm/params.hpp"
#include "mpmca/dokblocks/core/input_output_adaptor.hpp"

namespace mpmca::dokblocks::classic_washout_algorithm::model {
inline auto GetBlock(const Params& params)
{
    auto result = InputOutputAdaptor("classic_washout_algorithm_model", cwa::GetBlock(params));
    return result;
}
}  //namespace mpmca::dokblocks::classic_washout_algorithm::model