/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/dokblocks/classic_washout_algorithm/params.hpp"
#include "mpmca/dokblocks/components/integrator.hpp"
#include "mpmca/dokblocks/core/append.hpp"

namespace mpmca::dokblocks::classic_washout_algorithm::euler_rates_integrator {
inline auto GetBlock(const Params& params)
{
    auto p_integrator = Integrator("p_integrator");
    p_integrator.SetInputSignalNames({"p_HP"});
    p_integrator.SetOutputSignalNames({"phi_w"});

    auto q_integrator = Integrator("q_integrator");
    q_integrator.SetInputSignalNames({"q_HP"});
    q_integrator.SetOutputSignalNames({"theta_w"});

    auto r_integrator = Integrator("r_integrator");
    r_integrator.SetInputSignalNames({"r_HP"});
    r_integrator.SetOutputSignalNames({"psi_w"});

    auto euler_rates_integrator = MakeAppend("euler_rates_integrator", p_integrator, q_integrator, r_integrator);
    return euler_rates_integrator;
}
}  //namespace mpmca::dokblocks::classic_washout_algorithm::euler_rates_integrator