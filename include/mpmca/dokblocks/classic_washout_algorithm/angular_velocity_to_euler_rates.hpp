/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <cmath>

#include "mpmca/dokblocks/classic_washout_algorithm/params.hpp"
#include "mpmca/dokblocks/components/algebraic_function.hpp"
#include "mpmca/dokblocks/core/matrix_types.hpp"

namespace mpmca::dokblocks::classic_washout_algorithm::angular_velocity_to_euler_rates {
inline auto GetBlock(const Params& params)
{
    auto fun = [](const Vector<6>& in) {
        Vector<3> angular_velocity_body = in.topRows<3>();
        Vector<3> euler_angles = in.bottomRows<3>();

        Real phi = euler_angles[0];
        Real theta = euler_angles[1];
        Real cos_phi = std::cos(phi);
        Real sin_phi = std::sin(phi);
        Real cos_theta = std::cos(theta);
        Real tan_theta = std::tan(theta);
        Real sec_theta = 1. / cos_theta;

        Real p = 1 * angular_velocity_body[0] + sin_phi * tan_theta * angular_velocity_body[1] +
                 cos_phi * tan_theta * angular_velocity_body[2];
        Real q = 0 * angular_velocity_body[0] + cos_phi * angular_velocity_body[1] - sin_phi * angular_velocity_body[2];
        Real r = 0 * angular_velocity_body[0] + sin_phi * sec_theta * angular_velocity_body[1] +
                 cos_phi * sec_theta * angular_velocity_body[2];

        Vector<3> euler_rates;
        euler_rates << p, q, r;
        return euler_rates;
    };

    auto result = AlgebraicFunction<6, 3>("angular_velocity_to_euler_rates", fun);
    result.SetInputSignalNames({"omega_x_K", "omega_y_K", "omega_z_K", "phi_vehicle", "theta_vehicle", "psi_vehicle"});
    result.SetOutputSignalNames({"p", "q", "r"});
    return result;
}
}  //namespace mpmca::dokblocks::classic_washout_algorithm::angular_velocity_to_euler_rates