/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/dokblocks/classic_washout_algorithm/params.hpp"
#include "mpmca/dokblocks/components/high_pass_first_order.hpp"
#include "mpmca/dokblocks/core/append.hpp"

namespace mpmca::dokblocks::classic_washout_algorithm::euler_rates_washout_high_pass {
inline auto GetBlock(const Params& params)
{
    auto euler_rates_washout_high_pass_p =
        HighPassFirstOrder("euler_rates_washout_high_pass_p", params.GetEulerRatesWashoutHighPassOmegaRoll());
    euler_rates_washout_high_pass_p.SetInputSignalNames({"p"});
    euler_rates_washout_high_pass_p.SetOutputSignalNames({"p_HP"});

    auto euler_rates_washout_high_pass_q =
        HighPassFirstOrder("euler_rates_washout_high_pass_q", params.GetEulerRatesWashoutHighPassOmegaPitch());
    euler_rates_washout_high_pass_q.SetInputSignalNames({"q"});
    euler_rates_washout_high_pass_q.SetOutputSignalNames({"q_HP"});

    auto euler_rates_washout_high_pass_r =
        HighPassFirstOrder("euler_rates_washout_high_pass_r", params.GetEulerRatesWashoutHighPassOmegaYaw());
    euler_rates_washout_high_pass_r.SetInputSignalNames({"r"});
    euler_rates_washout_high_pass_r.SetOutputSignalNames({"r_HP"});

    auto euler_rates_washout_high_pass = MakeAppend("euler_rates_washout_high_pass", euler_rates_washout_high_pass_p,
                                                    euler_rates_washout_high_pass_q, euler_rates_washout_high_pass_r);
    return euler_rates_washout_high_pass;
}
}  //namespace mpmca::dokblocks::classic_washout_algorithm::euler_rates_washout_high_pass