/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <cmath>

#include "mpmca/dokblocks/classic_washout_algorithm/params.hpp"
#include "mpmca/dokblocks/components/algebraic_function.hpp"
#include "mpmca/dokblocks/core/matrix_types.hpp"

namespace mpmca::dokblocks::classic_washout_algorithm::specific_force_to_acceleration {
inline auto GetBlock(const Params& params)
{
    auto fun = [gravity_world = params.GetGravityVector()](const Vector<6>& in) {
        Vector<3> specific_force_body = in.topRows<3>();
        Vector<3> euler_angles = in.bottomRows<3>();

        Real phi = euler_angles[0];
        Real theta = euler_angles[1];
        Real psi = euler_angles[2];
        Real cos_phi = std::cos(phi);
        Real sin_phi = std::sin(phi);
        Real cos_theta = std::cos(theta);
        Real sin_theta = std::sin(theta);
        Real cos_psi = std::cos(psi);
        Real sin_psi = std::sin(psi);

        Real gravity_x = (cos_theta * cos_psi) * gravity_world[0] +  // = 0
                         (cos_theta * sin_psi) * gravity_world[1] +  // = 0
                         (-sin_theta) * gravity_world[2];
        Real gravity_y = (sin_phi * sin_theta * cos_psi - cos_phi * sin_psi) * gravity_world[0] +  // = 0
                         (sin_phi * sin_theta * sin_psi + cos_phi * cos_psi) * gravity_world[1] +  // = 0
                         (sin_phi * cos_theta) * gravity_world[2];
        Real gravity_z = (cos_phi * sin_theta * cos_psi + sin_phi * sin_psi) * gravity_world[0] +  // = 0
                         (cos_phi * sin_theta * sin_psi - sin_phi * cos_psi) * gravity_world[1] +  // = 0
                         (cos_phi * cos_theta) * gravity_world[2];
        Vector<3> gravity_body;
        gravity_body << gravity_x, gravity_y, gravity_z;

        Vector<3> acceleration_body = -specific_force_body + gravity_body;

        return acceleration_body;
    };

    auto result = AlgebraicFunction<6, 3>("specific_force_to_acceleration", fun);
    result.SetInputSignalNames({"f_x", "f_y", "f_z", "phi_vehicle", "theta_vehicle", "psi_vehicle"});
    result.SetOutputSignalNames({"a_x", "a_y", "a_z"});
    return result;
}
}  //namespace mpmca::dokblocks::classic_washout_algorithm::specific_force_to_acceleration