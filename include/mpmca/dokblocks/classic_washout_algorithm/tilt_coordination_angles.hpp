/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <algorithm>
#include <cmath>

#include "mpmca/dokblocks/classic_washout_algorithm/params.hpp"
#include "mpmca/dokblocks/components/algebraic_function.hpp"

namespace mpmca::dokblocks::classic_washout_algorithm::tilt_coordination_angles {
inline auto GetBlock(const Params& params)
{
    auto fun = [gravity_world = params.GetGravityVector()](const Vector<2>& in) {
        Real gravity_norm = gravity_world.norm();
        Real acceleration_x = in(0);
        Real acceleration_y = in(1);

        Real theta_tilt_coordination = std::asin(std::max(-1., std::min(1., acceleration_x / gravity_norm)));
        Real phi_tilt_coordination = std::asin(
            std::max(-1.0, std::min(1.0, -acceleration_y / (gravity_norm * std::cos(theta_tilt_coordination)))));

        return (Vector<2>() << phi_tilt_coordination, theta_tilt_coordination).finished();
    };

    return AlgebraicFunction<2, 2>("tilt_coordination_angles", fun);
}
}  //namespace mpmca::dokblocks::classic_washout_algorithm::tilt_coordination_angles