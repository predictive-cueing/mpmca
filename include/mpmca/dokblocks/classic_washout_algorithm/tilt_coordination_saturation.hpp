/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <algorithm>

#include "mpmca/dokblocks/classic_washout_algorithm/params.hpp"
#include "mpmca/dokblocks/components/algebraic_function.hpp"

namespace mpmca::dokblocks::classic_washout_algorithm::tilt_coordination_saturation {
inline auto GetBlock(const Params& params)
{
    auto fun = [tilt_coordination_max_roll = params.GetTiltCoordinationMaximumRoll(),
                tilt_coordination_max_pitch = params.GetTiltCoordinationMaximumPitch()](const Vector<2>& in) {
        Real tilt_coordination_roll = in(0);
        Real tilt_coordination_pitch = in(1);

        Real tilt_coordination_roll_limited =
            std::max(-tilt_coordination_max_roll, std::min(tilt_coordination_max_roll, tilt_coordination_roll));
        Real tilt_coordination_pitch_limited =
            std::max(-tilt_coordination_max_pitch, std::min(tilt_coordination_max_pitch, tilt_coordination_pitch));

        return (Vector<2>() << tilt_coordination_roll_limited, tilt_coordination_pitch_limited).finished();
    };

    return AlgebraicFunction<2, 2>("tilt_coordination_saturation", fun);
}
}  //namespace mpmca::dokblocks::classic_washout_algorithm::tilt_coordination_saturation