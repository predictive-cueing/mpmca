/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/dokblocks/classic_washout_algorithm/params.hpp"
#include "mpmca/dokblocks/components/gain_coefficient_wise.hpp"

namespace mpmca::dokblocks::classic_washout_algorithm::angular_velocity_gain {
inline auto GetBlock(const Params& params)
{
    auto result = GainCoefficientWise<3>("angular_velocity_gain", params.GetAngularVelocityGain());
    result.SetInputSignalNames({"omega_x", "omega_y", "omega_z"});
    result.SetOutputSignalNames({"omega_x_K", "omega_y_K", "omega_z_K"});
    return result;
}
}  //namespace mpmca::dokblocks::classic_washout_algorithm::angular_velocity_gain