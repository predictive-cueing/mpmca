/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/dokblocks/classic_washout_algorithm/angular_velocity_gain.hpp"
#include "mpmca/dokblocks/classic_washout_algorithm/angular_velocity_to_euler_rates.hpp"
#include "mpmca/dokblocks/classic_washout_algorithm/euler_rates_integrator.hpp"
#include "mpmca/dokblocks/classic_washout_algorithm/euler_rates_washout_high_pass.hpp"
#include "mpmca/dokblocks/classic_washout_algorithm/params.hpp"
#include "mpmca/dokblocks/components/gain_coefficient_wise.hpp"
#include "mpmca/dokblocks/core/block_diagram.hpp"

namespace mpmca::dokblocks ::classic_washout_algorithm::angular_velocity_path {
inline auto GetBlock(const Params& params)
{
    auto angular_velocity_gain = angular_velocity_gain::GetBlock(params);
    auto euler_vehicle = GainCoefficientWise<3>("euler_vehicle", 1.);
    euler_vehicle.SetInputSignalNames({"phi_vehicle", "theta_vehicle", "psi_vehicle"});
    euler_vehicle.SetOutputSignalNames({"phi_vehicle", "theta_vehicle", "psi_vehicle"});

    auto angular_velocity_euler_angle_append =
        MakeAppend("angular_velocity_euler_angle_append", angular_velocity_gain, euler_vehicle);

    auto angular_velocity_to_euler_rates = angular_velocity_to_euler_rates::GetBlock(params);
    auto euler_rates_washout_high_pass = euler_rates_washout_high_pass::GetBlock(params);
    auto euler_rates_integrator = euler_rates_integrator::GetBlock(params);

    auto angular_velocity_path =
        MakeSeries("angular_velocity_path", angular_velocity_euler_angle_append, angular_velocity_to_euler_rates,
                   euler_rates_washout_high_pass, euler_rates_integrator);

    return angular_velocity_path;
}
}  //namespace mpmca::dokblocks::classic_washout_algorithm::angular_velocity_path