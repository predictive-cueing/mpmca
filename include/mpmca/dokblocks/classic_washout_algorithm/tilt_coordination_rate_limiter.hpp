/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/dokblocks/classic_washout_algorithm/params.hpp"
#include "mpmca/dokblocks/components/rate_limiter.hpp"
#include "mpmca/dokblocks/core/matrix_types.hpp"

namespace mpmca::dokblocks::classic_washout_algorithm::tilt_coordination_rate_limiter {
inline auto GetBlock(const Params& params)
{
    auto tilt_coordination_rate_limiter_roll =
        RateLimiter("tilt_coordination_rate_limiter_roll", -params.GetTiltCoordinationMaximumRollRate(),
                    params.GetTiltCoordinationMaximumRollRate());
    auto tilt_coordination_rate_limiter_pitch =
        RateLimiter("tilt_coordination_rate_limiter_pitch", -params.GetTiltCoordinationMaximumPitchRate(),
                    params.GetTiltCoordinationMaximumPitchRate());
    auto tilt_coordination_rate_limiter = MakeAppend(
        "tilt_coordination_rate_limiter", tilt_coordination_rate_limiter_roll, tilt_coordination_rate_limiter_pitch);

    return tilt_coordination_rate_limiter;
}
}  //namespace mpmca::dokblocks::classic_washout_algorithm::tilt_coordination_rate_limiter