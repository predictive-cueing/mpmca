/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/dokblocks/classic_washout_algorithm/acceleration_gain.hpp"
#include "mpmca/dokblocks/classic_washout_algorithm/acceleration_split_low_pass.hpp"
#include "mpmca/dokblocks/classic_washout_algorithm/params.hpp"
#include "mpmca/dokblocks/classic_washout_algorithm/specific_force_to_acceleration.hpp"
#include "mpmca/dokblocks/classic_washout_algorithm/tilt_coordination_angles.hpp"
#include "mpmca/dokblocks/classic_washout_algorithm/tilt_coordination_low_pass.hpp"
#include "mpmca/dokblocks/classic_washout_algorithm/tilt_coordination_rate_limiter.hpp"
#include "mpmca/dokblocks/classic_washout_algorithm/tilt_coordination_saturation.hpp"
#include "mpmca/dokblocks/components/constant.hpp"
#include "mpmca/dokblocks/components/differentiator.hpp"
#include "mpmca/dokblocks/components/rate_limiter.hpp"
#include "mpmca/dokblocks/components/signal_select.hpp"
#include "mpmca/dokblocks/core/block_diagram.hpp"

namespace mpmca::dokblocks::classic_washout_algorithm::tilt_coordination_path {
inline auto GetBlock(const Params& params)
{
    auto specific_force_to_acceleration = specific_force_to_acceleration::GetBlock(params);
    auto acceleration_gain = acceleration_gain::GetBlock(params);
    auto select_acceleration_x_y = SignalSelect<0, 2, 3>("select_acceleration_x_y");
    auto gain_and_select_acceleration =
        MakeSeries("gain_and_select_acceleration", acceleration_gain, select_acceleration_x_y);

    auto acceleration_split_low_pass = acceleration_split_low_pass::GetBlock(params);
    auto tilt_coordination_angles = tilt_coordination_angles::GetBlock(params);
    auto tilt_coordination_saturation = tilt_coordination_saturation::GetBlock(params);

    auto tilt_coordination_rate_limiter = tilt_coordination_rate_limiter::GetBlock(params);
    auto tilt_coordination_low_pass = tilt_coordination_low_pass::GetBlock(params);

    auto tilt_coordination_roll_pitch =
        MakeSeries("tilt_coordination_roll_pitch", specific_force_to_acceleration, gain_and_select_acceleration,
                   acceleration_split_low_pass, tilt_coordination_angles, tilt_coordination_saturation,
                   tilt_coordination_rate_limiter, tilt_coordination_low_pass);

    auto tilt_coordination_yaw = Constant("tilt_coordination_yaw", 0.);

    auto tilt_coordination_path =
        MakeAppend("tilt_coordination_path", tilt_coordination_roll_pitch, tilt_coordination_yaw);

    return tilt_coordination_path;
}
}  //namespace mpmca::dokblocks::classic_washout_algorithm::tilt_coordination_path