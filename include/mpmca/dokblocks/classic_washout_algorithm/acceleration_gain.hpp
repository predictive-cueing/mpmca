/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/dokblocks/classic_washout_algorithm/params.hpp"
#include "mpmca/dokblocks/components/algebraic_function.hpp"

namespace mpmca::dokblocks::classic_washout_algorithm::acceleration_gain {
inline auto GetBlock(const Params& params)
{
    auto function = [acceleration_gain = params.GetAccelerationGain()](const Vector<3>& acceleration) {
        Vector<3> a_k = acceleration.cwiseProduct(acceleration_gain);
        return a_k;
    };
    auto result = AlgebraicFunction<3, 3>("acceleration_gain", function);

    result.SetInputSignalNames({"a_x", "a_y", "a_z"});
    result.SetOutputSignalNames({"a_x_K", "a_y_K", "a_z_K"});
    return result;
}
}  //namespace mpmca::dokblocks::classic_washout_algorithm::acceleration_gain