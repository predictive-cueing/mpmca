/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/json.hpp"
#include "mpmca/linear_algebra.hpp"

namespace Eigen {

template <typename T, int Rows, int Cols, int Options>
inline void to_json(nlohmann::json& jsn, Matrix<T, Rows, Cols, Options> const& v)
{
    jsn = nlohmann::json::array();

    if (Rows == 1)  // row vector
    {
        for (int j = 0; j < v.cols(); ++j)
            jsn.push_back(v(0, j));
    }
    else if (Cols == 1)  // column vector
    {
        for (int j = 0; j < v.rows(); ++j)
            jsn.push_back(v(j, 0));
    }
    else  // matrix
    {
        for (int i = 0; i < v.rows(); ++i) {
            nlohmann::json row = nlohmann::json::array();

            for (int j = 0; j < v.cols(); ++j)
                row.push_back(v(i, j));

            jsn.push_back(row);
        }
    }
}

template <typename T, int Rows, int Cols, int Options>
inline void from_json(nlohmann::json const& j, Matrix<T, Rows, Cols, Options>& v)
{
    if (Rows == 1 || Cols == 1) {
        if ((int)v.size() != (int)j.size())
            throw std::invalid_argument("Number of elements (" + std::to_string(j.size()) +
                                        ") in json field does not match vector size (" + std::to_string(v.size()) +
                                        ").");

        int ii = 0;
        for (auto const& val : j) {
            T numeric_val{};

            if (val.is_array() && val.size() == 1)
                numeric_val = val.at(0);
            else if (val.is_number())
                numeric_val = val;
            else
                throw std::invalid_argument("All elements of a vector to be converted from json must be a number.");

            if (Rows == 1)
                v(0, ii) = numeric_val;
            else
                v(ii, 0) = numeric_val;

            ++ii;
        }
    }
    else  // matrix
    {
        if (j.is_array() && j.size() == Rows) {
            for (int ii = 0; ii < Rows; ++ii) {
                if (j.at(ii).is_array() && j.at(ii).size() == Cols) {
                    for (int jj = 0; jj < Cols; ++jj) {
                        if (j.at(ii).at(jj).is_number())
                            v(ii, jj) = j.at(ii).at(jj);
                        else
                            throw std::invalid_argument(
                                "All elements of a matrix to be converted from json must be a number. " +
                                std::string("The problematic value is: ") + j.at(ii).at(jj).dump());
                    }
                }
            }
        }
        else
            throw std::invalid_argument("Cannot convert Json input (" + j.dump() + " into an Eigen matrix of size [ " +
                                        std::to_string(Rows) + ", " + std::to_string(Cols) + "].");
    }
}
}  //namespace Eigen