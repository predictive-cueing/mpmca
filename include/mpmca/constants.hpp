/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

namespace mpmca::constants {
// as in math.h
static constexpr double kE = 2.7182818284590452354; /* M_E = e */
static constexpr double kLog2e = 1.4426950408889634074; /* M_LOG2E = log_2 e */
static constexpr double kLog10e = 0.43429448190325182765; /* M_LOG10E = log_10 e */
static constexpr double kLn2 = 0.69314718055994530942; /* M_LN2 = log_e 2 */
static constexpr double kLn10 = 2.30258509299404568402; /* M_LN10 = log_e 10 */
static constexpr double kPi = 3.14159265358979323846; /* M_PI = pi */
static constexpr double kPi_2 = 1.57079632679489661923; /* M_PI_2 = pi/2 */
static constexpr double kPi_4 = 0.78539816339744830962; /* M_PI_4 = pi/4 */
static constexpr double k1_pi = 0.31830988618379067154; /* M_1_PI = 1/pi */
static constexpr double k2_pi = 0.63661977236758134308; /* M_2_PI = 2/pi */
static constexpr double k2_sqrtpi = 1.12837916709551257390; /* M_2_SQRTPI = 2/sqrt(pi) */
static constexpr double kSqrt2 = 1.41421356237309504880; /* M_SQRT2 = sqrt(2) */
static constexpr double kSqrt1_2 = 0.70710678118654752440; /* M_SQRT1_2 = 1/sqrt(2) */

}  //namespace mpmca::constants