/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/linear_algebra.hpp"

namespace mpmca {

class PoseRotMatVector;
class TransformationMatrix;
class RotationMatrix;

class PoseVector : public Vector<18>
{
  public:
    inline double GetX() const { return this->Vector<18>::operator[](0); }
    inline double& GetX() { return this->Vector<18>::operator[](0); }
    inline double GetY() const { return this->Vector<18>::operator[](1); }
    inline double& GetY() { return this->Vector<18>::operator[](1); }
    inline double GetZ() const { return this->Vector<18>::operator[](2); }
    inline double& GetZ() { return this->Vector<18>::operator[](2); }

    inline double GetPhi() const { return this->Vector<18>::operator[](3); }
    inline double& GetPhi() { return this->Vector<18>::operator[](3); }
    inline double GetTheta() const { return this->Vector<18>::operator[](4); }
    inline double& GetTheta() { return this->Vector<18>::operator[](4); }
    inline double GetPsi() const { return this->Vector<18>::operator[](5); }
    inline double& GetPsi() { return this->Vector<18>::operator[](5); }

    inline double GetXd() const { return this->Vector<18>::operator[](6); }
    inline double& GetXd() { return this->Vector<18>::operator[](6); }
    inline double GetYd() const { return this->Vector<18>::operator[](7); }
    inline double& GetYd() { return this->Vector<18>::operator[](7); }
    inline double GetZd() const { return this->Vector<18>::operator[](8); }
    inline double& GetZd() { return this->Vector<18>::operator[](8); }

    inline double GetP() const { return this->Vector<18>::operator[](9); }
    inline double& GetP() { return this->Vector<18>::operator[](9); }
    inline double GetQ() const { return this->Vector<18>::operator[](10); }
    inline double& GetQ() { return this->Vector<18>::operator[](10); }
    inline double GetR() const { return this->Vector<18>::operator[](11); }
    inline double& GetR() { return this->Vector<18>::operator[](11); }

    inline double GetXdd() const { return this->Vector<18>::operator[](12); }
    inline double& GetXdd() { return this->Vector<18>::operator[](12); }
    inline double GetYdd() const { return this->Vector<18>::operator[](13); }
    inline double& GetYdd() { return this->Vector<18>::operator[](13); }
    inline double GetZdd() const { return this->Vector<18>::operator[](14); }
    inline double& GetZdd() { return this->Vector<18>::operator[](14); }

    inline double GetPd() const { return this->Vector<18>::operator[](15); }
    inline double& GetPd() { return this->Vector<18>::operator[](15); }
    inline double GetQd() const { return this->Vector<18>::operator[](16); }
    inline double& GetQd() { return this->Vector<18>::operator[](16); }
    inline double GetRd() const { return this->Vector<18>::operator[](17); }
    inline double& GetRd() { return this->Vector<18>::operator[](17); }

    PoseVector(void)
        : Vector<18>()
    {
    }

    // This constructor allows you to construct PoseVector from Eigen
    // expressions
    template <typename OtherDerived>
    PoseVector(const Eigen::MatrixBase<OtherDerived>& other)
        : Vector<18>(other)
    {
    }

    // This method allows you to assign Eigen expressions to PoseVector
    template <typename OtherDerived>
    PoseVector& operator=(const Eigen::MatrixBase<OtherDerived>& other)
    {
        this->Vector<18>::operator=(other);
        return *this;
    }

    PoseRotMatVector ToPoseRotMatVector() const;
    TransformationMatrix GetTransformationMatrix() const;
    RotationMatrix GetRotationMatrix() const;
    Vector<3> AccelerationsToSpecificForces(const Vector<3>& gravity_world) const;
    Vector<3> EulerRatesToAngularVelocities() const;
    Vector<3> EulerAccelerationsToAngularAccelerations() const;
    PoseVector Extrapolate(double time) const;
};

}  //namespace mpmca