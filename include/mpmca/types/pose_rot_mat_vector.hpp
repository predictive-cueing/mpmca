/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/linear_algebra.hpp"

namespace mpmca {

class PoseVector;
class TransformationMatrix;

class PoseRotMatVector : public Vector<24>
{
  public:
    inline double GetX() const { return this->Vector<24>::operator[](0); }
    inline double& GetX() { return this->Vector<24>::operator[](0); }
    inline double GetY() const { return this->Vector<24>::operator[](1); }
    inline double& GetY() { return this->Vector<24>::operator[](1); }
    inline double GetZ() const { return this->Vector<24>::operator[](2); }
    inline double& GetZ() { return this->Vector<24>::operator[](2); }

    inline double GetT00() const { return this->Vector<24>::operator[](3); }
    inline double& GetT00() { return this->Vector<24>::operator[](3); }
    inline double GetT01() const { return this->Vector<24>::operator[](4); }
    inline double& GetT01() { return this->Vector<24>::operator[](4); }
    inline double GetT02() const { return this->Vector<24>::operator[](5); }
    inline double& GetT02() { return this->Vector<24>::operator[](5); }
    inline double GetT10() const { return this->Vector<24>::operator[](6); }
    inline double& GetT10() { return this->Vector<24>::operator[](6); }
    inline double GetT11() const { return this->Vector<24>::operator[](7); }
    inline double& GetT11() { return this->Vector<24>::operator[](7); }
    inline double GetT12() const { return this->Vector<24>::operator[](8); }
    inline double& GetT12() { return this->Vector<24>::operator[](8); }
    inline double GetT20() const { return this->Vector<24>::operator[](9); }
    inline double& GetT20() { return this->Vector<24>::operator[](9); }
    inline double GetT21() const { return this->Vector<24>::operator[](10); }
    inline double& GetT21() { return this->Vector<24>::operator[](10); }
    inline double GetT22() const { return this->Vector<24>::operator[](11); }
    inline double& GetT22() { return this->Vector<24>::operator[](11); }

    inline double GetT03() const { return this->Vector<24>::operator[](0); }
    inline double& GetT03() { return this->Vector<24>::operator[](0); }
    inline double GetT13() const { return this->Vector<24>::operator[](1); }
    inline double& GetT13() { return this->Vector<24>::operator[](1); }
    inline double GetT23() const { return this->Vector<24>::operator[](2); }
    inline double& GetT23() { return this->Vector<24>::operator[](2); }

    inline TransformationMatrix GetTransformationMatrix() const;

    inline double GetXd() const { return this->Vector<24>::operator[](12); }
    inline double& GetXd() { return this->Vector<24>::operator[](12); }
    inline double GetYd() const { return this->Vector<24>::operator[](13); }
    inline double& GetYd() { return this->Vector<24>::operator[](13); }
    inline double GetZd() const { return this->Vector<24>::operator[](14); }
    inline double& GetZd() { return this->Vector<24>::operator[](14); }

    inline double GetP() const { return this->Vector<24>::operator[](15); }
    inline double& GetP() { return this->Vector<24>::operator[](15); }
    inline double GetQ() const { return this->Vector<24>::operator[](16); }
    inline double& GetQ() { return this->Vector<24>::operator[](16); }
    inline double GetR() const { return this->Vector<24>::operator[](17); }
    inline double& GetR() { return this->Vector<24>::operator[](17); }

    inline double GetXdd() const { return this->Vector<24>::operator[](18); }
    inline double& GetXdd() { return this->Vector<24>::operator[](18); }
    inline double GetYdd() const { return this->Vector<24>::operator[](19); }
    inline double& GetYdd() { return this->Vector<24>::operator[](19); }
    inline double GetZdd() const { return this->Vector<24>::operator[](20); }
    inline double& GetZdd() { return this->Vector<24>::operator[](20); }

    inline double GetPd() const { return this->Vector<24>::operator[](21); }
    inline double& GetPd() { return this->Vector<24>::operator[](21); }
    inline double GetQd() const { return this->Vector<24>::operator[](22); }
    inline double& GetQd() { return this->Vector<24>::operator[](22); }
    inline double GetRd() const { return this->Vector<24>::operator[](23); }
    inline double& GetRd() { return this->Vector<24>::operator[](23); }

    PoseRotMatVector(void)
        : Vector<24>()
    {
    }

    // This constructor allows you to construct PoseRotMatVector from Eigen
    // expressions
    template <typename OtherDerived>
    PoseRotMatVector(const Eigen::MatrixBase<OtherDerived>& other)
        : Vector<24>(other)
    {
    }

    // This method allows you to assign Eigen expressions to PoseRotMatVector
    template <typename OtherDerived>
    PoseRotMatVector& operator=(const Eigen::MatrixBase<OtherDerived>& other)
    {
        this->Vector<24>::operator=(other);
        return *this;
    }

    PoseVector ToPoseVector() const;
};

}  //namespace mpmca