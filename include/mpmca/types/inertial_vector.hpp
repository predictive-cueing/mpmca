/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/linear_algebra.hpp"

namespace mpmca {

class InertialVector : public Vector<9>
{
  public:
    inline double GetFx() const { return this->Vector<9>::operator[](0); }
    inline double& GetFx() { return this->Vector<9>::operator[](0); }
    inline double GetFy() const { return this->Vector<9>::operator[](1); }
    inline double& GetFy() { return this->Vector<9>::operator[](1); }
    inline double GetFz() const { return this->Vector<9>::operator[](2); }
    inline double& GetFz() { return this->Vector<9>::operator[](2); }

    inline double GetOmegaX() const { return this->Vector<9>::operator[](3); }
    inline double& GetOmegaX() { return this->Vector<9>::operator[](3); }
    inline double GetOmegaY() const { return this->Vector<9>::operator[](4); }
    inline double& GetOmegaY() { return this->Vector<9>::operator[](4); }
    inline double GetOmegaZ() const { return this->Vector<9>::operator[](5); }
    inline double& GetOmegaZ() { return this->Vector<9>::operator[](5); }

    inline double GetAlphaX() const { return this->Vector<9>::operator[](6); }
    inline double& GetAlphaX() { return this->Vector<9>::operator[](6); }
    inline double GetAlphaY() const { return this->Vector<9>::operator[](7); }
    inline double& GetAlphaY() { return this->Vector<9>::operator[](7); }
    inline double GetAlphaZ() const { return this->Vector<9>::operator[](8); }
    inline double& GetAlphaZ() { return this->Vector<9>::operator[](8); }

    static InertialVector GetNeutral();

    InertialVector(void)
        : Vector<9>()
    {
    }

    // This constructor allows you to construct InertialVector from Eigen
    // expressions
    template <typename OtherDerived>
    InertialVector(const Eigen::MatrixBase<OtherDerived>& other)
        : Vector<9>(other)
    {
    }

    // This method allows you to assign Eigen expressions to InertialVector
    template <typename OtherDerived>
    InertialVector& operator=(const Eigen::MatrixBase<OtherDerived>& other)
    {
        this->Vector<9>::operator=(other);
        return *this;
    }
};

}  //namespace mpmca