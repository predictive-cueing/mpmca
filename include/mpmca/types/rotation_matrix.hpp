/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/linear_algebra.hpp"

namespace mpmca {

class RotationMatrix : public mpmca::Matrix<3, 3>
{
  public:
    static RotationMatrix FromEulerAngles(double phi, double theta, double psi);

    RotationMatrix(void)
        : mpmca::Matrix<3, 3>()
    {
    }

    template <typename OtherDerived>
    RotationMatrix(const Eigen::MatrixBase<OtherDerived>& other)
        : mpmca::Matrix<3, 3>(other)
    {
    }

    template <typename OtherDerived>
    RotationMatrix& operator=(const Eigen::MatrixBase<OtherDerived>& other)
    {
        this->mpmca::Matrix<3, 3>::operator=(other);
        return *this;
    }
};

}  //namespace mpmca