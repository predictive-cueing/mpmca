/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/linear_algebra.hpp"

namespace mpmca {

class TransformationMatrix : public mpmca::Matrix<4, 4>
{
  public:
    inline double GetX() const { return this->mpmca::Matrix<4, 4>::operator()(0, 3); }
    inline double& GetX() { return this->mpmca::Matrix<4, 4>::operator()(0, 3); }
    inline double GetY() const { return this->mpmca::Matrix<4, 4>::operator()(1, 3); }
    inline double& GetY() { return this->mpmca::Matrix<4, 4>::operator()(1, 3); }
    inline double GetZ() const { return this->mpmca::Matrix<4, 4>::operator()(2, 3); }
    inline double& GetZ() { return this->mpmca::Matrix<4, 4>::operator()(2, 3); }

    static TransformationMatrix FromDenavitHartenbergParameters(double theta, double d, double alpha, double a);

    static TransformationMatrix FromPoseParameters(double x, double y, double z, double roll, double pitch, double yaw);

    TransformationMatrix(void)
        : mpmca::Matrix<4, 4>()
    {
    }

    // This constructor allows you to construct TransformationMatrix from Eigen
    // expressions
    template <typename OtherDerived>
    TransformationMatrix(const Eigen::MatrixBase<OtherDerived>& other)
        : mpmca::Matrix<4, 4>(other)
    {
    }

    // This method allows you to assign Eigen expressions to TransformationMatrix
    template <typename OtherDerived>
    TransformationMatrix& operator=(const Eigen::MatrixBase<OtherDerived>& other)
    {
        this->mpmca::Matrix<4, 4>::operator=(other);
        return *this;
    }
};

}  //namespace mpmca