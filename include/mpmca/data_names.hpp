/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <string>
#include <vector>

#include "mpmca/utilities/data_properties.hpp"

namespace mpmca {

class DN
{
  public:
    static const utilities::DataProperties& f_x_head();
    static const utilities::DataProperties& f_y_head();
    static const utilities::DataProperties& f_z_head();
    static const utilities::DataProperties& omega_x_head();
    static const utilities::DataProperties& omega_y_head();
    static const utilities::DataProperties& omega_z_head();
    static const utilities::DataProperties& alpha_x_head();
    static const utilities::DataProperties& alpha_y_head();
    static const utilities::DataProperties& alpha_z_head();
    static const utilities::DataProperties& X_vehicle();
    static const utilities::DataProperties& Y_vehicle();
    static const utilities::DataProperties& Z_vehicle();
    static const utilities::DataProperties& phi_vehicle();
    static const utilities::DataProperties& theta_vehicle();
    static const utilities::DataProperties& psi_vehicle();
    static const utilities::DataProperties& u_vehicle();
    static const utilities::DataProperties& v_vehicle();
    static const utilities::DataProperties& w_vehicle();
    static const utilities::DataProperties& p_vehicle();
    static const utilities::DataProperties& q_vehicle();
    static const utilities::DataProperties& r_vehicle();
    static const utilities::DataProperties& a_x_vehicle();
    static const utilities::DataProperties& a_y_vehicle();
    static const utilities::DataProperties& a_z_vehicle();
    static const utilities::DataProperties& p_dot_vehicle();
    static const utilities::DataProperties& q_dot_vehicle();
    static const utilities::DataProperties& r_dot_vehicle();
    static const utilities::DataProperties& car_throttle();
    static const utilities::DataProperties& car_brake();
    static const utilities::DataProperties& car_throttle_dot();
    static const utilities::DataProperties& car_brake_dot();
    static const utilities::DataProperties& car_clutch();
    static const utilities::DataProperties& car_engine_rotational_velocity();
    static const utilities::DataProperties& car_engine_torque();
    static const utilities::DataProperties& car_transmission_gear();
    static const utilities::DataProperties& car_steer();
    static const utilities::DataProperties& car_shift();
    static const utilities::DataProperties& car_automatic_gear_change();
    static const utilities::DataProperties& X_vehicle_0();
    static const utilities::DataProperties& Y_vehicle_0();
    static const utilities::DataProperties& Z_vehicle_0();
    static const utilities::DataProperties& phi_vehicle_0();
    static const utilities::DataProperties& theta_vehicle_0();
    static const utilities::DataProperties& psi_vehicle_0();
    static const utilities::DataProperties& gravity();
    static const utilities::DataProperties& vehicle_mass();
    static const utilities::DataProperties& car_C_distance_rear_axle();
    static const utilities::DataProperties& car_S_aero_force();
    static const utilities::DataProperties& car_S_brake_force();
    static const utilities::DataProperties& car_M1D_gear_ratio();
    static const utilities::DataProperties& car_M2D_engine_torque();
    static std::vector<std::string> all_signal_names();
};
}  //namespace mpmca