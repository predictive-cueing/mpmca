/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/predict/inertial/vector_6.hpp"

namespace mpmca::predict::inertial {
class VehicleState : public Vector6
{
  public:
    VehicleState(double x = 0., double y = 0., double z = 0., double phi = 0., double theta = 0., double psi = 0.,
                 double u = 0., double v = 0., double w = 0., double p = 0., double q = 0., double r = 0.,
                 double a_x = 0., double a_y = 0., double a_z = 0., double pd = 0., double qd = 0., double rd = 0.,
                 double j_x = 0., double j_y = 0., double j_z = 0., double pdd = 0., double qdd = 0., double rdd = 0.);

    friend std::ostream &operator<<(std::ostream &out, const VehicleState &c);

    VehicleState PredictPosition(double tau) const;

    double GetU() const;
    double GetV() const;
    double GetW() const;
    double GetP() const;
    double GetQ() const;
    double GetR() const;
    double GetAx() const;
    double GetAy() const;
    double GetAz() const;
    double GetPd() const;
    double GetQd() const;
    double GetRd() const;
    double GetJx() const;
    double GetJy() const;
    double GetJz() const;
    double GetPdd() const;
    double GetQdd() const;
    double GetRdd() const;

    void SetU(double i);
    void SetV(double i);
    void SetW(double i);
    void SetP(double i);
    void SetQ(double i);
    void SetR(double i);
    void SetAx(double i);
    void SetAy(double i);
    void SetAz(double i);
    void SetPd(double i);
    void SetQd(double i);
    void SetRd(double i);
    void SetJx(double i);
    void SetJy(double i);
    void SetJz(double i);
    void SetPdd(double i);
    void SetQdd(double i);
    void SetRdd(double i);

  protected:
    double m_u = 0;
    double m_v = 0;
    double m_w = 0;

    double m_p = 0;
    double m_q = 0;
    double m_r = 0;

    double m_a_x = 0;
    double m_a_y = 0;
    double m_a_z = 0;

    double m_pd = 0;
    double m_qd = 0;
    double m_rd = 0;

    double m_j_x = 0;
    double m_j_y = 0;
    double m_j_z = 0;

    double m_pdd = 0;
    double m_qdd = 0;
    double m_rdd = 0;
};
}  //namespace mpmca::predict::inertial