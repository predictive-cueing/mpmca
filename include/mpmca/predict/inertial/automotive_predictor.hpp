/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/pipeline/message_bus.hpp"
#include "mpmca/predict/inertial/car_minimal.hpp"
#include "mpmca/predict/inertial/driver_minimal.hpp"
#include "mpmca/predict/inertial/inertial_output.hpp"
#include "mpmca/predict/inertial/path.hpp"
#include "mpmca/predict/inertial/path_reader.hpp"
#include "mpmca/predict/inertial/simulation.hpp"
#include "mpmca/utilities/configuration.hpp"
#include "mpmca/utilities/horizon.hpp"
#include "mpmca/utilities/logger.hpp"

namespace mpmca::predict::inertial {

class AutomotivePredictor
{
  private:
    utilities::Logger m_logger;
    Simulation<DriverMinimal, CarMinimal, InertialOutput> m_sim;
    bool m_simulate_called;

  public:
    AutomotivePredictor(utilities::ConfigurationPtr config, const utilities::Horizon& horizon, const std::string& name);

    void PrepareTaskMessageBus(pipeline::MessageBus& message_bus);
    void CheckTaskMessageBusPrepared(const pipeline::MessageBus& message_bus);

    void ReadMessageBus(pipeline::MessageBus& message_bus);
    void UpdateMessageBus(pipeline::MessageBus& message_bus);

    void PrepareTick();

    void SetCurrentTimeMs(int64_t time);
    void Predict();

    bool GetTerminate() const;
};
}  //namespace mpmca::predict::inertial