/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <memory>

#include "mpmca/predict/inertial/fraction_circle.hpp"
#include "mpmca/predict/inertial/interpolatable_cubic_spline.hpp"
#include "mpmca/predict/inertial/path_index.hpp"
#include "mpmca/predict/inertial/path_point.hpp"
#include "mpmca/predict/inertial/path_point_e.hpp"
#include "mpmca/predict/inertial/path_point_interpolatable.hpp"
#include "mpmca/utilities/prevent_heap_allocation_mixin.hpp"

namespace mpmca::predict::inertial {
class PathFactory;

class Path : public utilities::PreventHeapAllocationMixin
{
  public:
    Path(const std::string& name, const std::vector<PathPointE>& path, bool closed);
    ~Path();
    const std::string& GetName() const;
    const PathPointE& GetPathPointAt(int index) const;
    const PathPointE& GetPathPointAt(const PathIndex& p_index) const;
    PathPoint GetInterpolated(const PathIndex& p_index) const;
    bool SetEndOfPathReached(const PathIndex& p_index) const;

    int GetUniqueId() const;
    int GetStartIndex() const;
    size_t GetNumPathPoints() const;
    int GetEndIndex() const;
    bool IsClosed() const;
    bool IsPrepared() const;
    int ProtectIndex(int index) const;
    int ProtectIndexSilent(int index) const;
    double GetSmallestApproximatePointToPointDistance() const;
    void SetSmallestApproximatePointToPointDistance(double i);

  private:
    const std::string m_name;
    const bool m_closed;
    const std::vector<PathPointE> m_path;
    const int m_unique_id;
    double m_smallest_approximate_point_to_point_distance;

    static int GetNextId()
    {
        static int s_next_id = 0;
        return s_next_id++;
    }
};

typedef std::shared_ptr<Path> PathPtr;
}  //namespace mpmca::predict::inertial