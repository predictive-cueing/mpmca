/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

namespace mpmca::predict::inertial {
class PathSegment
{
  private:
    int m_from;
    int m_to;
    bool m_minimum_point_found;
    bool m_all_points_evaluated;
    int m_minimum_index;
    double m_minimum_distance;
    double m_distance_at_center;

  public:
    PathSegment(int from, int to);

    int GetFrom() const;
    int GetTo() const;
    int GetNumberOfPoints() const;
    bool GetAllPointsAreEvaluated() const;
    void SetAllPointsAreEvaluated(bool eval);

    int GetCenter() const;
    bool IsMinimumPointFound() const;
    int GetMinimumIndex() const;
    double GetMinimumDistance() const;
    void IsMinimumPointFound(int index, double min_dist);
    double GetDistanceAtCenter() const;
    void SetDistanceAtCenter(double dist_center);

    bool operator<(const PathSegment& other) const;
};
}  //namespace mpmca::predict::inertial