/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/predict/inertial/path.hpp"

namespace mpmca::predict::inertial {
class PathFactory
{
  public:
    PathFactory(const std::vector<PathPoint>& path, bool closed);
    PathFactory(const std::string& factory_name, const std::vector<PathPoint>& path, bool closed);

    PathPtr GetPreparedPath(const std::string& path_name = "unnamed_path");

    void SetOmitHeadingCheck(bool omit);
    void SetHeadingLimit(double psi_limit);

    static PathPtr CreatePathCircle(double ds, double psi_start, double psi_end, double R, double x0, double y0,
                                    bool closed, const std::string& name = "circle_path");

    static PathPtr CreatePathLine(double ds, double x0, double y0, double x1, double y1, double u_mean = 10,
                                  double u_standard_deviation = 1, const std::string& name = "line_path");

    static std::vector<PathPoint> CreatePathPointVectorCircle(double ds, double psi_start, double psi_end, double R,
                                                              double x0, double y0);
    static std::vector<PathPoint> GetCirclePathPointVector(int num_points_whole_circle, int num_points_vector, double R,
                                                           double x_center = 0.0, double y_center = 0.0);
    static std::vector<PathPoint> GetStraightPathPointVector(const Vector6& start, const Vector6& end, double ds);
    static std::vector<PathPoint> GetCircularPathPointVector(int num_points, double ds, double R, double x_center = 0.0,
                                                             double y_center = 0.0);

    template <class T>
    static void ProcessVector(std::vector<T>& vec, bool closed_path)
    {
        if (vec.size() <= 1)
            return;

        for (int i = 0; i < vec.size(); ++i)
            vec[i].self(vec[i]);

        if (closed_path) {
            int N = vec.size();

            for (int i = 0; i < N; ++i) {
                double behind = 0;
                for (int j = 1; j <= vec[i].GetMaxNumPointsRequiredBehind(); ++j) {
                    behind += vec[utilities::Math::PositiveModulo(i - j + 1, N)].Vector2::GetDistance(
                        vec[utilities::Math::PositiveModulo(i - j, N)]);
                    vec[i].GetAdjacentPoint(-j, behind, vec[utilities::Math::PositiveModulo(i - j, N)]);
                }

                double ahead = 0;
                for (int j = 1; j <= vec[i].GetMaxNumPointsRequiredAhead(); ++j) {
                    ahead += vec[utilities::Math::PositiveModulo(i + j - 1, N)].Vector2::GetDistance(
                        vec[utilities::Math::PositiveModulo(i + j, N)]);
                    vec[i].GetAdjacentPoint(j, ahead, vec[utilities::Math::PositiveModulo(i + j, N)]);
                }
            }
        }
        else {
            int L = vec.size() - 1;

            for (int i = 0; i <= L; ++i) {
                double behind = 0;
                for (int j = 1; j <= vec[i].GetMaxNumPointsRequiredBehind(); ++j) {
                    if (i - j >= 0) {
                        behind += vec[i - j + 1].Vector2::GetDistance(vec[i - j]);
                        vec[i].GetAdjacentPoint(-j, behind, vec[i - j]);
                    }
                }

                double ahead = 0;
                for (int j = 1; j <= vec[i].GetMaxNumPointsRequiredAhead(); ++j) {
                    if (i + j <= L) {
                        ahead += vec[i + j - 1].Vector2::GetDistance(vec[i + j]);
                        vec[i].GetAdjacentPoint(j, ahead, vec[i + j]);
                    }
                }
            }
        }

        std::for_each(vec.begin(), vec.end(), [](auto& p) { p.Finalize(); });
    }

  private:
    void Prepare();
    void SetIndices();
    void CheckClosed();
    void CheckHeading();
    void CheckDs();
    void CheckHeading(double actual_psi, double derivative_psi, int i) const;
    void DetermineSmallestApproximatePointToPointDistance();

    std::string m_factory_name;
    bool m_closed;
    bool m_omit_heading_check;
    bool m_prepared;
    double m_heading_limit = 0.15;
    double m_smallest_distance;
    std::vector<PathPoint> m_unprepared_path_point_vector;
    std::vector<PathPointE> m_prepared_path_point_vector;

    template <class T>
    const T& GetPathPointAt(std::vector<T>& vec, int index) const
    {
        if (m_closed)
            return vec[utilities::Math::PositiveModulo(index, vec.size())];
        else {
            if (index < 0 || index >= vec.size())
                throw std::runtime_error("Requested index (" + std::to_string(index) + ") out of range (0, " +
                                         std::to_string(vec.size()) + ").");

            return vec[index];
        }
    }
};
}  //namespace mpmca::predict::inertial