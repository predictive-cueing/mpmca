/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/predict/inertial/interpolatable.hpp"

namespace mpmca::predict::inertial {
namespace testing {
class PathPointInterpolatableTest_asymmetricCubicSplineTest_Test;
}
class InterpolatableCubicSpline : public Interpolatable
{
    /**
     * @brief InterpolatableCubicSpline
     * The number of points this class uses to compute slopes is hardcoded, because using variables that contain these
     * numbers suggest that the class would work for other values as well, which is not true.
     *
     */
  private:
    friend testing::PathPointInterpolatableTest_asymmetricCubicSplineTest_Test;
    struct SplineParameters
    {
        double p0 = 0;
        double p1 = 0;
        double m0 = 0;
        double m1 = 0;
        double ds = 1;
    };

    std::array<double, 4> m_values;
    std::array<double, 4> m_dss;
    std::array<bool, 4> m_was_set;
    SplineParameters m_behind;
    SplineParameters m_ahead;
    bool m_has_previous;
    bool m_has_next;

  public:
    InterpolatableCubicSpline(double value = 0.0);
    void SetValue(double value) override;

    int GetNumPointsRequiredBehind() const override { return 2; };
    int GetNumPointsRequiredAhead() const override { return 2; };
    double GetInterpolated(double fraction) const override;
    double Derivative(double fraction) const override;
    void Finalize(std::function<double(const double&, const double&)> difference_function =
                      [](const double& x, const double& y) { return x - y; }) override;
    void GetAdjacentPoint(int index_difference, double ds, const Interpolatable& other) override;
};
}  //namespace mpmca::predict::inertial