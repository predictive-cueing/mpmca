/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/predict/inertial/inertial_output.hpp"
#include "mpmca/predict/inertial/path.hpp"
#include "mpmca/utilities/configuration.hpp"
#include "mpmca/utilities/empty_time_measurement.hpp"
#include "mpmca/utilities/horizon.hpp"
#include "mpmca/utilities/logger.hpp"
#include "mpmca/utilities/time_measurement.hpp"

namespace mpmca::predict::inertial {

enum class SimulationControlOption
{
    kForControl,
    kForPrediction
};

template <class Human, class Car, class Output>
class Simulation
{
  private:
    const std::string m_name;
    const utilities::Horizon m_horizon;
    utilities::Logger m_logger;

    PathPtr m_path_ptr;
    Human m_human_model;
    Car m_car_model;
    Output m_output_model;

    utilities::TimeMeasurement m_time_measurement;

    utilities::ClockIndex m_clock_idx_human;
    utilities::ClockIndex m_clock_idx_car;
    utilities::ClockIndex m_clock_idx_output;

    bool m_end_of_path_reached = false;

    void ShiftBack();
    void CopyInitVectors();

  public:
    Simulation(utilities::ConfigurationPtr config, const utilities::Horizon& horizon, const std::string& name,
               SimulationControlOption for_control = SimulationControlOption::kForPrediction);

    void Simulate();

    template <typename MT>
    void ReadMessage(const MT& message)
    {
        m_car_model.ReadMessage(message);
        m_human_model.ReadMessage(message);
        m_output_model.ReadMessage(message);
    }

    template <typename MT>
    void UpdateMessage(MT& message)
    {
        m_car_model.UpdateMessage(message);
        m_human_model.UpdateMessage(message);
        m_output_model.UpdateMessage(message);
    }

    void PrintStatisticsUs() const;
    void PrintStatisticsNs() const;
    const utilities::Horizon& GetHorizon() const;

    void PrepareTick();
    void InitTime(int64_t t);
    void SetEndOfPathReached(bool eop);
    bool GetEndOfPathReached() const;
    void SetPathPtr(PathPtr path_ptr);
    int GetSignalIndex(const std::string& signal_name);
    PathPtr GetPath() const;

    Human& GetHumanModel();
    Car& GetCarModel();
    InertialOutput& GetOutputModel();
};

template <class Human, class Car, class Output>
Simulation<Human, Car, Output>::Simulation(utilities::ConfigurationPtr config, const utilities::Horizon& horizon,
                                           const std::string& name, SimulationControlOption for_control)
    : m_name(name)
    , m_horizon(horizon)
    , m_logger(m_name, "Simulation" + name)
    , m_path_ptr(nullptr)
    , m_human_model(config, horizon, name + ":Human", for_control == SimulationControlOption::kForControl)
    , m_car_model(config, horizon, name + ":Car", for_control == SimulationControlOption::kForControl)
    , m_output_model(config, horizon, name + ":Output", for_control == SimulationControlOption::kForControl)
    , m_clock_idx_human(m_time_measurement.RegisterClock("Human"))
    , m_clock_idx_car(m_time_measurement.RegisterClock("Car"))
    , m_clock_idx_output(m_time_measurement.RegisterClock("Output"))
{
    if (m_car_model.GetNPrediction() != m_human_model.GetNPrediction())
        throw std::runtime_error("The prediction horizons of the car and the human do not have the same lengths.");
}

template <class Human, class Car, class Output>
void Simulation<Human, Car, Output>::Simulate()
{
    ShiftBack();
    CopyInitVectors();

    m_time_measurement.ResetStatistics();
    m_human_model.ResetStatistics();
    m_car_model.ResetStatistics();

    if (!m_path_ptr)
        throw std::runtime_error("The simulation cannot run, because the path pointer is equal to nullptr.");

    for (size_t j = m_human_model.GetHorizonStartIndex(); j <= m_human_model.GetHorizonEndIndex(); ++j) {
        m_time_measurement.StartCycle();
        m_human_model.Step(m_car_model, m_path_ptr, j);
        m_time_measurement.StopClock(m_clock_idx_human);
        m_car_model.Step(m_human_model, m_output_model, m_path_ptr, j);
        m_time_measurement.StopClock(m_clock_idx_car);
        m_output_model.Step(m_car_model, j);
        m_time_measurement.StopClock(m_clock_idx_output);
        m_time_measurement.StopCycle();
    }

    SetEndOfPathReached(m_human_model.GetEndOfPathReached());

    if (GetEndOfPathReached())
        m_logger.Warning(
            "The end of the path was reached. The predictor will signal to the Pipeline that the simulation should be "
            "stopped.");
}

template <class Human, class Car, class Output>
void Simulation<Human, Car, Output>::PrintStatisticsUs() const
{
    m_human_model.PrintStatisticsUs();
    m_car_model.PrintStatisticsUs();
    m_time_measurement.PrintStatisticsUs();
}

template <class Human, class Car, class Output>
const utilities::Horizon& Simulation<Human, Car, Output>::GetHorizon() const
{
    return m_horizon;
}

template <class Human, class Car, class Output>
void Simulation<Human, Car, Output>::PrintStatisticsNs() const
{
    m_human_model.PrintStatisticsNs();
    m_car_model.PrintStatisticsNs();
    m_time_measurement.PrintStatisticsNs();
}
template <class Human, class Car, class Output>
void Simulation<Human, Car, Output>::ShiftBack()
{
    m_human_model.ShiftBack();
    m_car_model.ShiftBack();
    m_output_model.ShiftBack();
}
template <class Human, class Car, class Output>
void Simulation<Human, Car, Output>::CopyInitVectors()
{
    m_human_model.CopyInitVector();
    m_car_model.CopyInitVector();
    m_output_model.CopyInitVector();
}

template <class Human, class Car, class Output>
void Simulation<Human, Car, Output>::PrepareTick()
{
    if (!m_path_ptr)
        throw std::runtime_error("Cannot call PrepareTick before a path is set.");

    m_human_model.PrepareTick(m_path_ptr);
    m_car_model.PrepareTick(m_path_ptr);
    m_output_model.PrepareTick(m_path_ptr);
}
template <class Human, class Car, class Output>
void Simulation<Human, Car, Output>::InitTime(int64_t t)
{
    m_human_model.InitTime(t);
    m_car_model.InitTime(t);
    m_output_model.InitTime(t);
}
template <class Human, class Car, class Output>
void Simulation<Human, Car, Output>::SetEndOfPathReached(bool eop)
{
    m_end_of_path_reached = m_end_of_path_reached || eop;
}
template <class Human, class Car, class Output>
bool Simulation<Human, Car, Output>::GetEndOfPathReached() const
{
    return m_end_of_path_reached;
}
template <class Human, class Car, class Output>
void Simulation<Human, Car, Output>::SetPathPtr(PathPtr path_ptr)
{
    if (path_ptr == nullptr) {
        throw std::runtime_error("SetPathPtr called with nullptr.");
    }

    if (!m_path_ptr || (m_path_ptr && m_path_ptr->GetUniqueId() != path_ptr->GetUniqueId())) {
        m_logger.Info("Setting new path (" + path_ptr->GetName() + ").");
        m_path_ptr = path_ptr;
    }
}
template <class Human, class Car, class Output>
int Simulation<Human, Car, Output>::GetSignalIndex(const std::string& signal_name)
{
    int human_index = m_human_model.GetSignalIndex(signal_name);
    int car_index = m_car_model.GetSignalIndex(signal_name);
    int output_index = m_output_model.GetSignalIndex(signal_name);

    if (human_index >= 0)
        return human_index;
    else if (car_index >= 0)
        return m_human_model.GetNSignals() + car_index;
    else if (output_index >= 0)
        return m_human_model.GetNSignals() + m_car_model.GetNSignals() + output_index;
    else
        return -1;
}
template <class Human, class Car, class Output>
PathPtr Simulation<Human, Car, Output>::GetPath() const
{
    return m_path_ptr;
}

template <class Human, class Car, class Output>
Human& Simulation<Human, Car, Output>::GetHumanModel()
{
    return m_human_model;
}
template <class Human, class Car, class Output>
Car& Simulation<Human, Car, Output>::GetCarModel()
{
    return m_car_model;
}
template <class Human, class Car, class Output>
InertialOutput& Simulation<Human, Car, Output>::GetOutputModel()
{
    return m_output_model;
}

}  //namespace mpmca::predict::inertial