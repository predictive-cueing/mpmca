/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/predict/inertial/interpolatable.hpp"

namespace mpmca::predict::inertial {
class InterpolatableLinear : public Interpolatable
{
  public:
    InterpolatableLinear(double value = 0.0);
    void SetValue(double value) override;
    int GetNumPointsRequiredBehind() const override { return 1; };
    int GetNumPointsRequiredAhead() const override { return 1; };
    double GetInterpolated(double fraction) const override;
    double Derivative(double fraction) const override;
    void Finalize(std::function<double(const double&, const double&)> difference_function =
                      [](const double& x, const double& y) { return x - y; }) override;
    void GetAdjacentPoint(int index_difference, double ds, const Interpolatable& other) override;

  private:
    std::array<double, 2> m_values;
    std::array<double, 2> m_dss;
    std::array<bool, 2> m_was_set;

    double m_dy_behind;
    double m_dy_ahead;
    double m_dy_ds_behind;
    double m_dy_ds_ahead;
};
}  //namespace mpmca::predict::inertial