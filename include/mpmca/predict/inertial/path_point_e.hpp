/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/predict/inertial/lateral_error.hpp"
#include "mpmca/utilities/math.hpp"

namespace mpmca::predict::inertial {
class PathPointE : public LateralError
{
  public:
    using LateralError::LateralError;
    PathPointE(const PathPoint& p)
        : LateralError(p)
    {
    }

    template <class TB>
    void self(const TB& s)
    {
        PathPointInterpolatable::self(s);
        FractionCircle::self(s);
        LateralError::self(s);
    }

    void Finalize()
    {
        PathPointInterpolatable::Finalize();
        FractionCircle::Finalize();
        LateralError::Finalize();
    }

    template <class TA>
    void GetAdjacentPoint(int index_difference, double ds, const TA& other)
    {
        PathPointInterpolatable::GetAdjacentPoint(index_difference, ds, other);
        FractionCircle::GetAdjacentPoint(index_difference, ds, other);
        LateralError::GetAdjacentPoint(index_difference, ds, other);
    }

    int GetMaxNumPointsRequiredBehind()
    {
        return std::max({PathPointInterpolatable::GetNumPointsRequiredBehind(),
                         FractionCircle::GetNumPointsRequiredBehind(), LateralError::GetNumPointsRequiredBehind()});
    }
    int GetMaxNumPointsRequiredAhead()
    {
        return std::max({PathPointInterpolatable::GetNumPointsRequiredAhead(),
                         FractionCircle::GetNumPointsRequiredAhead(), LateralError::GetNumPointsRequiredAhead()});
    }
};
}  //namespace mpmca::predict::inertial