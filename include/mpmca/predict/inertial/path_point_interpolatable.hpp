/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <vector>

#include "mpmca/predict/inertial/path_point.hpp"
#include "mpmca/utilities/math.hpp"

namespace mpmca::predict::inertial {
template <class T>
class PathPointInterpolatable : public PathPoint
{
  public:
    PathPointInterpolatable(const PathPoint& p)
        : PathPoint(p)
    {
    }
    using PathPoint::PathPoint;

    PathPoint GetInterpolated(double fraction) const
    {
        PathPoint ret;
        ret.SetX(m_x_inter.GetInterpolated(fraction));
        ret.SetY(m_y_inter.GetInterpolated(fraction));
        ret.SetZ(m_z_inter.GetInterpolated(fraction));
        ret.SetPhi(m_phi_inter.GetInterpolated(fraction));
        ret.SetTheta(m_theta_inter.GetInterpolated(fraction));
        ret.SetPsi(m_psi_inter.GetInterpolated(fraction));
        ret.SetUMean(m_u_mean_inter.GetInterpolated(fraction));
        ret.SetUStandardDeviation(m_u_standard_deviation_inter.GetInterpolated(fraction));
        ret.SetRoadWidth(m_road_width_inter.GetInterpolated(fraction));

        ret.SetDxDs(m_x_inter.Derivative(fraction));
        ret.SetDyDs(m_y_inter.Derivative(fraction));
        ret.SetDzDs(m_z_inter.Derivative(fraction));
        ret.SetDPhiDs(m_phi_inter.Derivative(fraction));
        ret.SetDThetaDs(m_theta_inter.Derivative(fraction));
        ret.SetDPsiDs(m_psi_inter.Derivative(fraction));
        ret.SetDuMeanDs(m_u_mean_inter.Derivative(fraction));
        ret.SetDuStandardDeviationDs(m_u_standard_deviation_inter.Derivative(fraction));
        ret.SetDRoadWidthDs(m_road_width_inter.Derivative(fraction));
        return ret;
    }

    void Finalize()
    {
        m_x_inter.Finalize();
        m_y_inter.Finalize();
        m_z_inter.Finalize();
        m_phi_inter.Finalize(utilities::Math::GetAngleDiff);
        m_theta_inter.Finalize(utilities::Math::GetAngleDiff);
        m_psi_inter.Finalize(utilities::Math::GetAngleDiff);
        m_u_mean_inter.Finalize();
        m_u_standard_deviation_inter.Finalize();
        m_road_width_inter.Finalize();

        SetDxDs(m_x_inter.Derivative(0));
        SetDyDs(m_y_inter.Derivative(0));
        SetDzDs(m_z_inter.Derivative(0));
        SetDPhiDs(m_phi_inter.Derivative(0));
        SetDThetaDs(m_theta_inter.Derivative(0));
        SetDPsiDs(m_psi_inter.Derivative(0));
        SetDuMeanDs(m_u_mean_inter.Derivative(0));
        SetDuStandardDeviationDs(m_u_standard_deviation_inter.Derivative(0));
        SetDRoadWidthDs(m_road_width_inter.Derivative(0));
    }

    template <class TA>
    void self(const TA& s)
    {
        m_x_inter.SetValue(s.GetX());
        m_y_inter.SetValue(s.GetY());
        m_z_inter.SetValue(s.GetZ());
        m_phi_inter.SetValue(s.GetPhi());
        m_theta_inter.SetValue(s.GetTheta());
        m_psi_inter.SetValue(s.GetPsi());
        m_u_mean_inter.SetValue(s.GetUMean());
        m_u_standard_deviation_inter.SetValue(s.GetUStandardDeviation());
        m_road_width_inter.SetValue(s.GetRoadWidth());
    }

    template <class TA>
    void GetAdjacentPoint(int index_difference, double ds, const TA& other)
    {
        m_x_inter.GetAdjacentPoint(index_difference, ds, other.m_x_inter);
        m_y_inter.GetAdjacentPoint(index_difference, ds, other.m_y_inter);
        m_z_inter.GetAdjacentPoint(index_difference, ds, other.m_z_inter);
        m_phi_inter.GetAdjacentPoint(index_difference, ds, other.m_phi_inter);
        m_theta_inter.GetAdjacentPoint(index_difference, ds, other.m_theta_inter);
        m_psi_inter.GetAdjacentPoint(index_difference, ds, other.m_psi_inter);
        m_u_mean_inter.GetAdjacentPoint(index_difference, ds, other.m_u_mean_inter);
        m_u_standard_deviation_inter.GetAdjacentPoint(index_difference, ds, other.m_u_standard_deviation_inter);
        m_road_width_inter.GetAdjacentPoint(index_difference, ds, other.m_road_width_inter);
    }

    int GetNumPointsRequiredBehind() const { return m_x_inter.GetNumPointsRequiredBehind(); }
    int GetNumPointsRequiredAhead() const { return m_x_inter.GetNumPointsRequiredAhead(); }

  private:
    T m_x_inter;
    T m_y_inter;
    T m_z_inter;
    T m_phi_inter;
    T m_theta_inter;
    T m_psi_inter;
    T m_u_mean_inter;
    T m_u_standard_deviation_inter;
    T m_road_width_inter;
};
}  //namespace mpmca::predict::inertial