/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/predict/inertial/fraction_circle.hpp"

namespace mpmca::predict::inertial {
class LateralError : public FractionCircle
{
  public:
    LateralError(const PathPoint& p)
        : FractionCircle(p)
    {
    }
    using FractionCircle::FractionCircle;

    double GetLateralError(const Vector6& p) const;
    double GetLateralErrorDot(const VehicleState& x) const;
    double GetLateralError(double fraction, const Vector6& p) const;
    double GetLateralErrorDot(double fraction, const VehicleState& x) const;
    double GetSignOfSide(const Vector6& p) const;

  private:
    static double GetSignOfSide(const PathPoint& path_point, const Vector6& p);
};
}  //namespace mpmca::predict::inertial