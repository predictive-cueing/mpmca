/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/predict/inertial/vector_2.hpp"

namespace mpmca::predict::inertial {
class Vector3 : public Vector2
{
  public:
    Vector3();
    Vector3(double x, double y, double z);

    double GetDistance(const Vector3& other) const;
    double GetDistanceSquared(const Vector3& other) const;

    double GetZ() const;
    void SetZ(double i);

    Vector3 GetMeanWith(const Vector3& other) const;
    Vector3 operator-(const Vector3& other) const;
    Vector3 operator+(const Vector3& other) const;
    Vector3 operator*(double mult) const;

    friend std::ostream& operator<<(std::ostream& out, const Vector3& c);

  protected:
    double m_z = 0;
};
}  //namespace mpmca::predict::inertial