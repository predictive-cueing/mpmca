/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <array>

#include "mpmca/linear_algebra.hpp"
#include "mpmca/predict/inertial/vector_6.hpp"
#include "mpmca/predict/inertial/vehicle_state.hpp"

namespace mpmca::predict::inertial {
class PathFactory;

template <class T>
class PathPointInterpolatable;

class PathPoint : public Vector6
{
  public:
    PathPoint();
    PathPoint(double x, double y);
    PathPoint(double x, double y, double z, double phi, double theta, double psi);
    PathPoint(const std::array<double, 6> &mpc_vector);

    int GetIndex() const;

    double GetUMean() const;
    double GetUStandardDeviation() const;
    double GetRoadWidth() const;
    Vector<4> GetOrientationQuaternion() const;

    double GetDxDs() const;
    double GetDyDs() const;
    double GetDzDs() const;
    double GetDPhiDs() const;
    double GetDThetaDs() const;
    double GetDPsiDs() const;

    double GetDuMeanDs() const;
    double GetDuStandardDeviationDs() const;

    void SetUMean(double i);
    void SetUStandardDeviation(double i);
    void SetRoadWidth(double road_width);

    std::string ToString() const;
    friend std::ostream &operator<<(std::ostream &out, const PathPoint &c);

  protected:
    friend PathFactory;
    template <typename T>
    friend class PathPointInterpolatable;

    void SetIndex(int index);

    void SetDxDs(double i);
    void SetDyDs(double i);
    void SetDzDs(double i);
    void SetDPhiDs(double i);
    void SetDThetaDs(double i);
    void SetDPsiDs(double i);

    void SetDuMeanDs(double i);
    void SetDuStandardDeviationDs(double i);
    void SetDRoadWidthDs(double i);

    void CheckValues() const;

    int m_idx = -1;

    double m_u_mean = 0;
    double m_u_standard_deviation = 1.0;

    double m_road_width = 1;

    double m_dx_ds = 0;
    double m_dy_ds = 0;
    double m_dz_ds = 0;
    double m_d_phi_ds = 0;
    double m_d_theta_ds = 0;
    double m_d_psi_ds = 0;
    double m_du_mean_ds = 0;
    double m_du_standard_deviation_ds = 0;
    double m_d_road_width_ds = 0;
};
}  //namespace mpmca::predict::inertial