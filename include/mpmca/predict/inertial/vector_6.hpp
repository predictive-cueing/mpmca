/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <iostream>

#include "mpmca/predict/inertial/vector_3.hpp"

namespace mpmca::predict::inertial {
class Vector6 : public Vector3
{
  public:
    Vector6();
    Vector6(double x, double y);
    Vector6(double x, double y, double z, double phi, double theta, double psi);

    double GetPhi() const;
    double GetTheta() const;
    double GetPsi() const;

    void SetPhi(double i);
    void SetTheta(double i);
    void SetPsi(double i);

    Vector6 GetMeanWith(const Vector6& other) const;
    Vector6 operator-(const Vector6& other) const;
    Vector6 operator+(const Vector6& other) const;
    Vector6 operator*(double mult) const;

    bool IsClose(const Vector6& other, double translational_tolerance, double angular_tolerance) const;
    friend std::ostream& operator<<(std::ostream& out, const Vector6& c);

  protected:
    double m_phi = 0;
    double m_theta = 0;
    double m_psi = 0;
};
}  //namespace mpmca::predict::inertial