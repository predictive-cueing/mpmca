/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <iostream>

namespace mpmca::predict::inertial {
class PathIndex
{
  private:
    int64_t m_index = 0;
    double m_fraction = 0;

  public:
    PathIndex();
    PathIndex(int64_t index, double fraction);

    void operator()(const PathIndex &other);

    void GetIndex(int64_t i);
    void GetFraction(double i);

    int64_t GetIndex() const;
    double GetFraction() const;
};

inline std::ostream &operator<<(std::ostream &out, const PathIndex &c)
{
    out << c.GetIndex() << ":" << c.GetFraction();
    return out;
}
}  //namespace mpmca::predict::inertial