/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <iostream>

namespace mpmca::predict::inertial {
class Vector2
{
  public:
    Vector2();
    Vector2(double x, double y);

    double GetDistance(const Vector2& other) const;
    double GetDistanceSquared(const Vector2& other) const;
    double GetHeading() const;
    double GetHeadingFrom(const Vector2& other) const;

    double GetX() const;
    double GetY() const;

    void SetX(double i);
    void SetY(double i);

    double GetLength() const;
    Vector2 GetUnitVector() const;
    Vector2 MoveDistance(const Vector2& towards, double distance) const;
    Vector2 GetMeanWith(const Vector2& other) const;
    Vector2 operator-(const Vector2& other) const;
    Vector2 operator+(const Vector2& other) const;
    Vector2 operator*(double mult) const;
    double GetDotProductWith(const Vector2& other) const;

    friend std::ostream& operator<<(std::ostream& out, const Vector2& c);

  protected:
    double m_x = 0;
    double m_y = 0;
};
}  //namespace mpmca::predict::inertial