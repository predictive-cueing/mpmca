/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <vector>

#include "mpmca/predict/inertial/interpolatable_cubic_spline.hpp"
#include "mpmca/predict/inertial/path_point_interpolatable.hpp"
#include "mpmca/predict/inertial/vector_2.hpp"

namespace mpmca::predict::inertial {
namespace testing {
class FractionCircleTest_DetailedCircleTest_Test;
class FractionCircleTest_AngleBetweenTest_Test;
}  //namespace testing
class FractionCircle : public PathPointInterpolatable<InterpolatableCubicSpline>
{
  public:
    FractionCircle(const PathPoint& p);
    using PathPointInterpolatable<InterpolatableCubicSpline>::PathPointInterpolatable;

    double GetFraction(const Vector2& p) const;
    void Finalize();

    template <class TA>
    void self(const TA& s)
    {
        m_points[1] = {s.GetX(), s.GetY()};
        m_was_set[1] = true;
    }

    template <class TA>
    void GetAdjacentPoint(int index_difference, double ds, const TA& other)
    {
        if (index_difference == -1) {
            m_points[0] = {other.GetX(), other.GetY()};
            m_was_set[0] = true;
        }
        else if (index_difference == 1) {
            m_points[2] = {other.GetX(), other.GetY()};
            m_was_set[2] = true;
        }
    }

    int GetNumPointsRequiredBehind() const { return 1; }
    int GetNumPointsRequiredAhead() const { return 1; }

    bool HasCircle() const { return m_has_circle; }
    bool HasNext() const { return m_has_next; }
    bool HasPrevious() const { return m_has_previous; }

  private:
    friend testing::FractionCircleTest_DetailedCircleTest_Test;
    friend testing::FractionCircleTest_AngleBetweenTest_Test;

    static bool GetAngleIsBetween(double psi1, double psi2, double psi_test);
    double GetFractionNoCircle(const Vector2& p) const;
    void CalculateCircle();
    double CheckFraction(double fraction) const;

    std::array<Vector2, 3> m_points;
    std::array<bool, 3> m_was_set;
    Vector2 m_circle_center;
    Vector2 m_vector_prev;
    Vector2 m_vector_next;
    bool m_has_next;
    bool m_has_previous;
    bool m_has_circle;
    double m_psi_next;
    double m_psi_prev;
    double m_psi_mid;
    double m_psi_next_prev_diff;
    double m_ds_prev;
    double m_ds_next;
    double m_s_circle;
};
}  //namespace mpmca::predict::inertial