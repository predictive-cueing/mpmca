/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/messages/automotive_prediction_horizon.hpp"
#include "mpmca/messages/automotive_prediction_parameters.hpp"
#include "mpmca/messages/automotive_signals.hpp"
#include "mpmca/predict/inertial/model.hpp"
#include "mpmca/predict/inertial/path.hpp"
#include "mpmca/predict/inertial/path_point.hpp"
#include "mpmca/predict/inertial/path_searcher_segmented.hpp"
#include "mpmca/predict/inertial/signal.hpp"
#include "mpmca/predict/inertial/vehicle_state.hpp"
#include "mpmca/utilities/empty_time_measurement.hpp"
#include "mpmca/utilities/map_1d.hpp"
#include "mpmca/utilities/map_2d.hpp"
#include "mpmca/utilities/scalar.hpp"
#include "mpmca/utilities/time_measurement.hpp"
#include "mpmca/utilities/tunable.hpp"

namespace mpmca::predict::inertial {
class DriverMinimal : public Model<27, 5>
{
  public:
    DriverMinimal(utilities::ConfigurationPtr config, const utilities::Horizon&, const std::string& name,
                  bool is_for_control = false);
    DriverMinimal(const utilities::Horizon& t, const std::string& name, bool is_for_control = false);

    void SetEndOfPathReached(bool eop) { m_end_of_path_reached = eop; }
    void UpdateEndOfPathReached(bool eop) { m_end_of_path_reached = m_end_of_path_reached || eop; }

    bool GetEndOfPathReached() const { return m_end_of_path_reached; }
    void PrepareTick(PathPtr path) override;

    template <class Car>
    void Step(Car& car, PathPtr, int j);

    template <typename T>
    void ReadMessage(const T& message);

    template <typename MT>
    void UpdateMessage(MT& message);

    Signal<DriverMinimal> u_t;  //0
    Signal<DriverMinimal> u_e;  //1
    Signal<DriverMinimal> lat_e;  //2
    Signal<DriverMinimal> lat_e_dot;  //3
    Signal<DriverMinimal> psi_t;  //4
    Signal<DriverMinimal> psi_e;  //5
    Signal<DriverMinimal> r_c;  //6
    Signal<DriverMinimal> a_x_c;  //7
    Signal<DriverMinimal> steer;  //8
    Signal<DriverMinimal> throttle;  //9
    Signal<DriverMinimal> brake;  //10
    Signal<DriverMinimal> clutch;  //11
    Signal<DriverMinimal> shift;  //12
    Signal<DriverMinimal> throttle_dot;  //13
    Signal<DriverMinimal> brake_dot;  //14
    Signal<DriverMinimal> x_path;  //15
    Signal<DriverMinimal> y_path;  //16
    Signal<DriverMinimal> z_path;  //17
    Signal<DriverMinimal> phi_path;  //18
    Signal<DriverMinimal> theta_path;  //19
    Signal<DriverMinimal> psi_path;  //20
    Signal<DriverMinimal> u_standard_deviation;  //21
    Signal<DriverMinimal> u_standard_deviation_t;  //22
    Signal<DriverMinimal> disturbance_r_c;  //23
    Signal<DriverMinimal> disturbance_a_x;  //24
    Signal<DriverMinimal> gear_change;  //25
    Signal<DriverMinimal> a_x_model;  //26

    utilities::Scalar K_u_e_throttle;
    utilities::Scalar K_u_e_brake;
    utilities::Scalar K_lat_e;
    utilities::Scalar T_lat_e;
    utilities::Scalar K_psi_t;
    utilities::Scalar K_psi_e;
    utilities::Tunable<utilities::Scalar> initial_world_X_vehicle_m;
    utilities::Tunable<utilities::Scalar> initial_world_Y_vehicle_m;
    utilities::Tunable<utilities::Scalar> initial_world_Z_vehicle_m;
    utilities::Tunable<utilities::Scalar> initial_world_psi_vehicle_rad;

    utilities::Tunable<utilities::Scalar> gravity_m_s2;
    utilities::Tunable<utilities::Scalar> vehicle_mass_kg;
    utilities::Tunable<utilities::Scalar> car_S_aero_force_kg_m;
    utilities::Tunable<utilities::Scalar> car_S_brake_force_N_fraction;
    utilities::Tunable<utilities::Map1D> car_M1D_gear_ratio_1_m;
    utilities::Tunable<utilities::Map2D> car_M2D_engine_torque_Nm;

  private:
    template <class Car>
    void VehicleStates(Car& car, int i);

    template <class Car>
    void LookAtPath(const Car& car, PathPtr path, int j);

    template <class Car>
    void Derivatives(Car& car, int j);

    template <class Car>
    void HorizonDerivatives(Car& car, int j);

    template <class Car>
    void ControlInputs(Car& car, int j);

    template <class Car>
    void SpeedCurves(Car& car, int j);

    template <class Car>
    void LongitudinalControl(Car& car, int j);

    template <class Car>
    void LateralControl(Car& car, PathPtr path, int j);

    template <class Car>
    void CalculateModelLongitudinalAcceleration(Car& car, int j);

    void Extrapolate(int j);
    void ExtrapolateDisturbances(int j);
    void CheckDistance(PathPtr path, const PathIndex& path_index, const Vector6& point) const;

    double m_d_r_c = 0;
    double m_rd_first = 0;

    double m_d_a_x = 0;
    double m_d_a_x_dot_first = 0;

    double m_d_throttle_c = 0;
    double m_throttle_dot_first = 0;

    double m_d_brake_c = 0;
    double m_brake_dot_first = 0;
    double m_u_standard_deviation_w = 0;
    double m_u_standard_deviation_w_t = 0;

    PathSearcherSegmented m_searcher_global;
    PathSearcherSegmented m_searcher_horizon;

    PathIndex m_path_index_current;
    PathPoint m_path_point_current;
    VehicleState m_vehicle_state_current;

    bool m_end_of_path_reached;
    const double m_max_path_distance;

    long m_cycle;
};

template <>
inline void DriverMinimal::ReadMessage(const mpmca::messages::AutomotivePredictionParameters& message)
{
    initial_world_X_vehicle_m.SetNextValue(message.initial_world_X_vehicle_m);
    initial_world_Y_vehicle_m.SetNextValue(message.initial_world_Y_vehicle_m);
    initial_world_Z_vehicle_m.SetNextValue(message.initial_world_Z_vehicle_m);
    initial_world_psi_vehicle_rad.SetNextValue(message.initial_world_psi_vehicle_rad);
    gravity_m_s2.SetNextValue(message.gravity_m_s2);
    vehicle_mass_kg.SetNextValue(message.vehicle_mass_kg);
    car_S_aero_force_kg_m.SetNextValue(message.car_S_aero_force_kg_m);
    car_S_brake_force_N_fraction.SetNextValue(message.car_S_brake_force_N_fraction);
    car_M1D_gear_ratio_1_m.SetNextValue(message.car_M1D_gear_ratio_1_m);
    car_M2D_engine_torque_Nm.SetNextValue(message.car_M2D_engine_torque_Nm);
}

template <>
inline void DriverMinimal::UpdateMessage(messages::AutomotivePredictionHorizon& message)
{
    for (size_t i = 0; i < m_horizon.GetNPrediction(); ++i) {
        message.world_X_path_m.at(i) = x_path.GetValue(i + m_horizon_start_index);
        message.world_Y_path_m.at(i) = y_path.GetValue(i + m_horizon_start_index);
        message.world_Z_path_m.at(i) = z_path.GetValue(i + m_horizon_start_index);
        message.world_phi_path_rad.at(i) = phi_path.GetValue(i + m_horizon_start_index);
        message.world_theta_path_rad.at(i) = theta_path.GetValue(i + m_horizon_start_index);
        message.world_psi_path_rad.at(i) = psi_path.GetValue(i + m_horizon_start_index);
    }
}

template <>
inline void DriverMinimal::ReadMessage(const mpmca::messages::AutomotiveSignals& message)
{
    gear_change.Init(message.car_automatic_gear_change_boolean);
}

template <typename T>
void DriverMinimal::ReadMessage(const T& message)
{
}

template <typename MT>
void DriverMinimal::UpdateMessage(MT& message)
{
}

}  //namespace mpmca::predict::inertial