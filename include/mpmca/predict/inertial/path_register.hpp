/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <map>

#include "mpmca/predict/inertial/path.hpp"
namespace mpmca::predict::inertial {
class PathRegister
{
  public:
    static PathRegister* Instance()
    {
        static PathRegister path_register;
        return &path_register;
    }
    PathPtr StorePath(PathPtr path)
    {
        if (PathExists(path->GetName()))
            throw std::runtime_error("A path with name " + path->GetName() + " was already registered.");

        std::string name = path->GetName();
        m_path_register.insert(std::pair<std::string, PathPtr>(name, path));

        return path;
    }

    bool PathExists(const std::string& name)
    {
        auto it = m_path_register.find(name);
        return it != m_path_register.end();
    }
    PathPtr GetPath(const std::string& name)
    {
        auto it = m_path_register.find(name);
        if (it == m_path_register.end())
            throw std::runtime_error("A path named " + name + " was not registered (yet).");

        return it->second;
    }

    std::map<std::string, PathPtr> m_path_register;
};
}  //namespace mpmca::predict::inertial
