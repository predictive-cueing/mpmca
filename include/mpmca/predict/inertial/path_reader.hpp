/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/predict/inertial/path_point.hpp"
#include "mpmca/utilities/configuration.hpp"

namespace mpmca::predict::inertial {
class PathReader
{
  public:
    template <class T>
    static std::shared_ptr<T> ReadPath(utilities::ConfigurationPtr config, const std::string& name = "DefaultPathName");

    template <class T>
    static std::shared_ptr<T> ReadPath(const std::string& name, const std::string& filename, double ds_tol = 0.1);

  private:
    static std::vector<PathPoint> ReadPathPointVector(const std::string& name, const std::string& filename,
                                                      bool& closed);
};
}  //namespace mpmca::predict::inertial
