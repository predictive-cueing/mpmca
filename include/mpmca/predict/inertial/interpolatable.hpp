/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <functional>

namespace mpmca::predict::inertial {
class Interpolatable
{
  public:
    Interpolatable(double value = 0.0);
    double GetValue() const;
    virtual void SetValue(double value);
    virtual int GetNumPointsRequiredBehind() const;
    virtual int GetNumPointsRequiredAhead() const;
    virtual double GetInterpolated(double fraction) const;
    virtual double Derivative(double fraction) const;
    virtual void GetAdjacentPoint(int index_difference, double ds, const Interpolatable& other);
    virtual void Finalize(std::function<double(const double&, const double&)> difference_function =
                              [](const double& x, const double& y) { return x - y; });

  protected:
    double m_value = 0.0;
    bool m_ready = false;
};
}  //namespace mpmca::predict::inertial