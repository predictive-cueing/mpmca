/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <cmath>

#include "mpmca/messages/automotive_prediction_horizon.hpp"
#include "mpmca/messages/automotive_prediction_parameters.hpp"
#include "mpmca/messages/automotive_signals.hpp"
#include "mpmca/messages/vehicle_state_signals.hpp"
#include "mpmca/predict/inertial/inertial_output.hpp"
#include "mpmca/predict/inertial/model.hpp"
#include "mpmca/predict/inertial/path.hpp"
#include "mpmca/predict/inertial/signal.hpp"
#include "mpmca/utilities/map_1d.hpp"
#include "mpmca/utilities/map_2d.hpp"
#include "mpmca/utilities/scalar.hpp"
#include "mpmca/utilities/tunable.hpp"
namespace mpmca::predict::inertial {
class CarMinimal : public Model<25, 5>
{
  public:
    CarMinimal(utilities::ConfigurationPtr config, const utilities::Horizon& horizon, const std::string& name,
               bool is_for_control = false);
    CarMinimal(const utilities::Horizon& horizon, const std::string& name, bool is_for_control = false);

    Signal<CarMinimal> x;  // 0
    Signal<CarMinimal> y;  // 1
    Signal<CarMinimal> z;  // 2
    Signal<CarMinimal> phi;  // 3
    Signal<CarMinimal> theta;  // 4
    Signal<CarMinimal> psi;  // 5
    Signal<CarMinimal> p;  // 6
    Signal<CarMinimal> q;  // 7
    Signal<CarMinimal> r;  // 8
    Signal<CarMinimal> pd;  // 9
    Signal<CarMinimal> qd;  // 10
    Signal<CarMinimal> rd;  // 11
    Signal<CarMinimal> a_x;  // 12
    Signal<CarMinimal> a_y;  // 13
    Signal<CarMinimal> a_z;  // 14
    Signal<CarMinimal> u;  // 15
    Signal<CarMinimal> v;  // 16
    Signal<CarMinimal> w;  // 17
    Signal<CarMinimal> beta;  // 18
    Signal<CarMinimal> gear;  // 19
    Signal<CarMinimal> delta_f;  // 20
    Signal<CarMinimal> a_x_dot;  // 21
    Signal<CarMinimal> omega_engine;  // 22
    Signal<CarMinimal> T_engine;  // 23
    Signal<CarMinimal> disturbance_f_z;  // 24

    utilities::Tunable<utilities::Scalar> car_C_distance_rear_axle_m;

    template <typename MT>
    void ReadMessage(const MT& message);

    template <typename MT>
    void UpdateMessage(MT& message);

    template <class Human>
    void Step(Human& hum, InertialOutput& pred, PathPtr path, int j);

  private:
    void ExtrapolateDisturbances(int j);
    void CalculateBeta(int j);
};

template <class Human>
void CarMinimal::Step(Human& human, InertialOutput& pred, PathPtr path, int j)
{
    m_time_measurement.StartCycle();

    CalculateTakeoverTime(j);
    ExtrapolateDisturbances(j);

    z.SetValue(j, human.z_path.GetValue(j));
    phi.SetValue(j, human.phi_path.GetValue(j));
    theta.SetValue(j, human.theta_path.GetValue(j));

    z.CorrectOptimized(j, GetTakeoverFadeVal2());
    phi.CorrectOptimized(j, GetTakeoverFadeVal2());
    theta.CorrectOptimized(j, GetTakeoverFadeVal2());

    if (!IsFirstStepInHorizon(j))
        gear.SetValue(j, gear.GetValue(j - 1));

    CalculateBeta(j);

    v.SetValue(j, u.GetValue(j) * std::tan(beta.GetValue(j)));
    a_y.SetValue(j, r.GetValue(j) * u.GetValue(j));
    a_y.CorrectOptimized(j, GetTakeoverFadeVal2());

    a_z.SetValue(j, 0);
    a_z.CorrectOptimized(j, GetTakeoverFadeVal2());

    p.SetValue(j, 0);
    q.SetValue(j, 0);

    p.CorrectOptimized(j, GetTakeoverFadeVal2());
    q.CorrectOptimized(j, GetTakeoverFadeVal2());

    if (!IsLastStepInHorizon(j)) {
        psi.SetValue(j + 1,
                     psi.GetValue(j) + r.GetValue(j) * m_horizon.GetPredictionHorizonDeltaTime(ToHorizonIndex(j)));
        u.SetValue(j + 1, std::max(0.0, u.GetValue(j) + m_horizon.GetPredictionHorizonDeltaTime(ToHorizonIndex(j)) *
                                                            a_x.GetValue(j)));
        double psi_beta = psi.GetValue(j) + 0 * beta.GetValue(j);
        x.SetValue(j + 1, x.GetValue(j) + u.GetValue(j) * m_horizon.GetPredictionHorizonDeltaTime(ToHorizonIndex(j)) *
                                              std::cos(psi_beta));
        y.SetValue(j + 1, y.GetValue(j) + u.GetValue(j) * m_horizon.GetPredictionHorizonDeltaTime(ToHorizonIndex(j)) *
                                              std::sin(psi_beta));
    }

    double g_cos_theta = 9.81 * std::cos(theta.GetValue(j));
    pred.f_x.SetValue(j, -9.81 * std::sin(theta.GetValue(j)) - a_x.GetValue(j));
    pred.f_y.SetValue(j, g_cos_theta * std::sin(phi.GetValue(j)) - a_y.GetValue(j));
    pred.f_z.SetValue(j, g_cos_theta * std::cos(phi.GetValue(j)) - a_z.GetValue(j));
    pred.omega_x.SetValue(j, p.GetValue(j));
    pred.omega_y.SetValue(j, q.GetValue(j));
    pred.omega_z.SetValue(j, r.GetValue(j));
    pred.alpha_x.SetValue(j, 0.);
    pred.alpha_y.SetValue(j, 0.);
    pred.alpha_z.SetValue(j, rd.GetValue(j));

    if (IsForControl() && !std::isnan(disturbance_f_z.GetValue(j)))
        pred.f_z.SetValue(j, pred.f_z.GetValue(j) + disturbance_f_z.GetValue(j));
}

template <>
inline void CarMinimal::ReadMessage(const messages::AutomotivePredictionParameters& message)
{
    car_C_distance_rear_axle_m.SetNextValue(message.car_C_distance_rear_axle_m);
}
template <>
inline void CarMinimal::ReadMessage(const messages::VehicleStateSignals& message)
{
    x.Init(message.world_X_vehicle_m);
    y.Init(message.world_Y_vehicle_m);
    z.Init(message.world_Z_vehicle_m);
    phi.Init(message.world_phi_vehicle_rad);
    theta.Init(message.world_theta_vehicle_rad);
    psi.Init(message.world_psi_vehicle_rad);
    p.Init(message.body_p_vehicle_rad_s);
    q.Init(message.body_q_vehicle_rad_s);
    r.Init(message.body_r_vehicle_rad_s);
    pd.Init(message.body_p_dot_vehicle_rad_s2);
    qd.Init(message.body_q_dot_vehicle_rad_s2);
    rd.Init(message.body_r_dot_vehicle_rad_s2);
    a_x.Init(message.body_a_x_vehicle_m_s2);
    a_y.Init(message.body_a_y_vehicle_m_s2);
    a_z.Init(message.body_a_z_vehicle_m_s2);
    u.Init(message.body_u_vehicle_m_s);
    v.Init(message.body_v_vehicle_m_s);
    w.Init(message.body_w_vehicle_m_s);
}
template <>
inline void CarMinimal::ReadMessage(const messages::AutomotiveSignals& message)
{
    gear.Init(message.car_transmission_gear_integer);
    omega_engine.Init(message.car_engine_rotational_velocity_rad_s);
    T_engine.Init(message.car_engine_torque_Nm);
}

template <>
inline void CarMinimal::UpdateMessage(messages::AutomotivePredictionHorizon& message)
{
    for (size_t i = 0; i < m_horizon.GetNPrediction(); ++i) {
        message.world_X_vehicle_m.at(i) = x.GetValue(i + m_horizon_start_index);
        message.world_Y_vehicle_m.at(i) = y.GetValue(i + m_horizon_start_index);
        message.world_Z_vehicle_m.at(i) = z.GetValue(i + m_horizon_start_index);
        message.world_phi_vehicle_rad.at(i) = phi.GetValue(i + m_horizon_start_index);
        message.world_theta_vehicle_rad.at(i) = theta.GetValue(i + m_horizon_start_index);
        message.world_psi_vehicle_rad.at(i) = psi.GetValue(i + m_horizon_start_index);
        message.body_p_vehicle_rad_s.at(i) = p.GetValue(i + m_horizon_start_index);
        message.body_q_vehicle_rad_s.at(i) = q.GetValue(i + m_horizon_start_index);
        message.body_r_vehicle_rad_s.at(i) = r.GetValue(i + m_horizon_start_index);
        message.body_p_dot_vehicle_rad_s2.at(i) = pd.GetValue(i + m_horizon_start_index);
        message.body_q_dot_vehicle_rad_s2.at(i) = qd.GetValue(i + m_horizon_start_index);
        message.body_r_dot_vehicle_rad_s2.at(i) = rd.GetValue(i + m_horizon_start_index);
        message.body_a_x_vehicle_m_s2.at(i) = a_x.GetValue(i + m_horizon_start_index);
        message.body_a_y_vehicle_m_s2.at(i) = a_y.GetValue(i + m_horizon_start_index);
        message.body_a_z_vehicle_m_s2.at(i) = a_z.GetValue(i + m_horizon_start_index);
        message.body_u_vehicle_m_s.at(i) = u.GetValue(i + m_horizon_start_index);
        message.body_v_vehicle_m_s.at(i) = v.GetValue(i + m_horizon_start_index);
        message.body_w_vehicle_m_s.at(i) = w.GetValue(i + m_horizon_start_index);
        message.car_transmission_gear_integer.at(i) = gear.GetValue(i + m_horizon_start_index);
    }
}

template <typename MT>
void CarMinimal::ReadMessage(const MT& message)
{
}

template <typename MT>
void CarMinimal::UpdateMessage(MT& message)
{
}

}  //namespace mpmca::predict::inertial