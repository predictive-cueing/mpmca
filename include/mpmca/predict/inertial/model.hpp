/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/linear_algebra.hpp"
#include "mpmca/predict/inertial/path.hpp"
#include "mpmca/utilities/configuration.hpp"
#include "mpmca/utilities/horizon.hpp"
#include "mpmca/utilities/logger.hpp"
#include "mpmca/utilities/math.hpp"
#include "mpmca/utilities/scalar.hpp"
#include "mpmca/utilities/time_measurement.hpp"

namespace mpmca::predict::inertial {
template <size_t T_Nsignals, size_t T_NHist>
class Model
{
  public:
    typedef mpmca::Vector<T_Nsignals> SignalVector;

  protected:
    const std::string m_name;
    utilities::Logger m_logger;
    const utilities::Horizon m_horizon;
    int64_t GetCurrentTimeMs() const;

    void SetSignalNominalValue(size_t id, double v);

    const bool m_for_control;

    const size_t m_n_horizon_prediction;
    const size_t m_n_horizon_total;
    const size_t m_history_start_index;
    const size_t m_history_end_index;
    const size_t m_horizon_start_index;
    const size_t m_horizon_end_index;

    double m_takeover = 0;
    double m_takeover_fade_val2 = 0;
    double m_s_takeover = 0;
    double m_s_takeover_fade_val2 = 0;
    double m_t_takeover = 0;
    double m_t_takeover_fade_val2 = 0;

    int64_t m_t = 0;

    bool m_first_time = true;
    bool m_time_jumped = true;

    std::vector<SignalVector> m_signal_matrix;
    SignalVector m_init_value_vector;
    SignalVector m_nominal_value_vector;

    size_t m_n_signals_added = 0;

    std::vector<std::string> m_signal_names;
    std::vector<size_t> m_disturbance_idx;
    std::map<std::string, size_t> m_signal_indices;

  public:
    Model(const std::string& name, const utilities::Horizon& horizon, bool is_for_control);

    const std::string& GetName() const;
    SignalVector GetPredictionPlanVector(size_t i) const;

    size_t AddSignal(const std::string& name, double zero_value, bool is_disturbance);

    size_t GetNTotal() const;
    size_t GetNPrediction() const;
    size_t GetNHistory() const;
    size_t GetNSignals() const;

    size_t GetHistoryStartIndex() const;
    size_t GetHistoryEndIndex() const;
    size_t GetHorizonStartIndex() const;
    size_t GetHorizonEndIndex() const;

    bool IsFirstStepInHorizon(size_t j) const;
    bool IsLastStepInHorizon(size_t j) const;
    bool IsFirstStepInHistory(size_t j) const;
    bool IsLastStepInHistory(size_t j) const;

    void ResetDisturbances();
    void ShiftBack();
    void Init(const SignalVector& sv);
    void InitTime(int64_t t);

    void InitSignal(size_t signal_id, double value);
    double GetSignalValue(size_t signal_id, size_t horizon_index);
    double GetInitializedValue(size_t signal_id);
    void SetSignalValue(size_t signal_id, size_t horizon_index, double value);
    void SetNominalSignalValue(size_t signal_id, double value);
    void CopyInitVector();

    const std::vector<std::string>& GetSignalNames() const;

    SignalVector GetCurrentSignalVector();
    SignalVector GetUndisturbedCurrentSignalVector();
    SignalVector GetNominalSignalVector() const;

    size_t GetSignalIndex(const std::string& signal_name);

    void SetSignalValue(const std::string& signal_name, SignalVector& vec, double val);

    void CalculateTakeoverDistance(size_t j, double u);
    void CalculateTakeoverTime(size_t j);

    double GetTakeover() const;
    double GetTakeoverFade(double v0, double v1) const;
    double GetTakeoverFade(double v0, double v1, double fade) const;
    double GetTakeoverFade(double v0, double v1, double fade, double fade_start) const;
    double GetTakeoverFadeVal2() const;

    double GetTakeoverDistance() const;
    double GetTakeoverDistanceFade(double v0, double v1) const;
    double GetTakeoverDistanceFadeVal2() const;

    double GetTakeoverTime() const;
    double GetTakeoverTimeFade(double v0, double v1) const;
    double GetTakeoverTimeFadeVal2() const;

    bool GetTimeJumped() const;
    bool IsForControl() const { return m_for_control; };

    virtual void PrepareTick(PathPtr path) {};

    void PrintStatisticsMs() const { m_time_measurement.PrintStatisticsMs(); };
    void PrintStatisticsUs() const { m_time_measurement.PrintStatisticsUs(); };
    void PrintStatisticsNs() const { m_time_measurement.PrintStatisticsNs(); };
    void ResetStatistics() { m_time_measurement.ResetStatistics(); };

    utilities::Scalar s_fade;
    utilities::Scalar t_fade;

  protected:
    utilities::TimeMeasurement m_time_measurement;
    size_t ToHorizonIndex(size_t j) const;
};

template <size_t T_Nsignals, size_t T_NHist>
Model<T_Nsignals, T_NHist>::Model(const std::string& name, const utilities::Horizon& horizon, bool is_for_control)
    : m_name(name)
    , m_logger(m_name, "Model")
    , m_horizon(horizon)
    , m_for_control(is_for_control)
    , m_n_horizon_prediction(horizon.GetNPrediction())
    , m_n_horizon_total(m_n_horizon_prediction + T_NHist)
    , m_history_start_index(0)
    , m_history_end_index(T_NHist - 1)
    , m_horizon_start_index(T_NHist)
    , m_horizon_end_index(m_n_horizon_total - 1)
    , m_init_value_vector(Model<T_Nsignals, T_NHist>::SignalVector::Zero())
    , m_nominal_value_vector(Model<T_Nsignals, T_NHist>::SignalVector::Zero())
    , s_fade(20.)
    , t_fade(1.)
{
    if (m_for_control) {
        s_fade.SetValue(0.);
        t_fade.SetValue(0.);
    }

    m_signal_names.resize(T_Nsignals);
    m_signal_matrix.resize(m_n_horizon_total, m_nominal_value_vector);

    ResetDisturbances();
}
template <size_t T_Nsignals, size_t T_NHist>
const std::string& Model<T_Nsignals, T_NHist>::GetName() const
{
    return m_name;
}

template <size_t T_Nsignals, size_t T_NHist>
void Model<T_Nsignals, T_NHist>::SetSignalNominalValue(size_t id, double v)
{
    m_nominal_value_vector[id] = v;
}

template <size_t T_Nsignals, size_t T_NHist>
const std::vector<std::string>& Model<T_Nsignals, T_NHist>::GetSignalNames() const
{
    return m_signal_names;
}

template <size_t T_Nsignals, size_t T_NHist>
typename Model<T_Nsignals, T_NHist>::SignalVector Model<T_Nsignals, T_NHist>::GetNominalSignalVector() const
{
    return m_nominal_value_vector;
}

template <size_t T_Nsignals, size_t T_NHist>
void Model<T_Nsignals, T_NHist>::ShiftBack()
{
    for (size_t i = m_history_start_index; i <= (m_horizon_end_index - 1); ++i)
        m_signal_matrix[i] = m_signal_matrix[i + 1];
}

template <size_t T_Nsignals, size_t T_NHist>
double Model<T_Nsignals, T_NHist>::GetSignalValue(size_t signal_id, size_t horizon_index)
{
    return m_signal_matrix[horizon_index][signal_id];
}

template <size_t T_Nsignals, size_t T_NHist>
void Model<T_Nsignals, T_NHist>::SetSignalValue(size_t signal_id, size_t horizon_index, double value)
{
    m_signal_matrix[horizon_index][signal_id] = value;
}

template <size_t T_Nsignals, size_t T_NHist>
void Model<T_Nsignals, T_NHist>::SetNominalSignalValue(size_t signal_id, double value)
{
    m_nominal_value_vector[signal_id] = value;
}

template <size_t T_Nsignals, size_t T_NHist>
int64_t Model<T_Nsignals, T_NHist>::GetCurrentTimeMs() const
{
    return m_t;
}
template <size_t T_Nsignals, size_t T_NHist>
void Model<T_Nsignals, T_NHist>::InitSignal(size_t signal_id, double value)
{
    m_init_value_vector[signal_id] = value;
}
template <size_t T_Nsignals, size_t T_NHist>
double Model<T_Nsignals, T_NHist>::GetInitializedValue(size_t signal_id)
{
    return m_init_value_vector[signal_id];
}
template <size_t T_Nsignals, size_t T_NHist>
void Model<T_Nsignals, T_NHist>::CopyInitVector()
{
    m_signal_matrix[m_horizon_start_index] = m_init_value_vector;
}
template <size_t T_Nsignals, size_t T_NHist>
void Model<T_Nsignals, T_NHist>::Init(const SignalVector& sv)
{
    m_init_value_vector = sv;
}
template <size_t T_Nsignals, size_t T_NHist>
void Model<T_Nsignals, T_NHist>::CalculateTakeoverDistance(size_t j, double u)
{
    if (IsFirstStepInHorizon(j))
        m_s_takeover = 0;
    else
        m_s_takeover += (u * m_horizon.GetPredictionHorizonDeltaTime(ToHorizonIndex(j)));

    m_s_takeover_fade_val2 = utilities::Math::GetCosineFadeVal2(s_fade.GetValue(), m_s_takeover);

    m_takeover = m_s_takeover;
    m_takeover_fade_val2 = m_s_takeover_fade_val2;
}
template <size_t T_Nsignals, size_t T_NHist>
size_t Model<T_Nsignals, T_NHist>::ToHorizonIndex(size_t j) const
{
    return j - T_NHist;
}
template <size_t T_Nsignals, size_t T_NHist>
void Model<T_Nsignals, T_NHist>::CalculateTakeoverTime(size_t j)
{
    if (IsFirstStepInHorizon(j))
        m_t_takeover = 0;
    else
        m_t_takeover += m_horizon.GetPredictionHorizonDeltaTime(ToHorizonIndex(j));

    m_t_takeover_fade_val2 = utilities::Math::GetCosineFadeVal2(t_fade.GetValue(), m_t_takeover);

    m_takeover = m_t_takeover;
    m_takeover_fade_val2 = m_t_takeover_fade_val2;
}

template <size_t T_Nsignals, size_t T_NHist>
double Model<T_Nsignals, T_NHist>::GetTakeoverDistance() const
{
    return m_s_takeover;
}

template <size_t T_Nsignals, size_t T_NHist>
double Model<T_Nsignals, T_NHist>::GetTakeoverTime() const
{
    return m_t_takeover;
}

template <size_t T_Nsignals, size_t T_NHist>
double Model<T_Nsignals, T_NHist>::GetTakeover() const
{
    return m_takeover;
}

template <size_t T_Nsignals, size_t T_NHist>
double Model<T_Nsignals, T_NHist>::GetTakeoverTimeFade(double v0, double v1) const
{
    return utilities::Math::GetCosineFadeOptimized(v0, v1, m_t_takeover_fade_val2);
}

template <size_t T_Nsignals, size_t T_NHist>
double Model<T_Nsignals, T_NHist>::GetTakeoverDistanceFade(double v0, double v1) const
{
    return utilities::Math::GetCosineFadeOptimized(v0, v1, m_s_takeover_fade_val2);
}
template <size_t T_Nsignals, size_t T_NHist>
double Model<T_Nsignals, T_NHist>::GetTakeoverFade(double v0, double v1) const
{
    return utilities::Math::GetCosineFadeOptimized(v0, v1, m_takeover_fade_val2);
}

template <size_t T_Nsignals, size_t T_NHist>
double Model<T_Nsignals, T_NHist>::GetTakeoverFade(double v0, double v1, double fade) const
{
    return utilities::Math::GetCosineFade(v0, v1, fade, m_takeover);
}

template <size_t T_Nsignals, size_t T_NHist>
double Model<T_Nsignals, T_NHist>::GetTakeoverFade(double v0, double v1, double fade, double fade_start) const
{
    return utilities::Math::GetCosineFade(v0, v1, fade, m_takeover - fade_start);
}

template <size_t T_Nsignals, size_t T_NHist>
double Model<T_Nsignals, T_NHist>::GetTakeoverTimeFadeVal2() const
{
    return m_t_takeover_fade_val2;
}

template <size_t T_Nsignals, size_t T_NHist>
double Model<T_Nsignals, T_NHist>::GetTakeoverDistanceFadeVal2() const
{
    return m_s_takeover_fade_val2;
}

template <size_t T_Nsignals, size_t T_NHist>
double Model<T_Nsignals, T_NHist>::GetTakeoverFadeVal2() const
{
    return m_takeover_fade_val2;
}

template <size_t T_Nsignals, size_t T_NHist>
typename Model<T_Nsignals, T_NHist>::SignalVector Model<T_Nsignals, T_NHist>::GetCurrentSignalVector()
{
    return m_signal_matrix[m_horizon_start_index];
}

template <size_t T_Nsignals, size_t T_NHist>
void Model<T_Nsignals, T_NHist>::ResetDisturbances()
{
    for (int i = m_horizon_start_index; i < m_horizon_start_index; ++i) {
        for (int index : m_disturbance_idx)
            m_signal_matrix[i][index] = std::nan("");
    }
}

template <size_t T_Nsignals, size_t T_NHist>
typename Model<T_Nsignals, T_NHist>::SignalVector Model<T_Nsignals, T_NHist>::GetUndisturbedCurrentSignalVector()
{
    SignalVector sv = m_signal_matrix[m_horizon_start_index];
    for (int index : m_disturbance_idx)
        sv[index] = std::nan("");

    return sv;
}

template <size_t T_Nsignals, size_t T_NHist>
void Model<T_Nsignals, T_NHist>::InitTime(int64_t t)
{
    if (m_first_time) {
        m_time_jumped = true;
        m_t = t;
        m_first_time = false;
    }
    else {
        m_time_jumped = t < m_t || t > (m_t + 30);

        if (m_time_jumped)
            m_logger.Warning("The time jumped. Old time : " + std::to_string(m_t) +
                             ", new time : " + std::to_string(t));

        m_t = t;
    }
}

template <size_t T_Nsignals, size_t T_NHist>
bool Model<T_Nsignals, T_NHist>::GetTimeJumped() const
{
    return m_time_jumped;
}

template <size_t T_Nsignals, size_t T_NHist>
bool Model<T_Nsignals, T_NHist>::IsFirstStepInHorizon(size_t j) const
{
    return j == m_horizon_start_index;
}

template <size_t T_Nsignals, size_t T_NHist>
bool Model<T_Nsignals, T_NHist>::IsLastStepInHorizon(size_t j) const
{
    return j == m_horizon_end_index;
}

template <size_t T_Nsignals, size_t T_NHist>
bool Model<T_Nsignals, T_NHist>::IsFirstStepInHistory(size_t j) const
{
    return j == m_history_start_index;
}

template <size_t T_Nsignals, size_t T_NHist>
bool Model<T_Nsignals, T_NHist>::IsLastStepInHistory(size_t j) const
{
    return j == m_history_end_index;
}

template <size_t T_Nsignals, size_t T_NHist>
typename Model<T_Nsignals, T_NHist>::SignalVector Model<T_Nsignals, T_NHist>::GetPredictionPlanVector(size_t i) const
{
    return m_signal_matrix[m_horizon_start_index + i];
}

template <size_t T_Nsignals, size_t T_NHist>
size_t Model<T_Nsignals, T_NHist>::AddSignal(const std::string& name, double nominal_value, bool is_disturbance)
{
    if (m_n_signals_added >= T_Nsignals)
        throw std::runtime_error("When adding signal " + name +
                                 ", you are trying to add more signals than this model allows.");

    size_t this_index = m_n_signals_added;
    m_n_signals_added++;

    m_signal_names[this_index] = name;
    m_signal_indices[name] = this_index;

    if (is_disturbance)
        m_disturbance_idx.push_back(this_index);

    SetSignalNominalValue(this_index, nominal_value);

    return m_signal_indices[name];
}

template <size_t T_Nsignals, size_t T_NHist>
size_t Model<T_Nsignals, T_NHist>::GetSignalIndex(const std::string& signal_name)
{
    if (m_signal_indices.find(signal_name) != m_signal_indices.end())
        return m_signal_indices[signal_name];
    else {
        throw std::runtime_error("Signal " + signal_name + " is not present in model " + GetName());
        return 0;
    }
}

template <size_t T_Nsignals, size_t T_NHist>
void Model<T_Nsignals, T_NHist>::SetSignalValue(const std::string& signal_name, SignalVector& vec, double val)
{
    if (m_signal_indices.find(signal_name) != m_signal_indices.end())
        vec[m_signal_indices[signal_name]] = val;
}
template <size_t T_Nsignals, size_t T_NHist>
size_t Model<T_Nsignals, T_NHist>::GetNHistory() const
{
    return T_NHist;
}
template <size_t T_Nsignals, size_t T_NHist>
size_t Model<T_Nsignals, T_NHist>::GetNPrediction() const
{
    return m_n_horizon_prediction;
}
template <size_t T_Nsignals, size_t T_NHist>
size_t Model<T_Nsignals, T_NHist>::GetNTotal() const
{
    return m_n_horizon_total;
}
template <size_t T_Nsignals, size_t T_NHist>
size_t Model<T_Nsignals, T_NHist>::GetNSignals() const
{
    return m_n_signals_added;
}
template <size_t T_Nsignals, size_t T_NHist>
size_t Model<T_Nsignals, T_NHist>::GetHorizonStartIndex() const
{
    return m_horizon_start_index;
}

template <size_t T_Nsignals, size_t T_NHist>
size_t Model<T_Nsignals, T_NHist>::GetHorizonEndIndex() const
{
    return m_horizon_end_index;
}

template <size_t T_Nsignals, size_t T_NHist>
size_t Model<T_Nsignals, T_NHist>::GetHistoryStartIndex() const
{
    return m_history_start_index;
}

template <size_t T_Nsignals, size_t T_NHist>
size_t Model<T_Nsignals, T_NHist>::GetHistoryEndIndex() const
{
    return m_history_end_index;
}
}  //namespace mpmca::predict::inertial