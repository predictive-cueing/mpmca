/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/predict/inertial/path_searcher.hpp"

namespace mpmca::predict::inertial {
class PathSearcherGridSearch : public PathSearcher
{
  public:
    const PathIndex& FindIndexLocal(PathPtr path, const Vector6& p) override;
    const PathIndex& FindIndexGlobal(PathPtr path, const Vector6& p) override;
};
}  //namespace mpmca::predict::inertial