/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/predict/inertial/path.hpp"
#include "mpmca/predict/inertial/vector_6.hpp"

namespace mpmca::predict::inertial {
class PathSearcher
{
  public:
    virtual const PathIndex& FindIndexLocal(PathPtr path, const Vector6& p) = 0;
    virtual const PathIndex& FindIndexGlobal(PathPtr path, const Vector6& p) = 0;

    const PathIndex& GetLastPathIndex() const;
    int GetNumIterations() const;
    void CopyState(const PathSearcher& other);

  protected:
    int SearchAllFromTo(PathPtr path, const Vector6& p, int from, int to);

    Vector6 m_last_query_position;
    PathIndex m_last_path_index;
    int m_iterations = 0;
};
}  //namespace mpmca::predict::inertial