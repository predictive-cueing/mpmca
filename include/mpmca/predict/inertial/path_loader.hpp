/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/utilities/configuration.hpp"

namespace mpmca::predict::inertial {
class PathLoader
{
  public:
    PathLoader(utilities::ConfigurationPtr config);

    template <class T>
    static void GenerateOrLoadPath(const std::string& name, utilities::ConfigurationPtr config);
};
}  //namespace mpmca::predict::inertial