/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/predict/inertial/path_searcher.hpp"
#include "mpmca/predict/inertial/path_segment.hpp"

namespace mpmca::predict::inertial {
class PathSearcherSegmented : public PathSearcher
{
  public:
    const PathIndex& FindIndexLocal(PathPtr path, const Vector6& p) override;
    const PathIndex& FindIndexGlobal(PathPtr path, const Vector6& p) override;

  private:
    int SearchSegmented(PathPtr path, const Vector6& p, int from, int to);
    void SearchSegmented(PathPtr path, const Vector6& p, std::vector<PathSegment>& segments, int ident = 0);
    void EvaluateSegment(PathPtr path, const Vector6& p, PathSegment& segment);
    void SearchEntireSegment(PathPtr path, const Vector6& p, PathSegment& segments);

    const bool m_verbose = false;
    int m_num_points_search_max = 10;
    int m_minimum_number_of_sub_segments = 2;
    int m_maximum_number_of_sub_segments = 10;
    int m_num_segments_advance = 3;
    int m_maximum_points_local_search = 300;
};
}  //namespace mpmca::predict::inertial