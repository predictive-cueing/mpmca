/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <iostream>

#include "mpmca/utilities/math.hpp"

namespace mpmca::predict::inertial {

template <class Model>
class Signal
{
  private:
    const int m_id;
    const int m_horizon_start_index;
    const int m_horizon_end_index;

    const int m_history_start_index;
    const int m_history_end_index;

    const std::string m_name;
    Model* m_parent_model;
    const bool m_is_disturbance;

    double m_first_step_diff;

    bool IsFirstStepInHorizon(int j) const;

  public:
    Signal(Model* parent_model, const std::string& name, double nominal_value = 0., bool is_disturbance = false);

    void SetValue(int i, double value);
    void SetNominalValue(double value);
    double GetValue(int i) const;
    bool IsDisturbance() const { return m_is_disturbance; };

    void Init(double value);

    double GetInitializedValue() const;
    void Correct(int j, double s_fade, double s_takeover);
    void CorrectOptimized(int j, double GetCosineFadeVal2);
};

template <class Model>
Signal<Model>::Signal(Model* parent_model, const std::string& name, double nominal_value, bool is_disturbance)
    : m_id(parent_model->AddSignal(name, nominal_value, is_disturbance))
    , m_horizon_start_index(parent_model->GetHorizonStartIndex())
    , m_horizon_end_index(parent_model->GetHorizonEndIndex())
    , m_history_start_index(parent_model->GetHistoryStartIndex())
    , m_history_end_index(parent_model->GetHistoryEndIndex())
    , m_name(name)
    , m_parent_model(parent_model)
    , m_is_disturbance(is_disturbance)
    , m_first_step_diff(0)
{
}
template <class Model>
void Signal<Model>::SetValue(int i, double value)
{
    m_parent_model->SetSignalValue(m_id, i, value);
}
template <class Model>
void Signal<Model>::SetNominalValue(double value)
{
    m_parent_model->SetNominalSignalValue(m_id, value);
}
template <class Model>
void Signal<Model>::Init(double value)
{
    m_parent_model->InitSignal(m_id, value);
}
template <class Model>
double Signal<Model>::GetValue(int i) const
{
    return m_parent_model->GetSignalValue(m_id, i);
}
template <class Model>
double Signal<Model>::GetInitializedValue() const
{
    return m_parent_model->GetInitializedValue(m_id);
}
template <class Model>
bool Signal<Model>::IsFirstStepInHorizon(int j) const
{
    return j == m_horizon_start_index;
}
template <class Model>
void Signal<Model>::Correct(int j, double s_fade, double s_takeover)
{
    if (IsFirstStepInHorizon(j))
        m_first_step_diff = GetInitializedValue() - GetValue(j);

    SetValue(j, GetValue(j) + utilities::Math::GetCosineFade(m_first_step_diff, 0.0, s_fade, s_takeover));
}
template <class Model>
void Signal<Model>::CorrectOptimized(int j, double cosine_fade_val2)
{
    if (IsFirstStepInHorizon(j))
        m_first_step_diff = GetInitializedValue() - GetValue(j);

    SetValue(j, GetValue(j) + utilities::Math::GetCosineFadeOptimized(m_first_step_diff, 0.0, cosine_fade_val2));
}
}  //namespace mpmca::predict::inertial