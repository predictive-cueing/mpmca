/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <algorithm>

#include "mpmca/constants.hpp"
#include "mpmca/messages/automotive_prediction_horizon.hpp"
#include "mpmca/messages/automotive_prediction_parameters.hpp"
#include "mpmca/messages/inertial_head_signals.hpp"
#include "mpmca/predict/inertial/model.hpp"
#include "mpmca/predict/inertial/signal.hpp"
#include "mpmca/utilities/tunable.hpp"

namespace mpmca::predict::inertial {
class InertialOutput : public Model<9, 5>
{
  public:
    InertialOutput(utilities::ConfigurationPtr config, const utilities::Horizon&, const std::string& name,
                   bool is_for_control = false);
    InertialOutput(const utilities::Horizon& t, const std::string& name, bool is_for_control = false);

    utilities::Tunable<utilities::Scalar> gravity_m_s2;

    Signal<InertialOutput> f_x;  //0
    Signal<InertialOutput> f_y;  //1
    Signal<InertialOutput> f_z;  //2
    Signal<InertialOutput> omega_x;  //3
    Signal<InertialOutput> omega_y;  //4
    Signal<InertialOutput> omega_z;  //5
    Signal<InertialOutput> alpha_x;  //6
    Signal<InertialOutput> alpha_y;  //7
    Signal<InertialOutput> alpha_z;  //8

    template <typename T>
    void ReadMessage(const T& message);

    template <typename MT>
    void UpdateMessage(MT& message);

    template <class Car>
    void Step(const Car& car, int j)
    {
        CalculateTakeoverTime(j);

        f_x.CorrectOptimized(j, GetTakeoverFadeVal2());
        f_y.CorrectOptimized(j, GetTakeoverFadeVal2());

        if (IsForControl())
            f_z.CorrectOptimized(j, GetTakeoverFadeVal2());
        else
            f_z.Correct(j, 0.1, GetTakeover());

        omega_x.CorrectOptimized(j, GetTakeoverFadeVal2());
        omega_y.CorrectOptimized(j, GetTakeoverFadeVal2());
        omega_z.CorrectOptimized(j, GetTakeoverFadeVal2());
        alpha_x.CorrectOptimized(j, GetTakeoverFadeVal2());
        alpha_y.CorrectOptimized(j, GetTakeoverFadeVal2());
        alpha_z.CorrectOptimized(j, GetTakeoverFadeVal2());

        f_x.SetValue(j, std::clamp(f_x.GetValue(j), -8., 8.));
        f_y.SetValue(j, std::clamp(f_y.GetValue(j), -8., 8.));
        f_z.SetValue(j, std::clamp(f_z.GetValue(j), 9.81 - 12., 9.81 + 12.));

        omega_x.SetValue(j, std::clamp(omega_x.GetValue(j), -40 * constants::kPi / 180.0, 40 * constants::kPi / 180.0));
        omega_y.SetValue(j, std::clamp(omega_y.GetValue(j), -40 * constants::kPi / 180.0, 40 * constants::kPi / 180.0));
        omega_z.SetValue(j, std::clamp(omega_z.GetValue(j), -45 * constants::kPi / 180.0, 45 * constants::kPi / 180.0));

        alpha_x.SetValue(j,
                         std::clamp(alpha_x.GetValue(j), -100 * constants::kPi / 180.0, 100 * constants::kPi / 180.0));
        alpha_y.SetValue(j,
                         std::clamp(alpha_y.GetValue(j), -100 * constants::kPi / 180.0, 100 * constants::kPi / 180.0));
        alpha_z.SetValue(j,
                         std::clamp(alpha_z.GetValue(j), -100 * constants::kPi / 180.0, 100 * constants::kPi / 180.0));
    }
};

template <>
inline void InertialOutput::ReadMessage(const messages::AutomotivePredictionParameters& message)
{
    gravity_m_s2.SetNextValue(message.gravity_m_s2);
    f_z.SetNominalValue(message.gravity_m_s2);
}

template <>
inline void InertialOutput::ReadMessage(const messages::InertialHeadSignals& message)
{
    f_x.Init(message.head_f_x_head_m_s2);
    f_y.Init(message.head_f_y_head_m_s2);
    f_z.Init(message.head_f_z_head_m_s2);
    omega_x.Init(message.head_omega_x_head_rad_s);
    omega_y.Init(message.head_omega_y_head_rad_s);
    omega_z.Init(message.head_omega_z_head_rad_s);
    alpha_x.Init(message.head_alpha_x_head_rad_s2);
    alpha_y.Init(message.head_alpha_y_head_rad_s2);
    alpha_z.Init(message.head_alpha_z_head_rad_s2);
}

template <>
inline void InertialOutput::UpdateMessage(messages::AutomotivePredictionHorizon& message)
{
    for (size_t i = 0; i < m_horizon.GetNPrediction(); ++i) {
        message.head_f_x_head_m_s2.at(i) = f_x.GetValue(i + m_horizon_start_index);
        message.head_f_y_head_m_s2.at(i) = f_y.GetValue(i + m_horizon_start_index);
        message.head_f_z_head_m_s2.at(i) = f_z.GetValue(i + m_horizon_start_index);
        message.head_omega_x_head_rad_s.at(i) = omega_x.GetValue(i + m_horizon_start_index);
        message.head_omega_y_head_rad_s.at(i) = omega_y.GetValue(i + m_horizon_start_index);
        message.head_omega_z_head_rad_s.at(i) = omega_z.GetValue(i + m_horizon_start_index);
        message.head_alpha_x_head_rad_s2.at(i) = alpha_x.GetValue(i + m_horizon_start_index);
        message.head_alpha_y_head_rad_s2.at(i) = alpha_y.GetValue(i + m_horizon_start_index);
        message.head_alpha_z_head_rad_s2.at(i) = alpha_z.GetValue(i + m_horizon_start_index);
    }
}

template <typename T>
void InertialOutput::ReadMessage(const T& message)
{
}

template <typename MT>
void InertialOutput::UpdateMessage(MT& message)
{
}
}  //namespace mpmca::predict::inertial