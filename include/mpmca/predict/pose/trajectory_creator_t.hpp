/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/predict/pose/segment_end.hpp"
#include "mpmca/predict/pose/segment_free_move.hpp"
#include "mpmca/predict/pose/segment_goto.hpp"
#include "mpmca/predict/pose/segment_hold.hpp"
#include "mpmca/predict/pose/segment_linear_move.hpp"
#include "mpmca/predict/pose/segment_start.hpp"
#include "mpmca/predict/pose/trajectory.hpp"
#include "mpmca/predict/pose/trajectory_creator.hpp"

namespace mpmca::predict::pose {
template <class T>
void TrajectoryCreator<T>::CheckStartSegment(Trajectory& trajectory)
{
    if (!trajectory.HasStartSegment())
        throw std::runtime_error("This trajectory does not have a start segment yet, add this first.");
}

template <class T>
template <class... Args>
std::unique_ptr<T> TrajectoryCreator<T>::CreateTrajectory(Trajectory& trajectory, Args... args)
{
    CheckStartSegment(trajectory);

    return std::make_unique<T>(trajectory, args...);
}

template <>
template <class... Args>
std::unique_ptr<SegmentStart> TrajectoryCreator<SegmentStart>::CreateTrajectory(Trajectory& trajectory, Args... args)
{
    if (trajectory.HasStartSegment())
        throw std::runtime_error("This trajectory already has a start segment.");

    return std::make_unique<SegmentStart>(trajectory, args...);
}

template <>
template <class... Args>
std::unique_ptr<SegmentEnd> TrajectoryCreator<SegmentEnd>::CreateTrajectory(Trajectory& trajectory, Args... args)
{
    CheckStartSegment(trajectory);

    if (trajectory.HasEndSegment())
        throw std::runtime_error("This trajectory already has an end segment.");

    auto ret = std::make_unique<SegmentEnd>(trajectory, args...);
    trajectory.m_has_end_segment = true;
    return ret;
}
}  //namespace mpmca::predict::pose