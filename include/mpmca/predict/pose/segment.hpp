/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/linear_algebra.hpp"
#include "mpmca/types/pose_rot_mat_vector.hpp"
#include "mpmca/types/pose_vector.hpp"
#include "mpmca/utilities/configuration.hpp"

namespace mpmca::predict::pose {
class Trajectory;

class Segment
{
  protected:
    const std::string m_name;
    const int64_t m_dt_ms;
    int64_t m_t_duration_ms;
    int64_t m_t_end_ms;
    Segment* m_next_segment;
    Segment* m_predicted_next_segment;
    std::string m_type = "empty";

    bool TimeIsDivisible(int64_t t_check) const;
    int64_t CheckTimeIsDivisible(int64_t t_check) const;
    int64_t CheckTimeIsWithinBounds(int64_t t_check) const;

    std::vector<PoseRotMatVector> m_output_error_weight_plan;
    std::vector<PoseVector> m_reference_pose_plan;

    Segment(const Trajectory& trajectory, const std::string& name, int64_t t_duration_ms);

  public:
    const std::string& GetName() const;
    const std::string& GetType() const;
    void SetNextSegment(Segment* next_segment);

    int64_t GetDtMs() const;
    int64_t GetTimeDurationMs() const;

    Segment* GetNextSegment() const;
    Segment* GetPredictedNextSegment() const;
    const PoseVector& GetFinalReferencePose() const;

    virtual bool CanAddBehind() const { return true; }
    virtual int64_t GetTimeEndMs() const;
    virtual const PoseRotMatVector& GetOutputErrorWeightAtLocalTimeMs(int64_t tLocalMs) const;
    virtual const PoseVector& GetReferencePoseAtLocalTimeMs(int64_t tLocalMs) const;
    virtual bool TimeIsWithinBounds(int64_t t_check) const;
    virtual void DecideNextSegment(const Trajectory& trajectory) {};
    virtual void PredictNextSegment(const Trajectory& trajectory) {};
    virtual void Calculate(Segment const* previous_segment) = 0;
};
}  //namespace mpmca::predict::pose