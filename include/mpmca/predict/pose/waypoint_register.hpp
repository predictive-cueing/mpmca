/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <map>

#include "mpmca/linear_algebra.hpp"
#include "mpmca/predict/pose/waypoint.hpp"

namespace mpmca::predict::pose {
class WaypointRegister
{
  private:
    std::map<std::string, std::shared_ptr<Waypoint>> m_register;

  public:
    WaypointRegister();
    bool GetPointExists(const std::string& name) const;
    void MakePoint(const std::string& name, const PoseVector& pose);

    const PoseVector& GetPoseVector(const std::string& name) const;
    const PoseRotMatVector& GetOutputVector(const std::string& name) const;
};
}  //namespace mpmca::predict::pose