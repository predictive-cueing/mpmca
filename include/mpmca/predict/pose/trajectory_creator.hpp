/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <memory>

namespace mpmca::predict::pose {
class Trajectory;

template <class T>
class TrajectoryCreator
{
  public:
    TrajectoryCreator();

    template <class... Args>
    static std::unique_ptr<T> CreateTrajectory(Trajectory& trajectory, Args... args);

    static void CheckStartSegment(Trajectory& trajectory);
};
}  //namespace mpmca::predict::pose