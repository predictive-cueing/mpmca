/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/predict/pose/segment.hpp"
#include "mpmca/predict/pose/trajectory_creator.hpp"
namespace mpmca::predict::pose {
class SegmentStart : public Segment
{
  private:
    int64_t m_t_move_ms;
    PoseVector m_reference_pose;
    PoseRotMatVector m_output_error_weight;

  public:
    SegmentStart(const Trajectory& trajectory, utilities::ConfigurationPtr config);

    SegmentStart(const Trajectory& trajectory, const std::string& name, int64_t t_move_ms,
                 const PoseVector& reference_pose_start, const PoseRotMatVector& output_error_weight_start);

    void Calculate(Segment const* previous_segment) override;
    const PoseVector& GetReferencePose() const;
};
}  //namespace mpmca::predict::pose