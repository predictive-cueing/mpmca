/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/predict/pose/segment.hpp"
#include "mpmca/predict/pose/trajectory_creator.hpp"
namespace mpmca::predict::pose {
class SegmentLinearMove : public Segment
{
  private:
    int64_t m_t_move_ms;
    bool m_fixed_start_reference;
    PoseVector m_start_reference_pose;
    PoseVector m_end_reference_pose;
    PoseRotMatVector m_output_error_weight;

    static SegmentLinearMove InitTrajectorySegmentLinearMove(const Trajectory& trajectory,
                                                             utilities::ConfigurationPtr config);

  public:
    SegmentLinearMove(const Trajectory& trajectory, utilities::ConfigurationPtr config);

    SegmentLinearMove(const Trajectory& trajectory, const std::string& name, int64_t t_move_ms,
                      const PoseVector& end_reference_pose, const PoseRotMatVector& output_error_weight);

    SegmentLinearMove(const Trajectory& trajectory, const std::string& name, int64_t t_move_ms,
                      const PoseVector& start_reference_pose, const PoseVector& end_reference_pose,
                      const PoseRotMatVector& output_error_weight);

    SegmentLinearMove(const Trajectory& trajectory, const std::string& name, int64_t t_move_ms,
                      const PoseVector& start_reference_pose, const PoseVector& end_reference_pose,
                      const PoseRotMatVector& output_error_weight, bool fixed_start_reference);

    void Calculate(Segment const* previous_segment) override;

    const PoseVector& GetStartReferencePose() const;
    const PoseVector& GetEndReferencePose() const;
    const PoseRotMatVector& GetOutputErrorWeight() const;
};
}  //namespace mpmca::predict::pose