/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/linear_algebra.hpp"
#include "mpmca/types/pose_rot_mat_vector.hpp"
#include "mpmca/types/pose_vector.hpp"
#include "mpmca/utilities/configuration.hpp"

namespace mpmca::predict::pose {
class Waypoint
{
  private:
    const std::string m_name = "no-name";
    const PoseRotMatVector m_output_vector;
    const PoseVector m_pose_vector;

  public:
    Waypoint(std::string name, const PoseVector& pose);

    const PoseRotMatVector& GetOutputVector() const;
    const PoseVector& GetPoseVector() const;
    const std::string& GetName() const;
};
}  //namespace mpmca::predict::pose