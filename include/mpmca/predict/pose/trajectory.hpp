/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <deque>

#include "mpmca/predict/pose/segment.hpp"
#include "mpmca/predict/pose/waypoint_register.hpp"
#include "mpmca/utilities/configuration.hpp"
#include "mpmca/utilities/horizon.hpp"

namespace mpmca::predict::pose {
enum class SegmentType
{
    kStart,
    kFreeMove,
    kHold,
    kLinearMove,
    kLinearMovePose,
    kGotoSegment,
    kEnd,
    kInvalid
};

class Trajectory
{
  private:
    const std::map<std::string, SegmentType> segment_type_strings{{"Start", SegmentType::kStart},
                                                                  {"FreeMove", SegmentType::kFreeMove},
                                                                  {"Hold", SegmentType::kHold},
                                                                  {"LinearMove", SegmentType::kLinearMove},
                                                                  {"LinearMovePose", SegmentType::kLinearMovePose},
                                                                  {"End", SegmentType::kEnd},
                                                                  {"GoTo", SegmentType::kGotoSegment},
                                                                  {"Invalid", SegmentType::kInvalid}};
    template <class T>
    friend class TrajectoryCreator;

    const utilities::Horizon m_horizon;
    WaypointRegister m_waypoint_register;
    std::deque<std::unique_ptr<Segment>> m_segments;

    bool m_has_end_segment;

    std::vector<PoseVector> m_reference_pose_plan;
    std::vector<PoseRotMatVector> m_output_error_weight_plan;

    int64_t m_current_segment_local_time;
    Segment* m_current_segment;
    Segment* m_previous_segment;
    Segment* GetCurrentSegment();
    Segment* GetPreviousSegment();

    void SetNextSegment(Segment* next_segment);

  public:
    Trajectory(utilities::ConfigurationPtr config, const utilities::Horizon& horizon);
    Trajectory(const utilities::Horizon& horizon);

    int64_t GetFundamentalDtMs() const;
    size_t GetNumSegments() const;
    Segment* GetSegmentByIndex(size_t i) const;
    Segment* GetNextSegmentInDeque(Segment* current_segment);

    bool HasStartSegment() const;
    bool HasEndSegment() const;

    void AddWaypoint(const std::string& name, const PoseVector& pose);

    template <class T, class... Args>
    Segment* AddSegment(Args... args);

    PoseVector GetPoseFromRegisterOrConfig(utilities::ConfigurationPtr config, const std::string& name) const;
    PoseRotMatVector GetPoseFromRegisterOrConfigToOutputVector(utilities::ConfigurationPtr config,
                                                               const std::string& name) const;

    void Tick();
    void Calculate();

    const PoseRotMatVector& GetOutputErrorWeightAtIndex(size_t i) const;
    const PoseVector& GetReferencePoseAtIndex(size_t i) const;
    const PoseRotMatVector& GetOutputErrorWeightAtHorizonTimeMs(int64_t i) const;
    const PoseVector& GetReferencePoseAtHorizonTimeMs(int64_t i) const;
};
}  //namespace mpmca::predict::pose