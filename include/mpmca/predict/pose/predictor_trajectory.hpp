/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/predict/pose/pose_predictor.hpp"
#include "mpmca/predict/pose/trajectory.hpp"

namespace mpmca::predict::pose {
class PredictorTrajectory : public PosePredictor
{
  private:
    Trajectory m_trajectory;
    int m_cycle;

  public:
    PredictorTrajectory(utilities::ConfigurationPtr config, const utilities::Horizon& horizon, const std::string& name);
    void Predict() override;

    ~PredictorTrajectory();
};
}  //namespace mpmca::predict::pose