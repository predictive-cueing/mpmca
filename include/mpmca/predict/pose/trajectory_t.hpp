/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/predict/pose/trajectory.hpp"
#include "mpmca/predict/pose/trajectory_creator_t.hpp"

namespace mpmca::predict::pose {
template <class T, class... Args>
Segment* Trajectory::AddSegment(Args... args)
{
    if (m_segments.size() > 0 && !m_segments.back()->CanAddBehind())
        throw std::runtime_error("You cannot add a segment after a " + m_segments.back()->GetType());

    m_segments.push_back(TrajectoryCreator<T>::CreateTrajectory(*this, args...));
    return m_segments.back().get();
}
}  //namespace mpmca::predict::pose