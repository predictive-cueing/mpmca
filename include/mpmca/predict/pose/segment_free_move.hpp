/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/predict/pose/segment.hpp"
#include "mpmca/predict/pose/trajectory_creator.hpp"
namespace mpmca::predict::pose {
class SegmentFreeMove : public Segment
{
  private:
    int64_t m_t_move_ms;

  public:
    SegmentFreeMove(const Trajectory& trajectory, utilities::ConfigurationPtr config);
    SegmentFreeMove(const Trajectory& trajectory, const std::string& name, int64_t t_move_ms);

    void Calculate(Segment const* previous_segment) override;
};
}  //namespace mpmca::predict::pose