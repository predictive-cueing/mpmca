/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/json_tools.hpp"
#include "mpmca/pipeline/message_bus.hpp"
#include "mpmca/types/pose_rot_mat_vector.hpp"
#include "mpmca/types/pose_vector.hpp"
#include "mpmca/utilities/configuration.hpp"
#include "mpmca/utilities/horizon.hpp"
#include "mpmca/utilities/logger.hpp"

namespace mpmca::predict::pose {

class PosePredictor
{
  protected:
    std::string m_name;
    std::vector<PoseRotMatVector> m_reference_output_plan;
    std::vector<PoseRotMatVector> m_output_error_weight_plan;

    std::vector<std::string> m_all_signal_names;
    std::vector<std::string> m_output_signal_names;

    PoseRotMatVector m_y_simulator;

    int64_t m_t_current_ms;
    utilities::Horizon m_horizon;

    static std::vector<PoseRotMatVector> GetReferencePointsFromConfig(utilities::ConfigurationPtr config);
    utilities::Logger m_logger;

  public:
    PosePredictor(utilities::ConfigurationPtr config, const utilities::Horizon& horizon, const std::string& name);

    virtual void PrepareTaskMessageBus(pipeline::MessageBus& message_bus);
    virtual void CheckTaskMessageBusPrepared(const pipeline::MessageBus& message_bus);
    virtual void UpdateMessageBus(pipeline::MessageBus& message_bus);
    virtual void ReadMessageBus(pipeline::MessageBus& message_bus);

    const std::vector<std::string>& GetPredictorSignalNames() const;
    const std::vector<std::string>& GetOutputSignalNames() const;
    const std::vector<PoseRotMatVector>& GetPredictionOutputVectorPlan() const;
    const std::vector<PoseRotMatVector>& GetOutputErrorWeightPlan() const;

    void SetCurrentTimeMs(int64_t time);
    void SetActualSimulatorOutput(PoseRotMatVector const& y_simulator);

    void Prepare();
    void PrepareTick();
    bool GetTerminate() const;

    virtual void Predict() = 0;
    virtual ~PosePredictor();
};
}  //namespace mpmca::predict::pose