/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/linear_algebra.hpp"
#include "mpmca/predict/pose/segment.hpp"
#include "mpmca/predict/pose/trajectory_creator.hpp"
namespace mpmca::predict::pose {
class SegmentEnd : public Segment
{
  private:
    int64_t m_t_fade_ms;
    PoseVector m_reference_pose_end;
    PoseRotMatVector m_output_error_weight_end;

  public:
    SegmentEnd(const Trajectory& trajectory, utilities::ConfigurationPtr config);

    SegmentEnd(const Trajectory& trajectory, const std::string& name, int64_t t_fade_ms, PoseVector reference_pose_end,
               PoseRotMatVector output_error_weight_end);
    int64_t GetTimeEndMs() const override;
    void Calculate(Segment const* previous_segment) override;
    const PoseRotMatVector& GetOutputErrorWeightAtLocalTimeMs(int64_t t_local_ms) const override;
    const PoseVector& GetReferencePoseAtLocalTimeMs(int64_t t_local_ms) const override;
    bool TimeIsWithinBounds(int64_t t_check) const override;
    bool CanAddBehind() const override { return false; }
};
}  //namespace mpmca::predict::pose