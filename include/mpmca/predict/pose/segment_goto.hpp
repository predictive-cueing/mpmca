/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/predict/pose/segment.hpp"
#include "mpmca/predict/pose/trajectory_creator.hpp"
namespace mpmca::predict::pose {
class SegmentGoto : public Segment
{
  private:
    int64_t m_t_free_move_ms;
    std::string m_goto_segment_name;

  public:
    SegmentGoto(const Trajectory& trajectory, utilities::ConfigurationPtr config);
    SegmentGoto(const Trajectory& trajectory, const std::string& name, const std::string& goto_segment_name,
                int64_t t_free_move_ms);
    void Calculate(Segment const* previous_segment) override;
    void DecideNextSegment(const Trajectory& trajectory) override;
    void PredictNextSegment(const Trajectory& trajectory) override;
};
}  //namespace mpmca::predict::pose