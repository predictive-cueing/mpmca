/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/utilities/mpmca_mcap.hpp"
#include "mpmca/visualization/foxglove_message_convertor.hpp"

namespace mpmca::visualization {

class SimulationSignalsConvertor : public FoxgloveMessageConvertor
{
  public:
    SimulationSignalsConvertor(mcap::McapWriter& writer);
    void ProcessDataBucket(mcap::McapWriter& writer, const pipeline::DataBucket& data_bucket) override;

  private:
    mcap::ChannelId m_channel_id;
};

}  //namespace mpmca::visualization