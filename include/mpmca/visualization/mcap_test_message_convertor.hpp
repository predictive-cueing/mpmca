/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <iostream>

#include "mpmca/json.hpp"
#include "mpmca/messages/mcap_test_message.hpp"
#include "mpmca/visualization/foxglove_message_convertor.hpp"

namespace mpmca::visualization {

class McapTestMessageConvertor : public FoxgloveMessageConvertor
{
  public:
    McapTestMessageConvertor(mcap::McapWriter& writer) { std::cout << "HAllo" << std::endl; }
    void ProcessDataBucket(mcap::McapWriter& writer, const pipeline::DataBucket& data_bucket) override {}
};

}  //namespace mpmca::visualization