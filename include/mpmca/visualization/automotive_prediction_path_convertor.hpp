/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "foxglove/FrameTransform.pb.h"
#include "foxglove/Grid.pb.h"
#include "foxglove/SceneUpdate.pb.h"
#include "mpmca/messages/automotive_prediction_path.hpp"
#include "mpmca/predict/inertial/path.hpp"
#include "mpmca/utilities/mpmca_mcap.hpp"
#include "mpmca/visualization/foxglove_message_convertor.hpp"

namespace mpmca::visualization {

class AutomotivePredictionPathConvertor : public FoxgloveMessageConvertor
{
  public:
    AutomotivePredictionPathConvertor(mcap::McapWriter& writer);
    void ProcessDataBucket(mcap::McapWriter& writer, const pipeline::DataBucket& data_bucket) override;

  private:
    std::string SerializePath(predict::inertial::PathPtr path_ptr);
    std::string SerializeFrameTransform(mcap::McapWriter& writer);

    mcap::ChannelId m_path_channel_id;
    mcap::ChannelId m_frame_transform_channel_id;
};

}  //namespace mpmca::visualization