/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <string>

#include "mpmca/pipeline/bus_base_message.hpp"
#include "mpmca/pipeline/bus_message.hpp"
#include "mpmca/pipeline/data_bucket.hpp"
#include "mpmca/utilities/mpmca_mcap.hpp"
#include "mpmca/visualization/build_file_descriptor_set.hpp"

namespace mpmca::visualization {

class FoxgloveMessageConvertor
{
  public:
    virtual void ProcessDataBucket(mcap::McapWriter& writer, const pipeline::DataBucket& data_bucket) = 0;

    static void WriteSerializedString(mcap::McapWriter& writer, const std::string& string,
                                      const pipeline::DataBucket& data_bucket, mcap::ChannelId channel_id);
    static void WriteSerializedString(mcap::McapWriter& writer, const std::string& string, uint32_t sequence,
                                      mcap::Timestamp log_time, mcap::ChannelId channel_id);

    template <class T>
    static mcap::ChannelId AddSchema(mcap::McapWriter& writer, const std::string& channel_name)
    {
        mcap::Schema schema(T::descriptor()->full_name(), "protobuf",
                            foxglove::BuildFileDescriptorSet(T::descriptor()).SerializeAsString());
        writer.addSchema(schema);
        mcap::Channel channel(channel_name, "protobuf", schema.id);
        writer.addChannel(channel);
        return channel.id;
    }
};

}  //namespace mpmca::visualization