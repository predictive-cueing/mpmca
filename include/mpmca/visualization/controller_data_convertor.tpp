/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "foxglove/SceneUpdate.pb.h"
#include "generic_signal_horizon_plan.pb.h"
#include "mpmca/control/simulator.hpp"
#include "mpmca/messages/controller_bounds.hpp"
#include "mpmca/messages/controller_constraint_plan.hpp"
#include "mpmca/messages/controller_final_transform.hpp"
#include "mpmca/messages/controller_inertial_plan.hpp"
#include "mpmca/messages/controller_input_plan.hpp"
#include "mpmca/messages/controller_output_expected_plan.hpp"
#include "mpmca/messages/controller_output_reference_plan.hpp"
#include "mpmca/messages/controller_pose_plan.hpp"
#include "mpmca/messages/controller_state_error_weight_plan.hpp"
#include "mpmca/messages/controller_state_plan.hpp"
#include "mpmca/messages/controller_state_reference_plan.hpp"
#include "mpmca/utilities/math.hpp"
#include "mpmca/visualization/controller_data_convertor.hpp"
#include "mpmca/visualization/info_view_convertor.hpp"
#include "mpmca/visualization/proto_tools.hpp"
#include "signal_horizon_time_plan.pb.h"

namespace mpmca::visualization {

namespace {
template <class T>
std::string SerializeVectorVector(const std::vector<T>& payload)
{
    proto::GenericSignalHorizonPlan* pb_message = new proto::GenericSignalHorizonPlan();

    pb_message->mutable_signals()->Reserve(T::RowsAtCompileTime);

    for (int i = 0; i < T::RowsAtCompileTime; ++i) {
        auto* horizon_signal = new proto::GenericSignalHorizonPlan::HorizonSignal();
        ProtoTools::FillField(horizon_signal->mutable_horizon(), payload, i);
        pb_message->mutable_signals()->AddAllocated(horizon_signal);
    }

    return pb_message->SerializeAsString();
}

template <class T>
void ConvertRotationMatrices(const std::vector<T>& in_vector_vector, std::vector<PoseVector>& out_vector_vector)
{
    out_vector_vector.resize(in_vector_vector.size());

    for (size_t i = 0; i < in_vector_vector.size(); ++i) {
        out_vector_vector[i] = static_cast<PoseRotMatVector>(in_vector_vector[i]).ToPoseVector();
    }
}

template <class TSIM>
std::vector<foxglove::SceneEntity*> MakeModels(typename TSIM::InfoVector info_vector,
                                               typename TSIM::StateVector state_lb, typename TSIM::StateVector state_ub,
                                               typename TSIM::ConstraintVector constraint_lb,
                                               typename TSIM::ConstraintVector constraint_ub)
{
    auto info_view = control::MakeInfoView<TSIM>(info_vector);
    auto state_lb_view = control::MakeStateView<TSIM>(state_lb);
    auto state_ub_view = control::MakeStateView<TSIM>(state_ub);
    auto constraint_lb_view = control::MakeConstraintView<TSIM>(constraint_lb);
    auto constraint_ub_view = control::MakeConstraintView<TSIM>(constraint_ub);

    return InfoViewConvertor<TSIM>::MakeAllModels(info_view, state_lb_view, state_ub_view, constraint_lb_view,
                                                  constraint_ub_view);
}
}  //namespace

template <class TSIM>
ControllerDataConvertor<TSIM>::ControllerDataConvertor(utilities::ConfigurationPtr config, mcap::McapWriter& writer)
    : ControllerDataConvertor<TSIM>(writer, config->GetValue<bool>("ConvertRotationMatricesToEulerAngles", false))
{
}

template <class TSIM>
ControllerDataConvertor<TSIM>::ControllerDataConvertor(mcap::McapWriter& writer,
                                                       bool convert_rotation_matrices_to_euler_angles)
    : m_logger("ControllerDataConvertor", "ControllerDataConvertor")
    , m_output_inertial_reference_plan_channel_id(
          AddSchema<proto::GenericSignalHorizonPlan>(writer, "/control/output_inertial_reference_plan"))
    , m_output_pose_reference_plan_channel_id(
          AddSchema<proto::GenericSignalHorizonPlan>(writer, "/control/output_pose_reference_plan"))
    , m_output_inertial_expected_plan_channel_id(
          AddSchema<proto::GenericSignalHorizonPlan>(writer, "/control/output_inertial_expected_plan"))
    , m_output_pose_expected_plan_channel_id(
          AddSchema<proto::GenericSignalHorizonPlan>(writer, "/control/output_pose_expected_plan"))
    , m_state_reference_plan_channel_id(
          AddSchema<proto::GenericSignalHorizonPlan>(writer, "/control/state_reference_plan"))
    , m_state_expected_plan_channel_id(AddSchema<proto::GenericSignalHorizonPlan>(writer, "/control/state_plan"))
    , m_input_expected_plan_channel_id(AddSchema<proto::GenericSignalHorizonPlan>(writer, "/control/input_plan"))
    , m_controller_horizon_channel_id(AddSchema<proto::SignalHorizonTimePlan>(writer, "/horizon/controller"))
    , m_predictor_horizon_channel_id(AddSchema<proto::SignalHorizonTimePlan>(writer, "/horizon/predictor"))
    , m_models_updated(false)
    , m_convert_rotation_matrices_to_euler_angles(convert_rotation_matrices_to_euler_angles)
{
    auto simulator = control::Simulator<TSIM>();
    m_simulator_info_vector = simulator.GetInfo();

    m_controller_frame_transform_channel_ids.push_back(
        AddSchema<foxglove::FrameTransform>(writer, "/control/vis/world_frame_transform"));

    for (size_t i = 0; i < TSIM::ComponentList::size; ++i) {
        m_controller_frame_transform_channel_ids.push_back(
            AddSchema<foxglove::FrameTransform>(writer, "/control/vis/frame_transforms_" + std::to_string(i)));
    }

    m_controller_frame_transform_channel_ids.push_back(AddSchema<foxglove::FrameTransform>(
        writer, "/control/vis/frame_transforms_" + std::to_string(TSIM::ComponentList::size)));
    m_controller_frame_transform_channel_ids.push_back(
        AddSchema<foxglove::FrameTransform>(writer, "/control/vis/head_frame_transform"));

    m_state_lb = simulator.GetStateLowerBound();
    m_state_ub = simulator.GetStateUpperBound();
    m_constraint_lb = simulator.GetConstraintLowerBound();
    m_constraint_ub = simulator.GetConstraintUpperBound();
    m_final_transform = simulator.GetFinalTransform();

    UpdateModels();

    for (size_t i = 0; i < m_simulator_scene_entity_vector.size(); ++i) {
        m_controller_model_channel_ids.push_back(
            AddSchema<foxglove::SceneUpdate>(writer, "/control/vis/models_" + std::to_string(i)));
    }
}

template <class TSIM>
void ControllerDataConvertor<TSIM>::UpdateModels()
{
    m_simulator_scene_entity_vector =
        MakeModels<TSIM>(m_simulator_info_vector, m_state_lb, m_state_ub, m_constraint_lb, m_constraint_ub);
    m_models_updated = true;
}

template <class TSIM>
void ControllerDataConvertor<TSIM>::WriteModels(mcap::McapWriter& writer, const pipeline::DataBucket& data_bucket) const
{
    for (size_t i = 0; i < m_simulator_scene_entity_vector.size(); ++i) {
        foxglove::SceneUpdate* scene_update = new foxglove::SceneUpdate();
        scene_update->mutable_entities()->AddAllocated(m_simulator_scene_entity_vector[i]);
        WriteSerializedString(writer, scene_update->SerializeAsString(), data_bucket,
                              m_controller_model_channel_ids.at(i));
    }
}

template <class TSIM>
void ControllerDataConvertor<TSIM>::ProcessDataBucket(mcap::McapWriter& writer, const pipeline::DataBucket& data_bucket)
{
    proto::SignalHorizonTimePlan* controller_horizon = new proto::SignalHorizonTimePlan();
    ProtoTools::FillField(controller_horizon->mutable_horizon_time_grid(), data_bucket.GetHorizon().GetHorizonTime());
    WriteSerializedString(writer, controller_horizon->SerializeAsString(), data_bucket,
                          m_controller_horizon_channel_id);

    proto::SignalHorizonTimePlan* predictor_horizon = new proto::SignalHorizonTimePlan();
    ProtoTools::FillField(predictor_horizon->mutable_horizon_time_grid(),
                          data_bucket.GetHorizon().GetPredictionHorizonTime());
    WriteSerializedString(writer, predictor_horizon->SerializeAsString(), data_bucket, m_predictor_horizon_channel_id);

    data_bucket.GetTaskSignalBus().ReadMessageIfUpdated<messages::ControllerOutputReferencePlan<9>>(
        [&](const messages::ControllerOutputReferencePlan<9>& message) {
            WriteSerializedString(writer, SerializeVectorVector(message.y_ref_plan), data_bucket,
                                  m_output_inertial_reference_plan_channel_id);
        });

    data_bucket.GetTaskSignalBus().ReadMessageIfUpdated<messages::ControllerStateReferencePlan<TSIM>>(
        [&](const messages::ControllerStateReferencePlan<TSIM>& message) {
            WriteSerializedString(writer, SerializeVectorVector(message.x_ref_plan), data_bucket,
                                  m_state_reference_plan_channel_id);
        });

    if constexpr (TSIM::NY_SIMULATOR == 9) {
        data_bucket.GetTaskSignalBus().ReadMessageIfUpdated<messages::ControllerOutputExpectedPlan<9>>(
            [&](const messages::ControllerOutputExpectedPlan<9>& message) {
                WriteSerializedString(writer, SerializeVectorVector(message.y_expected_plan), data_bucket,
                                      m_output_inertial_expected_plan_channel_id);
            });

        data_bucket.GetTaskSignalBus().ReadMessageIfUpdated<messages::ControllerPosePlan<TSIM>>(
            [&](const messages::ControllerPosePlan<TSIM>& message) {
                if (m_convert_rotation_matrices_to_euler_angles) {
                    ConvertRotationMatrices(message.pose_plan, m_converted_pose_expected_plan_cache);
                    WriteSerializedString(writer, SerializeVectorVector(m_converted_pose_expected_plan_cache),
                                          data_bucket, m_output_pose_expected_plan_channel_id);
                }
                else {
                    WriteSerializedString(writer, SerializeVectorVector(message.pose_plan), data_bucket,
                                          m_output_pose_expected_plan_channel_id);
                }
            });
    }
    else if constexpr (TSIM::NY_SIMULATOR == 24) {
        data_bucket.GetTaskSignalBus().ReadMessageIfUpdated<messages::ControllerOutputReferencePlan<24>>(
            [&](const messages::ControllerOutputReferencePlan<24>& message) {
                if (m_convert_rotation_matrices_to_euler_angles) {
                    ConvertRotationMatrices(message.y_ref_plan, m_converted_pose_output_reference_plan_cache);
                    WriteSerializedString(writer, SerializeVectorVector(m_converted_pose_output_reference_plan_cache),
                                          data_bucket, m_output_pose_reference_plan_channel_id);
                }
                else {
                    WriteSerializedString(writer, SerializeVectorVector(message.y_ref_plan), data_bucket,
                                          m_output_pose_reference_plan_channel_id);
                }
            });

        data_bucket.GetTaskSignalBus().ReadMessageIfUpdated<messages::ControllerOutputExpectedPlan<24>>(
            [&](const messages::ControllerOutputExpectedPlan<24>& message) {
                if (m_convert_rotation_matrices_to_euler_angles) {
                    ConvertRotationMatrices(message.y_expected_plan, m_converted_pose_output_expected_plan_cache);
                    WriteSerializedString(writer, SerializeVectorVector(m_converted_pose_output_expected_plan_cache),
                                          data_bucket, m_output_pose_expected_plan_channel_id);
                }
                else {
                    WriteSerializedString(writer, SerializeVectorVector(message.y_expected_plan), data_bucket,
                                          m_output_pose_expected_plan_channel_id);
                }
            });

        data_bucket.GetTaskSignalBus().ReadMessageIfUpdated<messages::ControllerInertialPlan<TSIM>>(
            [&](const messages::ControllerInertialPlan<TSIM>& message) {
                WriteSerializedString(writer, SerializeVectorVector(message.i_plan), data_bucket,
                                      m_output_inertial_expected_plan_channel_id);
            });
    }

    data_bucket.GetTaskSignalBus().ReadMessageIfUpdated<messages::ControllerBounds<TSIM>>(
        [&](const messages::ControllerBounds<TSIM>& message) {
            m_state_lb = message.x_lower_bound;
            m_state_ub = message.x_upper_bound;
            m_constraint_lb = message.c_lower_bound;
            m_constraint_ub = message.c_upper_bound;
            UpdateModels();
        });

    data_bucket.GetTaskSignalBus().ReadMessageIfUpdated<mpmca::messages::ControllerFinalTransform>(
        [&](const mpmca::messages::ControllerFinalTransform& message) {
            m_final_transform = message.final_transform;
            UpdateModels();
        });

    typename TSIM::StateVector first_state_vector;
    typename TSIM::ConstraintVector first_constraint_vector;
    PoseVector first_head_pose_vector;

    data_bucket.GetTaskSignalBus().ReadMessageIfUpdated<messages::ControllerStatePlan<TSIM>>(
        [&](const messages::ControllerStatePlan<TSIM>& message) {
            first_state_vector = message.x_plan.at(0);
            WriteSerializedString(writer, SerializeVectorVector(message.x_plan), data_bucket,
                                  m_state_expected_plan_channel_id);
        });

    data_bucket.GetTaskSignalBus().ReadMessageIfUpdated<messages::ControllerInputPlan<TSIM>>(
        [&](const messages::ControllerInputPlan<TSIM>& message) {
            WriteSerializedString(writer, SerializeVectorVector(message.u_plan), data_bucket,
                                  m_input_expected_plan_channel_id);
        });

    data_bucket.GetTaskSignalBus().ReadMessageIfUpdated<messages::ControllerConstraintPlan<TSIM>>(
        [&](const messages::ControllerConstraintPlan<TSIM>& message) {
            first_constraint_vector = message.c_plan.at(0);
        });

    data_bucket.GetTaskSignalBus().ReadMessageIfUpdated<messages::ControllerPosePlan<TSIM>>(
        [&](const messages::ControllerPosePlan<TSIM>& message) {
            first_head_pose_vector = message.pose_plan.at(0).ToPoseVector();
        });

    auto state_view = mpmca::control::MakeStateView<TSIM>(first_state_vector);
    auto info_view = mpmca::control::MakeInfoView<TSIM>(m_simulator_info_vector);

    auto frame_transforms = InfoViewConvertor<TSIM>::MakeAllFrameTransforms(state_view, info_view, m_final_transform,
                                                                            first_head_pose_vector);

    for (size_t i = 0; i < frame_transforms.size(); ++i) {
        WriteSerializedString(writer, frame_transforms.at(i)->SerializeAsString(), data_bucket,
                              m_controller_frame_transform_channel_ids.at(i));
    }

    if (m_models_updated) {
        WriteModels(writer, data_bucket);
        m_models_updated = false;
    }
}
}  //namespace mpmca::visualization
