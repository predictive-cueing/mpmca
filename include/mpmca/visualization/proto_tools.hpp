/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <google/protobuf/repeated_field.h>

#include "mpmca/linear_algebra.hpp"

namespace mpmca::visualization {

class ProtoTools
{
  public:
    static inline void FillField(google::protobuf::RepeatedField<float>* proto_field, const std::vector<double>& vec)
    {
        proto_field->Resize(vec.size(), 0.);
        float* dst = proto_field->begin();

        for (int k = 0; k < (int)vec.size(); ++k)
            dst[k] = vec[k];
    }
    static inline void FillField(google::protobuf::RepeatedField<double>* proto_field, const std::vector<double>& vec)
    {
        proto_field->Resize(vec.size(), 0.);
        double* dst = proto_field->begin();

        for (int k = 0; k < (int)vec.size(); ++k)
            dst[k] = vec[k];
    }

    static inline void FillField(google::protobuf::RepeatedField<uint32_t>* proto_field,
                                 const std::vector<uint32_t>& vec)
    {
        proto_field->Resize(vec.size(), 0);
        uint32_t* dst = proto_field->begin();

        for (size_t k = 0; k < vec.size(); ++k)
            dst[k] = vec[k];
    }

    template <int i>
    static void FillField(google::protobuf::RepeatedField<float>* proto_field, const Eigen::Matrix<double, i, 1>& vec)
    {
        proto_field->Resize(i, 0.);
        float* dst = proto_field->begin();

        for (int k = 0; k < i; ++k)
            dst[k] = vec[k];
    }

    template <class T>
    static void FillField(google::protobuf::RepeatedField<float>* proto_field, const std::vector<T>& vec, int i)
    {
        proto_field->Resize(vec.size(), 0.);
        float* dst = proto_field->begin();

        for (int k = 0; k < (int)vec.size(); ++k)
            dst[k] = vec[k][i];
    }

    template <class T>
    static void FillField(google::protobuf::RepeatedField<double>* proto_field, const std::vector<T>& vec, int i)
    {
        proto_field->Resize(vec.size(), 0.);
        double* dst = proto_field->begin();

        for (int k = 0; k < (int)vec.size(); ++k)
            dst[k] = vec[k][i];
    }
};
}  //namespace mpmca::visualization