/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "foxglove/SceneEntity.pb.h"
#include "mpmca/messages/controller_output_reference_plan.hpp"
#include "mpmca/types/pose_rot_mat_vector.hpp"
#include "mpmca/types/pose_vector.hpp"
#include "mpmca/types/transformation_matrix.hpp"
#include "mpmca/utilities/configuration.hpp"
#include "mpmca/utilities/logger.hpp"
#include "mpmca/utilities/mpmca_mcap.hpp"
#include "mpmca/visualization/foxglove_message_convertor.hpp"

namespace mpmca::visualization {

template <class TSIM>
class ControllerDataConvertor : public FoxgloveMessageConvertor
{
  private:
    utilities::Logger m_logger;

    mcap::ChannelId m_output_inertial_reference_plan_channel_id;
    mcap::ChannelId m_output_pose_reference_plan_channel_id;
    mcap::ChannelId m_output_inertial_expected_plan_channel_id;
    mcap::ChannelId m_output_pose_expected_plan_channel_id;

    mcap::ChannelId m_state_reference_plan_channel_id;
    mcap::ChannelId m_state_expected_plan_channel_id;
    mcap::ChannelId m_input_expected_plan_channel_id;
    mcap::ChannelId m_controller_horizon_channel_id;
    mcap::ChannelId m_predictor_horizon_channel_id;

    mcap::ChannelId m_inertial_head_signals_channel_id;

    std::vector<mcap::ChannelId> m_controller_model_channel_ids;
    std::vector<mcap::ChannelId> m_controller_frame_transform_channel_ids;

    std::vector<mpmca::PoseVector> m_converted_pose_expected_plan_cache;
    std::vector<mpmca::PoseVector> m_converted_pose_output_reference_plan_cache;
    std::vector<mpmca::PoseVector> m_converted_pose_output_expected_plan_cache;

    typename TSIM::InfoVector m_simulator_info_vector;
    typename TSIM::StateVector m_state_lb;
    typename TSIM::StateVector m_state_ub;
    typename TSIM::ConstraintVector m_constraint_lb;
    typename TSIM::ConstraintVector m_constraint_ub;
    TransformationMatrix m_final_transform;

    bool m_models_updated;
    const bool m_convert_rotation_matrices_to_euler_angles;

    std::vector<foxglove::SceneEntity*> m_simulator_scene_entity_vector;

    void WriteModels(mcap::McapWriter& writer, const pipeline::DataBucket& data_bucket) const;
    void UpdateModels();

  public:
    ControllerDataConvertor(mcap::McapWriter& writer, bool convert_rotation_matrices_to_euler_angles = false);
    ControllerDataConvertor(utilities::ConfigurationPtr config, mcap::McapWriter& writer);
    void ProcessDataBucket(mcap::McapWriter& writer, const pipeline::DataBucket& data_bucket) override;
};

}  //namespace mpmca::visualization
