/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */

#pragma once
#include "mpmca/messages/automotive_prediction_horizon.hpp"
#include "mpmca/utilities/logger.hpp"
#include "mpmca/utilities/mpmca_mcap.hpp"
#include "mpmca/visualization/foxglove_message_convertor.hpp"

namespace mpmca::visualization {

class AutomotivePredictionHorizonConvertor : public FoxgloveMessageConvertor
{
  private:
    utilities::Logger m_logger;

    mcap::ChannelId m_path_channel_id;
    mcap::ChannelId m_car_coordinate_transform_id;
    mcap::ChannelId m_signals_channel_id;

    static int64_t GetSeconds(int64_t pipeline_time_ms);
    static int64_t GetNanos(int64_t pipeline_time_ms);

  public:
    AutomotivePredictionHorizonConvertor(mcap::McapWriter& writer);

    void ProcessDataBucket(mcap::McapWriter& writer, const pipeline::DataBucket& data_bucket) override;

    void WriteSignals(mcap::McapWriter& writer, const messages::AutomotivePredictionHorizon& message,
                      const pipeline::DataBucket& data_bucket);
    void WriteCarCoordinateTransform(mcap::McapWriter& writer, const messages::AutomotivePredictionHorizon& message,
                                     const pipeline::DataBucket& data_bucket);
    void WritePath(mcap::McapWriter& writer, const messages::AutomotivePredictionHorizon& message,
                   const pipeline::DataBucket& data_bucket);
};

}  //namespace mpmca::visualization