/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <vector>

#include "foxglove/FrameTransform.pb.h"
#include "foxglove/SceneEntity.pb.h"
#include "mpmca/constants.hpp"
#include "mpmca/control/components.hpp"
#include "mpmca/control/constraint.hpp"
#include "mpmca/control/info.hpp"
#include "mpmca/control/input.hpp"
#include "mpmca/control/simulator.hpp"
#include "mpmca/control/state.hpp"
#include "mpmca/linear_algebra.hpp"
#include "mpmca/utilities/math.hpp"
namespace mpmca::visualization {

template <typename TSIM>
class InfoViewConvertor
{
  public:
    static foxglove::SceneEntity *MakeTransformationMatrixModel(std::size_t index)
    {
        auto *scene_entity = new foxglove::SceneEntity();
        scene_entity->set_frame_id(GetChildFrameId(index));
        auto *center_sphere = scene_entity->mutable_spheres()->Add();

        center_sphere->mutable_pose()->mutable_position()->set_x(0);
        center_sphere->mutable_pose()->mutable_position()->set_y(0);
        center_sphere->mutable_pose()->mutable_position()->set_z(0);

        center_sphere->mutable_pose()->mutable_orientation()->set_w(1);
        center_sphere->mutable_pose()->mutable_orientation()->set_x(0);
        center_sphere->mutable_pose()->mutable_orientation()->set_y(0);
        center_sphere->mutable_pose()->mutable_orientation()->set_z(0);

        center_sphere->mutable_size()->set_x(0.25);
        center_sphere->mutable_size()->set_y(0.25);
        center_sphere->mutable_size()->set_z(0.25);

        auto *forward_arrow = scene_entity->mutable_arrows()->Add();
        forward_arrow->set_shaft_length(0.4);
        forward_arrow->set_shaft_diameter(0.05);
        forward_arrow->set_head_length(0.15);
        forward_arrow->set_head_diameter(0.1);

        forward_arrow->mutable_pose()->mutable_position()->set_x(0);
        forward_arrow->mutable_pose()->mutable_position()->set_y(0);
        forward_arrow->mutable_pose()->mutable_position()->set_z(0);

        auto forward_quat = utilities::Math::EulerToQuaternion({0.0, 0.0, 0.0});
        forward_arrow->mutable_pose()->mutable_orientation()->set_w(forward_quat[0]);
        forward_arrow->mutable_pose()->mutable_orientation()->set_x(forward_quat[1]);
        forward_arrow->mutable_pose()->mutable_orientation()->set_y(forward_quat[2]);
        forward_arrow->mutable_pose()->mutable_orientation()->set_z(forward_quat[3]);
        forward_arrow->mutable_color()->set_a(0.5);
        forward_arrow->mutable_color()->set_r(1);

        auto *right_arrow = scene_entity->mutable_arrows()->Add();
        right_arrow->set_shaft_length(0.4);
        right_arrow->set_shaft_diameter(0.05);
        right_arrow->set_head_length(0.15);
        right_arrow->set_head_diameter(0.1);

        right_arrow->mutable_pose()->mutable_position()->set_x(0);
        right_arrow->mutable_pose()->mutable_position()->set_y(0);
        right_arrow->mutable_pose()->mutable_position()->set_z(0);

        auto right_quat = utilities::Math::EulerToQuaternion({0.0, 0.0, constants::kPi_2});
        right_arrow->mutable_pose()->mutable_orientation()->set_w(right_quat[0]);
        right_arrow->mutable_pose()->mutable_orientation()->set_x(right_quat[1]);
        right_arrow->mutable_pose()->mutable_orientation()->set_y(right_quat[2]);
        right_arrow->mutable_pose()->mutable_orientation()->set_z(right_quat[3]);
        right_arrow->mutable_color()->set_a(0.5);
        right_arrow->mutable_color()->set_g(1);

        auto *down_arrow = scene_entity->mutable_arrows()->Add();
        down_arrow->set_shaft_length(0.4);
        down_arrow->set_shaft_diameter(0.05);
        down_arrow->set_head_length(0.15);
        down_arrow->set_head_diameter(0.1);

        down_arrow->mutable_pose()->mutable_position()->set_x(0);
        down_arrow->mutable_pose()->mutable_position()->set_y(0);
        down_arrow->mutable_pose()->mutable_position()->set_z(0);

        auto down_quat = utilities::Math::EulerToQuaternion({0.0, -constants::kPi_2, 0});
        down_arrow->mutable_pose()->mutable_orientation()->set_w(down_quat[0]);
        down_arrow->mutable_pose()->mutable_orientation()->set_x(down_quat[1]);
        down_arrow->mutable_pose()->mutable_orientation()->set_y(down_quat[2]);
        down_arrow->mutable_pose()->mutable_orientation()->set_z(down_quat[3]);
        down_arrow->mutable_color()->set_a(0.5);
        down_arrow->mutable_color()->set_b(1);

        return scene_entity;
    }

    template <typename COM>
    static void AppendSubModels(
        std::vector<foxglove::SceneEntity *> &models,  //
        std::size_t index,  //
        const control::templates::InfoViewAt<COM, typename TSIM::InfoVector> &info_view_at,  //
        const control::templates::StateViewAt<COM, typename TSIM::StateVector> &state_lower_bounds_view_at,  //
        const control::templates::StateViewAt<COM, typename TSIM::StateVector> &state_upper_bounds_view_at,  //
        const control::templates::ConstraintViewAt<COM, typename TSIM::ConstraintVector> &constraint_lb_view_at,  //
        const control::templates::ConstraintViewAt<COM, typename TSIM::ConstraintVector> &constraint_ub_view_at)
    {
        (void)index;
        if constexpr (std::is_same_v<COM, typename control::components::Hexapod>) {
            foxglove::SceneEntity *bottom_frame = new foxglove::SceneEntity();
            bottom_frame->set_frame_id(GetParentFrameId(index));

            for (size_t i = 0; i < 6; ++i) {
                foxglove::SpherePrimitive *sphere = bottom_frame->mutable_spheres()->Add();
                sphere->mutable_size()->set_x(0.1);
                sphere->mutable_size()->set_y(0.1);
                sphere->mutable_size()->set_z(0.1);
                sphere->mutable_pose()->mutable_position()->set_x(info_view_at.GetHexapodBaseCorners()[i * 3 + 0]);
                sphere->mutable_pose()->mutable_position()->set_y(info_view_at.GetHexapodBaseCorners()[i * 3 + 1]);
                sphere->mutable_pose()->mutable_position()->set_z(info_view_at.GetHexapodBaseCorners()[i * 3 + 2]);
            }
            models.push_back(bottom_frame);

            foxglove::SceneEntity *top_frame = new foxglove::SceneEntity();
            top_frame->set_frame_id(GetChildFrameId(index));

            auto *cylinder = top_frame->mutable_cylinders()->Add();
            double x_top_center = (info_view_at.GetHexapodPlatformCorners()[0 * 3] +  //
                                   info_view_at.GetHexapodPlatformCorners()[1 * 3] +  //
                                   info_view_at.GetHexapodPlatformCorners()[2 * 3] +  //
                                   info_view_at.GetHexapodPlatformCorners()[3 * 3] +  //
                                   info_view_at.GetHexapodPlatformCorners()[4 * 3] +  //
                                   info_view_at.GetHexapodPlatformCorners()[5 * 3]) /
                                  6.;
            double y_top_center = (info_view_at.GetHexapodPlatformCorners()[0 * 3 + 1] +  //
                                   info_view_at.GetHexapodPlatformCorners()[1 * 3 + 1] +  //
                                   info_view_at.GetHexapodPlatformCorners()[2 * 3 + 1] +  //
                                   info_view_at.GetHexapodPlatformCorners()[3 * 3 + 1] +  //
                                   info_view_at.GetHexapodPlatformCorners()[4 * 3 + 1] +  //
                                   info_view_at.GetHexapodPlatformCorners()[5 * 3 + 1]) /
                                  6.;

            double z_top_center = (info_view_at.GetHexapodPlatformCorners()[0 * 3 + 2] +  //
                                   info_view_at.GetHexapodPlatformCorners()[1 * 3 + 2] +  //
                                   info_view_at.GetHexapodPlatformCorners()[2 * 3 + 2] +  //
                                   info_view_at.GetHexapodPlatformCorners()[3 * 3 + 2] +  //
                                   info_view_at.GetHexapodPlatformCorners()[4 * 3 + 2] +  //
                                   info_view_at.GetHexapodPlatformCorners()[5 * 3 + 2]) /
                                  6.;
            double r_top_frame = std::sqrt((info_view_at.GetHexapodPlatformCorners()[0] - x_top_center) *
                                               (info_view_at.GetHexapodPlatformCorners()[0] - x_top_center) +
                                           (info_view_at.GetHexapodPlatformCorners()[1] - y_top_center) *
                                               (info_view_at.GetHexapodPlatformCorners()[1] - y_top_center));

            cylinder->set_top_scale(1.);
            cylinder->set_bottom_scale(1.);
            cylinder->mutable_pose()->mutable_position()->set_x(x_top_center);
            cylinder->mutable_pose()->mutable_position()->set_y(y_top_center);
            cylinder->mutable_pose()->mutable_position()->set_z(z_top_center);
            cylinder->mutable_size()->set_x(2.0 * 0.9 * r_top_frame);
            cylinder->mutable_size()->set_y(2.0 * 0.9 * r_top_frame);
            cylinder->mutable_size()->set_z(0.1);

            for (size_t i = 0; i < 6; ++i) {
                foxglove::SpherePrimitive *sphere = top_frame->mutable_spheres()->Add();
                sphere->mutable_size()->set_x(0.1);
                sphere->mutable_size()->set_y(0.1);
                sphere->mutable_size()->set_z(0.1);
                sphere->mutable_pose()->mutable_position()->set_x(info_view_at.GetHexapodPlatformCorners()[i * 3 + 0]);
                sphere->mutable_pose()->mutable_position()->set_y(info_view_at.GetHexapodPlatformCorners()[i * 3 + 1]);
                sphere->mutable_pose()->mutable_position()->set_z(info_view_at.GetHexapodPlatformCorners()[i * 3 + 2]);
            }

            auto *text = top_frame->mutable_texts()->Add();
            text->set_text(control::components::Hexapod::GetName());
            text->set_billboard(true);
            text->set_font_size(0.1);

            models.push_back(top_frame);
        }
        else if constexpr (std::is_same_v<COM, typename control::components::RevoluteLink>) {
        }
        else if constexpr (std::is_same_v<COM, typename control::components::PrismaticLink>) {
        }
        else if constexpr (std::is_same_v<COM, typename control::components::TransformationMatrix>) {
            auto quat = utilities::Math::EulerToQuaternion({info_view_at.GetRollPitchYaw()[0],
                                                            info_view_at.GetRollPitchYaw()[1],
                                                            info_view_at.GetRollPitchYaw()[2]});
            models.push_back(MakeTransformationMatrixModel(index));  //, info_view_at.GetXyz(), quat));
            (void)quat;
        }
        else if constexpr (std::is_same_v<COM, typename control::components::XyTable>) {
            foxglove::SceneEntity *bottom_frame = new foxglove::SceneEntity();
            bottom_frame->set_frame_id(GetParentFrameId(index));

            auto *bottom_cube = bottom_frame->mutable_cubes()->Add();
            bottom_cube->mutable_pose()->mutable_position()->set_x(
                (state_lower_bounds_view_at.GetX() + state_upper_bounds_view_at.GetY()) / 2.0);
            bottom_cube->mutable_pose()->mutable_position()->set_y(
                (state_lower_bounds_view_at.GetY() + state_upper_bounds_view_at.GetY()) / 2.0);

            bottom_cube->mutable_size()->set_x((state_upper_bounds_view_at.GetX() - state_lower_bounds_view_at.GetX()));
            bottom_cube->mutable_size()->set_y((state_upper_bounds_view_at.GetY() - state_lower_bounds_view_at.GetY()));
            bottom_cube->mutable_size()->set_z(0.1);
            models.push_back(bottom_frame);
        }
        else if constexpr (std::is_same_v<COM, typename control::components::YawTable>) {
        }
    }

    static foxglove::FrameTransform *MakeSimulatorWorldFrameTransform()
    {
        auto *frame_transform = new foxglove::FrameTransform();
        frame_transform->set_parent_frame_id("<root>");
        frame_transform->set_child_frame_id("simulator_world_frame");
        auto quat = utilities::Math::EulerToQuaternion({constants::kPi, 0, 0});
        frame_transform->mutable_rotation()->set_w(quat[0]);
        frame_transform->mutable_rotation()->set_x(quat[1]);
        frame_transform->mutable_rotation()->set_y(quat[2]);
        frame_transform->mutable_rotation()->set_z(quat[3]);
        return frame_transform;
    }

    static foxglove::FrameTransform *MakeSimulatorHeadFrameTransform(const PoseVector &head_pose)
    {
        auto *frame_transform = new foxglove::FrameTransform();
        frame_transform->set_parent_frame_id("simulator_world_frame");
        frame_transform->set_child_frame_id("head_frame_transform");

        frame_transform->mutable_translation()->set_x(head_pose.GetX());
        frame_transform->mutable_translation()->set_y(head_pose.GetY());
        frame_transform->mutable_translation()->set_z(head_pose.GetZ());

        auto quat = utilities::Math::EulerToQuaternion({head_pose.GetPhi(), head_pose.GetTheta(), head_pose.GetPsi()});
        frame_transform->mutable_rotation()->set_w(quat[0]);
        frame_transform->mutable_rotation()->set_x(quat[1]);
        frame_transform->mutable_rotation()->set_y(quat[2]);
        frame_transform->mutable_rotation()->set_z(quat[3]);
        return frame_transform;
    }

    static std::string GetParentFrameId(size_t index)
    {
        if (index == 0)
            return "simulator_world_frame";
        else
            return "simulator_model_frame_" + std::to_string(index - 1);
    }
    static std::string GetChildFrameId(size_t index) { return "simulator_model_frame_" + std::to_string(index); }

    static foxglove::FrameTransform *MakeSimulatorFinalTransform(size_t index, const Vector<3> &xyz,
                                                                 const Vector<4> &quat)
    {
        auto *frame_transform = new foxglove::FrameTransform();
        frame_transform->set_parent_frame_id(GetParentFrameId(index));
        frame_transform->set_child_frame_id(GetChildFrameId(index));

        frame_transform->mutable_translation()->set_x(xyz[0]);
        frame_transform->mutable_translation()->set_y(xyz[1]);
        frame_transform->mutable_translation()->set_z(xyz[2]);

        frame_transform->mutable_rotation()->set_w(quat[0]);
        frame_transform->mutable_rotation()->set_x(quat[1]);
        frame_transform->mutable_rotation()->set_y(quat[2]);
        frame_transform->mutable_rotation()->set_z(quat[3]);
        return frame_transform;
    }

    template <typename COM, std::size_t i>
    static foxglove::FrameTransform *MakeFrameTransform(
        std::size_t index,  //
        const control::templates::StateViewAt<COM, typename TSIM::StateVector> &state_view_at,  //
        const control::templates::InfoViewAt<COM, typename TSIM::InfoVector> &info_view_at)
    {
        auto *frame_transform = new foxglove::FrameTransform();
        frame_transform->set_parent_frame_id(GetParentFrameId(index));
        frame_transform->set_child_frame_id(GetChildFrameId(index));

        if constexpr (std::is_same_v<COM, typename control::components::Hexapod>) {
            frame_transform->mutable_translation()->set_x(state_view_at.GetX());
            frame_transform->mutable_translation()->set_y(state_view_at.GetY());
            frame_transform->mutable_translation()->set_z(state_view_at.GetZ());

            auto quat = utilities::Math::EulerToQuaternion(
                {state_view_at.GetPhi(), state_view_at.GetTheta(), state_view_at.GetPsi()});
            frame_transform->mutable_rotation()->set_w(quat[0]);
            frame_transform->mutable_rotation()->set_x(quat[1]);
            frame_transform->mutable_rotation()->set_y(quat[2]);
            frame_transform->mutable_rotation()->set_z(quat[3]);
        }
        else if constexpr (std::is_same_v<COM, typename control::components::ConstraintFreeBox>) {
            frame_transform->mutable_translation()->set_x(state_view_at.GetX());
            frame_transform->mutable_translation()->set_y(state_view_at.GetY());
            frame_transform->mutable_translation()->set_z(state_view_at.GetZ());

            auto quat = utilities::Math::EulerToQuaternion(
                {state_view_at.GetPhi(), state_view_at.GetTheta(), state_view_at.GetPsi()});
            frame_transform->mutable_rotation()->set_w(quat[0]);
            frame_transform->mutable_rotation()->set_x(quat[1]);
            frame_transform->mutable_rotation()->set_y(quat[2]);
            frame_transform->mutable_rotation()->set_z(quat[3]);
        }
        else if constexpr (std::is_same_v<COM, typename control::components::Gimbal>) {
            frame_transform->mutable_translation()->set_x(0.0);
            frame_transform->mutable_translation()->set_y(0.0);
            frame_transform->mutable_translation()->set_z(0.0);

            auto quat = utilities::Math::EulerToQuaternion(
                {state_view_at.GetPhi(), state_view_at.GetTheta(), state_view_at.GetPsi()});
            frame_transform->mutable_rotation()->set_w(quat[0]);
            frame_transform->mutable_rotation()->set_x(quat[1]);
            frame_transform->mutable_rotation()->set_y(quat[2]);
            frame_transform->mutable_rotation()->set_z(quat[3]);
        }
        else if constexpr (std::is_same_v<COM, typename control::components::RevoluteLink> ||
                           std::is_same_v<COM, typename control::components::PrismaticLink>) {
            double theta;
            double d;

            if constexpr (std::is_same_v<COM, typename control::components::RevoluteLink>) {
                d = info_view_at.GetD();
                theta = state_view_at.GetTheta() + info_view_at.GetThetaOffset();
            }
            else if constexpr (std::is_same_v<COM, typename control::components::PrismaticLink>) {
                d = state_view_at.GetD();
                theta = info_view_at.GetTheta();
            }

            double alpha = info_view_at.GetAlpha();
            double a = info_view_at.GetA();

            TransformationMatrix dh_matrix;
            // clang-format off
            dh_matrix
                <<  std::cos(theta), -std::sin(theta) * std::cos(alpha),  std::sin(theta) * std::sin(alpha), a * std::cos(theta),
                    std::sin(theta),  std::cos(theta) * std::cos(alpha), -std::cos(theta) * std::sin(alpha), a * std::sin(theta),
                    0.0,              std::sin(alpha),                    std::cos(alpha)                  , d,
                    0.0,              0 , 0 , 1;
            // clang-format on

            frame_transform->mutable_translation()->set_x(dh_matrix(0, 3));
            frame_transform->mutable_translation()->set_y(dh_matrix(1, 3));
            frame_transform->mutable_translation()->set_z(dh_matrix(2, 3));

            auto quat = utilities::Math::RotationMatrixToQuaternion(dh_matrix);
            frame_transform->mutable_rotation()->set_w(quat[0]);
            frame_transform->mutable_rotation()->set_x(quat[1]);
            frame_transform->mutable_rotation()->set_y(quat[2]);
            frame_transform->mutable_rotation()->set_z(quat[3]);
        }
        else if constexpr (std::is_same_v<COM, typename control::components::TransformationMatrix>) {
            frame_transform->mutable_translation()->set_x(info_view_at.GetXyz()[0]);
            frame_transform->mutable_translation()->set_y(info_view_at.GetXyz()[1]);
            frame_transform->mutable_translation()->set_z(info_view_at.GetXyz()[2]);

            auto quat = utilities::Math::EulerToQuaternion({info_view_at.GetRollPitchYaw()[0],
                                                            info_view_at.GetRollPitchYaw()[1],
                                                            info_view_at.GetRollPitchYaw()[2]});
            frame_transform->mutable_rotation()->set_w(quat[0]);
            frame_transform->mutable_rotation()->set_x(quat[1]);
            frame_transform->mutable_rotation()->set_y(quat[2]);
            frame_transform->mutable_rotation()->set_z(quat[3]);
        }
        else if constexpr (std::is_same_v<COM, typename control::components::XyTable>) {
            frame_transform->mutable_translation()->set_x(state_view_at.GetX());
            frame_transform->mutable_translation()->set_y(state_view_at.GetY());
            frame_transform->mutable_translation()->set_z(0.0);

            auto quat = utilities::Math::EulerToQuaternion({0.0, 0.0, 0.0});
            frame_transform->mutable_rotation()->set_w(quat[0]);
            frame_transform->mutable_rotation()->set_x(quat[1]);
            frame_transform->mutable_rotation()->set_y(quat[2]);
            frame_transform->mutable_rotation()->set_z(quat[3]);
        }
        else if constexpr (std::is_same_v<COM, typename control::components::YzTracks>) {
            frame_transform->mutable_translation()->set_x(0.0);
            frame_transform->mutable_translation()->set_y(state_view_at.GetY());
            frame_transform->mutable_translation()->set_z(state_view_at.GetZ());

            auto quat = utilities::Math::EulerToQuaternion({0.0, 0.0, 0.0});
            frame_transform->mutable_rotation()->set_w(quat[0]);
            frame_transform->mutable_rotation()->set_x(quat[1]);
            frame_transform->mutable_rotation()->set_y(quat[2]);
            frame_transform->mutable_rotation()->set_z(quat[3]);
        }
        else if constexpr (std::is_same_v<COM, typename control::components::YawTable>) {
            frame_transform->mutable_translation()->set_x(0.0);
            frame_transform->mutable_translation()->set_y(0.0);
            frame_transform->mutable_translation()->set_z(0.0);

            auto quat = utilities::Math::EulerToQuaternion({0.0, 0.0, state_view_at.GetPsi()});
            frame_transform->mutable_rotation()->set_w(quat[0]);
            frame_transform->mutable_rotation()->set_x(quat[1]);
            frame_transform->mutable_rotation()->set_y(quat[2]);
            frame_transform->mutable_rotation()->set_z(quat[3]);
        }

        return frame_transform;
    }

    template <typename I, typename S, typename C>
    static std::vector<foxglove::SceneEntity *> MakeAllModels(
        const control::templates::InfoView<TSIM, I> &info_view,  //
        const control::templates::StateView<TSIM, S> &state_lower_bound_view,  //
        const control::templates::StateView<TSIM, S> &state_upper_bound_view,  //
        const control::templates::ConstraintView<TSIM, C> &constraint_lower_bound_view,  //
        const control::templates::ConstraintView<TSIM, C> &constraint_upper_bound_view)
    {
        std::vector<foxglove::SceneEntity *> models;
        control::templates::index_apply<TSIM::ComponentList::size>([&](auto... Is) {
            ((AppendSubModels(models,  //
                              Is,  //
                              info_view.template ViewAt<Is>(),  //
                              state_lower_bound_view.template ViewAt<Is>(),  //
                              state_upper_bound_view.template ViewAt<Is>(),  //
                              constraint_lower_bound_view.template ViewAt<Is>(),  //
                              constraint_upper_bound_view.template ViewAt<Is>())),
             ...);
        });

        models.push_back(MakeTransformationMatrixModel(TSIM::ComponentList::size));

        return models;
    }

    template <typename S, typename I>
    static std::vector<foxglove::FrameTransform *> MakeAllFrameTransforms(
        const control::templates::StateView<TSIM, S> &state_view_at,  //
        const control::templates::InfoView<TSIM, I> &info_view, const TransformationMatrix &final_transform,
        const PoseVector &head_pose)
    {
        std::vector<foxglove::FrameTransform *> frame_transforms;
        frame_transforms.push_back(MakeSimulatorWorldFrameTransform());

        control::templates::index_apply<TSIM::ComponentList::size>([&](auto... Is) {
            ((frame_transforms.push_back(MakeFrameTransform<typename TSIM::ComponentList::template ComponentAt<Is>, Is>(
                 Is, state_view_at.template ViewAt<Is>(), info_view.template ViewAt<Is>()))),
             ...);
        });

        frame_transforms.push_back(MakeSimulatorFinalTransform(
            TSIM::ComponentList::size, {final_transform(0, 3), final_transform(1, 3), final_transform(2, 3)},
            utilities::Math::RotationMatrixToQuaternion(final_transform)));

        frame_transforms.push_back(MakeSimulatorHeadFrameTransform(head_pose));

        return frame_transforms;
    }
};

}  //namespace mpmca::visualization