/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <eigen3/Eigen/Dense>
#include <vector>

namespace mpmca {

static std::string ToString(const Eigen::MatrixXd& mat)
{
    std::stringstream ss;
    ss << mat;
    return ss.str();
}

template <unsigned M, int Options = Eigen::ColMajor>
using Vector = Eigen::Matrix<double, M, 1, Options>;

template <unsigned M, unsigned N, int Options = Eigen::ColMajor>
using Matrix = Eigen::Matrix<double, M, N, Options>;

using DynamicColumnVector = Eigen::Matrix<double, Eigen::Dynamic, 1>;
using DynamicRowVector = Eigen::Matrix<double, 1, Eigen::Dynamic>;
using DynamicMatrix = Eigen::MatrixXd;

using Eigen::ColMajor;
using Eigen::RowMajor;

class LinearAlgebra
{
  public:
    static inline DynamicColumnVector InitDynamicColumnVector(const std::vector<double>& in)
    {
        DynamicColumnVector b(in.size());
        for (size_t i = 0; i < in.size(); ++i)
            b(i) = in[i];

        return b;
    }

    static inline DynamicRowVector InitDynamicRowVector(const std::vector<double>& in)
    {
        DynamicRowVector b(in.size());
        for (size_t i = 0; i < in.size(); ++i)
            b(i) = in[i];

        return b;
    }

    static inline DynamicMatrix InitDynamicMatrix(const std::vector<std::vector<double>>& in)
    {
        DynamicMatrix b(in.size(), in[0].size());

        for (size_t i = 0; i < in.size(); ++i) {
            if (in[i].size() != b.cols())
                throw std::runtime_error("Not all vectors in this vector of vector have equal length.");

            for (size_t j = 0; j < in[i].size(); ++j)
                b(i, j) = in[i][j];
        }

        return b;
    }
    static inline DynamicMatrix InitDynamicMatrixColumnMajor(int N, int M, double data[])
    {
        DynamicMatrix b(N, M);

        for (int j = 0; j < M; ++j) {
            for (int i = 0; i < N; ++i) {
                b(i, j) = data[N * j + i];
            }
        }

        return b;
    }
};
}  //namespace mpmca
