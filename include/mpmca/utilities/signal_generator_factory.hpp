/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/utilities/signal_generator.hpp"
#include "mpmca/utilities/signal_generator_constant.hpp"
#include "mpmca/utilities/signal_generator_sine.hpp"
#include "mpmca/utilities/signal_generator_step.hpp"

namespace mpmca::utilities {
class SignalGeneratorFactory
{
  public:
    static std::unique_ptr<SignalGenerator> MakeSignalGenerator(ConfigurationPtr config)
    {
        std::string type = config->GetValue<std::string>("Type", "Sine");

        if (type.compare("Sine") == 0) {
            return std::make_unique<SignalGeneratorSine>(config);
        }
        else if (type.compare("Step") == 0) {
            return std::make_unique<SignalGeneratorStep>(config);
        }
        else if (type.compare("Constant") == 0) {
            return std::make_unique<SignalGeneratorConstant>(config);
        }
        else {
            throw(std::runtime_error("The SignalGeneratorFactory cannot create a signal generator of type " + type +
                                     ". Options are: Sine, Step, and Constant."));
            return nullptr;
        }
    }
};
}  //namespace mpmca::utilities