/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <cstddef>  // for std::size_t

namespace mpmca::utilities {
class PreventHeapAllocationMixin
{
  public:
    PreventHeapAllocationMixin(){};

  private:
    // prevent allocation on heap
    static void *operator new(std::size_t) = delete;
    static void *operator new[](std::size_t) = delete;
    static void operator delete(void *) = delete;
    static void operator delete[](void *) = delete;
};
}  //namespace mpmca