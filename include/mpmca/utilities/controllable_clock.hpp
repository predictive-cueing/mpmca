/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <chrono>

#include "mpmca/utilities/clock.hpp"

namespace mpmca::utilities {
class ControllableClock : public Clock
{
  public:
    ControllableClock(const Clock& clock);
    ControllableClock(const std::chrono::milliseconds& dt, const std::chrono::milliseconds& current_time);

    void Tick() override;
    void Reset();
    void SetCurrentTime(const std::chrono::milliseconds& current_time);
    void SetCurrentTimeMs(int64_t current_time_ms);
};
}  //namespace mpmca