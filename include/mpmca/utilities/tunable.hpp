/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <atomic>
#include <deque>

#include "mpmca/utilities/i_parameter.hpp"
#include "mpmca/utilities/i_tunable.hpp"
#include "mpmca/utilities/tunable_change.hpp"

namespace mpmca::utilities {
template <class T>
class Tunable : public ITunable
{
  public:
    Tunable(const std::string& basic_name, const std::string& unique_name)
        : ITunable(basic_name, unique_name, Tunable::GetParameterType())
        , m_has_parameter(false)
    {
    }

    Tunable(const std::string& basic_name, const std::string& unique_name, const T& initial)
        : Tunable(basic_name, unique_name)
    {
        SetNextValue(initial, 0, 0, TunableChange::ChangeMethod::kStep);
    };

    void Reset()
    {
        m_has_parameter.store(false);
        T last = m_parameters.back();
        m_parameters.clear();
        m_changes.clear();
        SetNextValue(last);
    }

    void SetNextValue(const T& tunable, int64_t t_start_ms = 0, int64_t t_change_duration_ms = 0,
                      TunableChange::ChangeMethod method = TunableChange::ChangeMethod::kStep)
    {
        m_parameters.push_back(T(tunable));
        m_changes.push_back(TunableChange(t_start_ms, t_change_duration_ms, method));
        m_has_parameter.store(true);
    };

    template <class... Args>
    double GetValue(int64_t t_current_ms, Args... args) const
    {
        if (!m_has_parameter.load())
            throw std::runtime_error("The Tunable " + GetBasicName() + " (" + GetUniqueName() + ")" +
                                     " has no parameter in memory.");

        return GetInternalValue(t_current_ms, m_parameters.size() - 1, args...);
    }

  private:
    template <class... Args>
    double GetInternalValue(int64_t t_current_ms, int n_parameter, Args... args) const
    {
        double current = m_parameters[n_parameter].GetValue(args...);
        double weight = m_changes[n_parameter].Weight(t_current_ms);

        if (n_parameter > 0 && weight < 1.0)
            current = current * weight + (1.0 - weight) * GetInternalValue(t_current_ms, n_parameter - 1, args...);

        return current;
    }

    static ParameterType GetParameterType();

    std::deque<T> m_parameters;
    std::deque<TunableChange> m_changes;
    std::atomic<bool> m_has_parameter;
};
}  //namespace mpmca::utilities