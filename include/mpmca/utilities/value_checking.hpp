/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

namespace mpmca::utilities {
class ValueChecking
{
  public:
    static void CheckNaN(const double& val, const std::string& name)
    {
        if (std::isnan(val))
            throw std::invalid_argument(name + " is nan.");
    }

    static void CheckInf(const double& val, const std::string& name)
    {
        if (std::isinf(val))
            throw std::invalid_argument(name + " is inf.");
    }
    static void CheckAll(const double& val, const std::string& name)
    {
        CheckNaN(val, name);
        CheckInf(val, name);
    }
};
}  //namespace mpmca::utilities