/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <fstream>

#include "mpmca/utilities/file_container.hpp"

namespace mpmca::utilities {

class A2D2FileReader
{
  public:
    static FileContainer ContainerFromA2D2JsonFile(const std::string& name, const std::string& filename);
};

}  //namespace mpmca::utilities