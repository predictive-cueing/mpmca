/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <memory>
namespace mpmca {
namespace pipeline {
class Task;
}

namespace utilities {
class Compare
{
  public:
    template <class T>
    static bool CompareUnits(const std::shared_ptr<T>& a, const std::shared_ptr<T>& b)
    {
        if (a->GetExecutionOrder() == b->GetExecutionOrder())
            throw(std::runtime_error("The execution order values of " + a->GetName() + " and " + b->GetName() +
                                     " are equal. They are " + std::to_string(a->GetExecutionOrder()) + " and " +
                                     std::to_string(b->GetExecutionOrder())));

        return a->GetExecutionOrder() < b->GetExecutionOrder();
    }

    static bool CompareTasks(const std::shared_ptr<mpmca::pipeline::Task>& a,
                             const std::shared_ptr<mpmca::pipeline::Task>& b);
};
}  //namespace utilities
}  //namespace mpmca