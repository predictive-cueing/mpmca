/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/utilities/configuration.hpp"
#include "mpmca/utilities/signal_generator.hpp"

namespace mpmca::utilities {
class SignalGeneratorSine : public SignalGenerator
{
  private:
    double m_amplitude;
    double m_frequency;
    double m_phase;

  public:
    SignalGeneratorSine(ConfigurationPtr config);
    SignalGeneratorSine(double amplitude, double frequency_rad, double phase);
    double CalculateValueAtTimeMs(long t_ms) override;
    std::string GetTypeName() const override { return "Sine"; }
};
}  //namespace mpmca