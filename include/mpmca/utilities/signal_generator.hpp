/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <string>

namespace mpmca::utilities {
class SignalGenerator
{
  public:
    virtual std::string GetTypeName() const = 0;
    virtual double CalculateValueAtTimeMs(long t_ms) = 0;
};
}  //namespace mpmca::utilities