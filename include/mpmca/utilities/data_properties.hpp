/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <string>
namespace mpmca::utilities {

class DataProperties
{
  private:
    std::string m_name;
    std::string m_frame_of_reference;
    std::string m_units;
    std::string m_total_name;

  public:
    DataProperties(const std::string& frame_of_reference, const std::string& name, const std::string& units);

    inline const std::string& GetName() const { return m_name; }
    inline const std::string& GetFrameOfReference() const { return m_frame_of_reference; }
    inline const std::string& GetUnits() const { return m_units; }
    inline const std::string& GetTotalName() const { return m_total_name; }
};
}  //namespace mpmca::utilities