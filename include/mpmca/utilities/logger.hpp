/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <chrono>
#include <memory>
#include <mutex>

namespace mpmca {
namespace pipeline {
class Pipeline;
}

namespace utilities {

class Configuration;
typedef std::shared_ptr<Configuration> ConfigurationPtr;

/**
 * @brief The logger class records string messages, and prints them to stdout or to a file.
 *
 */
class Logger
{
  public:
    enum class LoggerType : int
    {
        kSpdLog,
        kConsole
    };

    enum class LoggerOutputLevel : int
    {
        kTrace = 0,
        kDebug = 1,
        kInfo = 2,
        kWarning = 3,
        kError = 4,
        kCritical = 5,
        kNone = 6
    };

    Logger(const std::string& name, const std::string& type_name);
    ~Logger() = default;

    void Trace(const std::string& message) const;
    void Debug(const std::string& message) const;
    void Info(const std::string& message) const;
    void Warning(const std::string& message) const;
    void Error(const std::string& message) const;
    void Error(const std::exception& c) const;
    void Critical(const std::string& message) const;
    void Critical(const std::exception& c) const;

    void SetName(const std::string& name);
    void SetTypeName(const std::string& type_name);
    void SetLoggerOutputLevel(LoggerOutputLevel output_level);

    const std::string& GetName() const;
    const std::string& GetTypeName() const;

    class Impl;

  private:
    friend mpmca::pipeline::Pipeline;
    Logger(ConfigurationPtr config, const std::string& name, const std::string& type_name);

    static std::mutex s_pointer_to_impl_mutex;
    static std::unique_ptr<Impl> s_pointer_to_impl;

    std::string m_name;
    std::string m_type_name;
};
}  //namespace utilities
}  //namespace mpmca