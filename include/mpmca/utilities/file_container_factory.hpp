/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/utilities/configuration.hpp"
#include "mpmca/utilities/file_container.hpp"
namespace mpmca::utilities {
class FileContainerFactory
{
  public:
    static FileContainer ConstructFileContainer(ConfigurationPtr config, double dt);
};
}  //namespace mpmca::utilities