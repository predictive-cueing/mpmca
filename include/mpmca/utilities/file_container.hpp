/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <string>
#include <unordered_map>

#include "mpmca/linear_algebra.hpp"
#include "mpmca/utilities/logger.hpp"

namespace mpmca::utilities {
class FileContainer
{
  protected:
    const std::string m_name;
    Logger m_logger;
    const std::string m_filename;
    const std::vector<std::string> m_field_names;
    const std::vector<int64_t> m_time;
    const std::vector<double> m_x;
    const std::vector<double> m_y;
    const std::vector<double> m_z;
    const DynamicMatrix m_data;

    const std::unordered_map<std::string, int> m_field_numbers;
    const int64_t m_n_samples;
    const int64_t m_n_fields;
    const int64_t m_start;
    const int64_t m_end;
    const int64_t m_t_min;
    const int64_t m_t_max;
    const bool m_time_indexing;
    const bool m_position_indexing;
    const double m_dt;
    const int64_t m_dt_ms;

    int64_t m_kk;
    bool m_reached_end;

    static bool GetTimeIndexing(const std::string& indexing);
    static bool GetPositionIndexing(const std::string& indexing);
    static bool GetValidIndexingArgument(const std::string& indexing);
    static std::unordered_map<std::string, int> GetFieldNumbers(const std::vector<std::string>& field_names);

  public:
    FileContainer(const std::string& name, const std::string& filename, const std::string& indexing,
                  const std::vector<std::string>& field_names, const std::vector<int64_t>& time,
                  const std::vector<double>& x, const std::vector<double>& y, const std::vector<double>& z,
                  const DynamicMatrix& data, double dt);

    double GetDataInField(int field_index);
    double GetDataInField(int field_index, int64_t index);
    double GetDataInField(const std::string& name);
    double GetDataInFieldOrFallback(const std::string& name, double fallback_value = 0.0);
    double GetDataInField(const std::string& name, int64_t index);

    int64_t SetIndex(int64_t index);
    int64_t SetIndexByTime(int64_t t);
    int64_t SetIndexByPosition(double x, double y, double z, int64_t pre_samples);
    int64_t GetCurrentIndex() const;
    int64_t GetNumSamples() const;
    int64_t GetNumFields() const;

    const std::string& GetFieldName(int field_id) const;
    int GetFieldIndex(const std::string& name) const;
    int GetFieldIndex(const std::string& name, int not_found_value) const;

    const std::string& GetName() const;
    const std::string& GetFilename() const;

    int64_t GetTimeStartMs() const;
    int64_t GetTimeEndMs() const;
    bool GetReachedEnd() const;

    template <typename Vector>
    Vector GetVector()
    {
        return m_data.row(m_kk);
    }

    static FileContainer ConstructFileContainer(utilities::ConfigurationPtr config, double dt);
};
}  //namespace mpmca::utilities