/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/utilities/configuration.hpp"
#include "mpmca/utilities/signal_generator.hpp"

namespace mpmca::utilities {
class SignalGeneratorStep : public SignalGenerator
{
  private:
    double m_amplitude;
    double m_constant;
    double m_start_interval_ms;
    double m_duration_ms;

  public:
    SignalGeneratorStep(ConfigurationPtr config);
    SignalGeneratorStep(double amplitude, double constant, double frequency_rad, double phase);
    double CalculateValueAtTimeMs(long t_ms) override;
    std::string GetTypeName() const override { return "Step"; }
};
}  //namespace mpmca::utilities