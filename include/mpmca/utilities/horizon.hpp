/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <map>
#include <memory>
#include <vector>

namespace mpmca::utilities {
class Clock;
class StepCount;
class Configuration;
typedef std::shared_ptr<mpmca::utilities::Configuration> ConfigurationPtr;

class Horizon
{
  public:
    Horizon(const Clock& clock, int n_steps);

    Horizon(double dt, int n_steps) = delete;
    Horizon(double dt, StepCount steps_count, int prediction_max_step_ms) = delete;
    Horizon(ConfigurationPtr config, double dt) = delete;

    Horizon(ConfigurationPtr config);
    Horizon(int dt_ms, int n_steps);
    Horizon(StepCount steps_count, int prediction_max_step_ms);

    void PrintHorizon() const;

    static StepCount JsonToStepCount(ConfigurationPtr config);

    int GetNMpcFundamental() const;
    int GetNMpc() const;
    int GetNMpcPred() const;
    int GetNPrediction() const;

    double GetDt() const;
    int GetDtMs() const;

    int GetFinalPredictionHorizonTimeMs() const;
    int GetFinalControllerHorizonTimeMs() const;

    std::vector<double> GetHorizonTime() const;
    double GetHorizonTime(int) const;
    std::vector<double> GetHorizonDeltaTime() const;
    double GetHorizonDeltaTime(int) const;
    std::vector<int> GetHorizonTimeMs() const;
    int GetHorizonTimeMs(int) const;

    std::vector<int> GetHorizonTimeSteps() const;
    int GetHorizonTimeSteps(int) const;
    std::vector<int> GetHorizonDeltaTimeMs() const;
    int GetHorizonDeltaTimeMs(int) const;
    std::vector<int> GetHorizonDeltaTimeSteps() const;
    int GetHorizonDeltaTimeSteps(int) const;
    std::vector<unsigned> GetHorizonDeltaTimeStepsUnsigned() const;

    std::vector<double> GetPredictionHorizonTime() const;
    double GetPredictionHorizonTime(int) const;
    std::vector<double> GetPredictionHorizonDeltaTime() const;
    double GetPredictionHorizonDeltaTime(int j) const;
    std::vector<int> GetPredictionHorizonDeltaTimeMs() const;
    int GetPredictionHorizonDeltaTimeMs(int j) const;
    std::vector<int> GetPredictionHorizonDeltaTimeSteps() const;
    int GetPredictionHorizonDeltaTimeSteps(int j) const;
    std::vector<int> GetPredictionHorizonTimeMs() const;
    int GetPredictionHorizonTimeMs(int) const;
    std::vector<int> GetPredictionHorizonIndex() const;
    int GetPredictionHorizonIndex(int) const;

    int GetTimeMsToPredictionHorizonIndex(int time) const;
    int GetTimeMsToHorizonIndex(int time) const;

  private:
    int m_n_mpc_fundamental;
    int m_n_mpc;
    int m_n_pred;
    int m_n_mpc_pred;

    const double m_dt;
    const int m_dt_ms;
    const int m_prediction_horizon_maximum_time_step;

    int m_mpc_step;

    std::vector<double> m_controller_horizon_time;
    std::vector<double> m_prediction_horizon_time;

    std::vector<double> m_controller_horizon_delta_time;
    std::vector<double> m_prediction_horizon_delta_time;

    std::vector<int> m_controller_horizon_time_steps;
    std::vector<int> m_prediction_horizon_time_steps;

    std::vector<int> m_controller_horizon_delta_time_steps;
    std::vector<int> m_prediction_horizon_delta_time_steps;

    std::vector<int> m_prediction_horizon_time_ms;
    std::vector<int> m_prediction_horizon_index;

    std::vector<int> m_controller_horizon_time_ms;
    std::vector<int> m_controller_horizon_delta_time_ms;
    std::vector<int> m_prediction_horizon_delta_time_ms;

    std::map<int, int> m_time_ms_horizon_index;
    std::map<int, int> m_time_ms_prediction_horizon_index;

    void SetPredictionHorizon();
    void SetControllerHorizon();
};
}  //namespace mpmca::utilities