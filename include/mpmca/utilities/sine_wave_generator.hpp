/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/constants.hpp"
#include "mpmca/utilities/configuration.hpp"
namespace mpmca::utilities {

struct SineWaveGenerator
{
    // y = amplitude_m_or_rad_s2 * sin( 2 * pi * frequency_hz * t + phase_rad ) + offset_rad_m;
    double m_amplitude_m_or_rad_s2;
    double m_frequency_hz;
    double m_phase_offset_rad;
    double m_offset_rad_m;

    SineWaveGenerator(utilities::ConfigurationPtr config)
        : m_amplitude_m_or_rad_s2(config->GetValue<double>("AmplitudeMeterOrRadPerS2", 0.1))
        , m_frequency_hz(config->GetValue<double>("FrequencyHz", 0.1))
        , m_phase_offset_rad(config->GetValue<double>("PhaseOffsetRad", 0))
        , m_offset_rad_m(config->GetValue<double>("OffsetRadOrM", 0))
    {
    }
    double GetValue(double t)
    {
        return m_offset_rad_m +
               m_amplitude_m_or_rad_s2 * std::sin(2 * constants::kPi * m_frequency_hz * t + m_phase_offset_rad);
    }
};
}  //namespace mpmca::utilities