/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <filesystem>
#include <iostream>
#include <string>

namespace mpmca::utilities {

class TestConstants
{
  public:
    static TestConstants* Instance()
    {
        static TestConstants test_constants;
        return &test_constants;
    }

    static std::string TEST_DATA_PATH() { return TestConstants::Instance()->GetTestDataPath(); }

    void SetTestDataPath(const std::string& value_set_file_path)
    {
        if (value_set_file_path.back() != '/') {
            m_test_data_path = value_set_file_path + "/";
        }
        else {
            m_test_data_path = value_set_file_path;
        }
    }

  private:
    TestConstants()
        : m_test_data_path("data/test_data/")
    {
    }

    std::string GetTestDataPath() const { return m_test_data_path; }

    std::string m_test_data_path;
};
}  //namespace mpmca::utilities
