/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/json.hpp"
#include "mpmca/json_tools.hpp"
#include "mpmca/linear_algebra.hpp"
#include "mpmca/utilities/configuration.hpp"
#include "mpmca/utilities/i_parameter.hpp"
namespace mpmca::utilities {
class Map2D : public IParameter
{
  public:
    enum InterpolationMethod
    {
        kNearestNeighbour,
        kLinear
    };

    Map2D();

    Map2D(const std::initializer_list<double>& x_vector, const std::initializer_list<double>& y_vector,
          const std::initializer_list<std::vector<double>>& value_matrix);

    Map2D(const DynamicRowVector& x_vector, const DynamicRowVector& y_vector, const DynamicMatrix& value_matrix);
    Map2D(ConfigurationPtr config);
    double GetValue(double at_x, double at_y, InterpolationMethod method = InterpolationMethod::kLinear) const;
    double GetValue(double at_x, double at_y, double outside,
                    InterpolationMethod method = InterpolationMethod::kLinear) const;

    void SetXVector(const std::vector<double>& x_vector);
    void SetYVector(const std::vector<double>& y_vector);
    void SetValueMatrix(const std::vector<std::vector<double>>& value_matrix);

    const DynamicRowVector& GetXVector() const;
    const DynamicRowVector& GetYVector() const;
    const DynamicMatrix& GetValueMatrix() const;

    void ProcessNewValues();

  private:
    double Interp2(double xi, double yi) const;
    double Nearest(double xi, double yi) const;

    DynamicRowVector m_x_vector;
    DynamicRowVector m_y_vector;
    DynamicRowVector m_x_mid_vector;
    DynamicRowVector m_y_mid_vector;
    DynamicMatrix m_value_matrix;

    DynamicMatrix m_d_value_dx_matrix;
    DynamicMatrix m_d_value_dy_matrix;
    DynamicRowVector m_dy_vector;

    double m_x_min;
    double m_x_max;
    double m_y_min;
    double m_y_max;

    bool m_requires_process_call;
};

inline void to_json(nlohmann::json& jsn, Map2D const& map)
{
    jsn = nlohmann::json::array();
    jsn["x"] = map.GetXVector();
    jsn["y"] = map.GetYVector();
    jsn["Value"] = nlohmann::json::array();

    for (size_t i = 0; i < map.GetValueMatrix().rows(); ++i) {
        DynamicRowVector tmp = map.GetValueMatrix().row(i);
        jsn["Value"][i] = tmp;
    }
}

inline void from_json(const nlohmann::json& json, Map2D& map)
{
    map.SetXVector(std::vector<double>{json.at("x")});
    map.SetYVector(std::vector<double>{json.at("y")});
    map.SetValueMatrix(std::vector<std::vector<double>>{json.at("Value")});
    map.ProcessNewValues();
}
}  //namespace mpmca::utilities