/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <iostream>
#include <string>

#include "mpmca/utilities/configuration.hpp"
#include "mpmca/utilities/logger.hpp"
#include "mpmca/utilities/mpmca_asio.hpp"

namespace mpmca::utilities {
class UdpSenderReceiver
{
  protected:
    bool m_initialized;
    const std::string m_name;
    Logger m_logger;

    std::string m_local_endpoint_host;
    std::string m_remote_endpoint_host;

    ::asio::io_service m_io_service;
    ::asio::ip::udp::resolver m_resolver;
    ::asio::ip::udp::resolver::iterator m_iter;
    ::asio::ip::udp::endpoint m_local_endpoint;
    ::asio::ip::udp::endpoint m_remote_endpoint;
    ::asio::ip::udp::socket m_socket;
    ::asio::error_code m_sending_error_code;
    ::asio::error_code m_receiving_error_code;

    static std::string CheckEndpoint(const std::string& endpoint_host, const std::string& endpoint_port);

    template <typename T>
    bool ReceiveSinglePackage(T& receive_package);

  public:
    UdpSenderReceiver(const std::string& name, ConfigurationPtr config);
    UdpSenderReceiver(const std::string& name, const std::string& local_endpoint_host,
                      const std::string& local_endpoint_port, const std::string& remote_endpoint_host,
                      const std::string& remote_endpoint_port, bool throw_on_error = true);

    template <typename T>
    bool Receive(T&, bool handle_multiple_packages = true);

    template <typename T>
    bool Send(const T&);

    template <typename T>
    bool SendProtobuf(const T&);

    bool SendRawData(const std::vector<char>& buffer);

    template <typename T>
    static bool IsIntegerMultiple(T test_number, T divisor);

    void ClearBuffer();
    std::string GetName();
};

template <typename T>
inline bool UdpSenderReceiver::IsIntegerMultiple(T test_number, T divisor)
{
    double test_number_d = static_cast<double>(test_number);
    double divisor_d = static_cast<double>(divisor);

    double division = test_number_d / divisor_d;

    return std::abs(division - std::round(division)) < 1e-5;
}

template <typename T>
inline bool UdpSenderReceiver::Send(const T& object)
{
    if (!m_initialized)
        return false;

    std::vector<char> buffer;
    buffer.resize(sizeof(T));
    memcpy(buffer.data(), &object, sizeof(T));

    return SendRawData(buffer);
}

template <typename T>
bool UdpSenderReceiver::SendProtobuf(const T& package)
{
    if (!m_initialized)
        return false;

    std::vector<char> buffer;
    buffer.resize(package.ByteSizeLong());
    if (package.SerializeToArray(buffer.data(), buffer.size()))
        return SendRawData(buffer);
    else
        return false;
}

template <typename T>
bool UdpSenderReceiver::ReceiveSinglePackage(T& receive_pack)
{
    std::array<char, sizeof(T)> recv_buf;
    ::asio::error_code err;
    m_socket.receive_from(::asio::buffer(recv_buf), m_remote_endpoint, 0, err);

    if (m_receiving_error_code && !err) {
        m_logger.Info("Error receive: " + m_receiving_error_code.message() + " cleared!");
        m_receiving_error_code = err;
    }

    if (err) {
        if (m_receiving_error_code != err) {
            m_receiving_error_code = err;
            m_logger.Error("Error receive: " + m_receiving_error_code.message());
        }
        return false;
    }
    else {
        memcpy((char*)&receive_pack, (char*)&recv_buf, sizeof(T));
        return true;
    }
}

template <typename T>
inline bool UdpSenderReceiver::Receive(T& receive_pack, bool handle_multiple_packages)
{
    try {
        size_t bytes_available = m_socket.available();
        if (handle_multiple_packages && bytes_available > 0 &&
            IsIntegerMultiple(bytes_available, static_cast<size_t>(sizeof(T)))) {
            size_t num_packages_in_buffer = bytes_available / sizeof(T);

            for (size_t i = 0; i < num_packages_in_buffer; ++i) {
                if (!ReceiveSinglePackage(receive_pack))
                    return false;
            }
            return true;
        }
        else if (!handle_multiple_packages && bytes_available == sizeof(T)) {
            return ReceiveSinglePackage(receive_pack);
        }
        else if (bytes_available > 0) {
            ClearBuffer();

            if (handle_multiple_packages) {
                m_logger.Warning("There are " + std::to_string(bytes_available) +
                                 ", but I expect an integer multiple of " + std::to_string(sizeof(T)) + ".");
            }
            else {
                m_logger.Warning("There are " + std::to_string(bytes_available) + ", but I expect exactly " +
                                 std::to_string(sizeof(T)) + " bytes.");
            }
            return false;
        }
        else {
            return false;
        }
    }
    catch (std::exception& e) {
        m_logger.Error("Error : " + std::string(e.what()));
        return false;
    }
}
}  //namespace mpmca