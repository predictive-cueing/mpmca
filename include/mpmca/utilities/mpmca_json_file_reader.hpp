/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/json.hpp"
#include "mpmca/linear_algebra.hpp"
#include "mpmca/utilities/file_container.hpp"

namespace mpmca::utilities {
class MpmcaJsonFileReader
{
  public:
    static FileContainer ContainerFromMpmcaJsonFile(const std::string& name, const std::string& filename,
                                                    const std::string& indexing, double dt);

  private:
    static std::vector<int64_t> GetTime(nlohmann::json& data);

    static std::vector<std::pair<std::string, int>> GetAvailableFieldIndices(
        nlohmann::json& data, const std::vector<std::string>& requested_field_names);

    static std::vector<double> GetField(nlohmann::json& data, const std::string& field_name);

    static DynamicMatrix GetData(nlohmann::json& data, const std::vector<std::pair<std::string, int>>&);
};
}  //namespace mpmca::utilities
