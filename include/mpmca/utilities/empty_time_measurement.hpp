/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <chrono>
#include <string>
#include <vector>

#include "mpmca/utilities/clock_index.hpp"
#include "mpmca/utilities/prevent_heap_allocation_mixin.hpp"

namespace mpmca::utilities {
class EmptyTimeMeasurement : public PreventHeapAllocationMixin
{
  private:
    typedef std::chrono::high_resolution_clock clock;

    void CalculateStatistics();

  public:
    EmptyTimeMeasurement();
    void PrintStatisticsMs();
    void PrintStatisticsUs();
    void PrintStatisticsNs();

    void ResetStatistics();
    ClockIndex RegisterClock(const std::string& name);

    void StartCycle();
    void StopCycle() {};
    clock::duration StopClock(const ClockIndex& index);

    clock::time_point ClockTime(const ClockIndex& index);
    clock::duration ClockDuration(const ClockIndex& index);

    uint32_t ClockDurationSinceCycleStartMs(const ClockIndex& index) const;
    uint32_t ClockDurationSinceCycleStartUs(const ClockIndex& index) const;
    uint32_t ClockDurationSinceCycleStartNs(const ClockIndex& index) const;

    uint32_t ClockDurationSinceCreationMs(const ClockIndex& index) const;
    uint32_t ClockDurationSinceCreationUs(const ClockIndex& index) const;
    uint32_t ClockDurationSinceCreationNs(const ClockIndex& index) const;

    uint32_t ClockDurationMs(const ClockIndex& index) const;
    uint32_t ClockDurationUs(const ClockIndex& index) const;
    uint32_t ClockDurationNs(const ClockIndex& index) const;

    std::vector<uint32_t> ClockDurationsSinceCycleStartUs() const;
    std::vector<uint32_t> ClockDurationsUs() const;
    std::vector<uint32_t> ClockDurationsMs() const;
    std::vector<uint32_t> ClockDurationsNs() const;

    std::string ClockName(const ClockIndex& index) const;
    int NumClocks() const;

    std::vector<std::string> GetClockNames() const;
};
}  //namespace mpmca::utilities