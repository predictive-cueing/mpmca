/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <initializer_list>

#include "mpmca/json.hpp"
#include "mpmca/json_tools.hpp"
#include "mpmca/linear_algebra.hpp"
#include "mpmca/utilities/configuration.hpp"
#include "mpmca/utilities/i_parameter.hpp"

namespace mpmca::utilities {
class Map1D : public IParameter
{
  public:
    enum InterpolationMethod
    {
        kNearestNeighbour,
        kLinear
    };

    Map1D();
    Map1D(std::initializer_list<double> x_vector, std::initializer_list<double> value_vector);
    Map1D(DynamicRowVector x_vector, DynamicRowVector value_vector);
    Map1D(ConfigurationPtr config);
    double GetValue(double at_x, InterpolationMethod method = InterpolationMethod::kLinear) const;

    const DynamicRowVector& GetXVector() const { return m_x_vector; }
    const DynamicRowVector& GetValueVector() const { return m_value_vector; }

    void SetXVector(const std::vector<double>& x_vector);
    void SetValueVector(const std::vector<double>& value_vector);

    void SetXVector(const DynamicRowVector& x_vector);
    void SetValueVector(const DynamicRowVector& value_vector);

    void ProcessNewValues();

  private:
    DynamicRowVector m_x_vector;
    DynamicRowVector m_x_mid;
    DynamicRowVector m_value_vector;
    DynamicRowVector m_d_value_dx;

    double Interp1(double at_x) const;
    double Nearest(double at_x) const;

    bool m_requires_process_call;
};

inline void to_json(nlohmann::json& jsn, Map1D const& map)
{
    jsn = nlohmann::json::array();
    jsn["x"] = map.GetXVector();
    jsn["Value"] = map.GetValueVector();
}

inline void from_json(const nlohmann::json& json, Map1D& map)
{
    map.SetXVector(std::vector<double>{json.at("x")});
    map.SetValueVector(std::vector<double>{json.at("Value")});
    map.ProcessNewValues();
}

}  //namespace mpmca::utilities
