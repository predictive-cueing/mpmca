/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

namespace mpmca::utilities {
class TimeMeasurement;
class EmptyTimeMeasurement;
class ClockIndex
{
  private:
    friend TimeMeasurement;
    friend EmptyTimeMeasurement;
    const int m_clock_idx;
    ClockIndex(int index);

  public:
    int GetIndex() const;
};
}  //namespace mpmca