/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <algorithm>
#include <cmath>

#include "mpmca/linear_algebra.hpp"
namespace mpmca::utilities {

class Math
{
  public:
    static double Sign(double val);
    // signum and sign are not the same thing!!!
    // signum should return 0 if val is equal to zero.
    template <typename T>
    int Signum(T val)
    {
        return (T(0) < val) - (val < T(0));
    }

    static double GetCosineFade(double v1, double v2, double s_fade, double s_takeover);

    template <class T>
    static std::array<double, 3> BodyToWorld(const std::array<T, 3>& body_vector,
                                             const std::array<double, 3>& world_to_body_euler_rotation)
    {
        std::array<double, 3> world_vector;

        double body_x = body_vector[0];
        double body_y = body_vector[1];
        double body_z = body_vector[2];

        double cos_phi = std::cos(world_to_body_euler_rotation[0]);
        double sin_phi = std::sin(world_to_body_euler_rotation[0]);
        double cos_theta = std::cos(world_to_body_euler_rotation[1]);
        double sin_theta = std::sin(world_to_body_euler_rotation[1]);
        double cos_psi = std::cos(world_to_body_euler_rotation[2]);
        double sin_psi = std::sin(world_to_body_euler_rotation[2]);

        world_vector[0] = cos_psi * cos_theta * body_x + (cos_psi * sin_theta * sin_phi - sin_psi * cos_phi) * body_y +
                          (cos_psi * sin_theta * cos_phi + sin_psi * sin_phi) * body_z;
        world_vector[1] = sin_psi * cos_theta * body_x + (sin_psi * sin_theta * sin_phi + cos_psi * cos_phi) * body_y +
                          (sin_psi * sin_theta * cos_phi - cos_psi * sin_phi) * body_z;
        world_vector[2] = -sin_theta * body_x + cos_theta * sin_phi * body_y + cos_theta * cos_phi * body_z;
        return world_vector;
    }

    template <class T>
    static std::array<double, 3> WorldToBody(const std::array<T, 3>& world_vector,
                                             const std::array<double, 3>& world_to_body_euler_rotation)
    {
        double world_x = world_vector[0];
        double world_y = world_vector[1];
        double world_z = world_vector[2];

        double cos_phi = std::cos(world_to_body_euler_rotation[0]);
        double sin_phi = std::sin(world_to_body_euler_rotation[0]);
        double cos_theta = std::cos(world_to_body_euler_rotation[1]);
        double sin_theta = std::sin(world_to_body_euler_rotation[1]);
        double cos_psi = std::cos(world_to_body_euler_rotation[2]);
        double sin_psi = std::sin(world_to_body_euler_rotation[2]);

        std::array<double, 3> body_vector;

        body_vector[0] = cos_psi * cos_theta * world_x + sin_psi * cos_theta * world_y - sin_theta * world_z;
        body_vector[1] = (cos_psi * sin_theta * sin_phi - sin_psi * cos_phi) * world_x +
                         (sin_psi * sin_theta * sin_phi + cos_psi * cos_phi) * world_y + cos_theta * sin_phi * world_z;
        body_vector[2] = (cos_psi * sin_theta * cos_phi + sin_psi * sin_phi) * world_x +
                         (sin_psi * sin_theta * cos_phi - cos_psi * sin_phi) * world_y + cos_theta * cos_phi * world_z;

        return body_vector;
    }

    template <class T>
    static std::array<double, 3> EulerRatesToAngularVelocities(const std::array<T, 3>& body_euler_rates,
                                                               const std::array<T, 3>& world_rotation)
    {
        std::array<double, 3> angular_velocities;

        double sin_phi = std::sin(world_rotation[0]);
        double cos_phi = std::cos(world_rotation[0]);
        double sin_theta = std::sin(world_rotation[1]);
        double cos_theta = std::cos(world_rotation[1]);

        angular_velocities[0] = body_euler_rates[0] - sin_theta * body_euler_rates[2];
        angular_velocities[1] = cos_phi * body_euler_rates[1] + cos_theta * sin_phi * body_euler_rates[2];
        angular_velocities[2] = -sin_phi * body_euler_rates[1] + cos_theta * cos_phi * body_euler_rates[2];

        return angular_velocities;
    }

    static Vector<3> EulerRatesToAngularVelocities(const Vector<3>& body_euler_rates, const Vector<3>& world_rotation);
    static Vector<4> EulerToQuaternion(const Vector<3> euler_angles);

    static Vector<4> RotationMatrixToQuaternion(const Matrix<4, 4>& m_in);
    static Vector<4> EulerToQuaternion(double roll, double pitch, double yaw);

    static std::array<double, 3> ConvertIso1151QuaternionToIso8855Euler(const std::array<double, 4>& quaternion);
    static double GetCosineFadeOptimized(double v1, double v2, double sin_val2);
    static double GetCosineFadeVal2(double s_fade, double s_takeover);
    static unsigned PositiveModulo(int value, unsigned m);
    static double PositiveModuloDouble(double x, double y);
    static double GetAngleDiff(double angle1, double angle2);
    static double ConvertRadiansToDegrees(double angle_radians);
    static double ConvertDegreesToRadians(double angle_degrees);

    static double Angle2Pi(double angle);
};
}  //namespace mpmca::utilities