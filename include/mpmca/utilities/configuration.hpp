/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <iostream>
#include <memory>
#include <string>

#include "mpmca/json.hpp"
#include "mpmca/utilities/file_tools.hpp"
#include "mpmca/utilities/logger.hpp"

namespace mpmca::utilities {
class Configuration;
typedef std::shared_ptr<Configuration> ConfigurationPtr;

class Configuration : public std::enable_shared_from_this<Configuration>
{
  public:
    static ConfigurationPtr CreateRootConfig(const std::string& name, bool use_default_values = false);
    static ConfigurationPtr CreateRootConfig(const std::string& name, nlohmann::json js,
                                             bool use_default_values = false);
    static ConfigurationPtr CreateChildConfig(const std::string& name, std::weak_ptr<Configuration> parent,
                                              std::shared_ptr<Configuration> root, nlohmann::json js,
                                              bool is_object_array, bool use_default_values);
    static ConfigurationPtr CreateChildConfig(size_t index, std::weak_ptr<Configuration> parent,
                                              std::shared_ptr<Configuration> root, nlohmann::json js,
                                              bool use_default_values);

    Configuration(const std::string& name, bool use_default_values = false);
    Configuration(const std::string& name, nlohmann::json js, bool use_default_values = false);

    Configuration(const std::string& name, std::weak_ptr<Configuration> parent, std::shared_ptr<Configuration> root,
                  nlohmann::json js, bool is_object_array, bool use_default_values);
    Configuration(size_t index, std::weak_ptr<Configuration> parent, std::shared_ptr<Configuration> root,
                  nlohmann::json js, bool use_default_values);

    static ConfigurationPtr CreateConfigFromString(const std::string& name, const std::string& json,
                                                   bool use_default_values = false);

    static ConfigurationPtr CreateConfigFromFile(const std::string& name, const std::string& filename,
                                                 bool use_default_values = false);

    std::string GetPath() const;
    std::string GetConfigurationPath() const;
    void SetPath(const std::string& path);
    void SetConfigurationPath(const std::string& path);

    void Update(nlohmann::json);
    void SetJson(nlohmann::json);
    void ReadJsonFromFile(const std::string& filename);
    void ReadJsonFromString(const std::string& json_text);

    void SaveJson(const std::string& filename, bool overwrite);
    nlohmann::json GetJson() const;
    nlohmann::json GetGlobalJson() const;
    std::string GetName() const;

    nlohmann::json GetChildrenJson();
    nlohmann::json GetChildJson(size_t index);
    void SetChildJson(size_t child_index, const std::string& name, size_t array_index, nlohmann::json child_json,
                      bool is_object_array_element);

    ConfigurationPtr GetConfigurationObject(const std::string& name, bool create_if_does_not_exist = false);

    bool IsBoolean(const std::string& name) const;
    bool IsString(const std::string& name) const;
    bool IsNumber(const std::string& name) const;
    bool IsArray(const std::string& name) const;
    bool IsObject(const std::string& name) const;
    bool IsObjectArray() const;
    bool IsObjectArrayElement() const;
    void SetChildIndex(size_t index);

    size_t GetIndex() const;
    size_t GetChildIndex() const;

    size_t GetNumObjectArrayElements() const;
    size_t GetNumArrayElements() const;
    size_t GetNumObjectElements() const;

    void ReadParentJsonFile();
    void SetFilename(const std::string& fn);

    std::vector<std::string> GetObjectNames() const;

    static void Deletor(Configuration*);

    ConfigurationPtr GetRootConfiguration();
    ConfigurationPtr GetArray(const std::string& name);
    ConfigurationPtr GetConfigurationObjectArray(const std::string& name, size_t num_items = 1);
    ConfigurationPtr GetConfigurationObjectArrayElement(size_t index);

    template <typename T>
    void SetValue(const std::string& name, T value, bool create_if_does_not_exist = false);

    template <typename T>
    T GetValue(const std::string& name, T default_value);

    template <typename T>
    T GetValueFromArray(size_t index, std::vector<T> default_value);

    template <typename T>
    T GetValueFromArray(size_t index, T default_value);

    bool GetKeyExists(const std::string& name) const;

  protected:
    ~Configuration();

    nlohmann::json m_my_json;
    static nlohmann::json MergeJson(nlohmann::json j1, nlohmann::json j2);
    static nlohmann::json MergeJsonArray(nlohmann::json j1, nlohmann::json j2, size_t array_index);

    void MergeParentJsonFile(const std::string& path, const std::string& filename);

    ConfigurationPtr AddChild(ConfigurationPtr child);

    Logger m_logger;
    std::string m_name;
    bool m_is_object_array_element;
    bool m_is_object_array;
    bool m_use_default_values;
    int m_index;
    int m_child_index;

    bool m_is_root;
    std::shared_ptr<Configuration> m_root;
    std::weak_ptr<Configuration> m_parent;
    std::vector<std::weak_ptr<Configuration>> m_children;
    std::string m_path;
    std::string m_configuration_path;
};

template <typename T>
T Configuration::GetValue(const std::string& name, T default_value)
{
    if (!GetKeyExists(name)) {
        if (m_use_default_values)
            m_my_json[name] = default_value;
        else {
            throw std::runtime_error("Key " + name + " is not present in the configuration object with path:\n" +
                                     GetConfigurationPath());
        }
    }

    try {
        return m_my_json[name];
    }
    catch (const std::exception& e) {
        throw std::runtime_error("Exception occurred while trying to read configuration value with name " + name +
                                 " in the configuration object with path:\n" + GetConfigurationPath() +
                                 "\nThe exception message is\n" + e.what());
        return m_my_json[name];
    }
}

template <typename T>
void Configuration::SetValue(const std::string& name, T value, bool create_if_does_not_exist)
{
    if (GetKeyExists(name) || create_if_does_not_exist) {
        m_my_json[name] = value;
    }
    else {
        throw std::runtime_error("Key " + name + " is not present in the configuration object with path:\n" +
                                 GetConfigurationPath());
    }
}

template <typename T>
T Configuration::GetValueFromArray(size_t index, std::vector<T> default_value)
{
    if (!m_my_json.is_array())
        throw std::runtime_error(m_name + " is not an array of values in configuration object with path:\n" +
                                 GetConfigurationPath());

    if (index >= default_value.size() && index >= m_my_json.size())
        throw std::runtime_error(
            "You requested an index from an array that exceeds the size of the default value array in "
            "configuration object with path:\n" +
            GetConfigurationPath());

    if (index >= m_my_json.size()) {
        if (m_use_default_values) {
            for (size_t i = m_my_json.size(); i < default_value.size(); ++i) {
                m_my_json[i] = default_value[i];
            }
        }
        else
            throw std::runtime_error("Index " + std::to_string(index) +
                                     " does not exist in the configuration object with path:\n" +
                                     GetConfigurationPath());
    }
    return m_my_json.at(index);
}

template <typename T>
T Configuration::GetValueFromArray(size_t index, T default_value)
{
    auto default_vector = std::vector<T>(index + 1, default_value);
    return GetValueFromArray<T>(index, default_vector);
}

}  //namespace mpmca::utilities