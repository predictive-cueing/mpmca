/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <string>

namespace mpmca::utilities {
struct FileParts
{
    std::string path;  //!< containing folder, if provided, including trailing slash
    std::string name;  //!< base file name, without extension
    std::string ext;  //!< extension, including '.'
};

class FileTools
{
  public:
    static FileParts GetFileParts(const std::string& fullpath);
    static bool GetFileExists(const std::string& filename);
};
}  //namespace mpmca