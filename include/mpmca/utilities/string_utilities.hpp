/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <string>
#include <vector>
namespace mpmca::utilities {
class StringUtilities
{
  public:
    static std::vector<std::string> SplitString(const std::string &my_string, char separator);
};
}  //namespace mpmca