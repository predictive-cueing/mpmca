/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <vector>

namespace mpmca::utilities {

class StepCount
{
  public:
    StepCount() = default;
    StepCount(int step_ms, int count);
    void Add(int step, int count);
    int GetNumStepCounts() const;
    int GetCount(size_t index) const;
    int GetStepMs(size_t index) const;
    int GetStep(size_t index) const;
    int GetMinStepMs() const;

  private:
    std::vector<int> m_step_ms;
    std::vector<int> m_count;
};
}  //namespace mpmca::utilities