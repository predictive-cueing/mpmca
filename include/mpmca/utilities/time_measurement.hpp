/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <chrono>
#include <string>
#include <vector>

#include "mpmca/utilities/clock_index.hpp"

namespace mpmca::utilities {
class TimeMeasurement
{
  private:
    typedef std::chrono::steady_clock clock;

    clock::time_point m_point_construction;
    clock::time_point m_point_cycle_start;
    clock::time_point m_point_previous_clock;

    std::vector<std::string> m_clock_names;
    std::vector<clock::time_point> m_time_points;
    std::vector<clock::duration> m_durations_since_previous;
    std::vector<clock::duration> m_durations_since_cycle_start;

    std::vector<double> m_average_durations_ns;
    std::vector<uint32_t> m_minimum_durations_ns;
    std::vector<uint32_t> m_maximum_durations_ns;
    std::vector<double> m_n_measurements;

    clock::duration m_duration_since_previous_cycle_start;

    bool m_prev_cycle_stopped = true;

    void CalculateStatistics();

  public:
    TimeMeasurement();

    void PrintStatisticsMs() const;
    void PrintStatisticsUs() const;
    void PrintStatisticsNs() const;

    std::string GetStatisticsMs(const std::string& line_prefix = "", bool mark_nondeterministic_numbers = false) const;
    std::string GetStatisticsUs(const std::string& line_prefix = "", bool mark_nondeterministic_numbers = false) const;
    std::string GetStatisticsNs(const std::string& line_prefix = "", bool mark_nondeterministic_numbers = false) const;

    void ResetStatistics();
    void Protect(const ClockIndex& index) const;

    ClockIndex RegisterClock(const std::string& name);

    void StartCycle();
    void StopCycle();
    void StopClock(const ClockIndex& index);

    clock::time_point GetClockTime(const ClockIndex& index) const;
    clock::duration GetClockDurationSinceCycleStart(const ClockIndex& index) const;
    clock::duration GetClockDurationSincePrevious(const ClockIndex& index) const;

    uint32_t GetClockDurationSinceCycleStartMs(const ClockIndex& index) const;
    uint32_t GetClockDurationSinceCycleStartUs(const ClockIndex& index) const;
    uint32_t GetClockDurationSinceCycleStartNs(const ClockIndex& index) const;

    uint32_t GetClockDurationSincePreviousMs(const ClockIndex& index) const;
    uint32_t GetClockDurationSincePreviousUs(const ClockIndex& index) const;
    uint32_t GetClockDurationSincePreviousNs(const ClockIndex& index) const;

    uint32_t GetDurationBetweenPreviousTwoCycleStartsMs() const;
    uint32_t GetDurationBetweenPreviousTwoCycleStartsUs() const;
    uint32_t GetDurationBetweenPreviousTwoCycleStartsNs() const;

    std::vector<uint32_t> GetClockDurationsSinceCycleStartMs() const;
    std::vector<uint32_t> GetClockDurationsSinceCycleStartUs() const;
    std::vector<uint32_t> GetClockDurationsSinceCycleStartNs() const;

    std::vector<uint32_t> GetClockDurationsSincePreviousUs() const;
    std::vector<uint32_t> GetClockDurationsSincePreviousMs() const;
    std::vector<uint32_t> GetClockDurationsSincePreviousNs() const;

    std::vector<double> GetAverageDurationMs() const;
    std::vector<double> GetAverageDurationUs() const;
    std::vector<double> GetAverageDurationNs() const;

    std::vector<uint32_t> GetMinDurationMs() const;
    std::vector<uint32_t> GetMinDurationUs() const;
    std::vector<uint32_t> GetMinDurationNs() const;

    std::vector<uint32_t> GetMaxDurationMs() const;
    std::vector<uint32_t> GetMaxDurationUs() const;
    std::vector<uint32_t> GetMaxDurationNs() const;
    std::string GetClockName(const ClockIndex& index) const;
    std::string GetClockName(size_t index) const;

    size_t GetNumClocks() const;

    const std::vector<std::string>& GetClockNames() const;
};
}  //namespace mpmca::utilities