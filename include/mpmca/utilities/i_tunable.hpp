/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/utilities/i_parameter.hpp"
#include "mpmca/utilities/tunable_register.hpp"

namespace mpmca::utilities {
class ITunable
{
  public:
    const std::string& GetBasicName() const;
    const std::string& GetUniqueName() const;
    ParameterType GetParameterType() const;
    static TunableRegister& GetTunableRegister();

  protected:
    ITunable(const std::string& basic_name, const std::string& unique_name, ParameterType type);
    ~ITunable();

  private:
    std::string m_name_basic;
    std::string m_name_unique;
    ParameterType m_type;
    static TunableRegister s_tunable_register;
};
}  //namespace mpmca