/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <algorithm>
#include <string>
#include <vector>

#include "mpmca/utilities/string_utilities.hpp"

// taken from https://stackoverflow.com/questions/865668/parsing-command-line-arguments-in-c

namespace mpmca::utilities {
class CommandLineOptionsParser
{
  public:
    CommandLineOptionsParser(int& argc, char** argv);

    size_t GetNumTokens() const;
    const std::vector<std::string>& GetTokens() const;
    const std::string& GetCommandOption(const std::string& option) const;
    bool GetCommandOptionExists(const std::string& option) const;

  private:
    std::vector<std::string> m_tokens;
};
}  //namespace mpmca::utilities
