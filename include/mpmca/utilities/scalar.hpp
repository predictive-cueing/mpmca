/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/utilities/i_parameter.hpp"

namespace mpmca::utilities {
class Scalar : public IParameter
{
  private:
    double m_value;

  public:
    Scalar(double value);
    double GetValue() const;
    void SetValue(double i);
};
}  //namespace mpmca::utilities