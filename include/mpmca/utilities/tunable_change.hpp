/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <cstdint>

namespace mpmca::utilities {
class TunableChange
{
  public:
    enum ChangeMethod
    {
        kStep,
        kLinear,
        kCosine,
    };

    TunableChange(int64_t t_start_ms, int64_t t_change_duration_ms, ChangeMethod method);
    double Weight(int64_t t_current_ms) const;

  private:
    const int64_t m_t_start_ms;
    const int64_t m_t_change_duration_ms;
    const double m_t_start_ms_real;
    const double m_t_change_duration_ms_real;
    const ChangeMethod m_change_method;
};
}  //namespace mpmca::utilities