/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <unordered_map>
#include <vector>

#include "mpmca/utilities/logger.hpp"
#include "mpmca/utilities/prevent_heap_allocation_mixin.hpp"

namespace mpmca::utilities {
class ITunable;
class TunableRegister : public PreventHeapAllocationMixin
{
  public:
    TunableRegister();
    ITunable* GetParameterByUniqueName(const std::string& name) const;
    std::vector<ITunable*> GetAllParametersByBasicName(const std::string& name) const;
    void RegisterParameter(ITunable* parameter_ptr);
    void DeregisterParameter(ITunable* parameter_ptr);

  private:
    Logger m_logger;
    std::unordered_map<std::string, ITunable*> m_register;
    bool ExistsInRegister(const std::string& name) const;
};
}  //namespace mpmca