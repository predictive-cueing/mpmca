/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/utilities/configuration.hpp"
#include "mpmca/utilities/signal_generator.hpp"

namespace mpmca::utilities {
class SignalGeneratorConstant : public SignalGenerator
{
  private:
    double m_constant;

  public:
    SignalGeneratorConstant(ConfigurationPtr config);
    SignalGeneratorConstant(double constant);
    double CalculateValueAtTimeMs(long t_ms) override;
    std::string GetTypeName() const override { return "Constant"; }
};
}  //namespace mpmca::utilities