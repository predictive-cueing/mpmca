/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <iostream>
#include <string>

namespace mpmca::utilities {
enum class ParameterType
{
    kNone,
    kScalar,
    kMap1D,
    kMap2D
};

inline std::string ToString(const ParameterType& type)
{
    switch (type) {
        case ParameterType::kNone:
            return std::string("None");
            break;
        case ParameterType::kScalar:
            return std::string("Scalar");
            break;
        default:
            return std::string("Undefined ParameterType");
    }
}

inline std::ostream& operator<<(std::ostream& os, const ParameterType& type)
{
    os << ToString(type);
    return os;
}

class IParameter
{
  public:
    IParameter(ParameterType type);

  private:
    ParameterType m_type;
};

}  //namespace mpmca::utilities