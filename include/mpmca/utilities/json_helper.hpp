/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */

#include <fstream>
#include <iostream>
#include <nlohmann/json.hpp>

#include "mpmca/utilities/file_tools.hpp"

namespace mpmca::utilities {
class JsonHelper
{
  public:
    static std::string ReadJsonFromFile(const std::string& filename)
    {
        if (!FileTools::GetFileExists(filename))
            throw std::runtime_error("Configuration file " + filename + " does not exist.");

        // the file is first read into a json object and then dumped to a string,
        // to validate the correctness of the json content
        nlohmann::json contents;
        std::ifstream(filename) >> contents;
        return contents.dump();
    }
};
}  //namespace mpmca