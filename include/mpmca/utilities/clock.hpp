/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/utilities/configuration.hpp"
#include "mpmca/utilities/prevent_heap_allocation_mixin.hpp"

namespace mpmca::utilities {
class Clock : public PreventHeapAllocationMixin
{
  public:
    // Clock(ConfigurationPtr config);
    explicit Clock(const std::chrono::milliseconds& dt);

    virtual void Tick();

    double GetCurrentTime() const;
    int64_t GetCurrentTimeMs() const;
    int GetDtMs() const;
    double GetDt() const;

    static double MillisecondToSecond(int time_ms) { return (double)time_ms / 1000.0; }

  protected:
    void TickClock();
    const int m_dt_ms;
    const double m_dt;
    int64_t m_current_time_ms;

    // max value of int64_t is
    // 9,223,372,036,854,775,807
    // if we use this to express time in milliseconds, we have
    // 9,223,372,036,854,775 s, which is 292471000 days. Should be plenty.
};
}  //namespace mpmca::utilities