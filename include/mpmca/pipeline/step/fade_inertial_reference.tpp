/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/control/state.hpp"
#include "mpmca/messages/controller_initial_state_and_input.hpp"
#include "mpmca/messages/controller_output_reference_plan.hpp"
#include "mpmca/messages/controller_state_error_weight_plan.hpp"
#include "mpmca/pipeline/step/fade_inertial_reference.hpp"
#include "mpmca/types/inertial_vector.hpp"
#include "mpmca/utilities/clock.hpp"
#include "mpmca/utilities/math.hpp"

namespace mpmca::pipeline::step {

template <typename TSIM>
void FadeInertialReference<TSIM>::ScaleReferenceOutputPlan(messages::ControllerOutputReferencePlan<9>& message,
                                                           const DataBucket& data_bucket)
{
    double k_fade_start = GetFadeValue(data_bucket, 0);

    for (int i = 0; i < message.y_ref_plan.size(); ++i) {
        double k_fade = GetFadeValue(data_bucket, data_bucket.GetHorizon().GetHorizonTime(i));

        InertialVector y_ref = message.y_ref_plan.at(i);

        y_ref.GetFx() = y_ref.GetFx() * k_fade;
        y_ref.GetFy() = y_ref.GetFy() * k_fade;
        y_ref.GetFz() = (y_ref.GetFz() - 9.81) * k_fade + 9.81;
        y_ref.GetOmegaX() = y_ref.GetOmegaX() * k_fade;
        y_ref.GetOmegaY() = y_ref.GetOmegaY() * k_fade;
        y_ref.GetOmegaZ() = y_ref.GetOmegaZ() * k_fade;
        y_ref.GetAlphaX() = y_ref.GetAlphaX() * k_fade;
        y_ref.GetAlphaY() = y_ref.GetAlphaY() * k_fade;
        y_ref.GetAlphaZ() = y_ref.GetAlphaZ() * k_fade;

        message.y_ref_plan.at(i) = y_ref;
    }
}

template <typename TSIM>
FadeInertialReference<TSIM>::FadeInertialReference(Task& task, const std::string& name,
                                                   utilities::ConfigurationPtr config)
    : Step(task, name, "FadeInertialReference", config)
    , m_t_fade_in(std::max(0.0, config->GetValue<double>("TimeFadeInSeconds", 0.)))
    , m_t_fade_out(std::max(0.0, config->GetValue<double>("TimeFadeOutSeconds", 0.)))
    , m_fade_out_position_weight(config->GetValue<double>("FadeOutPositionWeight", 10))
    , m_fade_out_velocity_weight(config->GetValue<double>("FadeOutVelocityWeight", 10))
{
}

template <typename TSIM>
std::string FadeInertialReference<TSIM>::ToScientificNotationString(double val)
{
    std::ostringstream stream_object;
    //Add double to stream
    stream_object << val;
    // Get string from output string stream
    return stream_object.str();
}

template <typename TSIM>
void FadeInertialReference<TSIM>::PrepareTaskMessageBus(const Task& task, MessageBus& message_bus)
{
    message_bus.MakeMessage<mpmca::messages::ControllerStateErrorWeightPlan<TSIM>>(task.GetHorizon());
}

template <typename TSIM>
void FadeInertialReference<TSIM>::CheckTaskMessageBusPrepared(const Task& task, const MessageBus& message_bus)
{
    message_bus.RequireMessage<mpmca::messages::ControllerOutputReferencePlan<9>>();
    message_bus.RequireMessage<mpmca::messages::ControllerInitialStateAndInput<TSIM>>();
    message_bus.RequireMessage<mpmca::messages::ControllerStateErrorWeightPlan<TSIM>>();
}
template <typename TSIM>
void FadeInertialReference<TSIM>::FadeOut(DataBucket& data_bucket)
{
    double position_weight = m_fade_out_position_weight;
    double velocity_weight = m_fade_out_velocity_weight;

    if (GetStateMachineClient().GetCurrentState() == State::kStopping) {
        double t_since_fade_start = data_bucket.GetPipelineStateTimeMs() / 1000.0;
        position_weight =
            utilities::Math::GetCosineFade(0, m_fade_out_position_weight, m_t_fade_out, t_since_fade_start);
        velocity_weight =
            utilities::Math::GetCosineFade(0, m_fade_out_velocity_weight, m_t_fade_out / 2.0, t_since_fade_start);
    }

    typename TSIM::StateVector w_x = TSIM::StateVector::Constant(position_weight);
    mpmca::control::MakeStateView<TSIM>(w_x).GetVelocity().fill(velocity_weight);

    data_bucket.GetTaskSignalBus().UpdateMessage<messages::ControllerStateErrorWeightPlan<TSIM>>(
        //
        [&](messages::ControllerStateErrorWeightPlan<TSIM>& message) {
            for (int i = 0; i < data_bucket.GetHorizon().GetNMpcPred(); ++i) {
                message.w_x_plan.at(i) = w_x;
            }
        });

    auto x0 =
        data_bucket.GetTaskSignalBus().ReadUpdatedMessage<messages::ControllerInitialStateAndInput<TSIM>>().x_initial;

    auto vel = mpmca::control::MakeStateView<TSIM>(x0).GetVelocity();

    if (vel.norm() < 1e-4)
        TransitionStateMachine();

    if (data_bucket.GetMainTickCycle() % 100 == 0) {
        using namespace std::string_literals;
        m_logger.Info("Waiting for platform to reach zero velocity.\n The norm of the velocity vector is " +
                      ToScientificNotationString(vel.norm()) + "\n");
    }
}

template <typename TSIM>
double FadeInertialReference<TSIM>::GetFadeValue(const DataBucket& data_bucket, double horizon_time_s)
{
    if (GetStateMachineClient().GetCurrentState() == State::kRun) {
        return utilities::Math::GetCosineFade(0.0, 1.0, m_t_fade_in,
                                              data_bucket.GetPipelineStateTime() + horizon_time_s);
    }
    else if (GetStateMachineClient().GetCurrentState() == State::kStopping) {
        return utilities::Math::GetCosineFade(1.0, 0.0, m_t_fade_out,
                                              data_bucket.GetPipelineStateTime() + horizon_time_s);
    }
    else {
        return 0;
    }
}

template <typename TSIM>
void FadeInertialReference<TSIM>::MainTick(DataBucket& data_bucket)
{
    data_bucket.GetTaskSignalBus().UpdateMessage<messages::ControllerOutputReferencePlan<9>>(
        [&](auto& message) { ScaleReferenceOutputPlan(message, data_bucket); });

    if (GetStateMachineClient().GetCurrentState() == State::kBooting) {
        GetStateMachineClient().SetGoToState(State::kBooted);
        return;
    }

    if (GetStateMachineClient().GetCurrentState() == State::kStopping ||
        GetStateMachineClient().GetCurrentState() == State::kStopped) {
        FadeOut(data_bucket);
    }
    else {
        TransitionStateMachine();
    }
}

}  //namespace mpmca::pipeline::step