/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <array>
#include <memory>

#include "mpmca/messages/controller_output_reference_plan.hpp"
#include "mpmca/pipeline/step.hpp"
#include "mpmca/pipeline/step_registrar.hpp"
#include "mpmca/utilities/signal_generator.hpp"
#include "mpmca/utilities/signal_generator_factory.hpp"

namespace mpmca::pipeline::step {

template <std::size_t NY>
class ReferenceSignalGenerator : public Step
{
  public:
    ReferenceSignalGenerator(Task& task, const std::string& name, utilities::ConfigurationPtr config);
    ~ReferenceSignalGenerator() = default;
    void PrepareTaskMessageBus(const Task& task, MessageBus&) override;
    void CheckTaskMessageBusPrepared(const Task& task, const MessageBus&) override;
    void MainTick(DataBucket& data_bucket) override;
    void Prepare() override { GetSafety().Prepared(); };

  protected:
    std::array<std::unique_ptr<utilities::SignalGenerator>, NY> m_generators;
    int64_t m_delay_ms;
};

template <std::size_t NY>
ReferenceSignalGenerator<NY>::ReferenceSignalGenerator(Task& task, const std::string& name,
                                                       utilities::ConfigurationPtr config)
    : Step(task, name, "ReferenceSignalGenerator", config)
    , m_delay_ms(config->GetValue<int64_t>("DelayMs", 0))
{
    auto signals_config = config->GetConfigurationObjectArray("Signals");
    for (size_t i = 0; i < m_generators.size(); ++i) {
        m_generators[i] = utilities::SignalGeneratorFactory::MakeSignalGenerator(
            signals_config->GetConfigurationObjectArrayElement(i));

        m_logger.Info("Created a signal generator of type " + m_generators[i]->GetTypeName() + " for signal number " +
                      std::to_string(i) + ". Value at time 0 is " +
                      std::to_string(m_generators[i]->CalculateValueAtTimeMs(0 - m_delay_ms)));
    }
}

template <std::size_t NY>
void ReferenceSignalGenerator<NY>::MainTick(DataBucket& data_bucket)
{
    TransitionStateMachine();

    data_bucket.GetTaskSignalBus().UpdateMessage<messages::ControllerOutputReferencePlan<NY>>(
        [&](messages::ControllerOutputReferencePlan<NY>& message) {
            //
            for (size_t signal_index = 0; signal_index < NY; ++signal_index) {
                for (size_t horizon_index = 0; horizon_index < data_bucket.GetHorizon().GetNMpc(); ++horizon_index) {
                    int64_t time = data_bucket.GetTaskStateTimeMs() +
                                   data_bucket.GetHorizon().GetHorizonTimeMs(horizon_index) - m_delay_ms;
                    message.y_ref_plan.at(horizon_index)[signal_index] =
                        m_generators[signal_index]->CalculateValueAtTimeMs(time);
                }
            }
            //
        });
}

template <std::size_t NY>
void ReferenceSignalGenerator<NY>::PrepareTaskMessageBus(const Task& task, MessageBus& message_bus)
{
    message_bus.MakeMessage<messages::ControllerOutputReferencePlan<NY>>(task.GetHorizon());
}

template <std::size_t NY>
void ReferenceSignalGenerator<NY>::CheckTaskMessageBusPrepared(const Task& task, const MessageBus& message_bus)
{
    message_bus.RequireMessage<messages::ControllerOutputReferencePlan<NY>>();
}
}  //namespace mpmca::pipeline::step