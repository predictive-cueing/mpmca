/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/pipeline/step/reference_predictor.hpp"
#include "mpmca/pipeline/task.hpp"

namespace mpmca::pipeline::step {
template <typename T>
ReferencePredictor<T>::ReferencePredictor(Task& task, const std::string& name, utilities::ConfigurationPtr config)
    : Step(task, name, "ReferencePredictor", config)
    , m_clock_index_init_prediction(task.GetTimeMeasurement().RegisterClock("ReferencePredictor:Init"))
    , m_clock_index_simulation(task.GetTimeMeasurement().RegisterClock("ReferencePredictor:Simulate"))
    , m_clock_index_fill_bus(task.GetTimeMeasurement().RegisterClock("ReferencePredictor:FillBus"))
    , m_predictor(config, task.GetHorizon(), "Predictor")
{
}
template <typename T>
void ReferencePredictor<T>::Prepare()
{
    GetSafety().Prepared();
}
template <typename T>
void ReferencePredictor<T>::PrepareTaskMessageBus(const Task& task, MessageBus& message_bus)
{
    m_predictor.PrepareTaskMessageBus(message_bus);
}
template <typename T>
void ReferencePredictor<T>::CheckTaskMessageBusPrepared(const Task& task, const MessageBus& message_bus)
{
    m_predictor.CheckTaskMessageBusPrepared(message_bus);
}
template <typename T>
void ReferencePredictor<T>::MainTick(DataBucket& data_bucket)
{
    m_predictor.ReadMessageBus(data_bucket.GetTaskSignalBus());

    if (GetStateMachineClient().GetCurrentState() == State::kIdle) {
        data_bucket.GetTimeMeasurement().StopClock(m_clock_index_init_prediction);
        data_bucket.GetTimeMeasurement().StopClock(m_clock_index_simulation);
    }
    else if (GetStateMachineClient().GetCurrentState() == State::kStarting) {
        data_bucket.GetTimeMeasurement().StopClock(m_clock_index_init_prediction);
        m_predictor.PrepareTick();
        data_bucket.GetTimeMeasurement().StopClock(m_clock_index_simulation);
    }
    else if (GetStateMachineClient().GetCurrentState() == State::kRun) {
        m_predictor.SetCurrentTimeMs(data_bucket.GetPipelineTimeMs());
        data_bucket.GetTimeMeasurement().StopClock(m_clock_index_init_prediction);
        m_predictor.Predict();
        data_bucket.GetTimeMeasurement().StopClock(m_clock_index_simulation);
    }

    m_predictor.UpdateMessageBus(data_bucket.GetTaskSignalBus());

    data_bucket.GetTimeMeasurement().StopClock(m_clock_index_fill_bus);

    TransitionStateMachine();
}
}  //namespace mpmca::pipeline::step