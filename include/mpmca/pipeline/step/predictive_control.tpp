/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <fstream>
#include <stdexcept>

#include "mpmca/control/state.hpp"
#include "mpmca/linear_algebra.hpp"
#include "mpmca/messages/controller_bounds.hpp"
#include "mpmca/messages/controller_constraint_plan.hpp"
#include "mpmca/messages/controller_final_transform.hpp"
#include "mpmca/messages/controller_inertial_plan.hpp"
#include "mpmca/messages/controller_initial_state_and_input.hpp"
#include "mpmca/messages/controller_input_error_weight_plan.hpp"
#include "mpmca/messages/controller_input_plan.hpp"
#include "mpmca/messages/controller_output_error_weight_plan.hpp"
#include "mpmca/messages/controller_output_expected_plan.hpp"
#include "mpmca/messages/controller_output_reference_plan.hpp"
#include "mpmca/messages/controller_pose_plan.hpp"
#include "mpmca/messages/controller_state_error_weight_plan.hpp"
#include "mpmca/messages/controller_state_plan.hpp"
#include "mpmca/messages/controller_state_reference_plan.hpp"
#include "mpmca/pipeline/step/predictive_control.hpp"
#include "mpmca/types/pose_rot_mat_vector.hpp"

namespace mpmca::pipeline::step {

template <typename TSIM>
PredictiveControl<TSIM>::PredictiveControl(Task& task, const std::string& name, utilities::ConfigurationPtr config)
    : Step(task, name, "PredictiveControl", config)
    , m_controller(task.GetHorizon().GetHorizonDeltaTimeStepsUnsigned(), task.GetHorizon().GetDt(),
                   TSIM::StateVector::Zero(), m_simulator.GetNominalInput(), m_controller_options)
    , m_x0(TSIM::StateVector::Zero())
    , m_neutral_reference_output(
          config->GetValue<typename TSIM::OutputVector>("NeutralReferenceOutput", TSIM::OutputVector::Zero()))
    , m_neutral_reference_state(
          config->GetValue<typename TSIM::StateVector>("NeutralReferenceState", TSIM::StateVector::Zero()))
    , m_neutral_reference_input(
          config->GetValue<typename TSIM::InputVector>("NeutralReferenceInput", TSIM::InputVector::Zero()))
    , m_neutral_output_error_weight(
          config->GetValue<typename TSIM::OutputVector>("OutputErrorWeight", m_controller.GetOutputErrorWeightAt(0)))
    , m_neutral_input_error_weight(
          config->GetValue<typename TSIM::InputVector>("InputErrorWeight", m_controller.GetInputErrorWeightAt(0)))
    , m_neutral_state_error_weight(
          config->GetValue<typename TSIM::StateVector>("StateErrorWeight", m_controller.GetStateErrorWeightAt(0)))
    , m_protect(config->GetValue<bool>("Protector", true))
    , m_limit_axes(config->GetValue<std::array<bool, TSIM::Dimension::NQ>>("LimitAxes", {{false}}))
    , m_change_factor(config->GetValue<double>("ChangeFactor", 1000.0))
    , m_position_limiter(GetLimiter<control::StateSubsetToModify::kPosition>(m_protect, m_limit_axes))
    , m_velocity_limiter(GetLimiter<control::StateSubsetToModify::kVelocity>(m_protect, m_limit_axes))
    , m_clock_index_prepare(task.GetTimeMeasurement().RegisterClock("PredictiveControl:Preparation"))
    , m_clock_index_feedback(task.GetTimeMeasurement().RegisterClock("PredictiveControl:Feedback"))
    , m_clock_index_copy_data(task.GetTimeMeasurement().RegisterClock("PredictiveControl:CopyData"))
    , m_first_step(true)
    , m_use_external_initial_state_and_input(config->GetValue<bool>("UseExternalInitialStateAndInput", true))
{
    m_logger.Debug("Initializing controller for " + m_simulator.GetName() + ".");

    m_controller.SetLevenbergMarquardt(
        config->GetValue<double>("LevenbergMarquardt", m_controller.GetLevenbergMarquardt()));
    m_controller.SetTerminalStateErrorWeight(config->GetValue<typename TSIM::StateVector>(
        "TerminalStateErrorWeight", m_controller.GetTerminalStateErrorWeight()));

    m_controller.SetStateErrorWeight(m_neutral_state_error_weight);
    m_controller.SetInputErrorWeight(m_neutral_input_error_weight);
    m_controller.SetOutputErrorWeight(m_neutral_output_error_weight);

    m_controller.SetTighteningHorizon(config->GetValue<int>("TighteningHorizon", m_controller.GetTighteningHorizon()));

    m_controller.SetBoundsTightening(config->GetValue<double>("BoundsTightening", m_controller.GetBoundsTightening()));

    m_controller.SetTerminalStateBounds(
        config->GetValue<typename TSIM::StateVector>("StateLowerBound", m_controller.GetTerminalStateLowerBound()),
        config->GetValue<typename TSIM::StateVector>("StateUpperBound", m_controller.GetTerminalStateUpperBound()));

    typename TSIM::StateVector x_lb_default = m_controller.GetStateLowerBound();
    typename TSIM::StateVector x_ub_default = m_controller.GetStateUpperBound();
    typename TSIM::InputVector u_lb_default = m_controller.GetInputLowerBound();
    typename TSIM::InputVector u_ub_default = m_controller.GetInputUpperBound();

    typename TSIM::StateVector x_lb_config =
        config->GetValue<typename TSIM::StateVector>("StateLowerBound", m_controller.GetStateLowerBound());
    typename TSIM::StateVector x_ub_config =
        config->GetValue<typename TSIM::StateVector>("StateUpperBound", m_controller.GetStateUpperBound());

    typename TSIM::InputVector u_lb_config =
        config->GetValue<typename TSIM::InputVector>("InputLowerBound", m_controller.GetInputLowerBound());
    typename TSIM::InputVector u_ub_config =
        config->GetValue<typename TSIM::InputVector>("InputUpperBound", m_controller.GetInputUpperBound());

    if ((control::MakeStateView<TSIM>(x_lb_default).GetPosition().array() !=
         control::MakeStateView<TSIM>(x_lb_config).GetPosition().array())
            .any())
        m_logger.Warning("The default values for the lower bound on positions are equal to: [" +
                         mpmca::ToString(control::MakeStateView<TSIM>(x_lb_default).GetPosition().transpose()) +
                         "].\nThe configuration file changed these values to: [" +
                         mpmca::ToString(control::MakeStateView<TSIM>(x_lb_config).GetPosition().transpose()) + "].");

    if ((control::MakeStateView<TSIM>(x_lb_default).GetVelocity().array() !=
         control::MakeStateView<TSIM>(x_lb_config).GetVelocity().array())
            .any())
        m_logger.Warning("The default values for the lower bound on velocities are equal to: [" +
                         mpmca::ToString(control::MakeStateView<TSIM>(x_lb_default).GetVelocity().transpose()) +
                         "].\nThe configuration file changed these values to: [" +
                         mpmca::ToString(control::MakeStateView<TSIM>(x_lb_config).GetVelocity().transpose()) + "].");

    if ((u_lb_default.array() != u_lb_config.array()).any())
        m_logger.Warning("The default values for the lower bound on accelerations are equal to: [" +
                         mpmca::ToString(u_lb_default.transpose()) +
                         "].\nThe configuration file changed these values to: [" +
                         mpmca::ToString(u_lb_config.transpose()) + "].");

    if ((control::MakeStateView<TSIM>(x_lb_default).GetPosition().array() !=
         control::MakeStateView<TSIM>(x_lb_config).GetPosition().array())
            .any())
        m_logger.Warning("The default values for the upper bound on positions are equal to: [" +
                         mpmca::ToString(control::MakeStateView<TSIM>(x_ub_default).GetPosition().transpose()) +
                         "].\nThe configuration file changed these values to: [" +
                         mpmca::ToString(control::MakeStateView<TSIM>(x_ub_config).GetPosition().transpose()) + "].");

    if ((control::MakeStateView<TSIM>(x_ub_default).GetVelocity().array() !=
         control::MakeStateView<TSIM>(x_ub_config).GetVelocity().array())
            .any())
        m_logger.Warning("The default values for the upper bound on velocities are equal to: [" +
                         mpmca::ToString(control::MakeStateView<TSIM>(x_ub_default).GetVelocity().transpose()) +
                         "].\nThe configuration file changed these values to: [" +
                         mpmca::ToString(control::MakeStateView<TSIM>(x_ub_config).GetVelocity().transpose()) + "].");

    if ((u_ub_default.array() != u_ub_config.array()).any()) {
        m_logger.Warning("The default values for the upper bound on accelerations are equal to: [" +
                         mpmca::ToString(u_ub_default.transpose()) +
                         "].\nThe configuration file changed these values to: [" +
                         mpmca::ToString(u_ub_config.transpose()) + "].");
    }

    m_controller.SetStateBounds(x_lb_config, x_ub_config);
    m_controller.SetInputBounds(u_lb_config, u_ub_config);

    auto x_b_compare = (m_controller.GetStateLowerBound().array() >= m_controller.GetStateUpperBound().array());
    auto u_b_compare = (m_controller.GetInputLowerBound().array() >= m_controller.GetInputUpperBound().array());
    auto x_n_b_compare =
        (m_controller.GetTerminalStateLowerBound().array() >= m_controller.GetTerminalStateUpperBound().array());
    auto c_b_compare =
        (m_controller.GetConstraintLowerBound().array() >= m_controller.GetConstraintUpperBound().array());

    if (x_b_compare.any()) {
        throw std::invalid_argument(
            "At least one of the state lower bound values is greater than or equal to the corresponding "
            "upper bound. This will not work.\nLower bound : [" +
            mpmca::ToString(m_controller.GetStateLowerBound().transpose()) + "].\nUpper bound : [" +
            mpmca::ToString(m_controller.GetStateUpperBound().transpose()) + "].\nComparison: [" +
            mpmca::ToString(x_b_compare.matrix().template cast<double>().transpose()) + "].");
    }

    if (x_n_b_compare.any()) {
        throw std::invalid_argument(
            "At least one of the terminal state lower bound values is greater than or equal to the corresponding "
            "upper bound. This will not work.\nLower bound : [" +
            mpmca::ToString(m_controller.GetTerminalStateLowerBound().transpose()) + "].\nUpper bound : [" +
            mpmca::ToString(m_controller.GetTerminalStateUpperBound().transpose()) + "].\nComparison: [" +
            mpmca::ToString(x_n_b_compare.matrix().template cast<double>().transpose()) + "].");
    }

    if (u_b_compare.any())
        throw std::invalid_argument(
            "At least one of the input lower bound values is greater than or equal to the corresponding upper bound. "
            "This will not work.\nLower bound : [" +
            mpmca::ToString(m_controller.GetInputLowerBound().transpose()) + "].\nUpper bound : [" +
            mpmca::ToString(m_controller.GetInputUpperBound().transpose()) + "].\nComparison: [" +
            mpmca::ToString(u_b_compare.matrix().template cast<double>().transpose()) + "].");

    if (c_b_compare.any())
        throw std::invalid_argument(
            "At least one of the constraint lower bound values is greater than or equal to the corresponding upper "
            "bound. This will not work.\nLower bound : [" +
            mpmca::ToString(m_controller.GetConstraintLowerBound().transpose()) + "].\nUpper bound : [" +
            mpmca::ToString(m_controller.GetConstraintUpperBound().transpose()) + "].\nComparison: [" +
            mpmca::ToString(c_b_compare.matrix().template cast<double>().transpose()) + "].");
}

template <typename TSIM>
void PredictiveControl<TSIM>::PrepareTaskMessageBus(const Task& task, MessageBus& message_bus)
{
    message_bus.MakeMessage<messages::ControllerConstraintPlan<TSIM>>(task.GetHorizon());
    message_bus.MakeMessage<messages::ControllerOutputExpectedPlan<TSIM::Dimension::NY>>(
        task.GetHorizon(), m_simulator.GetNominalOutput());
    message_bus.MakeMessage<messages::ControllerPosePlan<TSIM>>(task.GetHorizon());
    message_bus.MakeMessage<messages::ControllerInertialPlan<TSIM>>(task.GetHorizon());
    message_bus.MakeMessage<messages::ControllerInputPlan<TSIM>>(task.GetHorizon(), m_simulator.GetNominalInput());
    message_bus.MakeMessage<messages::ControllerStatePlan<TSIM>>(task.GetHorizon(), m_simulator.GetNominalState());
    message_bus.MakeMessage<messages::ControllerBounds<TSIM>>();
}
template <typename TSIM>
void PredictiveControl<TSIM>::CheckTaskMessageBusPrepared(const Task& task, const MessageBus& message_bus)
{
    if (m_use_external_initial_state_and_input) {
        message_bus.RequireMessage<messages::ControllerInitialStateAndInput<TSIM>>();
    }
    message_bus.RequireMessage<messages::ControllerOutputReferencePlan<TSIM::Dimension::NY>>();
    message_bus.RequireMessage<messages::ControllerFinalTransform>();
}

template <typename TSIM>
void PredictiveControl<TSIM>::SetReferenceOutputPlan(const std::vector<typename TSIM::OutputVector>& y_ref)
{
    for (std::size_t i = 0; i < m_controller.GetIntegrationHorizon(); ++i)
        m_controller.SetReferenceOutput(i, y_ref.at(i));
}

template <typename TSIM>
void PredictiveControl<TSIM>::SetReferenceOutputPlan(const typename TSIM::OutputVector& y_ref)
{
    for (std::size_t i = 0; i < m_controller.GetIntegrationHorizon(); ++i)
        m_controller.SetReferenceOutput(i, y_ref);
}

template <typename TSIM>
void PredictiveControl<TSIM>::SetReferenceStatePlan(const std::vector<typename TSIM::StateVector>& x_ref)
{
    for (std::size_t i = 0; i < m_controller.GetIntegrationHorizon(); ++i)
        m_controller.SetReferenceState(i, x_ref.at(i));
}

template <typename TSIM>
void PredictiveControl<TSIM>::SetReferenceStatePlan(const typename TSIM::StateVector& x_ref)
{
    for (std::size_t i = 0; i < m_controller.GetIntegrationHorizon(); ++i)
        m_controller.SetReferenceState(i, x_ref);
}

template <typename TSIM>
void PredictiveControl<TSIM>::SetOutputErrorWeightPlan(const std::vector<typename TSIM::OutputVector>& w_y_plan)
{
    for (std::size_t i = 0; i < m_controller.GetIntegrationHorizon(); ++i)
        m_controller.SetOutputErrorWeight(i, w_y_plan.at(i));
}

template <typename TSIM>
void PredictiveControl<TSIM>::SetOutputErrorWeightPlan(const typename TSIM::OutputVector& w_y)
{
    for (std::size_t i = 0; i < m_controller.GetIntegrationHorizon(); ++i)
        m_controller.SetOutputErrorWeight(i, w_y);
}

template <typename TSIM>
void PredictiveControl<TSIM>::SetStateErrorWeightPlan(const std::vector<typename TSIM::StateVector>& w_x_plan)
{
    for (std::size_t i = 0; i < m_controller.GetIntegrationHorizon(); ++i)
        m_controller.SetStateErrorWeight(i, w_x_plan.at(i));
}

template <typename TSIM>
void PredictiveControl<TSIM>::SetStateErrorWeightPlan(const typename TSIM::StateVector& w_x)
{
    for (std::size_t i = 0; i < m_controller.GetIntegrationHorizon(); ++i)
        m_controller.SetStateErrorWeight(i, w_x);
}

template <typename TSIM>
void PredictiveControl<TSIM>::SetInputErrorWeightPlan(const std::vector<typename TSIM::InputVector>& w_u_plan)
{
    for (std::size_t i = 0; i < m_controller.GetIntegrationHorizon(); ++i)
        m_controller.SetInputErrorWeight(i, w_u_plan.at(i));
}

template <typename TSIM>
void PredictiveControl<TSIM>::SetInputErrorWeightPlan(const typename TSIM::InputVector& w_u)
{
    for (std::size_t i = 0; i < m_controller.GetIntegrationHorizon(); ++i)
        m_controller.SetInputErrorWeight(i, w_u);
}

template <typename TSIM>
void PredictiveControl<TSIM>::Prepare()
{
    GetSafety().Prepared();
}
template <typename TSIM>
void PredictiveControl<TSIM>::ReadMessages(MessageBus& message_bus)
{
    // handle required messages
    {
        using message_type = mpmca::messages::ControllerOutputReferencePlan<TSIM::Dimension::NY>;
        message_bus.ReadUpdatedMessage<message_type>(
            [&](const message_type& message) { SetReferenceOutputPlan(message.y_ref_plan); });
    }

    // handle optional messages
    message_bus.ReadMessageIfUpdated<mpmca::messages::ControllerFinalTransform>(
        [&](const mpmca::messages::ControllerFinalTransform& message) {
            m_controller.SetFinalTransform(message.final_transform);
        });

    {
        using message_type = mpmca::messages::ControllerStateErrorWeightPlan<TSIM>;
        if (!message_bus.ReadMessageIfUpdated<message_type>(
                [&](const message_type& message) { SetStateErrorWeightPlan(message.w_x_plan); })) {
            SetStateErrorWeightPlan(m_neutral_state_error_weight);
        }
    }

    {
        using message_type = mpmca::messages::ControllerStateReferencePlan<TSIM>;
        if (!message_bus.ReadMessageIfUpdated<message_type>(
                [&](const message_type& message) { SetReferenceStatePlan(message.x_ref_plan); })) {
            SetReferenceStatePlan(m_neutral_reference_state);
        }
    }

    {
        using message_type = mpmca::messages::ControllerOutputErrorWeightPlan<TSIM::Dimension::NY>;
        if (!message_bus.ReadMessageIfUpdated<message_type>(
                [&](const message_type& message) { SetOutputErrorWeightPlan(message.w_y_plan); })) {
            SetOutputErrorWeightPlan(m_neutral_output_error_weight);
        }
    }

    {
        using message_type = mpmca::messages::ControllerInputErrorWeightPlan<TSIM>;
        if (!message_bus.ReadMessageIfUpdated<message_type>(
                [&](const message_type& message) { SetInputErrorWeightPlan(message.w_u_plan); })) {
            SetInputErrorWeightPlan(m_neutral_input_error_weight);
        }
    }
}

template <typename TSIM>
void PredictiveControl<TSIM>::ProtectInitialState()
{
    if (!m_protect)
        return;

    typename TSIM::StateVector x_feasible = m_x0;
    typename TSIM::StateVector x_feasible_position = m_x0;

    m_position_limiter.MakeFeasible(x_feasible_position, m_controller.GetStateLowerBound(),
                                    m_controller.GetStateUpperBound(), m_controller.GetInputLowerBound(),
                                    m_controller.GetInputUpperBound());

    typename TSIM::StateVector x_feasible_velocity = m_x0;

    m_velocity_limiter.MakeFeasible(x_feasible_velocity, m_controller.GetStateLowerBound(),
                                    m_controller.GetStateUpperBound(), m_controller.GetInputLowerBound(),
                                    m_controller.GetInputUpperBound());

    if ((x_feasible_position - m_x0).norm() > 0.0)
        m_logger.Warning("Position:\nFrom:\t" + mpmca::ToString(m_x0.transpose()) + "\nTo:\t" +
                         mpmca::ToString(x_feasible_position.transpose()));

    if ((x_feasible_velocity - x_feasible_position).norm() > 0.0)
        m_logger.Warning("Velocity:\nFrom:\t" + mpmca::ToString(x_feasible_position.transpose()) + "\nTo:\t" +
                         mpmca::ToString(x_feasible_velocity.transpose()));

    auto x_feasible_view = control::MakeStateView<TSIM>(x_feasible);
    auto x_feasible_position_view = control::MakeStateView<TSIM>(x_feasible_position);
    auto x_feasible_velocity_view = control::MakeStateView<TSIM>(x_feasible_velocity);

    x_feasible_view.GetPosition() = x_feasible_position_view.GetPosition();
    x_feasible_view.GetVelocity() = x_feasible_velocity_view.GetVelocity();

    x_feasible = m_x0 + (x_feasible - m_x0) * m_change_factor;

    auto x_changed = (x_feasible.array() != m_x0.array());

    if (x_changed.any())
        m_logger.Warning("Changed:\n" + mpmca::ToString(x_changed.transpose().template cast<double>()));

    m_x0 = x_feasible;
}

template <typename TSIM>
void PredictiveControl<TSIM>::UpdateMessages(MessageBus& message_bus)
{
    message_bus.UpdateMessage<messages::ControllerStatePlan<TSIM>>([&](messages::ControllerStatePlan<TSIM>& message) {
        for (std::size_t i = 0; i < m_controller.GetMpcHorizonLength() + 1; ++i) {
            message.x_plan.at(i) = m_controller.GetStatePlan(i);
        }
    });

    message_bus.UpdateMessage<messages::ControllerPosePlan<TSIM>>([&](messages::ControllerPosePlan<TSIM>& message) {
        for (std::size_t i = 0; i < m_controller.GetMpcHorizonLength(); ++i) {
            message.pose_plan.at(i) = m_simulator.GetHeadPva(m_controller.GetStatePlan(i), m_controller.GetInputPlan(i),
                                                             m_controller.GetFinalTransform());
        }
    });

    message_bus.UpdateMessage<messages::ControllerInertialPlan<TSIM>>(
        [&](messages::ControllerInertialPlan<TSIM>& message) {
            for (std::size_t i = 0; i < m_controller.GetMpcHorizonLength(); ++i) {
                message.i_plan.at(i) = m_simulator.GetHeadInertial(
                    m_controller.GetStatePlan(i), m_controller.GetInputPlan(i), m_controller.GetFinalTransform());
            }
        });

    message_bus.UpdateMessage<messages::ControllerInputPlan<TSIM>>([&](messages::ControllerInputPlan<TSIM>& message) {
        for (std::size_t i = 0; i < m_controller.GetMpcHorizonLength(); ++i) {
            message.u_plan.at(i) = m_controller.GetInputPlan(i);
        }
    });

    message_bus.UpdateMessage<messages::ControllerOutputExpectedPlan<TSIM::Dimension::NY>>(
        [&](messages::ControllerOutputExpectedPlan<TSIM::Dimension::NY>& message) {
            for (std::size_t i = 0; i < m_controller.GetMpcHorizonLength(); ++i) {
                message.y_expected_plan.at(i) = m_simulator.GetOutput(
                    m_controller.GetStatePlan(i), m_controller.GetInputPlan(i), m_controller.GetFinalTransform());
            }
        });

    message_bus.UpdateMessage<messages::ControllerConstraintPlan<TSIM>>(
        [&](messages::ControllerConstraintPlan<TSIM>& message) {
            for (std::size_t i = 0; i < m_controller.GetMpcHorizonLength(); ++i) {
                message.c_plan.at(i) =
                    m_simulator.GetConstraint(m_controller.GetStatePlan(i), m_controller.GetInputPlan(i));
            }
        });

    if (m_first_step) {
        message_bus.UpdateMessage<messages::ControllerBounds<TSIM>>([&](messages::ControllerBounds<TSIM>& message) {
            message.u_lower_bound = m_controller.GetInputLowerBound();
            message.u_upper_bound = m_controller.GetInputUpperBound();

            message.x_lower_bound = m_controller.GetStateLowerBound();
            message.x_upper_bound = m_controller.GetStateUpperBound();

            message.c_lower_bound = m_controller.GetConstraintLowerBound();
            message.c_upper_bound = m_controller.GetConstraintUpperBound();
        });
        m_first_step = false;
    }
}

template <typename TSIM>
void PredictiveControl<TSIM>::UpdateMessagesWithNominalValues(MessageBus& message_bus)
{
    message_bus.UpdateMessage<messages::ControllerStatePlan<TSIM>>([&](messages::ControllerStatePlan<TSIM>& message) {
        for (std::size_t i = 0; i < m_controller.GetMpcHorizonLength() + 1; ++i)
            message.x_plan.at(i) = m_x0;
    });

    message_bus.UpdateMessage<messages::ControllerPosePlan<TSIM>>([&](messages::ControllerPosePlan<TSIM>& message) {
        for (std::size_t i = 0; i < m_controller.GetMpcHorizonLength(); ++i) {
            message.pose_plan.at(i) =
                m_simulator.GetHeadPva(m_x0, m_simulator.GetNominalInput(), m_controller.GetFinalTransform());
        }
    });

    message_bus.UpdateMessage<messages::ControllerInertialPlan<TSIM>>(
        [&](messages::ControllerInertialPlan<TSIM>& message) {
            for (std::size_t i = 0; i < m_controller.GetMpcHorizonLength(); ++i) {
                message.i_plan.at(i) =
                    m_simulator.GetHeadInertial(m_x0, m_simulator.GetNominalInput(), m_controller.GetFinalTransform());
            }
        });

    message_bus.UpdateMessage<messages::ControllerInputPlan<TSIM>>([&](messages::ControllerInputPlan<TSIM>& message) {
        for (std::size_t i = 0; i < m_controller.GetMpcHorizonLength(); ++i) {
            message.u_plan.at(i) = m_simulator.GetNominalInput();
        }
    });

    message_bus.UpdateMessage<messages::ControllerOutputExpectedPlan<TSIM::Dimension::NY>>(
        [&](messages::ControllerOutputExpectedPlan<TSIM::Dimension::NY>& message) {
            for (std::size_t i = 0; i < m_controller.GetMpcHorizonLength(); ++i) {
                message.y_expected_plan.at(i) =
                    m_simulator.GetOutput(m_x0, m_simulator.GetNominalInput(), m_controller.GetFinalTransform());
            }
        });

    message_bus.UpdateMessage<messages::ControllerConstraintPlan<TSIM>>(
        [&](messages::ControllerConstraintPlan<TSIM>& message) {
            for (std::size_t i = 0; i < m_controller.GetMpcHorizonLength(); ++i) {
                message.c_plan.at(i) = m_simulator.GetConstraint(m_x0, m_simulator.GetNominalInput());
            }
        });

    if (m_first_step) {
        message_bus.UpdateMessage<messages::ControllerBounds<TSIM>>([&](messages::ControllerBounds<TSIM>& message) {
            message.u_lower_bound = m_controller.GetInputLowerBound();
            message.u_upper_bound = m_controller.GetInputUpperBound();

            message.x_lower_bound = m_controller.GetStateLowerBound();
            message.x_upper_bound = m_controller.GetStateUpperBound();

            message.c_lower_bound = m_controller.GetConstraintLowerBound();
            message.c_upper_bound = m_controller.GetConstraintUpperBound();
        });
        m_first_step = false;
    }
}

template <typename TSIM>
void PredictiveControl<TSIM>::MainTick(DataBucket& data_bucket)
{
    TransitionStateMachine();

    if (m_use_external_initial_state_and_input) {
        data_bucket.GetTaskSignalBus().ReadUpdatedMessage<messages::ControllerInitialStateAndInput<TSIM>>(
            //
            [&](const messages::ControllerInitialStateAndInput<TSIM>& message) {
                // current/initial state is received from outside!
                m_x0 = message.x_initial;

                // only if we are in Preparing, set the working point to the initial state.
                if (GetStateMachineClient().GetCurrentState() == State::kPreparing) {
                    m_controller.SetWorkingPoint(message.x_initial, message.u_initial);
                }
            });
    }
    else {
        m_x0 = m_controller.GetStatePlan(1);
        // only if we are in Preparing, set the working point to the initial state.
        if (GetStateMachineClient().GetCurrentState() == State::kPreparing) {
            m_controller.SetWorkingPoint(m_neutral_reference_state, TSIM::InputVector::Zero());
        }
    }

    ReadMessages(data_bucket.GetTaskSignalBus());

    if (GetStateMachineClient().GetCurrentState() == State::kIdle ||
        GetStateMachineClient().GetCurrentState() == State::kBooting ||
        GetStateMachineClient().GetCurrentState() == State::kBooted) {
        data_bucket.GetTimeMeasurement().StopClock(m_clock_index_prepare);
        data_bucket.GetTimeMeasurement().StopClock(m_clock_index_feedback);
        UpdateMessagesWithNominalValues(data_bucket.GetTaskSignalBus());
        data_bucket.GetTimeMeasurement().StopClock(m_clock_index_copy_data);
        return;
    }
    else {
        try {
            m_controller.Preparation();
        }
        catch (const std::exception& ex) {
            throw(std::runtime_error("Exception in controller preparation step:\n" + std::string(ex.what())));
        }

        data_bucket.GetTimeMeasurement().StopClock(m_clock_index_prepare);

        try {
            m_controller.Feedback(m_x0);
        }
        catch (const std::exception& ex) {
            throw std::runtime_error("Exception in controller feedback step:\n" + std::string(ex.what()));
        }

        data_bucket.GetTimeMeasurement().StopClock(m_clock_index_feedback);

        UpdateMessages(data_bucket.GetTaskSignalBus());

        data_bucket.GetTimeMeasurement().StopClock(m_clock_index_copy_data);
    }
}

}  //namespace mpmca::pipeline::step