/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/pipeline/step.hpp"
#include "mpmca/pipeline/step_registrar.hpp"
#include "mpmca/utilities/sine_wave_generator.hpp"

namespace mpmca::pipeline::step {

class SyntheticInertialSignalGenerator : public Step
{
  public:
    SyntheticInertialSignalGenerator(Task& task, const std::string& name, utilities::ConfigurationPtr config);
    ~SyntheticInertialSignalGenerator() = default;
    void PrepareTaskMessageBus(const Task& task, MessageBus&) override;
    void CheckTaskMessageBusPrepared(const Task& task, const MessageBus&) override;
    void MainTick(DataBucket& data_bucket) override;
    void Prepare() override { GetSafety().Prepared(); };

  protected:
    utilities::SineWaveGenerator m_f_x_sine_generator;
    utilities::SineWaveGenerator m_f_y_sine_generator;
    utilities::SineWaveGenerator m_f_z_sine_generator;
    utilities::SineWaveGenerator m_omega_x_sine_generator;
    utilities::SineWaveGenerator m_omega_y_sine_generator;
    utilities::SineWaveGenerator m_omega_z_sine_generator;
    utilities::SineWaveGenerator m_alpha_x_sine_generator;
    utilities::SineWaveGenerator m_alpha_y_sine_generator;
    utilities::SineWaveGenerator m_alpha_z_sine_generator;
};

static StepRegistrar<SyntheticInertialSignalGenerator> SyntheticInertialSignalGeneratorRegistrar(
    "SyntheticInertialSignalGenerator");

}  //namespace mpmca::pipeline::step