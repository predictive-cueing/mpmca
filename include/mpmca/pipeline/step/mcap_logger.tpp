/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <filesystem>
#include <map>

#include "mpmca/messages/message_id_macro_definition.hpp"
#include "mpmca/pipeline/step/mcap_logger.hpp"
#include "mpmca/utilities/mpmca_mcap.hpp"
#include "mpmca/visualization/automotive_prediction_horizon_convertor.hpp"
#include "mpmca/visualization/automotive_prediction_path_convertor.hpp"
#include "mpmca/visualization/classic_washout_signals_convertor.hpp"
#include "mpmca/visualization/controller_data_convertor.hpp"
#include "mpmca/visualization/controller_data_convertor.tpp"
#include "mpmca/visualization/foxglove_message_convertor.hpp"
#include "mpmca/visualization/mcap_test_message_convertor.hpp"
#include "mpmca/visualization/simulation_signals_convertor.hpp"

namespace mpmca::pipeline::step {

template <class TSIM>
McapLogger<TSIM>::McapLogger(Task& task, const std::string& name, utilities::ConfigurationPtr config)
    : Step(task, name, "McapLogger", config)
    , m_filename(config->GetValue<std::string>("Filename", "test.mcap"))
{
    auto options = mcap::McapWriterOptions("");
    const auto res = m_writer.open(m_filename, options);
    if (!res.ok()) {
        throw std::runtime_error("Failed to open " + m_filename + " for writing: " + res.message);
    }

    m_message_convertors.push_back(std::make_unique<visualization::AutomotivePredictionPathConvertor>(m_writer));
    m_message_convertors.push_back(std::make_unique<visualization::AutomotivePredictionHorizonConvertor>(m_writer));
    m_message_convertors.push_back(std::make_unique<visualization::SimulationSignalsConvertor>(m_writer));
    m_message_convertors.push_back(std::make_unique<visualization::ClassicWashoutSignalsConvertor>(m_writer));

    if (config->GetKeyExists("ControllerDataConvertor")) {
        m_message_convertors.push_back(std::make_unique<visualization::ControllerDataConvertor<TSIM>>(
            config->GetConfigurationObject("ControllerDataConvertor"), m_writer));
    }
    else {
        m_logger.Warning("You do not have a ControllerDataConvertor configuration object!");
        m_message_convertors.push_back(std::make_unique<visualization::ControllerDataConvertor<TSIM>>(m_writer));
    }
}

template <class TSIM>
McapLogger<TSIM>::~McapLogger()
{
    m_writer.close();
    std::filesystem::path cwd = std::filesystem::current_path();
    m_logger.Info("Wrote MCAP data to {" + cwd.string() + "/" + m_filename + "}");
}

template <class TSIM>
void McapLogger<TSIM>::PrepareTaskMessageBus(const Task& task, MessageBus& message_bus)
{
}

template <class TSIM>
void McapLogger<TSIM>::CheckTaskMessageBusPrepared(const Task& task, const MessageBus& message_bus)
{
}

template <class TSIM>
void McapLogger<TSIM>::MainTick(DataBucket& data_bucket)
{
    TransitionStateMachine();

    for (auto& message_convertor : m_message_convertors) {
        message_convertor->ProcessDataBucket(m_writer, data_bucket);
    }
}

template <class TSIM>
void McapLogger<TSIM>::Prepare()
{
    GetSafety().Prepared();
}

}  //namespace mpmca::pipeline::step