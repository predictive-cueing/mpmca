/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/messages/controller_output_reference_plan.hpp"
#include "mpmca/pipeline/step.hpp"
#include "mpmca/pipeline/step_registrar.hpp"
#include "mpmca/pipeline/task.hpp"

namespace mpmca::pipeline::step {
class ScaleInertialReference : public Step
{
  private:
    double K_f_x;
    double K_f_y;
    double K_f_z;
    double K_omega_x;
    double K_omega_y;
    double K_omega_z;
    double K_alpha_x;
    double K_alpha_y;
    double K_alpha_z;

    void MainTick(DataBucket& data_bucket);
    void Prepare();
    void ScaleReferenceOutputPlan(messages::ControllerOutputReferencePlan<9>& message) const;

  public:
    ScaleInertialReference(Task& task, const std::string& name, utilities::ConfigurationPtr config);

    void PrepareTaskMessageBus(const Task& task, MessageBus& message_bus) override;
    void CheckTaskMessageBusPrepared(const Task& task, const MessageBus& message_bus) override;
};

static StepRegistrar<ScaleInertialReference> ScaleInertialReferenceRegistrar("ScaleInertialReference");
}  //namespace mpmca::pipeline::step