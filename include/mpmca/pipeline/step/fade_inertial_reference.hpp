/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/messages/controller_output_reference_plan.hpp"
#include "mpmca/pipeline/step.hpp"

namespace mpmca::pipeline::step {

template <typename TSIM>
class FadeInertialReference : public Step
{
  private:
    double m_t_fade_in;
    double m_t_fade_out;
    double m_fade_out_position_weight;
    double m_fade_out_velocity_weight;

    void MainTick(DataBucket& data_bucket) override;
    void Prepare() override { GetSafety().Prepared(); };

    void PrepareTaskMessageBus(const Task& task, MessageBus& message_bus) override;
    void CheckTaskMessageBusPrepared(const Task& task, const MessageBus& message_bus) override;
    void FadeOut(DataBucket& data_bucket);
    double GetFadeValue(const DataBucket& data_bucket, double horizon_time_s);
    void ScaleReferenceOutputPlan(messages::ControllerOutputReferencePlan<9>& message, const DataBucket& data_bucket);

    static std::string ToScientificNotationString(double val);

  public:
    FadeInertialReference(Task& task, const std::string& name, utilities::ConfigurationPtr config);
};
}  //namespace mpmca::pipeline::step
