/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/control/simulator.hpp"
#include "mpmca/control/state.hpp"
#include "mpmca/dokblocks/classic_washout_algorithm/classic_washout_algorithm_model.hpp"
#include "mpmca/json_tools.hpp"
#include "mpmca/linear_algebra.hpp"
#include "mpmca/messages/classic_washout_signals.hpp"
#include "mpmca/messages/controller_final_transform.hpp"
#include "mpmca/messages/controller_output_error_weight_plan.hpp"
#include "mpmca/messages/controller_output_reference_plan.hpp"
#include "mpmca/messages/inertial_head_signals.hpp"
#include "mpmca/messages/vehicle_state_signals.hpp"
#include "mpmca/pipeline/step.hpp"
#include "mpmca/pipeline/step_registrar.hpp"
#include "mpmca/pipeline/task.hpp"
#include "mpmca/types/inertial_vector.hpp"
#include "mpmca/types/pose_rot_mat_vector.hpp"
#include "mpmca/types/pose_vector.hpp"
#include "mpmca/utilities/math.hpp"
namespace mpmca::pipeline::step {

template <typename TSIM>
class ClassicWashoutReference : public Step
{
    static_assert(TSIM::NY_SIMULATOR == 24, "Step ClassicWashoutReference requires a Simulator with OutputVector<24>");
    dokblocks::classic_washout_algorithm::Params m_params;
    decltype(dokblocks::classic_washout_algorithm::model::GetBlock(
        dokblocks::classic_washout_algorithm::Params())) m_classic_washout_model;

    mpmca::control::Simulator<TSIM> m_simulator;
    PoseRotMatVector m_nominal_output;
    TransformationMatrix m_final_transform;
    typename TSIM::OutputVector m_base_output_error_weight;
    std::vector<double> m_output_error_weight_horizon_multiplier;

    uint32_t m_num_samples_per_tick;

  public:
    ClassicWashoutReference(Task& task, const std::string& name, utilities::ConfigurationPtr config)
        : Step(task, name, "ClassicWashoutReference", config)
        , m_params(config->GetConfigurationObject("Parameters"))
        , m_classic_washout_model(dokblocks::classic_washout_algorithm::model::GetBlock(m_params))
        , m_nominal_output(m_simulator.GetNominalOutput())
        , m_final_transform(TransformationMatrix::Identity())
        , m_base_output_error_weight(
              config->GetValue<typename TSIM::OutputVector>("BaseOutputErrorWeight", TSIM::OutputVector::Zero()))
        , m_output_error_weight_horizon_multiplier(
              config->GetValue<std::vector<double>>("OutputErrorWeightHorizonMultiplier", {1, 0}))
        , m_num_samples_per_tick(config->GetValue<uint32_t>("NumberOfSamplesPerTick", 1))
    {
        m_classic_washout_model.SetSimulationStep(task.GetClock().GetDt() / (double)m_num_samples_per_tick);
        m_logger.Info("Nominal output of this simulator is " + mpmca::ToString(m_nominal_output.transpose()));
    }

  private:
    void MainTick(DataBucket& data_bucket) override
    {
        TransitionStateMachine();

        data_bucket.GetTaskSignalBus().ReadMessageIfUpdated<messages::InertialHeadSignals>(
            [&](const messages::InertialHeadSignals& message) {
                m_classic_washout_model.SetInput("f_x", message.head_f_x_head_m_s2);
                m_classic_washout_model.SetInput("f_y", message.head_f_y_head_m_s2);
                m_classic_washout_model.SetInput("f_z", message.head_f_z_head_m_s2);
                m_classic_washout_model.SetInput("omega_x", message.head_omega_x_head_rad_s);
                m_classic_washout_model.SetInput("omega_y", message.head_omega_y_head_rad_s);
                m_classic_washout_model.SetInput("omega_z", message.head_omega_z_head_rad_s);
            });

        data_bucket.GetTaskSignalBus().ReadMessageIfUpdated<messages::VehicleStateSignals>(
            [&](const messages::VehicleStateSignals& message) {
                m_classic_washout_model.SetInput("phi_vehicle", message.world_phi_vehicle_rad);
                m_classic_washout_model.SetInput("theta_vehicle", message.world_theta_vehicle_rad);
                m_classic_washout_model.SetInput("psi_vehicle", message.world_psi_vehicle_rad);
            });

        data_bucket.GetTaskSignalBus().ReadMessageIfUpdated<messages::ControllerFinalTransform>(
            [&](const messages::ControllerFinalTransform& message) { m_final_transform = message.final_transform; });

        for (int sample = 0; sample < m_num_samples_per_tick; ++sample) {
            m_classic_washout_model.SetInput("phi_sim", m_classic_washout_model.GetOutput("phi_sim"));
            m_classic_washout_model.SetInput("theta_sim", m_classic_washout_model.GetOutput("theta_sim"));
            m_classic_washout_model.SetInput("psi_sim", m_classic_washout_model.GetOutput("psi_sim"));

            m_classic_washout_model.Simulate();
        }

        PoseVector pose_0 = PoseVector::Zero();
        pose_0.GetX() = m_classic_washout_model.GetOutput("x_sim") + m_nominal_output.GetX() + m_final_transform.GetX();
        pose_0.GetY() = m_classic_washout_model.GetOutput("y_sim") + m_nominal_output.GetY() + m_final_transform.GetY();
        pose_0.GetZ() = m_classic_washout_model.GetOutput("z_sim") + m_nominal_output.GetZ() + m_final_transform.GetZ();
        pose_0.GetPhi() = m_classic_washout_model.GetOutput("phi_sim");
        pose_0.GetTheta() = m_classic_washout_model.GetOutput("theta_sim");
        pose_0.GetPsi() = m_classic_washout_model.GetOutput("psi_sim");

        pose_0.GetXd() = m_classic_washout_model.GetOutput("xd_sim");
        pose_0.GetYd() = m_classic_washout_model.GetOutput("yd_sim");
        pose_0.GetZd() = m_classic_washout_model.GetOutput("zd_sim");
        pose_0.GetP() = m_classic_washout_model.GetOutput("p_sim");
        pose_0.GetQ() = m_classic_washout_model.GetOutput("q_sim");
        pose_0.GetR() = m_classic_washout_model.GetOutput("r_sim");

        pose_0.GetXdd() = m_classic_washout_model.GetOutput("a_x_sim");
        pose_0.GetYdd() = m_classic_washout_model.GetOutput("a_y_sim");
        pose_0.GetZdd() = m_classic_washout_model.GetOutput("a_z_sim");
        pose_0.GetPd() = m_classic_washout_model.GetOutput("pd_sim");
        pose_0.GetQd() = m_classic_washout_model.GetOutput("qd_sim");
        pose_0.GetRd() = m_classic_washout_model.GetOutput("rd_sim");

        data_bucket.GetTaskSignalBus().UpdateMessage<messages::ControllerOutputErrorWeightPlan<24>>(
            [&](messages::ControllerOutputErrorWeightPlan<24>& message) {
                for (int i = 0; i < message.w_y_plan.size(); ++i) {
                    double multiplier;

                    if (i < m_output_error_weight_horizon_multiplier.size()) {
                        multiplier = m_output_error_weight_horizon_multiplier[i];
                    }
                    else {
                        multiplier =
                            m_output_error_weight_horizon_multiplier[m_output_error_weight_horizon_multiplier.size() -
                                                                     1];
                    }
                    message.w_y_plan.at(i) = m_base_output_error_weight * multiplier;
                }
            });

        data_bucket.GetTaskSignalBus().UpdateMessage<messages::ClassicWashoutSignals>(
            [&](messages::ClassicWashoutSignals& message) {
                message.head_inertial_cwa.GetFx() = m_classic_washout_model.GetOutput("f_x_cwa");
                message.head_inertial_cwa.GetFy() = m_classic_washout_model.GetOutput("f_y_cwa");
                message.head_inertial_cwa.GetFz() = m_classic_washout_model.GetOutput("f_z_cwa");
                message.head_inertial_cwa.GetOmegaX() = m_classic_washout_model.GetOutput("omega_x_cwa");
                message.head_inertial_cwa.GetOmegaY() = m_classic_washout_model.GetOutput("omega_y_cwa");
                message.head_inertial_cwa.GetOmegaZ() = m_classic_washout_model.GetOutput("omega_z_cwa");
                message.head_inertial_cwa.GetAlphaX() = m_classic_washout_model.GetOutput("alpha_x_cwa");
                message.head_inertial_cwa.GetAlphaY() = m_classic_washout_model.GetOutput("alpha_y_cwa");
                message.head_inertial_cwa.GetAlphaZ() = m_classic_washout_model.GetOutput("alpha_z_cwa");

                message.simulator_state_reference_cwa = pose_0;
                Vector<3> euler_rates = pose_0.EulerRatesToAngularVelocities();
                message.simulator_state_reference_cwa.GetP() = euler_rates[0];
                message.simulator_state_reference_cwa.GetQ() = euler_rates[1];
                message.simulator_state_reference_cwa.GetR() = euler_rates[2];
                Vector<3> euler_accelerations = pose_0.EulerAccelerationsToAngularAccelerations();
                message.simulator_state_reference_cwa.GetPd() = euler_accelerations[0];
                message.simulator_state_reference_cwa.GetQd() = euler_accelerations[1];
                message.simulator_state_reference_cwa.GetRd() = euler_accelerations[2];
            });

        data_bucket.GetTaskSignalBus().UpdateMessage<messages::ControllerOutputReferencePlan<24>>(
            [&](messages::ControllerOutputReferencePlan<24>& message) {
                for (std::size_t i = 0; i < message.y_ref_plan.size(); ++i) {
                    double t = data_bucket.GetHorizon().GetHorizonTime(i);
                    PoseVector pose_i = pose_0.Extrapolate(t);
                    PoseRotMatVector y_ref_i = pose_i.ToPoseRotMatVector();

                    Vector<3> euler_rates = pose_i.EulerRatesToAngularVelocities();
                    y_ref_i.GetP() = euler_rates[0];
                    y_ref_i.GetQ() = euler_rates[1];
                    y_ref_i.GetR() = euler_rates[2];
                    Vector<3> euler_accelerations = pose_i.EulerAccelerationsToAngularAccelerations();
                    y_ref_i.GetPd() = euler_accelerations[0];
                    y_ref_i.GetQd() = euler_accelerations[1];
                    y_ref_i.GetRd() = euler_accelerations[2];

                    message.y_ref_plan.at(i) = y_ref_i;
                }
            });
    }
    void Prepare() override { GetSafety().Prepared(); };

    void PrepareTaskMessageBus(const Task& task, MessageBus& message_bus) override
    {
        message_bus.MakeMessage<messages::ControllerOutputReferencePlan<24>>(task.GetHorizon());
        message_bus.MakeMessage<messages::ControllerOutputErrorWeightPlan<24>>(task.GetHorizon());
        message_bus.MakeMessage<messages::ClassicWashoutSignals>();
    }

    void CheckTaskMessageBusPrepared(const Task& task, const MessageBus& message_bus) override
    {
        message_bus.RequireMessage<messages::InertialHeadSignals>();
        message_bus.RequireMessage<messages::VehicleStateSignals>();
        message_bus.RequireMessage<messages::ControllerFinalTransform>();
    }
};

}  //namespace mpmca::pipeline::step