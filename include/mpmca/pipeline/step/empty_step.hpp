/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/pipeline/step.hpp"
#include "mpmca/pipeline/step_registrar.hpp"

namespace mpmca::pipeline::step {

class EmptyStep : public Step
{
  public:
    EmptyStep(Task& task, const std::string& name, utilities::ConfigurationPtr config);
    ~EmptyStep();
    void PrepareTaskMessageBus(const Task& task, MessageBus&) override;
    void CheckTaskMessageBusPrepared(const Task& task, const MessageBus&) override;
    void MainTick(DataBucket& data_bucket) override;
    void Prepare() override;
};

static StepRegistrar<EmptyStep> EmptyStepRegistrar("EmptyStep");

}  //namespace mpmca::pipeline::step