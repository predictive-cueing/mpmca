/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <memory>
#include <vector>

#include "mpmca/pipeline/cereal_reader.hpp"
#include "mpmca/pipeline/step.hpp"
#include "mpmca/pipeline/step_registrar.hpp"
#include "mpmca/utilities/horizon.hpp"

namespace mpmca::pipeline::step {

class BinaryInertialOraclePlayback : public Step
{
  public:
    BinaryInertialOraclePlayback(Task& task, const std::string& name, utilities::ConfigurationPtr config);
    ~BinaryInertialOraclePlayback() = default;
    void PrepareTaskMessageBus(const Task& task, MessageBus&) override;
    void CheckTaskMessageBusPrepared(const Task& task, const MessageBus&) override;
    void MainTick(DataBucket& data_bucket) override;
    void Prepare() override;

  protected:
    int64_t CheckTimestampRequirementsAndReturnFinalSample(const utilities::Horizon& horizon) const;
    void UpdateMessageBus(const utilities::Horizon& horizon, MessageBus& message_bus, int64_t current_time);
    bool WillNextMainTickFail(const DataBucket& data_bucket) const;

    CerealReader m_binary_reader;
    int64_t m_final_sample_ms;
    int64_t m_last_time_step_in_run;
};

static StepRegistrar<BinaryInertialOraclePlayback> BinaryInertialHorizonPlaybackRegistrar(
    "BinaryInertialOraclePlayback");

}  //namespace mpmca::pipeline::step