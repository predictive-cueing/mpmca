/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/messages/automotive_prediction_parameters.hpp"
#include "mpmca/pipeline/step.hpp"
#include "mpmca/pipeline/step_registrar.hpp"
#include "mpmca/utilities/file_container.hpp"
#include "mpmca/utilities/map_1d.hpp"
#include "mpmca/utilities/map_2d.hpp"

namespace mpmca {
namespace predict::inertial {
class Path;
}

namespace pipeline::step {
class JsonPlayback : public Step
{
  public:
    JsonPlayback(Task& task, const std::string& name, utilities::ConfigurationPtr config);

  private:
    void MainTick(DataBucket& data_bucket) override;
    void Prepare() override { GetSafety().Prepared(); };
    void PrepareTaskMessageBus(const Task& task, MessageBus& message_bus) override;
    void CheckTaskMessageBusPrepared(const Task& task, const MessageBus& message_bus) override;

    bool m_read_path;
    bool m_update_automotive_prediction_parameters;
    bool m_update_inertial_head_signals;
    bool m_update_vehicle_state_signals;
    bool m_update_automotive_signals;
    bool m_update_controller_output_reference_plan_9;
    bool m_oracle_prediction;

    utilities::FileContainer m_file_container;
    std::shared_ptr<predict::inertial::Path> m_path_ptr;

    uint64_t m_start_time_ms;
    uint64_t m_stop_time_ms;

    bool m_go_to_idle;
    bool m_go_to_terminate;

    messages::AutomotivePredictionParameters m_automotive_prediction_parameters;
};

static StepRegistrar<JsonPlayback> JsonPlaybackRegistrar("JsonPlayback");
}  //namespace pipeline::step
}  //namespace mpmca