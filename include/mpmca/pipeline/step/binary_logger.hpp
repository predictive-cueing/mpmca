/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <memory>
#include <vector>

#include "mpmca/pipeline/cereal_bus_base_message_logger.hpp"
#include "mpmca/pipeline/cereal_logger.hpp"
#include "mpmca/pipeline/step.hpp"
#include "mpmca/pipeline/step_registrar.hpp"

namespace mpmca::pipeline::step {

class BinaryLogger : public Step
{
  public:
    BinaryLogger(Task& task, const std::string& name, utilities::ConfigurationPtr config);
    ~BinaryLogger() = default;
    void PrepareTaskMessageBus(const Task& task, MessageBus&) override;
    void CheckTaskMessageBusPrepared(const Task& task, const MessageBus&) override;
    void MainTick(DataBucket& data_bucket) override;
    void Prepare() override;

  protected:
    CerealBusBaseMessageLogger m_binary_logger;
};

static StepRegistrar<BinaryLogger> BinaryLoggerRegistrar("BinaryLogger");

}  //namespace mpmca::pipeline::step