/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/control/controller.hpp"
#include "mpmca/control/controller_options.hpp"
#include "mpmca/control/limiter.hpp"
#include "mpmca/control/simulator.hpp"
#include "mpmca/json_tools.hpp"
#include "mpmca/pipeline/step.hpp"
#include "mpmca/pipeline/step_registrar.hpp"
#include "mpmca/pipeline/task.hpp"
#include "mpmca/utilities/clock_index.hpp"
#include "mpmca/utilities/configuration.hpp"

namespace mpmca::pipeline::step {
template <typename TSIM>
class PredictiveControl : public Step
{
  public:
    PredictiveControl(Task& task, const std::string& name, utilities::ConfigurationPtr config);
    void PrepareTaskMessageBus(const Task& task, MessageBus& message_bus) override;
    void CheckTaskMessageBusPrepared(const Task& task, const MessageBus& message_bus) override;

    void Prepare() override;
    void MainTick(DataBucket& data_bucket) override;

    void ProtectInitialState();

    void ReadMessages(MessageBus& message_bus);
    void UpdateMessages(MessageBus& message_bus);
    void UpdateMessagesWithNominalValues(MessageBus& message_bus);

    void SetReferenceOutputPlan(const std::vector<typename TSIM::OutputVector>& y_ref_plan);
    void SetReferenceOutputPlan(const typename TSIM::OutputVector& y_ref);

    void SetReferenceStatePlan(const std::vector<typename TSIM::StateVector>& x_ref_plan);
    void SetReferenceStatePlan(const typename TSIM::StateVector& x_ref);

    void SetStateErrorWeightPlan(const std::vector<typename TSIM::StateVector>& w_x_plan);
    void SetStateErrorWeightPlan(const typename TSIM::StateVector& w_x);

    void SetOutputErrorWeightPlan(const std::vector<typename TSIM::OutputVector>& w_y_plan);
    void SetOutputErrorWeightPlan(const typename TSIM::OutputVector& w_y);

    void SetInputErrorWeightPlan(const std::vector<typename TSIM::InputVector>& w_u_plan);
    void SetInputErrorWeightPlan(const typename TSIM::InputVector& w_u);

  private:
    template <mpmca::control::StateSubsetToModify S>
    mpmca::control::Limiter<TSIM, S> GetLimiter(bool protect, std::array<bool, TSIM::Dimension::NQ> limit_axes);

    mpmca::control::ControllerOptions<TSIM> m_controller_options;
    mpmca::control::Simulator<TSIM> m_simulator;
    mpmca::control::Controller<TSIM> m_controller;

    typename TSIM::StateVector m_x0;
    typename TSIM::OutputVector m_neutral_reference_output;
    typename TSIM::StateVector m_neutral_reference_state;
    typename TSIM::InputVector m_neutral_reference_input;

    typename TSIM::OutputVector m_neutral_output_error_weight;
    typename TSIM::InputVector m_neutral_input_error_weight;
    typename TSIM::StateVector m_neutral_state_error_weight;

    bool m_protect;
    std::array<bool, TSIM::Dimension::NQ> m_limit_axes;
    double m_change_factor;
    mpmca::control::Limiter<TSIM, mpmca::control::StateSubsetToModify::kPosition> m_position_limiter;
    mpmca::control::Limiter<TSIM, mpmca::control::StateSubsetToModify::kVelocity> m_velocity_limiter;

    utilities::ClockIndex m_clock_index_prepare;
    utilities::ClockIndex m_clock_index_feedback;
    utilities::ClockIndex m_clock_index_copy_data;

    bool m_first_step;
    bool m_use_external_initial_state_and_input;
};

template <typename TSIM>
template <mpmca::control::StateSubsetToModify S>
mpmca::control::Limiter<TSIM, S> PredictiveControl<TSIM>::GetLimiter(bool protect,
                                                                     std::array<bool, TSIM::Dimension::NQ> limit_axes)
{
    if (protect)
        return mpmca::control::Limiter<TSIM, S>(limit_axes);
    else
        return mpmca::control::Limiter<TSIM, S>(std::array<bool, TSIM::Dimension::NQ>{{false}});
}

}  //namespace mpmca::pipeline::step