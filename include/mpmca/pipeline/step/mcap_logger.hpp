/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <memory>
#include <vector>

#include "mpmca/pipeline/step.hpp"
#include "mpmca/pipeline/step_registrar.hpp"
#include "mpmca/utilities/mpmca_mcap.hpp"
#include "mpmca/visualization/foxglove_message_convertor.hpp"

namespace mpmca::pipeline::step {

template <typename TSIM>
class McapLogger : public Step
{
  public:
    McapLogger(Task& task, const std::string& name, utilities::ConfigurationPtr config);
    ~McapLogger();
    void PrepareTaskMessageBus(const Task& task, MessageBus&) override;
    void CheckTaskMessageBusPrepared(const Task& task, const MessageBus&) override;
    void MainTick(DataBucket& data_bucket) override;
    void Prepare() override;

  private:
    std::string m_filename;
    mcap::McapWriter m_writer;

    std::vector<std::unique_ptr<visualization::FoxgloveMessageConvertor>> m_message_convertors;
};

}  //namespace mpmca::pipeline::step
