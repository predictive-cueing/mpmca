/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/pipeline/step.hpp"
#include "mpmca/pipeline/step_registrar.hpp"

namespace mpmca::pipeline::step {

template <typename T>
class ReferencePredictor : public Step
{
  public:
    ReferencePredictor(Task& task, const std::string& name, utilities::ConfigurationPtr config);

    void Prepare() override;
    void MainTick(DataBucket& data_bucket) override;
    void PrepareTaskMessageBus(const Task& task, MessageBus& message_bus) override;
    void CheckTaskMessageBusPrepared(const Task& task, const MessageBus& message_bus) override;

  private:
    utilities::ClockIndex m_clock_index_init_prediction;
    utilities::ClockIndex m_clock_index_simulation;
    utilities::ClockIndex m_clock_index_fill_bus;

    T m_predictor;
    bool m_prepare_tick_called;
};

}  //namespace mpmca::pipeline::step