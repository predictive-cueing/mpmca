/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/pipeline/step.hpp"
#include "mpmca/pipeline/step_registrar.hpp"
#include "mpmca/utilities/clock_index.hpp"
#include "mpmca/utilities/time_measurement.hpp"

namespace mpmca::pipeline::step {
class TimingPrinter : public Step
{
  public:
    TimingPrinter(Task& task, const std::string& name, utilities::ConfigurationPtr config);

  private:
    void Prepare() override;
    void MainTick(DataBucket& data_bucket) override;
    void PrepareTaskMessageBus(const Task& task, MessageBus&) override;
    void CheckTaskMessageBusPrepared(const Task& task, const MessageBus&) override;

    utilities::TimeMeasurement m_time_measurement;
    utilities::ClockIndex m_test_clock_index;
    int m_counter;
};

static StepRegistrar<TimingPrinter> TimingPrinterStepRegistrar("TimingPrinter");

}  //namespace mpmca::pipeline::step