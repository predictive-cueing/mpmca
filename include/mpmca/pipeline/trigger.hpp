/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <signal.h>

#include <atomic>
#include <chrono>
#include <string>
#include <thread>

#include "mpmca/pipeline/pipeline.hpp"
#include "mpmca/utilities/logger.hpp"

namespace mpmca::pipeline {
static std::atomic<bool> s_terminate_clean = false;

inline void GotSignalStd(int)
{
    s_terminate_clean.store(true);
}

class Trigger
{
  public:
    Trigger(const std::shared_ptr<Pipeline> &pipeline_ptr, int64_t max_iterations);
    ~Trigger();
    int ThreadedRun();
    int FreeRun();

  private:
    void InOutputLoop();
    void TaskLoop(size_t task_index);

    void DoLogicBlocking();
    void SetTerminate(bool terminate);
    void DoLogicNonblock();

    bool GetTerminateFromPipeline() const;

    Trigger(const Trigger &) = delete;
    Trigger &operator=(const Trigger &) = delete;

    utilities::Logger m_logger;
    std::shared_ptr<Pipeline> m_pipeline_ptr;

    const std::chrono::milliseconds m_base_step_ms;
    const std::chrono::time_point<std::chrono::steady_clock> m_start_time;

    std::atomic<bool> m_finished;
    std::deque<std::atomic<bool>> m_task_thread_running;
    std::atomic<bool> m_in_output_thread_running;
    std::deque<std::atomic<int>> m_start_task_tick;
    int64_t m_max_iterations;
};

}  //namespace mpmca