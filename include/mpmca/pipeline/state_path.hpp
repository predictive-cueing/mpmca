/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <map>
#include <vector>

#include "mpmca/pipeline/state.hpp"

namespace mpmca::pipeline {
class StatePath
{
  public:
    enum class Type
    {
        kRegular,
        kFailure
    };

    StatePath(State start, State transition, State transition_done, State next, Type type);
    const State& GetStartState() const;
    const State& GetTransitionState() const;
    const State& GetTransitionDoneState() const;
    const State& GetNextState() const;
    Type GetType() const;

  private:
    State m_start;
    State m_transition;
    State m_transition_done;
    State m_next;
    Type m_type;
};
}  //namespace mpmca::pipeline