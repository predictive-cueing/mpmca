/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <atomic>
#include <deque>
#include <mutex>

#include "mpmca/pipeline/message_bus.hpp"
#include "mpmca/pipeline/safety.hpp"
#include "mpmca/pipeline/state_machine.hpp"
#include "mpmca/utilities/clock.hpp"
#include "mpmca/utilities/configuration.hpp"
#include "mpmca/utilities/controllable_clock.hpp"
#include "mpmca/utilities/horizon.hpp"
#include "mpmca/utilities/logger.hpp"
#include "mpmca/utilities/time_measurement.hpp"

namespace mpmca::pipeline {
class InOutput;
class Task;
class DataBucket;

class Pipeline
{
  public:
    struct Options
    {
        int command_buffer_length = 3;
        int base_step_ms = 1;
        bool print_state_changes = true;
        bool print_client_state_changes = true;
        bool guarded = true;
        bool mark_nondeterministic_log_output = false;

        Options() = default;

        Options(utilities::ConfigurationPtr config)
            : command_buffer_length(config->GetConfigurationObject("Pipeline")->GetValue<int>("MainTickBufferSize", 3))
            , base_step_ms(config->GetConfigurationObject("Pipeline")->GetValue<int>("BaseStepMs", 1))
            , print_state_changes(
                  config->GetConfigurationObject("Pipeline")->GetValue<bool>("PrintStateChanges", false))
            , print_client_state_changes(
                  config->GetConfigurationObject("Pipeline")->GetValue<bool>("PrintClientStateChanges", false))
            , guarded(config->GetConfigurationObject("Pipeline")->GetValue<bool>("Guarded", true))
            , mark_nondeterministic_log_output(
                  config->GetConfigurationObject("Pipeline")->GetValue<bool>("MarkNondeterministicLogOutput", true))
        {
        }
    };

    Pipeline(const Options& options);
    Pipeline(const std::string& json_string);
    Pipeline(utilities::ConfigurationPtr config);
    ~Pipeline();

    int GetPipelineReturnCode() const;

    void Prepare();
    int GetBaseStepMs() const;
    int64_t GetTickCycle() const;
    int64_t GetTheoreticalMainTickCycle(size_t task_index) const;

    const utilities::TimeMeasurement& GetInOutputTickMeasurement() const;
    const utilities::TimeMeasurement& GetInOutputMainTickMeasurement() const;
    const utilities::TimeMeasurement& GetInOutputTaskCompletedMeasurement() const;

    const StateMachine& GetStateMachine() const;
    StateMachine& GetStateMachine();

    const Safety& GetSafety() const;
    Safety& GetSafety();

    std::shared_ptr<Task> GetTaskPtr(size_t task_index);
    std::shared_ptr<const Task> GetConstTaskPtr(size_t task_index) const;

    const utilities::Clock& GetClock() const;
    utilities::Clock& GetClock();

    const utilities::ControllableClock& GetStateClock() const;

    bool HasTaskSteps(size_t task_index) const;

    int GetNextExecutionOrder() const;

    int GetMainTickBufferSize() const;
    size_t GetNumInOutputs() const;
    size_t GetNumTasks() const;

    bool GetCurrentTickIsMainTick(size_t task_index) const;
    bool GetNextTickIsMainTick(size_t task_index) const;
    bool GetMarkNondeterministicLogOutput() const;

    void MainTick(size_t task_index);
    void Tick();

    template <class T, class... Args>
    std::shared_ptr<T> AddInOutput(const std::string& name, Args... args);

    template <class... Args>
    std::shared_ptr<Task> MakeTask(Args... args);

  private:
    void SortInOutputs();
    void SortTasks();

    void InitTasks(utilities::ConfigurationPtr config);
    void AddInOutputs(utilities::ConfigurationPtr config);
    void AddInOutput(const std::string& in_output_name, utilities::ConfigurationPtr config);

    void ThrowSafetyLevelUnpreparedError() const;
    void CheckDataBucketIsDone(int64_t theoretical_main_tick_cycle, size_t task_index) const;
    void TickClocks();
    void PrintSafetyLevelChange();

    void MainTickStateMachine();
    void TickStateMachine();

    void TickAllInOutputs();
    void MainTickAllInOutputs(DataBucket& data_bucket);
    void TaskCompletedAllInOutputs(const DataBucket& data_bucket);

    void TickSafeLogic();
    void TickFailLogic();

    void PerformOptionsCheck();
    void PrepareInOutputMessageBus();
    void PrepareTimeMeasurement();
    void MakeTaskChecks();

    const int m_command_buffer_length;
    const int m_base_step_ms;
    const bool m_print_state_changes;
    const bool m_print_client_state_changes;
    const bool m_mark_nondeterministic_log_output;

    Safety m_safety;
    utilities::Logger m_logger;
    utilities::Clock m_clock;
    utilities::ControllableClock m_state_clock;
    StateMachine m_state_machine;
    MessageBus m_in_output_message_bus;

    SafetyLevel m_previous_safety_level;

    utilities::TimeMeasurement m_in_output_tick_measurement;
    utilities::TimeMeasurement m_in_output_main_tick_measurement;
    utilities::TimeMeasurement m_in_output_task_completed_measurement;

    std::atomic<bool> m_tick_running;
    std::atomic<int64_t> m_tick_cycle;

    std::vector<std::shared_ptr<InOutput>> m_in_outputs;
    std::vector<utilities::ClockIndex> m_in_output_tick_clocks;
    std::vector<utilities::ClockIndex> m_in_output_main_tick_clocks;
    std::vector<utilities::ClockIndex> m_in_output_task_completed_clocks;
    std::deque<std::shared_ptr<Task>> m_tasks;

    int m_pipeline_return_code;
};
}  //namespace mpmca::pipeline

#include "mpmca/pipeline/in_output.hpp"
#include "mpmca/pipeline/task.hpp"

namespace mpmca::pipeline {

template <class T, class... Args>
std::shared_ptr<T> Pipeline::AddInOutput(const std::string& name, Args... args)
{
    if (m_safety.GetLevel() != SafetyLevel::kUnprepared) {
        throw std::runtime_error(
            "It is only possible to add InOutputs to the Pipeline before calling Pipeline::Prepare().");
    }

    m_in_outputs.push_back(std::make_shared<T>(*this, name, args...));
    return std::dynamic_pointer_cast<T>(m_in_outputs.back());
}

template <class... Args>
std::shared_ptr<Task> Pipeline::MakeTask(Args... args)
{
    MakeTaskChecks();
    m_tasks.emplace_back(std::make_shared<Task>(*this, args...));
    m_tasks.back()->SetTaskIndex(m_tasks.size() - 1);
    return m_tasks.back();
}

}  //namespace mpmca::pipeline