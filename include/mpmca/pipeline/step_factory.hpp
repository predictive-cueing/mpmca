/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <functional>
#include <map>
#include <memory>
#include <string>

namespace mpmca::pipeline {
class Step;
class StepFactory
{
  public:
    static StepFactory* Instance()
    {
        static StepFactory factory;
        return &factory;
    }

    StepFactory() {}

    std::shared_ptr<Step> Create(const std::string& class_name, Task& task, const std::string& name,
                                 utilities::ConfigurationPtr config)
    {
        Step* instance = nullptr;

        // find name in the registry and call factory method.
        auto it = m_factory_function_registry.find(class_name);
        if (it != m_factory_function_registry.end())
            instance = (it->second)(task, name, config);

        // wrap instance in a shared ptr and return
        if (instance != nullptr)
            return std::shared_ptr<Step>(instance);
        else
            return nullptr;
    }

    bool Exists(const std::string& class_name)
    {
        auto it = m_factory_function_registry.find(class_name);
        return it != m_factory_function_registry.end();
    }

    bool RegisterFactoryFunction(
        const std::string& class_Name,
        std::function<Step*(Task& task, const std::string& name, utilities::ConfigurationPtr config)>
            class_factory_function)
    {
        m_factory_function_registry[class_Name] = class_factory_function;
        return true;
    }

    std::map<std::string, std::function<Step*(Task& task, const std::string& name, utilities::ConfigurationPtr config)>>
        m_factory_function_registry;
};
}  //namespace mpmca::pipeline
