#pragma once
#include <stdexcept>
#include <type_traits>

#include "mpmca/json.hpp"
#include "mpmca/pipeline/bus_base_message.hpp"

namespace mpmca::pipeline {

template <class T>
class BusMessage final : public BusBaseMessage
{
  public:
    template <class... Args>
    BusMessage(Args... args)
        : BusBaseMessage(T::message_type_id, T::GetMessageName())
        , m_message(args...)
    {
        // if compiler throws error such as
        // ‘type_id’ is not a member of 'T’
        // you forgot to put the
        // in the struct/class you want to push on the message bus
    }

    T& GetMessage() { return m_message; }

    void SerializeToArchive(cereal::BinaryOutputArchive& archive) const override { archive(m_message); }
    void DeserializeFromArchive(cereal::BinaryInputArchive& archive) override { archive(m_message); }

  private:
    T m_message;
};

template <class T>
bool BusBaseMessage::CheckMessageIsTypeSlowButReliable(bool throw_error)
{
    if (dynamic_cast<BusMessage<T>*>(this) != nullptr)
        return true;

    if (throw_error) {
        throw std::runtime_error(
            "A message on the message bus is not of the type it thinks it is. You may have defined multiple types with "
            "the same name (hash).");
    }

    return false;
}

}  //namespace mpmca::pipeline