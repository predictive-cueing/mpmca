/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <string>

#include "mpmca/pipeline/in_output.hpp"
#include "mpmca/pipeline/in_output_factory.hpp"

namespace mpmca::pipeline {
template <class T>
class InOutputRegistrar
{
  public:
    InOutputRegistrar(const std::string& class_name)
    {
        InOutputFactory::Instance()->RegisterFactoryFunction(
            class_name,
            [](Pipeline& pipeline, const std::string& name, utilities::ConfigurationPtr config) -> InOutput* {
                return new T(pipeline, name, config);
            });
    }
};
}  //namespace mpmca::pipeline
