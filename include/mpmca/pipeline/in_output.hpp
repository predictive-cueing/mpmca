/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <mutex>

#include "mpmca/pipeline/data_bucket.hpp"
#include "mpmca/pipeline/message_bus.hpp"
#include "mpmca/pipeline/unit.hpp"
#include "mpmca/utilities/configuration.hpp"
#include "mpmca/utilities/logger.hpp"

namespace mpmca::pipeline {
class Pipeline;
class Task;

class InOutput : public Unit
{
  public:
    virtual ~InOutput();

  protected:
    friend Pipeline;
    friend Task;
    InOutput(Pipeline& pipeline, const std::string& name, const std::string& type_name);
    InOutput(Pipeline& pipeline, const std::string& name, const std::string& type_name,
             utilities::ConfigurationPtr config);

    virtual void Tick(MessageBus& in_output_message_bus) = 0;
    virtual void SafeTick() = 0;
    virtual void TaskCompleted(const DataBucket& data_bucket) = 0;
    virtual void MainTick(DataBucket& data_bucket) = 0;

    virtual void Prepare() = 0;
    virtual void PrepareTaskMessageBus(const Task& task, MessageBus& message_bus) override;
    virtual void CheckTaskMessageBusPrepared(const Task& task, const MessageBus&) override;
    virtual void PrepareInOutputMessageBus(MessageBus&) override;
    virtual void CheckInOutputMessageBusPrepared(const MessageBus&) override;
};
}  //namespace mpmca::pipeline