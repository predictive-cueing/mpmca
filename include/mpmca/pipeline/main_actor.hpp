/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <iostream>
#include <string>

#include "mpmca/pipeline/pipeline.hpp"
#include "mpmca/pipeline/safety_level.hpp"
#include "mpmca/pipeline/trigger.hpp"
#include "mpmca/utilities/command_line_options_parser.hpp"
#include "mpmca/utilities/configuration.hpp"
#include "mpmca/utilities/json_helper.hpp"
#include "mpmca/utilities/logger.hpp"
namespace mpmca::pipeline {
class MainActor
{
  private:
    utilities::Logger m_logger;
    std::string m_executable_name;
    bool m_print_help;
    bool m_config_generator;
    bool m_free_run;
    bool m_overwrite;
    int64_t m_max_iterations;
    std::string m_input_configuration_filename;
    std::string m_output_configuration_filename = "AUTO_generated_config.json";

  public:
    MainActor(int argc, char** argv);
    int Run();

  private:
    int RunPipeline();
    int ConfigGenerator();

    void ProcessInputs(int argc, char** argv);
    void PrintUsage();
};
}  //namespace mpmca::pipeline