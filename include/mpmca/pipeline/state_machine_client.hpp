/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <memory>

#include "mpmca/pipeline/state.hpp"

namespace mpmca::pipeline {
class StateMachine;
class StateMachineClient
{
  private:
    friend StateMachine;
    std::string m_name;
    State m_current_state;
    State m_go_to_state;
    StateMachine* m_state_machine = nullptr;

    StateMachineClient(const std::string& name, StateMachine* machine);
    void SetCurrentState(const State& current_state);

  public:
    const std::string& GetName() const;
    const State& GetCurrentState() const;
    const State& GetGoToState() const;
    bool IsCurrentStateFailState() const;
    bool IsCurrentStateTransitionState() const;
    int GetNumberClientsNotInState(const State& state) const;
    void SetGoToState(const State& go_to_state);
};
}  //namespace mpmca