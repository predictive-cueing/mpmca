/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <string>

namespace mpmca::pipeline {
enum class State
{
    kNone,
    kIdle,
    kPreparing,
    kPrepared,
    kReady,
    kStarting,
    kStarted,
    kRun,
    kPausing,
    kPaused,
    kPause,
    kStopping,
    kStopped,
    kResuming,
    kResumed,
    kTerminating,
    kTerminated,
    kTerminate,
    kFailing,
    kFailed,
    kFail,
    kRecovering,
    kRecovered,
    kFailTerminating,
    kFailTerminated,
    kFailTerminate,
    kBooting,
    kBooted
};

std::string ToString(State state);
State FromString(const std::string& string);
std::ostream& operator<<(std::ostream& os, const State& state);
}  //namespace mpmca::pipeline