/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <mutex>

#include "mpmca/pipeline/data_bucket.hpp"
#include "mpmca/pipeline/message_bus.hpp"
#include "mpmca/pipeline/safety.hpp"
#include "mpmca/pipeline/state_machine.hpp"
#include "mpmca/pipeline/state_machine_client.hpp"
#include "mpmca/utilities/configuration.hpp"
#include "mpmca/utilities/logger.hpp"

namespace mpmca::pipeline {
class Pipeline;
class Task;
class Step;
class InOutput;

class Unit
{
  public:
    virtual ~Unit();
    bool operator<(const Unit& other) const;

    int GetExecutionOrder() const;
    const std::string& GetName() const;
    const std::string& GetTypeName() const;

    const Safety& GetSafety() const;
    Safety& GetSafety();

    void SetMaximumComputationTimeUs(int time_us);
    const std::chrono::microseconds& GetMaximumComputationTimeUs() const;

    const StateMachineClient& GetStateMachineClient() const;
    StateMachineClient& GetStateMachineClient();

  protected:
    void GoToIdleAndTerminate(bool condition);
    void TransitionStateMachine();

    utilities::Logger m_logger;

    virtual void Prepare() = 0;
    virtual void Tick(MessageBus& in_output_message_bus) = 0;
    virtual void SafeTick() = 0;
    virtual void TaskCompleted(const DataBucket& data_bucket) = 0;
    virtual void MainTick(DataBucket& data_bucket) = 0;

    virtual void PrepareTaskMessageBus(const Task& task, MessageBus& message_bus) = 0;
    virtual void CheckTaskMessageBusPrepared(const Task& task, const MessageBus&) = 0;
    virtual void PrepareInOutputMessageBus(MessageBus&) = 0;
    virtual void CheckInOutputMessageBusPrepared(const MessageBus&) = 0;

    void SetExecutionOrder(int priority);
    void SetStateMachineClient(StateMachineClient* state_machine_client);
    void SetSafety(Safety* safety);

  private:
    friend Pipeline;
    friend Task;
    friend Step;
    friend InOutput;

    Unit(const std::string& name, const std::string& type_name, Safety& parent_safety,
         StateMachine& parent_state_machine, int execution_order, int maximum_computation_time_us);

    Unit(const std::string& name, const std::string& type_name, Safety& parent_safety,
         StateMachine& parent_state_machine, utilities::ConfigurationPtr config);

    void GuardedPrepare();
    void GuardedTick(MessageBus& in_output_message_bus);
    void GuardedTaskCompleted(const DataBucket& data_bucket);
    void GuardedMainTick(DataBucket& data_bucket);
    void GuardedSafeTick();

    const std::string m_name;
    const std::string m_type_name;
    int m_execution_order;
    std::chrono::microseconds m_maximum_computation_time_us;
    Safety* m_safety_ptr;
    StateMachineClient* m_state_machine_client_ptr;

    std::mutex m_data_mutex;
};
}  //namespace mpmca::pipeline