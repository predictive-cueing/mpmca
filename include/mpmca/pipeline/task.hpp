/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <atomic>
#include <cstdint>
#include <cstdlib>
#include <deque>
#include <iostream>
#include <memory>

#include "mpmca/pipeline/data_bucket.hpp"
#include "mpmca/pipeline/safety.hpp"
#include "mpmca/pipeline/state_machine.hpp"
#include "mpmca/pipeline/step.hpp"
#include "mpmca/pipeline/step_factory.hpp"
#include "mpmca/utilities/clock.hpp"
#include "mpmca/utilities/configuration.hpp"
#include "mpmca/utilities/controllable_clock.hpp"
#include "mpmca/utilities/horizon.hpp"

namespace mpmca::pipeline {
class Pipeline;
class InOutput;
class Task
{
  public:
    Task(Pipeline& pipeline, const std::string& name, const utilities::Horizon& horizon);
    Task(Pipeline& pipeline, const std::string& name, int dt_ms, const utilities::Horizon& horizon);
    Task(Pipeline& pipeline, const std::string& name, utilities::ConfigurationPtr config);
    ~Task();

    const Safety& GetSafety() const;
    const utilities::Clock& GetClock() const;
    const utilities::ControllableClock& GetStateClock() const;
    const utilities::Horizon& GetHorizon() const;
    size_t GetTaskIndex() const;

    utilities::TimeMeasurement& GetTimeMeasurement();
    const utilities::TimeMeasurement& GetTimeMeasurement() const;

    bool IsEmpty() const;
    const std::string& GetName() const;

    template <class T, class... Args>
    std::shared_ptr<T> AddStep(const std::string& name, Args... args);

  private:
    friend Pipeline;
    friend Step;
    Safety& GetSafety();
    void InitializeDataBuckets(const Pipeline& pipeline);
    void PrepareTaskMessageBus(std::vector<std::shared_ptr<InOutput>>& in_outputs);
    void PrepareTimeMeasurement();
    void SetTaskIndex(size_t task_index);

    void SortSteps();
    int GetMainTickMultiple() const;
    std::mutex& GetDataBucketMutex(int64_t main_tick_cycle);

    DataBucket& GetDataBucket(int64_t main_tick_cycle);
    const DataBucket& GetDataBucket(int64_t main_tick_cycle) const;
    StateMachine& GetStateMachine();
    int GetNextExecutionOrder() const;

    void Prepare();
    void MainTick(DataBucket& data_bucket);
    void TickClocks();

    size_t GetNumSteps() const;
    std::shared_ptr<Step> GetStepPtr(size_t step_index);

    void AddSteps(utilities::ConfigurationPtr config);
    void AddStep(const std::string& step_name, utilities::ConfigurationPtr config);
    Task(const Task&) = delete;
    Task& operator=(const Task&) = delete;

    std::atomic<int64_t>& GetActualMainTickCycle();
    std::mutex& GetMainTickMutex();
    std::atomic<bool>& GetMainTickRunning();

    Pipeline& m_pipeline;
    Safety m_safety;
    utilities::Logger m_logger;
    utilities::TimeMeasurement m_time_measurement;
    const std::string m_name;
    std::atomic<bool> m_main_tick_protector;

    const utilities::Horizon m_horizon;
    utilities::Clock m_clock;
    utilities::ControllableClock m_state_clock;
    std::deque<DataBucket> m_data_buckets;
    std::deque<std::mutex> m_data_bucket_mutex;

    std::vector<std::shared_ptr<Step>> m_steps;

    utilities::TimeMeasurement m_step_measurement;
    std::vector<utilities::ClockIndex> m_step_clocks;

    const int m_main_tick_multiple;
    const bool m_mark_nondeterministic_log_output;
    std::atomic<int64_t> m_actual_main_tick_cycle;
    std::mutex m_main_tick_mutex;
    std::atomic<bool> m_main_tick_running;
    size_t m_task_index;
};
}  //namespace mpmca::pipeline

#include "mpmca/pipeline/pipeline.hpp"

namespace mpmca::pipeline {

template <class T, class... Args>
std::shared_ptr<T> Task::AddStep(const std::string& name, Args... args)
{
    if (m_safety.GetLevel() != SafetyLevel::kUnprepared) {
        throw std::runtime_error(
            "It is only possible to add Steps to the Task of a Pipeline before calling Pipeline::Prepare().");
    }
    m_steps.push_back(std::make_shared<T>(*this, name, args...));
    return std::dynamic_pointer_cast<T>(m_steps.back());
}

}  //namespace mpmca::pipeline