/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <string>

#include "mpmca/pipeline/step.hpp"
#include "mpmca/pipeline/step_factory.hpp"

namespace mpmca::pipeline {
template <class T>
class StepRegistrar
{
  public:
    StepRegistrar(const std::string& class_name)
    {
        StepFactory::Instance()->RegisterFactoryFunction(
            class_name, [](Task& task, const std::string& name, utilities::ConfigurationPtr config) -> Step* {
                return new T(task, name, config);
            });
    }
};
}  //namespace mpmca::pipeline
