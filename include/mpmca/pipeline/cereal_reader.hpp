/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <cereal/archives/binary.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/unordered_map.hpp>
#include <cereal/types/vector.hpp>
#include <chrono>
#include <fstream>
#include <map>
#include <vector>

#include "mpmca/cereal_tools.hpp"
#include "mpmca/linear_algebra.hpp"
#include "mpmca/pipeline/bus_base_message.hpp"
#include "mpmca/pipeline/cereal_types.hpp"
#include "mpmca/pipeline/message_bus.hpp"

namespace mpmca::pipeline {

class CerealReader
{
    std::string m_filename;
    std::ifstream m_os;
    std::streamsize m_file_size;
    cereal::BinaryInputArchive m_archive;
    std::map<cereal_types::bus_message_type_id, std::map<cereal_types::time_ms_type, cereal_types::memory_address_type>>
        m_message_start_address;

    void ReadFile();

  public:
    CerealReader(const std::string& filename);

    const std::string& GetFilename() const;
    void AddMessageAddress(cereal_types::bus_message_type_id message_type_id, cereal_types::time_ms_type time_ms,
                           cereal_types::memory_address_type address);
    bool HasMessageType(cereal_types::bus_message_type_id message_type_id) const;
    bool HasMessageOfTypeAtTime(cereal_types::bus_message_type_id message_type_id,
                                cereal_types::time_ms_type time_ms) const;

    template <class T>
    std::set<cereal_types::time_ms_type> GetSetOfValidTimestamps() const
    {
        std::set<cereal_types::time_ms_type> result;
        if (HasMessageType<T>()) {
            for (const auto& [key, value] : m_message_start_address.at(T::message_type_id)) {
                result.insert(key);
            }
        }
        return result;
    }

    template <class T>
    bool HasMessageType() const
    {
        return HasMessageType(T::message_type_id);
    }
    template <class T>
    bool HasMessageOfTypeAtTime(cereal_types::time_ms_type time_ms) const
    {
        return HasMessageOfTypeAtTime(T::message_type_id, time_ms);
    }
    template <class T>
    void UpdateMessage(T& message, cereal_types::time_ms_type time_ms)
    {
        if (HasMessageOfTypeAtTime(T::message_type_id, time_ms)) {
            m_os.seekg(m_message_start_address.at(T::message_type_id).at(time_ms));
            m_archive(message);
        }
        else {
            if (HasMessageType(T::message_type_id)) {
                throw std::runtime_error("Message of type " + T::GetMessageName() + " does not have a record at time " +
                                         std::to_string(time_ms) + ".");
            }
            else {
                throw std::runtime_error("Message of type " + T::GetMessageName() +
                                         " is not present in the data file.");
            }
        }
    }
};
}  //namespace mpmca::pipeline