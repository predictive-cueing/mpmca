/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/pipeline/data_bucket.hpp"
#include "mpmca/pipeline/task.hpp"
#include "mpmca/pipeline/unit.hpp"
#include "mpmca/utilities/configuration.hpp"
#include "mpmca/utilities/horizon.hpp"
#include "mpmca/utilities/logger.hpp"

namespace mpmca::pipeline {
class Step : public Unit
{
  public:
    virtual ~Step();

  protected:
    Step(Task& task, const std::string& name, const std::string& type_name);
    Step(Task& task, const std::string& name, const std::string& type_name, utilities::ConfigurationPtr config);

  private:
    friend Task;
    friend Pipeline;

    virtual void Prepare() override = 0;
    virtual void MainTick(DataBucket& data_bucket) override = 0;
    virtual void Tick(MessageBus& in_output_message_bus) final override;
    virtual void SafeTick() final override;
    virtual void TaskCompleted(const DataBucket& data_bucket) final override;

    virtual void PrepareTaskMessageBus(const Task& task, MessageBus&);
    virtual void CheckTaskMessageBusPrepared(const Task& task, const MessageBus&);

    virtual void PrepareInOutputMessageBus(MessageBus&) final override;
    virtual void CheckInOutputMessageBusPrepared(const MessageBus&) final override;
};
}  //namespace mpmca::pipeline