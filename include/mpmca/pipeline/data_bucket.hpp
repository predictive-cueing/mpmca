/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/pipeline/message_bus.hpp"
#include "mpmca/utilities/logger.hpp"
#include "mpmca/utilities/time_measurement.hpp"

namespace mpmca::pipeline {
class Pipeline;
class Task;

class DataBucket
{
  public:
    DataBucket(const utilities::Horizon& horizon, utilities::TimeMeasurement& time_measurement);

    const utilities::TimeMeasurement& GetTimeMeasurement() const;
    utilities::TimeMeasurement& GetTimeMeasurement();
    const utilities::Horizon& GetHorizon() const;

    MessageBus& GetTaskSignalBus();
    MessageBus const& GetTaskSignalBus() const;

    int64_t GetTickCycle() const;

    int64_t GetPipelineTimeMs() const;
    double GetPipelineTimeSecond() const;
    int64_t GetPipelineStateTimeMs() const;
    double GetPipelineStateTime() const;
    int64_t GetMainTickCycle() const;
    int64_t GetMainTickDtMs() const;
    bool GetMainStateChangedOnThisMainTick() const;
    size_t GetTaskIndex() const;
    const std::string& GetTaskName() const;
    bool IsPipelineReady() const;
    bool IsDone() const;

    int GetId() const;

    int64_t GetTaskStateTimeMs() const;
    int64_t GetTaskTimeMs() const;

  protected:
    friend Pipeline;
    friend Task;
    void InitFromPipeline(const Pipeline& pipeline, int64_t theoretical_main_tick_cycle, size_t task_index);
    void DeclarePipelineReady();
    void SetMainTickCycle(int64_t counter);
    void InitFromTask(const Task& task);
    bool IsTaskReady() const;
    void DeclareTaskReady();
    void DeclareDone();

    static int s_instances;
    int m_id;
    const utilities::Horizon m_horizon;
    utilities::TimeMeasurement& m_time_measurement;
    MessageBus m_task_message_bus;
    int64_t m_tick_cycle;
    int64_t m_main_tick_cycle;
    int64_t m_main_tick_dt_ms;
    int64_t m_pipeline_time_ms;
    int64_t m_pipeline_state_time_ms;

    bool m_pipeline_initialized;
    bool m_pipeline_ready_for_processing;
    bool m_task_initialized;
    bool m_task_ready_for_processing;
    bool m_done;

    bool m_main_state_changed_on_this_main_tick;

    int64_t m_task_time_ms;
    int64_t m_task_state_time_ms;
    size_t m_task_index;
    std::string m_task_name;
};

}  //namespace mpmca::pipeline