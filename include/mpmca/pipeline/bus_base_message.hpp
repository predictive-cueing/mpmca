/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <cereal/archives/binary.hpp>
#include <cstdint>
#include <string>
#include <vector>

namespace mpmca::pipeline {

class BusBaseMessage
{
  public:
    BusBaseMessage(uint64_t message_type_id, const std::string& message_type_name);
    virtual ~BusBaseMessage() = default;

    uint64_t GetTypeId() const;
    const std::string& GetTypeName() const;

    bool IsUpdated() const;
    void SetIsUpdated(bool is_updated);

    template <class T>
    bool GetMessageIsType()
    {
        return m_type_id == T::message_type_id;
    }

    template <class T>
    bool CheckMessageIsTypeSlowButReliable(bool throw_error = true);

    virtual void SerializeToArchive(cereal::BinaryOutputArchive& archive) const = 0;
    virtual void DeserializeFromArchive(cereal::BinaryInputArchive& archive) = 0;

    bool GetMessageIsType();

  private:
    const uint64_t m_type_id;
    const std::string m_type_name;
    bool m_is_updated;
};

}  //namespace mpmca::pipeline
