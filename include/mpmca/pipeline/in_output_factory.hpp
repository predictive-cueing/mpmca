/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <functional>
#include <map>
#include <memory>
#include <string>

#include "mpmca/pipeline/in_output.hpp"
#include "mpmca/utilities/logger.hpp"
namespace mpmca::pipeline {

class InOutputFactory
{
  public:
    static InOutputFactory* Instance()
    {
        static InOutputFactory factory;
        return &factory;
    }

    InOutputFactory() = default;

    std::shared_ptr<InOutput> Create(const std::string& class_name, Pipeline& pipeline, const std::string& name,
                                     utilities::ConfigurationPtr config)
    {
        InOutput* instance = nullptr;

        // find name in the registry and call factory method.
        auto it = m_factory_function_registry.find(class_name);
        if (it != m_factory_function_registry.end())
            instance = (it->second)(pipeline, name, config);

        // wrap instance in a shared ptr and return
        if (instance != nullptr)
            return std::shared_ptr<InOutput>(instance);
        else
            return nullptr;
    }

    bool Exists(const std::string& class_name) const
    {
        auto it = m_factory_function_registry.find(class_name);
        return it != m_factory_function_registry.end();
    }

    void RegisterFactoryFunction(
        const std::string& class_name,
        std::function<InOutput*(Pipeline& pipeline, const std::string& name, utilities::ConfigurationPtr config)>
            class_factory_function)
    {
        m_factory_function_registry[class_name] = class_factory_function;
    }

    std::map<std::string,
             std::function<InOutput*(Pipeline& pipeline, const std::string& name, utilities::ConfigurationPtr config)>>
        m_factory_function_registry;
};
}  //namespace mpmca::pipeline
