/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/pipeline/safety_level.hpp"

namespace mpmca::pipeline {
bool operator<(SafetyLevel a, SafetyLevel b) = delete;
bool operator>(SafetyLevel a, SafetyLevel b) = delete;
bool operator<=(SafetyLevel a, SafetyLevel b) = delete;
bool operator>=(SafetyLevel a, SafetyLevel b) = delete;
bool operator<(const SafetyLevel& a, const SafetyLevel& b) = delete;
bool operator>(const SafetyLevel& a, const SafetyLevel& b) = delete;
bool operator<=(const SafetyLevel& a, const SafetyLevel& b) = delete;
bool operator>=(const SafetyLevel& a, const SafetyLevel& b) = delete;

inline std::string ToString(SafetyLevel level)
{
    switch (level) {
        case SafetyLevel::kSafe:
            return std::string("SAFE");
            break;
        case SafetyLevel::kFail:
            return std::string("FAIL");
            break;
        case SafetyLevel::kUnprepared:
            return std::string("UNPREPARED");
            break;
        case SafetyLevel::kCritical:
            return std::string("CRITICAL");
            break;
        default:
            return std::string("UNDEFINED_SAFETY_LEVEL");
            break;
    }
}

inline std::ostream& operator<<(std::ostream& os, const SafetyLevel& level)
{
    std::string str = ToString(level);
    os << str.c_str();
    return os;
}
}  //namespace mpmca