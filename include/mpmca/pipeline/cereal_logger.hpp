/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <cereal/archives/binary.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/unordered_map.hpp>
#include <cereal/types/vector.hpp>
#include <chrono>
#include <fstream>
#include <map>
#include <vector>

#include "mpmca/cereal_tools.hpp"
#include "mpmca/linear_algebra.hpp"
#include "mpmca/messages/message_id_macro_definition.hpp"
#include "mpmca/pipeline/bus_base_message.hpp"
#include "mpmca/pipeline/cereal_types.hpp"
#include "mpmca/pipeline/message_bus.hpp"

namespace mpmca::pipeline {
class CerealLogger
{
  protected:
    std::ofstream m_output_file_stream;
    std::ostringstream m_buffer;
    cereal::BinaryOutputArchive m_archive;
    cereal::BinaryOutputArchive m_buffered_archive;

  public:
    CerealLogger(const std::string& filename);

    template <class T>
    void WriteMessage(cereal_types::time_ms_type current_time_ms, const T& message)
    {
        m_buffered_archive(message);
        DoWriteMessage(current_time_ms, T::message_type_id);
    }

  protected:
    void DoWriteMessage(cereal_types::time_ms_type current_time_ms, cereal_types::bus_message_type_id message_type_id);
};
}  //namespace mpmca::pipeline