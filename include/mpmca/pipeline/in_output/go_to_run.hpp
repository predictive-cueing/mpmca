/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/pipeline/in_output.hpp"
#include "mpmca/pipeline/in_output_registrar.hpp"

namespace mpmca::pipeline::in_output {
class GoToRun : public InOutput
{
  public:
    GoToRun(Pipeline& pipeline, const std::string& name);
    GoToRun(Pipeline& pipeline, const std::string& name, utilities::ConfigurationPtr config);

    void Tick(MessageBus& in_output_message_bus) override;
    void SafeTick() override;
    void MainTick(DataBucket& data_bucket) override;
    void TaskCompleted(const DataBucket& data_bucket) override;
    void Prepare() override { GetSafety().Prepared(); }
    enum class TargetState
    {
        kRun,
        kIdle
    };

    void SetTargetState(TargetState state);

  private:
    TargetState m_target_state;
    bool m_was_in_target_state;
};

static InOutputRegistrar<GoToRun> GoToRunRegistrar("GoToRun");

}  //namespace mpmca::pipeline::in_output