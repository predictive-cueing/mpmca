/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/pipeline/in_output.hpp"
#include "mpmca/pipeline/in_output_registrar.hpp"
#include "mpmca/utilities/clock_index.hpp"
#include "mpmca/utilities/time_measurement.hpp"
namespace mpmca::pipeline::in_output {
class TimingPrinter : public InOutput
{
  public:
    TimingPrinter(Pipeline& pipeline, const std::string& name, utilities::ConfigurationPtr config);

    void Prepare() override;
    void Tick(MessageBus& in_output_message_bus) override;
    void SafeTick() override;
    void TaskCompleted(const DataBucket& data_bucket) override;
    void MainTick(DataBucket& data_bucket) override;

  private:
    utilities::TimeMeasurement m_time_measurement;
    utilities::ClockIndex m_test_clock_index;
    int m_counter;
};

static InOutputRegistrar<TimingPrinter> TimingPrinterInOutputRegistrar("TimingPrinter");

}  //namespace mpmca::pipeline::in_output