/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/pipeline/in_output.hpp"
#include "mpmca/pipeline/in_output_registrar.hpp"

namespace mpmca::pipeline::in_output {

class HelloWorld : public InOutput
{
  public:
    HelloWorld(Pipeline& pipeline, const std::string& name, utilities::ConfigurationPtr config);
    HelloWorld(Pipeline& pipeline, const std::string& name, bool print_messages);
    ~HelloWorld();

    void Prepare() override;
    void Tick(MessageBus& in_output_message_bus) override;
    void SafeTick() override;
    void TaskCompleted(const DataBucket& data_bucket) override;
    void MainTick(DataBucket& data_bucket) override;

    void PrepareTaskMessageBus(const Task& task, MessageBus&) override;
    void CheckTaskMessageBusPrepared(const Task& task, const MessageBus&) override;

    void PrepareInOutputMessageBus(MessageBus&) override;
    void CheckInOutputMessageBusPrepared(const MessageBus&) override;

  private:
    bool m_print_messages;
};

static InOutputRegistrar<HelloWorld> HelloWorldInOutputRegistrar("HelloWorld");

}  //namespace mpmca::pipeline::in_output