/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/pipeline/in_output.hpp"
#include "mpmca/pipeline/in_output_registrar.hpp"

namespace mpmca::pipeline::in_output {
class EmptyInOutput : public InOutput
{
  public:
    EmptyInOutput(Pipeline& pipeline, const std::string& name, utilities::ConfigurationPtr config);
    EmptyInOutput(Pipeline& pipeline, const std::string& name);
    ~EmptyInOutput();

  protected:
    void Prepare() override;
    void Tick(MessageBus& in_output_message_bus) override;
    void SafeTick() override;
    void TaskCompleted(const DataBucket& data_bucket) override;
    void MainTick(DataBucket& data_bucket) override;
};

static InOutputRegistrar<EmptyInOutput> EmptyInputRegistrar("EmptyInOutput");

}  //namespace mpmca::pipeline::in_output