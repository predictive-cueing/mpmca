/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/control/simulator.hpp"
#include "mpmca/pipeline/in_output.hpp"
#include "mpmca/pipeline/in_output_registrar.hpp"
#include "mpmca/utilities/horizon.hpp"

namespace mpmca::pipeline::in_output {

template <typename TSIM>
struct CascadeReferencePoint
{
    int64_t m_horizon_time_offset_ms;
    int m_to_horizon_index;
    typename TSIM::StateVector m_x_ref;
    typename TSIM::InputVector m_u_ref;

    CascadeReferencePoint(int64_t horizon_time_offset_ms, int horizon_index)
        : m_horizon_time_offset_ms(horizon_time_offset_ms)
        , m_to_horizon_index(horizon_index)
        , m_x_ref{TSIM::StateVector::Zero()}
        , m_u_ref{TSIM::InputVector::Zero()}
    {
    }
};

template <typename TSIM>
class CascadeController : public InOutput
{
  public:
    CascadeController(Pipeline& pipeline, const std::string& name, utilities::ConfigurationPtr config);
    void PrepareTaskMessageBus(const Task& task, MessageBus&) override;
    void CheckTaskMessageBusPrepared(const Task& task, const MessageBus&) override;

  private:
    void Prepare() override;
    void Tick(MessageBus& in_output_message_bus) override;
    void SafeTick() override {};
    void TaskCompleted(const DataBucket& data_bucket) override;
    void MainTick(DataBucket& data_bucket) override;

    std::string m_from_task_name;
    std::string m_to_task_name;
    std::vector<typename TSIM::StateVector> m_state_plan;
    std::vector<typename TSIM::InputVector> m_input_plan;
    std::vector<CascadeReferencePoint<TSIM>> m_reference_points;
    typename TSIM::StateVector m_reference_point_state_weight;
    typename TSIM::StateVector m_non_reference_point_state_weight;
    std::unique_ptr<utilities::Horizon> m_from_horizon;

    bool m_from_task_checks_okay;
    bool m_to_task_checks_okay;
};

}  //namespace mpmca::pipeline::in_output