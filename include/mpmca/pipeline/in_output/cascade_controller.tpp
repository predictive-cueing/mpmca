/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/control/input.hpp"
#include "mpmca/control/state.hpp"
#include "mpmca/json_tools.hpp"
#include "mpmca/linear_algebra.hpp"
#include "mpmca/messages/controller_input_plan.hpp"
#include "mpmca/messages/controller_state_error_weight_plan.hpp"
#include "mpmca/messages/controller_state_plan.hpp"
#include "mpmca/messages/controller_state_reference_plan.hpp"
#include "mpmca/pipeline/in_output/cascade_controller.hpp"
#include "mpmca/pipeline/task.hpp"

namespace mpmca::pipeline::in_output {

namespace {

template <typename TSIM>
std::vector<CascadeReferencePoint<TSIM>> ReadCascadePointConfiguration(utilities::ConfigurationPtr config)
{
    std::vector<int64_t> horizon_time_offsets =
        config->GetValue<std::vector<int64_t>>("ReferencePointHorizonTimeOffsetsMs", {0});

    std::vector<CascadeReferencePoint<TSIM>> result;
    for (int64_t horizon_time_offset : horizon_time_offsets) {
        result.emplace_back(horizon_time_offset, -10);
    }
    return result;
};
}  //namespace

template <typename TSIM>
CascadeController<TSIM>::CascadeController(Pipeline& pipeline, const std::string& name,
                                           utilities::ConfigurationPtr config)
    : InOutput(pipeline, name, "CascadeController", config)
    , m_from_task_name{config->GetValue<std::string>("FromTaskName", "Task1")}
    , m_to_task_name{config->GetValue<std::string>("ToTaskName", "Task1")}
    , m_reference_points{ReadCascadePointConfiguration<TSIM>(config)}
    , m_reference_point_state_weight{config->GetValue<typename TSIM::StateVector>("ReferencePointStateErrorWeight",
                                                                                  TSIM::StateVector::Zero())}
    , m_non_reference_point_state_weight{config->GetValue<typename TSIM::StateVector>(
          "NonReferencePointStateErrorWeight", TSIM::StateVector::Zero())}
    , m_from_task_checks_okay{false}
    , m_to_task_checks_okay{false}
{
    m_logger.Info("This CascadeController will provide a state reference to the PredictiveController in Task \"" +
                  m_to_task_name + "\" based on a state calculated by the PredictiveController in Task \"" +
                  m_from_task_name + "\".");
}

template <typename TSIM>
void CascadeController<TSIM>::PrepareTaskMessageBus(const Task& task, MessageBus& message_bus)
{
    if (task.GetName() != m_to_task_name) {
        return;
    }

    // We send these messages to the "ToTask":
    message_bus.MakeMessage<messages::ControllerStateErrorWeightPlan<TSIM>>(task.GetHorizon());
    message_bus.MakeMessage<messages::ControllerStateReferencePlan<TSIM>>(task.GetHorizon());

    if (m_to_task_checks_okay) {
        // PrepareTaskMessageBus is called once for each MessageBus in the Task, we only need to execute the code below
        // once.
        return;
    }

    for (CascadeReferencePoint<TSIM>& reference_point : m_reference_points) {
        reference_point.m_to_horizon_index = -1;

        for (size_t i = 0; i < task.GetHorizon().GetNMpc(); ++i) {
            if (reference_point.m_horizon_time_offset_ms == task.GetHorizon().GetHorizonTimeMs(i)) {
                reference_point.m_to_horizon_index = i;
            }
        }

        if (reference_point.m_to_horizon_index < 0) {
            std::string horizon_times = std::to_string(task.GetHorizon().GetHorizonTimeMs(0));

            for (size_t i = 1; i < task.GetHorizon().GetNMpc(); ++i) {
                horizon_times += (", " + std::to_string(task.GetHorizon().GetHorizonTimeMs(i)));
            }

            throw std::runtime_error(
                "ReferencePointHorizonTimeOffsetMs must be exactly equal to a point on the Horizon of the \"ToTask\", "
                "error for " +
                std::to_string(reference_point.m_horizon_time_offset_ms) +
                ". Time offsets present on the horizon are: " + horizon_times);
        }
        else {
            m_logger.Info("The index of the point with offset " +
                          std::to_string(reference_point.m_horizon_time_offset_ms) + " ms on the Horizon of " +
                          m_to_task_name + " is " + std::to_string(reference_point.m_to_horizon_index) + ".");
        }
    }

    // For this to work, a Task with a name equal to "ToTaskName" needs to exist (which is true, because otherwise we
    // would not end up here) and the controller horizon length should be greater or equal than the time offset of the
    // desired reference point.
    m_to_task_checks_okay = true;
}

template <typename TSIM>
void CascadeController<TSIM>::CheckTaskMessageBusPrepared(const Task& task, const MessageBus& message_bus)
{
    if (task.GetName() != m_from_task_name) {
        return;
    }
    message_bus.RequireMessage<messages::ControllerStatePlan<TSIM>>();
    message_bus.RequireMessage<messages::ControllerInputPlan<TSIM>>();
    m_from_task_checks_okay = true;
}

template <typename TSIM>
void CascadeController<TSIM>::Prepare()
{
    if (!m_from_task_checks_okay || !m_to_task_checks_okay) {
        throw std::runtime_error("The configuration of " + GetName() +
                                 " did not pass all checks, we cannot declare ourselves Prepared.");
    }
    GetSafety().Prepared();
}

template <typename TSIM>
void CascadeController<TSIM>::Tick(MessageBus& /* in_output_message_bus */)
{
    TransitionStateMachine();
}

template <typename TSIM>
void CascadeController<TSIM>::TaskCompleted(const DataBucket& data_bucket)
{
    if (data_bucket.GetTaskName() != m_from_task_name) {
        return;
    }

    data_bucket.GetTaskSignalBus().ReadUpdatedMessage<messages::ControllerStatePlan<TSIM>>(
        [&](const messages::ControllerStatePlan<TSIM>& message) {
            m_state_plan.resize(message.x_plan.size());
            std::copy(message.x_plan.cbegin(), message.x_plan.cend(), m_state_plan.begin());
        });

    data_bucket.GetTaskSignalBus().ReadUpdatedMessage<messages::ControllerInputPlan<TSIM>>(
        [&](const messages::ControllerInputPlan<TSIM>& message) {
            m_input_plan.resize(message.u_plan.size());
            std::copy(message.u_plan.cbegin(), message.u_plan.cend(), m_input_plan.begin());
        });

    if (m_from_horizon == nullptr) {
        m_from_horizon = std::make_unique<utilities::Horizon>(data_bucket.GetHorizon());
    }

    if (m_input_plan.size() != data_bucket.GetHorizon().GetNMpc()) {
        throw std::runtime_error("The size of the input plan (" + std::to_string(m_input_plan.size()) +
                                 ") should be equal to " + std::to_string(data_bucket.GetHorizon().GetNMpc()));
    }

    if (m_state_plan.size() != data_bucket.GetHorizon().GetNMpc() + 1) {
        throw std::runtime_error("The size of the state plan (" + std::to_string(m_state_plan.size()) +
                                 ") should be equal to " + std::to_string(data_bucket.GetHorizon().GetNMpc() + 1));
    }
}

template <typename TSIM>
void CascadeController<TSIM>::MainTick(DataBucket& data_bucket)
{
    if (data_bucket.GetTaskName() != m_to_task_name) {
        return;
    }

    if (m_input_plan.size() == 0 || m_state_plan.size() == 0) {
        return;
    }

    if (!m_from_horizon) {
        throw std::runtime_error("The m_from_horizon object is empty, even though the input and state plans are not.");
    }

    for (CascadeReferencePoint<TSIM>& reference_point : m_reference_points) {
        typename TSIM::StateVector x_ref = TSIM::StateVector::Zero();
        typename TSIM::InputVector u_ref = TSIM::InputVector::Zero();

        for (size_t i = 0; i < m_from_horizon->GetNMpc(); ++i) {
            int current_horizon_offset_ms = m_from_horizon->GetHorizonTimeMs(i);
            int next_horizon_offset_ms = m_from_horizon->GetHorizonTimeMs(i + 1);

            if (current_horizon_offset_ms == reference_point.m_horizon_time_offset_ms) {
                x_ref = m_state_plan.at(i);
                u_ref = m_input_plan.at(i);
                break;
            }
            else if (reference_point.m_horizon_time_offset_ms > current_horizon_offset_ms &&
                     reference_point.m_horizon_time_offset_ms < next_horizon_offset_ms) {
                double t_delta = (reference_point.m_horizon_time_offset_ms - current_horizon_offset_ms) / 1000.0;
                if (t_delta <= 0) {
                    throw std::runtime_error("t_delta should not be smaller or equal to 0. It is " +
                                             std::to_string(t_delta));
                }
                auto x_0 = m_state_plan.at(i);
                auto u_0 = m_input_plan.at(i);

                auto x_ref_view = control::MakeStateView<TSIM>(x_ref);
                auto x_0_view = control::MakeStateView<TSIM>(x_0);

                x_ref_view.GetPosition() =
                    x_0_view.GetPosition() + x_0_view.GetVelocity() * t_delta + 0.5 * u_0 * t_delta * t_delta;
                x_ref_view.GetVelocity() = x_0_view.GetVelocity() + u_0 * t_delta;

                u_ref = u_0;
                break;
            }
        }

        reference_point.m_x_ref = x_ref;
        reference_point.m_u_ref = u_ref;
    }

    data_bucket.GetTaskSignalBus().UpdateMessage<messages::ControllerStateReferencePlan<TSIM>>(
        [&](messages::ControllerStateReferencePlan<TSIM>& message) {
            std::fill(message.x_ref_plan.begin(), message.x_ref_plan.end(), TSIM::StateVector::Zero());

            for (const CascadeReferencePoint<TSIM>& reference_point : m_reference_points) {
                message.x_ref_plan.at(reference_point.m_to_horizon_index) = reference_point.m_x_ref;
            }
        });

    data_bucket.GetTaskSignalBus().UpdateMessage<messages::ControllerStateErrorWeightPlan<TSIM>>(
        [&](messages::ControllerStateErrorWeightPlan<TSIM>& message) {
            std::fill(message.w_x_plan.begin(), message.w_x_plan.end(), m_non_reference_point_state_weight);

            for (const CascadeReferencePoint<TSIM>& reference_point : m_reference_points) {
                message.w_x_plan.at(reference_point.m_to_horizon_index) = m_reference_point_state_weight;
            }
        });
}
}  //namespace mpmca::pipeline::in_output