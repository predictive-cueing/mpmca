/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/control/simulator.hpp"
#include "mpmca/pipeline/in_output.hpp"
#include "mpmca/pipeline/in_output_registrar.hpp"

namespace mpmca::pipeline::in_output {

template <typename TSIM>
class ControlCommands : public InOutput
{
  public:
    ControlCommands(Pipeline& pipeline, const std::string& name, utilities::ConfigurationPtr config);
    void PrepareTaskMessageBus(const Task& task, MessageBus&) override;
    void CheckTaskMessageBusPrepared(const Task& task, const MessageBus&) override;
    void FirstTaskCompletedMainTick(DataBucket& data_bucket);
    void FirstTaskNotCompletedMainTick(DataBucket& data_bucket);

  private:
    void Prepare() override;
    void Tick(MessageBus& in_output_message_bus) override;
    void SafeTick() override {};
    void TaskCompleted(const DataBucket& data_bucket) override;
    void MainTick(DataBucket& data_bucket) override;

    PoseVector GetFinalTransformPose(utilities::ConfigurationPtr config);
    TransformationMatrix GetFinalTransform(const PoseVector& pose_vector);

    mpmca::control::Simulator<TSIM> m_simulator;

    const int m_horizon_offset;
    const int m_command_buffer_length;

    int64_t m_data_main_tick_cycle;
    bool m_first_task_completed;

    PoseVector m_final_transform_pose;
    TransformationMatrix m_final_transform;
    typename TSIM::StateVector m_x0;
    typename TSIM::InputVector m_u0;

    std::vector<typename TSIM::StateVector> m_state_plan;
    std::vector<typename TSIM::InputVector> m_input_plan;

    std::string m_task_name;
    bool m_all_checks_okay;
};

}  //namespace mpmca::pipeline::in_output