/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/control/controller.hpp"
#include "mpmca/json_tools.hpp"
#include "mpmca/linear_algebra.hpp"
#include "mpmca/messages/controller_final_transform.hpp"
#include "mpmca/messages/controller_initial_state_and_input.hpp"
#include "mpmca/messages/controller_input_plan.hpp"
#include "mpmca/messages/controller_state_plan.hpp"
#include "mpmca/pipeline/in_output/control_commands.hpp"
#include "mpmca/pipeline/pipeline.hpp"

namespace mpmca::pipeline::in_output {

template <typename TSIM>
PoseVector ControlCommands<TSIM>::GetFinalTransformPose(utilities::ConfigurationPtr config)
{
    PoseVector final_transform_pose = PoseVector::Zero();
    final_transform_pose.GetX() = config->GetValue<double>("FinalTransformX", 0.0);
    final_transform_pose.GetY() = config->GetValue<double>("FinalTransformY", 0.0);
    final_transform_pose.GetZ() = config->GetValue<double>("FinalTransformZ", 0.0);

    return final_transform_pose;
}

template <typename TSIM>
TransformationMatrix ControlCommands<TSIM>::GetFinalTransform(const PoseVector& pose_vector)
{
    mpmca::control::Controller<TSIM> controller(1, 1, 0.01, m_simulator.GetNominalState(),
                                                m_simulator.GetNominalInput());
    controller.SetFinalTransform(pose_vector.GetX(), pose_vector.GetY(), pose_vector.GetZ(), pose_vector.GetPhi(),
                                 pose_vector.GetTheta(), pose_vector.GetPsi());
    return controller.GetFinalTransform();
}

template <typename TSIM>
ControlCommands<TSIM>::ControlCommands(Pipeline& pipeline, const std::string& name, utilities::ConfigurationPtr config)
    : InOutput(pipeline, name, "ControlCommands", config)
    , m_horizon_offset(config->GetValue<int>("HorizonOffset", 1))
    , m_command_buffer_length(pipeline.GetMainTickBufferSize())
    , m_data_main_tick_cycle(-1)
    , m_first_task_completed(false)
    , m_final_transform_pose(GetFinalTransformPose(config))
    , m_final_transform(GetFinalTransform(m_final_transform_pose))
    , m_x0(config->GetValue<bool>("InitialStateEqualsNominalState", false)
               ? m_simulator.GetNominalState()
               : config->GetValue<typename TSIM::StateVector>("InitialState", TSIM::StateVector::Zero()))
    , m_u0(TSIM::InputVector::Zero())
    , m_task_name(config->GetValue<std::string>("FromTaskName", "Task1"))
    , m_all_checks_okay{false}
{
    if (config->GetValue<bool>("InitialStateEqualsNominalState", true) && config->GetKeyExists("InitialState"))
        throw std::runtime_error(
            "If option \"InitialStateEqualsNominalState\" is true, you should not also provide "
            "option \"InitialState\".");

    m_state_plan.resize(m_command_buffer_length, m_x0);
    m_input_plan.resize(m_command_buffer_length, m_u0);

    if (m_horizon_offset < 0)
        throw std::invalid_argument(
            "The integer value for option \"HorizonOffset\" should be greater than or equal to one.");

    m_logger.Info("I will send the " + std::to_string(m_horizon_offset) +
                  "-th sample on the horizon to the plant/simulator.");
    m_logger.Info("The state plan buffer was initialized with " + mpmca::ToString(m_x0.transpose()));
    m_logger.Info("The input plan buffer was initialized with " + mpmca::ToString(m_u0.transpose()));

    m_logger.Info("The nominal output of this simulator is " +
                  mpmca::ToString(m_simulator.GetNominalOutput().transpose()));
    m_logger.Info("The nominal state of this simulator is " +
                  mpmca::ToString(m_simulator.GetNominalState().transpose()));
    m_logger.Info("The nominal input of this simulator is " +
                  mpmca::ToString(m_simulator.GetNominalInput().transpose()));
}

template <typename TSIM>
void ControlCommands<TSIM>::PrepareTaskMessageBus(const Task& task, MessageBus& message_bus)
{
    message_bus.MakeMessage<messages::ControllerInitialStateAndInput<TSIM>>();
    message_bus.MakeMessage<messages::ControllerFinalTransform>();
}

template <typename TSIM>
void ControlCommands<TSIM>::CheckTaskMessageBusPrepared(const Task& task, const MessageBus& message_bus)
{
    if (task.GetName() == m_task_name) {
        message_bus.RequireMessage<messages::ControllerStatePlan<TSIM>>();
        message_bus.RequireMessage<messages::ControllerInputPlan<TSIM>>();

        m_all_checks_okay = true;
    }
    else {
        // no messages required
    }
}

template <typename TSIM>
void ControlCommands<TSIM>::Prepare()
{
    if (!m_all_checks_okay) {
        throw std::runtime_error("Not all checks passed. Hint: does the task with name \"" + m_task_name + "\" exist?");
    }

    m_data_main_tick_cycle = -1;
    m_first_task_completed = false;

    GetSafety().Prepared();
}

template <typename TSIM>
void ControlCommands<TSIM>::Tick(MessageBus& /* in_output_message_bus */)
{
    TransitionStateMachine();
}

template <typename TSIM>
void ControlCommands<TSIM>::TaskCompleted(const DataBucket& data_bucket)
{
    if (data_bucket.GetTaskName() != m_task_name) {
        return;
    }

    if (GetStateMachineClient().GetCurrentState() == State::kIdle) {
        m_data_main_tick_cycle = data_bucket.GetMainTickCycle();
        return;
    }

    if (data_bucket.GetMainTickCycle() != m_data_main_tick_cycle + 1)
        throw std::runtime_error("Processing MainTick #" + std::to_string(data_bucket.GetMainTickCycle()) +
                                 ", but previous was #" + std::to_string(m_data_main_tick_cycle));

    m_data_main_tick_cycle = data_bucket.GetMainTickCycle();

    if ((m_horizon_offset + m_command_buffer_length) >= data_bucket.GetHorizon().GetNMpc())
        throw std::runtime_error("I cannot read sample " + std::to_string(m_horizon_offset + m_command_buffer_length) +
                                 " on the horizon, because the length of the horizon is " +
                                 std::to_string(data_bucket.GetHorizon().GetNMpc()) + ".");

    if ((m_horizon_offset + m_command_buffer_length) >= data_bucket.GetHorizon().GetNMpcFundamental())
        throw std::runtime_error("It is probably dangerous to use the " +
                                 std::to_string(m_horizon_offset + m_command_buffer_length) +
                                 "-th sample on the horizon, because the time step for this sample is not "
                                 "equal to the fundamental time step of " +
                                 std::to_string(data_bucket.GetHorizon().GetDtMs()) + " ms.");

    data_bucket.GetTaskSignalBus().ReadUpdatedMessage<messages::ControllerStatePlan<TSIM>>(
        //
        [&](const messages::ControllerStatePlan<TSIM>& message) {
            for (size_t i = 0; i < m_command_buffer_length; ++i)
                m_state_plan[i] = message.x_plan.at(i + m_horizon_offset);
        }  //
    );

    data_bucket.GetTaskSignalBus().ReadUpdatedMessage<messages::ControllerInputPlan<TSIM>>(
        //
        [&](const messages::ControllerInputPlan<TSIM>& message) {
            for (size_t i = 0; i < m_command_buffer_length; ++i)
                m_input_plan[i] = message.u_plan.at(i + m_horizon_offset);
        }  //
    );

    m_first_task_completed = true;
}

template <typename TSIM>
void ControlCommands<TSIM>::FirstTaskNotCompletedMainTick(DataBucket& data_bucket)
{
    data_bucket.GetTaskSignalBus().UpdateMessage<messages::ControllerInitialStateAndInput<TSIM>>(
        [&](messages::ControllerInitialStateAndInput<TSIM>& message) {
            message.x_initial = m_x0;
            message.u_initial = m_u0;
        });
}

template <typename TSIM>
void ControlCommands<TSIM>::FirstTaskCompletedMainTick(DataBucket& data_bucket)
{
    if (data_bucket.GetTaskName() == m_task_name) {
        const int64_t state_input_plan_offset = data_bucket.GetMainTickCycle() - (1 + m_data_main_tick_cycle);

        if (state_input_plan_offset < 0)
            throw std::runtime_error(
                "Requested data from a main tick (#" + std::to_string(data_bucket.GetMainTickCycle()) +
                ") that is smaller than the latest data update (#" + std::to_string(m_data_main_tick_cycle) + ").");

        if (state_input_plan_offset != 0)
            m_logger.Warning("The next control input was calculated " + std::to_string(state_input_plan_offset) +
                             " samples ago on main cycle " + std::to_string(data_bucket.GetMainTickCycle()) + ".");

        if (state_input_plan_offset < m_state_plan.size() && state_input_plan_offset < m_input_plan.size()) {
            data_bucket.GetTaskSignalBus().UpdateMessage<messages::ControllerInitialStateAndInput<TSIM>>(
                [&](messages::ControllerInitialStateAndInput<TSIM>& message) {
                    message.x_initial = m_state_plan[state_input_plan_offset];
                    message.u_initial = m_input_plan[state_input_plan_offset];
                });
        }
        else {
            throw std::runtime_error("The Task lags more steps (" + std::to_string(state_input_plan_offset) +
                                     ") behind than the size of my buffer (" + std::to_string(m_state_plan.size()) +
                                     ").");
        }
    }
    else {
        data_bucket.GetTaskSignalBus().UpdateMessage<messages::ControllerInitialStateAndInput<TSIM>>(
            [&](messages::ControllerInitialStateAndInput<TSIM>& message) {
                message.x_initial = m_state_plan[0];
                message.u_initial = m_input_plan[0];
            });
    }
}

template <typename TSIM>
void ControlCommands<TSIM>::MainTick(DataBucket& data_bucket)
{
    data_bucket.GetTaskSignalBus().UpdateMessage<messages::ControllerFinalTransform>(
        [&](messages::ControllerFinalTransform& message) { message.final_transform = m_final_transform; });

    if (m_first_task_completed)
        FirstTaskCompletedMainTick(data_bucket);
    else
        FirstTaskNotCompletedMainTick(data_bucket);
}
}  //namespace mpmca::pipeline::in_output