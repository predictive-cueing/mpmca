/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <map>
#include <memory>
#include <set>
#include <vector>

#include "mpmca/pipeline/state.hpp"
#include "mpmca/pipeline/state_machine_client.hpp"
#include "mpmca/pipeline/state_path.hpp"

namespace mpmca::pipeline {
class Pipeline;
class Task;
class Unit;
class StateMachine
{
  private:
    friend Pipeline;
    friend Task;
    friend Unit;

    std::vector<StateMachineClient*> m_clients;
    std::vector<StatePath> m_paths;
    std::vector<State> m_always_reachable_transition_states;
    State m_current_state;
    StatePath* m_current_state_path = nullptr;
    bool m_states_changed_on_this_tick;
    bool m_main_state_changed_on_this_tick;

    State m_entry_state;
    std::vector<State> m_exit_states;

    void AddStatePath(const StatePath& path);
    void GotoTransitionState(State transition_state);
    void DetermineStatesChangedOnThisTick();
    bool AnyClientRequestedAlwaysReachableState();
    bool ClientRequestedAlwaysReachableState(StateMachineClient* state_machine_client);
    void CheckAllClientsAreInCurrentState();
    void HandleRequestForTransitionState();
    bool StateIsAlwaysReachableState(State state) const;
    void ResetCurrentStatePath();

  protected:
    StateMachine();
    StateMachineClient MakeClient(const std::string& name);
    StateMachineClient* MakeClientPtr(const std::string& name);

  public:
    std::string ListClients() const;
    std::string GetMermaidDiagram() const;
    void PrintClients() const;
    const State& GetCurrentState() const;
    void Tick();
    void MainTick();
    bool GetStatesChangedOnThisTick() const;
    bool GetMainStateChangedOnThisTick() const;
    int GetNumberClientsNotInState(const State& state) const;
    static void TransitionStateMachineClient(StateMachineClient& client);
};

class StateMachineTester : public StateMachine
{
  public:
    StateMachineClient MakeClient(const std::string& name);
    StateMachineClient* MakeClientPtr(const std::string& name);
};
}  //namespace mpmca