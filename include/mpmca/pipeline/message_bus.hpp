/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <functional>
#include <map>
#include <memory>

#include "mpmca/pipeline/bus_message.hpp"
#include "mpmca/utilities/horizon.hpp"

namespace mpmca::pipeline {
class MessageBus
{
  private:
    std::map<uint32_t, std::shared_ptr<BusBaseMessage>> m_messages;

  public:
    MessageBus() = default;
    ~MessageBus() = default;

    void ResetUpdateStatusOnAllMessages()
    {
        for (auto& message : m_messages) {
            message.second->SetIsUpdated(false);
        }
    }

    const std::map<uint32_t, std::shared_ptr<BusBaseMessage>>& GetBusBaseMessages() const { return m_messages; }

    template <class T, class... Args>
    void MakeMessage(Args... args)
    {
        if (!HasMessage<T>()) {
            m_messages.insert({T::message_type_id, std::make_shared<BusMessage<T>>(args...)});
            CheckMessageTypeSlow<T>();
        }
        else {
            // The HasMessage<T>() function only checks T::message_type_id, which is fast, but unreliable.
            // MakeMessage is not supposed to be a fast function, so we additionally perform a slow
            // check, to make sure the typing is correct.
            CheckMessageTypeSlow<T>();
        }
    }

    template <class T>
    void CheckMessageTypeSlow() const
    {
        if (!(m_messages.at(T::message_type_id)->template CheckMessageIsTypeSlowButReliable<T>(false))) {
            throw std::runtime_error(
                "Two messages of different type, but with the same type_id, were added to this MessageBus.");
        }
    }

    template <class T>
    bool HasMessage() const
    {
        return m_messages.count(T::message_type_id) == 1;
    }

    template <class T>
    void RequireMessage() const
    {
        if (m_messages.count(T::message_type_id) != 1) {
            throw std::runtime_error("Message " + T::GetMessageName() + " is not present in the MessageBus.");
        }
    }
    template <class T>
    void RequireUpdatedMessage() const
    {
        RequireMessage<T>();
        if (!m_messages.at(T::message_type_id)->IsUpdated()) {
            throw std::runtime_error("Message " + T::GetMessageName() +
                                     " is present in the MessageBus, but was not updated before reading.");
        }
    }
    template <class T>
    bool HasUpdatedMessage() const
    {
        if (!HasMessage<T>()) {
            return false;
        }
        else {
            return m_messages.at(T::message_type_id)->IsUpdated();
        }
    }
    template <class T>
    const T& ReadUpdatedMessage() const
    {
        RequireUpdatedMessage<T>();
        CheckMessageTypeSlow<T>();
        return std::dynamic_pointer_cast<BusMessage<T>>(m_messages.at(T::message_type_id))->GetMessage();
    }
    const BusBaseMessage& GetBusBaseMessage(uint64_t message_type_id) const
    {
        if (m_messages.count(message_type_id) != 1) {
            throw std::runtime_error("Message with type number " + std::to_string(message_type_id) +
                                     " is not present in the MessageBus.");
        }
        return *(m_messages.at(message_type_id));
    }
    template <class T>
    void UpdateMessage(const std::function<void(T&)>& func)
    {
        RequireMessage<T>();
        CheckMessageTypeSlow<T>();
        func(std::dynamic_pointer_cast<BusMessage<T>>(m_messages.at(T::message_type_id))->GetMessage());
        m_messages.at(T::message_type_id)->SetIsUpdated(true);
    }
    template <class T>
    void ReadUpdatedMessage(const std::function<void(const T&)>& func) const
    {
        RequireUpdatedMessage<T>();
        CheckMessageTypeSlow<T>();
        func(std::dynamic_pointer_cast<BusMessage<T>>(m_messages.at(T::message_type_id))->GetMessage());
    }
    template <class T>
    bool ReadMessageIfUpdated(const std::function<void(const T&)>& func) const
    {
        if (HasMessage<T>() && m_messages.at(T::message_type_id)->IsUpdated()) {
            CheckMessageTypeSlow<T>();
            func(std::dynamic_pointer_cast<BusMessage<T>>(m_messages.at(T::message_type_id))->GetMessage());
            return true;
        }
        else {
            return false;
        }
    }
};
}  //namespace mpmca