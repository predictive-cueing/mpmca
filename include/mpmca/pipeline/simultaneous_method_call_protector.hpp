/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <atomic>

#include "mpmca/pipeline/safety.hpp"
#include "mpmca/utilities/prevent_heap_allocation_mixin.hpp"

namespace mpmca::pipeline {
template <typename EnumClass, EnumClass EnumVal>
class SimultaneousMethodCallProtector : public utilities::PreventHeapAllocationMixin
{
  private:
    std::atomic<bool>& m_bool_switch;
    Safety& m_safety;
    std::string m_error_message;

  public:
    SimultaneousMethodCallProtector(std::atomic<bool>& bool_switch, Safety& safety, const std::string& error_message)
        : m_bool_switch(bool_switch)
        , m_safety(safety)
        , m_error_message(error_message)
    {
        static_assert(std::is_same_v<EnumClass, SafetyLevel>);
        static_assert(EnumVal == SafetyLevel::kFail || EnumVal == SafetyLevel::kCritical);

        bool expected = false;
        if (!m_bool_switch.compare_exchange_strong(expected, true))
            throw(std::runtime_error(m_error_message));
    };

    void success()
    {
        bool expected = true;
        if (!m_bool_switch.compare_exchange_strong(expected, false))
            throw(std::runtime_error(m_error_message));
    };

    // missing implementation should produce linker error for types not explicitly defined
    ~SimultaneousMethodCallProtector();
};
}  //namespace mpmca::pipeline