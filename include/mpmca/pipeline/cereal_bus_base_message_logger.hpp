/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <cereal/archives/binary.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/unordered_map.hpp>
#include <cereal/types/vector.hpp>
#include <chrono>
#include <fstream>
#include <map>
#include <vector>

#include "mpmca/cereal_tools.hpp"
#include "mpmca/linear_algebra.hpp"
#include "mpmca/messages/message_id_macro_definition.hpp"
#include "mpmca/pipeline/bus_base_message.hpp"
#include "mpmca/pipeline/cereal_logger.hpp"
#include "mpmca/pipeline/message_bus.hpp"

namespace mpmca::pipeline {
class CerealBusBaseMessageLogger : protected CerealLogger
{
    bool m_log_all_messages;
    std::map<uint32_t, bool> m_include_message_type_id;

    std::set<std::string> m_message_type_name_to_include;
    std::set<std::string> m_message_type_name_to_exclude;

  public:
    CerealBusBaseMessageLogger(const std::string& filename, bool log_all_messages);
    void WriteBusBaseMessage(int64_t current_time_ms, const BusBaseMessage& bus_base_message);
    void WriteBusBaseMessages(int64_t current_time_ms,
                              const std::map<uint32_t, std::shared_ptr<BusBaseMessage>> bus_base_messages);

    bool IsMessageToLog(uint32_t message_type_id, const std::string& message_type_name);
    void AddMessageTypeToInclude(uint32_t message_type_id);
    void AddMessageTypeToExclude(uint32_t message_type_id);
    void AddMessageNameToInclude(const std::string& message_name);
    void AddMessageNameToExclude(const std::string& message_name);

    template <class T>
    bool IsMessageToLog()
    {
        return IsMessageToLog(T::message_type_id, T::GetMessageName());
    }
};
}  //namespace mpmca::pipeline