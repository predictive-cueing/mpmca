/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <cstdint>

namespace mpmca::cereal_types {
typedef uint32_t bus_message_type_id;
typedef int64_t time_ms_type;
typedef uint64_t memory_address_type;
}  //namespace mpmca