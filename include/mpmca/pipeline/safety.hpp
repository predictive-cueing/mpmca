/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <algorithm>
#include <iostream>
#include <memory>
#include <mutex>
#include <vector>

#include "mpmca/pipeline/safety_level.hpp"
#include "mpmca/pipeline/safety_level_tools.hpp"

namespace mpmca::pipeline {
class Pipeline;

class Safety
{
  public:
    Safety MakeChild(const std::string& name) &;
    Safety* MakeChildPtr(const std::string& name) &;
    // prevents calling MakeChild() on temporary objects:
    Safety MakeChild(const std::string& name) && = delete;
    Safety* MakeChildPtr(const std::string& name) && = delete;

    static Safety DoSomethingUnsafe(const std::string& name, bool guarded);

    bool HasParent() const;

    void Fail();
    void Critical();
    void Prepared();
    bool GetGuarded() const;
    void PrintChildren() const;
    std::string ListChildren() const;

    SafetyLevel GetLevel() const;
    std::string GetLevelName() const;
    std::string GetName() const;
    Safety(Safety&&);
    Safety& operator=(Safety&&);
    ~Safety();

  protected:
    Safety(const std::string& name, bool guarded);
    Safety(const std::string& name, Safety* parent, bool guarded);

  private:
    class Impl;
    std::unique_ptr<Impl> m_pimpl;
    friend mpmca::pipeline::Pipeline;
};
}  //namespace mpmca::pipeline