/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <iterator>
#include <vector>

#include "mpmca/control/templates/component_traits.hpp"
#include "mpmca/control/templates/type_list.hpp"

namespace mpmca::control::templates {
template <typename... Cs>
class ComponentList
{
    static_assert(all_derived_from_component<Cs...>::value,
                  "ComponentList can only include Component or derived types.");

    constexpr static auto GetName_impl();

  public:
    using ThisTypeList = TypeList<Cs...>;
    template <std::size_t i>
    using ComponentAt = typename TypeAt<i, ThisTypeList>::Type;
    enum
    {
        size = ThisTypeList::size
    };

    constexpr static auto GetName();
};

template <typename... Cs>
constexpr auto ComponentList<Cs...>::GetName_impl()
{
    std::ostringstream oss = index_apply<size>([](auto... Is) {
        std::ostringstream oss;
        ((oss << (Is == 0 ? "" : " ") << ComponentAt<Is>::GetName()), ...);
        return oss;
    });
    return oss;
}

template <typename... Cs>
constexpr auto ComponentList<Cs...>::GetName()
{
    std::ostringstream oss = GetName_impl();
    std::istringstream iss(oss.str());
    std::vector<std::string> names(std::istream_iterator<std::string>{iss}, std::istream_iterator<std::string>());
    return names;
}
}  //namespace mpmca::control::templates