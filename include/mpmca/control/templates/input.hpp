/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/control/templates/type_list.hpp"

namespace mpmca::control::templates {
template <typename TSIM, typename MT>
class InputView
{
    static_assert(is_component_list<typename TSIM::ComponentList>::value,
                  "The template parameter for InputView must be a ComponentList.");
    using d = DimensionsT<TSIM>;

  public:
    InputView(MT& u);

    template <std::size_t i>
    decltype(auto) At();
    template <std::size_t i>
    decltype(auto) At() const;

  private:
    MT& m_u;
};

template <typename TSIM, typename MT>
InputView<TSIM, MT>::InputView(MT& u)
    : m_u{u}
{
}

template <typename TSIM, typename MT>
template <std::size_t i>
decltype(auto) InputView<TSIM, MT>::At()
{
    static_assert(i < TSIM::ComponentList::size, "Component index must be smaller than the ComponentList size.");

    constexpr auto NU = d::template NU_at<i>;
    constexpr auto IDX = d::template NU_prev<i>;

    return m_u.template middleRows<NU>(IDX);
}

template <typename TSIM, typename MT>
template <std::size_t i>
decltype(auto) InputView<TSIM, MT>::At() const
{
    static_assert(i < TSIM::ComponentList::size, "Component index must be smaller than the ComponentList size.");

    constexpr auto NU = d::template NU_at<i>;
    constexpr auto IDX = d::template NU_prev<i>;

    return m_u.template middleRows<NU>(IDX);
}

}  //namespace mpmca::control::templates
