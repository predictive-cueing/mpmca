/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <iostream>
#include <type_traits>
#include <typeinfo>
#include <utility>

#include "mpmca/linear_algebra.hpp"

namespace mpmca::control::templates {

template <typename... Types>
struct TypeList
{
    TypeList(TypeList const&) = delete;
    enum
    {
        size = sizeof...(Types)
    };
};

template <std::size_t index, class TypeList>
struct TypeAt;

template <class Head, typename... Tail>
struct TypeAt<0, TypeList<Head, Tail...>>
{
    using Type = Head;
};

template <std::size_t index, class Head, typename... Tail>
struct TypeAt<index, TypeList<Head, Tail...>>
{
    using Type = typename TypeAt<index - 1, TypeList<Tail...>>::Type;
};

template <std::size_t index>
struct TypeAt<index, TypeList<>>
{
    static_assert(index < 0, "TypeAt index out of bounds");
    using Type = TypeAt;
};

template <class TypeList, class Ch, class Tr, std::size_t... Is>
void PrintTypeListImpl(std::basic_ostream<Ch, Tr>& os, std::index_sequence<Is...>)
{
    ((os << (Is == 0 ? "" : ", ") << typeid(typename TypeAt<Is, TypeList>::Type).name()), ...);
}

template <class TypeList, class Ch, class Tr>
void PrintTypeList(std::basic_ostream<Ch, Tr>& os)
{
    os << "(";
    PrintTypeListImpl<TypeList>(os, std::make_index_sequence<TypeList::size>{});
    os << ")\n";
}

template <std::size_t nr, typename Type>
struct UWrap : public Type
{
    UWrap(Type const& type)
        : Type(type)
    {
    }
};

template <std::size_t nr, typename... PolicyTypes>
struct MultiBase;

template <std::size_t nr>
struct MultiBase<nr>
{
};

template <std::size_t nr, typename PolicyT1, typename... PolicyTypes>
struct MultiBase<nr, PolicyT1, PolicyTypes...>
    : public UWrap<nr, PolicyT1>
    , public MultiBase<nr + 1, PolicyTypes...>
{
    using Type = PolicyT1;
    using Base = MultiBase<nr + 1, PolicyTypes...>;

    MultiBase(PolicyT1&& policyt1, PolicyTypes&&... policytypes)
        : UWrap<nr, PolicyT1>(std::forward<PolicyT1>(policyt1))
        , MultiBase<nr + 1, PolicyTypes...>(std::forward<PolicyTypes>(policytypes)...)
    {
    }
};

template <template <typename> class Policy, typename... Types>
struct Multi : public MultiBase<0, Policy<Types>...>
{
    using PlainTypes = TypeList<Types...>;
    using Base = MultiBase<0, Policy<Types>...>;

    enum
    {
        size = PlainTypes::size
    };

    Multi(Policy<Types>&&... types)
        : MultiBase<0, Policy<Types>...>(std::forward<Policy<Types>>(types)...)
    {
    }

    Multi()
        : MultiBase<0, Policy<Types>...>(Types()...)
    {
    }
};

template <std::size_t index_t, typename Multi>
class TypeAtt
{
    template <std::size_t index, typename MultiBase>
    struct PolType;

    template <std::size_t index, std::size_t nr, typename PolicyT1, typename... PolicyTypes>
    struct PolType<index, MultiBase<nr, PolicyT1, PolicyTypes...>>
    {
        typedef typename PolType<index - 1, MultiBase<nr + 1, PolicyTypes...>>::Type Type;
    };

    template <std::size_t nr, typename PolicyT1, typename... PolicyTypes>
    struct PolType<0, MultiBase<nr, PolicyT1, PolicyTypes...>>
    {
        typedef PolicyT1 Type;
    };

  public:
    TypeAtt(TypeAtt const&) = delete;
    typedef typename PolType<index_t, typename Multi::Base>::Type Type;
};

template <std::size_t index_t, typename Multi>
class PlainTypeAt
{
    template <std::size_t index, typename List>
    struct At;

    template <std::size_t index, typename Head, typename... Tail>
    struct At<index, TypeList<Head, Tail...>>
    {
        typedef typename At<index - 1, TypeList<Tail...>>::Type Type;
    };

    template <typename Head, typename... Tail>
    struct At<0, TypeList<Head, Tail...>>
    {
        typedef Head Type;
    };

  public:
    PlainTypeAt(PlainTypeAt const&) = delete;
    using Type = typename At<index_t, typename Multi::PlainTypes>::Type;
};

template <std::size_t index, typename Multi>
inline typename TypeAtt<index, Multi>::Type& get(Multi& multi)
{
    return static_cast<UWrap<index, typename TypeAtt<index, Multi>::Type>&>(multi);
}

template <typename Type>
struct Policy
{
    Type d_type;
    Policy(Type&& type)
        : d_type(std::forward<Type>(type))
    {
    }
};

template <class Ch, class Tr, class MultiObj, std::size_t... Is>
void print_multi_impl(std::basic_ostream<Ch, Tr>& os, MultiObj& t, std::index_sequence<Is...>)
{
    ((os << (Is == 0 ? "" : ", ") << typeid(get<Is>(t).d_type).name()), ...);
}

template <class Ch, class Tr, template <typename> class Policy, typename... Ts>
auto& operator<<(std::basic_ostream<Ch, Tr>& os, Multi<Policy, Ts...>& t)
{
    os << "(";
    print_multi_impl(os, t, std::make_index_sequence<Multi<Policy, Ts...>::size>{});
    return os << ")";
}

template <int M, int N>
void ShiftByRows(Vector<M>& v, Vector<N> const& shift)
{
    auto tmp = v.template segment<M - N>(N);
    v.template topRows<M - N>() = tmp;
    v.template bottomRows<N>() = shift;
}

template <class F, size_t... Is>
constexpr auto index_apply_impl(F f, std::index_sequence<Is...>)
{
    return f(std::integral_constant<std::size_t, Is>{}...);
}

template <std::size_t N, typename F>
constexpr auto index_apply(F f)
{
    return index_apply_impl(f, std::make_index_sequence<N>{});
}
}  //namespace mpmca::control::templates
