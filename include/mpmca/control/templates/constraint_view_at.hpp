/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

namespace mpmca::control::templates {

template <typename TSIM, typename T>
class ConstraintViewAt
{
  public:
    ConstraintViewAt(T& g, std::size_t offset){};
};

}  //namespace mpmca::control::templates