/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

namespace mpmca::control::templates {

template <typename TSIM, typename T>
class InfoViewAt
{
    T& m_info_vector;
    std::size_t m_offset;

  public:
    InfoViewAt(T& info_vector, std::size_t offset)
        : m_info_vector(info_vector)
        , m_offset(offset)
    {
    }
};

}  //namespace mpmca::control::templates
