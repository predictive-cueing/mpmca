/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <type_traits>

namespace mpmca::control::templates {
template <std::size_t NQ, std::size_t NC, std::size_t NZ, std::size_t NP, std::size_t NI, const char* NAME>
class Component;

template <typename... Cs>
struct ComponentList;

template <typename...>
using trying_to_instantiate = void;

using disregard_this = void;

template <typename, template <typename...> typename Expression, typename... Ts>
struct is_detected_impl : std::false_type
{
};

template <template <typename...> typename Expression, typename... Ts>
struct is_detected_impl<trying_to_instantiate<Expression<Ts...>>, Expression, Ts...> : std::true_type
{
};

template <template <typename...> typename Expression, typename... Ts>
using is_detected = is_detected_impl<disregard_this, Expression, Ts...>;

template <class T>
using is_derived_from_component_expr =
    std::is_base_of<Component<T::GetNumberOfAxes(), T::GetNumberOfConstraints(), T::GetNumberOfAlgStates(),
                              T::GetNumberOfParameters(), T::GetNumberOfInfos(), T::GetName()>,
                    T>;
//
template <class T>
using is_derived_from_component = is_detected<is_derived_from_component_expr, T>;

template <class... Ds>
struct all_derived_from_component;

template <class D, class... Ds>
struct all_derived_from_component<D, Ds...>
{
    static constexpr bool value = is_derived_from_component<D>::value && all_derived_from_component<Ds...>::value;
};

template <>
struct all_derived_from_component<>
{
    static constexpr bool value = true;
};

template <typename... Cs>
constexpr std::true_type is_componentlist_impl(ComponentList<Cs...> const&);

constexpr std::false_type is_componentlist_impl(...);

template <typename T>
using is_component_list = decltype(is_componentlist_impl(std::declval<T>()));
}  //namespace mpmca::control::templates
