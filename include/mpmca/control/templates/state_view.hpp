/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/control/templates/dimension.hpp"
#include "mpmca/control/templates/state_view_at.hpp"
#include "mpmca/control/templates/type_list.hpp"

namespace mpmca::control::templates {
template <typename TSIM, typename MT>
class StateView
{
    static_assert(is_component_list<typename TSIM::ComponentList>::value,
                  "The template parameter for StateView must be a ComponentList.");
    using d = DimensionsT<TSIM>;
    MT& m_state_vector;

  public:
    StateView(MT& x);

    decltype(auto) GetPosition();
    decltype(auto) GetPosition() const;
    decltype(auto) GetVelocity();
    decltype(auto) GetVelocity() const;

    template <std::size_t i>
    auto At() const;

    template <std::size_t i>
    decltype(auto) ViewAt();

    template <std::size_t i>
    decltype(auto) ViewAt() const;

    template <std::size_t i>
    decltype(auto) GetPositionAt();

    template <std::size_t i>
    decltype(auto) GetPositionAt() const;

    template <std::size_t i>
    decltype(auto) GetVelocityAt();

    template <std::size_t i>
    decltype(auto) GetVelocityAt() const;
};

template <typename TSIM, typename MT>
StateView<TSIM, MT>::StateView(MT& x)
    : m_state_vector(x)
{
}

template <typename TSIM, typename MT>
template <std::size_t i>
decltype(auto) StateView<TSIM, MT>::ViewAt()
{
    static_assert(i < TSIM::ComponentList::size, "Component index must be smaller than the ComponentList size.");

    constexpr std::size_t id_position = d::template NQ_prev<i>;
    constexpr std::size_t id_velocity = d::template NQ_prev<i> + d::NU;
    return StateViewAt<typename TSIM::ComponentList::template ComponentAt<i>, MT>(m_state_vector, id_position,
                                                                                  id_velocity);
}

template <typename TSIM, typename MT>
template <std::size_t i>
decltype(auto) StateView<TSIM, MT>::ViewAt() const
{
    static_assert(i < TSIM::ComponentList::size, "Component index must be smaller than the ComponentList size.");

    constexpr std::size_t id_position = d::template NQ_prev<i>;
    constexpr std::size_t id_velocity = d::template NQ_prev<i> + d::NU;
    return StateViewAt<typename TSIM::ComponentList::template ComponentAt<i>, MT>(m_state_vector, id_position,
                                                                                  id_velocity);
}

template <typename TSIM, typename MT>
decltype(auto) StateView<TSIM, MT>::GetPosition()
{
    return m_state_vector.template topRows<d::NQ>();
}

template <typename TSIM, typename MT>
decltype(auto) StateView<TSIM, MT>::GetPosition() const
{
    return m_state_vector.template topRows<d::NQ>();
}

template <typename TSIM, typename MT>
decltype(auto) StateView<TSIM, MT>::GetVelocity()
{
    return m_state_vector.template bottomRows<d::NQ>();
}

template <typename TSIM, typename MT>
decltype(auto) StateView<TSIM, MT>::GetVelocity() const
{
    return m_state_vector.template bottomRows<d::NQ>();
}

template <typename TSIM, typename MT>
template <std::size_t i>
auto StateView<TSIM, MT>::At() const
{
    // Concatenate position and velocities
    typename TSIM::ComponentList::template ComponentAt<i>::StateVector xi;
    xi << GetPositionAt<i>(), GetVelocityAt<i>();
    return xi;
}

template <typename TSIM, typename MT>
template <std::size_t i>
decltype(auto) StateView<TSIM, MT>::GetPositionAt()
{
    static_assert(i < TSIM::ComponentList::size, "Component index must be smaller than the ComponentList size.");

    constexpr auto NQ = d::template NQ_at<i>;
    constexpr auto IDX = d::template NQ_prev<i>;

    return m_state_vector.template middleRows<NQ>(IDX);
}

template <typename TSIM, typename MT>
template <std::size_t i>
decltype(auto) StateView<TSIM, MT>::GetPositionAt() const
{
    static_assert(i < TSIM::ComponentList::size, "Component index must be smaller than the ComponentList size.");

    constexpr auto NQ = d::template NQ_at<i>;
    constexpr auto IDX = d::template NQ_prev<i>;

    return m_state_vector.template middleRows<NQ>(IDX);
}

template <typename TSIM, typename MT>
template <std::size_t i>
decltype(auto) StateView<TSIM, MT>::GetVelocityAt()
{
    static_assert(i < TSIM::ComponentList::size, "Component index must be smaller than the ComponentList size.");

    constexpr auto NQ = d::template NQ_at<i>;
    constexpr auto IDX = d::NQ + d::template NQ_prev<i>;

    return m_state_vector.template middleRows<NQ>(IDX);
}

template <typename TSIM, typename MT>
template <std::size_t i>
decltype(auto) StateView<TSIM, MT>::GetVelocityAt() const
{
    static_assert(i < TSIM::ComponentList::size, "Component index must be smaller than the ComponentList size.");

    constexpr auto NQ = d::template NQ_at<i>;
    constexpr auto IDX = d::NQ + d::template NQ_prev<i>;

    return m_state_vector.template middleRows<NQ>(IDX);
}

}  //namespace mpmca::control::templates
