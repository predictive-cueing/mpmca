/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/control/templates/dimension.hpp"
#include "mpmca/control/templates/info_view_at.hpp"
#include "mpmca/control/templates/type_list.hpp"

namespace mpmca::control::templates {

template <typename TSIM, typename T>
class InfoView
{
    static_assert(is_component_list<typename TSIM::ComponentList>::value,
                  "The template parameter for InfoView must be a ComponentList.");
    using d = DimensionsT<TSIM>;
    T& m_info_vector;

  public:
    InfoView(T& info);

    template <std::size_t i>
    decltype(auto) At();
    template <std::size_t i>
    decltype(auto) At() const;
    template <std::size_t i>
    decltype(auto) ViewAt();
    template <std::size_t i>
    decltype(auto) ViewAt() const;
};

template <typename TSIM, typename T>
InfoView<TSIM, T>::InfoView(T& info)
    : m_info_vector{info}
{
}

template <typename TSIM, typename T>
template <std::size_t i>
decltype(auto) InfoView<TSIM, T>::At()
{
    static_assert(i < TSIM::ComponentList::size, "Component index must be smaller than the ComponentList size.");

    constexpr auto NI = d::template NI_at<i>;
    constexpr auto IDX = d::template NI_prev<i>;

    return m_info_vector.template middleRows<NI>(IDX);
}

template <typename TSIM, typename T>
template <std::size_t i>
decltype(auto) InfoView<TSIM, T>::At() const
{
    static_assert(i < TSIM::ComponentList::size, "Component index must be smaller than the ComponentList size.");

    constexpr auto NI = d::template NI_at<i>;
    constexpr auto IDX = d::template NI_prev<i>;

    return m_info_vector.template middleRows<NI>(IDX);
}

template <typename TSIM, typename T>
template <std::size_t i>
decltype(auto) InfoView<TSIM, T>::ViewAt()
{
    static_assert(i < TSIM::ComponentList::size, "Component index must be smaller than the ComponentList size.");

    constexpr auto IDX = d::template NI_prev<i>;
    return InfoViewAt<typename TSIM::ComponentList::template ComponentAt<i>, T>(m_info_vector, IDX);
}

template <typename TSIM, typename T>
template <std::size_t i>
decltype(auto) InfoView<TSIM, T>::ViewAt() const
{
    static_assert(i < TSIM::ComponentList::size, "Component index must be smaller than the ComponentList size.");

    constexpr auto IDX = d::template NI_prev<i>;
    return InfoViewAt<typename TSIM::ComponentList::template ComponentAt<i>, T>(m_info_vector, IDX);
}
}  //namespace mpmca::control::templates