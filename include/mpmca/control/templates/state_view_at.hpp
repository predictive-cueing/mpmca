/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

namespace mpmca::control::templates {

template <typename TSIM, typename T>
class StateViewAt
{
    T& m_state_vector;
    std::size_t m_position_offset;
    std::size_t m_velocity_offset;

  public:
    StateViewAt(T& state_vector, std::size_t position_offset, std::size_t velocity_offset)
        : m_state_vector(state_vector)
        , m_position_offset(position_offset)
        , m_velocity_offset(velocity_offset)
    {
    }
};

}  //namespace mpmca::control::templates
