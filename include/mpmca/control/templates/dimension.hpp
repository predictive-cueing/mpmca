/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <cstddef>

#include "mpmca/control/templates/component_traits.hpp"
#include "mpmca/control/templates/type_list.hpp"

namespace mpmca::control::templates {
template <typename TSIM>
class DimensionsT
{
    static_assert(is_component_list<typename TSIM::ComponentList>::value,
                  "The template parameter for Dimensions must be a ComponentList.");

    static constexpr std::size_t GetNumberOfAxes();
    static constexpr std::size_t GetNumberOfStates();
    static constexpr std::size_t GetNumberOfAlgStates();
    static constexpr std::size_t GetNumberOfInputs();
    static constexpr std::size_t GetNumberOfConstraints();
    static constexpr std::size_t GetNumberOfParameters();
    static constexpr std::size_t GetNumberOfInfos();
    static constexpr std::size_t GetNumberOfOutputs();
    template <std::size_t i>
    static constexpr std::size_t GetNumberOfAxesAt();
    template <std::size_t i>
    static constexpr std::size_t GetNumberOfStatesAt();
    template <std::size_t i>
    static constexpr std::size_t GetNumberOfAlgStatesAt();
    template <std::size_t i>
    static constexpr std::size_t GetNumberOfInputsAt();
    template <std::size_t i>
    static constexpr std::size_t GetNumberOfConstraintsAt();
    template <std::size_t i>
    static constexpr std::size_t GetNumberOfParametersAt();
    template <std::size_t i>
    static constexpr std::size_t GetNumberOfInfosAt();
    template <std::size_t i>
    static constexpr std::size_t GetNumberOfAxesPrev();
    template <std::size_t i>
    static constexpr std::size_t GetNumberOfStatesPrev();
    template <std::size_t i>
    static constexpr std::size_t GetNumberOfAlgStatesPrev();
    template <std::size_t i>
    static constexpr std::size_t GetNumberOfInputsPrev();
    template <std::size_t i>
    static constexpr std::size_t GetNumberOfConstraintsPrev();
    template <std::size_t i>
    static constexpr std::size_t GetNumberOfParametersPrev();
    template <std::size_t i>
    static constexpr std::size_t GetNumberOfInfosPrev();

  public:
    static constexpr std::size_t NQ = GetNumberOfAxes();
    static constexpr std::size_t NX = GetNumberOfStates();
    static constexpr std::size_t NZ = GetNumberOfAlgStates();
    static constexpr std::size_t NU = GetNumberOfInputs();
    static constexpr std::size_t NC = GetNumberOfConstraints();
    static constexpr std::size_t NP = GetNumberOfParameters();
    static constexpr std::size_t NI = GetNumberOfInfos();
    static constexpr std::size_t NY = GetNumberOfOutputs();
    template <std::size_t i>
    static constexpr std::size_t NQ_at = GetNumberOfAxesAt<i>();
    template <std::size_t i>
    static constexpr std::size_t NX_at = GetNumberOfStatesAt<i>();
    template <std::size_t i>
    static constexpr std::size_t NZ_at = GetNumberOfAlgStatesAt<i>();
    template <std::size_t i>
    static constexpr std::size_t NU_at = GetNumberOfInputsAt<i>();
    template <std::size_t i>
    static constexpr std::size_t NC_at = GetNumberOfConstraintsAt<i>();
    template <std::size_t i>
    static constexpr std::size_t NP_at = GetNumberOfParametersAt<i>();
    template <std::size_t i>
    static constexpr std::size_t NI_at = GetNumberOfInfosAt<i>();
    template <std::size_t i>
    static constexpr std::size_t NQ_prev = GetNumberOfAxesPrev<i>();
    template <std::size_t i>
    static constexpr std::size_t NX_prev = GetNumberOfStatesPrev<i>();
    template <std::size_t i>
    static constexpr std::size_t NZ_prev = GetNumberOfAlgStatesPrev<i>();
    template <std::size_t i>
    static constexpr std::size_t NU_prev = GetNumberOfInputsPrev<i>();
    template <std::size_t i>
    static constexpr std::size_t NC_prev = GetNumberOfConstraintsPrev<i>();
    template <std::size_t i>
    static constexpr std::size_t NP_prev = GetNumberOfParametersPrev<i>();
    template <std::size_t i>
    static constexpr std::size_t NI_prev = GetNumberOfInfosPrev<i>();
};

template <typename TSIM>
template <std::size_t i>
constexpr std::size_t DimensionsT<TSIM>::GetNumberOfAxesPrev()
{
    constexpr std::size_t NQ = index_apply<i>([](auto... Is) constexpr {
        std::size_t NQ = 0;
        ((NQ += TSIM::ComponentList::template ComponentAt<Is>::GetNumberOfAxes()), ...);
        return NQ;
    });
    return NQ;
}

template <typename TSIM>
template <std::size_t i>
constexpr std::size_t DimensionsT<TSIM>::GetNumberOfStatesPrev()
{
    constexpr std::size_t NX = index_apply<i>([](auto... Is) constexpr {
        std::size_t NX = 0;
        ((NX += TSIM::ComponentList::template ComponentAt<Is>::GetNumberOfStates()), ...);
        return NX;
    });
    return NX;
}

template <typename TSIM>
template <std::size_t i>
constexpr std::size_t DimensionsT<TSIM>::GetNumberOfAlgStatesPrev()
{
    constexpr std::size_t NZ = index_apply<i>([](auto... Is) constexpr {
        std::size_t NZ = 0;
        ((NZ += TSIM::ComponentList::template ComponentAt<Is>::GetNumberOfAlgStates()), ...);
        return NZ;
    });
    return NZ;
}

template <typename TSIM>
template <std::size_t i>
constexpr std::size_t DimensionsT<TSIM>::GetNumberOfInputsPrev()
{
    constexpr std::size_t NU = index_apply<i>([](auto... Is) constexpr {
        std::size_t NU = 0;
        ((NU += TSIM::ComponentList::template ComponentAt<Is>::GetNumberOfInputs()), ...);
        return NU;
    });
    return NU;
}

template <typename TSIM>
template <std::size_t i>
constexpr std::size_t DimensionsT<TSIM>::GetNumberOfConstraintsPrev()
{
    constexpr std::size_t NC = index_apply<i>([](auto... Is) constexpr {
        std::size_t NC = 0;
        ((NC += TSIM::ComponentList::template ComponentAt<Is>::GetNumberOfConstraints()), ...);
        return NC;
    });
    return NC;
}

template <typename TSIM>
template <std::size_t i>
constexpr std::size_t DimensionsT<TSIM>::GetNumberOfParametersPrev()
{
    constexpr std::size_t NP = index_apply<i>([](auto... Is) constexpr {
        std::size_t NP = 0;
        ((NP += TSIM::ComponentList::template ComponentAt<Is>::GetNumberOfParameters()), ...);
        return NP;
    });
    return NP;
}

template <typename TSIM>
template <std::size_t i>
constexpr std::size_t DimensionsT<TSIM>::GetNumberOfInfosPrev()
{
    constexpr std::size_t NI = index_apply<i>([](auto... Is) constexpr {
        std::size_t NI = 0;
        ((NI += TSIM::ComponentList::template ComponentAt<Is>::GetNumberOfInfos()), ...);
        return NI;
    });
    return NI;
}

template <typename TSIM>
constexpr std::size_t DimensionsT<TSIM>::GetNumberOfAxes()
{
    return GetNumberOfAxesPrev<TSIM::ComponentList::size>();
}

template <typename TSIM>
constexpr std::size_t DimensionsT<TSIM>::GetNumberOfStates()
{
    return GetNumberOfStatesPrev<TSIM::ComponentList::size>();
}

template <typename TSIM>
constexpr std::size_t DimensionsT<TSIM>::GetNumberOfAlgStates()
{
    return GetNumberOfAlgStatesPrev<TSIM::ComponentList::size>();
}

template <typename TSIM>
constexpr std::size_t DimensionsT<TSIM>::GetNumberOfInputs()
{
    return GetNumberOfInputsPrev<TSIM::ComponentList::size>();
}

template <typename TSIM>
constexpr std::size_t DimensionsT<TSIM>::GetNumberOfConstraints()
{
    return GetNumberOfConstraintsPrev<TSIM::ComponentList::size>();
}

template <typename TSIM>
constexpr std::size_t DimensionsT<TSIM>::GetNumberOfParameters()
{
    return GetNumberOfParametersPrev<TSIM::ComponentList::size>();
}

template <typename TSIM>
constexpr std::size_t DimensionsT<TSIM>::GetNumberOfInfos()
{
    return GetNumberOfInfosPrev<TSIM::ComponentList::size>();
}

template <typename TSIM>
constexpr std::size_t DimensionsT<TSIM>::GetNumberOfOutputs()
{
    return TSIM::NY_SIMULATOR;
}

template <typename TSIM>
template <std::size_t i>
constexpr std::size_t DimensionsT<TSIM>::GetNumberOfAxesAt()
{
    return TSIM::ComponentList::template ComponentAt<i>::GetNumberOfAxes();
}

template <typename TSIM>
template <std::size_t i>
constexpr std::size_t DimensionsT<TSIM>::GetNumberOfStatesAt()
{
    return TSIM::ComponentList::template ComponentAt<i>::GetNumberOfStates();
}

template <typename TSIM>
template <std::size_t i>
constexpr std::size_t DimensionsT<TSIM>::GetNumberOfAlgStatesAt()
{
    return TSIM::ComponentList::template ComponentAt<i>::GetNumberOfAlgStates();
}

template <typename TSIM>
template <std::size_t i>
constexpr std::size_t DimensionsT<TSIM>::GetNumberOfInputsAt()
{
    return TSIM::ComponentList::template ComponentAt<i>::GetNumberOfInputs();
}

template <typename TSIM>
template <std::size_t i>
constexpr std::size_t DimensionsT<TSIM>::GetNumberOfConstraintsAt()
{
    return TSIM::ComponentList::template ComponentAt<i>::GetNumberOfConstraints();
}

template <typename TSIM>
template <std::size_t i>
constexpr std::size_t DimensionsT<TSIM>::GetNumberOfParametersAt()
{
    return TSIM::ComponentList::template ComponentAt<i>::GetNumberOfParameters();
}

template <typename TSIM>
template <std::size_t i>
constexpr std::size_t DimensionsT<TSIM>::GetNumberOfInfosAt()
{
    return TSIM::ComponentList::template ComponentAt<i>::GetNumberOfInfos();
}
}  //namespace mpmca::control::templates
