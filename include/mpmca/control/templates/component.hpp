/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <limits>

#include "mpmca/linear_algebra.hpp"

namespace mpmca::control::templates {

template <std::size_t NQ, std::size_t NC, std::size_t NZ, std::size_t NP, std::size_t NI, const char* NAME>
class Component
{
    enum
    {
        nq = NQ,
        nx = 2 * NQ,
        nu = NQ,
        nc = NC,
        nz = NZ,
        np = NP,
        ni = NI
    };

    static constexpr const char* name = NAME;

    Vector<2 * NQ> m_x0;
    Vector<NQ> m_u0;
    Vector<NC> m_c_lb;
    Vector<NC> m_c_ub;

  public:
    static constexpr std::size_t GetNumberOfAxes();
    static constexpr std::size_t GetNumberOfStates();
    static constexpr std::size_t GetNumberOfAlgStates();
    static constexpr std::size_t GetNumberOfInputs();
    static constexpr std::size_t GetNumberOfConstraints();
    static constexpr std::size_t GetNumberOfParameters();
    static constexpr std::size_t GetNumberOfInfos();

    static constexpr const char* GetName() { return name; }

    using AxesVector = Vector<NQ>;
    using StateVector = Vector<2 * NQ>;
    using InputVector = Vector<NQ>;
    using ConstraintVector = Vector<NC>;
    using ParameterVector = Vector<NP>;
    using AlgStateVector = Vector<NZ>;
    using InfoVector = Vector<NI>;

    Component();
    Component(StateVector const& x0, InputVector const& u0, ConstraintVector const& lbc, ConstraintVector const& ubc);

    StateVector const& GetDefaultState() noexcept;
    AxesVector GetDefaultStatePosition() noexcept;
    AxesVector GetDefaultStateVelocity() noexcept;
    InputVector const& GetDefaultInput() noexcept;
    ConstraintVector const& GetDefaultConstraintLowerBound() noexcept;
    ConstraintVector const& GetDefaultConstraintUpperBound() noexcept;

    void SetDefaultState(StateVector const&) noexcept;
    void SetDefaultInput(InputVector const&) noexcept;
    void SetDefaultConstraintLowerBound(ConstraintVector const&) noexcept;
    void SetDefaultConstraintUpperBound(ConstraintVector const&) noexcept;

    decltype(auto) StateViewPosition(StateVector& x) const;
    decltype(auto) StateViewPosition(StateVector const& x) const;
    decltype(auto) StateViewVelocity(StateVector& x) const;
    decltype(auto) StateViewVelocity(StateVector const& x) const;
};

template <std::size_t NQ, std::size_t NC, std::size_t NZ, std::size_t NP, std::size_t NI, const char* NAME>
Component<NQ, NC, NZ, NP, NI, NAME>::Component()
    : m_x0{StateVector::Zero()}
    , m_u0{InputVector::Zero()}
    , m_c_lb{ConstraintVector::Constant(-std::numeric_limits<double>::infinity())}
    , m_c_ub{ConstraintVector::Constant(std::numeric_limits<double>::infinity())}
{
}

template <std::size_t NQ, std::size_t NC, std::size_t NZ, std::size_t NP, std::size_t NI, const char* NAME>
Component<NQ, NC, NZ, NP, NI, NAME>::Component(StateVector const& x0, InputVector const& u0,
                                               ConstraintVector const& lbc, ConstraintVector const& ubc)
    : m_x0{x0}
    , m_u0{u0}
    , m_c_lb{lbc}
    , m_c_ub{ubc}
{
}

template <std::size_t NQ, std::size_t NC, std::size_t NZ, std::size_t NP, std::size_t NI, const char* NAME>
constexpr std::size_t Component<NQ, NC, NZ, NP, NI, NAME>::GetNumberOfAxes()
{
    return nq;
}

template <std::size_t NQ, std::size_t NC, std::size_t NZ, std::size_t NP, std::size_t NI, const char* NAME>
constexpr std::size_t Component<NQ, NC, NZ, NP, NI, NAME>::GetNumberOfStates()
{
    return nx;
}

template <std::size_t NQ, std::size_t NC, std::size_t NZ, std::size_t NP, std::size_t NI, const char* NAME>
constexpr std::size_t Component<NQ, NC, NZ, NP, NI, NAME>::GetNumberOfAlgStates()
{
    return nz;
}

template <std::size_t NQ, std::size_t NC, std::size_t NZ, std::size_t NP, std::size_t NI, const char* NAME>
constexpr std::size_t Component<NQ, NC, NZ, NP, NI, NAME>::GetNumberOfInputs()
{
    return nu;
}

template <std::size_t NQ, std::size_t NC, std::size_t NZ, std::size_t NP, std::size_t NI, const char* NAME>
constexpr std::size_t Component<NQ, NC, NZ, NP, NI, NAME>::GetNumberOfConstraints()
{
    return nc;
}

template <std::size_t NQ, std::size_t NC, std::size_t NZ, std::size_t NP, std::size_t NI, const char* NAME>
constexpr std::size_t Component<NQ, NC, NZ, NP, NI, NAME>::GetNumberOfParameters()
{
    return np;
}

template <std::size_t NQ, std::size_t NC, std::size_t NZ, std::size_t NP, std::size_t NI, const char* NAME>
constexpr std::size_t Component<NQ, NC, NZ, NP, NI, NAME>::GetNumberOfInfos()
{
    return ni;
}

template <std::size_t NQ, std::size_t NC, std::size_t NZ, std::size_t NP, std::size_t NI, const char* NAME>
Vector<2 * NQ> const& Component<NQ, NC, NZ, NP, NI, NAME>::GetDefaultState() noexcept
{
    return m_x0;
}

template <std::size_t NQ, std::size_t NC, std::size_t NZ, std::size_t NP, std::size_t NI, const char* NAME>
typename Component<NQ, NC, NZ, NP, NI, NAME>::AxesVector
Component<NQ, NC, NZ, NP, NI, NAME>::GetDefaultStatePosition() noexcept
{
    return Component<NQ, NC, NZ, NP, NI, NAME>::StateViewPosition(m_x0);
}

template <std::size_t NQ, std::size_t NC, std::size_t NZ, std::size_t NP, std::size_t NI, const char* NAME>
typename Component<NQ, NC, NZ, NP, NI, NAME>::AxesVector
Component<NQ, NC, NZ, NP, NI, NAME>::GetDefaultStateVelocity() noexcept
{
    return Component<NQ, NC, NZ, NP, NI, NAME>::StateViewVelocity(m_x0);
}

template <std::size_t NQ, std::size_t NC, std::size_t NZ, std::size_t NP, std::size_t NI, const char* NAME>
Vector<NQ> const& Component<NQ, NC, NZ, NP, NI, NAME>::GetDefaultInput() noexcept
{
    return m_u0;
}

template <std::size_t NQ, std::size_t NC, std::size_t NZ, std::size_t NP, std::size_t NI, const char* NAME>
Vector<NC> const& Component<NQ, NC, NZ, NP, NI, NAME>::GetDefaultConstraintLowerBound() noexcept
{
    return m_c_lb;
}

template <std::size_t NQ, std::size_t NC, std::size_t NZ, std::size_t NP, std::size_t NI, const char* NAME>
Vector<NC> const& Component<NQ, NC, NZ, NP, NI, NAME>::GetDefaultConstraintUpperBound() noexcept
{
    return m_c_ub;
}

template <std::size_t NQ, std::size_t NC, std::size_t NZ, std::size_t NP, std::size_t NI, const char* NAME>
void Component<NQ, NC, NZ, NP, NI, NAME>::SetDefaultState(Vector<2 * NQ> const& x0) noexcept
{
    m_x0 = x0;
}

template <std::size_t NQ, std::size_t NC, std::size_t NZ, std::size_t NP, std::size_t NI, const char* NAME>
void Component<NQ, NC, NZ, NP, NI, NAME>::SetDefaultInput(Vector<NQ> const& u0) noexcept
{
    m_u0 = u0;
}

template <std::size_t NQ, std::size_t NC, std::size_t NZ, std::size_t NP, std::size_t NI, const char* NAME>
void Component<NQ, NC, NZ, NP, NI, NAME>::SetDefaultConstraintLowerBound(Vector<NC> const& lbc) noexcept
{
    m_c_lb = lbc;
}

template <std::size_t NQ, std::size_t NC, std::size_t NZ, std::size_t NP, std::size_t NI, const char* NAME>
void Component<NQ, NC, NZ, NP, NI, NAME>::SetDefaultConstraintUpperBound(Vector<NC> const& ubc) noexcept
{
    m_c_ub = ubc;
}

template <std::size_t NQ, std::size_t NC, std::size_t NZ, std::size_t NP, std::size_t NI, const char* NAME>
decltype(auto) Component<NQ, NC, NZ, NP, NI, NAME>::StateViewPosition(StateVector& x) const
{
    return x.template topRows<nq>();
}

template <std::size_t NQ, std::size_t NC, std::size_t NZ, std::size_t NP, std::size_t NI, const char* NAME>
decltype(auto) Component<NQ, NC, NZ, NP, NI, NAME>::StateViewPosition(StateVector const& x) const
{
    return x.template topRows<nq>();
}

template <std::size_t NQ, std::size_t NC, std::size_t NZ, std::size_t NP, std::size_t NI, const char* NAME>
decltype(auto) Component<NQ, NC, NZ, NP, NI, NAME>::StateViewVelocity(StateVector& x) const
{
    return x.template bottomRows<nq>();
}

template <std::size_t NQ, std::size_t NC, std::size_t NZ, std::size_t NP, std::size_t NI, const char* NAME>
decltype(auto) Component<NQ, NC, NZ, NP, NI, NAME>::StateViewVelocity(StateVector const& x) const
{
    return x.template bottomRows<nq>();
}

}  //namespace mpmca::control::templates