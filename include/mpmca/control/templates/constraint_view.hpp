/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/control/templates/component.hpp"
#include "mpmca/control/templates/component_list.hpp"
#include "mpmca/control/templates/constraint_view_at.hpp"
#include "mpmca/control/templates/dimension.hpp"
#include "mpmca/control/templates/type_list.hpp"

namespace mpmca::control::templates {

template <typename TSIM, typename T>
class ConstraintView
{
    static_assert(is_component_list<typename TSIM::ComponentList>::value,
                  "The template parameter for ConstraintView must be a ComponentList.");
    using d = DimensionsT<TSIM>;
    T& m_constraint_vector;

  public:
    ConstraintView(T& g);

    template <std::size_t i>
    decltype(auto) At();
    template <std::size_t i>
    decltype(auto) At() const;
    template <std::size_t i>
    decltype(auto) ViewAt();
    template <std::size_t i>
    decltype(auto) ViewAt() const;
};

template <typename TSIM, typename T>
ConstraintView<TSIM, T>::ConstraintView(T& constraint_vector)
    : m_constraint_vector{constraint_vector}
{
}

template <typename TSIM, typename T>
template <std::size_t i>
decltype(auto) ConstraintView<TSIM, T>::At()
{
    static_assert(i < TSIM::ComponentList::size, "Component index must be smaller than the ComponentList size.");

    constexpr auto NC = d::template NC_at<i>;
    constexpr auto IDX = d::template NC_prev<i>;

    return m_constraint_vector.template middleRows<NC>(IDX);
}

template <typename TSIM, typename T>
template <std::size_t i>
decltype(auto) ConstraintView<TSIM, T>::At() const
{
    static_assert(i < TSIM::ComponentList::size, "Component index must be smaller than the ComponentList size.");

    constexpr auto NC = d::template NC_at<i>;
    constexpr auto IDX = d::template NC_prev<i>;

    return m_constraint_vector.template middleRows<NC>(IDX);
}

template <typename TSIM, typename T>
template <std::size_t i>
decltype(auto) ConstraintView<TSIM, T>::ViewAt()
{
    static_assert(i < TSIM::ComponentList::size, "Component index must be smaller than the ComponentList size.");

    constexpr auto IDX = d::template NC_prev<i>;
    return ConstraintViewAt<typename TSIM::ComponentList::template ComponentAt<i>, T>(m_constraint_vector, IDX);
}

template <typename TSIM, typename T>
template <std::size_t i>
decltype(auto) ConstraintView<TSIM, T>::ViewAt() const
{
    static_assert(i < TSIM::ComponentList::size, "Component index must be smaller than the ComponentList size.");

    constexpr auto IDX = d::template NC_prev<i>;
    return ConstraintViewAt<typename TSIM::ComponentList::template ComponentAt<i>, T>(m_constraint_vector, IDX);
}

}  //namespace mpmca::control::templates
