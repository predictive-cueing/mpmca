/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <memory>
#include <tmpc/EigenKernel.hpp>
#include <tmpc/mpc/MpcOcpSize.hpp>
#include <tmpc/mpc/MpcTrajectory.hpp>
#include <tmpc/ocp/OcpSize.hpp>
#include <tmpc/print/Printing.hpp>
#include <tmpc/qp/HpmpcWorkspace.hpp>

#include "mpmca/control/controller_options.hpp"
#include "mpmca/control/hessian_calculation_method.hpp"
#include "mpmca/control/ocp.hpp"
#include "mpmca/control/ocp_data.hpp"
#include "mpmca/control/state_stoppability_opt.hpp"

namespace mpmca::control {

template <class TSIM, StateStoppabilityOption STOP_OPT = StateStoppabilityOption::kNone,
          HessianCalculationMethod HM = HessianCalculationMethod::kGaussNewton>
class Controller
{
  private:
    Controller(double dt, const std::vector<unsigned>& mpc_steps, typename TSIM::StateVector const& x0,
               typename TSIM::InputVector const& u0, IntegrationMethod integration_method, double alpha,
               ControllerOptions<TSIM> const& options);

  public:
    Controller(const std::vector<unsigned>& mpc_steps, double dt_cost, typename TSIM::StateVector const& x0,
               typename TSIM::InputVector const& u0, ControllerOptions<TSIM> const& opt = ControllerOptions<TSIM>());
    Controller(unsigned horizon_length, unsigned horizon_step, double horizon_dt, typename TSIM::StateVector const& x0,
               typename TSIM::InputVector const& u0, ControllerOptions<TSIM> const& opt = ControllerOptions<TSIM>());
    Controller(Controller&& rhs) = default;

    ~Controller() = default;

    double GetMpcSampleTime(unsigned k) const noexcept;
    unsigned GetMpcStep(unsigned k) const noexcept;
    unsigned GetMpcHorizonLength() const noexcept;

    double GetIntegrationSampleTime(unsigned k) const noexcept;
    unsigned GetIntegrationPoints(unsigned k) const noexcept;
    unsigned GetIntegrationHorizon() const noexcept;

    typename TSIM::OutputVector const& GetReferenceOutput(unsigned k) const;
    typename TSIM::StateVector const& GetReferenceState(unsigned k) const;
    typename TSIM::StateVector const& GetTerminalReferenceState() const;

    void SetReferenceState(typename TSIM::StateVector const& x_ref);
    void SetReferenceState(unsigned k, typename TSIM::StateVector const& x_ref);
    void SetReferenceOutput(unsigned k, typename TSIM::OutputVector const& y);
    void SetTerminalReferenceState(typename TSIM::StateVector const& x_n_ref);

    typename TSIM::StateVector const& GetStateLowerBound() const;
    typename TSIM::StateVector const& GetStateUpperBound() const;
    typename TSIM::InputVector const& GetInputLowerBound() const;
    typename TSIM::InputVector const& GetInputUpperBound() const;
    typename TSIM::ConstraintVector const& GetConstraintLowerBound() const;
    typename TSIM::ConstraintVector const& GetConstraintUpperBound() const;
    typename TSIM::StateVector const& GetTerminalStateLowerBound() const;
    typename TSIM::StateVector const& GetTerminalStateUpperBound() const;

    void SetStateBounds(typename TSIM::StateVector const& x_min, typename TSIM::StateVector const& x_max);
    void SetInputBounds(typename TSIM::InputVector const& u_min, typename TSIM::InputVector const& u_max);
    void SetConstraintBounds(typename TSIM::ConstraintVector const& lbg, typename TSIM::ConstraintVector const& ubg);
    void SetTerminalStateBounds(typename TSIM::StateVector const& x_n_min, typename TSIM::StateVector const& x_n_max);
    void SetTerminalVelocityLimits(typename TSIM::AxesVector const& v_min, typename TSIM::AxesVector const& v_max);

    typename TSIM::InputVector const& GetInputErrorWeightAt(unsigned k) const;
    typename TSIM::StateVector const& GetStateErrorWeightAt(unsigned k) const;
    typename TSIM::StateVector const& GetTerminalStateErrorWeight() const;
    typename TSIM::OutputVector const& GetOutputErrorWeightAt(unsigned k) const;

    void SetInputErrorWeight(typename TSIM::InputVector const& wu);
    void SetInputErrorWeight(std::vector<typename TSIM::InputVector> const& wu_vec);
    void SetInputErrorWeight(unsigned k, typename TSIM::InputVector const& wu_k);

    void SetStateErrorWeight(typename TSIM::StateVector const& wx);
    void SetStateErrorWeight(std::vector<typename TSIM::StateVector> const& wx_vec);
    void SetStateErrorWeight(unsigned k, typename TSIM::StateVector const& wx_k);

    void SetOutputErrorWeight(typename TSIM::OutputVector const& wy);
    void SetOutputErrorWeight(std::vector<typename TSIM::OutputVector> const& wy_vec);
    void SetOutputErrorWeight(unsigned k, typename TSIM::OutputVector const& wy_k);

    void SetTerminalStateErrorWeight(typename TSIM::StateVector const& w_x_n);

    TransformationMatrix const& GetFinalTransform() const;
    void SetFinalTransform(TransformationMatrix const& val);
    void SetFinalTransform(double x, double y, double z, double roll, double pitch, double yaw);

    void SetTighteningHorizon(size_t n);
    void SetBoundsTightening(double val);
    size_t GetTighteningHorizon();
    double GetBoundsTightening();

    void SetLevenbergMarquardt(double val);
    double GetLevenbergMarquardt() const noexcept;

    void SetScalingFactorForStoppabilityBounds(double alpha);
    double GetScalingFactorForStoppabilityBounds() const noexcept;

    void Preparation();

    typename TSIM::InputVector Feedback(typename TSIM::StateVector const& x0);

    void SetWorkingPoint(typename TSIM::StateVector const& x, typename TSIM::InputVector const& u);

    typename TSIM::InputVector GetInputPlan(unsigned k) const noexcept;
    typename TSIM::StateVector GetStatePlan(unsigned k) const noexcept;

    unsigned GetNumIter() const noexcept;

    void PrintQp(std::ostream& os) const;

    typename TSIM::StateMatrix GetStatePlan() const;
    typename TSIM::InputMatrix GetInputPlan() const;
    typename TSIM::OutputMatrix GetReferenceOutput() const;

  private:
    template <class T>
    using MatrixAligned = std::vector<T, Eigen::aligned_allocator<T>>;
    using OcpPoint = tmpc::OcpPoint<tmpc::EigenKernel<double>>;
    using OcpProblem = OCP<TSIM, STOP_OPT, HM>;

    static std::vector<tmpc::OcpSize> MakeSize(size_t nt);

    template <typename T>
    T CalculateBoundsShrinking(T const& lb, T const& ub, size_t k);

    void PrepareStage(size_t index);
    void PrepareFinalStage();

    OcpData<TSIM> m_ocp_data;
    OcpProblem m_ocp_problem;

    MatrixAligned<typename TSIM::StateStateMatrix> m_a_matrix;
    MatrixAligned<typename TSIM::StateInputMatrix> m_b_matrix;

    double m_bounds_tightening;
    size_t m_tightening_horizon;

    typename TSIM::StateVector m_xn_min;
    typename TSIM::StateVector m_xn_max;
    typename TSIM::StateVector m_x_n_ref;

    typename TSIM::StateStateMatrix m_w_x_n;
    typename TSIM::StateVector m_w_x_n_vec;

    tmpc::HpmpcWorkspace<tmpc::EigenKernel<double>> m_qp;

    std::vector<OcpPoint> m_working_point;

    std::vector<typename TSIM::InputVector> m_lagrange_multiplier_lower_bound_input;
    std::vector<typename TSIM::InputVector> m_lagrange_multiplier_upper_bound_input;
    std::vector<typename TSIM::StateVector> m_lagrange_multiplier_lower_bound_state;
    std::vector<typename TSIM::StateVector> m_lagrange_multiplier_upper_bound_state;
    std::vector<typename OcpProblem::ConstraintVector> m_lagrange_multiplier_lower_bound_constraint;
    std::vector<typename OcpProblem::ConstraintVector> m_lagrange_multiplier_upper_bound_constraint;

    bool m_prepared;
};

static void SetQpSolverOptions(tmpc::HpmpcWorkspace<tmpc::EigenKernel<double>>& ws)
{
    ws.maxIter(100);
    ws.muTol(1.e-6);
}

}  //namespace mpmca::control

#include "mpmca/control/controller.tpp"
