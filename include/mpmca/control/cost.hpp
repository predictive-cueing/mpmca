/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/control/from_tmpc/casadi_interface/generated_function.hpp"
#include "mpmca/control/hessian_calculation_method.hpp"
#include "mpmca/control/ocp_data.hpp"
#include "mpmca/control/state.hpp"
#include "mpmca/control/state_stoppability_opt.hpp"
#include "mpmca/linear_algebra.hpp"

namespace mpmca::control {

template <typename TSIM, HessianCalculationMethod HM>
class Cost
{
    casadi_interface::GeneratedFunction m_cost_function;
    casadi_interface::GeneratedFunction m_cost_jacobian_function;
    casadi_interface::GeneratedFunction m_cost_hessian_full_function;
    casadi_interface::GeneratedFunction m_cost_hessian_gauss_newton;

  public:
    Cost();

    double GetCostDt(typename TSIM::StateVector const& x, typename TSIM::InputVector const& u,
                     typename TSIM::OutputVector const& y_ref, typename TSIM::StateVector const& x_ref,
                     TransformationMatrix const& T_HP, typename TSIM::StateVector const& wx,
                     typename TSIM::InputVector const& wu, typename TSIM::OutputVector const& wy) const;

    void GetCostJacobianDt(typename TSIM::StateVector const& x, typename TSIM::InputVector const& u,
                           typename TSIM::OutputVector const& y_ref, typename TSIM::StateVector const& x_ref,
                           TransformationMatrix const& T_HP, typename TSIM::StateVector const& wx,
                           typename TSIM::InputVector const& wu, typename TSIM::OutputVector const& wy,
                           typename TSIM::StateVector& dJ_dx, typename TSIM::InputVector& dJ_du) const;

    void GetCostHessianDt(typename TSIM::StateVector const& x, typename TSIM::InputVector const& u,
                          typename TSIM::OutputVector const& y_ref, typename TSIM::StateVector const& x_ref,
                          TransformationMatrix const& T_HP, typename TSIM::StateVector const& wx,
                          typename TSIM::InputVector const& wu, typename TSIM::OutputVector const& wy,
                          typename TSIM::StateVector& dJ_dx, typename TSIM::InputVector& dJ_du,
                          typename TSIM::StateStateMatrix& d2J_dx2, typename TSIM::StateInputMatrix& d2J_dx_duT,
                          typename TSIM::InputInputMatrix& d2J_du2) const;
};

template <typename TSIM, HessianCalculationMethod HM>
Cost<TSIM, HM>::Cost()
    : m_cost_function(TSIM::Cost_functions())
    , m_cost_jacobian_function(TSIM::CostJacobian_functions())
    , m_cost_hessian_full_function(TSIM::CostHessian_functions())
    , m_cost_hessian_gauss_newton(TSIM::LagrangeHessianGaussNewton_functions())
{
}

template <typename TSIM, HessianCalculationMethod HM>
double Cost<TSIM, HM>::GetCostDt(typename TSIM::StateVector const& x, typename TSIM::InputVector const& u,
                                 typename TSIM::OutputVector const& y_ref, typename TSIM::StateVector const& x_ref,
                                 TransformationMatrix const& T_HP, typename TSIM::StateVector const& wx,
                                 typename TSIM::InputVector const& wu, typename TSIM::OutputVector const& wy) const
{
    double J;
    m_cost_function({x.data(), nullptr, u.data(), nullptr, T_HP.data(), x_ref.data(), y_ref.data(), wx.data(),
                     wu.data(), wy.data()},
                    {&J});

    return J;
}

template <typename TSIM, HessianCalculationMethod HM>
void Cost<TSIM, HM>::GetCostJacobianDt(typename TSIM::StateVector const& x, typename TSIM::InputVector const& u,
                                       typename TSIM::OutputVector const& y_ref,
                                       typename TSIM::StateVector const& x_ref, TransformationMatrix const& T_HP,
                                       typename TSIM::StateVector const& wx, typename TSIM::InputVector const& wu,
                                       typename TSIM::OutputVector const& wy, typename TSIM::StateVector& dJ_dx,
                                       typename TSIM::InputVector& dJ_du) const
{
    m_cost_jacobian_function({x.data(), nullptr, u.data(), nullptr, T_HP.data(), x_ref.data(), y_ref.data(), wx.data(),
                              wu.data(), wy.data()},
                             {dJ_dx.data(), dJ_du.data()});
}

template <typename TSIM, HessianCalculationMethod HM>
void Cost<TSIM, HM>::GetCostHessianDt(typename TSIM::StateVector const& x, typename TSIM::InputVector const& u,
                                      typename TSIM::OutputVector const& y_ref, typename TSIM::StateVector const& x_ref,
                                      TransformationMatrix const& T_HP, typename TSIM::StateVector const& wx,
                                      typename TSIM::InputVector const& wu, typename TSIM::OutputVector const& wy,
                                      typename TSIM::StateVector& dJ_dx, typename TSIM::InputVector& dJ_du,
                                      typename TSIM::StateStateMatrix& d2J_dx2,
                                      typename TSIM::StateInputMatrix& d2J_dx_duT,
                                      typename TSIM::InputInputMatrix& d2J_du2) const
{
    if constexpr (HM == HessianCalculationMethod::kGaussNewton) {
        m_cost_hessian_gauss_newton({x.data(), nullptr, u.data(), nullptr, T_HP.data(), x_ref.data(), y_ref.data(),
                                     wx.data(), wu.data(), wy.data()},
                                    {dJ_dx.data(), dJ_du.data(), d2J_dx2.data(), d2J_dx_duT.data(), d2J_du2.data()});
    }
    else if constexpr (HM == HessianCalculationMethod::kFull) {
        m_cost_hessian_full_function({x.data(), nullptr, u.data(), nullptr, T_HP.data(), x_ref.data(), y_ref.data(),
                                      wx.data(), wu.data(), wy.data()},
                                     {dJ_dx.data(), dJ_du.data(), d2J_dx2.data(), d2J_dx_duT.data(), d2J_du2.data()});
    }
}

}  //namespace mpmca::control