/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/control/components.hpp"
#include "mpmca/control/templates/constraint_view.hpp"

namespace mpmca::control {

template <typename TSIM, typename T>
inline auto MakeConstraintView(T& g)
{
    return templates::ConstraintView<TSIM, T>{g};
}
}  //namespace mpmca::control

#include "mpmca/control/hexapod_constraint.tpp"
