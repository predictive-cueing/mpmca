/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <algorithm>
#include <eigen3/Eigen/Dense>
#include <nlohmann/json.hpp>
#include <sstream>

namespace Eigen {
template <typename VT>
void from_json(nlohmann::json const& j, PlainObjectBase<VT>& v)
{
    using Scalar = typename VT::Scalar;

    v.resize(j.size());
    std::transform(j.begin(), j.end(), v.data(), [](nlohmann::json const& val) -> Scalar {
        if (val.is_string()) {
            static constexpr Scalar inf = std::numeric_limits<Scalar>::infinity();

            if (val == "-inf")
                return -inf;
            else if (val == "inf")
                return inf;
            else {
                std::ostringstream message;
                message << "Invalid value in config file: a double number, a \"inf\" or a \"-inf\" was expected, but \""
                        << val << "\" found.";
                throw std::invalid_argument(message.str());
            }
        }
        else
            return val;
    });
}

template <typename T, int Rows, int Cols, int Options>
void from_json(nlohmann::json const& j, Matrix<T, Rows, Cols, Options>& v)
{
    using Scalar = T;

    if (v.size() != j.size())
        throw std::invalid_argument("Number of elements in json value does not match matrix size");

    int ii = 0, jj = 0;
    for (auto const& val : j) {
        T numeric_value = std::numeric_limits<Scalar>::signaling_NaN();

        if (val.is_string()) {
            static constexpr Scalar inf = std::numeric_limits<Scalar>::infinity();

            if (val == "-inf")
                numeric_value = -inf;
            else if (val == "inf")
                numeric_value = inf;
            else {
                std::ostringstream message;
                message << "Invalid value in config file: a number, a \"inf\" or a \"-inf\" was expected, but \"" << val
                        << "\" found.";
                throw std::invalid_argument(message.str());
            }
        }
        else
            numeric_value = val;

        v(ii, jj) = numeric_value;

        if (++jj >= v.cols()) {
            jj = 0;
            ++ii;
        }
    }
}
}  //namespace Eigen