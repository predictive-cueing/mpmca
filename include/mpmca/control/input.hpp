/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/control/templates/input.hpp"

namespace mpmca::control {

template <typename TSIM, typename T>
inline auto MakeInputView(T& info)
{
    return templates::InputView<TSIM, T>{info};
}
}  //namespace mpmca::control
