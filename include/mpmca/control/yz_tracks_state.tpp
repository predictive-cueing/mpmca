/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/control/components.hpp"
#include "mpmca/control/templates/state_view.hpp"
#include "mpmca/control/templates/state_view_at.hpp"

namespace mpmca::control::templates {

template <typename T>
class StateViewAt<components::YzTracks, T>
{
    T& m_state_vector;
    std::size_t m_position_offset;
    std::size_t m_velocity_offset;

  public:
    StateViewAt(T& state_vector, std::size_t position_offset, std::size_t velocity_offset);

    double GetY();
    double GetY() const;
    double GetZ();
    double GetZ() const;

    double GetYd();
    double GetYd() const;
    double GetZd();
    double GetZd() const;
};

template <typename T>
StateViewAt<components::YzTracks, T>::StateViewAt(T& state_vector, std::size_t position_offset,
                                                  std::size_t velocity_offset)
    : m_state_vector(state_vector)
    , m_position_offset(position_offset)
    , m_velocity_offset(velocity_offset)
{
}
template <typename T>
double StateViewAt<components::YzTracks, T>::GetY()
{
    return m_state_vector(m_position_offset + 0);
}
template <typename T>
double StateViewAt<components::YzTracks, T>::GetY() const
{
    return m_state_vector(m_position_offset + 0);
}
template <typename T>
double StateViewAt<components::YzTracks, T>::GetZ()
{
    return m_state_vector(m_position_offset + 1);
}
template <typename T>
double StateViewAt<components::YzTracks, T>::GetZ() const
{
    return m_state_vector(m_position_offset + 1);
}
template <typename T>
double StateViewAt<components::YzTracks, T>::GetYd()
{
    return m_state_vector(m_velocity_offset + 0);
}
template <typename T>
double StateViewAt<components::YzTracks, T>::GetYd() const
{
    return m_state_vector(m_velocity_offset + 0);
}
template <typename T>
double StateViewAt<components::YzTracks, T>::GetZd()
{
    return m_state_vector(m_velocity_offset + 1);
}
template <typename T>
double StateViewAt<components::YzTracks, T>::GetZd() const
{
    return m_state_vector(m_velocity_offset + 1);
}

}  //namespace mpmca::control