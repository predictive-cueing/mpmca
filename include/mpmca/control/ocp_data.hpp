/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <numeric>

#include "mpmca/control/controller_options.hpp"

namespace mpmca::control {
enum class IntegrationMethod
{
    kConstantXY,
    kVariableXY
};

template <typename TSIM>
class OcpData
{
    double m_dt;
    std::vector<unsigned> m_mpc_steps;
    std::vector<double> m_mpc_sample_times;
    std::size_t m_mpc_horizon_length;

    std::vector<unsigned> m_number_of_integration_points_at_mpc_index;
    std::vector<double> m_integration_sample_times;
    std::vector<unsigned> m_first_integration_point_at_mpc_index;
    std::size_t m_integration_horizon;

    typename TSIM::StateVector m_state_lower_bound;
    typename TSIM::StateVector m_state_upper_bound;
    typename TSIM::InputVector m_input_lower_bound;
    typename TSIM::InputVector m_input_upper_bound;
    typename TSIM::ConstraintVector m_constraint_lower_bound;
    typename TSIM::ConstraintVector m_constraint_upper_bound;

    std::vector<typename TSIM::OutputVector> m_output_error_weight;
    std::vector<typename TSIM::StateVector> m_state_error_weight;
    std::vector<typename TSIM::InputVector> m_input_error_weight;

    double m_alpha;

    double m_levenberg_marquardt;

    TransformationMatrix m_final_transform;

    std::vector<typename TSIM::StateVector> m_x_ref;
    std::vector<typename TSIM::OutputVector> m_y_ref;

    void InitMpcTimeVectors();
    void InitIntegrationTimeVectors(IntegrationMethod integration_method);
    void InitReferenceSignals(ControllerOptions<TSIM> const& options);

  public:
    using IterOutput = typename std::vector<typename TSIM::OutputVector>::const_iterator;
    using IterState = typename std::vector<typename TSIM::StateVector>::const_iterator;

    OcpData(double dt, std::vector<unsigned> const& mpc_steps, IntegrationMethod integration_method,
            ControllerOptions<TSIM> const& options);
    OcpData(double dt, unsigned mpc_horizon, unsigned mpc_step_k, IntegrationMethod integration_method,
            ControllerOptions<TSIM> const& options);

    double GetControlSampleTime() const noexcept;
    unsigned GetMpcHorizonLength() const noexcept;
    std::vector<unsigned> const& GetMpcSteps() const noexcept;
    unsigned GetMpcStepAt(unsigned index) const;
    double GetMpcSampleTimeAt(unsigned index) const;

    unsigned GetTotalNumberOfIntegrationIntervals() const noexcept;
    double GetIntegrationSampleTimeAt(unsigned index) const;
    unsigned GetNumberOfIntegrationIntervalsAt(unsigned index) const;

    typename TSIM::StateVector const& GetStateLowerBound() const noexcept;
    typename TSIM::StateVector const& GetStateUpperBound() const noexcept;
    typename TSIM::InputVector const& GetInputLowerBound() const noexcept;
    typename TSIM::InputVector const& GetInputUpperBound() const noexcept;
    typename TSIM::ConstraintVector const& GetLowerBoundConstraints() const noexcept;
    typename TSIM::ConstraintVector const& GetUpperBoundConstraints() const noexcept;

    void SetLowerBoundState(typename TSIM::StateVector const& x_min) noexcept;
    void SetUpperBoundState(typename TSIM::StateVector const& x_max) noexcept;
    void SetLowerBoundInput(typename TSIM::InputVector const& u_min) noexcept;
    void SetUpperBoundInput(typename TSIM::InputVector const& u_max) noexcept;
    void SetLowerBoundConstraints(typename TSIM::ConstraintVector const& g_min) noexcept;
    void SetUpperBoundConstraints(typename TSIM::ConstraintVector const& g_max) noexcept;

    typename TSIM::StateVector const& GetStateErrorWeightAt(unsigned k) const;
    typename TSIM::InputVector const& GetInputErrorWeightAt(unsigned k) const;
    typename TSIM::OutputVector const& GetOutputErrorWeightAt(unsigned k) const;

    void SetStateErrorWeight(typename TSIM::StateVector const& w_x_fixed) noexcept;
    void SetStateErrorWeight(std::vector<typename TSIM::StateVector> const& w_x_all) noexcept;
    void SetStateErrorWeightAt(unsigned k, typename TSIM::StateVector const& w_x_k);

    void SetInputErrorWeight(typename TSIM::InputVector const& w_u_fixed) noexcept;
    void SetInputErrorWeight(std::vector<typename TSIM::InputVector> const& w_u_all) noexcept;
    void SetInputErrorWeightAt(unsigned k, typename TSIM::InputVector const& w_u_k);

    void SetOutputErrorWeight(typename TSIM::OutputVector const& w_y_fixed) noexcept;
    void SetOutputErrorWeight(std::vector<typename TSIM::OutputVector> const& w_y_all) noexcept;
    void SetOutputErrorWeightAt(unsigned k, typename TSIM::OutputVector const& w_y_k);

    double GetLevenbergMarquardt() const noexcept;
    void SetLevenbergMarquardt(double levenberg_marquardt) noexcept;

    double GetAlpha() const noexcept;
    void SetAlpha(double alpha) noexcept;

    TransformationMatrix const& GetFinalTransform() const noexcept;
    void SetFinalTransform(TransformationMatrix const& T_HP) noexcept;

    void GetReferenceStatesAt(unsigned k, IterState& x_begin, IterState& x_end) const;
    void GetReferenceOutputsAt(unsigned k, IterOutput& y_begin, IterOutput& y_end) const;

    typename TSIM::StateVector const& GetReferenceStateAtIntegrationIndex(unsigned k) const;
    typename TSIM::OutputVector const& GetReferenceOutputAtIntegrationIndex(unsigned k) const;

    void SetReferenceState(typename TSIM::StateVector const& x_ref);
    void SetReferenceStateAtIntegrationIndex(unsigned k, typename TSIM::StateVector const& x_ref_k);

    void SetReferenceOutput(typename TSIM::OutputVector const& y_ref_fixed);
    void SetReferenceOutputAtIntegrationIndex(unsigned k, typename TSIM::OutputVector const& y_ref_k);
};

template <typename TSIM>
OcpData<TSIM>::OcpData(double dt, std::vector<unsigned> const& mpc_steps, IntegrationMethod integration_method,
                       ControllerOptions<TSIM> const& options)
    : m_dt{dt}
    , m_mpc_steps{mpc_steps}
    , m_mpc_sample_times(mpc_steps.size())
    , m_mpc_horizon_length{mpc_steps.size()}
    , m_number_of_integration_points_at_mpc_index(mpc_steps.size())
    , m_integration_sample_times(mpc_steps.size())
    , m_first_integration_point_at_mpc_index(mpc_steps.size())
    , m_state_lower_bound{options.lower_bound_state}
    , m_state_upper_bound{options.upper_bound_state}
    , m_input_lower_bound{options.lower_bound_input}
    , m_input_upper_bound{options.upper_bound_input}
    , m_constraint_lower_bound{options.lower_bound_constraint}
    , m_constraint_upper_bound{options.upper_bound_constraint}
    , m_output_error_weight(mpc_steps.size(), options.output_error_weight)
    , m_state_error_weight(mpc_steps.size(), options.state_error_weight)
    , m_input_error_weight(mpc_steps.size(), options.input_error_weight)
    , m_alpha{1.}
    , m_levenberg_marquardt{0.}
    , m_final_transform{options.final_transform}
{
    InitMpcTimeVectors();
    InitIntegrationTimeVectors(integration_method);
    InitReferenceSignals(options);
}

template <typename TSIM>
void OcpData<TSIM>::InitMpcTimeVectors()
{
    std::transform(m_mpc_steps.begin(), m_mpc_steps.end(), m_mpc_sample_times.begin(),
                   [dt = m_dt](double x) { return x * dt; });
}

template <typename TSIM>
void OcpData<TSIM>::InitIntegrationTimeVectors(IntegrationMethod integration_method)
{
    if (integration_method == IntegrationMethod::kConstantXY) {
        m_integration_sample_times = m_mpc_sample_times;
        std::fill(m_number_of_integration_points_at_mpc_index.begin(),
                  m_number_of_integration_points_at_mpc_index.end(), 1);
    }
    else  //variableXY
    {
        std::fill(m_integration_sample_times.begin(), m_integration_sample_times.end(), m_dt);
        m_number_of_integration_points_at_mpc_index = m_mpc_steps;
    }

    m_first_integration_point_at_mpc_index.at(0) = 0;
    std::partial_sum(m_number_of_integration_points_at_mpc_index.cbegin(),
                     m_number_of_integration_points_at_mpc_index.cend() - 1,
                     m_first_integration_point_at_mpc_index.begin() + 1);

    m_integration_horizon =
        m_first_integration_point_at_mpc_index.back() + m_number_of_integration_points_at_mpc_index.back();
}

template <typename TSIM>
void OcpData<TSIM>::InitReferenceSignals(ControllerOptions<TSIM> const& options)
{
    m_x_ref.insert(m_x_ref.end(), m_integration_horizon, options.reference_state);
    m_y_ref.insert(m_y_ref.end(), m_integration_horizon, options.reference_output);
}

template <typename TSIM>
OcpData<TSIM>::OcpData(double dt, unsigned mpc_horizon, unsigned mpc_step_k, IntegrationMethod integration_method,
                       ControllerOptions<TSIM> const& options)
    : OcpData(dt, std::vector<unsigned>(mpc_horizon, mpc_step_k), integration_method, options)
{
}

template <typename TSIM>
double OcpData<TSIM>::GetControlSampleTime() const noexcept
{
    return m_dt;
}

template <typename TSIM>
unsigned OcpData<TSIM>::GetMpcHorizonLength() const noexcept
{
    return m_mpc_horizon_length;
}

template <typename TSIM>
std::vector<unsigned> const& OcpData<TSIM>::GetMpcSteps() const noexcept
{
    return m_mpc_steps;
}

template <typename TSIM>
unsigned OcpData<TSIM>::GetMpcStepAt(unsigned index) const
{
    return m_mpc_steps.at(index);
}

template <typename TSIM>
double OcpData<TSIM>::GetMpcSampleTimeAt(unsigned index) const
{
    return m_mpc_sample_times.at(index);
}

template <typename TSIM>
unsigned OcpData<TSIM>::GetTotalNumberOfIntegrationIntervals() const noexcept
{
    return m_integration_horizon;
}

template <typename TSIM>
double OcpData<TSIM>::GetIntegrationSampleTimeAt(unsigned index) const
{
    return m_integration_sample_times.at(index);
}

template <typename TSIM>
unsigned OcpData<TSIM>::GetNumberOfIntegrationIntervalsAt(unsigned index) const
{
    return m_number_of_integration_points_at_mpc_index.at(index);
}

template <typename TSIM>
typename TSIM::StateVector const& OcpData<TSIM>::GetStateLowerBound() const noexcept
{
    return m_state_lower_bound;
}

template <typename TSIM>
typename TSIM::StateVector const& OcpData<TSIM>::GetStateUpperBound() const noexcept
{
    return m_state_upper_bound;
}

template <typename TSIM>
typename TSIM::InputVector const& OcpData<TSIM>::GetInputLowerBound() const noexcept
{
    return m_input_lower_bound;
}

template <typename TSIM>
typename TSIM::InputVector const& OcpData<TSIM>::GetInputUpperBound() const noexcept
{
    return m_input_upper_bound;
}

template <typename TSIM>
typename TSIM::ConstraintVector const& OcpData<TSIM>::GetLowerBoundConstraints() const noexcept
{
    return m_constraint_lower_bound;
}

template <typename TSIM>
typename TSIM::ConstraintVector const& OcpData<TSIM>::GetUpperBoundConstraints() const noexcept
{
    return m_constraint_upper_bound;
}

template <typename TSIM>
void OcpData<TSIM>::SetLowerBoundState(typename TSIM::StateVector const& x_min) noexcept
{
    m_state_lower_bound = x_min;
}

template <typename TSIM>
void OcpData<TSIM>::SetUpperBoundState(typename TSIM::StateVector const& x_max) noexcept
{
    m_state_upper_bound = x_max;
}

template <typename TSIM>
void OcpData<TSIM>::SetLowerBoundInput(typename TSIM::InputVector const& u_min) noexcept
{
    m_input_lower_bound = u_min;
}

template <typename TSIM>
void OcpData<TSIM>::SetUpperBoundInput(typename TSIM::InputVector const& u_max) noexcept
{
    m_input_upper_bound = u_max;
}

template <typename TSIM>
void OcpData<TSIM>::SetLowerBoundConstraints(typename TSIM::ConstraintVector const& g_min) noexcept
{
    m_constraint_lower_bound = g_min;
}

template <typename TSIM>
void OcpData<TSIM>::SetUpperBoundConstraints(typename TSIM::ConstraintVector const& g_max) noexcept
{
    m_constraint_upper_bound = g_max;
}

template <typename TSIM>
typename TSIM::StateVector const& OcpData<TSIM>::GetStateErrorWeightAt(unsigned k) const
{
    return m_state_error_weight.at(k);
}

template <typename TSIM>
typename TSIM::InputVector const& OcpData<TSIM>::GetInputErrorWeightAt(unsigned k) const
{
    return m_input_error_weight.at(k);
}

template <typename TSIM>
typename TSIM::OutputVector const& OcpData<TSIM>::GetOutputErrorWeightAt(unsigned k) const
{
    return m_output_error_weight.at(k);
}

template <typename TSIM>
void OcpData<TSIM>::SetStateErrorWeight(typename TSIM::StateVector const& w_x_fixed) noexcept
{
    std::fill(m_state_error_weight.begin(), m_state_error_weight.end(), w_x_fixed);
}

template <typename TSIM>
void OcpData<TSIM>::SetStateErrorWeight(std::vector<typename TSIM::StateVector> const& w_x_all) noexcept
{
    m_state_error_weight = w_x_all;
}

template <typename TSIM>
void OcpData<TSIM>::SetStateErrorWeightAt(unsigned k, typename TSIM::StateVector const& w_x_k)
{
    m_state_error_weight.at(k) = w_x_k;
}

template <typename TSIM>
void OcpData<TSIM>::SetInputErrorWeight(typename TSIM::InputVector const& w_u_fixed) noexcept
{
    std::fill(m_input_error_weight.begin(), m_input_error_weight.end(), w_u_fixed);
}

template <typename TSIM>
void OcpData<TSIM>::SetInputErrorWeight(std::vector<typename TSIM::InputVector> const& w_u_all) noexcept
{
    m_input_error_weight = w_u_all;
}

template <typename TSIM>
void OcpData<TSIM>::SetInputErrorWeightAt(unsigned k, typename TSIM::InputVector const& w_u_k)
{
    m_input_error_weight.at(k) = w_u_k;
}

template <typename TSIM>
void OcpData<TSIM>::SetOutputErrorWeight(typename TSIM::OutputVector const& w_y_fixed) noexcept
{
    std::fill(m_output_error_weight.begin(), m_output_error_weight.end(), w_y_fixed);
}

template <typename TSIM>
void OcpData<TSIM>::SetOutputErrorWeight(std::vector<typename TSIM::OutputVector> const& w_y_all) noexcept
{
    m_output_error_weight = w_y_all;
}

template <typename TSIM>
void OcpData<TSIM>::SetOutputErrorWeightAt(unsigned k, typename TSIM::OutputVector const& w_y_k)
{
    m_output_error_weight.at(k) = w_y_k;
}

template <typename TSIM>
double OcpData<TSIM>::GetLevenbergMarquardt() const noexcept
{
    return m_levenberg_marquardt;
}

template <typename TSIM>
void OcpData<TSIM>::SetLevenbergMarquardt(double levenberg_marquardt) noexcept
{
    m_levenberg_marquardt = levenberg_marquardt;
}

template <typename TSIM>
double OcpData<TSIM>::GetAlpha() const noexcept
{
    return m_alpha;
}

template <typename TSIM>
void OcpData<TSIM>::SetAlpha(double alpha) noexcept
{
    m_alpha = alpha;
}

template <typename TSIM>
TransformationMatrix const& OcpData<TSIM>::GetFinalTransform() const noexcept
{
    return m_final_transform;
}

template <typename TSIM>
void OcpData<TSIM>::SetFinalTransform(TransformationMatrix const& T_HP) noexcept
{
    m_final_transform = T_HP;
}

template <typename TSIM>
void OcpData<TSIM>::GetReferenceStatesAt(unsigned k, IterState& x_begin, IterState& x_end) const
{
    unsigned k_begin = m_first_integration_point_at_mpc_index.at(k);
    unsigned k_end = m_first_integration_point_at_mpc_index.at(k) + m_number_of_integration_points_at_mpc_index.at(k);

    x_begin = m_x_ref.cbegin() + k_begin;
    x_end = m_x_ref.cbegin() + k_end;
}

template <typename TSIM>
void OcpData<TSIM>::GetReferenceOutputsAt(unsigned k, IterOutput& y_begin, IterOutput& y_end) const
{
    unsigned k_begin = m_first_integration_point_at_mpc_index.at(k);
    unsigned k_end = m_first_integration_point_at_mpc_index.at(k) + m_number_of_integration_points_at_mpc_index.at(k);

    y_begin = m_y_ref.cbegin() + k_begin;
    y_end = m_y_ref.cbegin() + k_end;
}

template <typename TSIM>
typename TSIM::StateVector const& OcpData<TSIM>::GetReferenceStateAtIntegrationIndex(unsigned k) const
{
    return m_x_ref.at(k);
}
template <typename TSIM>
typename TSIM::OutputVector const& OcpData<TSIM>::GetReferenceOutputAtIntegrationIndex(unsigned k) const
{
    return m_y_ref.at(k);
}

template <typename TSIM>
void OcpData<TSIM>::SetReferenceState(typename TSIM::StateVector const& x_ref_fixed)
{
    std::fill(m_x_ref.begin(), m_x_ref.end(), x_ref_fixed);
}

template <typename TSIM>
void OcpData<TSIM>::SetReferenceStateAtIntegrationIndex(unsigned k, typename TSIM::StateVector const& x_ref_k)
{
    m_x_ref.at(k) = x_ref_k;
}

template <typename TSIM>
void OcpData<TSIM>::SetReferenceOutput(typename TSIM::OutputVector const& y_ref_fixed)
{
    std::fill(m_y_ref.begin(), m_y_ref.end(), y_ref_fixed);
}

template <typename TSIM>
void OcpData<TSIM>::SetReferenceOutputAtIntegrationIndex(unsigned k, typename TSIM::OutputVector const& y_ref_k)
{
    m_y_ref.at(k) = y_ref_k;
}
}  //namespace mpmca::control