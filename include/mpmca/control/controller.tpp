/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <cmath>

#include "mpmca/control/controller.hpp"
namespace mpmca::control {

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
Controller<TSIM, STOP_OPT, HM>::Controller(const std::vector<unsigned>& mpc_steps, double dt,
                                           typename TSIM::StateVector const& x0, typename TSIM::InputVector const& u0,
                                           ControllerOptions<TSIM> const& options)
    : Controller<TSIM, STOP_OPT, HM>(
          dt, mpc_steps, x0, u0,
          options.integration_method == "constantXY" ? IntegrationMethod::kConstantXY : IntegrationMethod::kVariableXY,
          options.scaling_factor_for_stoppability_bounds, options)
{
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
Controller<TSIM, STOP_OPT, HM>::Controller(unsigned mpc_horizon, unsigned mpc_step, double dt,
                                           typename TSIM::StateVector const& x0, typename TSIM::InputVector const& u0,
                                           ControllerOptions<TSIM> const& options)
    : Controller<TSIM, STOP_OPT, HM>(std::vector<unsigned>(mpc_horizon, mpc_step), dt, x0, u0, options)
{
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
std::vector<tmpc::OcpSize> Controller<TSIM, STOP_OPT, HM>::MakeSize(size_t nt)
{
    std::vector<tmpc::OcpSize> sz;
    sz.reserve(nt + 1);

    for (size_t k = 0; k < nt; ++k) {
        sz.emplace_back(TSIM::Dimension::NX, TSIM::Dimension::NU, OcpProblem::NC);
    }

    sz.emplace_back(TSIM::Dimension::NX, 0, 0);

    return sz;
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
Controller<TSIM, STOP_OPT, HM>::Controller(double dt, const std::vector<unsigned>& mpc_steps,
                                           typename TSIM::StateVector const& x0, typename TSIM::InputVector const& u0,
                                           IntegrationMethod integration_method, double alpha,
                                           ControllerOptions<TSIM> const& options)
    : m_ocp_data{dt, mpc_steps, integration_method, options}
    , m_a_matrix(mpc_steps.size() + 1)
    , m_b_matrix(mpc_steps.size() + 1)
    , m_bounds_tightening{options.bounds_tightening}
    , m_tightening_horizon{options.tightening_horizon}
    , m_xn_min{options.lower_bound_terminal_state}
    , m_xn_max{options.upper_bound_terminal_state}
    , m_x_n_ref{options.reference_state}
    , m_qp{MakeSize(mpc_steps.size())}
    , m_prepared{false}
{
    m_ocp_data.SetAlpha(alpha);

    auto const I = Matrix<TSIM::Dimension::NQ, TSIM::Dimension::NQ>::Identity();
    auto const O = Matrix<TSIM::Dimension::NQ, TSIM::Dimension::NQ>::Zero();
    for (auto k = 0; k < m_ocp_data.GetMpcHorizonLength(); k++) {
        auto it_A_mpc = m_a_matrix.begin() + k;
        auto it_B_mpc = m_b_matrix.begin() + k;
        auto const ts = m_ocp_data.GetMpcSampleTimeAt(k);

        (*it_A_mpc) << I, ts * I, O, I;

        (*it_B_mpc) << ts * ts / 2. * I, ts * I;
    }

    SetTerminalStateErrorWeight(options.terminal_state_error_weight);

    SetWorkingPoint(x0, u0);
    SetQpSolverOptions(m_qp);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void Controller<TSIM, STOP_OPT, HM>::Preparation()
{
    for (size_t k = 0; k < m_ocp_data.GetMpcHorizonLength(); ++k) {
        PrepareStage(k);
    }

    PrepareFinalStage();
    m_prepared = true;
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void Controller<TSIM, STOP_OPT, HM>::PrepareStage(size_t index)
{
    auto const qp = m_qp.problem();
    auto const stage = qp.begin() + index;
    auto const p = m_working_point.cbegin() + index;
    auto const A_mpc = m_a_matrix.cbegin() + index;
    auto const B_mpc = m_b_matrix.cbegin() + index;
    auto const p_lam_lbc_k = m_lagrange_multiplier_lower_bound_constraint.cbegin() + index;
    auto const p_lam_ubc_k = m_lagrange_multiplier_upper_bound_constraint.cbegin() + index;

    auto const state_bounds_shrunken =
        CalculateBoundsShrinking(m_ocp_data.GetStateLowerBound(), m_ocp_data.GetStateUpperBound(), index);

    stage->relativeStateBounds(p->x(), m_ocp_data.GetStateLowerBound() + state_bounds_shrunken / 2.,
                               m_ocp_data.GetStateUpperBound() - state_bounds_shrunken / 2.);

    stage->relativeInputBounds(p->u(), m_ocp_data.GetInputLowerBound(), m_ocp_data.GetInputUpperBound());

    typename TSIM::StateVector f = (*A_mpc) * p->x() + (*B_mpc) * p->u();
    stage->linearizedShootingEquality(f, *A_mpc, *B_mpc, p[1].x());

    typename TSIM::StateVector cost_a;
    typename TSIM::InputVector cost_b;
    typename TSIM::StateStateMatrix cost_q;
    typename TSIM::InputInputMatrix cost_r;
    typename TSIM::StateInputMatrix cost_s;

    m_ocp_problem.CalculateCostHessian(index, p->x(), p->u(), m_ocp_data, *p_lam_lbc_k, *p_lam_ubc_k, cost_a, cost_b,
                                       cost_q, cost_s, cost_r);

    stage->Q(cost_q);
    stage->R(cost_r);
    stage->S(cost_s);
    stage->q(cost_a);
    stage->r(cost_b);

    typename OcpProblem::ConstraintVector cg, lbg, ubg;
    typename OcpProblem::ConstraintStateMatrix c_C;
    typename OcpProblem::ConstraintInputMatrix c_D;
    m_ocp_problem.CalculateConstraintJacobian(p->x(), p->u(), m_ocp_data, cg, c_C, c_D);
    m_ocp_problem.CalculateConstraintBounds(p->x(), p->u(), m_ocp_data, lbg, ubg);

    stage->C(c_C);
    stage->D(c_D);
    auto const constraint_bounds_shrunken = CalculateBoundsShrinking(lbg, ubg, index);
    stage->lbd(lbg - cg + constraint_bounds_shrunken / 2.);
    stage->ubd(ubg - cg - constraint_bounds_shrunken / 2.);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void Controller<TSIM, STOP_OPT, HM>::PrepareFinalStage()
{
    auto const qp = m_qp.problem();
    auto const stage = qp.begin() + m_ocp_data.GetMpcHorizonLength();
    auto const p = m_working_point.cbegin() + m_ocp_data.GetMpcHorizonLength();

    stage->Q(m_w_x_n);
    stage->q(m_w_x_n * (p->x() - m_x_n_ref));

    stage->relativeStateBounds(p->x(), m_xn_min, m_xn_max);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
typename TSIM::InputVector Controller<TSIM, STOP_OPT, HM>::Feedback(typename TSIM::StateVector const& x0)
{
    if (!m_prepared)
        throw std::runtime_error("Controller is not prepared.");

    m_qp.problem()[0].relativeStateBounds(m_working_point[0].x(), x0, x0);

    m_qp.solve();

    for (size_t i = 0; i < m_ocp_data.GetMpcHorizonLength(); ++i) {
        m_working_point[i].x(m_working_point[i].x() + m_qp.solution()[i].x());
        m_working_point[i].u(m_working_point[i].u() + m_qp.solution()[i].u());
        m_lagrange_multiplier_lower_bound_state[i] = m_qp.solution()[i].lam_lbx();
        m_lagrange_multiplier_upper_bound_state[i] = m_qp.solution()[i].lam_ubx();
        m_lagrange_multiplier_lower_bound_input[i] = m_qp.solution()[i].lam_lbu();
        m_lagrange_multiplier_upper_bound_input[i] = m_qp.solution()[i].lam_ubu();
        m_lagrange_multiplier_lower_bound_constraint[i] = m_qp.solution()[i].lam_lbd();
        m_lagrange_multiplier_upper_bound_constraint[i] = m_qp.solution()[i].lam_ubd();
    }

    auto mpc_horizon = m_ocp_data.GetMpcHorizonLength();
    m_working_point[mpc_horizon].x(m_working_point[mpc_horizon].x() + m_qp.solution()[mpc_horizon].x());
    m_working_point[mpc_horizon].u(m_working_point[mpc_horizon].u() + m_qp.solution()[mpc_horizon].u());
    m_lagrange_multiplier_lower_bound_state[mpc_horizon] = m_qp.solution()[mpc_horizon].lam_lbx();
    m_lagrange_multiplier_upper_bound_state[mpc_horizon] = m_qp.solution()[mpc_horizon].lam_ubx();

    m_prepared = false;

    return m_working_point[0].u();
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
template <typename T>
T Controller<TSIM, STOP_OPT, HM>::CalculateBoundsShrinking(T const& lb, T const& ub, size_t k)
{
    double const alpha = static_cast<double>(std::min(k, m_tightening_horizon)) / m_tightening_horizon;
    T bounds_shrinking = m_bounds_tightening * alpha * (ub - lb);
    bounds_shrinking = isfinite(bounds_shrinking.array()).select(bounds_shrinking, T::Zero());

    return bounds_shrinking;
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
double Controller<TSIM, STOP_OPT, HM>::GetMpcSampleTime(unsigned k) const noexcept
{
    return m_ocp_data.GetMpcSampleTimeAt(k);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
unsigned Controller<TSIM, STOP_OPT, HM>::GetMpcStep(unsigned k) const noexcept
{
    return m_ocp_data.GetMpcStepAt(k);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
unsigned Controller<TSIM, STOP_OPT, HM>::GetMpcHorizonLength() const noexcept
{
    return m_ocp_data.GetMpcHorizonLength();
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
double Controller<TSIM, STOP_OPT, HM>::GetIntegrationSampleTime(unsigned k) const noexcept
{
    return m_ocp_data.GetIntegrationSampleTimeAt(k);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
unsigned Controller<TSIM, STOP_OPT, HM>::GetIntegrationPoints(unsigned k) const noexcept
{
    return m_ocp_data.GetNumberOfIntegrationIntervalsAt(k);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
unsigned Controller<TSIM, STOP_OPT, HM>::GetIntegrationHorizon() const noexcept
{
    return m_ocp_data.GetTotalNumberOfIntegrationIntervals();
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
typename TSIM::OutputVector const& Controller<TSIM, STOP_OPT, HM>::GetReferenceOutput(unsigned k) const
{
    return m_ocp_data.GetReferenceOutputAtIntegrationIndex(k);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
typename TSIM::StateVector const& Controller<TSIM, STOP_OPT, HM>::GetReferenceState(unsigned k) const
{
    return m_ocp_data.GetReferenceStateAtIntegrationIndex(k);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
typename TSIM::StateVector const& Controller<TSIM, STOP_OPT, HM>::GetTerminalReferenceState() const
{
    return m_x_n_ref;
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void Controller<TSIM, STOP_OPT, HM>::SetReferenceOutput(unsigned k, typename TSIM::OutputVector const& y_ref)
{
    m_ocp_data.SetReferenceOutputAtIntegrationIndex(k, y_ref);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void Controller<TSIM, STOP_OPT, HM>::SetReferenceState(typename TSIM::StateVector const& x_ref)
{
    m_ocp_data.SetReferenceState(x_ref);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void Controller<TSIM, STOP_OPT, HM>::SetReferenceState(unsigned k, typename TSIM::StateVector const& x_ref)
{
    m_ocp_data.SetReferenceStateAtIntegrationIndex(k, x_ref);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void Controller<TSIM, STOP_OPT, HM>::SetTerminalReferenceState(typename TSIM::StateVector const& x_n_ref)
{
    m_x_n_ref = x_n_ref;
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
typename TSIM::StateVector const& Controller<TSIM, STOP_OPT, HM>::GetStateLowerBound() const
{
    return m_ocp_data.GetStateLowerBound();
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
typename TSIM::StateVector const& Controller<TSIM, STOP_OPT, HM>::GetStateUpperBound() const
{
    return m_ocp_data.GetStateUpperBound();
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
typename TSIM::InputVector const& Controller<TSIM, STOP_OPT, HM>::GetInputLowerBound() const
{
    return m_ocp_data.GetInputLowerBound();
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
typename TSIM::InputVector const& Controller<TSIM, STOP_OPT, HM>::GetInputUpperBound() const
{
    return m_ocp_data.GetInputUpperBound();
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
typename TSIM::ConstraintVector const& Controller<TSIM, STOP_OPT, HM>::GetConstraintLowerBound() const
{
    return m_ocp_data.GetLowerBoundConstraints();
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
typename TSIM::ConstraintVector const& Controller<TSIM, STOP_OPT, HM>::GetConstraintUpperBound() const
{
    return m_ocp_data.GetUpperBoundConstraints();
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
typename TSIM::StateVector const& Controller<TSIM, STOP_OPT, HM>::GetTerminalStateLowerBound() const
{
    return m_xn_min;
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
typename TSIM::StateVector const& Controller<TSIM, STOP_OPT, HM>::GetTerminalStateUpperBound() const
{
    return m_xn_max;
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void Controller<TSIM, STOP_OPT, HM>::SetStateBounds(typename TSIM::StateVector const& x_min,
                                                    typename TSIM::StateVector const& x_max)
{
    m_ocp_data.SetUpperBoundState(x_max);
    m_ocp_data.SetLowerBoundState(x_min);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void Controller<TSIM, STOP_OPT, HM>::SetInputBounds(typename TSIM::InputVector const& u_min,
                                                    typename TSIM::InputVector const& u_max)
{
    m_ocp_data.SetUpperBoundInput(u_max);
    m_ocp_data.SetLowerBoundInput(u_min);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void Controller<TSIM, STOP_OPT, HM>::SetTerminalStateBounds(typename TSIM::StateVector const& x_N_min,
                                                            typename TSIM::StateVector const& x_N_max)
{
    m_xn_min = x_N_min;
    m_xn_max = x_N_max;
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void Controller<TSIM, STOP_OPT, HM>::SetConstraintBounds(const typename TSIM::ConstraintVector& lbg,
                                                         const typename TSIM::ConstraintVector& ubg)
{
    m_ocp_data.SetLowerBoundConstraints(lbg);
    m_ocp_data.SetUpperBoundConstraints(ubg);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void Controller<TSIM, STOP_OPT, HM>::SetTerminalVelocityLimits(typename TSIM::AxesVector const& qd_min,
                                                               typename TSIM::AxesVector const& qd_max)
{
    MakeStateView<TSIM>(m_xn_min).GetVelocity() = qd_min;
    MakeStateView<TSIM>(m_xn_max).GetVelocity() = qd_max;
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
typename TSIM::InputVector const& Controller<TSIM, STOP_OPT, HM>::GetInputErrorWeightAt(unsigned k) const
{
    return m_ocp_data.GetInputErrorWeightAt(k);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
typename TSIM::StateVector const& Controller<TSIM, STOP_OPT, HM>::GetStateErrorWeightAt(unsigned k) const
{
    return m_ocp_data.GetStateErrorWeightAt(k);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
typename TSIM::StateVector const& Controller<TSIM, STOP_OPT, HM>::GetTerminalStateErrorWeight() const
{
    return m_w_x_n_vec;
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
typename TSIM::OutputVector const& Controller<TSIM, STOP_OPT, HM>::GetOutputErrorWeightAt(unsigned k) const
{
    return m_ocp_data.GetOutputErrorWeightAt(k);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void Controller<TSIM, STOP_OPT, HM>::SetStateErrorWeight(typename TSIM::StateVector const& wx)
{
    m_ocp_data.SetStateErrorWeight(wx);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void Controller<TSIM, STOP_OPT, HM>::SetStateErrorWeight(std::vector<typename TSIM::StateVector> const& wx_vec)
{
    m_ocp_data.SetStateErrorWeight(wx_vec);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void Controller<TSIM, STOP_OPT, HM>::SetStateErrorWeight(unsigned k, typename TSIM::StateVector const& wx_k)
{
    m_ocp_data.SetStateErrorWeightAt(k, wx_k);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void Controller<TSIM, STOP_OPT, HM>::SetTerminalStateErrorWeight(typename TSIM::StateVector const& w_x_n)
{
    m_w_x_n = (m_w_x_n_vec = w_x_n).cwiseAbs2().asDiagonal();
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void Controller<TSIM, STOP_OPT, HM>::SetInputErrorWeight(typename TSIM::InputVector const& wu)
{
    m_ocp_data.SetInputErrorWeight(wu);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void Controller<TSIM, STOP_OPT, HM>::SetInputErrorWeight(std::vector<typename TSIM::InputVector> const& wu_vec)
{
    m_ocp_data.SetInputErrorWeight(wu_vec);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void Controller<TSIM, STOP_OPT, HM>::SetInputErrorWeight(unsigned k, typename TSIM::InputVector const& wu_k)
{
    m_ocp_data.SetInputErrorWeightAt(k, wu_k);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void Controller<TSIM, STOP_OPT, HM>::SetOutputErrorWeight(typename TSIM::OutputVector const& wy)
{
    m_ocp_data.SetOutputErrorWeight(wy);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void Controller<TSIM, STOP_OPT, HM>::SetOutputErrorWeight(std::vector<typename TSIM::OutputVector> const& wy_vec)
{
    m_ocp_data.SetOutputErrorWeight(wy_vec);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void Controller<TSIM, STOP_OPT, HM>::SetOutputErrorWeight(unsigned k, typename TSIM::OutputVector const& wy_k)
{
    m_ocp_data.SetOutputErrorWeightAt(k, wy_k);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
TransformationMatrix const& Controller<TSIM, STOP_OPT, HM>::GetFinalTransform() const
{
    return m_ocp_data.GetFinalTransform();
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void Controller<TSIM, STOP_OPT, HM>::SetFinalTransform(TransformationMatrix const& val)
{
    m_ocp_data.SetFinalTransform(val);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void Controller<TSIM, STOP_OPT, HM>::SetFinalTransform(double x, double y, double z, double roll, double pitch,
                                                       double yaw)
{
    TransformationMatrix Troll, T_pitch, T_yaw, T_xyz, T;
    Troll << 1., 0., 0., 0., 0, std::cos(roll), -sin(roll), 0., 0., std::sin(roll), std::cos(roll), 0., 0., 0., 0., 1.;

    T_pitch << std::cos(pitch), 0., std::sin(pitch), 0., 0, 1., 0., 0., -sin(pitch), 0., std::cos(pitch), 0., 0., 0.,
        0., 1.;

    T_yaw << std::cos(yaw), -sin(yaw), 0., 0., std::sin(yaw), std::cos(yaw), 0., 0., 0., 0., 1., 0., 0., 0., 0., 1.;

    T_xyz << 1., 0., 0., x, 0., 1., 0., y, 0., 0., 1., z, 0., 0., 0., 1.;

    T = T_xyz * T_yaw * T_pitch * Troll;

    SetFinalTransform(T);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
size_t Controller<TSIM, STOP_OPT, HM>::GetTighteningHorizon()
{
    return m_tightening_horizon;
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
double Controller<TSIM, STOP_OPT, HM>::GetBoundsTightening()
{
    return m_bounds_tightening;
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void Controller<TSIM, STOP_OPT, HM>::SetTighteningHorizon(size_t n)
{
    if (!(n > 0))
        throw std::invalid_argument("Controller<TSIM, STOP_OPT, HM>::Impl::tightening_horizon(): value must be > 0.");

    m_tightening_horizon = n;
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void Controller<TSIM, STOP_OPT, HM>::SetBoundsTightening(double val)
{
    if (val < 0.)
        throw std::invalid_argument(
            "Controller<TSIM, STOP_OPT, HM>::Impl::bounds_tightening(): value must non-negative!");

    m_bounds_tightening = val;
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
double Controller<TSIM, STOP_OPT, HM>::GetLevenbergMarquardt() const noexcept
{
    return m_ocp_data.GetLevenbergMarquardt();
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void Controller<TSIM, STOP_OPT, HM>::SetLevenbergMarquardt(double val)
{
    m_ocp_data.SetLevenbergMarquardt(val);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
double Controller<TSIM, STOP_OPT, HM>::GetScalingFactorForStoppabilityBounds() const noexcept
{
    return m_ocp_data.GetAlpha();
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void Controller<TSIM, STOP_OPT, HM>::SetScalingFactorForStoppabilityBounds(double alpha)
{
    m_ocp_data.SetAlpha(alpha);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void Controller<TSIM, STOP_OPT, HM>::SetWorkingPoint(typename TSIM::StateVector const& x,
                                                     typename TSIM::InputVector const& u)
{
    auto mpc_horizon = m_ocp_data.GetMpcHorizonLength();
    m_working_point = tmpc::constantMpcTrajectory(OcpPoint{x, u}, mpc_horizon);
    m_prepared = false;

    m_lagrange_multiplier_lower_bound_state =
        std::vector<typename TSIM::StateVector>(mpc_horizon + 1, TSIM::StateVector::Zero());
    m_lagrange_multiplier_upper_bound_state =
        std::vector<typename TSIM::StateVector>(mpc_horizon + 1, TSIM::StateVector::Zero());
    m_lagrange_multiplier_lower_bound_input =
        std::vector<typename TSIM::InputVector>(mpc_horizon, TSIM::InputVector::Zero());
    m_lagrange_multiplier_upper_bound_input =
        std::vector<typename TSIM::InputVector>(mpc_horizon, TSIM::InputVector::Zero());
    m_lagrange_multiplier_lower_bound_constraint =
        std::vector<typename OcpProblem::ConstraintVector>(mpc_horizon, OcpProblem::ConstraintVector::Zero());
    m_lagrange_multiplier_upper_bound_constraint =
        std::vector<typename OcpProblem::ConstraintVector>(mpc_horizon, OcpProblem::ConstraintVector::Zero());
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
typename TSIM::InputVector Controller<TSIM, STOP_OPT, HM>::GetInputPlan(unsigned k) const noexcept
{
    return m_working_point.at(k).u();
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
typename TSIM::StateVector Controller<TSIM, STOP_OPT, HM>::GetStatePlan(unsigned k) const noexcept
{
    return m_working_point.at(k).x();
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
unsigned Controller<TSIM, STOP_OPT, HM>::GetNumIter() const noexcept
{
    return m_qp.numIter();
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void Controller<TSIM, STOP_OPT, HM>::PrintQp(std::ostream& os) const
{
    PrintMultistageQpMatlab(os, m_qp.problem(), "qp");
}
template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
typename TSIM::StateMatrix Controller<TSIM, STOP_OPT, HM>::GetStatePlan() const
{
    typename TSIM::StateMatrix x(TSIM::Dimension::NX, GetMpcHorizonLength() + 1);

    for (unsigned i = 0; i < x.cols(); ++i)
        x.col(i) = GetStatePlan(i);

    return x;
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
typename TSIM::InputMatrix Controller<TSIM, STOP_OPT, HM>::GetInputPlan() const
{
    typename TSIM::InputMatrix u(TSIM::Dimension::NU, GetMpcHorizonLength());

    for (unsigned i = 0; i < u.cols(); ++i)
        u.col(i) = GetInputPlan(i);

    return u;
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
typename TSIM::OutputMatrix Controller<TSIM, STOP_OPT, HM>::GetReferenceOutput() const
{
    typename TSIM::OutputMatrix y_ref(TSIM::Dimension::NY, GetMpcHorizonLength());

    for (unsigned i = 0; i < y_ref.cols(); ++i)
        y_ref.col(i) = GetReferenceOutput(i);

    return y_ref;
}

}  //namespace mpmca::control