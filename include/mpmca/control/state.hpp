/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/control/templates/state_view.hpp"
#include "mpmca/control/templates/state_view_at.hpp"

namespace mpmca::control {

template <typename TSIM, typename T>
inline auto MakeStateView(T& x)
{
    return templates::StateView<TSIM, T>{x};
}
}  //namespace mpmca::control

#include "mpmca/control/constraint_free_box_state.tpp"
#include "mpmca/control/gimbal_state.tpp"
#include "mpmca/control/hexapod_state.tpp"
#include "mpmca/control/prismatic_link_state.tpp"
#include "mpmca/control/revolute_link_state.tpp"
#include "mpmca/control/xy_table_state.tpp"
#include "mpmca/control/yaw_table_state.tpp"
#include "mpmca/control/yz_tracks_state.tpp"