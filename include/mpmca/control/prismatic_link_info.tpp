/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/control/components.hpp"
#include "mpmca/control/templates/info_view.hpp"
#include "mpmca/control/templates/info_view_at.hpp"

namespace mpmca::control::templates {

template <typename T>
class InfoViewAt<components::PrismaticLink, T>
{
    T& m_info_vector;
    std::size_t m_offset;

  public:
    InfoViewAt(T& info, std::size_t offset);
    double& GetTheta();
    const double& GetTheta() const;
    double& GetDOffset();
    const double& GetDOffset() const;
    double& GetAlpha();
    const double& GetAlpha() const;
    double& GetA();
    const double& GetA() const;
    decltype(auto) GetActiveDoF();
    decltype(auto) GetActiveDoF() const;
};

template <typename T>
InfoViewAt<components::PrismaticLink, T>::InfoViewAt(T& info, std::size_t offset)
    : m_info_vector{info}
    , m_offset{offset}
{
}

template <typename T>
double& InfoViewAt<components::PrismaticLink, T>::GetTheta()
{
    return m_info_vector(m_offset);
}

template <typename T>
const double& InfoViewAt<components::PrismaticLink, T>::GetTheta() const
{
    return m_info_vector(m_offset);
}

template <typename T>
double& InfoViewAt<components::PrismaticLink, T>::GetDOffset()
{
    return m_info_vector(m_offset + 1);
}

template <typename T>
const double& InfoViewAt<components::PrismaticLink, T>::GetDOffset() const
{
    return m_info_vector(m_offset + 1);
}

template <typename T>
double& InfoViewAt<components::PrismaticLink, T>::GetAlpha()
{
    return m_info_vector(m_offset + 2);
}

template <typename T>
const double& InfoViewAt<components::PrismaticLink, T>::GetAlpha() const
{
    return m_info_vector(m_offset + 2);
}

template <typename T>
double& InfoViewAt<components::PrismaticLink, T>::GetA()
{
    return m_info_vector(m_offset + 3);
}

template <typename T>
const double& InfoViewAt<components::PrismaticLink, T>::GetA() const
{
    return m_info_vector(m_offset + 3);
}

template <typename T>
decltype(auto) InfoViewAt<components::PrismaticLink, T>::GetActiveDoF()
{
    return m_info_vector.template segment<4>(m_offset + 4);
}

template <typename T>
decltype(auto) InfoViewAt<components::PrismaticLink, T>::GetActiveDoF() const
{
    return m_info_vector.template segment<4>(m_offset + 4);
}
}  //namespace mpmca::control