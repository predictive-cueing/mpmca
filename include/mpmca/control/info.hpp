/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/control/components.hpp"
#include "mpmca/control/templates/info_view.hpp"
#include "mpmca/control/templates/info_view_at.hpp"

namespace mpmca::control {

template <typename TSIM, typename T>
inline auto MakeInfoView(T& info)
{
    return templates::InfoView<TSIM, T>{info};
}
}  //namespace mpmca::control

#include "mpmca/control/constraint_free_box_info.tpp"
#include "mpmca/control/gimbal_info.tpp"
#include "mpmca/control/hexapod_info.tpp"
#include "mpmca/control/prismatic_link_info.tpp"
#include "mpmca/control/revolute_link_info.tpp"
#include "mpmca/control/transformation_matrix_info.tpp"
#include "mpmca/control/xy_table_info.tpp"
#include "mpmca/control/yaw_table_info.tpp"
#include "mpmca/control/yz_tracks_info.tpp"
