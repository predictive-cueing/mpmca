/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/control/from_tmpc/casadi_interface/generated_function.hpp"
#include "mpmca/control/hessian_calculation_method.hpp"
#include "mpmca/control/ocp_data.hpp"
#include "mpmca/control/state.hpp"
#include "mpmca/control/state_stoppability_opt.hpp"
#include "mpmca/linear_algebra.hpp"

namespace mpmca::control {
template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
class ConstraintsInLagrangian
{
    casadi_interface::GeneratedFunction m_lagrangian_hessian_function;

    static constexpr std::size_t NC_STOP = (STOP_OPT == StateStoppabilityOption::kNone) ? 0 : TSIM::Dimension::NX;
    static constexpr std::size_t NC = TSIM::Dimension::NC + NC_STOP;

    using ConstraintVector = Vector<NC>;
    using ConstraintInputMatrix = Matrix<NC, TSIM::Dimension::NU>;
    using ConstraintStateMatrix = Matrix<NC, TSIM::Dimension::NX>;

    void AddHessianOfStoppabilityConstraints(ConstraintVector const& lam_lbg, ConstraintVector const& lam_ubg,
                                             typename TSIM::InputVector const& u_min,
                                             typename TSIM::InputVector const& u_max,
                                             typename TSIM::StateStateMatrix& d2L_dx2) const;

    void AddHessianOfSimulatorConstraints(typename TSIM::StateVector const& x, typename TSIM::InputVector const& u,
                                          ConstraintVector const& lam_lbg, ConstraintVector const& lam_ubg,
                                          typename TSIM::StateStateMatrix& d2L_dx2,
                                          typename TSIM::StateInputMatrix& d2L_dx_duT,
                                          typename TSIM::InputInputMatrix& d2L_du2) const;

  public:
    ConstraintsInLagrangian();

    void AddHessianOfConstraints(typename TSIM::StateVector const& x, typename TSIM::InputVector const& u,
                                 ConstraintVector const& lam_lbg, ConstraintVector const& lam_ubg,
                                 typename TSIM::InputVector const& u_min, typename TSIM::InputVector const& u_max,
                                 typename TSIM::StateStateMatrix& d2L_dx2, typename TSIM::StateInputMatrix& d2L_dx_duT,
                                 typename TSIM::InputInputMatrix& d2L_du2) const;
};

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
ConstraintsInLagrangian<TSIM, STOP_OPT, HM>::ConstraintsInLagrangian()
    : m_lagrangian_hessian_function(TSIM::LagrangeHessian_functions())
{
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void ConstraintsInLagrangian<TSIM, STOP_OPT, HM>::AddHessianOfConstraints(
    typename TSIM::StateVector const& x, typename TSIM::InputVector const& u, ConstraintVector const& lam_lbg,
    ConstraintVector const& lam_ubg, typename TSIM::InputVector const& u_min, typename TSIM::InputVector const& u_max,
    typename TSIM::StateStateMatrix& d2L_dx2, typename TSIM::StateInputMatrix& d2L_dx_duT,
    typename TSIM::InputInputMatrix& d2L_du2) const
{
    AddHessianOfSimulatorConstraints(x, u, lam_lbg, lam_ubg, d2L_dx2, d2L_dx_duT, d2L_du2);
    AddHessianOfStoppabilityConstraints(lam_lbg, lam_ubg, u_min, u_max, d2L_dx2);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void ConstraintsInLagrangian<TSIM, STOP_OPT, HM>::AddHessianOfStoppabilityConstraints(
    ConstraintVector const& lam_lbg, ConstraintVector const& lam_ubg, typename TSIM::InputVector const& u_min,
    typename TSIM::InputVector const& u_max, typename TSIM::StateStateMatrix& d2L_dx2) const
{
    if (HM == HessianCalculationMethod::kGaussNewton) {
        return;
    }
    else {
        if constexpr (STOP_OPT == StateStoppabilityOption::kNone) {
            return;
        }
        else if constexpr (STOP_OPT == StateStoppabilityOption::kOriginalExpression) {
            typename TSIM::StateVector lam_lbg_stop = lam_lbg.template bottomRows<TSIM::Dimension::NX>();
            typename TSIM::StateVector lam_ubg_stop = lam_ubg.template bottomRows<TSIM::Dimension::NX>();

            typename TSIM::StateVector lam_stop = lam_ubg_stop - lam_lbg_stop;
            for (auto k = 0; k < TSIM::Dimension::NQ; ++k) {
                d2L_dx2(TSIM::Dimension::NQ + k, TSIM::Dimension::NQ + k) +=
                    (-lam_stop(k) / u_min(k) + lam_stop(TSIM::Dimension::NQ + k) / u_max(k));
            }
        }
        else if constexpr (STOP_OPT == StateStoppabilityOption::kExpressionWithoutDenominator) {
            typename TSIM::StateVector lam_lbg_stop = lam_lbg.template bottomRows<TSIM::Dimension::NX>();
            typename TSIM::StateVector lam_ubg_stop = lam_ubg.template bottomRows<TSIM::Dimension::NX>();

            typename TSIM::StateVector lam_stop = 2. * (lam_ubg_stop - lam_lbg_stop);
            for (auto k = 0; k < TSIM::Dimension::NQ; ++k)
                d2L_dx2(TSIM::Dimension::NQ + k, TSIM::Dimension::NQ + k) +=
                    (lam_stop(k) + lam_stop(TSIM::Dimension::NQ + k));
        }
    }
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void ConstraintsInLagrangian<TSIM, STOP_OPT, HM>::AddHessianOfSimulatorConstraints(
    typename TSIM::StateVector const& x, typename TSIM::InputVector const& u, ConstraintVector const& lam_lbg,
    ConstraintVector const& lam_ubg, typename TSIM::StateStateMatrix& d2L_dx2,
    typename TSIM::StateInputMatrix& d2L_dx_duT, typename TSIM::InputInputMatrix& d2L_du2) const
{
    if constexpr (HM == HessianCalculationMethod::kGaussNewton) {
        return;
    }
    else if constexpr (HM == HessianCalculationMethod::kFull) {
        typename TSIM::ConstraintVector lam_lbg_sim = lam_lbg.template topRows<TSIM::Dimension::NC>();
        typename TSIM::ConstraintVector lam_ubg_sim = lam_ubg.template topRows<TSIM::Dimension::NC>();

        typename TSIM::StateStateMatrix d2Lg_dx2;
        typename TSIM::StateInputMatrix d2Lg_dx_duT;
        typename TSIM::InputInputMatrix d2Lg_du2;
        m_lagrangian_hessian_function({x.data(), nullptr, u.data(), nullptr, lam_lbg_sim.data(), lam_ubg_sim.data()},
                                      {d2Lg_dx2.data(), d2Lg_dx_duT.data(), d2Lg_du2.data()});

        d2L_dx2 += d2Lg_dx2;
        d2L_dx_duT += d2Lg_dx_duT;
        d2L_du2 += d2Lg_du2;
    }
}
}  //namespace mpmca::control