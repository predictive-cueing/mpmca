/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/control/components.hpp"
#include "mpmca/control/templates/info_view.hpp"
#include "mpmca/control/templates/info_view_at.hpp"

namespace mpmca::control::templates {

template <typename T>
class InfoViewAt<components::Hexapod, T>
{
    T& m_info_vector;
    std::size_t m_offset;

  public:
    InfoViewAt(T& info, std::size_t offset);
    decltype(auto) GetHexapodBaseCorners();
    decltype(auto) GetHexapodBaseCorners() const;
    decltype(auto) GetHexapodPlatformCorners();
    decltype(auto) GetHexapodPlatformCorners() const;
    decltype(auto) GetActiveDoF();
    decltype(auto) GetActiveDoF() const;
};

template <typename T>
InfoViewAt<components::Hexapod, T>::InfoViewAt(T& info, std::size_t offset)
    : m_info_vector{info}
    , m_offset{offset}
{
}

template <typename T>
decltype(auto) InfoViewAt<components::Hexapod, T>::GetHexapodBaseCorners()
{
    return m_info_vector.template segment<18>(m_offset);
}

template <typename T>
decltype(auto) InfoViewAt<components::Hexapod, T>::GetHexapodBaseCorners() const
{
    return m_info_vector.template segment<18>(m_offset);
}

template <typename T>
decltype(auto) InfoViewAt<components::Hexapod, T>::GetHexapodPlatformCorners()
{
    return m_info_vector.template segment<18>(m_offset + 18);
}

template <typename T>
decltype(auto) InfoViewAt<components::Hexapod, T>::GetHexapodPlatformCorners() const
{
    return m_info_vector.template segment<18>(m_offset + 18);
}

template <typename T>
decltype(auto) InfoViewAt<components::Hexapod, T>::GetActiveDoF()
{
    return m_info_vector.template segment<6>(m_offset + 36);
}

template <typename T>
decltype(auto) InfoViewAt<components::Hexapod, T>::GetActiveDoF() const
{
    return m_info_vector.template segment<6>(m_offset + 36);
}
}  //namespace mpmca::control