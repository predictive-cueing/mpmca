/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/control/simulator.hpp"

namespace mpmca::control {
template <typename TSIM>
struct ControllerOptions
{
    ControllerOptions();

    std::string integration_method{"constantXY"};
    double scaling_factor_for_stoppability_bounds{1.0};

    typename TSIM::OutputVector output_error_weight{TSIM::OutputVector::Constant(1.)};
    typename TSIM::StateVector state_error_weight{TSIM::StateVector::Constant(1.)};
    typename TSIM::StateVector terminal_state_error_weight{TSIM::StateVector::Constant(1.)};
    typename TSIM::InputVector input_error_weight{TSIM::InputVector::Constant(1e-3)};

    double bounds_tightening{0.};
    std::size_t tightening_horizon{1};

    double levenberg_marquardt{0.};

    typename TSIM::StateVector reference_state;
    typename TSIM::StateVector lower_bound_state;
    typename TSIM::StateVector upper_bound_state;
    typename TSIM::StateVector lower_bound_terminal_state;
    typename TSIM::StateVector upper_bound_terminal_state;
    typename TSIM::InputVector lower_bound_input;
    typename TSIM::InputVector upper_bound_input;
    typename TSIM::OutputVector reference_output;
    typename TSIM::ConstraintVector lower_bound_constraint;
    typename TSIM::ConstraintVector upper_bound_constraint;
    TransformationMatrix final_transform;
};

template <typename TSIM>
ControllerOptions<TSIM>::ControllerOptions()
{
    Simulator<TSIM> simulator;
    reference_state = simulator.GetNominalState();
    lower_bound_state = simulator.GetStateLowerBound();
    upper_bound_state = simulator.GetStateUpperBound();
    lower_bound_terminal_state = lower_bound_state;
    upper_bound_terminal_state = upper_bound_state;
    lower_bound_input = simulator.GetInputLowerBound();
    upper_bound_input = simulator.GetInputUpperBound();
    reference_output = simulator.GetNominalOutput();
    final_transform = simulator.GetFinalTransform();
    lower_bound_constraint = simulator.GetConstraintLowerBound();
    upper_bound_constraint = simulator.GetConstraintUpperBound();
}
}  //namespace mpmca::control