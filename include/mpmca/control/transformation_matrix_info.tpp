/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/control/components.hpp"
#include "mpmca/control/templates/info_view.hpp"
#include "mpmca/control/templates/info_view_at.hpp"

namespace mpmca::control::templates {

template <typename T>
class InfoViewAt<components::TransformationMatrix, T>
{
    T& m_info_vector;
    std::size_t m_offset;

  public:
    InfoViewAt(T& info, std::size_t offset);
    decltype(auto) GetXyz();
    decltype(auto) GetXyz() const;
    decltype(auto) GetRollPitchYaw();
    decltype(auto) GetRollPitchYaw() const;
    decltype(auto) GetActiveDoF();
    decltype(auto) GetActiveDoF() const;
};

template <typename T>
InfoViewAt<components::TransformationMatrix, T>::InfoViewAt(T& info, std::size_t offset)
    : m_info_vector{info}
    , m_offset{offset}
{
}

template <typename T>
decltype(auto) InfoViewAt<components::TransformationMatrix, T>::GetXyz()
{
    return m_info_vector.template segment<3>(m_offset);
}

template <typename T>
decltype(auto) InfoViewAt<components::TransformationMatrix, T>::GetXyz() const
{
    return m_info_vector.template segment<3>(m_offset);
}

template <typename T>
decltype(auto) InfoViewAt<components::TransformationMatrix, T>::GetRollPitchYaw()
{
    return m_info_vector.template segment<3>(m_offset + 3);
}

template <typename T>
decltype(auto) InfoViewAt<components::TransformationMatrix, T>::GetRollPitchYaw() const
{
    return m_info_vector.template segment<3>(m_offset + 3);
}

template <typename T>
decltype(auto) InfoViewAt<components::TransformationMatrix, T>::GetActiveDoF()
{
    return m_info_vector.template segment<6>(m_offset + 6);
}

template <typename T>
decltype(auto) InfoViewAt<components::TransformationMatrix, T>::GetActiveDoF() const
{
    return m_info_vector.template segment<6>(m_offset + 6);
}

}  //namespace mpmca::control