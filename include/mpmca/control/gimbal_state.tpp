/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/control/components.hpp"
#include "mpmca/control/templates/state_view.hpp"
#include "mpmca/control/templates/state_view_at.hpp"

namespace mpmca::control::templates {

template <typename T>
class StateViewAt<components::Gimbal, T>
{
    T& m_state_vector;
    std::size_t m_position_offset;
    std::size_t m_velocity_offset;

  public:
    StateViewAt(T& state_vector, std::size_t position_offset, std::size_t velocity_offset);

    double GetPhi();
    double GetPhi() const;
    double GetTheta();
    double GetTheta() const;
    double GetPsi();
    double GetPsi() const;

    double GetP();
    double GetP() const;
    double GetQ();
    double GetQ() const;
    double GetR();
    double GetR() const;
};

template <typename T>
StateViewAt<components::Gimbal, T>::StateViewAt(T& state_vector, std::size_t position_offset,
                                                std::size_t velocity_offset)
    : m_state_vector(state_vector)
    , m_position_offset(position_offset)
    , m_velocity_offset(velocity_offset)
{
}
template <typename T>
double StateViewAt<components::Gimbal, T>::GetPhi()
{
    return m_state_vector(m_position_offset + 0);
}
template <typename T>
double StateViewAt<components::Gimbal, T>::GetPhi() const
{
    return m_state_vector(m_position_offset + 0);
}
template <typename T>
double StateViewAt<components::Gimbal, T>::GetTheta()
{
    return m_state_vector(m_position_offset + 1);
}
template <typename T>
double StateViewAt<components::Gimbal, T>::GetTheta() const
{
    return m_state_vector(m_position_offset + 1);
}
template <typename T>
double StateViewAt<components::Gimbal, T>::GetPsi()
{
    return m_state_vector(m_position_offset + 2);
}
template <typename T>
double StateViewAt<components::Gimbal, T>::GetPsi() const
{
    return m_state_vector(m_position_offset + 2);
}
template <typename T>
double StateViewAt<components::Gimbal, T>::GetP()
{
    return m_state_vector(m_velocity_offset + 0);
}
template <typename T>
double StateViewAt<components::Gimbal, T>::GetP() const
{
    return m_state_vector(m_velocity_offset + 0);
}
template <typename T>
double StateViewAt<components::Gimbal, T>::GetQ()
{
    return m_state_vector(m_velocity_offset + 1);
}
template <typename T>
double StateViewAt<components::Gimbal, T>::GetQ() const
{
    return m_state_vector(m_velocity_offset + 1);
}
template <typename T>
double StateViewAt<components::Gimbal, T>::GetR()
{
    return m_state_vector(m_velocity_offset + 2);
}
template <typename T>
double StateViewAt<components::Gimbal, T>::GetR() const
{
    return m_state_vector(m_velocity_offset + 2);
}

}  //namespace mpmca::control::templates