/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/control/from_tmpc/casadi_interface/generated_function.hpp"
#include "mpmca/control/state.hpp"
#include "mpmca/linear_algebra.hpp"
#include "mpmca/types/inertial_vector.hpp"
#include "mpmca/types/pose_rot_mat_vector.hpp"
#include "mpmca/types/pose_vector.hpp"
#include "mpmca/types/transformation_matrix.hpp"

namespace mpmca::control {
template <class TSIM>
class Simulator
{
    typename TSIM::StateVector m_x_lb;
    typename TSIM::StateVector m_x_ub;
    typename TSIM::StateVector m_x0;
    typename TSIM::InputVector m_u_lb;
    typename TSIM::InputVector m_u_ub;
    typename TSIM::InputVector m_u0;
    typename TSIM::ConstraintVector m_g_lb;
    typename TSIM::ConstraintVector m_g_ub;
    typename TSIM::StateVector m_xN_lb;
    typename TSIM::StateVector m_xN_ub;
    mpmca::TransformationMatrix m_T;
    Vector<3> m_gravity;
    typename TSIM::OutputVector m_y0;

    casadi_interface::GeneratedFunction m_output_function;
    casadi_interface::GeneratedFunction m_output_jacobian_function;
    casadi_interface::GeneratedFunction m_constraint_function;
    casadi_interface::GeneratedFunction m_head_pva_function;
    casadi_interface::GeneratedFunction m_head_inertial_function;
    casadi_interface::GeneratedFunction m_info_function;
    casadi_interface::GeneratedFunction m_simulator_data_function;

  public:
    Simulator();

    typename TSIM::OutputVector GetOutput(typename TSIM::StateVector const& x, typename TSIM::InputVector const& u,
                                          TransformationMatrix const& T_HP) const;

    void GetOutputJacobian(typename TSIM::StateVector const& x, typename TSIM::InputVector const& u,
                           TransformationMatrix const& T_HP, typename TSIM::OutputVector& y,
                           typename TSIM::OutputStateMatrix& dy_dx, typename TSIM::OutputInputMatrix& dy_du) const;

    PoseRotMatVector GetHeadPva(typename TSIM::StateVector const& x, typename TSIM::InputVector const& u,
                                TransformationMatrix const& T_HP) const;
    InertialVector GetHeadInertial(typename TSIM::StateVector const& x, typename TSIM::InputVector const& u,
                                   TransformationMatrix const& T_HP) const;

    typename TSIM::StateVector Integrate(typename TSIM::StateVector const& x, typename TSIM::InputVector const& u,
                                         double ts) const;

    typename TSIM::ConstraintVector GetConstraint(typename TSIM::StateVector const& x,
                                                  typename TSIM::InputVector const& u) const;

    typename TSIM::InfoVector GetInfo() const;

    std::string GetName() const;

    typename TSIM::OutputVector const& GetNominalOutput() const;
    typename TSIM::StateVector const& GetNominalState() const;
    typename TSIM::InputVector const& GetNominalInput() const;

    typename TSIM::StateVector const& GetStateLowerBound() const;
    typename TSIM::StateVector const& GetStateUpperBound() const;
    typename TSIM::InputVector const& GetInputLowerBound() const;
    typename TSIM::InputVector const& GetInputUpperBound() const;
    typename TSIM::ConstraintVector const& GetConstraintLowerBound() const;
    typename TSIM::ConstraintVector const& GetConstraintUpperBound() const;
    typename TSIM::StateVector const& GetTerminalStateLowerBound() const;
    typename TSIM::StateVector const& GetTerminalStateUpperBound() const;

    TransformationMatrix const& GetFinalTransform() const;
    Vector<3> const& GetGravity() const;
};

}  //namespace mpmca::control
#include "mpmca/control/simulator.tpp"
