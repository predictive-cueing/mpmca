/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

namespace mpmca::control {
enum class StateStoppabilityOption
{
    kNone,
    kOriginalExpression,
    kExpressionWithoutDenominator
};
}  //namespace mpmca::control