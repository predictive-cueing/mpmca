/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <memory>

#include "mpmca/control/constraints.hpp"
#include "mpmca/control/constraints_in_lagrangian.hpp"
#include "mpmca/control/cost.hpp"
#include "mpmca/control/from_tmpc/casadi_interface/generated_function.hpp"
#include "mpmca/control/hessian_calculation_method.hpp"
#include "mpmca/control/ocp_data.hpp"
#include "mpmca/control/state.hpp"
#include "mpmca/control/state_stoppability_opt.hpp"
#include "mpmca/linear_algebra.hpp"

namespace mpmca::control {

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
class OCP
{
    static constexpr std::size_t NC_STOP = (STOP_OPT == StateStoppabilityOption::kNone) ? 0 : TSIM::Dimension::NX;
    using StoppabilityVector = Vector<NC_STOP>;
    using StoppabilityInputMatrix = Matrix<NC_STOP, TSIM::Dimension::NU>;
    using StoppabilityStateMatrix = Matrix<NC_STOP, TSIM::Dimension::NX>;
    using IterOutput = typename std::vector<typename TSIM::OutputVector>::const_iterator;
    using IterState = typename std::vector<typename TSIM::StateVector>::const_iterator;

    Cost<TSIM, HM> m_cost_functions;
    Constraints<TSIM, STOP_OPT> m_constraint_functions;
    ConstraintsInLagrangian<TSIM, STOP_OPT, HM> m_constraint_in_lagrangian_functions;

  public:
    static constexpr std::size_t NC = TSIM::Dimension::NC + NC_STOP;

    using ConstraintVector = Vector<NC>;
    using ConstraintInputMatrix = Matrix<NC, TSIM::Dimension::NU>;
    using ConstraintStateMatrix = Matrix<NC, TSIM::Dimension::NX>;

    OCP();

    double CalculateCost(unsigned k, typename TSIM::StateVector const& x, typename TSIM::InputVector const& u,
                         OcpData<TSIM> const& ocp_data) const;

    void CalculateCostJacobian(unsigned k, typename TSIM::StateVector const& x, typename TSIM::InputVector const& u,
                               OcpData<TSIM> const& ocp_data, typename TSIM::StateVector& dJ_dx,
                               typename TSIM::InputVector& dJ_du) const;

    void CalculateCostHessian(unsigned k, typename TSIM::StateVector const& x, typename TSIM::InputVector const& u,
                              OcpData<TSIM> const& ocp_data, ConstraintVector const& lam_lbg,
                              ConstraintVector const& lam_ubg, typename TSIM::StateVector& dJ_dx,
                              typename TSIM::InputVector& dJ_du, typename TSIM::StateStateMatrix& d2L_dx2,
                              typename TSIM::StateInputMatrix& d2L_dx_duT,
                              typename TSIM::InputInputMatrix& d2L_du2) const;

    void CalculateConstraintJacobian(typename TSIM::StateVector const& x, typename TSIM::InputVector const& u,
                                     OcpData<TSIM> const& ocp_data, ConstraintVector& g, ConstraintStateMatrix& dg_dx,
                                     ConstraintInputMatrix& dg_du) const;

    void CalculateConstraintBounds(typename TSIM::StateVector const& x, typename TSIM::InputVector const& u,
                                   OcpData<TSIM> const& ocp_data, ConstraintVector& lgb, ConstraintVector& ugb) const;

    typename TSIM::StateVector Integrate(typename TSIM::StateVector const& x, typename TSIM::InputVector const& u,
                                         double ts) const;

    void CalculateDifferenceEquation(double ts, typename TSIM::StateStateMatrix& A, typename TSIM::StateInputMatrix& B);
};

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
OCP<TSIM, STOP_OPT, HM>::OCP()
    : m_cost_functions{}
    , m_constraint_functions{}
    , m_constraint_in_lagrangian_functions{}
{
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
double OCP<TSIM, STOP_OPT, HM>::CalculateCost(unsigned k, typename TSIM::StateVector const& x0,
                                              typename TSIM::InputVector const& u, OcpData<TSIM> const& ocp_data) const
{
    IterOutput y_ref_begin, y_ref_end;
    IterState x_ref_begin, x_ref_end;
    ocp_data.GetReferenceStatesAt(k, x_ref_begin, x_ref_end);
    ocp_data.GetReferenceOutputsAt(k, y_ref_begin, y_ref_end);
    double dt = ocp_data.GetIntegrationSampleTimeAt(k);

    typename TSIM::StateVector x_k = x0;
    auto y_ref_k = y_ref_begin;
    auto x_ref_k = x_ref_begin;
    auto J = 0.;

    for (; y_ref_k != y_ref_end; ++y_ref_k, ++x_ref_k) {
        J += m_cost_functions.GetCostDt(x_k, u, *y_ref_k, *x_ref_k, ocp_data.GetFinalTransform(),
                                        ocp_data.GetStateErrorWeightAt(k), ocp_data.GetInputErrorWeightAt(k),
                                        ocp_data.GetOutputErrorWeightAt(k)) *
             dt;

        typename TSIM::StateVector x_f = Integrate(x_k, u, dt);
        x_k = x_f;
    }

    return J;
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void OCP<TSIM, STOP_OPT, HM>::CalculateCostJacobian(unsigned k,  //
                                                    const typename TSIM::StateVector& x,
                                                    const typename TSIM::InputVector& u,  //
                                                    const OcpData<TSIM>& ocp_data,  //
                                                    typename TSIM::StateVector& dJ_dx,
                                                    typename TSIM::InputVector& dJ_du) const
{
    IterOutput y_ref_begin, y_ref_end;
    IterState x_ref_begin, x_ref_end;
    ocp_data.GetReferenceStatesAt(k, x_ref_begin, x_ref_end);
    ocp_data.GetReferenceOutputsAt(k, y_ref_begin, y_ref_end);
    double dt = ocp_data.GetIntegrationSampleTimeAt(k);

    typename TSIM::StateVector x_k = x;
    typename TSIM::StateVector dJ_dxk;
    typename TSIM::InputVector dJ_duk;
    typename TSIM::StateStateMatrix Ak = typename TSIM::StateStateMatrix::Identity();

    auto y_ref_k = y_ref_begin;
    auto x_ref_k = x_ref_begin;
    dJ_dx = typename TSIM::StateVector::Zero();
    dJ_du = typename TSIM::InputVector::Zero();

    for (; y_ref_k != y_ref_end; ++y_ref_k, ++x_ref_k) {
        m_cost_functions.GetCostJacobianDt(x_k, u, *y_ref_k, *x_ref_k, ocp_data.GetFinalTransform(),
                                           ocp_data.GetStateErrorWeightAt(k), ocp_data.GetInputErrorWeightAt(k),
                                           ocp_data.GetOutputErrorWeightAt(k), dJ_dxk, dJ_duk);

        dJ_dx += trans(Ak) * dJ_dxk * dt;
        dJ_du += dJ_duk * dt;

        typename TSIM::StateVector x_f = Integrate(x_k, u, dt);
        x_k = x_f;
    }
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void OCP<TSIM, STOP_OPT, HM>::CalculateCostHessian(unsigned k, typename TSIM::StateVector const& x,
                                                   typename TSIM::InputVector const& u, OcpData<TSIM> const& ocp_data,
                                                   ConstraintVector const& lam_lbg, ConstraintVector const& lam_ubg,
                                                   typename TSIM::StateVector& dJ_dx, typename TSIM::InputVector& dJ_du,
                                                   typename TSIM::StateStateMatrix& d2L_dx2,
                                                   typename TSIM::StateInputMatrix& d2L_dx_duT,
                                                   typename TSIM::InputInputMatrix& d2L_du2) const
{
    IterOutput y_ref_begin, y_ref_end;
    IterState x_ref_begin, x_ref_end;
    ocp_data.GetReferenceStatesAt(k, x_ref_begin, x_ref_end);
    ocp_data.GetReferenceOutputsAt(k, y_ref_begin, y_ref_end);
    double dt = ocp_data.GetIntegrationSampleTimeAt(k);

    typename TSIM::StateVector x_k = x;
    typename TSIM::StateVector dJ_dx_k;
    typename TSIM::InputVector dJ_du_k;
    typename TSIM::StateStateMatrix d2J_dx2_k;
    typename TSIM::StateInputMatrix d2J_dx_duT_k;
    typename TSIM::InputInputMatrix d2J_du2_k;
    typename TSIM::StateStateMatrix Ak = TSIM::StateStateMatrix::Identity();
    auto y_ref_k = y_ref_begin;
    auto x_ref_k = x_ref_begin;
    dJ_dx = TSIM::StateVector::Zero();
    dJ_du = TSIM::InputVector::Zero();
    d2L_dx2 = TSIM::StateStateMatrix::Zero();
    d2L_dx_duT = TSIM::StateInputMatrix::Zero();
    d2L_du2 = TSIM::InputInputMatrix::Zero();

    for (; y_ref_k != y_ref_end; ++y_ref_k, ++x_ref_k) {
        m_cost_functions.GetCostHessianDt(x_k, u, *y_ref_k, *x_ref_k, ocp_data.GetFinalTransform(),
                                          ocp_data.GetStateErrorWeightAt(k), ocp_data.GetInputErrorWeightAt(k),
                                          ocp_data.GetOutputErrorWeightAt(k), dJ_dx_k, dJ_du_k, d2J_dx2_k, d2J_dx_duT_k,
                                          d2J_du2_k);

        dJ_dx += trans(Ak) * dJ_dx_k * dt;
        dJ_du += dJ_du_k * dt;

        d2L_dx2 +=
            (trans(Ak) * d2J_dx2_k * Ak + ocp_data.GetLevenbergMarquardt() * TSIM::StateStateMatrix::Identity()) * dt;
        d2L_dx_duT += trans(Ak) * d2J_dx_duT_k * dt;
        d2L_du2 += (d2J_du2_k + ocp_data.GetLevenbergMarquardt() * TSIM::InputInputMatrix::Identity()) * dt;

        typename TSIM::StateVector x_f = Integrate(x_k, u, dt);
        x_k = x_f;
    }

    const auto u_min = ocp_data.GetInputLowerBound() * ocp_data.GetAlpha();
    const auto u_max = ocp_data.GetInputUpperBound() * ocp_data.GetAlpha();
    m_constraint_in_lagrangian_functions.AddHessianOfConstraints(x, u, lam_lbg, lam_ubg, u_min, u_max, d2L_dx2,
                                                                 d2L_dx_duT, d2L_du2);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void OCP<TSIM, STOP_OPT, HM>::CalculateDifferenceEquation(double ts, typename TSIM::StateStateMatrix& A,
                                                          typename TSIM::StateInputMatrix& B)
{
    auto const I = Matrix<TSIM::Dimension::NQ, TSIM::Dimension::NQ>::Identity();
    auto const O = Matrix<TSIM::Dimension::NQ, TSIM::Dimension::NQ>::Zero();

    A << I, ts * I, O, I;

    B << ts * ts / 2. * I, ts * I;
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void OCP<TSIM, STOP_OPT, HM>::CalculateConstraintJacobian(typename TSIM::StateVector const& x,
                                                          typename TSIM::InputVector const& u,
                                                          OcpData<TSIM> const& ocp_data, ConstraintVector& g,
                                                          ConstraintStateMatrix& dg_dx,
                                                          ConstraintInputMatrix& dg_du) const
{
    m_constraint_functions.CalculateConstraintJacobian(x, u, ocp_data, g, dg_dx, dg_du);
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
typename TSIM::StateVector OCP<TSIM, STOP_OPT, HM>::Integrate(typename TSIM::StateVector const& x,
                                                              typename TSIM::InputVector const& u, double ts) const
{
    typename TSIM::StateVector x_f;

    MakeStateView<TSIM>(x_f).GetPosition() =
        MakeStateView<TSIM>(x).GetPosition() + MakeStateView<TSIM>(x).GetVelocity() * ts + u * (ts * ts / 2.);
    MakeStateView<TSIM>(x_f).GetVelocity() = MakeStateView<TSIM>(x).GetVelocity() + u * ts;

    return x_f;
}

template <typename TSIM, StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
void OCP<TSIM, STOP_OPT, HM>::CalculateConstraintBounds(typename TSIM::StateVector const& x,
                                                        typename TSIM::InputVector const& u,
                                                        OcpData<TSIM> const& ocp_data, ConstraintVector& lbg,
                                                        ConstraintVector& ubg) const
{
    m_constraint_functions.CalculateConstraintBounds(x, u, ocp_data, lbg, ubg);
}

}  //namespace mpmca::control