/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/control/simulator.hpp"
#include "mpmca/linear_algebra.hpp"

namespace mpmca::control {

template <class TSIM>
Simulator<TSIM>::Simulator()
    : m_output_function(TSIM::Output_functions())
    , m_output_jacobian_function(TSIM::OutputJacobian_functions())
    , m_constraint_function(TSIM::Inequality_functions())
    , m_head_pva_function(TSIM::HeadPva_functions())
    , m_head_inertial_function(TSIM::HeadInertial_functions())
    , m_info_function(TSIM::Info_functions())
    , m_simulator_data_function(TSIM::SimulatorData_functions())
{
    if (m_output_function.n_row_in(0) != TSIM::Dimension::NX || m_output_function.n_col_in(0) != 1)
        throw std::logic_error("Invalid size of input argument 0 in function 'output'");

    if (m_output_function.n_row_in(1) != TSIM::Dimension::NZ || m_output_function.n_col_in(1) != 1)
        throw std::logic_error("Invalid size of input argument 1 in function 'output'");

    if (m_output_function.n_row_in(2) != TSIM::Dimension::NU || m_output_function.n_col_in(2) != 1)
        throw std::logic_error("Invalid size of input argument 2 in function 'output'");

    if (m_output_function.n_row_in(3) != TSIM::Dimension::NP || m_output_function.n_col_in(3) != 1)
        throw std::logic_error("Invalid size of input argument 3 in function 'output'");

    if (m_output_function.n_row_in(4) != 4 || m_output_function.n_col_in(4) != 4)
        throw std::logic_error("Invalid size of input argument 4 in function 'output'");

    if (m_output_function.n_row_out(0) != TSIM::Dimension::NY || m_output_function.n_col_out(0) != 1)
        throw std::logic_error("Invalid size of output argument 0 in function 'output'");

    m_simulator_data_function({}, {m_x_lb.data(), m_x_ub.data(), m_x0.data(), m_u_lb.data(), m_u_ub.data(), m_u0.data(),
                                   m_g_lb.data(), m_g_ub.data(), m_T.data(), m_gravity.data(), m_y0.data()});

    m_xN_lb = m_x_lb;
    m_xN_ub = m_x_ub;
}

template <typename TSIM>
typename TSIM::OutputVector Simulator<TSIM>::GetOutput(typename TSIM::StateVector const& x,
                                                       typename TSIM::InputVector const& u,
                                                       TransformationMatrix const& T_HP) const
{
    typename TSIM::OutputVector y;
    m_output_function({x.data(), nullptr, u.data(), nullptr, T_HP.data()}, {y.data()});

    return y;
}

template <typename TSIM>
void Simulator<TSIM>::GetOutputJacobian(typename TSIM::StateVector const& x, typename TSIM::InputVector const& u,
                                        TransformationMatrix const& T_HP, typename TSIM::OutputVector& y,
                                        typename TSIM::OutputStateMatrix& dy_dx,
                                        typename TSIM::OutputInputMatrix& dy_du) const
{
    m_output_jacobian_function({x.data(), nullptr, u.data(), nullptr, T_HP.data()},
                               {y.data(), dy_dx.data(), dy_du.data()});
}

template <typename TSIM>
PoseRotMatVector Simulator<TSIM>::GetHeadPva(typename TSIM::StateVector const& x, typename TSIM::InputVector const& u,
                                             TransformationMatrix const& T_HP) const
{
    PoseRotMatVector y;
    m_head_pva_function({x.data(), nullptr, u.data(), nullptr, T_HP.data()}, {y.data()});

    return y;
}

template <typename TSIM>
InertialVector Simulator<TSIM>::GetHeadInertial(typename TSIM::StateVector const& x,
                                                typename TSIM::InputVector const& u,
                                                TransformationMatrix const& T_HP) const
{
    InertialVector y;
    m_head_inertial_function({x.data(), nullptr, u.data(), nullptr, T_HP.data()}, {y.data()});

    return y;
}

template <typename TSIM>
typename TSIM::StateVector Simulator<TSIM>::Integrate(typename TSIM::StateVector const& x,
                                                      typename TSIM::InputVector const& u, double ts) const
{
    typename TSIM::StateVector xf;

    MakeStateView<TSIM>(xf).GetPosition() =
        MakeStateView<TSIM>(x).GetPosition() + MakeStateView<TSIM>(x).GetVelocity() * ts + u * (ts * ts / 2.);
    MakeStateView<TSIM>(xf).GetVelocity() = MakeStateView<TSIM>(x).GetVelocity() + u * ts;

    return xf;
}

template <typename TSIM>
typename TSIM::ConstraintVector Simulator<TSIM>::GetConstraint(typename TSIM::StateVector const& x,
                                                               typename TSIM::InputVector const& u) const
{
    typename TSIM::ConstraintVector g;
    m_constraint_function({x.data(), nullptr, u.data(), nullptr}, {g.data()});

    return g;
}

template <typename TSIM>
typename TSIM::InfoVector Simulator<TSIM>::GetInfo() const
{
    typename TSIM::InfoVector info;
    m_info_function({}, {info.data()});
    return info;
}

template <typename TSIM>
std::string Simulator<TSIM>::GetName() const
{
    const char* name_c = TSIM::name_name_out(0);
    std::string name_str(name_c);
    return name_str;
}

template <typename TSIM>
typename TSIM::StateVector const& Simulator<TSIM>::GetStateLowerBound() const
{
    return m_x_lb;
}

template <typename TSIM>
typename TSIM::StateVector const& Simulator<TSIM>::GetStateUpperBound() const
{
    return m_x_ub;
}

template <typename TSIM>
typename TSIM::StateVector const& Simulator<TSIM>::GetNominalState() const
{
    return m_x0;
}

template <typename TSIM>
typename TSIM::InputVector const& Simulator<TSIM>::GetInputLowerBound() const
{
    return m_u_lb;
}

template <typename TSIM>
typename TSIM::InputVector const& Simulator<TSIM>::GetInputUpperBound() const
{
    return m_u_ub;
}

template <typename TSIM>
typename TSIM::InputVector const& Simulator<TSIM>::GetNominalInput() const
{
    return m_u0;
}

template <typename TSIM>
typename TSIM::ConstraintVector const& Simulator<TSIM>::GetConstraintLowerBound() const
{
    return m_g_lb;
}

template <typename TSIM>
typename TSIM::ConstraintVector const& Simulator<TSIM>::GetConstraintUpperBound() const
{
    return m_g_ub;
}

template <typename TSIM>
typename TSIM::StateVector const& Simulator<TSIM>::GetTerminalStateLowerBound() const
{
    return m_xN_lb;
}

template <typename TSIM>
typename TSIM::StateVector const& Simulator<TSIM>::GetTerminalStateUpperBound() const
{
    return m_xN_ub;
}

template <typename TSIM>
TransformationMatrix const& Simulator<TSIM>::GetFinalTransform() const
{
    return m_T;
}

template <typename TSIM>
Vector<3> const& Simulator<TSIM>::GetGravity() const
{
    return m_gravity;
}

template <typename TSIM>
typename TSIM::OutputVector const& Simulator<TSIM>::GetNominalOutput() const
{
    return m_y0;
}
}  //namespace mpmca::control