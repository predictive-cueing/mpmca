/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/control/from_tmpc/casadi_interface/generated_function.hpp"
#include "mpmca/control/hessian_calculation_method.hpp"
#include "mpmca/control/ocp_data.hpp"
#include "mpmca/control/state.hpp"
#include "mpmca/control/state_stoppability_opt.hpp"
#include "mpmca/linear_algebra.hpp"

namespace mpmca::control {

template <typename TSIM, StateStoppabilityOption STOP_OPT>
class Constraints
{
    casadi_interface::GeneratedFunction m_inequality_jacobian_function;

    static constexpr std::size_t NC_STOP = (STOP_OPT == StateStoppabilityOption::kNone) ? 0 : TSIM::Dimension::NX;
    static constexpr std::size_t NC = TSIM::Dimension::NC + NC_STOP;

    using StoppabilityVector = Vector<NC_STOP>;
    using StoppabilityInputMatrix = Matrix<NC_STOP, TSIM::Dimension::NU>;
    using StoppabilityStateMatrix = Matrix<NC_STOP, TSIM::Dimension::NX>;
    using ConstraintVector = Vector<NC>;
    using ConstraintInputMatrix = Matrix<NC, TSIM::Dimension::NU>;
    using ConstraintStateMatrix = Matrix<NC, TSIM::Dimension::NX>;

    void CalculateSimulatorConstraintJacobian(typename TSIM::StateVector const& x, typename TSIM::InputVector const& u,
                                              OcpData<TSIM> const& ocp_data, typename TSIM::ConstraintVector& g,
                                              typename TSIM::ConstraintStateMatrix& dg_dx,
                                              typename TSIM::ConstraintInputMatrix& dg_du) const;

    void CalculateStoppabilityConstraintJacobian(typename TSIM::StateVector const& x, OcpData<TSIM> const& ocp_data,
                                                 StoppabilityVector& g, StoppabilityStateMatrix& dg_dx,
                                                 StoppabilityInputMatrix& dg_du) const;

    void CalculateStoppabilityConstraintJacobianNone(typename TSIM::StateVector const& x, OcpData<TSIM> const& ocp_data,
                                                     StoppabilityVector& g, StoppabilityStateMatrix& dg_dx,
                                                     StoppabilityInputMatrix& dg_du) const;

    void CalculateStoppabilityConstraintJacobianOriginalExpression(typename TSIM::StateVector const& x,
                                                                   OcpData<TSIM> const& ocp_data, StoppabilityVector& g,
                                                                   StoppabilityStateMatrix& dg_dx,
                                                                   StoppabilityInputMatrix& dg_du) const;

    void CalculateStoppabilityConstraintJacobianExpressionWithoutDenominator(typename TSIM::StateVector const& x,
                                                                             OcpData<TSIM> const& ocp_data,
                                                                             StoppabilityVector& g,
                                                                             StoppabilityStateMatrix& dg_dx,
                                                                             StoppabilityInputMatrix& dg_du) const;

    void CalculateConstraintBoundsNone(typename TSIM::StateVector const& x, typename TSIM::InputVector const& u,
                                       OcpData<TSIM> const& ocp_data, ConstraintVector& lbg,
                                       ConstraintVector& ubg) const;

    void CalculateConstraintBoundsOriginalExpression(typename TSIM::StateVector const& x,
                                                     typename TSIM::InputVector const& u, OcpData<TSIM> const& ocp_data,
                                                     ConstraintVector& lbg, ConstraintVector& ubg) const;

    void CalculateConstraintBoundsExpressionWithoutDenominator(typename TSIM::StateVector const& x,
                                                               typename TSIM::InputVector const& u,
                                                               OcpData<TSIM> const& ocp_data, ConstraintVector& lbg,
                                                               ConstraintVector& ubg) const;

  public:
    Constraints();

    void CalculateConstraintJacobian(typename TSIM::StateVector const& x, typename TSIM::InputVector const& u,
                                     OcpData<TSIM> const& ocp_data, ConstraintVector& g, ConstraintStateMatrix& dg_dx,
                                     ConstraintInputMatrix& dg_du) const;

    void CalculateConstraintBounds(typename TSIM::StateVector const& x, typename TSIM::InputVector const& u,
                                   OcpData<TSIM> const& ocp_data, ConstraintVector& lbg, ConstraintVector& ubg) const;
};

template <typename TSIM, StateStoppabilityOption STOP_OPT>
Constraints<TSIM, STOP_OPT>::Constraints()
    : m_inequality_jacobian_function(TSIM::InequalityJacobian_functions())
{
}

template <typename TSIM, StateStoppabilityOption STOP_OPT>
void Constraints<TSIM, STOP_OPT>::CalculateConstraintJacobian(typename TSIM::StateVector const& x,
                                                              typename TSIM::InputVector const& u,
                                                              OcpData<TSIM> const& ocp_data, ConstraintVector& g,
                                                              ConstraintStateMatrix& dg_dx,
                                                              ConstraintInputMatrix& dg_du) const
{
    typename TSIM::ConstraintVector g_sim;
    typename TSIM::ConstraintStateMatrix dg_dx_sim;
    typename TSIM::ConstraintInputMatrix dg_du_sim;
    CalculateSimulatorConstraintJacobian(x, u, ocp_data, g_sim, dg_dx_sim, dg_du_sim);

    StoppabilityVector g_stop;
    StoppabilityInputMatrix dg_du_stop;
    StoppabilityStateMatrix dg_dx_stop;
    CalculateStoppabilityConstraintJacobian(x, ocp_data, g_stop, dg_dx_stop, dg_du_stop);

    g << g_sim, g_stop;
    dg_dx << dg_dx_sim, dg_dx_stop;
    dg_du << dg_du_sim, dg_du_stop;
}

template <typename TSIM, StateStoppabilityOption STOP_OPT>
void Constraints<TSIM, STOP_OPT>::CalculateSimulatorConstraintJacobian(
    typename TSIM::StateVector const& x, typename TSIM::InputVector const& u, OcpData<TSIM> const& ocp_data,
    typename TSIM::ConstraintVector& g_sim, typename TSIM::ConstraintStateMatrix& dg_dx_sim,
    typename TSIM::ConstraintInputMatrix& dg_du_sim) const
{
    m_inequality_jacobian_function({x.data(), nullptr, u.data(), nullptr},
                                   {g_sim.data(), dg_dx_sim.data(), dg_du_sim.data()});
}

template <typename TSIM, StateStoppabilityOption STOP_OPT>
void Constraints<TSIM, STOP_OPT>::CalculateStoppabilityConstraintJacobian(typename TSIM::StateVector const& x,
                                                                          OcpData<TSIM> const& ocp_data,
                                                                          StoppabilityVector& g,
                                                                          StoppabilityStateMatrix& dg_dx,
                                                                          StoppabilityInputMatrix& dg_du) const
{
    if constexpr (STOP_OPT == StateStoppabilityOption::kNone) {
        CalculateStoppabilityConstraintJacobianNone(x, ocp_data, g, dg_dx, dg_du);
    }
    else if constexpr (STOP_OPT == StateStoppabilityOption::kOriginalExpression) {
        CalculateStoppabilityConstraintJacobianOriginalExpression(x, ocp_data, g, dg_dx, dg_du);
    }
    else if constexpr (STOP_OPT == StateStoppabilityOption::kExpressionWithoutDenominator) {
        CalculateStoppabilityConstraintJacobianExpressionWithoutDenominator(x, ocp_data, g, dg_dx, dg_du);
    }
}

template <typename TSIM, StateStoppabilityOption STOP_OPT>
void Constraints<TSIM, STOP_OPT>::CalculateStoppabilityConstraintJacobianNone(typename TSIM::StateVector const& x,
                                                                              OcpData<TSIM> const& ocp_data,
                                                                              StoppabilityVector& g,
                                                                              StoppabilityStateMatrix& dg_dx,
                                                                              StoppabilityInputMatrix& dg_du) const
{
}

template <typename TSIM, StateStoppabilityOption STOP_OPT>
void Constraints<TSIM, STOP_OPT>::CalculateStoppabilityConstraintJacobianOriginalExpression(
    typename TSIM::StateVector const& x, OcpData<TSIM> const& ocp_data, StoppabilityVector& g,
    StoppabilityStateMatrix& dg_dx, StoppabilityInputMatrix& dg_du) const
{
    const auto q = MakeStateView<TSIM>(x).GetPosition();
    const auto v = MakeStateView<TSIM>(x).GetVelocity();
    const auto q_min = MakeStateView<TSIM>(ocp_data.GetStateLowerBound()).GetPosition();
    const auto q_max = MakeStateView<TSIM>(ocp_data.GetStateUpperBound()).GetPosition();
    const auto u_min_k = ocp_data.GetInputLowerBound() * ocp_data.GetAlpha();
    const auto u_max_k = ocp_data.GetInputUpperBound() * ocp_data.GetAlpha();
    const auto v_over_u_min = v.cwiseQuotient(u_min_k);
    const auto v_over_u_max = v.cwiseQuotient(u_max_k);
    const auto v2_over_uMin = v.cwiseProduct(v_over_u_min);
    const auto v2_over_uMax = v.cwiseProduct(v_over_u_max);

    g << -v2_over_uMin / 2. + (q - q_max), v2_over_uMax / 2. + (q_min - q);

    dg_dx << TSIM::InputInputMatrix::Identity(), -typename TSIM::InputInputMatrix(v_over_u_min.asDiagonal()),
        -TSIM::InputInputMatrix::Identity(), typename TSIM::InputInputMatrix(v_over_u_max.asDiagonal());

    dg_du = StoppabilityInputMatrix::Zero();
}

template <typename TSIM, StateStoppabilityOption STOP_OPT>
void Constraints<TSIM, STOP_OPT>::CalculateStoppabilityConstraintJacobianExpressionWithoutDenominator(
    typename TSIM::StateVector const& x, OcpData<TSIM> const& ocp_data, StoppabilityVector& g,
    StoppabilityStateMatrix& dg_dx, StoppabilityInputMatrix& dg_du) const
{
    const auto q = MakeStateView<TSIM>(x).GetPosition();
    const auto v = MakeStateView<TSIM>(x).GetVelocity();
    const auto q_min = MakeStateView<TSIM>(ocp_data.GetStateLowerBound()).GetPosition();
    const auto q_max = MakeStateView<TSIM>(ocp_data.GetStateUpperBound()).GetPosition();
    const auto u_min_k = ocp_data.GetInputLowerBound() * ocp_data.GetAlpha();
    const auto u_max_k = ocp_data.GetInputUpperBound() * ocp_data.GetAlpha();

    g << v.cwiseAbs2() + 2. * (q_max - q).cwiseProduct(u_min_k), v.cwiseAbs2() + 2. * (q_min - q).cwiseProduct(u_max_k);

    dg_dx << -2. * typename TSIM::InputInputMatrix(u_min_k.asDiagonal()),
        2. * typename TSIM::InputInputMatrix(v.asDiagonal()),
        -2. * typename TSIM::InputInputMatrix(u_max_k.asDiagonal()),
        2. * typename TSIM::InputInputMatrix(v.asDiagonal());

    dg_du = StoppabilityInputMatrix::Zero();
}

template <typename TSIM, StateStoppabilityOption STOP_OPT>
void Constraints<TSIM, STOP_OPT>::CalculateConstraintBounds(typename TSIM::StateVector const& x,
                                                            typename TSIM::InputVector const& u,
                                                            OcpData<TSIM> const& ocp_data, ConstraintVector& lbg,
                                                            ConstraintVector& ubg) const
{
    if constexpr (STOP_OPT == StateStoppabilityOption::kNone) {
        CalculateConstraintBoundsNone(x, u, ocp_data, lbg, ubg);
    }
    else if constexpr (STOP_OPT == StateStoppabilityOption::kOriginalExpression) {
        CalculateConstraintBoundsOriginalExpression(x, u, ocp_data, lbg, ubg);
    }
    else if constexpr (STOP_OPT == StateStoppabilityOption::kExpressionWithoutDenominator) {
        CalculateConstraintBoundsExpressionWithoutDenominator(x, u, ocp_data, lbg, ubg);
    }
}

template <typename TSIM, StateStoppabilityOption STOP_OPT>
void Constraints<TSIM, STOP_OPT>::CalculateConstraintBoundsNone(typename TSIM::StateVector const& x,
                                                                typename TSIM::InputVector const& u,
                                                                OcpData<TSIM> const& ocp_data, ConstraintVector& lbg,
                                                                ConstraintVector& ubg) const
{
    lbg = ocp_data.GetLowerBoundConstraints();
    ubg = ocp_data.GetUpperBoundConstraints();
}

template <typename TSIM, StateStoppabilityOption STOP_OPT>
void Constraints<TSIM, STOP_OPT>::CalculateConstraintBoundsExpressionWithoutDenominator(
    typename TSIM::StateVector const& x, typename TSIM::InputVector const& u, OcpData<TSIM> const& ocp_data,
    ConstraintVector& lbg, ConstraintVector& ubg) const
{
    typename TSIM::ConstraintVector lbg_sim = ocp_data.GetLowerBoundConstraints();
    typename TSIM::ConstraintVector ubg_sim = ocp_data.GetUpperBoundConstraints();

    const auto q_min = MakeStateView<TSIM>(ocp_data.GetStateLowerBound()).GetPosition();
    const auto q_max = MakeStateView<TSIM>(ocp_data.GetStateUpperBound()).GetPosition();
    const auto u_min_k = ocp_data.GetInputLowerBound() * ocp_data.GetAlpha();
    const auto u_max_k = ocp_data.GetInputUpperBound() * ocp_data.GetAlpha();

    const auto lbg_stop_1 = 2. * (q_max - q_min).cwiseProduct(u_min_k);
    const auto lbg_stop_2 = 2. * (q_min - q_max).cwiseProduct(u_max_k);

    typename TSIM::StateVector lbg_stop;
    lbg_stop << lbg_stop_1, lbg_stop_2;

    const auto ubg_stop = TSIM::StateVector::Zero();

    lbg << lbg_sim, lbg_stop;
    ubg << ubg_sim, ubg_stop;
}

template <typename TSIM, StateStoppabilityOption STOP_OPT>
void Constraints<TSIM, STOP_OPT>::CalculateConstraintBoundsOriginalExpression(typename TSIM::StateVector const& x,
                                                                              typename TSIM::InputVector const& u,
                                                                              OcpData<TSIM> const& ocp_data,
                                                                              ConstraintVector& lbg,
                                                                              ConstraintVector& ubg) const
{
    typename TSIM::ConstraintVector lbg_sim = ocp_data.GetLowerBoundConstraints();
    typename TSIM::ConstraintVector ubg_sim = ocp_data.GetUpperBoundConstraints();

    const auto q_min = MakeStateView<TSIM>(ocp_data.GetStateLowerBound()).GetPosition();
    const auto q_max = MakeStateView<TSIM>(ocp_data.GetStateUpperBound()).GetPosition();
    const auto lbg_stop = (q_min - q_max);

    const auto ubg_stop = TSIM::StateVector::Zero();

    lbg << lbg_sim, lbg_stop, lbg_stop;
    ubg << ubg_sim, ubg_stop;
}
}  //namespace mpmca::control