/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include "mpmca/control/templates/constraint_view.hpp"
#include "mpmca/control/templates/constraint_view_at.hpp"

namespace mpmca::control::templates {

template <typename T>
class ConstraintViewAt<components::Hexapod, T>
{
    T& m_constraint_vector;
    std::size_t m_offset;

  public:
    ConstraintViewAt(T& g, std::size_t offset);
    decltype(auto) GetLegLength();
    decltype(auto) GetLegLength() const;
    decltype(auto) GetLegVelocity();
    decltype(auto) GetLegVelocity() const;
    decltype(auto) GetLegAcceleration();
    decltype(auto) GetLegAcceleration() const;
};

template <typename T>
ConstraintViewAt<components::Hexapod, T>::ConstraintViewAt(T& constraint_vector, std::size_t offset)
    : m_constraint_vector{constraint_vector}
    , m_offset{offset}
{
}

template <typename T>
decltype(auto) ConstraintViewAt<components::Hexapod, T>::GetLegLength()
{
    return m_constraint_vector.template segment<6>(m_offset);
}

template <typename T>
decltype(auto) ConstraintViewAt<components::Hexapod, T>::GetLegLength() const
{
    return m_constraint_vector.template segment<6>(m_offset);
}

template <typename T>
decltype(auto) ConstraintViewAt<components::Hexapod, T>::GetLegVelocity()
{
    return m_constraint_vector.template segment<6>(m_offset + 6);
}

template <typename T>
decltype(auto) ConstraintViewAt<components::Hexapod, T>::GetLegVelocity() const
{
    return m_constraint_vector.template segment<6>(m_offset + 6);
}

template <typename T>
decltype(auto) ConstraintViewAt<components::Hexapod, T>::GetLegAcceleration()
{
    return m_constraint_vector.template segment<6>(m_offset + 12);
}

template <typename T>
decltype(auto) ConstraintViewAt<components::Hexapod, T>::GetLegAcceleration() const
{
    return m_constraint_vector.template segment<6>(m_offset + 12);
}
}  //namespace mpmca::control