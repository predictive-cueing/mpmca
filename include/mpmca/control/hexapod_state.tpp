/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/control/components.hpp"
#include "mpmca/control/templates/state_view.hpp"
#include "mpmca/control/templates/state_view_at.hpp"

namespace mpmca::control::templates {

template <typename T>
class StateViewAt<components::Hexapod, T>
{
    T& m_state_vector;
    std::size_t m_position_offset;
    std::size_t m_velocity_offset;

  public:
    StateViewAt(T& state_vector, std::size_t position_offset, std::size_t velocity_offset);

    double GetX();
    double GetX() const;
    double GetY();
    double GetY() const;
    double GetZ();
    double GetZ() const;

    double GetPhi();
    double GetPhi() const;
    double GetTheta();
    double GetTheta() const;
    double GetPsi();
    double GetPsi() const;

    double GetXd();
    double GetXd() const;
    double GetYd();
    double GetYd() const;
    double GetZd();
    double GetZd() const;

    double GetP();
    double GetP() const;
    double GetQ();
    double GetQ() const;
    double GetR();
    double GetR() const;
};

template <typename T>
StateViewAt<components::Hexapod, T>::StateViewAt(T& state_vector, std::size_t position_offset,
                                                 std::size_t velocity_offset)
    : m_state_vector(state_vector)
    , m_position_offset(position_offset)
    , m_velocity_offset(velocity_offset)
{
}
template <typename T>
double StateViewAt<components::Hexapod, T>::GetX()
{
    return m_state_vector(m_position_offset + 0);
}
template <typename T>
double StateViewAt<components::Hexapod, T>::GetX() const
{
    return m_state_vector(m_position_offset + 0);
}
template <typename T>
double StateViewAt<components::Hexapod, T>::GetY()
{
    return m_state_vector(m_position_offset + 1);
}
template <typename T>
double StateViewAt<components::Hexapod, T>::GetY() const
{
    return m_state_vector(m_position_offset + 1);
}
template <typename T>
double StateViewAt<components::Hexapod, T>::GetZ()
{
    return m_state_vector(m_position_offset + 2);
}
template <typename T>
double StateViewAt<components::Hexapod, T>::GetZ() const
{
    return m_state_vector(m_position_offset + 2);
}
template <typename T>
double StateViewAt<components::Hexapod, T>::GetPhi()
{
    return m_state_vector(m_position_offset + 3);
}
template <typename T>
double StateViewAt<components::Hexapod, T>::GetPhi() const
{
    return m_state_vector(m_position_offset + 3);
}
template <typename T>
double StateViewAt<components::Hexapod, T>::GetTheta()
{
    return m_state_vector(m_position_offset + 4);
}
template <typename T>
double StateViewAt<components::Hexapod, T>::GetTheta() const
{
    return m_state_vector(m_position_offset + 4);
}
template <typename T>
double StateViewAt<components::Hexapod, T>::GetPsi()
{
    return m_state_vector(m_position_offset + 5);
}
template <typename T>
double StateViewAt<components::Hexapod, T>::GetPsi() const
{
    return m_state_vector(m_position_offset + 5);
}
template <typename T>
double StateViewAt<components::Hexapod, T>::GetXd()
{
    return m_state_vector(m_velocity_offset + 0);
}
template <typename T>
double StateViewAt<components::Hexapod, T>::GetXd() const
{
    return m_state_vector(m_velocity_offset + 0);
}
template <typename T>
double StateViewAt<components::Hexapod, T>::GetYd()
{
    return m_state_vector(m_velocity_offset + 1);
}
template <typename T>
double StateViewAt<components::Hexapod, T>::GetYd() const
{
    return m_state_vector(m_velocity_offset + 1);
}
template <typename T>
double StateViewAt<components::Hexapod, T>::GetZd()
{
    return m_state_vector(m_velocity_offset + 2);
}
template <typename T>
double StateViewAt<components::Hexapod, T>::GetZd() const
{
    return m_state_vector(m_velocity_offset + 2);
}
template <typename T>
double StateViewAt<components::Hexapod, T>::GetP()
{
    return m_state_vector(m_velocity_offset + 3);
}
template <typename T>
double StateViewAt<components::Hexapod, T>::GetP() const
{
    return m_state_vector(m_velocity_offset + 3);
}
template <typename T>
double StateViewAt<components::Hexapod, T>::GetQ()
{
    return m_state_vector(m_velocity_offset + 4);
}
template <typename T>
double StateViewAt<components::Hexapod, T>::GetQ() const
{
    return m_state_vector(m_velocity_offset + 4);
}
template <typename T>
double StateViewAt<components::Hexapod, T>::GetR()
{
    return m_state_vector(m_velocity_offset + 5);
}
template <typename T>
double StateViewAt<components::Hexapod, T>::GetR() const
{
    return m_state_vector(m_velocity_offset + 5);
}

}  //namespace mpmca::control