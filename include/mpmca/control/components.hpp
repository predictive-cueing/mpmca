/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/control/templates/component.hpp"

namespace mpmca::control::components {

static char transformation_matrix_name[] = "TransformationMatrix";
using TransformationMatrix = templates::Component<0, 0, 0, 0, 12, transformation_matrix_name>;
static char yaw_table_name[] = "YawTable";
using YawTable = templates::Component<1, 0, 0, 0, 6, yaw_table_name>;
static char xy_table_name[] = "XyTable";
using XyTable = templates::Component<2, 0, 0, 0, 6, xy_table_name>;
static char yz_tracks_name[] = "YzTracks";
using YzTracks = templates::Component<2, 0, 0, 0, 6, yz_tracks_name>;
static char gimbal_name[] = "Gimbal";
using Gimbal = templates::Component<3, 0, 0, 0, 6, gimbal_name>;
static char cfb_name[] = "ConstraintFreeBox";
using ConstraintFreeBox = templates::Component<6, 0, 0, 0, 6, cfb_name>;
static char hexapod_name[] = "Hexapod";
using Hexapod = templates::Component<6, 18, 0, 0, 42, hexapod_name>;
static char revolute_link_name[] = "RevoluteLink";
using RevoluteLink = templates::Component<1, 0, 0, 0, 8, revolute_link_name>;
static char prismatic_link_name[] = "PrismaticLink";
using PrismaticLink = templates::Component<1, 0, 0, 0, 8, prismatic_link_name>;

}  //namespace mpmca::control::components
