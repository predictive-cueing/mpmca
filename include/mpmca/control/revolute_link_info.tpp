/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/control/components.hpp"
#include "mpmca/control/templates/info_view.hpp"
#include "mpmca/control/templates/info_view_at.hpp"

namespace mpmca::control::templates {

template <typename T>
class InfoViewAt<components::RevoluteLink, T>
{
    T& m_info_vector;
    std::size_t m_offset;

  public:
    InfoViewAt(T& info, std::size_t offset);
    double GetThetaOffset() const;
    double GetD() const;
    double GetAlpha() const;
    double GetA() const;
    double& GetThetaOffset();
    double& GetD();
    double& GetAlpha();
    double& GetA();
    decltype(auto) GetActiveDoF();
    decltype(auto) GetActiveDoF() const;
};

template <typename T>
InfoViewAt<components::RevoluteLink, T>::InfoViewAt(T& info, std::size_t offset)
    : m_info_vector{info}
    , m_offset{offset}
{
}

template <typename T>
double& InfoViewAt<components::RevoluteLink, T>::GetThetaOffset()
{
    return m_info_vector.coeffRef(m_offset);
}

template <typename T>
double InfoViewAt<components::RevoluteLink, T>::GetThetaOffset() const
{
    return m_info_vector(m_offset);
}

template <typename T>
double& InfoViewAt<components::RevoluteLink, T>::GetD()
{
    return m_info_vector.coeffRef(m_offset + 1);
}

template <typename T>
double InfoViewAt<components::RevoluteLink, T>::GetD() const
{
    return m_info_vector(m_offset + 1);
}

template <typename T>
double& InfoViewAt<components::RevoluteLink, T>::GetAlpha()
{
    return m_info_vector.coeffRef(m_offset + 2);
}

template <typename T>
double InfoViewAt<components::RevoluteLink, T>::GetAlpha() const
{
    return m_info_vector(m_offset + 2);
}

template <typename T>
double& InfoViewAt<components::RevoluteLink, T>::GetA()
{
    return m_info_vector.coeffRef(m_offset + 3);
}

template <typename T>
double InfoViewAt<components::RevoluteLink, T>::GetA() const
{
    return m_info_vector(m_offset + 3);
}

template <typename T>
decltype(auto) InfoViewAt<components::RevoluteLink, T>::GetActiveDoF()
{
    return m_info_vector.template segment<4>(m_offset + 4);
}

template <typename T>
decltype(auto) InfoViewAt<components::RevoluteLink, T>::GetActiveDoF() const
{
    return m_info_vector.template segment<4>(m_offset + 4);
}

}  //namespace mpmca::control