/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once

#include <fstream>
#include <nlohmann/json.hpp>
#include <sstream>
#include <stdexcept>

#include "mpmca/control/json_tools.hpp"
#include "mpmca/types/inertial_vector.hpp"
#include "mpmca/types/pose_rot_mat_vector.hpp"
#include "mpmca/types/pose_vector.hpp"
namespace mpmca::control::testing {
template <typename TSIM>
struct TestData
{
    nlohmann::json m_json;

    typename TSIM::StateVector x;
    typename TSIM::AlgStateVector z;
    typename TSIM::InputVector u;
    typename TSIM::ParameterVector p;
    TransformationMatrix T_HP;
    typename TSIM::StateVector x_ref;
    typename TSIM::OutputVector y_ref;
    typename TSIM::StateVector wx;
    typename TSIM::InputVector wu;
    typename TSIM::OutputVector wy;
    typename TSIM::ConstraintVector lam_ubg;
    typename TSIM::ConstraintVector lam_lbg;
    typename TSIM::OutputVector y;
    typename TSIM::OutputStateMatrix dy_dx;
    typename TSIM::OutputInputMatrix dy_du;
    PoseRotMatVector head_pva;
    InertialVector head_inertial;
    typename TSIM::ConstraintVector g;
    typename TSIM::ConstraintStateMatrix dg_dx;
    typename TSIM::ConstraintInputMatrix dg_du;
    Matrix<1, 1> J;
    typename TSIM::StateVector dJ_dx;
    typename TSIM::InputVector dJ_du;
    typename TSIM::StateStateMatrix d2L_dx2_gauss_newton;
    typename TSIM::StateInputMatrix d2L_dx_du_gauss_newton;
    typename TSIM::InputInputMatrix d2L_du2_gauss_newton;
    typename TSIM::OutputStateStateMatrix d2y_dx2;
    typename TSIM::OutputStateInputMatrix d2y_dx_du;
    typename TSIM::OutputInputInputMatrix d2y_du2;
    typename TSIM::ConstraintStateStateMatrix d2g_dx2;
    typename TSIM::ConstraintStateInputMatrix d2g_dx_du;
    typename TSIM::ConstraintInputInputMatrix d2g_du2;
    typename TSIM::StateStateMatrix d2J_dx2;
    typename TSIM::StateInputMatrix d2J_dx_du;
    typename TSIM::InputInputMatrix d2J_du2;
    typename TSIM::StateStateMatrix d2Lg_dx2;
    typename TSIM::StateInputMatrix d2Lg_dx_du;
    typename TSIM::InputInputMatrix d2Lg_du2;
    typename TSIM::InfoVector info;
    std::string name;

    nlohmann::json ReadJson(std::string const& filename);
    TestData(std::string const& filename);
    TestData(nlohmann::json const& j);
};

template <typename TSIM>
nlohmann::json TestData<TSIM>::ReadJson(std::string const& filename)
{
    nlohmann::json j;
    std::ifstream(filename) >> j;

    return j;
}

template <typename TSIM>
TestData<TSIM>::TestData(std::string const& filename)
    : TestData(ReadJson(filename))
{
}

template <typename TSIM>
TestData<TSIM>::TestData(nlohmann::json const& j)
    : m_json(j)
    , x(j["x"].get<typename TSIM::StateVector>())
    , z(j["z"].get<typename TSIM::AlgStateVector>())
    , u(j["u"].get<typename TSIM::InputVector>())
    , p(j["p"].get<typename TSIM::ParameterVector>())
    , T_HP(j["T_HP"].get<TransformationMatrix>())
    , x_ref(j["x_ref"].get<typename TSIM::StateVector>())
    , y_ref(j["y_ref"].get<typename TSIM::OutputVector>())
    , wx(j["wx"].get<typename TSIM::StateVector>())
    , wu(j["wu"].get<typename TSIM::InputVector>())
    , wy(j["wy"].get<typename TSIM::OutputVector>())
    , lam_ubg(j["lam_ubg"].get<typename TSIM::ConstraintVector>())
    , lam_lbg(j["lam_lbg"].get<typename TSIM::ConstraintVector>())
    , y(j["y"].get<typename TSIM::OutputVector>())
    , dy_dx(j["dy_dx"].get<typename TSIM::OutputStateMatrix>())
    , dy_du(j["dy_du"].get<typename TSIM::OutputInputMatrix>())
    , head_pva(j["head_pva"].get<PoseRotMatVector>())
    , head_inertial(j["head_inertial"].get<InertialVector>())
    , g(j["g"].get<typename TSIM::ConstraintVector>())
    , dg_dx(j["dg_dx"].get<typename TSIM::ConstraintStateMatrix>())
    , dg_du(j["dg_du"].get<typename TSIM::ConstraintInputMatrix>())
    , J(j["J"].get<Matrix<1, 1>>())
    , dJ_dx(j["dJ_dx"].get<typename TSIM::StateVector>())
    , dJ_du(j["dJ_du"].get<typename TSIM::InputVector>())
    , d2L_dx2_gauss_newton(j["d2L_dx2_gauss_newton"].get<typename TSIM::StateStateMatrix>())
    , d2L_dx_du_gauss_newton(j["d2L_dx_du_gauss_newton"].get<typename TSIM::StateInputMatrix>())
    , d2L_du2_gauss_newton(j["d2L_du2_gauss_newton"].get<typename TSIM::InputInputMatrix>())
    , d2y_dx2(j["d2y_dx2"].get<typename TSIM::OutputStateStateMatrix>())
    , d2y_dx_du(j["d2y_dx_du"].get<typename TSIM::OutputStateInputMatrix>())
    , d2y_du2(j["d2y_du2"].get<typename TSIM::OutputInputInputMatrix>())
    , d2g_dx2(j["d2g_dx2"].get<typename TSIM::ConstraintStateStateMatrix>())
    , d2g_dx_du(j["d2g_dx_du"].get<typename TSIM::ConstraintStateInputMatrix>())
    , d2g_du2(j["d2g_du2"].get<typename TSIM::ConstraintInputInputMatrix>())
    , d2J_dx2(j["d2J_dx2"].get<typename TSIM::StateStateMatrix>())
    , d2J_dx_du(j["d2J_dx_du"].get<typename TSIM::StateInputMatrix>())
    , d2J_du2(j["d2J_du2"].get<typename TSIM::InputInputMatrix>())
    , d2Lg_dx2(j["d2Lg_dx2"].get<typename TSIM::StateStateMatrix>())
    , d2Lg_dx_du(j["d2Lg_dx_du"].get<typename TSIM::StateInputMatrix>())
    , d2Lg_du2(j["d2Lg_du2"].get<typename TSIM::InputInputMatrix>())
    , info(j["info"].get<typename TSIM::InfoVector>())
    , name(j["name"].get<std::string>())
{
}

}  //namespace mpmca::control::testing