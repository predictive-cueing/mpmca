/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <array>
#include <cmath>
#include <vector>

#include "mpmca/control/state.hpp"

namespace mpmca::control {
enum class StateSubsetToModify
{
    kNone,
    kPosition,
    kVelocity
};

template <typename TSIM, StateSubsetToModify S>
class Limiter
{
    std::array<bool, TSIM::Dimension::NQ> m_axes_to_limit;
    std::vector<unsigned> m_indexes;

    void MoveWithinBounds(double& p, double& v, double p_min, double p_max, double v_min, double v_max) const;
    void MakeStoppableByModifyingPosition(double& p, double& v, double p_min, double p_max, double u_min,
                                          double u_max) const;
    void MakeStoppableByModifyingVelocity(double& p, double& v, double p_min, double p_max, double u_min,
                                          double u_max) const;

    void MakeFeasibleByModifyingPosition(typename TSIM::StateVector& x, typename TSIM::StateVector const& x_min,
                                         typename TSIM::StateVector const& x_max,
                                         typename TSIM::InputVector const& u_min,
                                         typename TSIM::InputVector const& u_max) const;

    void MakeFeasibleByModifyingVelocity(typename TSIM::StateVector& x, typename TSIM::StateVector const& x_min,
                                         typename TSIM::StateVector const& x_max,
                                         typename TSIM::InputVector const& u_min,
                                         typename TSIM::InputVector const& u_max) const;

  public:
    Limiter(std::array<bool, TSIM::Dimension::NQ> const& axes_to_limit);

    void MakeFeasible(typename TSIM::StateVector& x, typename TSIM::StateVector const& x_min,
                      typename TSIM::StateVector const& x_max, typename TSIM::InputVector const& u_min,
                      typename TSIM::InputVector const& u_max) const;
};

template <typename TSIM, StateSubsetToModify S>
Limiter<TSIM, S>::Limiter(std::array<bool, TSIM::Dimension::NQ> const& axes_to_limit)
    : m_axes_to_limit{axes_to_limit}
{
    for (auto k = 0; k < m_axes_to_limit.size(); ++k) {
        if (m_axes_to_limit.at(k))
            m_indexes.push_back(k);
    }
}

template <typename TSIM, StateSubsetToModify S>
void Limiter<TSIM, S>::MakeFeasible(typename TSIM::StateVector& x, typename TSIM::StateVector const& x_min,
                                    typename TSIM::StateVector const& x_max, typename TSIM::InputVector const& u_min,
                                    typename TSIM::InputVector const& u_max) const
{
    if constexpr (S == StateSubsetToModify::kNone) {
        return;
    }
    else if constexpr (S == StateSubsetToModify::kPosition) {
        MakeFeasibleByModifyingPosition(x, x_min, x_max, u_min, u_max);
    }
    else if constexpr (S == StateSubsetToModify::kVelocity) {
        MakeFeasibleByModifyingVelocity(x, x_min, x_max, u_min, u_max);
    }
}

template <typename TSIM, StateSubsetToModify S>
void Limiter<TSIM, S>::MakeFeasibleByModifyingPosition(typename TSIM::StateVector& x,
                                                       typename TSIM::StateVector const& x_min,
                                                       typename TSIM::StateVector const& x_max,
                                                       typename TSIM::InputVector const& u_min,
                                                       typename TSIM::InputVector const& u_max) const
{
    auto p = MakeStateView<TSIM>(x).GetPosition();
    auto v = MakeStateView<TSIM>(x).GetVelocity();
    auto p_min = MakeStateView<TSIM>(x_min).GetPosition();
    auto v_min = MakeStateView<TSIM>(x_min).GetVelocity();
    auto p_max = MakeStateView<TSIM>(x_max).GetPosition();
    auto v_max = MakeStateView<TSIM>(x_max).GetVelocity();

    for (auto k : m_indexes) {
        MoveWithinBounds(p[k], v[k], p_min[k], p_max[k], v_min[k], v_max[k]);
        MakeStoppableByModifyingPosition(p[k], v[k], p_min[k], p_max[k], u_min[k], u_max[k]);
    }
}

template <typename TSIM, StateSubsetToModify S>
void Limiter<TSIM, S>::MakeFeasibleByModifyingVelocity(typename TSIM::StateVector& x,
                                                       typename TSIM::StateVector const& x_min,
                                                       typename TSIM::StateVector const& x_max,
                                                       typename TSIM::InputVector const& u_min,
                                                       typename TSIM::InputVector const& u_max) const
{
    auto p = MakeStateView<TSIM>(x).GetPosition();
    auto v = MakeStateView<TSIM>(x).GetVelocity();
    auto p_max = MakeStateView<TSIM>(x_max).GetPosition();
    auto v_max = MakeStateView<TSIM>(x_max).GetVelocity();
    auto p_min = MakeStateView<TSIM>(x_min).GetPosition();
    auto v_min = MakeStateView<TSIM>(x_min).GetVelocity();

    for (auto k : m_indexes) {
        MoveWithinBounds(p[k], v[k], p_min[k], p_max[k], v_min[k], v_max[k]);
        MakeStoppableByModifyingVelocity(p[k], v[k], p_min[k], p_max[k], u_min[k], u_max[k]);
    }
}

template <typename TSIM, StateSubsetToModify S>
void Limiter<TSIM, S>::MoveWithinBounds(double& p, double& v, double p_min, double p_max, double v_min,
                                        double v_max) const
{
    if (p > p_max) {
        p = p_max;
    }
    else if (p < p_min) {
        p = p_min;
    }

    if (v > v_max) {
        v = v_max;
    }
    else if (v < v_min) {
        v = v_min;
    }
}

template <typename TSIM, StateSubsetToModify S>
void Limiter<TSIM, S>::MakeStoppableByModifyingPosition(double& p, double& v, double p_min, double p_max, double u_min,
                                                        double u_max) const
{
    if (v > 0) {
        auto p_stop_max_k = v * v / (2. * u_min) + p_max;

        if (p > p_stop_max_k)
            p = p_stop_max_k;
    }
    else if (v < 0) {
        auto p_stop_min_k = v * v / (2. * u_max) + p_min;

        if (p < p_stop_min_k)
            p = p_stop_min_k;
    }
}

template <typename TSIM, StateSubsetToModify S>
void Limiter<TSIM, S>::MakeStoppableByModifyingVelocity(double& p, double& v, double p_min, double p_max, double u_min,
                                                        double u_max) const
{
    // This function assumes that p is within the bounds (p_min <= p <= p_max)
    if (v > 0) {
        auto v_stop_max_k = std::sqrt((p - p_max) * (2. * u_min));

        if (v > v_stop_max_k)
            v = v_stop_max_k;
    }
    else if (v < 0) {
        auto v_stop_min_k = -std::sqrt((p - p_min) * (2. * u_max));

        if (v < v_stop_min_k)
            v = v_stop_min_k;
    }
}
}  //namespace mpmca::control