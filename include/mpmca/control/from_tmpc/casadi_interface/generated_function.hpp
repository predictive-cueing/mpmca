/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
/*
 * CasADiGeneratedFunction.h
 *
 *  Created on: Apr 19, 2016
 *      Author: kotlyar
 */

#pragma once

#include <casadi/mem.h>

#include <array>
#include <initializer_list>
#include <sstream>
#include <string>
#include <vector>

namespace mpmca::control::casadi_interface {

class GeneratedFunction
{
  public:
    GeneratedFunction(casadi_functions const* f, std::string const& n = "Unknown CasADi function")
        : m_function(f)
        , m_name(n)
    {
        casadi_int sz_arg, sz_res, sz_iw, sz_w;
        if (int code = m_function.m_function->work(&sz_arg, &sz_res, &sz_iw, &sz_w) != 0)
            throw std::runtime_error(GetName() + "_fun_work() returned " + std::to_string(code));

        m_arg.resize(sz_arg);
        m_res.resize(sz_res);
        m_iw.resize(sz_iw);
        m_w.resize(sz_w);
    }

    GeneratedFunction(GeneratedFunction const& rhs)
        : m_function{rhs.m_function}
        , m_name{rhs.m_name}
        , m_arg(rhs.m_arg.size())
        , m_res(rhs.m_res.size())
        , m_iw(rhs.m_iw.size())
        , m_w(rhs.m_w.size())
    {
    }

    GeneratedFunction& operator=(GeneratedFunction const& rhs) = delete;

    std::string const& GetName() const { return m_name; }

    std::size_t n_in() const { return m_function.m_function->n_in(); }
    std::size_t n_out() const { return m_function.m_function->n_out(); }

    int n_row_in(int ind) const
    {
        if (ind < 0 || ind >= n_in())
            throw std::out_of_range("GeneratedFunction::n_row_in(): index is out of range");

        return m_function.m_function->sparsity_in(ind)[0];
    }

    int n_col_in(int ind) const
    {
        if (ind < 0 || ind >= n_in())
            throw std::out_of_range("GeneratedFunction::n_col_in(): index is out of range");

        return m_function.m_function->sparsity_in(ind)[1];
    }

    int n_row_out(int ind) const
    {
        if (ind < 0 || ind >= n_out())
            throw std::out_of_range("GeneratedFunction::n_row_out(): index is out of range");

        return m_function.m_function->sparsity_out(ind)[0];
    }

    int n_col_out(int ind) const
    {
        if (ind < 0 || ind >= n_out())
            throw std::out_of_range("GeneratedFunction::n_col_out(): index is out of range");

        return m_function.m_function->sparsity_out(ind)[1];
    }

    void operator()(std::initializer_list<const casadi_real*> arg, std::initializer_list<casadi_real*> res) const
    {
        if (arg.size() != n_in())
            throw std::invalid_argument("Invalid number of input arguments to " + GetName());

        if (res.size() != n_out())
            throw std::invalid_argument("Invalid number of output arguments to " + GetName());

        std::copy(arg.begin(), arg.end(), m_arg.begin());
        std::copy(res.begin(), res.end(), m_res.begin());

        m_function.m_function->eval(m_arg.data(), m_res.data(), m_iw.data(), m_w.data(), 0);
    }

  private:
    struct Functions
    {
        Functions(casadi_functions const* f)
            : m_function(f)
        {
            m_function->incref();
        }

        Functions(Functions const& f)
            : m_function(f.m_function)
        {
            m_function->incref();
        }

        Functions& operator=(Functions const& rhs) noexcept
        {
            if (&rhs != this) {
                m_function->decref();
                m_function = rhs.m_function;
                m_function->incref();
            }

            return *this;
        }

        ~Functions() { m_function->decref(); }

        casadi_functions const* m_function;
    };

    Functions const m_function;
    std::string const m_name;

    /*
    "To allow the evaluation to be performed efficiently with a small memory footprint, the
    user is expected to pass four work arrays. The function fname_work returns the length
    of these arrays, which have entries of type const double*, double*, int and double,
    respectively."

    CasADi user guide, section 5.3.
    http://casadi.sourceforge.net/users_guide/casadi-users_guide.pdf
    */
    mutable std::vector<casadi_real const*> m_arg;
    mutable std::vector<casadi_real*> m_res;
    mutable std::vector<casadi_int> m_iw;
    mutable std::vector<casadi_real> m_w;
};

}  //namespace mpmca::control::casadi_interface
