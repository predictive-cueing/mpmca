/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "lfs_in_output.hpp"

#include "coordinate_transform.hpp"
#include "mpmca/constants.hpp"
#include "mpmca/data_names.hpp"
#include "mpmca/messages/automotive_prediction_parameters.hpp"
#include "mpmca/messages/automotive_prediction_path.hpp"
#include "mpmca/messages/automotive_signals.hpp"
#include "mpmca/pipeline/data_bucket.hpp"
#include "mpmca/pipeline/in_output_registrar.hpp"
#include "mpmca/pipeline/pipeline.hpp"
#include "mpmca/utilities/math.hpp"

namespace mpmca::pipeline {
LfsInOutput::LfsInOutput(Pipeline& pipeline, const std::string& name, utilities::ConfigurationPtr config)
    : InOutput(pipeline, name, "LfsInOutput", config)
    , m_out_sim_receiver("OutSimReceiver", config->GetConfigurationObject("OutSimUdpReceiver"))
    , m_pth_reader(config->GetValue<std::string>("LiveForSpeedPthFilePath", "BL1.pth"))
    , m_path_ptr(m_pth_reader.GetPathPtr())
    , m_automotive_prediction_parameters(config->GetConfigurationObject("AutomotivePredictionParameters"))
    , m_received_from_lfs(0)
    , m_mpc_world_position{m_automotive_prediction_parameters.initial_world_X_vehicle_m,
                           m_automotive_prediction_parameters.initial_world_Y_vehicle_m,
                           m_automotive_prediction_parameters.initial_world_Z_vehicle_m}
    , m_mpc_world_rotation{0, 0, m_automotive_prediction_parameters.initial_world_psi_vehicle_rad}
    , m_mpc_body_translational_velocity{0, 0, 0}
    , m_mpc_body_euler_rates{0, 0, 0}
    , m_mpc_angular_velocity{0, 0, 0}
    , m_mpc_body_translational_acceleration{0, 0, 0}
    , m_mpc_body_gravity{0, 0, m_automotive_prediction_parameters.gravity_m_s2}
{
}

void LfsInOutput::UpdateSignalsNoConnection(messages::InertialHeadSignals& signals)
{
    signals.head_f_x_head_m_s2 = 0;
    signals.head_f_y_head_m_s2 = 0;
    signals.head_f_z_head_m_s2 = 9.81;
    signals.head_omega_x_head_rad_s = 0;
    signals.head_omega_y_head_rad_s = 0;
    signals.head_omega_z_head_rad_s = 0;
    signals.head_alpha_x_head_rad_s2 = 0;
    signals.head_alpha_y_head_rad_s2 = 0;
    signals.head_alpha_z_head_rad_s2 = 0;
}
void LfsInOutput::UpdateSignalsNoConnection(messages::VehicleStateSignals& signals)
{
    signals.world_X_vehicle_m = m_automotive_prediction_parameters.initial_world_X_vehicle_m;
    signals.world_Y_vehicle_m = m_automotive_prediction_parameters.initial_world_Y_vehicle_m;
    signals.world_Z_vehicle_m = m_automotive_prediction_parameters.initial_world_Z_vehicle_m;
    signals.world_phi_vehicle_rad = 0;
    signals.world_theta_vehicle_rad = 0;
    signals.world_psi_vehicle_rad = m_automotive_prediction_parameters.initial_world_psi_vehicle_rad;
    signals.body_u_vehicle_m_s = 0;
    signals.body_v_vehicle_m_s = 0;
    signals.body_w_vehicle_m_s = 0;
    signals.body_p_vehicle_rad_s = 0;
    signals.body_q_vehicle_rad_s = 0;
    signals.body_r_vehicle_rad_s = 0;
    signals.body_a_x_vehicle_m_s2 = 0;
    signals.body_a_y_vehicle_m_s2 = 0;
    signals.body_a_z_vehicle_m_s2 = 0;
    signals.body_p_dot_vehicle_rad_s2 = 0;
    signals.body_q_dot_vehicle_rad_s2 = 0;
    signals.body_r_dot_vehicle_rad_s2 = 0;
}
void LfsInOutput::UpdateSignalsNoConnection(messages::AutomotiveSignals& signals)
{
    signals.car_throttle_fraction = 0;
    signals.car_brake_fraction = 0;
    signals.car_throttle_dot_fraction_s = 0;
    signals.car_brake_dot_fraction_s = 0;
    signals.car_clutch_fraction = 0;
    signals.car_engine_torque_Nm = 0;
    signals.car_transmission_gear_integer = 0;
    signals.car_steer_rad = 0;
    signals.car_shift_integer = 0;
    signals.car_automatic_gear_change_boolean = 0;
    signals.car_engine_rotational_velocity_rad_s = 1000 / 60.0 * 2.0 * constants::kPi;
}
void LfsInOutput::ConvertLfsToMpcSignals()
{
    double x_phi = m_out_sim.OSMain.Roll;
    double x_theta = m_out_sim.OSMain.Pitch;
    double x_psi = m_out_sim.OSMain.Heading;
    std::array<double, 3> lfs_world_rotation{x_phi, x_theta, x_psi};

    // Converting LFS world-frame velocities to LFS body-frame components.
    double y_Xd = m_out_sim.OSMain.Vel.X;
    double y_Yd = m_out_sim.OSMain.Vel.Y;
    double y_Zd = m_out_sim.OSMain.Vel.Z;
    std::array<double, 3> lfs_world_translational_rates{y_Xd, y_Yd, y_Zd};

    auto lfs_body_velocity = utilities::Math::WorldToBody<double>(lfs_world_translational_rates, lfs_world_rotation);

    // Converting LFS world-frame accelerations to LFS body-frame components.
    double y_Xdd = m_out_sim.OSMain.Accel.X;
    double y_Ydd = m_out_sim.OSMain.Accel.Y;
    double y_Zdd = m_out_sim.OSMain.Accel.Z;
    std::array<double, 3> lfs_world_translational_acceleration{y_Xdd, y_Ydd, y_Zdd};
    auto lfs_body_translational_acceleration =
        utilities::Math::WorldToBody<double>(lfs_world_translational_acceleration, lfs_world_rotation);

    // // Converting LFS world-frame rotational rates to LFS body-frame components.
    double y_phi_d = m_out_sim.OSMain.AngVel.X;
    double y_theta_d = m_out_sim.OSMain.AngVel.Y;
    double y_psi_d = m_out_sim.OSMain.AngVel.Z;

    auto lfs_body_euler_rates = utilities::Math::WorldToBody<double>({y_phi_d, y_theta_d, y_psi_d}, lfs_world_rotation);

    // Converting LFS to MPC

    double x_X = (double)m_out_sim.OSMain.Pos.X / 65536.0;
    double x_Y = (double)m_out_sim.OSMain.Pos.Y / 65536.0;
    double x_Z = (double)m_out_sim.OSMain.Pos.Z / 65536.0;

    m_mpc_world_position = applications::live_for_speed::CoordinateTransform::LfsToMpc({x_X, x_Y, x_Z});
    m_mpc_world_rotation = applications::live_for_speed::CoordinateTransform::LfsToMpc(lfs_world_rotation);
    m_mpc_body_translational_velocity = applications::live_for_speed::CoordinateTransform::LfsToMpc(lfs_body_velocity);
    m_mpc_body_euler_rates = applications::live_for_speed::CoordinateTransform::LfsToMpc(lfs_body_euler_rates);
    m_mpc_body_translational_acceleration =
        applications::live_for_speed::CoordinateTransform::LfsToMpc(lfs_body_translational_acceleration);

    auto mpc_world_rotation_without_yaw = m_mpc_world_rotation;
    mpc_world_rotation_without_yaw[2] = 0;
    m_mpc_body_gravity =
        utilities::Math::WorldToBody(std::array<double, 3>{0.0, 0.0, 9.81}, mpc_world_rotation_without_yaw);

    m_mpc_angular_velocity =
        utilities::Math::EulerRatesToAngularVelocities(m_mpc_body_euler_rates, m_mpc_world_rotation);
}
void LfsInOutput::UpdateSignalsConnection(messages::VehicleStateSignals& vehicle_state_signals)
{
    vehicle_state_signals.world_X_vehicle_m = m_mpc_world_position[0];
    vehicle_state_signals.world_Y_vehicle_m = m_mpc_world_position[1];
    vehicle_state_signals.world_Z_vehicle_m = m_mpc_world_position[2];

    vehicle_state_signals.world_phi_vehicle_rad = m_mpc_world_rotation[0];
    vehicle_state_signals.world_theta_vehicle_rad = m_mpc_world_rotation[1];
    vehicle_state_signals.world_psi_vehicle_rad = m_mpc_world_rotation[2];

    vehicle_state_signals.body_u_vehicle_m_s = m_mpc_body_translational_velocity[0];
    vehicle_state_signals.body_v_vehicle_m_s = m_mpc_body_translational_velocity[1];
    vehicle_state_signals.body_w_vehicle_m_s = m_mpc_body_translational_velocity[2];

    vehicle_state_signals.body_p_vehicle_rad_s = m_mpc_body_euler_rates[0];
    vehicle_state_signals.body_q_vehicle_rad_s = m_mpc_body_euler_rates[1];
    vehicle_state_signals.body_r_vehicle_rad_s = m_mpc_body_euler_rates[2];

    vehicle_state_signals.body_a_x_vehicle_m_s2 = m_mpc_body_translational_acceleration[0];
    vehicle_state_signals.body_a_y_vehicle_m_s2 = m_mpc_body_translational_acceleration[1];
    vehicle_state_signals.body_a_z_vehicle_m_s2 = m_mpc_body_translational_acceleration[2];
}
void LfsInOutput::UpdateSignalsConnection(messages::InertialHeadSignals& inertial_head_signals)
{
    inertial_head_signals.head_f_x_head_m_s2 = m_mpc_body_gravity[0] - m_mpc_body_translational_acceleration[0];
    inertial_head_signals.head_f_y_head_m_s2 = m_mpc_body_gravity[1] - m_mpc_body_translational_acceleration[1];
    inertial_head_signals.head_f_z_head_m_s2 = m_mpc_body_gravity[2] - m_mpc_body_translational_acceleration[2];

    inertial_head_signals.head_omega_x_head_rad_s = m_mpc_angular_velocity[0];
    inertial_head_signals.head_omega_y_head_rad_s = m_mpc_angular_velocity[1];
    inertial_head_signals.head_omega_z_head_rad_s = m_mpc_angular_velocity[2];
}
void LfsInOutput::UpdateSignalsConnection(messages::AutomotiveSignals& automotive_signals)
{
    automotive_signals.car_throttle_fraction = m_out_sim.OSInputs.Throttle;
    automotive_signals.car_brake_fraction = m_out_sim.OSInputs.Brake;
    automotive_signals.car_clutch_fraction = m_out_sim.OSInputs.Clutch;
    automotive_signals.car_transmission_gear_integer = m_out_sim.Gear;
    automotive_signals.car_engine_rotational_velocity_rad_s = m_out_sim.EngineAngVel;
}

void LfsInOutput::Tick(MessageBus& /* in_output_message_bus */)
{
    if (m_out_sim_receiver.Receive<applications::live_for_speed::OutSimPack2>(m_out_sim)) {
        if (m_received_from_lfs == 0) {
            m_logger.Info("Receiving OutSim.");
        }
        m_received_from_lfs = 50;
    }
    else {
        if (m_received_from_lfs > 1) {
            --m_received_from_lfs;
        }
        else if (m_received_from_lfs == 1) {
            m_logger.Info("Stopped receiving OutSim.");
            m_received_from_lfs = 0;
        }
        else {
            m_received_from_lfs = 0;
        }
    }

    if (GetStateMachineClient().GetCurrentState() == State::kStarting) {
        if (m_received_from_lfs > 1) {
            GetStateMachineClient().SetGoToState(State::kStarted);
        }
    }
    else if (GetStateMachineClient().GetCurrentState() == State::kRun) {
        if (m_received_from_lfs == 0) {
            GetStateMachineClient().SetGoToState(State::kFailing);
        }
    }
    else {
        TransitionStateMachine();
    }
}

void LfsInOutput::MainTick(DataBucket& data_bucket)
{
    if (data_bucket.GetTickCycle() == 0) {
        data_bucket.GetTaskSignalBus().UpdateMessage<messages::AutomotivePredictionParameters>(
            [&](messages::AutomotivePredictionParameters& message) { message = m_automotive_prediction_parameters; });

        data_bucket.GetTaskSignalBus().UpdateMessage<messages::AutomotivePredictionPath>(
            {[&](messages::AutomotivePredictionPath& message) { message.path_ptr = m_path_ptr; }});
    }

    if (m_received_from_lfs > 1) {
        ConvertLfsToMpcSignals();

        data_bucket.GetTaskSignalBus().UpdateMessage<messages::VehicleStateSignals>(
            [&](messages::VehicleStateSignals& message) { UpdateSignalsConnection(message); });
        data_bucket.GetTaskSignalBus().UpdateMessage<messages::InertialHeadSignals>(
            [&](messages::InertialHeadSignals& message) { UpdateSignalsConnection(message); });
        data_bucket.GetTaskSignalBus().UpdateMessage<messages::AutomotiveSignals>(
            [&](messages::AutomotiveSignals& message) { UpdateSignalsConnection(message); });
    }
    else {
        data_bucket.GetTaskSignalBus().UpdateMessage<messages::VehicleStateSignals>(
            [&](messages::VehicleStateSignals& message) { UpdateSignalsNoConnection(message); });
        data_bucket.GetTaskSignalBus().UpdateMessage<messages::InertialHeadSignals>(
            [&](messages::InertialHeadSignals& message) { UpdateSignalsNoConnection(message); });
        data_bucket.GetTaskSignalBus().UpdateMessage<messages::AutomotiveSignals>(
            [&](messages::AutomotiveSignals& message) { UpdateSignalsNoConnection(message); });
    }
}

void LfsInOutput::PrepareTaskMessageBus(const Task& task, MessageBus& message_bus)
{
    message_bus.MakeMessage<messages::VehicleStateSignals>();
    message_bus.MakeMessage<messages::InertialHeadSignals>();
    message_bus.MakeMessage<messages::AutomotiveSignals>();
    message_bus.MakeMessage<messages::AutomotivePredictionParameters>();
    message_bus.MakeMessage<messages::AutomotivePredictionPath>();
}
void LfsInOutput::CheckTaskMessageBusPrepared(const Task& task, const MessageBus& message_bus)
{
    message_bus.RequireMessage<messages::VehicleStateSignals>();
    message_bus.RequireMessage<messages::InertialHeadSignals>();
    message_bus.RequireMessage<messages::AutomotiveSignals>();
    message_bus.RequireMessage<messages::AutomotivePredictionParameters>();
    message_bus.RequireMessage<messages::AutomotivePredictionPath>();
}
void LfsInOutput::Prepare()
{
    GetSafety().Prepared();
}

}  //namespace mpmca::pipeline

static mpmca::pipeline::InOutputRegistrar<mpmca::pipeline::LfsInOutput> LfsInOutputRegistrar("LiveForSpeed");