/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "pth_reader.hpp"

#include "coordinate_transform.hpp"

namespace mpmca::applications::live_for_speed {
PthReader::PthReader(const std::string& filename)
{
    static_assert(sizeof(HeaderBlock) == (6 + 2 + 8));
    static_assert(sizeof(NodeBlock) == 10 * 4);

    ReadPath(filename);
}

const NodeBlock& PthReader::GetNodeBlock(size_t i)
{
    return m_node_blocks.at(i);
}

double PthReader::GetCenterRoadPsi(size_t index) const
{
    size_t next_index = (index + 1) % m_node_blocks.size();
    return std::atan2(m_node_blocks[next_index].GetCenterRoadY() - m_node_blocks[index].GetCenterRoadY(),
                      m_node_blocks[next_index].GetCenterRoadX() - m_node_blocks[index].GetCenterRoadX());
}

double PthReader::GetCenterRoadPsiMpc(size_t index) const
{
    size_t next_index = (index + 1) % m_node_blocks.size();
    return std::atan2(m_node_blocks[next_index].GetCenterRoadX() - m_node_blocks[index].GetCenterRoadX(),
                      m_node_blocks[next_index].GetCenterRoadY() - m_node_blocks[index].GetCenterRoadY());
}

predict::inertial::PathPtr PthReader::GetPathPtr() const
{
    std::vector<predict::inertial::PathPoint> path_point_vector;
    path_point_vector.resize(m_node_blocks.size() + 1);

    for (size_t i = 0; i <= m_node_blocks.size(); ++i) {
        size_t index = i % m_node_blocks.size();

        auto mpc_position = CoordinateTransform::LfsToMpc({m_node_blocks[index].GetCenterRoadX(),  //
                                                           m_node_blocks[index].GetCenterRoadY(),  //
                                                           m_node_blocks[index].GetCenterRoadZ()});

        path_point_vector[i] = predict::inertial::PathPoint(mpc_position[0], mpc_position[1], mpc_position[2],  //
                                                            0,  //
                                                            0,  //
                                                            GetCenterRoadPsiMpc(index));

        path_point_vector[i].SetRoadWidth(m_node_blocks[index].GetRoadWidth());
    }

    predict::inertial::PathFactory factory(path_point_vector, true);
    return factory.GetPreparedPath();
}

std::vector<float> PthReader::GetCenterX() const
{
    std::vector<float> ret_value(m_node_blocks.size());
    for (size_t i = 0; i < m_node_blocks.size(); ++i) {
        ret_value[i] = m_node_blocks[i].GetCenterX();
    }
    return ret_value;
}
std::vector<float> PthReader::GetCenterY() const
{
    std::vector<float> ret_value(m_node_blocks.size());
    for (size_t i = 0; i < m_node_blocks.size(); ++i) {
        ret_value[i] = m_node_blocks[i].GetCenterY();
    }
    return ret_value;
}
std::vector<float> PthReader::GetRightRoadX() const
{
    std::vector<float> ret_value(m_node_blocks.size());
    for (size_t i = 0; i < m_node_blocks.size(); ++i) {
        ret_value[i] = m_node_blocks[i].GetRightRoadX();
    }
    return ret_value;
}
std::vector<float> PthReader::GetRightRoadY() const
{
    std::vector<float> ret_value(m_node_blocks.size());
    for (size_t i = 0; i < m_node_blocks.size(); ++i) {
        ret_value[i] = m_node_blocks[i].GetRightRoadY();
    }
    return ret_value;
}
std::vector<float> PthReader::GetLeftRoadX() const
{
    std::vector<float> ret_value(m_node_blocks.size());
    for (size_t i = 0; i < m_node_blocks.size(); ++i) {
        ret_value[i] = m_node_blocks[i].GetLeftRoadX();
    }
    return ret_value;
}
std::vector<float> PthReader::GetLeftRoadY() const
{
    std::vector<float> ret_value(m_node_blocks.size());
    for (size_t i = 0; i < m_node_blocks.size(); ++i) {
        ret_value[i] = m_node_blocks[i].GetLeftRoadY();
    }
    return ret_value;
}
std::vector<float> PthReader::GetCenterRoadX() const
{
    std::vector<float> ret_value(m_node_blocks.size());
    for (size_t i = 0; i < m_node_blocks.size(); ++i) {
        ret_value[i] = m_node_blocks[i].GetCenterRoadX();
    }
    return ret_value;
}
std::vector<float> PthReader::GetCenterRoadY() const
{
    std::vector<float> ret_value(m_node_blocks.size());
    for (size_t i = 0; i < m_node_blocks.size(); ++i) {
        ret_value[i] = m_node_blocks[i].GetCenterRoadY();
    }
    return ret_value;
}
std::vector<float> PthReader::GetCenterRoadPsi() const
{
    std::vector<float> ret_value(m_node_blocks.size());
    for (size_t i = 0; i < m_node_blocks.size(); ++i) {
        ret_value[i] = GetCenterRoadPsi(i);
    }
    return ret_value;
}
void PthReader::ReadPath(const std::string& filename)
{
    namespace fs = std::filesystem;
    // Open the stream to 'lock' the file.
    std::ifstream f(filename, std::ios::in | std::ios::binary);

    // Obtain the size of the file.
    const auto file_size = fs::file_size(filename);

    // Create a buffer.
    std::vector<char> buffer(file_size, 0);

    // Read the whole file into the buffer.
    f.read(buffer.data(), file_size);

    std::memcpy(&m_header_block, &buffer[0], sizeof(HeaderBlock));

    m_node_blocks.resize(m_header_block.m_num_nodes);

    constexpr size_t header_offset = sizeof(HeaderBlock);
    std::memcpy(m_node_blocks.data(), &buffer[header_offset], m_header_block.m_num_nodes * sizeof(NodeBlock));
}
}  //namespace mpmca::applications::live_for_speed