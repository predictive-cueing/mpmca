/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/step/reference_predictor.hpp"

#include "mpmca/pipeline/step/reference_predictor.tpp"
#include "mpmca/predict/inertial/automotive_predictor.hpp"

static mpmca::pipeline::StepRegistrar<
    mpmca::pipeline::step::ReferencePredictor<mpmca::predict::inertial::AutomotivePredictor>>
    ReferencePredictorRegistrar("AutomotivePredictor");