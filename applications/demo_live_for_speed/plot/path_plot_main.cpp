/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <matplot/backend/opengl.h>
#include <matplot/matplot.h>

#include <cmath>

#include "include/pth_reader.hpp"

void PlotPath()
{
    mpmca::applications::live_for_speed::PthReader pth_reader(
        "/home/fdrop/projects/liveforspeed/lfs_files/smx/BL1.pth");

    mpmca::predict::inertial::PathPtr path_ptr = pth_reader.GetPathPtr();

    using namespace matplot;

    std::vector<float> left_road_x = pth_reader.GetLeftRoadX();
    std::vector<float> left_road_y = pth_reader.GetLeftRoadY();
    std::vector<float> right_road_x = pth_reader.GetRightRoadX();
    std::vector<float> right_road_y = pth_reader.GetRightRoadY();
    std::vector<float> center_road_x = pth_reader.GetCenterRoadX();
    std::vector<float> center_road_y = pth_reader.GetCenterRoadY();
    std::vector<float> center_road_psi = pth_reader.GetCenterRoadPsi();

    figure<backend::opengl>();

    plot(pth_reader.GetLeftRoadX(), pth_reader.GetLeftRoadY(), "-r.");
    hold(on);
    plot(pth_reader.GetRightRoadX(), pth_reader.GetRightRoadY(), "-g.");
    plot(pth_reader.GetCenterRoadX(), pth_reader.GetCenterRoadY(), "-b.");

    std::vector<float> inter_x;
    std::vector<float> inter_y;

    for (int i = 0; i < path_ptr->GetNumPathPoints(); ++i) {
        for (double fraction = -0.45; fraction < 0.5; fraction += 0.1) {
            auto point = path_ptr->GetInterpolated({i, fraction});
            inter_x.push_back(point.GetX());
            inter_y.push_back(point.GetY());
        }
    }

    plot(inter_x, inter_y, "m-");

    axis(equal);

    show();
}

void PlotEngine()
{
    using namespace matplot;
    auto image = imread("/home/fdrop/projects/mpmca/applications/demo_live_for_speed/lfs_engine.jpeg");
    std::cout << "Read image..." << std::endl;
    imshow(image);

    show();
}

int main()
{
    PlotEngine();
    return 0;
}