/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include "mpmca/messages/automotive_prediction_parameters.hpp"
#include "mpmca/messages/automotive_signals.hpp"
#include "mpmca/messages/inertial_head_signals.hpp"
#include "mpmca/messages/vehicle_state_signals.hpp"
#include "mpmca/pipeline/in_output.hpp"
#include "mpmca/predict/inertial/path.hpp"
#include "mpmca/utilities/configuration.hpp"
#include "mpmca/utilities/map_1d.hpp"
#include "mpmca/utilities/map_2d.hpp"
#include "mpmca/utilities/udp_sender_receiver.hpp"
#include "out_sim.hpp"
#include "pth_reader.hpp"

namespace mpmca::pipeline {
class LfsInOutput : public InOutput
{
  public:
    LfsInOutput(Pipeline& pipeline, const std::string& name, utilities::ConfigurationPtr config);
    ~LfsInOutput() = default;

    void Prepare() override;
    void Tick(MessageBus& in_output_message_bus) override;
    void SafeTick() override {};
    void TaskCompleted(const DataBucket& data_bucket) override {};
    void MainTick(DataBucket& data_bucket) override;
    void PrepareTaskMessageBus(const Task& task, MessageBus& message_bus) override;
    void CheckTaskMessageBusPrepared(const Task& task, const MessageBus& message_bus) override;

  private:
    void ConvertLfsToMpcSignals();

    void UpdateSignalsNoConnection(messages::InertialHeadSignals& signals);
    void UpdateSignalsNoConnection(messages::VehicleStateSignals& signals);
    void UpdateSignalsNoConnection(messages::AutomotiveSignals& signals);

    void UpdateSignalsConnection(messages::VehicleStateSignals& vehicle_state_signals);
    void UpdateSignalsConnection(messages::InertialHeadSignals& inertial_head_signals);
    void UpdateSignalsConnection(messages::AutomotiveSignals& automotive_signals);

    utilities::UdpSenderReceiver m_out_sim_receiver;

    applications::live_for_speed::PthReader m_pth_reader;
    predict::inertial::PathPtr m_path_ptr;
    applications::live_for_speed::OutSimPack2 m_out_sim;

    messages::AutomotivePredictionParameters m_automotive_prediction_parameters;
    int m_received_from_lfs;

    std::array<double, 3> m_mpc_world_position;
    std::array<double, 3> m_mpc_world_rotation;
    std::array<double, 3> m_mpc_body_translational_velocity;
    std::array<double, 3> m_mpc_body_euler_rates;
    std::array<double, 3> m_mpc_angular_velocity;
    std::array<double, 3> m_mpc_body_translational_acceleration;
    std::array<double, 3> m_mpc_body_gravity;
};
}  //namespace mpmca::pipeline