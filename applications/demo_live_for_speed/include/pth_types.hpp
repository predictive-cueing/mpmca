/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <cstdint>

namespace mpmca::applications::live_for_speed {

struct HeaderBlock
{
    char m_lfspth[6];
    uint8_t m_version;
    uint8_t m_revision;
    uint32_t m_num_nodes;
    uint32_t m_finish_line;
};

struct NodeBlock
{
    int32_t m_center_x;
    int32_t m_center_y;
    int32_t m_center_z;
    float m_direction_x;
    float m_direction_y;
    float m_direction_z;
    float m_limit_left;
    float m_limit_right;
    float m_road_limit_left;
    float m_road_limit_right;

    float GetCenterX() const { return m_center_x / 65536.0; }
    float GetCenterY() const { return m_center_y / 65536.0; }
    float GetCenterZ() const { return m_center_z / 65536.0; }

    float GetRoadWidth() const { return m_road_limit_right - m_road_limit_left; }

    float GetLeftRoadX() const { return GetCenterX() + m_direction_y * m_road_limit_left; }
    float GetLeftRoadY() const { return GetCenterY() - m_direction_x * m_road_limit_left; }

    float GetRightRoadX() const { return GetCenterX() + m_direction_y * m_road_limit_right; }
    float GetRightRoadY() const { return GetCenterY() - m_direction_x * m_road_limit_right; }

    float GetCenterRoadX() const { return (GetLeftRoadX() + GetRightRoadX()) / 2.0; }
    float GetCenterRoadY() const { return (GetLeftRoadY() + GetRightRoadY()) / 2.0; }
    float GetCenterRoadZ() const { return GetCenterZ(); }
};
}  //namespace mpmca::applications::live_for_speed