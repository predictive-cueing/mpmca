/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#pragma once
#include <array>

namespace mpmca::applications::live_for_speed {
class CoordinateTransform
{
  public:
    static std::array<double, 6> LfsToMpc(double x_lfs, double y_lfs, double z_lfs,  //
                                          double phi_lfs, double theta_lfs, double psi_lfs)
    {
        std::array<double, 6> mpc_vector;

        mpc_vector[0] = y_lfs;
        mpc_vector[1] = x_lfs;
        mpc_vector[2] = -z_lfs;
        mpc_vector[3] = theta_lfs;
        mpc_vector[4] = phi_lfs;
        mpc_vector[5] = -1 * psi_lfs;

        return mpc_vector;
    }
    static std::array<double, 3> LfsToMpc(std::array<double, 3> lfs_vector)
    {
        std::array<double, 3> mpc_vector;

        mpc_vector[0] = lfs_vector[1];
        mpc_vector[1] = lfs_vector[0];
        mpc_vector[2] = -1 * lfs_vector[2];

        return mpc_vector;
    }
    static std::array<double, 3> MpcToLfs(std::array<double, 3> mpc_vector)
    {
        std::array<double, 3> lfs_vector;

        lfs_vector[0] = mpc_vector[1];
        lfs_vector[1] = mpc_vector[0];
        lfs_vector[2] = -1 * mpc_vector[2];

        return lfs_vector;
    }
};
}  //namespace mpmca::applications::live_for_speed