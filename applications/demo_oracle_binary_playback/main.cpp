/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/in_output/go_to_run.hpp"
#include "mpmca/pipeline/main_actor.hpp"
#include "mpmca/pipeline/pipeline.hpp"
#include "mpmca/pipeline/step/binary_inertial_oracle_playback.hpp"
#include "mpmca/pipeline/step/binary_logger.hpp"
#include "mpmca/pipeline/step/synthetic_inertial_signal_generator.hpp"

int main(int argc, char** argv)
{
    int return_code = 0;
    try {
        mpmca::pipeline::MainActor main_actor(argc, argv);
        return_code = main_actor.Run();
    }
    catch (const std::exception& e) {
        std::cout << "An exception was caught:\n" << e.what() << std::endl;
        return_code = 1;
    }
    catch (...) {
        std::cout << "A general exception was caught." << std::endl;
        return_code = 1;
    }

    return return_code;
}
