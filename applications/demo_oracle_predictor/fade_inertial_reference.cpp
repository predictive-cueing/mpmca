/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
// --8<-- [start:body]
#include "mpmca/pipeline/step/fade_inertial_reference.hpp"

#include "mpmca/pipeline/step/fade_inertial_reference.tpp"
#include "mpmca/pipeline/step_registrar.hpp"
#include "mpmca_hexapod/mpmca_hexapod.hpp"

using namespace mpmca::pipeline;

template class step::FadeInertialReference<mpmca::control::MpmcaHexapod>;

static StepRegistrar<step::FadeInertialReference<mpmca::control::MpmcaHexapod>>
    FadeInertialReferenceRegistrar("FadeInertialReference");
// --8<-- [end:body]