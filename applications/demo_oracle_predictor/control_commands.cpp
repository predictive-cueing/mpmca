/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
// --8<-- [start:body]
#include "mpmca/pipeline/in_output/control_commands.hpp"

#include "mpmca/pipeline/in_output/control_commands.tpp"
#include "mpmca_hexapod/mpmca_hexapod.hpp"

using namespace mpmca::pipeline;

template class in_output::ControlCommands<mpmca::control::MpmcaHexapod>;

static InOutputRegistrar<
    in_output::ControlCommands<mpmca::control::MpmcaHexapod>>
    ControlCommandsRegistrar("ControlCommands");

// --8<-- [end:body]