/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/step/mcap_logger.hpp"

#include "mpmca/pipeline/step/mcap_logger.tpp"
#include "mpmca_hexapod/mpmca_hexapod.hpp"

template class mpmca::pipeline::step::McapLogger<mpmca::control::MpmcaHexapod>;
static mpmca::pipeline::StepRegistrar<mpmca::pipeline::step::McapLogger<mpmca::control::MpmcaHexapod>>
    McapLoggerRegistrar("McapLogger");