/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
// --8<-- [start:body]
#include "mpmca/pipeline/step/predictive_control.hpp"

#include "mpmca/pipeline/step/predictive_control.tpp"
#include "mpmca_hexapod/mpmca_hexapod.hpp"

template class mpmca::pipeline::step::PredictiveControl<
    mpmca::control::MpmcaHexapod>;

static mpmca::pipeline::StepRegistrar<
    mpmca::pipeline::step::PredictiveControl<mpmca::control::MpmcaHexapod>>
    PredictiveControlRegistrar("PredictiveControl");
// --8<-- [end:body]