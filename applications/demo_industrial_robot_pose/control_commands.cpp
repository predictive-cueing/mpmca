/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/in_output/control_commands.hpp"

#include "mpmca/pipeline/in_output/control_commands.tpp"
#include "mpmca_industrial_robot_pose/mpmca_industrial_robot_pose.hpp"

template class mpmca::pipeline::in_output::ControlCommands<mpmca::control::MpmcaIndustrialRobotPose>;

static mpmca::pipeline::InOutputRegistrar<
    mpmca::pipeline::in_output::ControlCommands<mpmca::control::MpmcaIndustrialRobotPose>>
    ControlCommandsRegistrar("ControlCommands");