/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/control/controller.hpp"
#include "mpmca/control/simulator.hpp"
#include "mpmca/control/state.hpp"
#include "mpmca/linear_algebra.hpp"
#include "mpmca/utilities/logger.hpp"
#include "mpmca_constraint_free_simulator/mpmca_constraint_free_simulator.hpp"

int main(int argc, char** argv)
{
    using namespace mpmca;
    using CFS = control::MpmcaConstraintFreeSimulator;

    utilities::Logger logger("ControllerExample", "Main");

    Vector<12> initial_state = Vector<12>::Zero();
    Vector<6> initial_input = Vector<6>::Zero();

    unsigned horizon_length = 50;
    unsigned horizon_step = 1;
    double horizon_dt = 0.01;

    control::Controller<CFS> controller(
        horizon_length, horizon_step, horizon_dt, initial_state, initial_input);

    control::Simulator<CFS> simulator;

    // Although the controller will run fine without explicitly setting the
    // controller weights, it is recommended to do so.
    CFS::OutputVector output_error_weight;
    CFS::StateVector state_error_weight;
    CFS::InputVector input_error_weight;
    output_error_weight << 1, 1, 1, 10, 10, 10, 0, 0, 0;
    state_error_weight << 1, 1, 1, 1, 1, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1;
    input_error_weight << 0.1, 0.1, 0.1, 0.1, 0.1, 0.1;

    controller.SetOutputErrorWeight(output_error_weight);
    controller.SetStateErrorWeight(state_error_weight);
    controller.SetInputErrorWeight(input_error_weight);

    Vector<12> current_state = initial_state;
    Vector<12> next_state;

    // The control namespace provides a lot of "viewers". These objects allow
    // you to access the elements of InputVectors, StateVectors, InfoVectors,
    // and ConstraintVector more easily by name. The ViewAt template index
    // refers to the index of all Components of a simulator. That is, certain
    // simulators consist of multiple Components stacked 'on top of each other'.
    // For example, the MpmcaXyHexapodYaw simulator consists of a XyTable, a
    // Hexapod, and a YawTable.
    auto next_state_view = control::MakeStateView<CFS>(next_state).ViewAt<0>();

    double sine_frequency = 5;  // Hertz

    CFS::OutputVector reference_output;

    for (double time = 0; time < 1.0; time += horizon_dt) {
        // Generate a sine wave and use it for the reference output
        for (size_t kk = 0; kk < controller.GetMpcHorizonLength(); ++kk) {
            double horizon_time = time + horizon_dt * kk;
            double sine_value =
                horizon_time *
                std::sin(2 * 3.141 * sine_frequency * horizon_time);

            reference_output << 0, 0, 9.81 + sine_value, 0, 0, 0, 0, 0, 0;
            controller.SetReferenceOutput(kk, reference_output);
        }
        // The controller needs to be Prepared before calculating the next
        // control input in the call to Feedback. These calls are independent
        // calls, because it is possible to parallelize the Preparation call.
        controller.Preparation();
        // The return value of Feedback is the next input to be given to the
        // system. That is, the controller calculates the optimal inputs for
        // the entire control horizon, but only the first is used for
        // controlling the system (simulator).
        auto const first_input = controller.Feedback(current_state);

        // The simulator object can be used to integrate the state using the
        // calculated input.
        next_state =
            simulator.Integrate(current_state, first_input, horizon_dt);

        // Note that you can also get the next state from the
        // controller directly, by getting the state from the StatePlan at
        // position 1.
        auto const next_state_controller = controller.GetStatePlan(1);
        auto next_state_controller_view =
            control::MakeStateView<CFS>(next_state_controller).ViewAt<0>();

        logger.Info("At time " + std::to_string(time) +
                    " the z-position of the simulator is: " +
                    std::to_string(next_state_view.GetZ()) +
                    ", the z-position calculated by the controller is: " +
                    std::to_string(next_state_controller_view.GetZ()));

        // This is the end of the current sample, thus we update current_state
        // to next_state.
        current_state = next_state;
    }

    return 0;
}
