/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <gtest/gtest.h>

#include <exception>

#include "mpmca/dokblocks/classic_washout_algorithm/classic_washout_algorithm.hpp"
#include "mpmca/dokblocks/classic_washout_algorithm/params.hpp"
#include "mpmca/dokblocks/core/input_output_adaptor.hpp"

namespace mpmca::dokblocks {
class MermaidTest
{
  public:
    static void PrintStrings(const std::vector<std::string>& strings)
    {
        for (const std::string& string : strings) {
            std::cout << "  " << string << std::endl;
        }
    }
    static void PrintMermaid(const std::vector<std::string>& strings)
    {
        std::cout << "flowchart LR" << std::endl;
        PrintStrings(strings);
    }
};

}  //namespace mpmca::dokblocks

int main(int argc, char** argv)
{
    using namespace mpmca::dokblocks;
    classic_washout_algorithm::Params params;
    auto cwa = classic_washout_algorithm::cwa::GetBlock(params);

    auto input_adaptor = InputOutputAdaptor("cwa_inputs", cwa);

    mpmca::dokblocks::MermaidTest::PrintMermaid(input_adaptor.GetMermaidDiagramString());
    return 0;
}
