/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/step/classic_washout_reference.hpp"

#include "mpmca/control/input.hpp"
#include "mpmca/control/state.hpp"
#include "mpmca_hexapod_pose/mpmca_hexapod_pose.hpp"
#include "mpmca_xy_hexapod_yaw_pose/mpmca_xy_hexapod_yaw_pose.hpp"

using namespace mpmca::pipeline;

static StepRegistrar<
    step::ClassicWashoutReference<mpmca::control::MpmcaHexapodPose>>
    ClassicWashoutReferenceHexapodRegistrar("ClassicWashoutReferenceHexapod");

static StepRegistrar<
    step::ClassicWashoutReference<mpmca::control::MpmcaXyHexapodYawPose>>
    ClassicWashoutReferenceXyHexapodYawRegistrar(
        "ClassicWashoutReferenceXyHexapodYaw");