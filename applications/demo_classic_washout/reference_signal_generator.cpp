/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/step/reference_signal_generator.hpp"

#include "mpmca_hexapod_pose/mpmca_hexapod_pose.hpp"

static mpmca::pipeline::StepRegistrar<
    mpmca::pipeline::step::ReferenceSignalGenerator<9>>
    ReferenceSignalGeneratorInertialRegistrar(
        "ReferenceSignalGeneratorInertial");
