/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */

#include <matplot/matplot.h>

#include "mpmca/constants.hpp"
#include "mpmca/dokblocks/classic_washout_algorithm/classic_washout_algorithm_model.hpp"
#include "mpmca/dokblocks/classic_washout_algorithm/params.hpp"
#include "mpmca/dokblocks/core/input_output_adaptor.hpp"
#include "mpmca/dokblocks/core/simulation_runner.hpp"
#include "mpmca/dokblocks/core/time_series.hpp"
#include "mpmca/dokblocks/utils/matlab_utils.hpp"
#include "mpmca/linear_algebra.hpp"

template <class... Args>
auto Plot(mpmca::dokblocks::TimeSeries time_series, Args&&... args)
{
    return matplot::plot(time_series.GetTimeVector(),
                         time_series.GetDataVector(),
                         std::forward<Args>(args)...);
}

template <class... Args>
auto Plot(matplot::axes_handle ax, mpmca::dokblocks::TimeSeries time_series,
          Args&&... args)
{
    return matplot::plot(ax, time_series.GetTimeVector(),
                         time_series.GetDataVector(),
                         std::forward<Args>(args)...);
}

mpmca::dokblocks::TimeSeries CreateSineTimeSeries(size_t signal_length,
                                                  double dt)
{
    mpmca::dokblocks::TimeSeries result("test");
    const double m_f_signal = 2.0;

    for (size_t i = 0; i < signal_length; ++i) {
        double time = i * dt;
        result.SetSignalAtIndex(
            i, time,
            std::sin(2 * mpmca::constants::kPi * m_f_signal * time) + 10 +
                5 * time);
    }
    return result;
}

void PlotCwa()
{
    using namespace mpmca::dokblocks;
    classic_washout_algorithm::Params params;
    auto cwa = classic_washout_algorithm::cwa::GetBlock(params);

    const double dt = 0.01;
    const size_t N = 1000;
    SimulationRunner solver("cwa_solver", cwa, dt, true);

    for (size_t i = 0; i < N; ++i) {
        double time = i * dt;
        solver.GetInput("f_x").SetSignalAtIndex(i, time, 0);
        solver.GetInput("f_y").SetSignalAtIndex(i, time, 0);
        solver.GetInput("f_z").SetSignalAtIndex(i, time, 9.81);
        solver.GetInput("omega_x").SetSignalAtIndex(i, time, 0);
        solver.GetInput("omega_y").SetSignalAtIndex(i, time, 0);
        solver.GetInput("omega_z").SetSignalAtIndex(i, time, 1);
        solver.GetInput("phi_vehicle").SetSignalAtIndex(i, time, 0);
        solver.GetInput("theta_vehicle").SetSignalAtIndex(i, time, 0);
        solver.GetInput("psi_vehicle").SetSignalAtIndex(i, time, 0);
    }

    solver.GetInput("phi_sim").SetSignalAtIndex(0, 0, 0);
    solver.GetInput("theta_sim").SetSignalAtIndex(0, 0, 0);
    solver.GetInput("psi_sim").SetSignalAtIndex(0, 0, 0);

    solver.RunSimulation(N);

    if (false) {
        matplot::figure();
        Plot(solver.GetInput("omega_x"), "-k");
        matplot::hold(matplot::on);
        Plot(solver.GetOutput("phi_sim"), "-r");
        Plot(solver.GetOutput("p_sim"), "-g");
        matplot::show();
    }

    if (true) {
        matplot::figure();
        matplot::subplot(2, 1, 0);
        Plot(solver.GetInput("omega_z"), "-k");
        matplot::hold(matplot::on);
        Plot(solver.GetOutput("psi_sim"), "-r");
        Plot(solver.GetOutput("r_sim"), "-g");
        matplot::subplot(2, 1, 1);
        Plot(solver.GetOutput("rd_sim"), "-b");
        matplot::show();
    }

    if (false) {
        matplot::figure();
        // matplot::subplot(2, 1, 0);
        Plot(solver.GetInput("f_x"), "-k");
        matplot::hold(matplot::on);
        Plot(solver.GetOutput("a_x_sim"), "-r");
        Plot(solver.GetOutput("xd_sim"), "-g");
        Plot(solver.GetOutput("x_sim"), "-b");
        Plot(solver.GetOutput("z_sim"), "--b");
        // matplot::subplot(2, 1, 1);
        // Plot(solver.GetInput("f_x"), "-k");
        // matplot::hold(matplot::on);
        Plot(solver.GetOutput("theta_sim"), "-c");
        Plot(solver.GetOutput("q_sim"), "-m");
        // Plot(solver.GetOutput("qd_sim"), "-k");
        matplot::show();
    }

    if (false) {
        matplot::figure();
        // matplot::subplot(2, 1, 0);
        Plot(solver.GetInput("f_y"), "-k");
        matplot::hold(matplot::on);
        Plot(solver.GetInput("f_x"), "--r");
        Plot(solver.GetOutput("phi_sim"), "-g");
        Plot(solver.GetOutput("theta_sim"), "-b");
        // Plot(solver.GetOutput("a_y_sim"), "-r");
        // Plot(solver.GetOutput("yd_sim"), "-g");
        // Plot(solver.GetOutput("y_sim"), "-b");
        matplot::ylim({-2, 2});
        matplot::show();
    }
}

void PlotTests()
{
    std::vector<double> t, y;

    for (int i = 0; i < 100; ++i) {
        t.push_back(i * 0.01);
        y.push_back(i);
    }

    mpmca::dokblocks::TimeSeries series = CreateSineTimeSeries(500, 0.01);

    matplot::figure();
    auto ax = matplot::subplot(2, 1, 0);

    auto ax2 = matplot::subplot(2, 1, 1);
    matplot::plot(t, y, "-r.");
    matplot::hold(matplot::on);

    Plot(ax, series, "-b.");
    matplot::show();
}

int main(int argc, char** argv)
{
    PlotCwa();

    if (false) {
        auto dd = mpmca::dokblocks::DoubleDifferentiator("test", 100);

        std::cout << dd.GetA() << std::endl;
        std::cout << dd.GetB() << std::endl;
        std::cout << dd.GetC() << std::endl;
        std::cout << dd.GetD() << std::endl << std::endl;

        const double dt = 0.01;
        const size_t N = 1000;
        mpmca::dokblocks::SimulationRunner solver("cwa_solver", dd, dt, false);

        auto model = solver.GetModel();

        std::cout << model.GetA() << std::endl;
        std::cout << model.GetB() << std::endl;
        std::cout << model.GetC() << std::endl;
        std::cout << model.GetD() << std::endl;
    }
}