/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/step/predictive_control.hpp"

#include "mpmca/pipeline/step/predictive_control.tpp"
#include "mpmca_hexapod_pose/mpmca_hexapod_pose.hpp"
#include "mpmca_xy_hexapod_yaw_pose/mpmca_xy_hexapod_yaw_pose.hpp"

static mpmca::pipeline::StepRegistrar<
    mpmca::pipeline::step::PredictiveControl<mpmca::control::MpmcaHexapodPose>>
    PredictiveControlHexapodRegistrar("PredictiveControlHexapod");

static mpmca::pipeline::StepRegistrar<mpmca::pipeline::step::PredictiveControl<
    mpmca::control::MpmcaXyHexapodYawPose>>
    PredictiveControlXyHexapodYawRegistrar("PredictiveControlXyHexapodYaw");
