/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */

// --8<-- [start:includes]
#include "mpmca/pipeline/in_output/delayed_state_follower.hpp"
#include "mpmca/pipeline/in_output/empty_in_output.hpp"
#include "mpmca/pipeline/in_output/go_to_run.hpp"
#include "mpmca/pipeline/in_output/hello_world.hpp"
#include "mpmca/pipeline/main_actor.hpp"
#include "mpmca/pipeline/pipeline.hpp"
#include "mpmca/pipeline/step/hello_world.hpp"
// --8<-- [end:includes]

// --8<-- [start:main]
int main(int argc, char** argv)
{
    try {
        mpmca::pipeline::MainActor main_actor(argc, argv);
        return main_actor.Run();
    }
    catch (const std::exception& e) {
        std::cout << "An exception was caught:\n" << e.what() << std::endl;
        return 1;
    }
    catch (...) {
        std::cout << "A general exception was caught." << std::endl;
        return 1;
    }
}
// --8<-- [end:main]