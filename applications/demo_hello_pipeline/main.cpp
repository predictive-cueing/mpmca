/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/in_output/delayed_state_follower.hpp"
#include "mpmca/pipeline/in_output/empty_in_output.hpp"
#include "mpmca/pipeline/in_output/go_to_run.hpp"
#include "mpmca/pipeline/in_output/hello_world.hpp"
#include "mpmca/pipeline/pipeline.hpp"
#include "mpmca/pipeline/step/hello_world.hpp"
#include "mpmca/utilities/command_line_options_parser.hpp"

// --8<-- [start:Example1]
void Example1(mpmca::utilities::Logger& logger)
{
    using namespace mpmca::pipeline;

    // First, we create a Pipeline Options object, but do not modify it.
    Pipeline::Options pipeline_options;
    pipeline_options.mark_nondeterministic_log_output = true;

    // Then, construct the pipeline.
    Pipeline pipeline{pipeline_options};

    // After construction, we can add InOutputs, Tasks and Steps. Here, we add
    // three InOutputs, of which two are of type HelloWorld, and one is of type
    // EmptyInOutput. The boolean option controls whether messages are printed.
    pipeline.AddInOutput<in_output::HelloWorld>("HelloWorld InOutput One",
                                                true);
    pipeline.AddInOutput<in_output::HelloWorld>("HelloWorld InOutput Two",
                                                true);
    pipeline.AddInOutput<in_output::EmptyInOutput>("EmptyInOuput");

    // After constructing all InOutputs, Tasks and Steps, and before running,
    // the pipeline needs to be "prepared". In the preparing stage, the
    // InOutputs and Steps may perform expensive (blocking) operations that
    // could not be done during construction, and take too long to be done while
    // the pipeline is being "ticked" periodically. The InOutputs and Steps
    // should also declare themselves 'safe' to be ticked periodically.
    pipeline.Prepare();

    // This loop performs the periodic ticking of the pipeline.
    for (size_t t = 0; t < 3; ++t) {
        // This marks the start of one sample.
        logger.Info("==== Start sample " + std::to_string(t) + " ====");

        // Each sample, the pipeline needs to be "ticked". During a Tick() call,
        // all InOutputs are "ticked".
        pipeline.Tick();

        // The Task runs at an integer multiple rate of the InOutputs. To check
        // whether the current *Tick* is a *MainTick*, the
        // GetCurrentTickIsMainTick() method can be called.
        if (pipeline.GetCurrentTickIsMainTick(0)) {
            // If this Tick is a MainTick, we need to call MainTick().
            // The MainTick() call can be performed from a different thread than
            // the thread from where Tick() is called.
            pipeline.MainTick(0);
        }

        // This marks the end of the sample.
        logger.Info("==== End sample " + std::to_string(t) + " ====");
    }
}

// Run this example, by running ./demo_hello_pipeline --run_example1
// --8<-- [end:Example1]

// --8<-- [start:Example2]
void Example2(mpmca::utilities::Logger& logger)
{
    using namespace mpmca;

    // First, we create a Pipeline object and add two InOutputs, similar to
    // Example1.
    pipeline::Pipeline::Options pipeline_options;
    pipeline_options.mark_nondeterministic_log_output = true;
    // We explicitly set the base_step_ms option to 1, which means that the
    // InOutput update rate is once every millisecond.
    pipeline_options.base_step_ms = 1;
    // We also explicitly set the command_buffer_length option to 3, which means
    // that the MainTick() of the Task may take 3 samples longer than intended.
    // Search the documentation for "Command Buffer" for more
    // information.
    pipeline_options.command_buffer_length = 3;

    pipeline::Pipeline pipeline{pipeline_options};

    pipeline.AddInOutput<pipeline::in_output::HelloWorld>(
        "Hello World InOutput One", true);
    pipeline.AddInOutput<pipeline::in_output::HelloWorld>(
        "Hello World InOutput Two", true);

    // In this example, we also add a Task, containing two Steps.
    // The `Horizon` argument refers to the MPC horizon properties. This
    // constructor will create a Horizon with 50 steps, where each steps has a
    // length of 3 milliseconds.
    auto task = pipeline.MakeTask("Task", utilities::Horizon(3, 50));
    // The behavior of the HelloWorld step is similar to the HelloWorld
    // InOutput.
    task->AddStep<pipeline::step::HelloWorld>("Hello World Step One", true);
    task->AddStep<pipeline::step::HelloWorld>("Hello World Step Two", true);

    // Again, we prepare all InOutputs and Steps.
    pipeline.Prepare();

    // And run the main loop.
    for (size_t t = 0; t < 7; ++t) {
        if (pipeline.GetNextTickIsMainTick(0)) {
            logger.Info(
                "The next sample is a MainTick! Both Tick and MainTick should "
                "be called.");
        }
        else {
            logger.Info(
                "The next sample is a Tick! Only Tick should be called.");
        }

        logger.Info("==== Start sample " + std::to_string(t) + " ====");
        pipeline.Tick();

        if (pipeline.GetCurrentTickIsMainTick(0))
            pipeline.MainTick(0);

        logger.Info("==== End sample " + std::to_string(t) + " ====");
    }
}
// --8<-- [end:Example2]

// --8<-- [start:Example3]
void Example3(mpmca::utilities::Logger& logger)
{
    using namespace mpmca;

    pipeline::Pipeline::Options pipeline_options;
    pipeline_options.mark_nondeterministic_log_output = true;
    pipeline::Pipeline pipeline{pipeline_options};

    pipeline.AddInOutput<pipeline::in_output::HelloWorld>(
        "Hello World InOutput One", false);
    pipeline.AddInOutput<pipeline::in_output::HelloWorld>(
        "Hello World InOutput Two", false);
    auto go_to_run_in_output =
        pipeline.AddInOutput<pipeline::in_output::GoToRun>(
            "Go To Run InOutput");
    pipeline.AddInOutput<pipeline::in_output::DelayedStateFollower>(
        "Delayed Follower", 2);

    auto task = pipeline.MakeTask("Task", utilities::Horizon(3, 50));

    task->AddStep<pipeline::step::HelloWorld>("Hello World Step One", false);
    task->AddStep<pipeline::step::HelloWorld>("Hello World Step Two", false);

    pipeline.Prepare();

    for (size_t t = 0; t < 18; ++t) {
        logger.Info("==== Start sample " + std::to_string(t) + " ====");

        pipeline.Tick();

        if (pipeline.GetCurrentTickIsMainTick(0))
            pipeline.MainTick(0);

        logger.Info("==== End sample " + std::to_string(t) + " ====");
    }

    // After reaching state kRun on the 17th sample, we tell the GoToRunInOutput
    // to command the StateMachine back to IDLE.
    go_to_run_in_output->SetTargetState(
        pipeline::in_output::GoToRun::TargetState::kIdle);

    for (size_t t = 18; t < 25; ++t) {
        logger.Info("==== Start sample " + std::to_string(t) + " ====");

        pipeline.Tick();

        if (pipeline.GetCurrentTickIsMainTick(0))
            pipeline.MainTick(0);

        logger.Info("==== End sample " + std::to_string(t) + " ====");
    }
}
// --8<-- [end:Example3]

int main(int argc, char** argv)
{
    mpmca::utilities::Logger logger("Main", "main");

    mpmca::utilities::CommandLineOptionsParser options_parser{argc, argv};

    if (options_parser.GetCommandOptionExists("--run_example1")) {
        logger.Info("<<<<< Example 1 >>>>>");
        Example1(logger);
    }

    if (options_parser.GetCommandOptionExists("--run_example2")) {
        logger.Info("<<<<< Example 2 >>>>>");
        Example2(logger);
    }

    if (options_parser.GetCommandOptionExists("--run_example3")) {
        logger.Info("<<<<< Example 3 >>>>>");
        Example3(logger);
    }

    return 0;
}
