/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/in_output/control_commands.hpp"

#include "mpmca/pipeline/in_output/control_commands.tpp"
#include "mpmca_hexapod/mpmca_hexapod.hpp"
#include "mpmca_xy_hexapod_yaw/mpmca_xy_hexapod_yaw.hpp"

using namespace mpmca::pipeline;

static InOutputRegistrar<
    in_output::ControlCommands<mpmca::control::MpmcaHexapod>>
    ControlCommandsRegistrar("ControlCommandsHexapod");

static InOutputRegistrar<
    in_output::ControlCommands<mpmca::control::MpmcaXyHexapodYaw>>
    ControlCommandsRegistrarXy("ControlCommandsXyHexapodYaw");
