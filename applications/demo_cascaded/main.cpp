/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/in_output/go_to_run.hpp"
#include "mpmca/pipeline/main_actor.hpp"
#include "mpmca/pipeline/step/json_playback.hpp"

int main(int argc, char** argv)
{
    // a small change
    try {
        mpmca::pipeline::MainActor main_actor(argc, argv);
        return main_actor.Run();
    }
    catch (const std::exception& e) {
        std::cout << "An exception was caught:\n" << e.what() << std::endl;
        return 1;
    }
    catch (...) {
        std::cout << "A general exception was caught." << std::endl;
        return 1;
    }
}