/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/step/fade_inertial_reference.hpp"

#include "mpmca/pipeline/step/fade_inertial_reference.tpp"
#include "mpmca/pipeline/step_registrar.hpp"
#include "mpmca_hexapod/mpmca_hexapod.hpp"
#include "mpmca_xy_hexapod_yaw/mpmca_xy_hexapod_yaw.hpp"

using namespace mpmca::pipeline;

static StepRegistrar<step::FadeInertialReference<mpmca::control::MpmcaHexapod>>
    FadeInertialReferenceRegistrar("FadeInertialReferenceHexapod");

static StepRegistrar<
    step::FadeInertialReference<mpmca::control::MpmcaXyHexapodYaw>>
    FadeInertialReferenceRegistrarXy("FadeInertialReferenceXyHexapodYaw");