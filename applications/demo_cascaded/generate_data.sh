#!/bin/sh
# ./build/bin/demo_cascaded -c applications/demo_cascaded/config_short_hexapod.json --max_iter 20000 --free
# ./build/bin/demo_cascaded -c applications/demo_cascaded/config_long_hexapod.json --max_iter 20000 --free
# ./build/bin/demo_cascaded -c applications/demo_cascaded/config_cascaded_hexapod.json --max_iter 20000 --free
# ./build/bin/demo_cascaded -c applications/demo_cascaded/config_short_xy.json --max_iter 200000 --free
# ./build/bin/demo_cascaded -c applications/demo_cascaded/config_long_xy.json --max_iter 200000 --free
./build/bin/demo_cascaded -c applications/demo_cascaded/config_cascaded_xy.json --max_iter 200000
mv -f cascaded_* python/mcap/data/