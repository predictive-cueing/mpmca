#pragma once
#include "mpmca_xy_hexapod_yaw_pose/mpmca_xy_hexapod_yaw_pose.hpp"
namespace mpmca::control::testing {
	using TestSimulatorType = mpmca::control::MpmcaXyHexapodYawPose;
} // namespace mpmca::control::testing
