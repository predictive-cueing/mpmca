#pragma once
#include "mpmca_industrial_robot_inertial/mpmca_industrial_robot_inertial.hpp"
namespace mpmca::control::testing {
	using TestSimulatorType = mpmca::control::MpmcaIndustrialRobotInertial;
} // namespace mpmca::control::testing
