#pragma once
#include "mpmca_xy_hexapod_yaw_generated.h"
#include "mpmca/control/components.hpp"
#include "mpmca/control/templates/component.hpp"
#include "mpmca/control/templates/component_list.hpp"
#include "mpmca/control/templates/dimension.hpp"

namespace mpmca::control {
struct MpmcaXyHexapodYaw
{
    using ComponentList = templates::ComponentList<components::XyTable,components::TransformationMatrix,components::Hexapod,components::YawTable>;
    static constexpr std::size_t NY_SIMULATOR = 9;
    using Dimension = templates::DimensionsT<MpmcaXyHexapodYaw>;
    using AxesVector = Vector<Dimension::NQ>;
    using StateVector = Vector<Dimension::NX>;
    using AlgStateVector = Vector<Dimension::NZ>;
    using InputVector = Vector<Dimension::NU>;
    using ConstraintVector = Vector<Dimension::NC>;
    using ParameterVector = Vector<Dimension::NP>;
    using OutputVector = Vector<Dimension::NY>;
    using InfoVector = Vector<Dimension::NI>;
    using StateStateMatrix = Matrix<Dimension::NX, Dimension::NX>;
    using StateInputMatrix = Matrix<Dimension::NX, Dimension::NU>;
    using InputInputMatrix = Matrix<Dimension::NU, Dimension::NU>;
    using OutputOutputMatrix = Matrix<Dimension::NY, Dimension::NY>;
    using OutputStateMatrix = Matrix<Dimension::NY, Dimension::NX>;
    using OutputInputMatrix = Matrix<Dimension::NY, Dimension::NU>;
    using ConstraintStateMatrix = Matrix<Dimension::NC, Dimension::NX>;
    using ConstraintInputMatrix = Matrix<Dimension::NC, Dimension::NU>;
    using OutputStateStateMatrix = Matrix<Dimension::NX * Dimension::NY, Dimension::NX>;
    using OutputStateInputMatrix = Matrix<Dimension::NX * Dimension::NY, Dimension::NU>;
    using OutputInputInputMatrix = Matrix<Dimension::NU * Dimension::NY, Dimension::NU>;
    using ConstraintStateStateMatrix = Matrix<Dimension::NX * Dimension::NC, Dimension::NX>;
    using ConstraintStateInputMatrix = Matrix<Dimension::NX * Dimension::NC, Dimension::NU>;
    using ConstraintInputInputMatrix = Matrix<Dimension::NU * Dimension::NC, Dimension::NU>;
    using InputMatrix = Eigen::Matrix<double, Dimension::NU, Eigen::Dynamic>;
    using StateMatrix = Eigen::Matrix<double, Dimension::NX, Eigen::Dynamic>;
    using OutputMatrix = Eigen::Matrix<double, Dimension::NY, Eigen::Dynamic>;
    static casadi_functions* Output_functions(void) { return MpmcaXyHexapodYawOutput_functions(); }
    static casadi_functions* OutputJacobian_functions(void) { return MpmcaXyHexapodYawOutputJacobian_functions(); }
    static casadi_functions* OutputHessian_functions(void) { return MpmcaXyHexapodYawOutputHessian_functions(); }
    static casadi_functions* Inequality_functions(void) { return MpmcaXyHexapodYawInequality_functions(); }
    static casadi_functions* InequalityJacobian_functions(void) { return MpmcaXyHexapodYawInequalityJacobian_functions(); }
    static casadi_functions* InequalityHessian_functions(void) { return MpmcaXyHexapodYawInequalityHessian_functions(); }
    static casadi_functions* Cost_functions(void) { return MpmcaXyHexapodYawCost_functions(); }
    static casadi_functions* CostJacobian_functions(void) { return MpmcaXyHexapodYawCostJacobian_functions(); }
    static casadi_functions* CostHessian_functions(void) { return MpmcaXyHexapodYawCostHessian_functions(); }
    static casadi_functions* LagrangeHessianGaussNewton_functions(void) { return MpmcaXyHexapodYawLagrangeHessianGaussNewton_functions(); }
    static casadi_functions* LagrangeHessian_functions(void) { return MpmcaXyHexapodYawLagrangeHessian_functions(); }
    static casadi_functions* SimulatorData_functions(void) { return MpmcaXyHexapodYawSimulatorData_functions(); }
    static casadi_functions* HeadPva_functions(void) { return MpmcaXyHexapodYawHeadPva_functions(); }
    static casadi_functions* HeadInertial_functions(void) { return MpmcaXyHexapodYawHeadInertial_functions(); }
    static casadi_functions* Info_functions(void) { return MpmcaXyHexapodYawInfo_functions(); }
    static casadi_functions* Name_functions(void) { return MpmcaXyHexapodYawName_functions(); }
    static casadi_functions* StoppabilityConstraints_functions(void) { return MpmcaXyHexapodYawStoppabilityConstraints_functions(); }
    static casadi_functions* StoppabilityConstraintsHessian_functions(void) { return MpmcaXyHexapodYawStoppabilityConstraintsHessian_functions(); }
    static casadi_functions* StoppabilityConstraintsOriginal_functions(void) { return MpmcaXyHexapodYawStoppabilityConstraintsOriginal_functions(); }
    static casadi_functions* StoppabilityConstraintsHessianOriginal_functions(void) { return MpmcaXyHexapodYawStoppabilityConstraintsHessianOriginal_functions(); }
    static const char* name_name_out(casadi_int i) { return MpmcaXyHexapodYawName_name_out(i); }
    static constexpr const char* GetName() { return "MpmcaXyHexapodYaw"; }
    static constexpr const char* GetSnakeCaseName() { return "mpmca_xy_hexapod_yaw"; }
};
} // namespace mpmca::control
