#pragma once
#include "mpmca_xy_hexapod_yaw/mpmca_xy_hexapod_yaw.hpp"
namespace mpmca::control::testing {
	using TestSimulatorType = mpmca::control::MpmcaXyHexapodYaw;
} // namespace mpmca::control::testing
