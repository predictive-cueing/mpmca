#pragma once
#include "mpmca_hexapod_pose/mpmca_hexapod_pose.hpp"
namespace mpmca::control::testing {
	using TestSimulatorType = mpmca::control::MpmcaHexapodPose;
} // namespace mpmca::control::testing
