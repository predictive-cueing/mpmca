#pragma once
#include "mpmca_industrial_robot_pose/mpmca_industrial_robot_pose.hpp"
namespace mpmca::control::testing {
	using TestSimulatorType = mpmca::control::MpmcaIndustrialRobotPose;
} // namespace mpmca::control::testing
