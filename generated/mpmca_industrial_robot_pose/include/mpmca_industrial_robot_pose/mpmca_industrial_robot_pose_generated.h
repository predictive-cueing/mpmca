/* This file was automatically generated by CasADi.
   The CasADi copyright holders make no ownership claim of its contents. */
#ifdef __cplusplus
extern "C" {
#endif

#ifndef casadi_real
#define casadi_real double
#endif

#ifndef CASADI_INT_TYPE
#define CASADI_INT_TYPE long long int
#endif

#include <casadi/mem.h>
int MpmcaIndustrialRobotPoseOutputJacobian(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaIndustrialRobotPoseOutputJacobian_incref(void);
void MpmcaIndustrialRobotPoseOutputJacobian_decref(void);
casadi_int MpmcaIndustrialRobotPoseOutputJacobian_n_in(void);
casadi_int MpmcaIndustrialRobotPoseOutputJacobian_n_out(void);
const char* MpmcaIndustrialRobotPoseOutputJacobian_name_in(casadi_int i);
const char* MpmcaIndustrialRobotPoseOutputJacobian_name_out(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseOutputJacobian_sparsity_in(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseOutputJacobian_sparsity_out(casadi_int i);
int MpmcaIndustrialRobotPoseOutputJacobian_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaIndustrialRobotPoseOutputJacobian_functions(void);
int MpmcaIndustrialRobotPoseOutput(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaIndustrialRobotPoseOutput_incref(void);
void MpmcaIndustrialRobotPoseOutput_decref(void);
casadi_int MpmcaIndustrialRobotPoseOutput_n_in(void);
casadi_int MpmcaIndustrialRobotPoseOutput_n_out(void);
const char* MpmcaIndustrialRobotPoseOutput_name_in(casadi_int i);
const char* MpmcaIndustrialRobotPoseOutput_name_out(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseOutput_sparsity_in(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseOutput_sparsity_out(casadi_int i);
int MpmcaIndustrialRobotPoseOutput_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaIndustrialRobotPoseOutput_functions(void);
int MpmcaIndustrialRobotPoseOutputHessian(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaIndustrialRobotPoseOutputHessian_incref(void);
void MpmcaIndustrialRobotPoseOutputHessian_decref(void);
casadi_int MpmcaIndustrialRobotPoseOutputHessian_n_in(void);
casadi_int MpmcaIndustrialRobotPoseOutputHessian_n_out(void);
const char* MpmcaIndustrialRobotPoseOutputHessian_name_in(casadi_int i);
const char* MpmcaIndustrialRobotPoseOutputHessian_name_out(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseOutputHessian_sparsity_in(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseOutputHessian_sparsity_out(casadi_int i);
int MpmcaIndustrialRobotPoseOutputHessian_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaIndustrialRobotPoseOutputHessian_functions(void);
int MpmcaIndustrialRobotPoseInequality(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaIndustrialRobotPoseInequality_incref(void);
void MpmcaIndustrialRobotPoseInequality_decref(void);
casadi_int MpmcaIndustrialRobotPoseInequality_n_in(void);
casadi_int MpmcaIndustrialRobotPoseInequality_n_out(void);
const char* MpmcaIndustrialRobotPoseInequality_name_in(casadi_int i);
const char* MpmcaIndustrialRobotPoseInequality_name_out(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseInequality_sparsity_in(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseInequality_sparsity_out(casadi_int i);
int MpmcaIndustrialRobotPoseInequality_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaIndustrialRobotPoseInequality_functions(void);
int MpmcaIndustrialRobotPoseInequalityJacobian(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaIndustrialRobotPoseInequalityJacobian_incref(void);
void MpmcaIndustrialRobotPoseInequalityJacobian_decref(void);
casadi_int MpmcaIndustrialRobotPoseInequalityJacobian_n_in(void);
casadi_int MpmcaIndustrialRobotPoseInequalityJacobian_n_out(void);
const char* MpmcaIndustrialRobotPoseInequalityJacobian_name_in(casadi_int i);
const char* MpmcaIndustrialRobotPoseInequalityJacobian_name_out(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseInequalityJacobian_sparsity_in(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseInequalityJacobian_sparsity_out(casadi_int i);
int MpmcaIndustrialRobotPoseInequalityJacobian_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaIndustrialRobotPoseInequalityJacobian_functions(void);
int MpmcaIndustrialRobotPoseInequalityHessian(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaIndustrialRobotPoseInequalityHessian_incref(void);
void MpmcaIndustrialRobotPoseInequalityHessian_decref(void);
casadi_int MpmcaIndustrialRobotPoseInequalityHessian_n_in(void);
casadi_int MpmcaIndustrialRobotPoseInequalityHessian_n_out(void);
const char* MpmcaIndustrialRobotPoseInequalityHessian_name_in(casadi_int i);
const char* MpmcaIndustrialRobotPoseInequalityHessian_name_out(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseInequalityHessian_sparsity_in(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseInequalityHessian_sparsity_out(casadi_int i);
int MpmcaIndustrialRobotPoseInequalityHessian_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaIndustrialRobotPoseInequalityHessian_functions(void);
int MpmcaIndustrialRobotPoseCost(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaIndustrialRobotPoseCost_incref(void);
void MpmcaIndustrialRobotPoseCost_decref(void);
casadi_int MpmcaIndustrialRobotPoseCost_n_in(void);
casadi_int MpmcaIndustrialRobotPoseCost_n_out(void);
const char* MpmcaIndustrialRobotPoseCost_name_in(casadi_int i);
const char* MpmcaIndustrialRobotPoseCost_name_out(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseCost_sparsity_in(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseCost_sparsity_out(casadi_int i);
int MpmcaIndustrialRobotPoseCost_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaIndustrialRobotPoseCost_functions(void);
int MpmcaIndustrialRobotPoseCostJacobian(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaIndustrialRobotPoseCostJacobian_incref(void);
void MpmcaIndustrialRobotPoseCostJacobian_decref(void);
casadi_int MpmcaIndustrialRobotPoseCostJacobian_n_in(void);
casadi_int MpmcaIndustrialRobotPoseCostJacobian_n_out(void);
const char* MpmcaIndustrialRobotPoseCostJacobian_name_in(casadi_int i);
const char* MpmcaIndustrialRobotPoseCostJacobian_name_out(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseCostJacobian_sparsity_in(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseCostJacobian_sparsity_out(casadi_int i);
int MpmcaIndustrialRobotPoseCostJacobian_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaIndustrialRobotPoseCostJacobian_functions(void);
int MpmcaIndustrialRobotPoseCostHessian(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaIndustrialRobotPoseCostHessian_incref(void);
void MpmcaIndustrialRobotPoseCostHessian_decref(void);
casadi_int MpmcaIndustrialRobotPoseCostHessian_n_in(void);
casadi_int MpmcaIndustrialRobotPoseCostHessian_n_out(void);
const char* MpmcaIndustrialRobotPoseCostHessian_name_in(casadi_int i);
const char* MpmcaIndustrialRobotPoseCostHessian_name_out(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseCostHessian_sparsity_in(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseCostHessian_sparsity_out(casadi_int i);
int MpmcaIndustrialRobotPoseCostHessian_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaIndustrialRobotPoseCostHessian_functions(void);
int MpmcaIndustrialRobotPoseLagrangeHessianGaussNewton(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaIndustrialRobotPoseLagrangeHessianGaussNewton_incref(void);
void MpmcaIndustrialRobotPoseLagrangeHessianGaussNewton_decref(void);
casadi_int MpmcaIndustrialRobotPoseLagrangeHessianGaussNewton_n_in(void);
casadi_int MpmcaIndustrialRobotPoseLagrangeHessianGaussNewton_n_out(void);
const char* MpmcaIndustrialRobotPoseLagrangeHessianGaussNewton_name_in(casadi_int i);
const char* MpmcaIndustrialRobotPoseLagrangeHessianGaussNewton_name_out(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseLagrangeHessianGaussNewton_sparsity_in(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseLagrangeHessianGaussNewton_sparsity_out(casadi_int i);
int MpmcaIndustrialRobotPoseLagrangeHessianGaussNewton_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaIndustrialRobotPoseLagrangeHessianGaussNewton_functions(void);
int MpmcaIndustrialRobotPoseLagrangeHessian(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaIndustrialRobotPoseLagrangeHessian_incref(void);
void MpmcaIndustrialRobotPoseLagrangeHessian_decref(void);
casadi_int MpmcaIndustrialRobotPoseLagrangeHessian_n_in(void);
casadi_int MpmcaIndustrialRobotPoseLagrangeHessian_n_out(void);
const char* MpmcaIndustrialRobotPoseLagrangeHessian_name_in(casadi_int i);
const char* MpmcaIndustrialRobotPoseLagrangeHessian_name_out(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseLagrangeHessian_sparsity_in(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseLagrangeHessian_sparsity_out(casadi_int i);
int MpmcaIndustrialRobotPoseLagrangeHessian_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaIndustrialRobotPoseLagrangeHessian_functions(void);
int MpmcaIndustrialRobotPoseSimulatorData(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaIndustrialRobotPoseSimulatorData_incref(void);
void MpmcaIndustrialRobotPoseSimulatorData_decref(void);
casadi_int MpmcaIndustrialRobotPoseSimulatorData_n_in(void);
casadi_int MpmcaIndustrialRobotPoseSimulatorData_n_out(void);
const char* MpmcaIndustrialRobotPoseSimulatorData_name_in(casadi_int i);
const char* MpmcaIndustrialRobotPoseSimulatorData_name_out(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseSimulatorData_sparsity_in(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseSimulatorData_sparsity_out(casadi_int i);
int MpmcaIndustrialRobotPoseSimulatorData_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaIndustrialRobotPoseSimulatorData_functions(void);
int MpmcaIndustrialRobotPoseInfo(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaIndustrialRobotPoseInfo_incref(void);
void MpmcaIndustrialRobotPoseInfo_decref(void);
casadi_int MpmcaIndustrialRobotPoseInfo_n_in(void);
casadi_int MpmcaIndustrialRobotPoseInfo_n_out(void);
const char* MpmcaIndustrialRobotPoseInfo_name_in(casadi_int i);
const char* MpmcaIndustrialRobotPoseInfo_name_out(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseInfo_sparsity_in(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseInfo_sparsity_out(casadi_int i);
int MpmcaIndustrialRobotPoseInfo_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaIndustrialRobotPoseInfo_functions(void);
int MpmcaIndustrialRobotPoseName(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaIndustrialRobotPoseName_incref(void);
void MpmcaIndustrialRobotPoseName_decref(void);
casadi_int MpmcaIndustrialRobotPoseName_n_in(void);
casadi_int MpmcaIndustrialRobotPoseName_n_out(void);
const char* MpmcaIndustrialRobotPoseName_name_in(casadi_int i);
const char* MpmcaIndustrialRobotPoseName_name_out(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseName_sparsity_in(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseName_sparsity_out(casadi_int i);
int MpmcaIndustrialRobotPoseName_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaIndustrialRobotPoseName_functions(void);
int MpmcaIndustrialRobotPoseStoppabilityConstraints(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaIndustrialRobotPoseStoppabilityConstraints_incref(void);
void MpmcaIndustrialRobotPoseStoppabilityConstraints_decref(void);
casadi_int MpmcaIndustrialRobotPoseStoppabilityConstraints_n_in(void);
casadi_int MpmcaIndustrialRobotPoseStoppabilityConstraints_n_out(void);
const char* MpmcaIndustrialRobotPoseStoppabilityConstraints_name_in(casadi_int i);
const char* MpmcaIndustrialRobotPoseStoppabilityConstraints_name_out(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseStoppabilityConstraints_sparsity_in(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseStoppabilityConstraints_sparsity_out(casadi_int i);
int MpmcaIndustrialRobotPoseStoppabilityConstraints_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaIndustrialRobotPoseStoppabilityConstraints_functions(void);
int MpmcaIndustrialRobotPoseStoppabilityConstraintsHessian(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaIndustrialRobotPoseStoppabilityConstraintsHessian_incref(void);
void MpmcaIndustrialRobotPoseStoppabilityConstraintsHessian_decref(void);
casadi_int MpmcaIndustrialRobotPoseStoppabilityConstraintsHessian_n_in(void);
casadi_int MpmcaIndustrialRobotPoseStoppabilityConstraintsHessian_n_out(void);
const char* MpmcaIndustrialRobotPoseStoppabilityConstraintsHessian_name_in(casadi_int i);
const char* MpmcaIndustrialRobotPoseStoppabilityConstraintsHessian_name_out(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseStoppabilityConstraintsHessian_sparsity_in(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseStoppabilityConstraintsHessian_sparsity_out(casadi_int i);
int MpmcaIndustrialRobotPoseStoppabilityConstraintsHessian_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaIndustrialRobotPoseStoppabilityConstraintsHessian_functions(void);
int MpmcaIndustrialRobotPoseStoppabilityConstraintsOriginal(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaIndustrialRobotPoseStoppabilityConstraintsOriginal_incref(void);
void MpmcaIndustrialRobotPoseStoppabilityConstraintsOriginal_decref(void);
casadi_int MpmcaIndustrialRobotPoseStoppabilityConstraintsOriginal_n_in(void);
casadi_int MpmcaIndustrialRobotPoseStoppabilityConstraintsOriginal_n_out(void);
const char* MpmcaIndustrialRobotPoseStoppabilityConstraintsOriginal_name_in(casadi_int i);
const char* MpmcaIndustrialRobotPoseStoppabilityConstraintsOriginal_name_out(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseStoppabilityConstraintsOriginal_sparsity_in(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseStoppabilityConstraintsOriginal_sparsity_out(casadi_int i);
int MpmcaIndustrialRobotPoseStoppabilityConstraintsOriginal_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaIndustrialRobotPoseStoppabilityConstraintsOriginal_functions(void);
int MpmcaIndustrialRobotPoseStoppabilityConstraintsHessianOriginal(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaIndustrialRobotPoseStoppabilityConstraintsHessianOriginal_incref(void);
void MpmcaIndustrialRobotPoseStoppabilityConstraintsHessianOriginal_decref(void);
casadi_int MpmcaIndustrialRobotPoseStoppabilityConstraintsHessianOriginal_n_in(void);
casadi_int MpmcaIndustrialRobotPoseStoppabilityConstraintsHessianOriginal_n_out(void);
const char* MpmcaIndustrialRobotPoseStoppabilityConstraintsHessianOriginal_name_in(casadi_int i);
const char* MpmcaIndustrialRobotPoseStoppabilityConstraintsHessianOriginal_name_out(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseStoppabilityConstraintsHessianOriginal_sparsity_in(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseStoppabilityConstraintsHessianOriginal_sparsity_out(casadi_int i);
int MpmcaIndustrialRobotPoseStoppabilityConstraintsHessianOriginal_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaIndustrialRobotPoseStoppabilityConstraintsHessianOriginal_functions(void);
int MpmcaIndustrialRobotPoseHeadPva(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaIndustrialRobotPoseHeadPva_incref(void);
void MpmcaIndustrialRobotPoseHeadPva_decref(void);
casadi_int MpmcaIndustrialRobotPoseHeadPva_n_in(void);
casadi_int MpmcaIndustrialRobotPoseHeadPva_n_out(void);
const char* MpmcaIndustrialRobotPoseHeadPva_name_in(casadi_int i);
const char* MpmcaIndustrialRobotPoseHeadPva_name_out(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseHeadPva_sparsity_in(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseHeadPva_sparsity_out(casadi_int i);
int MpmcaIndustrialRobotPoseHeadPva_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaIndustrialRobotPoseHeadPva_functions(void);
int MpmcaIndustrialRobotPoseHeadInertial(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaIndustrialRobotPoseHeadInertial_incref(void);
void MpmcaIndustrialRobotPoseHeadInertial_decref(void);
casadi_int MpmcaIndustrialRobotPoseHeadInertial_n_in(void);
casadi_int MpmcaIndustrialRobotPoseHeadInertial_n_out(void);
const char* MpmcaIndustrialRobotPoseHeadInertial_name_in(casadi_int i);
const char* MpmcaIndustrialRobotPoseHeadInertial_name_out(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseHeadInertial_sparsity_in(casadi_int i);
const casadi_int* MpmcaIndustrialRobotPoseHeadInertial_sparsity_out(casadi_int i);
int MpmcaIndustrialRobotPoseHeadInertial_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaIndustrialRobotPoseHeadInertial_functions(void);
#ifdef __cplusplus
} /* extern "C" */
#endif
