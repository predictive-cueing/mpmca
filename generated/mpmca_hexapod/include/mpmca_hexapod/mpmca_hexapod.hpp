#pragma once
#include "mpmca_hexapod_generated.h"
#include "mpmca/control/components.hpp"
#include "mpmca/control/templates/component.hpp"
#include "mpmca/control/templates/component_list.hpp"
#include "mpmca/control/templates/dimension.hpp"

namespace mpmca::control {
struct MpmcaHexapod
{
    using ComponentList = templates::ComponentList<components::TransformationMatrix,components::Hexapod>;
    static constexpr std::size_t NY_SIMULATOR = 9;
    using Dimension = templates::DimensionsT<MpmcaHexapod>;
    using AxesVector = Vector<Dimension::NQ>;
    using StateVector = Vector<Dimension::NX>;
    using AlgStateVector = Vector<Dimension::NZ>;
    using InputVector = Vector<Dimension::NU>;
    using ConstraintVector = Vector<Dimension::NC>;
    using ParameterVector = Vector<Dimension::NP>;
    using OutputVector = Vector<Dimension::NY>;
    using InfoVector = Vector<Dimension::NI>;
    using StateStateMatrix = Matrix<Dimension::NX, Dimension::NX>;
    using StateInputMatrix = Matrix<Dimension::NX, Dimension::NU>;
    using InputInputMatrix = Matrix<Dimension::NU, Dimension::NU>;
    using OutputOutputMatrix = Matrix<Dimension::NY, Dimension::NY>;
    using OutputStateMatrix = Matrix<Dimension::NY, Dimension::NX>;
    using OutputInputMatrix = Matrix<Dimension::NY, Dimension::NU>;
    using ConstraintStateMatrix = Matrix<Dimension::NC, Dimension::NX>;
    using ConstraintInputMatrix = Matrix<Dimension::NC, Dimension::NU>;
    using OutputStateStateMatrix = Matrix<Dimension::NX * Dimension::NY, Dimension::NX>;
    using OutputStateInputMatrix = Matrix<Dimension::NX * Dimension::NY, Dimension::NU>;
    using OutputInputInputMatrix = Matrix<Dimension::NU * Dimension::NY, Dimension::NU>;
    using ConstraintStateStateMatrix = Matrix<Dimension::NX * Dimension::NC, Dimension::NX>;
    using ConstraintStateInputMatrix = Matrix<Dimension::NX * Dimension::NC, Dimension::NU>;
    using ConstraintInputInputMatrix = Matrix<Dimension::NU * Dimension::NC, Dimension::NU>;
    using InputMatrix = Eigen::Matrix<double, Dimension::NU, Eigen::Dynamic>;
    using StateMatrix = Eigen::Matrix<double, Dimension::NX, Eigen::Dynamic>;
    using OutputMatrix = Eigen::Matrix<double, Dimension::NY, Eigen::Dynamic>;
    static casadi_functions* Output_functions(void) { return MpmcaHexapodOutput_functions(); }
    static casadi_functions* OutputJacobian_functions(void) { return MpmcaHexapodOutputJacobian_functions(); }
    static casadi_functions* OutputHessian_functions(void) { return MpmcaHexapodOutputHessian_functions(); }
    static casadi_functions* Inequality_functions(void) { return MpmcaHexapodInequality_functions(); }
    static casadi_functions* InequalityJacobian_functions(void) { return MpmcaHexapodInequalityJacobian_functions(); }
    static casadi_functions* InequalityHessian_functions(void) { return MpmcaHexapodInequalityHessian_functions(); }
    static casadi_functions* Cost_functions(void) { return MpmcaHexapodCost_functions(); }
    static casadi_functions* CostJacobian_functions(void) { return MpmcaHexapodCostJacobian_functions(); }
    static casadi_functions* CostHessian_functions(void) { return MpmcaHexapodCostHessian_functions(); }
    static casadi_functions* LagrangeHessianGaussNewton_functions(void) { return MpmcaHexapodLagrangeHessianGaussNewton_functions(); }
    static casadi_functions* LagrangeHessian_functions(void) { return MpmcaHexapodLagrangeHessian_functions(); }
    static casadi_functions* SimulatorData_functions(void) { return MpmcaHexapodSimulatorData_functions(); }
    static casadi_functions* HeadPva_functions(void) { return MpmcaHexapodHeadPva_functions(); }
    static casadi_functions* HeadInertial_functions(void) { return MpmcaHexapodHeadInertial_functions(); }
    static casadi_functions* Info_functions(void) { return MpmcaHexapodInfo_functions(); }
    static casadi_functions* Name_functions(void) { return MpmcaHexapodName_functions(); }
    static casadi_functions* StoppabilityConstraints_functions(void) { return MpmcaHexapodStoppabilityConstraints_functions(); }
    static casadi_functions* StoppabilityConstraintsHessian_functions(void) { return MpmcaHexapodStoppabilityConstraintsHessian_functions(); }
    static casadi_functions* StoppabilityConstraintsOriginal_functions(void) { return MpmcaHexapodStoppabilityConstraintsOriginal_functions(); }
    static casadi_functions* StoppabilityConstraintsHessianOriginal_functions(void) { return MpmcaHexapodStoppabilityConstraintsHessianOriginal_functions(); }
    static const char* name_name_out(casadi_int i) { return MpmcaHexapodName_name_out(i); }
    static constexpr const char* GetName() { return "MpmcaHexapod"; }
    static constexpr const char* GetSnakeCaseName() { return "mpmca_hexapod"; }
};
} // namespace mpmca::control
