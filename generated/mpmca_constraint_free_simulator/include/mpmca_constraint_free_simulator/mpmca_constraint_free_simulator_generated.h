/* This file was automatically generated by CasADi.
   The CasADi copyright holders make no ownership claim of its contents. */
#ifdef __cplusplus
extern "C" {
#endif

#ifndef casadi_real
#define casadi_real double
#endif

#ifndef CASADI_INT_TYPE
#define CASADI_INT_TYPE long long int
#endif

#include <casadi/mem.h>
int MpmcaConstraintFreeSimulatorOutputJacobian(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaConstraintFreeSimulatorOutputJacobian_incref(void);
void MpmcaConstraintFreeSimulatorOutputJacobian_decref(void);
casadi_int MpmcaConstraintFreeSimulatorOutputJacobian_n_in(void);
casadi_int MpmcaConstraintFreeSimulatorOutputJacobian_n_out(void);
const char* MpmcaConstraintFreeSimulatorOutputJacobian_name_in(casadi_int i);
const char* MpmcaConstraintFreeSimulatorOutputJacobian_name_out(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorOutputJacobian_sparsity_in(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorOutputJacobian_sparsity_out(casadi_int i);
int MpmcaConstraintFreeSimulatorOutputJacobian_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaConstraintFreeSimulatorOutputJacobian_functions(void);
int MpmcaConstraintFreeSimulatorOutput(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaConstraintFreeSimulatorOutput_incref(void);
void MpmcaConstraintFreeSimulatorOutput_decref(void);
casadi_int MpmcaConstraintFreeSimulatorOutput_n_in(void);
casadi_int MpmcaConstraintFreeSimulatorOutput_n_out(void);
const char* MpmcaConstraintFreeSimulatorOutput_name_in(casadi_int i);
const char* MpmcaConstraintFreeSimulatorOutput_name_out(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorOutput_sparsity_in(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorOutput_sparsity_out(casadi_int i);
int MpmcaConstraintFreeSimulatorOutput_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaConstraintFreeSimulatorOutput_functions(void);
int MpmcaConstraintFreeSimulatorOutputHessian(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaConstraintFreeSimulatorOutputHessian_incref(void);
void MpmcaConstraintFreeSimulatorOutputHessian_decref(void);
casadi_int MpmcaConstraintFreeSimulatorOutputHessian_n_in(void);
casadi_int MpmcaConstraintFreeSimulatorOutputHessian_n_out(void);
const char* MpmcaConstraintFreeSimulatorOutputHessian_name_in(casadi_int i);
const char* MpmcaConstraintFreeSimulatorOutputHessian_name_out(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorOutputHessian_sparsity_in(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorOutputHessian_sparsity_out(casadi_int i);
int MpmcaConstraintFreeSimulatorOutputHessian_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaConstraintFreeSimulatorOutputHessian_functions(void);
int MpmcaConstraintFreeSimulatorInequality(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaConstraintFreeSimulatorInequality_incref(void);
void MpmcaConstraintFreeSimulatorInequality_decref(void);
casadi_int MpmcaConstraintFreeSimulatorInequality_n_in(void);
casadi_int MpmcaConstraintFreeSimulatorInequality_n_out(void);
const char* MpmcaConstraintFreeSimulatorInequality_name_in(casadi_int i);
const char* MpmcaConstraintFreeSimulatorInequality_name_out(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorInequality_sparsity_in(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorInequality_sparsity_out(casadi_int i);
int MpmcaConstraintFreeSimulatorInequality_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaConstraintFreeSimulatorInequality_functions(void);
int MpmcaConstraintFreeSimulatorInequalityJacobian(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaConstraintFreeSimulatorInequalityJacobian_incref(void);
void MpmcaConstraintFreeSimulatorInequalityJacobian_decref(void);
casadi_int MpmcaConstraintFreeSimulatorInequalityJacobian_n_in(void);
casadi_int MpmcaConstraintFreeSimulatorInequalityJacobian_n_out(void);
const char* MpmcaConstraintFreeSimulatorInequalityJacobian_name_in(casadi_int i);
const char* MpmcaConstraintFreeSimulatorInequalityJacobian_name_out(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorInequalityJacobian_sparsity_in(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorInequalityJacobian_sparsity_out(casadi_int i);
int MpmcaConstraintFreeSimulatorInequalityJacobian_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaConstraintFreeSimulatorInequalityJacobian_functions(void);
int MpmcaConstraintFreeSimulatorInequalityHessian(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaConstraintFreeSimulatorInequalityHessian_incref(void);
void MpmcaConstraintFreeSimulatorInequalityHessian_decref(void);
casadi_int MpmcaConstraintFreeSimulatorInequalityHessian_n_in(void);
casadi_int MpmcaConstraintFreeSimulatorInequalityHessian_n_out(void);
const char* MpmcaConstraintFreeSimulatorInequalityHessian_name_in(casadi_int i);
const char* MpmcaConstraintFreeSimulatorInequalityHessian_name_out(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorInequalityHessian_sparsity_in(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorInequalityHessian_sparsity_out(casadi_int i);
int MpmcaConstraintFreeSimulatorInequalityHessian_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaConstraintFreeSimulatorInequalityHessian_functions(void);
int MpmcaConstraintFreeSimulatorCost(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaConstraintFreeSimulatorCost_incref(void);
void MpmcaConstraintFreeSimulatorCost_decref(void);
casadi_int MpmcaConstraintFreeSimulatorCost_n_in(void);
casadi_int MpmcaConstraintFreeSimulatorCost_n_out(void);
const char* MpmcaConstraintFreeSimulatorCost_name_in(casadi_int i);
const char* MpmcaConstraintFreeSimulatorCost_name_out(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorCost_sparsity_in(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorCost_sparsity_out(casadi_int i);
int MpmcaConstraintFreeSimulatorCost_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaConstraintFreeSimulatorCost_functions(void);
int MpmcaConstraintFreeSimulatorCostJacobian(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaConstraintFreeSimulatorCostJacobian_incref(void);
void MpmcaConstraintFreeSimulatorCostJacobian_decref(void);
casadi_int MpmcaConstraintFreeSimulatorCostJacobian_n_in(void);
casadi_int MpmcaConstraintFreeSimulatorCostJacobian_n_out(void);
const char* MpmcaConstraintFreeSimulatorCostJacobian_name_in(casadi_int i);
const char* MpmcaConstraintFreeSimulatorCostJacobian_name_out(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorCostJacobian_sparsity_in(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorCostJacobian_sparsity_out(casadi_int i);
int MpmcaConstraintFreeSimulatorCostJacobian_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaConstraintFreeSimulatorCostJacobian_functions(void);
int MpmcaConstraintFreeSimulatorCostHessian(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaConstraintFreeSimulatorCostHessian_incref(void);
void MpmcaConstraintFreeSimulatorCostHessian_decref(void);
casadi_int MpmcaConstraintFreeSimulatorCostHessian_n_in(void);
casadi_int MpmcaConstraintFreeSimulatorCostHessian_n_out(void);
const char* MpmcaConstraintFreeSimulatorCostHessian_name_in(casadi_int i);
const char* MpmcaConstraintFreeSimulatorCostHessian_name_out(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorCostHessian_sparsity_in(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorCostHessian_sparsity_out(casadi_int i);
int MpmcaConstraintFreeSimulatorCostHessian_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaConstraintFreeSimulatorCostHessian_functions(void);
int MpmcaConstraintFreeSimulatorLagrangeHessianGaussNewton(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaConstraintFreeSimulatorLagrangeHessianGaussNewton_incref(void);
void MpmcaConstraintFreeSimulatorLagrangeHessianGaussNewton_decref(void);
casadi_int MpmcaConstraintFreeSimulatorLagrangeHessianGaussNewton_n_in(void);
casadi_int MpmcaConstraintFreeSimulatorLagrangeHessianGaussNewton_n_out(void);
const char* MpmcaConstraintFreeSimulatorLagrangeHessianGaussNewton_name_in(casadi_int i);
const char* MpmcaConstraintFreeSimulatorLagrangeHessianGaussNewton_name_out(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorLagrangeHessianGaussNewton_sparsity_in(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorLagrangeHessianGaussNewton_sparsity_out(casadi_int i);
int MpmcaConstraintFreeSimulatorLagrangeHessianGaussNewton_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaConstraintFreeSimulatorLagrangeHessianGaussNewton_functions(void);
int MpmcaConstraintFreeSimulatorLagrangeHessian(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaConstraintFreeSimulatorLagrangeHessian_incref(void);
void MpmcaConstraintFreeSimulatorLagrangeHessian_decref(void);
casadi_int MpmcaConstraintFreeSimulatorLagrangeHessian_n_in(void);
casadi_int MpmcaConstraintFreeSimulatorLagrangeHessian_n_out(void);
const char* MpmcaConstraintFreeSimulatorLagrangeHessian_name_in(casadi_int i);
const char* MpmcaConstraintFreeSimulatorLagrangeHessian_name_out(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorLagrangeHessian_sparsity_in(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorLagrangeHessian_sparsity_out(casadi_int i);
int MpmcaConstraintFreeSimulatorLagrangeHessian_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaConstraintFreeSimulatorLagrangeHessian_functions(void);
int MpmcaConstraintFreeSimulatorSimulatorData(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaConstraintFreeSimulatorSimulatorData_incref(void);
void MpmcaConstraintFreeSimulatorSimulatorData_decref(void);
casadi_int MpmcaConstraintFreeSimulatorSimulatorData_n_in(void);
casadi_int MpmcaConstraintFreeSimulatorSimulatorData_n_out(void);
const char* MpmcaConstraintFreeSimulatorSimulatorData_name_in(casadi_int i);
const char* MpmcaConstraintFreeSimulatorSimulatorData_name_out(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorSimulatorData_sparsity_in(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorSimulatorData_sparsity_out(casadi_int i);
int MpmcaConstraintFreeSimulatorSimulatorData_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaConstraintFreeSimulatorSimulatorData_functions(void);
int MpmcaConstraintFreeSimulatorInfo(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaConstraintFreeSimulatorInfo_incref(void);
void MpmcaConstraintFreeSimulatorInfo_decref(void);
casadi_int MpmcaConstraintFreeSimulatorInfo_n_in(void);
casadi_int MpmcaConstraintFreeSimulatorInfo_n_out(void);
const char* MpmcaConstraintFreeSimulatorInfo_name_in(casadi_int i);
const char* MpmcaConstraintFreeSimulatorInfo_name_out(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorInfo_sparsity_in(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorInfo_sparsity_out(casadi_int i);
int MpmcaConstraintFreeSimulatorInfo_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaConstraintFreeSimulatorInfo_functions(void);
int MpmcaConstraintFreeSimulatorName(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaConstraintFreeSimulatorName_incref(void);
void MpmcaConstraintFreeSimulatorName_decref(void);
casadi_int MpmcaConstraintFreeSimulatorName_n_in(void);
casadi_int MpmcaConstraintFreeSimulatorName_n_out(void);
const char* MpmcaConstraintFreeSimulatorName_name_in(casadi_int i);
const char* MpmcaConstraintFreeSimulatorName_name_out(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorName_sparsity_in(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorName_sparsity_out(casadi_int i);
int MpmcaConstraintFreeSimulatorName_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaConstraintFreeSimulatorName_functions(void);
int MpmcaConstraintFreeSimulatorStoppabilityConstraints(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaConstraintFreeSimulatorStoppabilityConstraints_incref(void);
void MpmcaConstraintFreeSimulatorStoppabilityConstraints_decref(void);
casadi_int MpmcaConstraintFreeSimulatorStoppabilityConstraints_n_in(void);
casadi_int MpmcaConstraintFreeSimulatorStoppabilityConstraints_n_out(void);
const char* MpmcaConstraintFreeSimulatorStoppabilityConstraints_name_in(casadi_int i);
const char* MpmcaConstraintFreeSimulatorStoppabilityConstraints_name_out(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorStoppabilityConstraints_sparsity_in(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorStoppabilityConstraints_sparsity_out(casadi_int i);
int MpmcaConstraintFreeSimulatorStoppabilityConstraints_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaConstraintFreeSimulatorStoppabilityConstraints_functions(void);
int MpmcaConstraintFreeSimulatorStoppabilityConstraintsHessian(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaConstraintFreeSimulatorStoppabilityConstraintsHessian_incref(void);
void MpmcaConstraintFreeSimulatorStoppabilityConstraintsHessian_decref(void);
casadi_int MpmcaConstraintFreeSimulatorStoppabilityConstraintsHessian_n_in(void);
casadi_int MpmcaConstraintFreeSimulatorStoppabilityConstraintsHessian_n_out(void);
const char* MpmcaConstraintFreeSimulatorStoppabilityConstraintsHessian_name_in(casadi_int i);
const char* MpmcaConstraintFreeSimulatorStoppabilityConstraintsHessian_name_out(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorStoppabilityConstraintsHessian_sparsity_in(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorStoppabilityConstraintsHessian_sparsity_out(casadi_int i);
int MpmcaConstraintFreeSimulatorStoppabilityConstraintsHessian_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaConstraintFreeSimulatorStoppabilityConstraintsHessian_functions(void);
int MpmcaConstraintFreeSimulatorStoppabilityConstraintsOriginal(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaConstraintFreeSimulatorStoppabilityConstraintsOriginal_incref(void);
void MpmcaConstraintFreeSimulatorStoppabilityConstraintsOriginal_decref(void);
casadi_int MpmcaConstraintFreeSimulatorStoppabilityConstraintsOriginal_n_in(void);
casadi_int MpmcaConstraintFreeSimulatorStoppabilityConstraintsOriginal_n_out(void);
const char* MpmcaConstraintFreeSimulatorStoppabilityConstraintsOriginal_name_in(casadi_int i);
const char* MpmcaConstraintFreeSimulatorStoppabilityConstraintsOriginal_name_out(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorStoppabilityConstraintsOriginal_sparsity_in(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorStoppabilityConstraintsOriginal_sparsity_out(casadi_int i);
int MpmcaConstraintFreeSimulatorStoppabilityConstraintsOriginal_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaConstraintFreeSimulatorStoppabilityConstraintsOriginal_functions(void);
int MpmcaConstraintFreeSimulatorStoppabilityConstraintsHessianOriginal(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaConstraintFreeSimulatorStoppabilityConstraintsHessianOriginal_incref(void);
void MpmcaConstraintFreeSimulatorStoppabilityConstraintsHessianOriginal_decref(void);
casadi_int MpmcaConstraintFreeSimulatorStoppabilityConstraintsHessianOriginal_n_in(void);
casadi_int MpmcaConstraintFreeSimulatorStoppabilityConstraintsHessianOriginal_n_out(void);
const char* MpmcaConstraintFreeSimulatorStoppabilityConstraintsHessianOriginal_name_in(casadi_int i);
const char* MpmcaConstraintFreeSimulatorStoppabilityConstraintsHessianOriginal_name_out(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorStoppabilityConstraintsHessianOriginal_sparsity_in(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorStoppabilityConstraintsHessianOriginal_sparsity_out(casadi_int i);
int MpmcaConstraintFreeSimulatorStoppabilityConstraintsHessianOriginal_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaConstraintFreeSimulatorStoppabilityConstraintsHessianOriginal_functions(void);
int MpmcaConstraintFreeSimulatorHeadPva(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaConstraintFreeSimulatorHeadPva_incref(void);
void MpmcaConstraintFreeSimulatorHeadPva_decref(void);
casadi_int MpmcaConstraintFreeSimulatorHeadPva_n_in(void);
casadi_int MpmcaConstraintFreeSimulatorHeadPva_n_out(void);
const char* MpmcaConstraintFreeSimulatorHeadPva_name_in(casadi_int i);
const char* MpmcaConstraintFreeSimulatorHeadPva_name_out(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorHeadPva_sparsity_in(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorHeadPva_sparsity_out(casadi_int i);
int MpmcaConstraintFreeSimulatorHeadPva_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaConstraintFreeSimulatorHeadPva_functions(void);
int MpmcaConstraintFreeSimulatorHeadInertial(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, void* mem);
void MpmcaConstraintFreeSimulatorHeadInertial_incref(void);
void MpmcaConstraintFreeSimulatorHeadInertial_decref(void);
casadi_int MpmcaConstraintFreeSimulatorHeadInertial_n_in(void);
casadi_int MpmcaConstraintFreeSimulatorHeadInertial_n_out(void);
const char* MpmcaConstraintFreeSimulatorHeadInertial_name_in(casadi_int i);
const char* MpmcaConstraintFreeSimulatorHeadInertial_name_out(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorHeadInertial_sparsity_in(casadi_int i);
const casadi_int* MpmcaConstraintFreeSimulatorHeadInertial_sparsity_out(casadi_int i);
int MpmcaConstraintFreeSimulatorHeadInertial_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w);
casadi_functions* MpmcaConstraintFreeSimulatorHeadInertial_functions(void);
#ifdef __cplusplus
} /* extern "C" */
#endif
