#pragma once
#include "mpmca_constraint_free_simulator/mpmca_constraint_free_simulator.hpp"
namespace mpmca::control::testing {
	using TestSimulatorType = mpmca::control::MpmcaConstraintFreeSimulator;
} // namespace mpmca::control::testing
