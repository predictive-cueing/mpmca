# Copyright Frank Drop and Mario Olivari ("Predictive cueing").
# Please read carefully the terms and conditions (root/LICENCE.md) and
# any accompanying documentation before you download and/or use
# the Model Predictive Motion Cueing Algorithm (MPMCA) software.

import importlib
import pathlib

from generate.model_generator import ModelGenerator

if __name__ == "__main__":
    # Variable to set

    project_dir = str(pathlib.Path(__file__).parent.resolve()) + "/../../"
    simulators = [
        "mpmca_constraint_free_simulator",
        "mpmca_hexapod_pose",
        "mpmca_hexapod",
        "mpmca_industrial_robot_inertial",
        "mpmca_industrial_robot_pose",
        "mpmca_xy_hexapod_yaw_pose",
        "mpmca_xy_hexapod_yaw",
    ]
    # simulators = ["mpmca_industrial_robot_inertial"]
    # Generate code
    for simulator_k in simulators:

        print("Start : " + simulator_k)

        with_hessian = True  # (sys.argv[5] == 'ON')

        # Create generator obj
        this_simulator = importlib.import_module("simulator_definitions." + simulator_k)
        generator = ModelGenerator(this_simulator.Simulator(), project_dir + "generated/" + simulator_k, with_hessian)

        # Generate code
        generator.GenerateCode()

        # Make some test data
        generator.GenerateTestData()

        # Write the file with #define
        generator.GenerateHeaders()

        print("\n")
