# type: ignore
# Copyright Frank Drop and Mario Olivari ("Predictive cueing").
# Please read carefully the terms and conditions (root/LICENCE.md) and
# any accompanying documentation before you download and/or use
# the Model Predictive Motion Cueing Algorithm (MPMCA) software.
#
# Generate C code for motion platform.
#

import itertools
import json
import os
import shutil
import tempfile
from typing import Any, Dict, List, Tuple, Union

import casadi
import casadi.tools as ct
import numpy
from _io import TextIOWrapper
from casadi.casadi import MX, Function
from casadi_extras.structure3 import MXVeccatStruct
from customizable_simulator import extras
from numpy import ndarray


class NumpyEncoder(json.JSONEncoder):
    def default(self, obj: ndarray) -> List[float]:  # pylint: disable=E0202
        if isinstance(obj, numpy.ndarray):
            # Make 2d array
            rc = obj.shape
            if len(rc) == 1:
                obj = obj.reshape(rc[0], 1)

            # Convert to list
            obj_list = obj.tolist()

            # Flatten
            obj_fl = list(itertools.chain.from_iterable(obj_list))

            # Replace inf
            obj_inf = ["inf" if x == numpy.inf else "-inf" if x == -numpy.inf else x for x in obj_fl]

            # Return
            return obj_inf
        else:
            return super().default(self, obj)  # pylint: disable=E1121


def ensure_dir_exist(dirname: str) -> None:
    try:
        os.makedirs(dirname)
    except OSError:
        if os.path.exists(dirname):
            # We are nearly safe
            pass
        else:
            # There was an error on creation, so make sure we know about it
            raise


def move_file(src: str, dst: str) -> None:
    dirname = os.path.dirname(dst)
    if dirname:
        ensure_dir_exist(dirname)
    shutil.move(src, dst)


def write_matrix(file, item):
    # Write dimensions
    it_sh = item.shape
    if len(it_sh) == 1:
        file.write("{0} {1}\n".format(item.shape[0], 1))
    else:
        file.write("{0} {1}\n".format(item.shape[0], item.shape[1]))

    # Substitute inf values
    item_array = numpy.array(item)
    item_array[item_array == float("inf")] = 1000.0
    item_array[item_array == float("-inf")] = -1000.0

    # Write file
    item_array.tofile(file, " ")
    file.write("\n")


def jacobian_xu(y: Union[MX, MXVeccatStruct], x: MXVeccatStruct, u: MX) -> Tuple[MX, MX]:
    # Jacobian
    # dy/ (dx.T)
    dy_dx = casadi.jacobian(y, x)
    # dy/ (du.T)
    dy_du = casadi.jacobian(y, u)

    return dy_dx, dy_du


def jacobianHessian_xu(y: Union[MX, MXVeccatStruct], x: MXVeccatStruct, u: MX) -> Tuple[MX, MX, MX, MX, MX, MX]:

    # Jacobian
    dy_dx, dy_du = jacobian_xu(y, x, u)

    # Hessian as jacobian of jacobian
    # d ( vec( dy/dx ) )/ (dx.T)
    d2y_dx2 = casadi.jacobian(dy_dx.T, x)
    # d ( vec( dy/dx ) )/ (du.T)
    d2y_dx_du = casadi.jacobian(dy_dx.T, u)
    # d ( vec( dy/du ) )/ (dx.T)
    d2y_du_dx = casadi.jacobian(dy_du.T, x)
    # d ( vec( dy/du ) )/ (du.T)
    d2y_du_du = casadi.jacobian(dy_du.T, u)

    return dy_dx, dy_du, d2y_dx2, d2y_dx_du, d2y_du_dx, d2y_du_du


class ModelGenerator:

    def __init__(self, model: Any, generated_files_dir: str, withHessian: bool = False) -> None:
        self.model = model
        self.withHessian = withHessian

        self.generic_test_header_path = generated_files_dir + "/include/generic_test_header.hpp"
        self.header_path = (
            generated_files_dir + "/include/" + self.model.snake_case_name + "/" + self.model.snake_case_name + "_generated.h"
        )
        self.source_path = generated_files_dir + "/src/" + self.model.snake_case_name + "_generated.c"
        self.test_data_path = generated_files_dir + "/data/" + self.model.snake_case_name + "_data.json"
        self.define_data_path = generated_files_dir + "/include/"

    def _makeOutput(self) -> Dict[str, Function]:
        x = self.model.state.expr
        z = self.model.algState.expr
        u = self.model.input.expr
        p = self.model.param.expr

        # Transformation matrix from HF to PF
        T_HP = casadi.MX.sym("T_HP", 4, 4)

        # Output: y = f(x, z, u)
        y = self.model.output(x=x, z=z, u=u, p=p, g=self.model.gravity, T_HP=T_HP)["y"]

        # Jacobian and Hessian
        dy_dx, dy_du, d2y_dx2, d2y_dx_du, d2y_du_dx, d2y_du2 = jacobianHessian_xu(y, x, u)

        # Functions
        outputFunction = casadi.Function(
            self.model.camel_case_name + "Output",
            [x, z, u, p, T_HP],
            [casadi.densify(y)],
            ["x", "z", "u", "p", "T_HP"],
            ["y"],
        )

        outputJacobianFunction = casadi.Function(
            self.model.camel_case_name + "OutputJacobian",
            [x, z, u, p, T_HP],
            [casadi.densify(y), casadi.densify(dy_dx), casadi.densify(dy_du)],
            ["x", "z", "u", "p", "T_HP"],
            ["y", "dy_dx", "dy_du"],
        )
        if self.withHessian:
            outputHessianFunction = casadi.Function(
                self.model.camel_case_name + "OutputHessian",
                [x, z, u, p, T_HP],
                [casadi.densify(d2y_dx2), casadi.densify(d2y_dx_du), casadi.densify(d2y_du2)],
                ["x", "z", "u", "p", "T_HP"],
                ["d2y_dx2", "d2y_dx_du", "d2y_du2"],
            )
        else:
            empty = casadi.MX.sym("empty", 0)
            outputHessianFunction = casadi.Function(
                self.model.camel_case_name + "OutputHessian",
                [x, z, u, p, T_HP],
                [empty, empty, empty],
                ["x", "z", "u", "p", "T_HP"],
                ["d2y_dx2", "d2y_dx_du", "d2y_du2"],
            )

        allOutputFunctions = {
            self.model.camel_case_name + "OutputJacobianFunction": outputJacobianFunction.expand(),
            self.model.camel_case_name + "OutputFunction": outputFunction.expand(),
            self.model.camel_case_name + "OutputHessianFunction": outputHessianFunction.expand(),
        }

        return allOutputFunctions

    def _makeHeadPva(self) -> Dict[str, Function]:
        x = self.model.state.expr
        z = self.model.algState.expr
        u = self.model.input.expr
        p = self.model.param.expr

        # Transformation matrix from HF to PF
        T_HP = casadi.MX.sym("T_HP", 4, 4)

        # Output: y = f(x, z, u)
        head_pva = self.model.headPva(x=x, z=z, u=u, p=p, g=self.model.gravity, T_HP=T_HP)["y"]

        # Functions
        headPvaFunction = casadi.Function(
            self.model.camel_case_name + "HeadPva",
            [x, z, u, p, T_HP],
            [casadi.densify(head_pva)],
            ["x", "z", "u", "p", "T_HP"],
            ["head_pva"],
        )

        # Return value
        return {self.model.camel_case_name + "HeadPvaFunction": headPvaFunction.expand()}

    def _makeHeadInertial(self) -> Dict[str, Function]:
        x = self.model.state.expr
        z = self.model.algState.expr
        u = self.model.input.expr
        p = self.model.param.expr

        # Transformation matrix from HF to PF
        T_HP = casadi.MX.sym("T_HP", 4, 4)

        # Output: y = f(x, z, u)
        head_inertial = self.model.headInertial(x=x, z=z, u=u, p=p, g=self.model.gravity, T_HP=T_HP)["y"]

        # Functions
        headInertialFunction = casadi.Function(
            self.model.camel_case_name + "HeadInertial",
            [x, z, u, p, T_HP],
            [casadi.densify(head_inertial)],
            ["x", "z", "u", "p", "T_HP"],
            ["head_inertial"],
        )

        # Return value
        return {self.model.camel_case_name + "HeadInertialFunction": headInertialFunction.expand()}

    def _makeHeadPva(self) -> Dict[str, Function]:
        x = self.model.state.expr
        z = self.model.algState.expr
        u = self.model.input.expr
        p = self.model.param.expr

        # Transformation matrix from HF to PF
        T_HP = casadi.MX.sym("T_HP", 4, 4)

        # Output: y = f(x, z, u)
        head_pva = self.model.headPva(x=x, z=z, u=u, p=p, g=self.model.gravity, T_HP=T_HP)["y"]

        # Functions
        headPvaFunction = casadi.Function(
            self.model.camel_case_name + "HeadPva",
            [x, z, u, p, T_HP],
            [casadi.densify(head_pva)],
            ["x", "z", "u", "p", "T_HP"],
            ["head_pva"],
        )

        # Return value
        return {self.model.camel_case_name + "HeadPvaFunction": headPvaFunction.expand()}

    def _makeHeadInertial(self) -> Dict[str, Function]:
        x = self.model.state.expr
        z = self.model.algState.expr
        u = self.model.input.expr
        p = self.model.param.expr

        # Transformation matrix from HF to PF
        T_HP = casadi.MX.sym("T_HP", 4, 4)

        # Output: y = f(x, z, u)
        head_inertial = self.model.headInertial(x=x, z=z, u=u, p=p, g=self.model.gravity, T_HP=T_HP)["y"]

        # Functions
        headInertialFunction = casadi.Function(
            self.model.camel_case_name + "HeadInertial",
            [x, z, u, p, T_HP],
            [casadi.densify(head_inertial)],
            ["x", "z", "u", "p", "T_HP"],
            ["head_inertial"],
        )

        # Return value
        return {self.model.camel_case_name + "HeadInertialFunction": headInertialFunction.expand()}

    def _makeConstraints(self) -> Dict[str, Function]:
        x = self.model.state.expr
        z = self.model.algState.expr
        u = self.model.input.expr
        p = self.model.param.expr

        # Inequality constraints
        h = self.model.constraint.expr

        # Jacobian and Hessian
        dh_dx, dh_du, d2h_dx2, d2h_dx_du, d2h_du_dx, d2h_du2 = jacobianHessian_xu(h, x, u)

        # Functions
        inequalityFunction = casadi.Function(
            self.model.camel_case_name + "Inequality", [x, z, u, p], [casadi.densify(h)], ["x", "z", "u", "p"], ["h"]
        )

        inequalityJacobianFunction = casadi.Function(
            self.model.camel_case_name + "InequalityJacobian",
            [x, z, u, p],
            [casadi.densify(h), casadi.densify(dh_dx), casadi.densify(dh_du)],
            ["x", "z", "u", "p"],
            ["h", "dh_dx", "dh_du"],
        )
        if self.withHessian:
            inequalityHessianFunction = casadi.Function(
                self.model.camel_case_name + "InequalityHessian",
                [
                    x,
                    z,
                    u,
                    p,
                ],
                [casadi.densify(d2h_dx2), casadi.densify(d2h_dx_du), casadi.densify(d2h_du2)],
                ["x", "z", "u", "p"],
                ["d2h_dx2", "d2h_dx_du", "d2h_du2"],
            )
        else:
            empty = casadi.MX.sym("empty", 0)
            inequalityHessianFunction = casadi.Function(
                self.model.camel_case_name + "InequalityHessian",
                [x, z, u, p],
                [empty, empty, empty],
                ["x", "z", "u", "p"],
                ["d2h_dx2", "d2h_dx_du", "d2h_du2"],
            )

        allInequalityFunctions = {
            self.model.camel_case_name + "InequalityFunction": inequalityFunction.expand(),
            self.model.camel_case_name + "InequalityJacobianFunction": inequalityJacobianFunction.expand(),
            self.model.camel_case_name + "InequalityHessianFunction": inequalityHessianFunction.expand(),
        }
        return allInequalityFunctions

    def _makeCostFunction(self) -> Dict[str, Function]:
        x = self.model.state.expr
        z = self.model.algState.expr
        u = self.model.input.expr
        p = self.model.param.expr
        cq = self.model.constraint.expr

        # Lagrangian multipliers for path constraints
        mu_ub = casadi.MX.sym("mu_ub", cq.shape)
        mu_lb = casadi.MX.sym("mu_lb", cq.shape)

        # Transformation matrix from HF to PF
        T_HP = casadi.MX.sym("T_HP", 4, 4)

        # Output: y = f(x, z, u)
        y = self.model.output(x=x, z=z, u=u, p=p, g=self.model.gravity, T_HP=T_HP)["y"]

        # Weights
        wx = casadi.MX.sym("wx", x.shape)
        Wx = casadi.diag(wx**2)
        wu = casadi.MX.sym("wu", u.shape)
        Wu = casadi.diag(wu**2)
        wy = casadi.MX.sym("wy", y.shape)
        Wy = casadi.diag(wy**2)

        # Cost function
        x_ref = casadi.MX.sym("x_ref", x.shape)
        y_ref = casadi.MX.sym("y_ref", y.shape)

        J = (
            1.0
            / 2.0
            * (
                casadi.mtimes([casadi.transpose(u), Wu, u])
                + casadi.mtimes([casadi.transpose(x.cat - x_ref), Wx, x.cat - x_ref])
                + casadi.mtimes([casadi.transpose(y - y_ref), Wy, y - y_ref])
            )
        )

        # Lagrangian - contribution of path constraints
        # Lg = cq['cqInequality'][0]#-casadi.mtimes(mu_lb.T, cq) + casadi.mtimes(mu_ub.T, cq)
        Lg = -casadi.mtimes(mu_lb.T, cq) + casadi.mtimes(mu_ub.T, cq)

        # Jacobian and Full Hessian
        # NOTE: Using the hessian function instead of jacobian-jacobian generates a slightly faster C code
        #   dJ_dx, dJ_du, d2J_dx2, d2J_dx_du, d2J_du_dx, d2J_du2 = derivatives.jacobianHessian_xu(J, x.cat, u)
        NX = x.shape[0]
        [H, g] = casadi.hessian(J, casadi.vertcat(x.cat, u))
        dJ_dx = g[:NX].T
        dJ_du = g[NX:].T
        d2J_dx2 = H[:NX, :NX]
        d2J_dx_du = H[:NX, NX:]
        d2J_du2 = H[NX:, NX:]

        [Hg, gg] = casadi.hessian(Lg, casadi.vertcat(x.cat, u))
        d2Lg_dx2 = Hg[:NX, :NX]
        d2Lg_dx_du = Hg[:NX, NX:]
        d2Lg_du2 = Hg[NX:, NX:]

        # Gauss-Newton Hessian approximation
        r = y - y_ref
        dr_dxT = casadi.jacobian(r, x.cat)
        dr_duT = casadi.jacobian(r, u)
        d2J_dx2GaussNewton = casadi.mtimes([dr_dxT.T, Wy, dr_dxT]) + Wx
        d2J_dx_duTGaussNewton = casadi.mtimes([dr_dxT.T, Wy, dr_duT])
        d2J_du2GaussNewton = casadi.mtimes([dr_duT.T, Wy, dr_duT]) + Wu

        # Functions
        costFunction = casadi.Function(
            self.model.camel_case_name + "Cost",
            [x, z, u, p, T_HP, x_ref, y_ref, wx, wu, wy],
            [casadi.densify(J)],
            ["x", "z", "u", "p", "T_HP", "x_ref", "y_ref", "wx", "wu", "wy"],
            ["J"],
        )

        costJacobianFunction = casadi.Function(
            self.model.camel_case_name + "CostJacobian",
            [x, z, u, p, T_HP, x_ref, y_ref, wx, wu, wy],
            [casadi.densify(dJ_dx), casadi.densify(dJ_du)],
            ["x", "z", "u", "p", "T_HP", "x_ref", "y_ref", "wx", "wu", "wy"],
            ["dJ_dx", "dJ_du"],
        )
        lagrangeHessianGaussNewtonFunction = casadi.Function(
            self.model.camel_case_name + "LagrangeHessianGaussNewton",
            [x, z, u, p, T_HP, x_ref, y_ref, wx, wu, wy],
            [
                casadi.densify(dJ_dx),
                casadi.densify(dJ_du),
                casadi.densify(d2J_dx2GaussNewton),
                casadi.densify(d2J_dx_duTGaussNewton),
                casadi.densify(d2J_du2GaussNewton),
            ],
            ["x", "z", "u", "p", "T_HP", "x_ref", "y_ref", "wx", "wu", "wy"],
            ["dJ_dx", "dJ_du", "d2L_dx2", "d2L_dx_du", "d2L_du2"],
        )

        if self.withHessian:
            costHessianFunction = casadi.Function(
                self.model.camel_case_name + "CostHessian",
                [x, z, u, p, T_HP, x_ref, y_ref, wx, wu, wy],
                [
                    casadi.densify(dJ_dx),
                    casadi.densify(dJ_du),
                    casadi.densify(d2J_dx2),
                    casadi.densify(d2J_dx_du),
                    casadi.densify(d2J_du2),
                ],
                ["x", "z", "u", "p", "T_HP", "x_ref", "y_ref", "wx", "wu", "wy"],
                ["dJ_dx", "dJ_du", "d2J_dx2", "d2J_dx_du", "d2J_du2"],
            )
            lagrangeHessianFunction = casadi.Function(
                self.model.camel_case_name + "LagrangeHessian",
                [x, z, u, p, mu_lb, mu_ub],
                [casadi.densify(d2Lg_dx2), casadi.densify(d2Lg_dx_du), casadi.densify(d2Lg_du2)],
                ["x", "z", "u", "p", "mu_lb", "mu_ub"],
                ["d2Lg_dx2", "d2Lg_dx_du", "d2Lg_du2"],
            )
        else:
            empty = casadi.MX.sym("empty", 0)
            costHessianFunction = casadi.Function(
                self.model.camel_case_name + "CostHessian",
                [x, z, u, p, T_HP, x_ref, y_ref, wx, wu, wy],
                [casadi.densify(dJ_dx), casadi.densify(dJ_du), empty, empty, empty],
                ["x", "z", "u", "p", "T_HP", "x_ref", "y_ref", "wx", "wu", "wy"],
                ["dJ_dx", "dJ_du", "d2J_dx2", "d2J_dx_du", "d2J_du2"],
            )
            lagrangeHessianFunction = casadi.Function(
                self.model.camel_case_name + "LagrangeHessian",
                [x, z, u, p, mu_lb, mu_ub],
                [empty, empty, empty],
                ["x", "z", "u", "p", "mu_lb", "mu_ub"],
                ["d2Lg_dx2", "d2Lg_dx_du", "d2Lg_du2"],
            )

        allCostFunctions = {
            self.model.camel_case_name + "CostFunction": costFunction.expand(),
            self.model.camel_case_name + "CostJacobianFunction": costJacobianFunction.expand(),
            self.model.camel_case_name + "CostHessianFunction": costHessianFunction.expand(),
            self.model.camel_case_name + "LagrangeHessianGaussNewtonFunction": lagrangeHessianGaussNewtonFunction.expand(),
            self.model.camel_case_name + "LagrangeHessianFunction": lagrangeHessianFunction.expand(),
        }
        return allCostFunctions

    def _makeSimulatorData(self) -> Dict[str, Function]:
        # Default values
        lbx = self.model.state.lb.cat.full()
        ubx = self.model.state.ub.cat.full()
        x0 = self.model.state.nominal.cat.full()
        lbu = self.model.input.lb.full()
        ubu = self.model.input.ub.full()
        u0 = self.model.input.nominal.full()
        lbg = self.model.constraint.lb.cat.full()
        ubg = self.model.constraint.ub.cat.full()
        T = self.model.headToPlatform
        g = self.model.gravity
        z0 = numpy.zeros(self.model.algState.expr.shape)
        p0 = numpy.zeros(self.model.param.expr.shape)
        y0 = self.model.output(x=x0, z=z0, u=u0, p=p0, g=g, T_HP=T)["y"].full()

        # Default function
        simulator_dataFunction = casadi.Function(
            self.model.camel_case_name + "SimulatorData",
            [],
            [lbx, ubx, x0, lbu, ubu, u0, lbg, ubg, T, g, y0],
            [],
            ["lbx", "ubx", "x0", "lbu", "ubu", "u0", "lbg", "ubg", "T", "g", "y0"],
        )

        simulator_dataFunctions = {self.model.camel_case_name + "SimulatorDataFunction": simulator_dataFunction.expand()}

        return simulator_dataFunctions

    def _makeInfo(self) -> Dict[str, Function]:
        # Model info
        info = self.model.info

        # Function that returns the info vector
        infoFunction = casadi.Function(self.model.camel_case_name + "Info", [], [info], [], ["info"])

        # Create dictionary
        infoFunctions = {self.model.camel_case_name + "InfoFunction": infoFunction.expand()}

        return infoFunctions

    def _makeName(self) -> Dict[str, Function]:
        # Function that returns a dummy output whose name corresponds to the model name
        nameFunction = casadi.Function(self.model.camel_case_name + "Name", [], [0], [], [self.model.camel_case_name])

        # Create dictionary
        nameFunctions = {self.model.camel_case_name + "NameFunction": nameFunction.expand()}

        return nameFunctions

    def _makeStoppabilityConstraints(self) -> Dict[str, Function]:
        # Expressions for stoppability constraints
        x = self.model.state.expr
        u = self.model.input.expr
        Nq = self.model.input.expr.shape
        q_min = casadi.MX.sym("q_min", Nq)
        q_max = casadi.MX.sym("q_max", Nq)
        u_min = casadi.MX.sym("u_min", Nq)
        u_max = casadi.MX.sym("u_max", Nq)
        #
        cg1 = x["v"] ** 2 + 2 * (q_max - x["q"]) * u_min
        cg2 = x["v"] ** 2 + 2 * (q_min - x["q"]) * u_max
        cg = casadi.vertcat(cg1, cg2)
        #
        cg1Original = -x["v"] ** 2 / (2.0 * u_min) + (x["q"] - q_max)
        cg2Original = x["v"] ** 2 / (2.0 * u_max) + (q_min - x["q"])
        cgOriginal = casadi.vertcat(cg1Original, cg2Original)

        # Jacobian and Hessian
        dcg_dx, dcg_du, d2cg_dx2, d2cg_dx_du, d2cg_du_dx, d2cg_du2 = jacobianHessian_xu(cg, x, u)
        dcg_dxOriginal, dcg_duOriginal, d2cg_dx2Original, d2cg_dx_duOriginal, d2cg_du_dxOriginal, d2cg_du2Original = jacobianHessian_xu(
            cgOriginal, x, u
        )

        # Lagrangian multipliers for path constraints
        mu_ub = casadi.MX.sym("mu_ub", cg.shape)
        mu_lb = casadi.MX.sym("mu_lb", cg.shape)

        # Lagrangian cost term
        Lg = -casadi.mtimes(mu_lb.T, cg) + casadi.mtimes(mu_ub.T, cg)
        LgOriginal = -casadi.mtimes(mu_lb.T, cgOriginal) + casadi.mtimes(mu_ub.T, cgOriginal)

        # Hessian of the lagrangian
        NX = x.shape[0]
        [Hg, gg] = casadi.hessian(Lg, casadi.vertcat(x.cat, u))
        d2L_dx2 = Hg[:NX, :NX]
        d2L_dx_du = Hg[:NX, NX:]
        d2L_du2 = Hg[NX:, NX:]
        [HgOriginal, ggOriginal] = casadi.hessian(LgOriginal, casadi.vertcat(x.cat, u))
        d2L_dx2Original = HgOriginal[:NX, :NX]
        d2L_dx_duOriginal = HgOriginal[:NX, NX:]
        d2L_du2Original = HgOriginal[NX:, NX:]

        # Create functions
        cgStopFunction = casadi.Function(
            self.model.camel_case_name + "StoppabilityConstraints",
            [x, q_min, q_max, u_min, u_max],
            [casadi.densify(cg), casadi.densify(dcg_dx), casadi.densify(dcg_du)],
            ["x", "q_min", "q_max", "u_min", "u_max"],
            ["cg", "dcg_dx", "dcg_du"],
        )

        cgStopFunctionOriginal = casadi.Function(
            self.model.camel_case_name + "StoppabilityConstraintsOriginal",
            [x, q_min, q_max, u_min, u_max],
            [casadi.densify(cgOriginal), casadi.densify(dcg_dxOriginal), casadi.densify(dcg_duOriginal)],
            ["x", "q_min", "q_max", "u_min", "u_max"],
            ["cg", "dcg_dx", "dcg_du"],
        )

        cgStopHessianFunction = casadi.Function(
            self.model.camel_case_name + "StoppabilityConstraintsHessian",
            [x, q_min, q_max, u_min, u_max, mu_lb, mu_ub],
            [casadi.densify(d2L_dx2), casadi.densify(d2L_dx_du), casadi.densify(d2L_du2)],
            ["x", "q_min", "q_max", "u_min", "u_max", "mu_lb", "mu_ub"],
            ["d2L_dx2", "d2L_dx_du", "d2L_du2"],
        )

        cgStopHessianFunctionOriginal = casadi.Function(
            self.model.camel_case_name + "StoppabilityConstraintsHessianOriginal",
            [x, q_min, q_max, u_min, u_max, mu_lb, mu_ub],
            [casadi.densify(d2L_dx2Original), casadi.densify(d2L_dx_duOriginal), casadi.densify(d2L_du2Original)],
            ["x", "q_min", "q_max", "u_min", "u_max", "mu_lb", "mu_ub"],
            ["d2L_dx2", "d2L_dx_du", "d2L_du2"],
        )

        # Crate dictionary
        cgStop_dict = {
            self.model.camel_case_name + "StoppabilityConstraintsFunction": cgStopFunction.expand(),
            self.model.camel_case_name + "StoppabilityConstraintsHessianFunction": cgStopHessianFunction.expand(),
            self.model.camel_case_name + "StoppabilityConstraintsFunctionOriginal": cgStopFunctionOriginal.expand(),
            self.model.camel_case_name + "StoppabilityConstraintsHessianFunctionOriginal": cgStopHessianFunctionOriginal.expand(),
        }

        return cgStop_dict

    def _makeFunctions(self) -> Dict[str, Function]:
        fun = self._makeOutput()
        fun.update(self._makeConstraints())
        fun.update(self._makeCostFunction())
        fun.update(self._makeSimulatorData())
        fun.update(self._makeInfo())
        fun.update(self._makeName())
        fun.update(self._makeStoppabilityConstraints())
        fun.update(self._makeHeadPva())
        fun.update(self._makeHeadInertial())
        return fun

    def GenerateCode(self) -> None:
        functions = self._makeFunctions()

        header_fname = self.header_path
        source_fname = self.source_path

        name_c = "model_generated.c"
        name_h = "model_generated.h"
        gen = casadi.CodeGenerator(name_c, {"mex": False, "with_header": True, "with_mem": True})
        for f in functions.values():
            gen.add(f)
        gen.generate()

        move_file(name_h, header_fname)
        move_file(name_c, source_fname)

        print("C code successfully generated.")
        print("Header is written to {0}.".format(header_fname))
        print("Source is written to {0}.".format(source_fname))

    def GenerateTestData(self) -> None:
        functions = self._makeFunctions()
        file_name = "data.txt"

        # Initialize random data
        numpy.random.seed(0)
        x_ = numpy.random.rand(*self.model.state.expr.shape)
        z_ = numpy.random.rand(*self.model.algState.expr.shape)
        u_ = numpy.random.rand(*self.model.input.expr.shape)
        p_ = numpy.random.rand(*self.model.param.expr.shape)
        T_HP_ = numpy.array([[1.0, 0.0, 0.0, 0.0], [0.0, -1.0, 0.0, 0.0], [0.0, 0.0, -1.0, -0.87], [0.0, 0.0, 0.0, 1.0]])
        x_ref_ = numpy.random.rand(*self.model.state.expr.shape)
        y_ref_ = numpy.random.rand(self.model.getOutputSize())
        wx_ = numpy.random.rand(*self.model.state.expr.shape)
        wu_ = numpy.random.rand(*self.model.input.expr.shape)
        wy_ = numpy.random.rand(self.model.getOutputSize())
        mu_lb_ = numpy.random.rand(*self.model.constraint.expr.shape)
        mu_ub_ = numpy.random.rand(*self.model.constraint.expr.shape)

        # Calculate function values
        output_ = functions[self.model.camel_case_name + "OutputJacobianFunction"](x=x_, z=z_, u=u_, p=p_, T_HP=T_HP_)
        head_pva_ = functions[self.model.camel_case_name + "HeadPvaFunction"](x=x_, z=z_, u=u_, p=p_, T_HP=T_HP_)
        head_inertial_ = functions[self.model.camel_case_name + "HeadInertialFunction"](x=x_, z=z_, u=u_, p=p_, T_HP=T_HP_)
        inequality_ = functions[self.model.camel_case_name + "InequalityJacobianFunction"](x=x_, z=z_, u=u_, p=p_)
        cost_ = functions[self.model.camel_case_name + "CostFunction"](
            x=x_, z=z_, u=u_, p=p_, T_HP=T_HP_, x_ref=x_ref_, y_ref=y_ref_, wx=wx_, wu=wu_, wy=wy_
        )
        costJacobian_ = functions[self.model.camel_case_name + "CostJacobianFunction"](
            x=x_, z=z_, u=u_, p=p_, T_HP=T_HP_, x_ref=x_ref_, y_ref=y_ref_, wx=wx_, wu=wu_, wy=wy_
        )
        outputHessian_ = functions[self.model.camel_case_name + "OutputHessianFunction"](x=x_, z=z_, u=u_, p=p_, T_HP=T_HP_)
        inequalityHessian_ = functions[self.model.camel_case_name + "InequalityHessianFunction"](x=x_, z=z_, u=u_, p=p_)
        costHessian_ = functions[self.model.camel_case_name + "CostHessianFunction"](
            x=x_, z=z_, u=u_, p=p_, T_HP=T_HP_, x_ref=x_ref_, y_ref=y_ref_, wx=wx_, wu=wu_, wy=wy_
        )
        lagrangeHessianGaussNewton_ = functions[self.model.camel_case_name + "LagrangeHessianGaussNewtonFunction"](
            x=x_, z=z_, u=u_, p=p_, T_HP=T_HP_, x_ref=x_ref_, y_ref=y_ref_, wx=wx_, wu=wu_, wy=wy_
        )
        lagrangeHessian_ = functions[self.model.camel_case_name + "LagrangeHessianFunction"](x=x_, z=z_, u=u_, p=p_, mu_lb=mu_lb_, mu_ub=mu_ub_)
        info_ = functions[self.model.camel_case_name + "InfoFunction"]()
        name_ = functions[self.model.camel_case_name + "NameFunction"].name_out(0)

        # Save data in a format for json
        test_data = {}
        test_data["x"] = x_
        test_data["z"] = z_
        test_data["u"] = u_
        test_data["p"] = p_
        test_data["T_HP"] = T_HP_
        test_data["x_ref"] = x_ref_
        test_data["y_ref"] = y_ref_
        test_data["wx"] = wx_
        test_data["wu"] = wu_
        test_data["wy"] = wy_
        test_data["lam_lbg"] = mu_lb_
        test_data["lam_ubg"] = mu_ub_
        test_data["y"] = output_["y"].full()
        test_data["dy_dx"] = output_["dy_dx"].full()
        test_data["dy_du"] = output_["dy_du"].full()
        test_data["head_pva"] = head_pva_["head_pva"].full()
        test_data["head_inertial"] = head_inertial_["head_inertial"].full()
        test_data["g"] = inequality_["h"].full()
        test_data["dg_dx"] = inequality_["dh_dx"].full()
        test_data["dg_du"] = inequality_["dh_du"].full()
        test_data["J"] = cost_["J"].full()
        test_data["dJ_dx"] = costJacobian_["dJ_dx"].T.full()
        test_data["dJ_du"] = costJacobian_["dJ_du"].T.full()
        test_data["d2L_dx2_gauss_newton"] = lagrangeHessianGaussNewton_["d2L_dx2"].full()
        test_data["d2L_dx_du_gauss_newton"] = lagrangeHessianGaussNewton_["d2L_dx_du"].full()
        test_data["d2L_du2_gauss_newton"] = lagrangeHessianGaussNewton_["d2L_du2"].full()
        test_data["d2y_dx2"] = outputHessian_["d2y_dx2"].full()
        test_data["d2y_dx_du"] = outputHessian_["d2y_dx_du"].full()
        test_data["d2y_du2"] = outputHessian_["d2y_du2"].full()
        test_data["d2g_dx2"] = inequalityHessian_["d2h_dx2"].full()
        test_data["d2g_dx_du"] = inequalityHessian_["d2h_dx_du"].full()
        test_data["d2g_du2"] = inequalityHessian_["d2h_du2"].full()
        test_data["d2J_dx2"] = costHessian_["d2J_dx2"].full()
        test_data["d2J_dx_du"] = costHessian_["d2J_dx_du"].full()
        test_data["d2J_du2"] = costHessian_["d2J_du2"].full()
        test_data["d2Lg_dx2"] = lagrangeHessian_["d2Lg_dx2"].full()
        test_data["d2Lg_dx_du"] = lagrangeHessian_["d2Lg_dx_du"].full()
        test_data["d2Lg_du2"] = lagrangeHessian_["d2Lg_du2"].full()
        test_data["info"] = info_["info"].full()
        test_data["name"] = name_

        # Save
        with open(file_name, "w") as outfile:
            json.dump(test_data, outfile, cls=NumpyEncoder, indent=4, allow_nan=True)

        # Change name and move file
        move_file(file_name, self.test_data_path)
        print("Test data successfully generated in {0}.".format(self.test_data_path))

    def printCasadiFunctionsLine(self, outfile, function_name):
        outfile.write(
            "    static casadi_functions* " + function_name + "(void) { return " + self.model.camel_case_name + function_name + "(); }\n"
        )

    def printSimulatorStruct(self, outfile):
        component_tokens = str(self.model).split(";")
        component_list = ",".join(["components::" + token_k for token_k in component_tokens])

        outfile.write("struct " + self.model.camel_case_name + "\n{\n")

        outfile.write("    using ComponentList = templates::ComponentList<" + component_list + ">;\n")
        outfile.write("    static constexpr std::size_t NY_SIMULATOR = " + str(self.model.getOutputSize()) + ";\n")
        outfile.write("    using Dimension = templates::DimensionsT<" + self.model.camel_case_name + ">;\n")
        outfile.write("    using AxesVector = Vector<Dimension::NQ>;\n")
        outfile.write("    using StateVector = Vector<Dimension::NX>;\n")
        outfile.write("    using AlgStateVector = Vector<Dimension::NZ>;\n")
        outfile.write("    using InputVector = Vector<Dimension::NU>;\n")
        outfile.write("    using ConstraintVector = Vector<Dimension::NC>;\n")
        outfile.write("    using ParameterVector = Vector<Dimension::NP>;\n")
        outfile.write("    using OutputVector = Vector<Dimension::NY>;\n")
        outfile.write("    using InfoVector = Vector<Dimension::NI>;\n")
        outfile.write("    using StateStateMatrix = Matrix<Dimension::NX, Dimension::NX>;\n")
        outfile.write("    using StateInputMatrix = Matrix<Dimension::NX, Dimension::NU>;\n")
        outfile.write("    using InputInputMatrix = Matrix<Dimension::NU, Dimension::NU>;\n")
        outfile.write("    using OutputOutputMatrix = Matrix<Dimension::NY, Dimension::NY>;\n")
        outfile.write("    using OutputStateMatrix = Matrix<Dimension::NY, Dimension::NX>;\n")
        outfile.write("    using OutputInputMatrix = Matrix<Dimension::NY, Dimension::NU>;\n")
        outfile.write("    using ConstraintStateMatrix = Matrix<Dimension::NC, Dimension::NX>;\n")
        outfile.write("    using ConstraintInputMatrix = Matrix<Dimension::NC, Dimension::NU>;\n")
        outfile.write("    using OutputStateStateMatrix = Matrix<Dimension::NX * Dimension::NY, Dimension::NX>;\n")
        outfile.write("    using OutputStateInputMatrix = Matrix<Dimension::NX * Dimension::NY, Dimension::NU>;\n")
        outfile.write("    using OutputInputInputMatrix = Matrix<Dimension::NU * Dimension::NY, Dimension::NU>;\n")
        outfile.write("    using ConstraintStateStateMatrix = Matrix<Dimension::NX * Dimension::NC, Dimension::NX>;\n")
        outfile.write("    using ConstraintStateInputMatrix = Matrix<Dimension::NX * Dimension::NC, Dimension::NU>;\n")
        outfile.write("    using ConstraintInputInputMatrix = Matrix<Dimension::NU * Dimension::NC, Dimension::NU>;\n")
        outfile.write("    using InputMatrix = Eigen::Matrix<double, Dimension::NU, Eigen::Dynamic>;\n")
        outfile.write("    using StateMatrix = Eigen::Matrix<double, Dimension::NX, Eigen::Dynamic>;\n")
        outfile.write("    using OutputMatrix = Eigen::Matrix<double, Dimension::NY, Eigen::Dynamic>;\n")

        self.printCasadiFunctionsLine(outfile, "Output_functions")
        self.printCasadiFunctionsLine(outfile, "OutputJacobian_functions")
        self.printCasadiFunctionsLine(outfile, "OutputHessian_functions")
        self.printCasadiFunctionsLine(outfile, "Inequality_functions")
        self.printCasadiFunctionsLine(outfile, "InequalityJacobian_functions")
        self.printCasadiFunctionsLine(outfile, "InequalityHessian_functions")
        self.printCasadiFunctionsLine(outfile, "Cost_functions")
        self.printCasadiFunctionsLine(outfile, "CostJacobian_functions")
        self.printCasadiFunctionsLine(outfile, "CostHessian_functions")
        self.printCasadiFunctionsLine(outfile, "LagrangeHessianGaussNewton_functions")
        self.printCasadiFunctionsLine(outfile, "LagrangeHessian_functions")
        self.printCasadiFunctionsLine(outfile, "SimulatorData_functions")
        self.printCasadiFunctionsLine(outfile, "HeadPva_functions")
        self.printCasadiFunctionsLine(outfile, "HeadInertial_functions")
        self.printCasadiFunctionsLine(outfile, "Info_functions")
        self.printCasadiFunctionsLine(outfile, "Name_functions")
        self.printCasadiFunctionsLine(outfile, "StoppabilityConstraints_functions")
        self.printCasadiFunctionsLine(outfile, "StoppabilityConstraintsHessian_functions")
        self.printCasadiFunctionsLine(outfile, "StoppabilityConstraintsOriginal_functions")
        self.printCasadiFunctionsLine(outfile, "StoppabilityConstraintsHessianOriginal_functions")

        outfile.write("    static const char* name_name_out(casadi_int i) { return " + self.model.camel_case_name + "Name_name_out(i); }\n")
        outfile.write('    static constexpr const char* GetName() { return "' + self.model.camel_case_name + '"; }\n')
        outfile.write('    static constexpr const char* GetSnakeCaseName() { return "' + self.model.snake_case_name + '"; }\n')

        outfile.write("};\n")

    def GenerateHeaders(self) -> None:

        generic_test_temporary_path = "generic_test_header.hpp"
        generic_test_destination_path = os.path.join(self.define_data_path, generic_test_temporary_path)

        with open(generic_test_temporary_path, "w") as outfile:
            outfile.write("#pragma once\n")
            outfile.write('#include "' + self.model.snake_case_name + "/" + self.model.snake_case_name + '.hpp"\n')
            outfile.write("namespace mpmca::control::testing {\n")
            outfile.write("\tusing TestSimulatorType = mpmca::control::" + self.model.camel_case_name + ";\n")
            outfile.write("} // namespace mpmca::control::testing\n")

        move_file(generic_test_temporary_path, generic_test_destination_path)

        specific_temporary_path = self.model.snake_case_name + ".hpp"
        specific_destination_path = os.path.join(self.define_data_path, self.model.snake_case_name + "/" + specific_temporary_path)

        with open(specific_temporary_path, "w") as outfile:
            outfile.write("#pragma once\n")
            outfile.write('#include "' + self.model.snake_case_name + '_generated.h"\n')
            outfile.write('#include "mpmca/control/components.hpp"\n')
            outfile.write('#include "mpmca/control/templates/component.hpp"\n')
            outfile.write('#include "mpmca/control/templates/component_list.hpp"\n')
            outfile.write('#include "mpmca/control/templates/dimension.hpp"\n')

            outfile.write("\nnamespace mpmca::control {\n")
            self.printSimulatorStruct(outfile)
            outfile.write("} // namespace mpmca::control\n")

        move_file(specific_temporary_path, specific_destination_path)
