# Copyright Frank Drop and Mario Olivari ("Predictive cueing").
# Please read carefully the terms and conditions (root/LICENCE.md) and
# any accompanying documentation before you download and/or use
# the Model Predictive Motion Cueing Algorithm (MPMCA) software.
# cSpell:disable
import math
from typing import Tuple, Union, overload

import numpy

import casadi  # type: ignore
import omsf
from casadi.casadi import MX  # type: ignore
from omsf import transform


def mul2_dot(
    T1: Union[MX, omsf.T_NDArray], T2: Union[MX, omsf.T_NDArray], Tdot1: Union[MX, omsf.T_NDArray], Tdot2: Union[MX, omsf.T_NDArray]
) -> Union[MX, omsf.T_NDArray]:
    return transform.mul(Tdot1, T2) + transform.mul(T1, Tdot2)  # type: ignore


def mul3_dot(
    T1: Union[MX, omsf.T_NDArray],
    T2: Union[MX, omsf.T_NDArray],
    T3: Union[MX, omsf.T_NDArray],
    Tdot1: Union[MX, omsf.T_NDArray],
    Tdot2: Union[MX, omsf.T_NDArray],
    Tdot3: Union[MX, omsf.T_NDArray],
) -> Union[MX, omsf.T_NDArray]:
    res = (  # type: ignore
        transform.mul(transform.mul(Tdot1, T2), T3)
        + transform.mul(transform.mul(T1, Tdot2), T3)
        + transform.mul(transform.mul(T1, T2), Tdot3)
    )
    return res  # type: ignore


def mul4_dot(T1: MX, T2: MX, T3: MX, T4: MX, Tdot1: MX, Tdot2: MX, Tdot3: MX, Tdot4: MX) -> MX:
    res = (  # type:ignore
        transform.mul(transform.mul(transform.mul(Tdot1, T2), T3), T4)
        + transform.mul(transform.mul(transform.mul(T1, Tdot2), T3), T4)
        + transform.mul(transform.mul(transform.mul(T1, T2), Tdot3), T4)
        + transform.mul(transform.mul(transform.mul(T1, T2), T3), Tdot4)
    )
    assert isinstance(res, MX)
    return res


def mul2_ddot(T1: MX, T2: MX, Tdot1: MX, Tdot2: MX, Tddot1: MX, Tddot2: MX) -> MX:
    res = transform.mul(Tddot1, T2) + 2 * transform.mul(Tdot1, Tdot2) + transform.mul(T1, Tddot2)  # type:ignore
    assert isinstance(res, MX)
    return res


def mul3_ddot(T1: MX, T2: MX, T3: MX, Tdot1: MX, Tdot2: MX, Tdot3: MX, Tddot1: MX, Tddot2: MX, Tddot3: MX) -> MX:
    res = (  # type:ignore
        transform.mul(transform.mul(Tddot1, T2), T3)
        + transform.mul(transform.mul(Tdot1, Tdot2), T3)
        + transform.mul(transform.mul(Tdot1, T2), Tdot3)
        + transform.mul(transform.mul(Tdot1, Tdot2), T3)
        + transform.mul(transform.mul(T1, Tddot2), T3)
        + transform.mul(transform.mul(T1, Tdot2), Tdot3)
        + transform.mul(transform.mul(Tdot1, T2), Tdot3)
        + transform.mul(transform.mul(T1, Tdot2), Tdot3)
        + transform.mul(transform.mul(T1, T2), Tddot3)
    )
    assert isinstance(res, MX)
    return res


def mul4_ddot(
    T1: MX, T2: MX, T3: MX, T4: MX, Tdot1: MX, Tdot2: MX, Tdot3: MX, Tdot4: MX, Tddot1: MX, Tddot2: MX, Tddot3: MX, Tddot4: MX
) -> MX:
    res = (  # type: ignore
        transform.mul(transform.mul(transform.mul(Tddot1, T2), T3), T4)
        + transform.mul(transform.mul(transform.mul(Tdot1, Tdot2), T3), T4)
        + transform.mul(transform.mul(transform.mul(Tdot1, T2), Tdot3), T4)
        + transform.mul(transform.mul(transform.mul(Tdot1, T2), T3), Tdot4)
        + transform.mul(transform.mul(transform.mul(Tdot1, Tdot2), T3), T4)
        + transform.mul(transform.mul(transform.mul(T1, Tddot2), T3), T4)
        + transform.mul(transform.mul(transform.mul(T1, Tdot2), Tdot3), T4)
        + transform.mul(transform.mul(transform.mul(T1, Tdot2), T3), Tdot4)
        + transform.mul(transform.mul(transform.mul(Tdot1, T2), Tdot3), T4)
        + transform.mul(transform.mul(transform.mul(T1, Tdot2), Tdot3), T4)
        + transform.mul(transform.mul(transform.mul(T1, T2), Tddot3), T4)
        + transform.mul(transform.mul(transform.mul(T1, T2), Tdot3), Tdot4)
        + transform.mul(transform.mul(transform.mul(Tdot1, T2), T3), Tdot4)
        + transform.mul(transform.mul(transform.mul(T1, Tdot2), T3), Tdot4)
        + transform.mul(transform.mul(transform.mul(T1, T2), Tdot3), Tdot4)
        + transform.mul(transform.mul(transform.mul(T1, T2), T3), Tddot4)
    )
    assert isinstance(res, MX)
    return res


@overload
def sum1(X: casadi.SX) -> casadi.SX: ...


@overload
def sum1(X: casadi.MX) -> casadi.MX: ...


@overload
def sum1(X: omsf.T_NDArray) -> omsf.T_NDArray: ...


def sum1(X: Union[casadi.SX, casadi.MX, omsf.T_NDArray]) -> Union[casadi.SX, casadi.MX, omsf.T_NDArray]:
    dtype: type = type(X)
    if dtype == casadi.SX or dtype == casadi.MX:
        return casadi.sum1(X)  # type:ignore
    else:
        return numpy.sum(X, axis=0)  # type:ignore


@overload
def TranslationXY(xy: omsf.T_NDArray) -> omsf.T_NDArray: ...


@overload
def TranslationXY(xy: MX) -> MX: ...


def TranslationXY(xy: Union[omsf.T_NDArray, MX]) -> Union[omsf.T_NDArray, MX]:
    transformation_matrix = transform.eye(xy)
    transformation_matrix[:2, 3] = xy
    return transformation_matrix


@overload
def Translation_dot(xyz_dot: MX) -> MX: ...


@overload
def Translation_dot(xyz_dot: omsf.T_NDArray) -> omsf.T_NDArray: ...


def Translation_dot(xyz_dot: Union[MX, omsf.T_NDArray]) -> Union[MX, omsf.T_NDArray]:
    T_dot = _Zeros(xyz_dot)
    T_dot[:3, 3] = xyz_dot
    return T_dot


@overload
def TranslationX_dot(x_dot: MX) -> MX: ...


@overload
def TranslationX_dot(x_dot: omsf.T_NDArray) -> omsf.T_NDArray: ...


def TranslationX_dot(x_dot: Union[MX, omsf.T_NDArray]) -> Union[MX, omsf.T_NDArray]:
    Tdot = _Zeros(x_dot)
    Tdot[0, 3] = x_dot
    return Tdot


@overload
def TranslationY_dot(y_dot: MX) -> MX: ...


@overload
def TranslationY_dot(y_dot: omsf.T_NDArray) -> omsf.T_NDArray: ...


def TranslationY_dot(y_dot: Union[MX, omsf.T_NDArray]) -> Union[MX, omsf.T_NDArray]:
    Tdot = _Zeros(y_dot)
    Tdot[1, 3] = y_dot
    return Tdot


@overload
def TranslationZ_dot(z_dot: MX) -> MX: ...


@overload
def TranslationZ_dot(z_dot: omsf.T_NDArray) -> omsf.T_NDArray: ...


def TranslationZ_dot(z_dot: Union[MX, omsf.T_NDArray]) -> Union[MX, omsf.T_NDArray]:
    Tdot = _Zeros(z_dot)
    Tdot[2, 3] = z_dot
    return Tdot


@overload
def RotationX_dot(theta: float, theta_dot: float) -> omsf.T_NDArray: ...


@overload
def RotationX_dot(theta: MX, theta_dot: MX) -> MX: ...


def RotationX_dot(theta: Union[float, MX], theta_dot: Union[float, MX]) -> Union[omsf.T_NDArray, MX]:
    Tdot = _Zeros(theta)
    Tdot[1, 1] = Tdot[2, 2] = -transform.mul(transform.sin(theta), theta_dot)
    Tdot[2, 1] = transform.mul(transform.cos(theta), theta_dot)
    Tdot[1, 2] = -Tdot[2, 1]
    return Tdot


def RotationY_dot(theta: MX, theta_dot: MX) -> MX:
    Tdot = _Zeros(theta)
    Tdot[0, 0] = Tdot[2, 2] = transform.mul(-transform.sin(theta), theta_dot)  # type: ignore
    Tdot[0, 2] = transform.mul(transform.cos(theta), theta_dot)
    Tdot[2, 0] = -Tdot[0, 2]
    return Tdot


def RotationZ_dot(theta: MX, theta_dot: MX) -> MX:
    Tdot = _Zeros(theta)
    Tdot[0, 0] = Tdot[1, 1] = -transform.mul(transform.sin(theta), theta_dot)
    Tdot[1, 0] = transform.mul(transform.cos(theta), theta_dot)
    Tdot[0, 1] = -Tdot[1, 0]
    return Tdot


def Translation_ddot(xyz_ddot: MX) -> MX:
    T_ddot = _Zeros(xyz_ddot)
    T_ddot[:3, 3] = xyz_ddot
    return T_ddot


def TranslationX_ddot(x_ddot: MX) -> MX:
    Tddot = _Zeros(x_ddot)
    Tddot[0, 3] = x_ddot
    return Tddot


def TranslationY_ddot(y_ddot: MX) -> MX:
    Tddot = _Zeros(y_ddot)
    Tddot[1, 3] = y_ddot
    return Tddot


def TranslationZ_ddot(z_ddot: MX) -> MX:
    Tddot = _Zeros(z_ddot)
    Tddot[2, 3] = z_ddot
    return Tddot


def RotationX_ddot(theta: MX, theta_dot: MX, theta_ddot: MX) -> MX:
    Tddot = _Zeros(theta)
    Tddot[1, 1] = Tddot[2, 2] = transform.mul(-transform.cos(theta), theta_dot**2) + transform.mul(-transform.sin(theta), theta_ddot)  # type: ignore
    Tddot[2, 1] = transform.mul(-transform.sin(theta), theta_dot**2) + transform.mul(transform.cos(theta), theta_ddot)  # type: ignore
    Tddot[1, 2] = -Tddot[2, 1]
    return Tddot


def RotationY_ddot(theta: MX, theta_dot: MX, theta_ddot: MX) -> MX:
    Tddot = _Zeros(theta)
    Tddot[0, 0] = Tddot[2, 2] = transform.mul(-transform.cos(theta), theta_dot**2) + transform.mul(-transform.sin(theta), theta_ddot)  # type: ignore
    Tddot[0, 2] = transform.mul(-transform.sin(theta), theta_dot**2) + transform.mul(transform.cos(theta), theta_ddot)  # type: ignore
    Tddot[2, 0] = -Tddot[0, 2]
    return Tddot


def RotationZ_ddot(theta: MX, theta_dot: MX, theta_ddot: MX) -> MX:
    T = _Zeros(theta)
    T[0, 0] = T[1, 1] = -transform.mul(transform.cos(theta), theta_dot**2) - transform.mul(transform.sin(theta), theta_ddot)  # type: ignore
    T[1, 0] = -transform.mul(transform.sin(theta), theta_dot**2) + transform.mul(transform.cos(theta), theta_ddot)  # type: ignore
    T[0, 1] = -T[1, 0]
    return T


_tolSin = 2.0 * math.sin(math.pi)


@overload
def _Zeros(dtype: Union[float, omsf.T_NDArray]) -> omsf.T_NDArray: ...


@overload
def _Zeros(dtype: casadi.MX) -> casadi.MX: ...


@overload
def _Zeros(dtype: casadi.SX) -> casadi.SX: ...


def _Zeros(dtype: Union[float, casadi.MX, omsf.T_NDArray, casadi.SX]) -> Union[casadi.SX, casadi.MX, omsf.T_NDArray]:
    if type(dtype) == casadi.SX:
        return casadi.SX(4, 4)
    elif type(dtype) == casadi.MX:
        return casadi.MX(4, 4)
    elif type(dtype) == float:
        return numpy.zeros([4, 4], float)
    elif type(dtype) == omsf.T_NDArray:
        return numpy.zeros([4, 4], float)
    else:
        raise TypeError("dtype is of unsupported type: " + str(type(dtype)))


@overload
def _fullAndSqueeze(x: casadi.DM) -> casadi.DM: ...


@overload
def _fullAndSqueeze(x: casadi.MX) -> casadi.MX: ...


@overload
def _fullAndSqueeze(x: omsf.T_NDArray) -> omsf.T_NDArray: ...


def _fullAndSqueeze(x: Union[casadi.DM, casadi.MX, omsf.T_NDArray]) -> Union[omsf.T_NDArray, casadi.DM, casadi.MX]:
    if isinstance(x, numpy.ndarray):
        return x.squeeze()
    elif isinstance(x, casadi.DM):
        return x.full().squeeze()  # type: ignore
    elif isinstance(x, casadi.MX):  # type: ignore
        return x
    else:
        raise TypeError("x is of unsupported type")


def xyzeul_2_T(
    xyzeul: Union[casadi.MX, omsf.T_NDArray],
    xyzeul_d: Union[casadi.MX, omsf.T_NDArray],
    xyzeul_d2: Union[casadi.MX, omsf.T_NDArray],
) -> Tuple[casadi.MX, casadi.MX, casadi.MX]:

    xyzeul = _fullAndSqueeze(xyzeul)
    xyzeul_d = _fullAndSqueeze(xyzeul_d)
    xyzeul_d2 = _fullAndSqueeze(xyzeul_d2)

    Ttrans = transform.translation(xyzeul[0:3])  # type: ignore
    Ttrans_d = Translation_dot(xyzeul_d[0:3])  # type: ignore
    Ttrans_d2 = Translation_ddot(xyzeul_d2[0:3])  # type: ignore

    Rx = transform.rotationX(xyzeul[3])  # type: ignore
    Ry = transform.rotationY(xyzeul[4])  # type: ignore
    Rz = transform.rotationZ(xyzeul[5])  # type: ignore
    Rx_d = RotationX_dot(xyzeul[3], xyzeul_d[3])  # type: ignore
    Ry_d = RotationY_dot(xyzeul[4], xyzeul_d[4])  # type: ignore
    Rz_d = RotationZ_dot(xyzeul[5], xyzeul_d[5])  # type: ignore
    Rx_d2 = RotationX_ddot(xyzeul[3], xyzeul_d[3], xyzeul_d2[3])  # type: ignore
    Ry_d2 = RotationY_ddot(xyzeul[4], xyzeul_d[4], xyzeul_d2[4])  # type: ignore
    Rz_d2 = RotationZ_ddot(xyzeul[5], xyzeul_d[5], xyzeul_d2[5])  # type: ignore
    Trot = transform.mul(transform.mul(Rz, Ry), Rx)  # type: ignore
    Trot_d = mul3_dot(Rz, Ry, Rx, Rz_d, Ry_d, Rx_d)  # type: ignore
    Trot_d2 = mul3_ddot(Rz, Ry, Rx, Rz_d, Ry_d, Rx_d, Rz_d2, Ry_d2, Rx_d2)  # type: ignore

    T = transform.mul(Ttrans, Trot)  # type: ignore
    Td = mul2_dot(Ttrans, Trot, Ttrans_d, Trot_d)  # type: ignore
    Td2 = mul2_ddot(Ttrans, Trot, Ttrans_d, Trot_d, Ttrans_d2, Trot_d2)  # type: ignore

    return T, Td, Td2  # type:ignore


@overload
def T_2_xyzeul(transformation_matrix: casadi.MX) -> casadi.MX: ...


@overload
def T_2_xyzeul(transformation_matrix: omsf.T_NDArray) -> omsf.T_NDArray: ...


def T_2_xyzeul(transformation_matrix: Union[casadi.SX, casadi.MX, omsf.T_NDArray]) -> Union[casadi.MX, omsf.T_NDArray]:

    # # Calculate euler angles (roll, pitch, yaw)
    dtype = type(transformation_matrix)
    if dtype == casadi.SX or dtype == casadi.MX:
        # Linear position
        assert isinstance(transformation_matrix, casadi.MX)
        xyz_mx: casadi.MX = transformation_matrix[:3, [3]]  # type: ignore
        rotation_matrix_mx: casadi.MX = transformation_matrix[:3, :3]  # type: ignore
        assert isinstance(xyz_mx, casadi.MX) and isinstance(rotation_matrix_mx, casadi.MX)

        square_root: casadi.MX = casadi.sqrt(rotation_matrix_mx[2, 1] ** 2 + rotation_matrix_mx[2, 2] ** 2)  # type: ignore
        assert isinstance(square_root, casadi.MX)

        eul_mx: casadi.MX = casadi.vertcat(  # type: ignore
            casadi.arctan2(rotation_matrix_mx[2, 1], rotation_matrix_mx[2, 2]),  # type: ignore
            casadi.arctan2(-rotation_matrix_mx[2, 0], square_root),  # type: ignore
            casadi.arctan2(rotation_matrix_mx[1, 0], rotation_matrix_mx[0, 0]),  # type: ignore
        )
        assert isinstance(eul_mx, casadi.MX)
        xyzeul_mx = casadi.vertcat(xyz_mx, eul_mx)  # type: ignore
        return xyzeul_mx  # type: ignore
    else:
        # Linear position
        assert isinstance(transformation_matrix, numpy.ndarray)
        xyz: omsf.T_NDArray = transformation_matrix[:3, [3]]
        rotation_matrix: omsf.T_NDArray = transformation_matrix[:3, :3]
        eul = numpy.vstack(
            [
                numpy.arctan2(rotation_matrix[2, 1], rotation_matrix[2, 2]),
                numpy.arctan2(-rotation_matrix[2, 0], numpy.sqrt(rotation_matrix[2, 1] ** 2 + rotation_matrix[2, 2] ** 2)),
                numpy.arctan2(rotation_matrix[1, 0], rotation_matrix[0, 0]),
            ]
        )
        xyzeul = numpy.vstack([xyz, eul])

    # Return
    return xyzeul
