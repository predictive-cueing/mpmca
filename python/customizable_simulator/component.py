# Copyright Frank Drop and Mario Olivari ("Predictive cueing").
# Please read carefully the terms and conditions (root/LICENCE.md) and
# any accompanying documentation before you download and/or use
# the Model Predictive Motion Cueing Algorithm (MPMCA) software.
# cSpell:disable

from typing import Any, List, Union

import numpy

import casadi  # type: ignore
import omsf
from omsf import MotionLimits

from .dof import DenavitHartenbergDoF, DoF, EmptyDoF, PoseDoF, PoseWithLegsDoF
from .extras import T_2_xyzeul

"""
Component base class
"""


class Component:

    def __init__(self, dof: DoF = EmptyDoF(), info: List[Any] = []):
        # Data check
        assert isinstance(dof, DoF)

        # Init states
        self.q: casadi.MX = dof.q
        self.qd = dof.qd
        self.qd2 = dof.qd2
        self.motion_limits = dof.q_motion_limits
        self.nominal_position = dof.nominal_position
        self.transformation_matrix = dof.LocalToBase()

        # Constraints
        self.c_expr: List[casadi.MX] = dof.gc
        self.lbc: List[float] = dof.lbc
        self.ubc: List[float] = dof.ubc

        # Infos
        self.info = info

    def get_constraint(self) -> "dict[str, Union[casadi.MX, omsf.T_NDArray]]":
        res: casadi.MX = casadi.vertcat(*self.c_expr)  # type: ignore
        return {"expr": res, "lb": numpy.array(self.lbc), "ub": numpy.array(self.ubc)}

    def _set_members(
        self,
        q: casadi.MX,
        qd: casadi.MX,
        qd2: casadi.MX,
        T: casadi.MX,
        motion_limits: List[MotionLimits],
        nominal_position: omsf.T_NDArray,
        c_expr: List[casadi.MX],
        lbc: List[float],
        ubc: List[float],
        info: List[Any],
    ) -> None:
        self.q = q
        self.qd = qd
        self.qd2 = qd2
        self.motion_limits = motion_limits
        self.nominal_position = nominal_position
        self.transformation_matrix = T
        self.c_expr = c_expr
        self.lbc = lbc
        self.ubc = ubc
        self.info = info

    def __add__(self, next_component: "Component"):
        # States
        q: casadi.MX = casadi.vertcat(self.q, next_component.q)  # type: ignore
        qd: casadi.MX = casadi.vertcat(self.qd, next_component.qd)  # type: ignore
        qd2: casadi.MX = casadi.vertcat(self.qd2, next_component.qd2)  # type: ignore

        # Motion limits
        motion_limits = self.motion_limits + next_component.motion_limits

        # Nominal position
        nominal_position = numpy.concatenate((self.nominal_position, next_component.nominal_position))

        # Local to Base transformation
        transformation_matrix = casadi.mtimes(self.transformation_matrix, next_component.transformation_matrix)  # type:ignore

        # Constraints
        c_expr = self.c_expr + next_component.c_expr
        lbc = self.lbc + next_component.lbc
        ubc = self.ubc + next_component.ubc

        # Info
        info = self.info + next_component.info

        # Return Component
        c = Component()
        assert (
            isinstance(q, casadi.MX)
            and isinstance(qd, casadi.MX)
            and isinstance(qd2, casadi.MX)
            and isinstance(transformation_matrix, casadi.MX)
        )
        c._set_members(
            q,
            qd,
            qd2,
            transformation_matrix,
            motion_limits,
            nominal_position,
            c_expr,
            lbc,
            ubc,
            info,
        )
        return c


"""
Components
"""


def _indexToLogical(index: List[int], N: int) -> List[bool]:
    logical = [False] * N
    for k in index:
        logical[k] = True
    return logical


class TransformationMatrix(Component):
    def __init__(self, transformation_matrix: Union[casadi.MX, omsf.T_NDArray]) -> None:
        # Create T DoF
        T_dof = EmptyDoF(transformation_matrix)

        # Transformation matrix info includes: euler angles, translation vector, is_DoF_active
        is_DoF_active = [False] * 6
        info: List[Any] = T_2_xyzeul(transformation_matrix).flatten().tolist() + is_DoF_active  # type: ignore

        # Init super class
        assert isinstance(info, list)
        super().__init__(T_dof, info)

    def __repr__(self) -> str:
        return "TransformationMatrix"


class YawTable(Component):
    def __init__(self, nominal_position: omsf.T_NDArray, motion_limits: List[MotionLimits]) -> None:
        # Create YawTable DoF
        active_DoFs = [5]
        yaw_dof = PoseDoF(active_DoFs, nominal_position, motion_limits)

        # YawTable info includes: is_DoF_active
        is_DoF_active = _indexToLogical(active_DoFs, 6)
        info = is_DoF_active

        # Init super class
        super().__init__(yaw_dof, info)

    def __repr__(self) -> str:
        return "YawTable"


class XyTable(Component):
    def __init__(self, nominal_position: omsf.T_NDArray, motion_limits: List[MotionLimits]) -> None:
        # Create XyTable DoF
        active_DoFs = [0, 1]
        xy_dof = PoseDoF(active_DoFs, nominal_position, motion_limits)

        # XyTable info includes: is_DoF_active
        is_DoF_active = _indexToLogical(active_DoFs, 6)
        info = is_DoF_active

        # Init super class
        super().__init__(xy_dof, info)

    def __repr__(self) -> str:
        return "XyTable"


class YzTracks(Component):
    def __init__(self, nominal_position: omsf.T_NDArray, motion_limits: List[MotionLimits]) -> None:
        # Create yz DoF
        active_DoFs = [1, 2]
        yz_dof = PoseDoF(active_DoFs, nominal_position, motion_limits)

        # Infos
        is_DoF_active = _indexToLogical(active_DoFs, 6)
        info = is_DoF_active

        # Init super class
        super().__init__(yz_dof, info)

    def __repr__(self) -> str:
        return "YzTracks"


class Gimbal(Component):
    def __init__(self, nominal_position: omsf.T_NDArray, motion_limits: List[MotionLimits]) -> None:
        # Create Gimbal DoF (Roll - Yaw - Pitch)
        active_DoFs = [3, 5, 4]
        gimbal_dof = PoseDoF(active_DoFs, nominal_position, motion_limits)

        # Gimbal info includes: is_DoF_active
        is_DoF_active = _indexToLogical(active_DoFs, 6)
        info = is_DoF_active

        # Init super class
        super().__init__(gimbal_dof, info)

    def __repr__(self) -> str:
        return "Gimbal"


class ConstraintFreeBox(Component):
    def __init__(self, nominal_position: omsf.T_NDArray, motion_limits: List[MotionLimits]) -> None:
        active_DoFs = [0, 1, 2, 5, 4, 3]
        cfs_dof = PoseDoF(active_DoFs, nominal_position, motion_limits)

        is_DoF_active = _indexToLogical(active_DoFs, 6)
        info = is_DoF_active

        # Init super class
        super().__init__(cfs_dof, info)

    def __repr__(self) -> str:
        return "ConstraintFreeBox"


class Hexapod(Component):
    def __init__(
        self,
        platform_nominal_position: omsf.T_NDArray,
        platform_motion_limits: List[MotionLimits],
        leg_motion_limits: List[MotionLimits],
        platform_corners_P: omsf.T_NDArray,
        base_corners_B: omsf.T_NDArray,
    ):
        # Assert
        N_legs = len(leg_motion_limits)
        assert N_legs == 6
        assert N_legs == platform_corners_P.shape[1]
        assert N_legs == base_corners_B.shape[1]

        # Create hexapod DoF
        active_DoFs = [0, 1, 2, 5, 4, 3]
        hex_dof = PoseWithLegsDoF(
            active_DoFs,
            platform_nominal_position,
            platform_motion_limits,
            platform_corners_P,
            base_corners_B,
            leg_motion_limits,
        )

        # Hexapod info includes: base_corners_B, platform_corners_P, is_DoF_active
        is_DoF_active = _indexToLogical(active_DoFs, 6)
        info = base_corners_B.transpose().flatten().tolist() + platform_corners_P.transpose().flatten().tolist() + is_DoF_active

        # Init super class
        super().__init__(hex_dof, info)

    def __repr__(self) -> str:
        return "Hexapod"


class PrismaticLink(Component):
    def __init__(self, d_nominal: float, d_ml: MotionLimits, a: float, alpha: float, theta: float) -> None:

        # Create Denavit-Hartenberg DoF
        q0 = numpy.array([theta, d_nominal, alpha, a])
        are_DoFs_active = [False, True, False, False]
        dh_prismatic_DoF = DenavitHartenbergDoF(are_DoFs_active, q0, [d_ml])

        # Info vector
        d_offset = 0.0
        q_info = [theta, d_offset, alpha, a]
        info = q_info + are_DoFs_active

        # Init super class
        super().__init__(dh_prismatic_DoF, info)

    def __repr__(self) -> str:
        return "PrismaticLink"


class RevoluteLink(Component):
    def __init__(
        self,
        theta_nominal: float,
        theta_ml: MotionLimits,
        a: float,
        d: float,
        alpha: float,
        theta_scale: float = 1.0,
        theta_offset: float = 0.0,
    ) -> None:
        # Create Denavit-Hartenberg DoF
        q0 = numpy.array([theta_nominal, d, alpha, a])
        are_DoFs_active = [True, False, False, False]
        dh_revolute_DoF = DenavitHartenbergDoF(are_DoFs_active, q0, [theta_ml], theta_scale, theta_offset)

        # Info vector
        q_info = [theta_offset, d, alpha, a]
        info = q_info + are_DoFs_active

        # Init super class
        super().__init__(dh_revolute_DoF, info)

    def __repr__(self) -> str:
        return "RevoluteLink"
