# Copyright Frank Drop and Mario Olivari ("Predictive cueing").
# Please read carefully the terms and conditions (root/LICENCE.md) and
# any accompanying documentation before you download and/or use
# the Model Predictive Motion Cueing Algorithm (MPMCA) software.

from typing import List

import casadi  # type: ignore
from customizable_simulator import component

import casadi_extras as ct
import omsf


def makePlatform(component_list: List[component.Component]) -> component.Component:
    # Compose the platform
    platform = component.Component()
    for comp_k in component_list:
        platform += comp_k
    if platform.q.is_empty():  # type: ignore
        raise ValueError("The number of states is 0. However, the Customizable Simulator requires a number of states > 0.")

    return platform


def makeHeadInertial(platform: component.Component, transformation_matrix_head_platform: casadi.MX):
    q = platform.q
    v = platform.qd
    u = platform.qd2
    transformation_matrix_platform_world = platform.transformation_matrix

    rotation_matrix_WP: casadi.MX = casadi.transpose(transformation_matrix_platform_world[:3, :3])  # type:ignore
    a_P, omega_P, alpha_P = omsf.inertialSignalFromTransformationMatrix(transformation_matrix_platform_world, q, v, u)
    f_P: casadi.MX = casadi.mtimes(rotation_matrix_WP, omsf.GRAVITY) - a_P  # type: ignore

    transformation_matrix_platform_head: casadi.MX = omsf.transform.inv(transformation_matrix_head_platform)  # type: ignore
    rotation_matrix_head_platform: casadi.MX = transformation_matrix_head_platform[:3, 3]  # type: ignore
    rotation_matrix_platform_head: casadi.MX = transformation_matrix_platform_head[:3, :3]  # type: ignore

    f_H: casadi.MX = casadi.mtimes(  # type: ignore
        rotation_matrix_platform_head,
        f_P - casadi.cross(alpha_P, rotation_matrix_head_platform) - casadi.cross(omega_P, casadi.cross(omega_P, rotation_matrix_head_platform)),  # type: ignore
    )
    omega_H: casadi.MX = casadi.mtimes(rotation_matrix_platform_head, omega_P)  # type: ignore
    alpha_H: casadi.MX = casadi.mtimes(rotation_matrix_platform_head, alpha_P)  # type: ignore

    y = ct.struct_MX(omsf.INERTIAL_SIGNAL)
    y["f"] = casadi.densify(f_H)  # type: ignore
    y["omega"] = casadi.densify(omega_H)  # type: ignore
    y["alpha"] = casadi.densify(alpha_H)  # type: ignore

    return y


def makeHeadPosePva(platform: component.Component, transformation_matrix_head_platform: casadi.MX) -> ct.struct_MX:
    q = platform.q
    qd = platform.qd
    qdd = platform.qd2
    transformation_matrix_platform_world = platform.transformation_matrix

    # Transformation matrix Head to World
    transformation_matrix_head_world = casadi.mtimes(transformation_matrix_platform_world, transformation_matrix_head_platform)  # type: ignore

    # Time-derivative of the transformation matrix
    dT_HW = casadi.jtimes(transformation_matrix_head_world, q, qd)  # type: ignore

    # Second time-derivative of the transformation matrix
    ddT_HW = casadi.jtimes(dT_HW, q, qd) + casadi.jtimes(transformation_matrix_head_world, q, qdd)  # type: ignore

    # Rotation matrices and time derivatives
    rotation_matrix_head_world = transformation_matrix_head_world[:3, :3]  # type: ignore
    dR_HW = dT_HW[:3, :3]  # type: ignore
    ddR_HW = ddT_HW[:3, :3]  # type: ignore

    # Rotation matrix from World to Head
    rotation_matrix_world_head = casadi.transpose(rotation_matrix_head_world)  # type: ignore

    # Rotational velocity in MF.
    Omega = casadi.mtimes(rotation_matrix_world_head, dR_HW)  # type: ignore
    omega = casadi.vertcat(Omega[2, 1], Omega[0, 2], Omega[1, 0])  # type: ignore

    # Rotational acceleration in MF.
    Alpha = casadi.mtimes(casadi.transpose(dR_HW), dR_HW) + casadi.mtimes(rotation_matrix_world_head, ddR_HW)  # type: ignore
    alpha = casadi.vertcat(Alpha[2, 1], Alpha[0, 2], Alpha[1, 0])  # type: ignore

    p_W = transformation_matrix_head_world[:3, 3]  # type: ignore
    v_W = dT_HW[:3, 3]  # type: ignore
    a_W = ddT_HW[:3, 3]  # type: ignore

    rotation_matrix_head_world = transformation_matrix_head_world[:3, :3]  # type: ignore

    y = ct.struct_MX(
        [
            ct.entry("p_W", expr=p_W),
            ct.entry("R_HW", expr=rotation_matrix_head_world),
            ct.entry("v_W", expr=v_W),
            ct.entry("omega", expr=omega),
            ct.entry("a_W", expr=a_W),
            ct.entry("alpha", expr=alpha),
        ]
    )

    return y


if __name__ == "__main__":
    pass
