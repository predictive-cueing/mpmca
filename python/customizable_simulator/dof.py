# Copyright Frank Drop and Mario Olivari ("Predictive cueing").
# Please read carefully the terms and conditions (root/LICENCE.md) and
# any accompanying documentation before you download and/or use
# the Model Predictive Motion Cueing Algorithm (MPMCA) software.

import typing
from abc import ABC, abstractmethod
from typing import List, Tuple, Union

import numpy

import casadi  # type: ignore
import omsf
from casadi.casadi import MX  # type: ignore
from omsf import MotionLimits, transform
from omsf.motion_limits import MotionLimits

from .leg_geometry import LegGeometry

## Empty symbolic
EMPTY_MX: casadi.MX = casadi.MX.sym("empty_mx", 0)  # type: ignore

"""
DoF policies
"""


class DoF(ABC):
    def __init__(self, Nq: int, q0: omsf.T_NDArray, q_motion_limits: List[MotionLimits]):
        # Data preprocess
        assert len(q0) == Nq and len(q_motion_limits) == Nq

        # Init DoF members
        self.q: casadi.MX = casadi.MX.sym("q", Nq)  # type: ignore
        self.qd: casadi.MX = casadi.MX.sym("qd", Nq)  # type: ignore
        self.qd2: casadi.MX = casadi.MX.sym("qd2", Nq)  # type: ignore
        self.Nq = Nq
        self.nominal_position = q0
        self.q_motion_limits = q_motion_limits

        # Init Constraint members with empty constraints
        self.lbc: List[float] = []
        self.ubc: List[float] = []
        self.gc: List[casadi.MX] = [EMPTY_MX]

        super().__init__()

    # Abstract method
    @abstractmethod
    def LocalToBase(self) -> Union[casadi.MX, omsf.T_NDArray]:
        pass

class EmptyDoF(DoF):
    def __init__(self, transformation_matrix: Union[casadi.MX, omsf.T_NDArray] = casadi.MX.eye(4)) -> None:  # type: ignore
        self.transformation_matrix = transformation_matrix
        super().__init__(0, numpy.array([]), [])

    def LocalToBase(self) -> Union[casadi.MX, omsf.T_NDArray]:
        return self.transformation_matrix


class PoseDoF(DoF):
    # active_DoFs = [0, ..., 5]: x, y, z, roll, pitch, yaw
    def __init__(self, active_DoFs: List[int], q0: omsf.T_NDArray, q_motion_limits: List[MotionLimits]) -> None:
        # Data preprocess
        Nq = len(active_DoFs)

        # Init members
        self.active_DoFs = active_DoFs
        self.active_DoFs_sorted = sorted(active_DoFs)

        # Init Base Class
        super().__init__(Nq, q0, q_motion_limits)

    def LocalToBase(self) -> casadi.MX:
        q6: casadi.MX = self._get6DoF(self.q)

        T_matrix: casadi.MX = transform.identityTyped(self.q)
        for DoF_k in self.active_DoFs:
            tmp: typing.Any = q6[DoF_k]
            assert isinstance(tmp, casadi.MX)
            if DoF_k == 0:
                T_matrix = transform.mul(T_matrix, transform.translationX(tmp))
            if DoF_k == 1:
                T_matrix = transform.mul(T_matrix, transform.translationY(tmp))
            if DoF_k == 2:
                T_matrix = transform.mul(T_matrix, transform.translationZ(tmp))
            if DoF_k == 3:
                T_matrix = transform.mul(T_matrix, transform.rotationX(tmp))
            if DoF_k == 4:
                T_matrix = transform.mul(T_matrix, transform.rotationY(tmp))
            if DoF_k == 5:
                T_matrix = transform.mul(T_matrix, transform.rotationZ(tmp))

        return T_matrix

    def _get6DoF(self, q: casadi.MX) -> casadi.MX:
        q6 = casadi.MX(6, 1)
        for k, DoF_k in enumerate(self.active_DoFs_sorted):
            q6[DoF_k] = q[k]
        return q6


class PoseWithLegsDoF(PoseDoF):
    def __init__(
        self,
        active_DoFs: List[int],
        q0: omsf.T_NDArray,
        qml: List[MotionLimits],
        platform_corners_P: omsf.T_NDArray,
        base_corners_B: omsf.T_NDArray,
        leg_motion_limits: List[MotionLimits],
    ):
        super().__init__(active_DoFs, q0, qml)

        self.N_legs: int = len(leg_motion_limits)
        assert self.N_legs == platform_corners_P.shape[1]
        assert self.N_legs == base_corners_B.shape[1]

        self.l, self.ld, self.ld2 = self._calculateInverseKinematics(platform_corners_P, base_corners_B)
        self.l_ub, self.l_lb, self.ld_ub, self.ld_lb, self.ld2_ub, self.ld2_lb = self._unpackMotionLimits(leg_motion_limits)

        self._addLegs()

    def _calculateInverseKinematics(
        self, platform_corners_P: omsf.T_NDArray, base_corners_B: omsf.T_NDArray
    ) -> Tuple[casadi.MX, casadi.MX, casadi.MX]:
        lg = LegGeometry(platform_corners_P, base_corners_B)

        q6 = self._get6DoF(self.q)
        q6_d = self._get6DoF(self.qd)
        q6_d2 = self._get6DoF(self.qd2)

        l, ld, ld2 = lg.inverse_kinematics(q6, q6_d, q6_d2)

        return l, ld, ld2

    def _unpackMotionLimits(
        self, leg_motion_limits: List[MotionLimits]
    ) -> Tuple[List[float], List[float], List[float], List[float], List[float], List[float]]:
        l_ub = [leg_motion_limits[k].positionMax for k in range(self.N_legs)]
        l_lb = [leg_motion_limits[k].positionMin for k in range(self.N_legs)]
        ld_ub = [leg_motion_limits[k].velocityMax for k in range(self.N_legs)]
        ld_lb = [leg_motion_limits[k].velocityMin for k in range(self.N_legs)]
        ld2_ub = [leg_motion_limits[k].accelerationMax for k in range(self.N_legs)]
        ld2_lb = [leg_motion_limits[k].accelerationMin for k in range(self.N_legs)]

        return l_ub, l_lb, ld_ub, ld_lb, ld2_ub, ld2_lb

    def _addLegs(self) -> None:
        # Add constraints
        self.gc += [casadi.vertcat(self.l, self.ld, self.ld2)]  # type: ignore
        self.lbc += self.l_lb + self.ld_lb + self.ld2_lb
        self.ubc += self.l_ub + self.ld_ub + self.ld2_ub

    def addLegsStoppability(self) -> None:
        # Expression
        gcStop_1: casadi.MX = self.ld**2 - 2 * (self.l - self.l_ub) * self.ld2_lb  # type: ignore
        gcStop_2: casadi.MX = self.ld**2 - 2 * (self.l - self.l_lb) * self.ld2_ub  # type: ignore

        # Limits
        lbcStop_1 = [-2 * self.ld2_lb[kk] * (self.l_lb[kk] - self.l_ub[kk]) for kk in range(self.N_legs)]
        ubcStop_1 = [0.0] * self.N_legs
        lbcStop_2 = [-2 * self.ld2_ub[kk] * (self.l_ub[kk] - self.l_lb[kk]) for kk in range(self.N_legs)]
        ubcStop_2 = [0.0] * self.N_legs

        # Add constraints
        self.gc += [casadi.vertcat(gcStop_1, gcStop_2)]  # type: ignore
        self.lbc += lbcStop_1 + lbcStop_2
        self.ubc += ubcStop_1 + ubcStop_2


class PoseWithNormalizedLegsDoF(PoseDoF):
    def __init__(
        self,
        active_DoFs: List[int],
        q0: omsf.T_NDArray,
        qml: List[MotionLimits],
        platform_corners_P: omsf.T_NDArray,
        base_corners_B: omsf.T_NDArray,
        leg_motion_limits: List[MotionLimits],
    ):
        super().__init__(active_DoFs, q0, qml)

        self.N_legs = len(leg_motion_limits)
        assert self.N_legs == platform_corners_P.shape[1]
        assert self.N_legs == base_corners_B.shape[1]

        self.l, self.ld, self.ld2 = self._calculateInverseKinematics(platform_corners_P, base_corners_B)
        self.l_ub, self.l_lb, self.ld_ub, self.ld_lb, self.ld2_ub, self.ld2_lb = self._unpackMotionLimits(leg_motion_limits)

        self._addNormalizedLegs()

    def _calculateInverseKinematics(
        self, platform_corners_P: omsf.T_NDArray, base_corners_B: omsf.T_NDArray
    ) -> Tuple[casadi.MX, casadi.MX, casadi.MX]:
        lg = LegGeometry(platform_corners_P, base_corners_B)

        q6 = self._get6DoF(self.q)
        q6_d = self._get6DoF(self.qd)
        q6_d2 = self._get6DoF(self.qd2)

        l, ld, ld2 = lg.inverse_kinematics(q6, q6_d, q6_d2)

        return l, ld, ld2

    def _unpackMotionLimits(self, leg_motion_limits: List[MotionLimits]):
        l_ub = [leg_motion_limits[k].positionMax for k in range(self.N_legs)]
        l_lb = [leg_motion_limits[k].positionMin for k in range(self.N_legs)]
        ld_ub = [leg_motion_limits[k].velocityMax for k in range(self.N_legs)]
        ld_lb = [leg_motion_limits[k].velocityMin for k in range(self.N_legs)]
        ld2_ub = [leg_motion_limits[k].accelerationMax for k in range(self.N_legs)]
        ld2_lb = [leg_motion_limits[k].accelerationMin for k in range(self.N_legs)]

        return l_ub, l_lb, ld_ub, ld_lb, ld2_ub, ld2_lb

    def _addNormalizedLegs(self) -> None:
        # Add constraints
        # TODO: INTRODUCE casadi.fabs?
        l_n: List[float] = [(self.l[kk] - self.l_lb[kk]) / (self.l_ub[kk] - self.l_lb[kk]) for kk in range(self.N_legs)]
        ld_n: List[float] = [(self.ld[kk] - self.ld_lb[kk]) / (self.ld_ub[kk] - self.ld_lb[kk]) for kk in range(self.N_legs)]
        ld2_n: List[float] = [(self.ld2[kk] - self.ld2_lb[kk]) / (self.ld2_ub[kk] - self.ld2_lb[kk]) for kk in range(self.N_legs)]

        self.gc += [casadi.vertcat(*l_n, *ld_n, *ld2_n)]  # type: ignore

        self.lbc += [0.0] * self.N_legs * 3
        self.ubc += [1.0] * self.N_legs * 3


class DenavitHartenbergDoF(DoF):
    # DoFs = theta (Rz), d (Tz), alpha (Tx), a (Rx)
    def __init__(
        self,
        are_DoFs_active: List[bool],
        q0_all_DoFs: omsf.T_NDArray,
        qml_active_DoFs: List[MotionLimits],
        theta_scale: float = 1.0,
        theta_offset: float = 0.0,
    ) -> None:
        # Data preprocess_
        assert len(are_DoFs_active) == 4
        Nq = sum(are_DoFs_active)

        # Init members
        self.q0_all = q0_all_DoFs
        self.are_DoFs_active = are_DoFs_active
        self.theta_scale = theta_scale
        self.theta_offset = theta_offset

        # Init Base Class
        q0_activeDoFs = numpy.array([q0_all_DoFs[k] for k, x in enumerate(are_DoFs_active) if x])
        super().__init__(Nq, q0_activeDoFs, qml_active_DoFs)

    def LocalToBase(self) -> MX:
        q4 = self._get4DoF()
        theta: casadi.MX = self.theta_scale * q4[0] + self.theta_offset  # type: ignore
        d = q4[1]  # type: ignore
        alpha = q4[2]  # type: ignore
        a = q4[3]  # type: ignore

        assert isinstance(theta, casadi.MX) and isinstance(a, casadi.MX) and isinstance(d, casadi.MX) and isinstance(alpha, casadi.MX)
        return transform.denavitHartenberg(a, d, alpha, theta)

    def _get4DoF(self) -> MX:
        q4 = casadi.MX(self.q0_all)
        index_active_DoFs = [k for k, x in enumerate(self.are_DoFs_active) if x]
        for k, index_active_DoF_k in enumerate(index_active_DoFs):
            q4[index_active_DoF_k] = self.q[k]

        return q4
