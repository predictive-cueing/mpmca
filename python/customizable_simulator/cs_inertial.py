# Copyright Frank Drop and Mario Olivari ("Predictive cueing").
# Please read carefully the terms and conditions (root/LICENCE.md) and
# any accompanying documentation before you download and/or use
# the Model Predictive Motion Cueing Algorithm (MPMCA) software.

from typing import Any, List

import casadi  # type: ignore
import numpy
from customizable_simulator import component

import casadi_extras as ct
import omsf

from .cs import makeHeadInertial, makeHeadPosePva, makePlatform


class CustomizableSimulatorWithInertialOutput(omsf.DoubleIntegratorDynamicSystem):
    def __init__(
        self,
        component_list: List[component.Component],
        snake_case_name: str = "",
        camel_case_name: str = "",
        param: ct.BoundedVariable = ct.BoundedVariable(),  # type: ignore
        gravity: omsf.T_NDArray = numpy.array([0, 0, 9.81]),
        head_to_platform: omsf.T_NDArray = numpy.identity(4),
        **kwargs: "dict[str, Any]"
    ):

        platform = makePlatform(component_list)
        platform_functions: omsf.FunctionsForDoubleIntegrator = self._makePlatformFunctions(platform)

        # Initialize base class
        super().__init__(
            double_integrator_functions=platform_functions,
            motion_limits=platform.motion_limits,
            nominal_position=platform.nominal_position,
            param=param,
            constraint_lower_limit=platform.get_constraint()["lb"],  # type:ignore
            constraint_upper_limit=platform.get_constraint()["ub"],  # type:ignore
            gravity=gravity,
            head_to_platform=head_to_platform,
            **kwargs
        )

        # Add some members
        self.component_list = component_list
        self.info = platform.info
        self.snake_case_name = snake_case_name
        self.camel_case_name = camel_case_name

    def _makePlatformFunctions(self, platform: component.Component) -> omsf.FunctionsForDoubleIntegrator:
        # q = platform.q
        # v = platform.qd
        # u = platform.qd2
        # transformation_matrix_platform_world = platform.transformation_matrix

        # rotation_matrix_WP: casadi.MX = casadi.transpose(transformation_matrix_platform_world[:3, :3])  # type:ignore
        # a_P, omega_P, alpha_P = omsf.inertialSignalFromTransformationMatrix(transformation_matrix_platform_world, q, v, u)
        # f_P: casadi.MX = casadi.mtimes(rotation_matrix_WP, omsf.GRAVITY) - a_P  # type: ignore

        transformation_matrix_head_platform: casadi.MX = casadi.MX.sym("T_HP", 4, 4)  # type: ignore
        # transformation_matrix_platform_head: casadi.MX = omsf.transform.inv(transformation_matrix_head_platform)  # type: ignore
        # rotation_matrix_head_platform: casadi.MX = transformation_matrix_head_platform[:3, 3]  # type: ignore
        # rotation_matrix_platform_head: casadi.MX = transformation_matrix_platform_head[:3, :3]  # type: ignore

        # f_H: casadi.MX = casadi.mtimes(  # type: ignore
        #     rotation_matrix_platform_head,
        #     f_P - casadi.cross(alpha_P, rotation_matrix_head_platform) - casadi.cross(omega_P, casadi.cross(omega_P, rotation_matrix_head_platform)),  # type: ignore
        # )
        # omega_H: casadi.MX = casadi.mtimes(rotation_matrix_platform_head, omega_P)  # type: ignore
        # alpha_H: casadi.MX = casadi.mtimes(rotation_matrix_platform_head, alpha_P)  # type: ignore

        # y = ct.struct_MX(omsf.INERTIAL_SIGNAL)
        # y["f"] = casadi.densify(f_H)  # type: ignore
        # y["omega"] = casadi.densify(omega_H)  # type: ignore
        # y["alpha"] = casadi.densify(alpha_H)  # type: ignore

        assert isinstance(transformation_matrix_head_platform, casadi.MX)
        cg = platform.get_constraint()["expr"]
        assert isinstance(cg, casadi.MX)

        functions = omsf.FunctionsForDoubleIntegrator(
            platform.q,
            makeHeadInertial(platform, transformation_matrix_head_platform),
            platform.transformation_matrix,
            v=platform.qd,
            u=platform.qd2,
            transformation_matrix_head_platform=transformation_matrix_head_platform,
            cg=cg,
            head_pva=makeHeadPosePva(platform, transformation_matrix_head_platform),
            head_inertial=makeHeadInertial(platform, transformation_matrix_head_platform),
        )

        return functions

    def __str__(self) -> str:
        cl_str = ";".join(map(str, self.component_list))
        return cl_str
