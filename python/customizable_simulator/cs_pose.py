# Copyright Frank Drop and Mario Olivari ("Predictive cueing").
# Please read carefully the terms and conditions (root/LICENCE.md) and
# any accompanying documentation before you download and/or use
# the Model Predictive Motion Cueing Algorithm (MPMCA) software.

from typing import Any, List

import casadi  # type: ignore
import numpy
from customizable_simulator import component

import casadi_extras as ct
import omsf

from .cs import makeHeadInertial, makeHeadPosePva, makePlatform


class CustomizableSimulatorWithPoseOutput(omsf.DoubleIntegratorDynamicSystem):
    def __init__(
        self,
        component_list: List[component.Component],
        snake_case_name: str = "",
        camel_case_name: str = "",
        param: ct.BoundedVariable = ct.BoundedVariable(),  # type: ignore
        gravity: omsf.T_NDArray = numpy.array([0, 0, 9.81]),
        head_to_platform: omsf.T_NDArray = numpy.identity(4),
        **kwargs: "dict[str, Any]"
    ):

        platform = makePlatform(component_list)
        platform_functions = self._makePlatformFunctions(platform)

        # Initialize base class
        omsf.DoubleIntegratorDynamicSystem.__init__(
            self,
            double_integrator_functions=platform_functions,
            motion_limits=platform.motion_limits,
            nominal_position=platform.nominal_position,
            param=param,
            constraint_lower_limit=platform.get_constraint()["lb"],  # type: ignore
            constraint_upper_limit=platform.get_constraint()["ub"],  # type: ignore
            gravity=gravity,
            head_to_platform=head_to_platform,
            **kwargs
        )

        # Add some members
        self.component_list = component_list
        self.info = platform.info
        self.snake_case_name = snake_case_name
        self.camel_case_name = camel_case_name

    def _makePlatformFunctions(self, platform: component.Component) -> omsf.FunctionsForDoubleIntegrator:
        transformation_matrix_head_platform: casadi.MX = casadi.MX.sym("T_HP", 4, 4)  # type:ignore
        cg = platform.get_constraint()["expr"]

        assert isinstance(transformation_matrix_head_platform, casadi.MX)
        assert isinstance(cg, casadi.MX)

        functions_for_double_integrator = omsf.FunctionsForDoubleIntegrator(
            platform.q,
            makeHeadPosePva(platform, transformation_matrix_head_platform),
            platform.transformation_matrix,
            v=platform.qd,
            u=platform.qd2,
            transformation_matrix_head_platform=transformation_matrix_head_platform,
            cg=cg,
            head_pva=makeHeadPosePva(platform, transformation_matrix_head_platform),
            head_inertial=makeHeadInertial(platform, transformation_matrix_head_platform),
        )

        return functions_for_double_integrator

    def __str__(self):
        cl_str = ";".join(map(str, self.component_list))
        return cl_str
