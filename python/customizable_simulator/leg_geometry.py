# Copyright Frank Drop and Mario Olivari ("Predictive cueing").
# Please read carefully the terms and conditions (root/LICENCE.md) and
# any accompanying documentation before you download and/or use
# the Model Predictive Motion Cueing Algorithm (MPMCA) software.
# cSpell:disable
from typing import Tuple, Union

import numpy

import casadi  # type:ignore
import omsf
from omsf import transform

from .extras import sum1, xyzeul_2_T


class LegGeometry:
    def __init__(self, pc_P: omsf.T_NDArray, bc_B: omsf.T_NDArray):
        self.pc_P = pc_P
        self.bc_B = bc_B
        self.N_legs: int = pc_P.shape[1]

    # Works for any number of legs
    def inverse_kinematics(
        self,
        p: Union[casadi.MX, omsf.T_NDArray],
        pd: Union[casadi.MX, omsf.T_NDArray],
        pd2: Union[casadi.MX, omsf.T_NDArray],
    ) -> Tuple[casadi.MX, casadi.MX, casadi.MX]:
        # Find square of leg length and its derivatives
        l2, l2_d, l2_d2 = self._fromPoseToLeg2(p, pd, pd2)

        # Find leg length and it derivatives
        l: casadi.MX = casadi.sqrt(l2)  # type: ignore
        ld: casadi.MX = l2_d / (2 * l)  # type: ignore
        ld2: casadi.MX = l2_d2 / (2 * l) - l2_d * ld / (2 * (l**2))  # type: ignore

        assert isinstance(l, casadi.MX) and isinstance(ld, casadi.MX) and isinstance(ld2, casadi.MX)
        return l, ld, ld2

    def _fromPoseToLeg2(
        self,
        p: Union[casadi.MX, omsf.T_NDArray],
        pd: Union[casadi.MX, omsf.T_NDArray],
        pd2: Union[casadi.MX, omsf.T_NDArray],
    ) -> Tuple[casadi.MX, casadi.MX, casadi.MX]:

        # Find transformation matrices
        T, Td, Td2 = xyzeul_2_T(p, pd, pd2)

        # Platform corners in P0 FoR
        # pc_P_h = numpy.vstack((self.pc_P, numpy.ones([1, self.Nlegs]))) # homogeneous coordinates
        pc_P_h = casadi.vcat((self.pc_P, numpy.ones([1, self.N_legs])))  # type: ignore homogeneous coordinates
        pc_P0_h = transform.mul(T, pc_P_h)  # type: ignore
        pcd_P0_h = transform.mul(Td, pc_P_h)  # type: ignore
        pcd2_P0_h = transform.mul(Td2, pc_P_h)  # type: ignore
        pc_P0 = pc_P0_h[:3, :]  # type:ignore
        pcd_P0 = pcd_P0_h[:3, :]  # type:ignore
        pcd2_P0 = pcd2_P0_h[:3, :]  # type:ignore

        ## Find legs vectors in P0 FoR
        # bc_B_h = numpy.vstack((self.bc_B, numpy.ones([1, self.Nlegs]))) # homogeneous coordinates
        bc_B_h = casadi.vcat((self.bc_B, numpy.ones([1, self.N_legs])))  # type:ignore homogeneous coordinates
        bc_B = bc_B_h[:3, :]  # type:ignore
        leg = pc_P0 - bc_B  # type:ignore
        legd = pcd_P0  # type:ignore
        legd2 = pcd2_P0  # type:ignore

        # Find square of leg lengths and derivatives
        leg2 = (sum1(leg**2)).T  # type:ignore
        leg2_d = (2 * sum1(leg * legd)).T  # type:ignore
        leg2_d2 = (2 * sum1(legd**2 + leg * legd2)).T  # type:ignore

        return leg2, leg2_d, leg2_d2  # type:ignore
