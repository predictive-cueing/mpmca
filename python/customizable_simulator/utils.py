# Copyright Frank Drop and Mario Olivari ("Predictive cueing").
# Please read carefully the terms and conditions (root/LICENCE.md) and
# any accompanying documentation before you download and/or use
# the Model Predictive Motion Cueing Algorithm (MPMCA) software.

from typing import Any, List, Union, overload

import numpy

import casadi  # type:ignore
import omsf

"""
Handle numpy and casadi symbolic data
"""


def isSymbolic(x: Any) -> bool:
    return type(x) is casadi.SX or type(x) is casadi.MX or type(x) is casadi.DM


def isNumpyArray(x: Any) -> bool:
    return type(x) is numpy.ndarray


@overload
def flatten(x: casadi.MX) -> casadi.MX: ...


@overload
def flatten(x: casadi.SX) -> casadi.SX: ...


@overload
def flatten(x: casadi.DM) -> casadi.DM: ...


@overload
def flatten(x: omsf.T_NDArray) -> omsf.T_NDArray: ...


def flatten(x: Union[casadi.MX, casadi.SX, casadi.DM, omsf.T_NDArray]) -> Union[casadi.MX, casadi.SX, casadi.DM, omsf.T_NDArray]:
    if isSymbolic(x):
        xfl = casadi.reshape(x.T, x.numel(), 1)  # type:ignore

    elif isNumpyArray(x):
        xfl = x.flatten()  # type:ignore

    else:
        raise NotImplementedError()

    return xfl  # type:ignore


def tolist(x: Union[casadi.MX, casadi.SX, casadi.DM, omsf.T_NDArray]) -> Union[List[float], List[List[float]]]:
    if isSymbolic(x):
        x_list = list()  # type:ignore
        for kr in range(x.shape[0]):  # type:ignore
            row_k = list()  # type:ignore
            for kc in range(x.shape[1]):  # type:ignore
                row_k = row_k + [x[kr, kc]]  # type:ignore
            x_list.append(row_k)  # type:ignore

    elif isNumpyArray(x):
        x_list = x.tolist()  # type:ignore

    else:
        raise NotImplementedError()

    return x_list  # type:ignore
