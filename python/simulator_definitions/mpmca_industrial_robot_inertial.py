# Copyright Frank Drop and Mario Olivari ("Predictive cueing").
# Please read carefully the terms and conditions (root/LICENCE.md) and
# any accompanying documentation before you download and/or use
# the Model Predictive Motion Cueing Algorithm (MPMCA) software.

import casadi  # type:ignore
import numpy
import omsf
from customizable_simulator import component, cs_inertial
from omsf import transform
from omsf.motion_limits import MotionLimits

# Denavit-Hartenberg conventions.
#
# https://en.wikipedia.org/wiki/Denavit%E2%80%93Hartenberg_parameters
#
# d         = distance along z axis of current joint to the next joint
# theta     = angle around the z axis of current joint to the next joint
# a         = distance along x axis to the next joint
# alpha     = angle around the x axis to the next joint


# A1 (From frame 0 to frame 1)
def A1_theta_nominal() -> float:
    return 0.0


def A1_theta_ml() -> omsf.MotionLimits:
    theta_max = 200.0 * numpy.pi / 180.0  # rad
    theta_min = -theta_max
    theta_d_max = 130.0 * numpy.pi / 180.0  # rad / s
    theta_d_min = -theta_d_max
    theta_d2_max = 110.0 * numpy.pi / 180.0  # rad / s^2
    theta_d2_min = -theta_d2_max

    return omsf.MotionLimits(theta_min, theta_max, theta_d_min, theta_d_max, theta_d2_min, theta_d2_max)


def A1_d() -> float:
    return -0.75


def A1_theta() -> float:
    return 0.0


def A1_a() -> float:
    return 0.45


def A1_alpha() -> float:
    return numpy.pi / 2.0


# A2 (from frame 1 to frame 2)
def A2_theta_nominal() -> float:
    return -90.0 * numpy.pi / 180.0


def A2_theta_ml() -> MotionLimits:
    theta_max = -10.0 * numpy.pi / 180.0  # rad
    theta_min = -150.0 * numpy.pi / 180.0
    theta_d_max = 120.0 * numpy.pi / 180.0  # rad / s
    theta_d_min = -theta_d_max
    theta_d2_max = 120.0 * numpy.pi / 180.0  # rad / s^2
    theta_d2_min = -theta_d2_max

    return omsf.MotionLimits(theta_min, theta_max, theta_d_min, theta_d_max, theta_d2_min, theta_d2_max)


def A2_d() -> float:
    return 0.0


def A2_theta() -> float:
    return 0.0


def A2_a() -> float:
    return 1.30


def A2_alpha() -> float:
    return 0.0


# A3 (from frame 2 to frame 3)
def A3_theta_nominal() -> float:
    return 90.0 * numpy.pi / 180.0


def A3_theta_ml() -> MotionLimits:
    theta_max = 170.0 * numpy.pi / 180.0  # rad
    theta_min = -140.0 * numpy.pi / 180.0
    theta_d_max = 120.0 * numpy.pi / 180.0  # rad / s
    theta_d_min = -theta_d_max
    theta_d2_max = 110.0 * numpy.pi / 180.0  # rad / s^2
    theta_d2_min = -theta_d2_max

    return omsf.MotionLimits(theta_min, theta_max, theta_d_min, theta_d_max, theta_d2_min, theta_d2_max)


def A3_d() -> float:
    return 0.0


def A3_theta() -> float:
    return -90.0 * numpy.pi / 180.0


def A3_a() -> float:
    return -0.05


def A3_alpha() -> float:
    return -90.0 * numpy.pi / 180.0


# A4 (from frame 3 to frame 4)
def A4_theta_nominal() -> float:
    return 0.0


def A4_theta_ml() -> MotionLimits:
    theta_max = 300.0 * numpy.pi / 180.0  # rad
    theta_min = -theta_max
    theta_d_max = 100.0 * numpy.pi / 180.0  # rad / s
    theta_d_min = -theta_d_max
    theta_d2_max = 80.0 * numpy.pi / 180.0  # rad / s^2
    theta_d2_min = -theta_d2_max

    return omsf.MotionLimits(theta_min, theta_max, theta_d_min, theta_d_max, theta_d2_min, theta_d2_max)


def A4_d() -> float:
    return 1.0


def A4_theta() -> float:
    return 0.0


def A4_a() -> float:
    return 0.0


def A4_alpha() -> float:
    return 90.0 * numpy.pi / 180.0


# A5 (from frame 4 to frame 5)
def A5_theta_nominal() -> float:
    return 0.0


def A5_theta_ml() -> MotionLimits:
    theta_max = 100 * numpy.pi / 180.0  # rad
    theta_min = -theta_max
    theta_d_max = 100.0 * numpy.pi / 180.0  # rad / s
    theta_d_min = -theta_d_max
    theta_d2_max = 70.0 * numpy.pi / 180.0  # rad / s^2
    theta_d2_min = -theta_d2_max

    return omsf.MotionLimits(theta_min, theta_max, theta_d_min, theta_d_max, theta_d2_min, theta_d2_max)


def A5_d() -> float:
    return 0.0


def A5_theta() -> float:
    return 0.0


def A5_a() -> float:
    return 0.0


def A5_alpha() -> float:
    return -90.0 * numpy.pi / 180.0


# A6 (from frame 5 to frame 6)
def A6_theta_nominal() -> float:
    return 0.0


def A6_theta_ml() -> MotionLimits:
    theta_max = 550.0 * numpy.pi / 180.0  # rad
    theta_min = -theta_max
    theta_d_max = 150.0 * numpy.pi / 180.0  # rad / s
    theta_d_min = -theta_d_max
    theta_d2_max = 120.0 * numpy.pi / 180.0  # rad / s^2
    theta_d2_min = -theta_d2_max

    return omsf.MotionLimits(theta_min, theta_max, theta_d_min, theta_d_max, theta_d2_min, theta_d2_max)


def A6_d() -> float:
    return 0.3


def A6_theta() -> float:
    return 0.0


def A6_a() -> float:
    return 0.0


def A6_alpha() -> float:
    return 0.0


class Simulator(cs_inertial.CustomizableSimulatorWithInertialOutput):
    def __init__(self):

        # Kinematic chain
        A1 = component.RevoluteLink(A1_theta_nominal(), A1_theta_ml(), A1_a(), A1_d(), A1_alpha(), 1.0, A1_theta())
        A2 = component.RevoluteLink(A2_theta_nominal(), A2_theta_ml(), A2_a(), A2_d(), A2_alpha(), 1.0, A2_theta())
        A3 = component.RevoluteLink(A3_theta_nominal(), A3_theta_ml(), A3_a(), A3_d(), A3_alpha(), 1.0, A3_theta())
        A4 = component.RevoluteLink(A4_theta_nominal(), A4_theta_ml(), A4_a(), A4_d(), A4_alpha(), 1.0, A4_theta())
        A5 = component.RevoluteLink(A5_theta_nominal(), A5_theta_ml(), A5_a(), A5_d(), A5_alpha(), 1.0, A5_theta())
        A6 = component.RevoluteLink(A6_theta_nominal(), A6_theta_ml(), A6_a(), A6_d(), A6_alpha(), 1.0, A6_theta())

        T = component.TransformationMatrix(transform.rotationY(-casadi.pi / 2.0))

        # Name
        camel_case_name = "MpmcaIndustrialRobotInertial"
        snake_case_name = "mpmca_industrial_robot_inertial"

        # Init super class
        super().__init__(component_list=[A1, A2, A3, A4, A5, A6, T], snake_case_name=snake_case_name, camel_case_name=camel_case_name)


if __name__ == "__main__":
    sim = Simulator()
    print(sim)
    pass
