# Copyright Frank Drop and Mario Olivari ("Predictive cueing").
# Please read carefully the terms and conditions (root/LICENCE.md) and
# any accompanying documentation before you download and/or use
# the Model Predictive Motion Cueing Algorithm (MPMCA) software.
from typing import List

import numpy
from customizable_simulator import component, cs_inertial

import omsf
from omsf import transform
from omsf.motion_limits import MotionLimits


def XyTableMotionLimits() -> List[MotionLimits]:
    qb_x = 10  # m
    qb_y = 10
    vb_x = 9.0  # m/s
    vb_y = 9.0
    ub_x = 8.0  # m/s^2
    ub_y = 8.0
    ml = [omsf.MotionLimits(-qb_x, qb_x, -vb_x, vb_x, -ub_x, ub_x), omsf.MotionLimits(-qb_y, qb_y, -vb_y, vb_y, -ub_y, ub_y)]
    return ml


# XyTable nominal position
def XyTableNominalPosition() -> omsf.T_NDArray:
    return numpy.zeros(2)


# Coordinates of hexapod base corners in the Base FoR [m]
def HexapodBaseCorners() -> omsf.T_NDArray:
    B1 = [1.78, 2.40, 3.59]
    B2 = [1.19, 2.74, 3.59]
    B3 = [-2.97, 0.34, 3.59]
    B4 = [-2.97, -0.34, 3.59]
    B5 = [1.19, -2.74, 3.59]
    B6 = [1.78, -2.40, 3.59]
    B = numpy.array([B1, B2, B3, B4, B5, B6]).T

    return B


# Coordinates of hexapod platform corners in the Platform FoR [m]
def HexapodPlatformCorners() -> omsf.T_NDArray:
    P1 = [2.15, 0.18, 0.000000]
    P2 = [-0.92, 1.95, 0.000000]
    P3 = [-1.23, 1.77, 0.000000]
    P4 = [-1.23, -1.77, 0.000000]
    P5 = [-0.92, -1.95, 0.000000]
    P6 = [2.15, -0.18, 0.000000]
    P = numpy.array([P1, P2, P3, P4, P5, P6]).T

    return P


# Legs motion limits
def HexapodLegLimits() -> List[MotionLimits]:
    l_min = 3.4
    l_max = 5.1
    ld_max = 1.3
    ld2_max = 10
    ll = [omsf.MotionLimits(l_min, l_max, -ld_max, ld_max, -ld2_max, ld2_max)] * 6
    return ll


# Hexapod motion limits for the platform pose
def HexapodMotionLimits() -> List[MotionLimits]:
    q_max = 100.0
    ml = [omsf.MotionLimits(-q_max, q_max, -q_max, q_max, -q_max, q_max)] * 6
    return ml


# Hexapod nominal position for the platform pose
def HexapodNominalPosition() -> omsf.T_NDArray:
    return numpy.zeros(6)


# YawTable motion limits
def YawTableMotionLimits() -> List[omsf.MotionLimits]:
    qb = 250.0 * numpy.pi / 180.0
    vb = 60.0 * numpy.pi / 180.0
    ab = 30.0 * numpy.pi / 180.0
    ml = [omsf.MotionLimits(-qb, qb, -vb, vb, -ab, ab)]
    return ml


# YawTable nominal position
def YawTableNominalPosition() -> omsf.T_NDArray:
    return numpy.zeros(1)


def XyToHexapodBase() -> omsf.T_NDArray:
    z = -3.7
    return transform.translationZ(z)


"""
Simulator
"""


class Simulator(cs_inertial.CustomizableSimulatorWithInertialOutput):
    def __init__(self):

        # XyTable
        xy_table_ml = XyTableMotionLimits()
        xy_table = component.XyTable(XyTableNominalPosition(), xy_table_ml)

        # Hexapod
        hexapod = component.Hexapod(
            HexapodNominalPosition(), HexapodMotionLimits(), HexapodLegLimits(), HexapodPlatformCorners(), HexapodBaseCorners()
        )

        # YawTable
        yaw_table = component.YawTable(YawTableNominalPosition(), YawTableMotionLimits())

        transformation_matrix = component.TransformationMatrix(XyToHexapodBase())

        # Name
        camel_case_name = "MpmcaXyHexapodYaw"
        snake_case_name = "mpmca_xy_hexapod_yaw"

        # Init super class
        super().__init__(
            component_list=[xy_table, transformation_matrix, hexapod, yaw_table],
            snake_case_name=snake_case_name,
            camel_case_name=camel_case_name,
        )


if __name__ == "__main__":
    sim = Simulator()
    print(sim)
    pass
