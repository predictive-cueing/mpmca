import numpy as np
import omsf
from customizable_simulator import component, cs_pose
from omsf import transform

"""
Data
"""


def hex_base_corners_B() -> omsf.T_NDArray:
    B1 = [0.58, 1.0, 1.3]
    B2 = [0.36, 1.05, 1.3]
    B3 = [-1.35, 0.12, 1.3]
    B4 = [-1.35, -0.12, 1.3]
    B5 = [0.36, -1.05, 1.3]
    B6 = [0.58, -1.0, 1.3]
    B = np.array([B1, B2, B3, B4, B5, B6]).T

    return B


def hex_platform_corners_P() -> omsf.T_NDArray:
    P1 = [0.85, 0.12, 0.0]
    P2 = [-0.55, 0.92, 0.0]
    P3 = [-0.75, 0.80, 0.0]
    P4 = [-0.75, -0.80, 0.0]
    P5 = [-0.55, -0.92, 0.0]
    P6 = [0.85, -0.12, 0.0]
    P = np.array([P1, P2, P3, P4, P5, P6]).T

    return P


# Legs motion limits
def hex_leg_limits() -> "list[omsf.MotionLimits]":
    l_min = 1.6 - 0.35
    l_max = 1.6 + 0.35
    ld_max = 0.5
    ld2_max = 10
    ll = [omsf.MotionLimits(l_min, l_max, -ld_max, ld_max, -ld2_max, ld2_max)] * 6
    return ll


# Hexapod motion limits for the platform pose
def hex_motion_limits() -> "list[omsf.MotionLimits]":
    q_max = 100.0
    ml = [omsf.MotionLimits(-q_max, q_max, -q_max, q_max, -q_max, q_max)] * 6
    return ml


# Hexapod nominal position for the platform pose
def hex_nominal_position() -> omsf.T_NDArray:
    return np.zeros(6)


def Ground_To_HexapodBase() -> omsf.T_NDArray:
    z = -1.4
    return transform.translationZ(z)


class Simulator(cs_pose.CustomizableSimulatorWithPoseOutput):
    def __init__(self):
        # Hexapod
        hexapod = component.Hexapod(
            hex_nominal_position(), hex_motion_limits(), hex_leg_limits(), hex_platform_corners_P(), hex_base_corners_B()
        )

        T = component.TransformationMatrix(Ground_To_HexapodBase())

        # Name
        camel_case_name = "MpmcaHexapodPose"
        snake_case_name = "mpmca_hexapod_pose"

        # Init super class
        super().__init__(component_list=[T, hexapod], snake_case_name=snake_case_name, camel_case_name=camel_case_name)


if __name__ == "__main__":
    sim = Simulator()
    print(sim)
    pass
