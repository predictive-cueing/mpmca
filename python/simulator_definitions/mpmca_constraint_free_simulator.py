# Copyright Frank Drop and Mario Olivari ("Predictive cueing").
# Please read carefully the terms and conditions (root/LICENCE.md) and
# any accompanying documentation before you download and/or use
# the Model Predictive Motion Cueing Algorithm (MPMCA) software.

from typing import List

import numpy
from customizable_simulator import component, cs_inertial

import omsf


def ConstraintFreeSimulatorMotionLimits() -> List[omsf.MotionLimits]:
    q_max = 100.0
    ml = [omsf.MotionLimits(-q_max, q_max, -q_max, q_max, -q_max, q_max)] * 6
    return ml


def ConstraintFreeSimulatorNominalPosition() -> omsf.T_NDArray:
    return numpy.zeros(6)


class Simulator(cs_inertial.CustomizableSimulatorWithInertialOutput):
    def __init__(self):
        # Constraint free simulator.
        cfs = component.ConstraintFreeBox(ConstraintFreeSimulatorNominalPosition(), ConstraintFreeSimulatorMotionLimits())

        # Name
        camel_case_name = "MpmcaConstraintFreeSimulator"
        snake_case_name = "mpmca_constraint_free_simulator"

        # Init super class
        super().__init__(component_list=[cfs], snake_case_name=snake_case_name, camel_case_name=camel_case_name)


if __name__ == "__main__":
    sim = Simulator()
    print(sim)
    pass
