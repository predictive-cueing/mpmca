# pyright: reportUnknownMemberType=false
import sys

import matplotlib.pyplot as plt
import mcap.reader
import mcap.records
import numpy
from mcap_protobuf.decoder import DecoderFactory


class PlotMpmca:

    def __init__(self, filename: str):
        with open(filename, "rb") as f:
            reader = mcap.reader.make_reader(f, decoder_factories=[DecoderFactory()])
            stats = reader.get_summary().statistics  # pyright: ignore[reportOptionalMemberAccess]
            assert stats is not None

            self.__read_output_inertial_reference_plan_and_time(reader, stats)
            self.__read_output_inertial_expected_plan(reader, stats)
            self.__read_state_plan(reader, stats)

    def __read_output_inertial_reference_plan_and_time(self, reader: mcap.reader.McapReader, stats: mcap.records.Statistics) -> None:
        self.t = numpy.array(0, dtype=float)
        self.y_ref_f_x = numpy.array(0, dtype=float)
        self.y_ref_f_y = numpy.array(0, dtype=float)
        self.y_ref_f_z = numpy.array(0, dtype=float)
        self.y_ref_omega_x = numpy.array(0, dtype=float)
        self.y_ref_omega_y = numpy.array(0, dtype=float)
        self.y_ref_omega_z = numpy.array(0, dtype=float)

        ii = 0
        for _, channel, message, proto_msg in reader.iter_decoded_messages(topics=["/control/output_inertial_reference_plan"]):
            if ii == 0:
                new_shape = stats.channel_message_counts.get(channel.id)  # pyright: ignore[reportOptionalMemberAccess]
                assert type(new_shape) is int

                self.t.resize((new_shape))
                self.y_ref_f_x.resize((new_shape))
                self.y_ref_f_y.resize((new_shape))
                self.y_ref_f_z.resize((new_shape))
                self.y_ref_omega_x.resize((new_shape))
                self.y_ref_omega_y.resize((new_shape))
                self.y_ref_omega_z.resize((new_shape))

            self.t[ii] = message.log_time / 1e9
            self.y_ref_f_x[ii] = proto_msg.signals[0].horizon[0]
            self.y_ref_f_y[ii] = proto_msg.signals[1].horizon[0]
            self.y_ref_f_z[ii] = proto_msg.signals[2].horizon[0]
            self.y_ref_omega_x[ii] = proto_msg.signals[3].horizon[0]
            self.y_ref_omega_y[ii] = proto_msg.signals[4].horizon[0]
            self.y_ref_omega_z[ii] = proto_msg.signals[5].horizon[0]

            ii += 1

    def __read_output_inertial_expected_plan(self, reader: mcap.reader.McapReader, stats: mcap.records.Statistics) -> None:
        self.y_exp_f_x = numpy.array(0, dtype=float)
        self.y_exp_f_y = numpy.array(0, dtype=float)
        self.y_exp_f_z = numpy.array(0, dtype=float)
        self.y_exp_omega_x = numpy.array(0, dtype=float)
        self.y_exp_omega_y = numpy.array(0, dtype=float)
        self.y_exp_omega_z = numpy.array(0, dtype=float)

        ii = 0
        for _, channel, message, proto_msg in reader.iter_decoded_messages(topics=["/control/output_inertial_expected_plan"]):
            if ii == 0:
                new_shape = stats.channel_message_counts.get(channel.id)  # pyright: ignore[reportOptionalMemberAccess]
                assert type(new_shape) is int

                self.t.resize((new_shape))
                self.y_exp_f_x.resize((new_shape))
                self.y_exp_f_y.resize((new_shape))
                self.y_exp_f_z.resize((new_shape))
                self.y_exp_omega_x.resize((new_shape))
                self.y_exp_omega_y.resize((new_shape))
                self.y_exp_omega_z.resize((new_shape))

            self.t[ii] = message.log_time / 1e9
            self.y_exp_f_x[ii] = proto_msg.signals[0].horizon[0]
            self.y_exp_f_y[ii] = proto_msg.signals[1].horizon[0]
            self.y_exp_f_z[ii] = proto_msg.signals[2].horizon[0]
            self.y_exp_omega_x[ii] = proto_msg.signals[3].horizon[0]
            self.y_exp_omega_y[ii] = proto_msg.signals[4].horizon[0]
            self.y_exp_omega_z[ii] = proto_msg.signals[5].horizon[0]

            ii += 1

    def __read_state_plan(self, reader: mcap.reader.McapReader, stats: mcap.records.Statistics) -> None:
        self.x_exp = numpy.array(0, dtype=float)

        ii = 0
        for _, channel, message, proto_msg in reader.iter_decoded_messages(topics=["/control/output_inertial_expected_plan"]):
            if ii == 0:
                new_shape = stats.channel_message_counts.get(channel.id)  # pyright: ignore[reportOptionalMemberAccess]
                assert type(new_shape) is int

                self.t.resize((new_shape))
                self.y_exp_f_x.resize((new_shape))
                self.y_exp_f_y.resize((new_shape))
                self.y_exp_f_z.resize((new_shape))
                self.y_exp_omega_x.resize((new_shape))
                self.y_exp_omega_y.resize((new_shape))
                self.y_exp_omega_z.resize((new_shape))

            self.t[ii] = message.log_time / 1e9
            self.y_exp_f_x[ii] = proto_msg.signals[0].horizon[0]
            self.y_exp_f_y[ii] = proto_msg.signals[1].horizon[0]
            self.y_exp_f_z[ii] = proto_msg.signals[2].horizon[0]
            self.y_exp_omega_x[ii] = proto_msg.signals[3].horizon[0]
            self.y_exp_omega_y[ii] = proto_msg.signals[4].horizon[0]
            self.y_exp_omega_z[ii] = proto_msg.signals[5].horizon[0]

            ii += 1


if __name__ == "__main__":
    d_short = PlotMpmca("data/cascaded_short.mcap")
    d_long = PlotMpmca("data/cascaded_long.mcap")
    d_cascaded = PlotMpmca("data/cascaded_cascaded_short.mcap")

    plt.figure(1)
    plt.subplot(2, 1, 1)
    plt.plot(d_short.t, d_short.y_ref_f_x, "r")
    plt.plot(d_long.t, d_long.y_ref_f_x, "g--")
    plt.plot(d_cascaded.t, d_cascaded.y_ref_f_x, "b-.")
    plt.grid(True)
    plt.xlim([10, 15])

    plt.subplot(2, 1, 2)
    plt.plot(d_short.t, d_short.y_exp_f_x, "r")
    plt.plot(d_long.t, d_long.y_exp_f_x, "g--")
    plt.plot(d_cascaded.t, d_cascaded.y_exp_f_x, "b-.")
    plt.xlim([10, 15])
    plt.grid(True)
    # plt.show()

    plt.figure(2)
    plt.subplot(2, 1, 1)
    plt.plot(d_short.t, d_short.y_ref_omega_x, "r")
    plt.plot(d_long.t, d_long.y_ref_omega_x, "g--")
    plt.plot(d_cascaded.t, d_cascaded.y_ref_omega_x, "b-.")
    plt.grid(True)
    plt.xlim([10, 15])

    plt.subplot(2, 1, 2)
    plt.plot(d_short.t, d_short.y_exp_omega_x, "r")
    plt.plot(d_long.t, d_long.y_exp_omega_x, "g--")
    plt.plot(d_cascaded.t, d_cascaded.y_exp_omega_x, "b-.")
    plt.xlim([10, 15])
    plt.grid(True)
    plt.show()
