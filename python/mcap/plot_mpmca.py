# pyright: reportUnknownMemberType=false
import sys
from typing import Tuple

import matplotlib.pyplot as plt
import mcap.reader
import mcap.records
import numpy
from mcap_protobuf.decoder import DecoderFactory


class PlotMpmca:

    def __init__(self, filename: str):
        with open(filename, "rb") as f:
            reader = mcap.reader.make_reader(f, decoder_factories=[DecoderFactory()])
            stats = reader.get_summary().statistics  # pyright: ignore[reportOptionalMemberAccess]
            assert stats is not None

            self.__read_output_inertial_reference_plan_and_time(reader, stats)
            self.__read_output_inertial_expected_plan(reader, stats)
            self.__read_state_expected_plan(reader, stats)

    def __read_output_inertial_reference_plan_and_time(self, reader: mcap.reader.McapReader, stats: mcap.records.Statistics) -> None:
        self.t = numpy.array(0, dtype=float)
        self.y_ref_f_x = numpy.array(0, dtype=float)
        self.y_ref_f_y = numpy.array(0, dtype=float)
        self.y_ref_f_z = numpy.array(0, dtype=float)
        self.y_ref_omega_x = numpy.array(0, dtype=float)
        self.y_ref_omega_y = numpy.array(0, dtype=float)
        self.y_ref_omega_z = numpy.array(0, dtype=float)

        ii = 0
        t_start = 0
        for _, channel, message, proto_msg in reader.iter_decoded_messages(topics=["/control/output_inertial_reference_plan"]):
            if ii == 0:
                new_shape = stats.channel_message_counts.get(channel.id)  # pyright: ignore[reportOptionalMemberAccess]
                assert type(new_shape) is int

                t_start = message.log_time / 1e9
                self.t.resize((new_shape))
                self.y_ref_f_x.resize((new_shape))
                self.y_ref_f_y.resize((new_shape))
                self.y_ref_f_z.resize((new_shape))
                self.y_ref_omega_x.resize((new_shape))
                self.y_ref_omega_y.resize((new_shape))
                self.y_ref_omega_z.resize((new_shape))

            self.t[ii] = message.log_time / 1e9 - t_start
            self.y_ref_f_x[ii] = proto_msg.signals[0].horizon[0]
            self.y_ref_f_y[ii] = proto_msg.signals[1].horizon[0]
            self.y_ref_f_z[ii] = proto_msg.signals[2].horizon[0]
            self.y_ref_omega_x[ii] = proto_msg.signals[3].horizon[0]
            self.y_ref_omega_y[ii] = proto_msg.signals[4].horizon[0]
            self.y_ref_omega_z[ii] = proto_msg.signals[5].horizon[0]

            ii += 1

    def __read_output_inertial_expected_plan(self, reader: mcap.reader.McapReader, stats: mcap.records.Statistics) -> None:
        self.y_exp_f_x = numpy.array(0, dtype=float)
        self.y_exp_f_y = numpy.array(0, dtype=float)
        self.y_exp_f_z = numpy.array(0, dtype=float)
        self.y_exp_omega_x = numpy.array(0, dtype=float)
        self.y_exp_omega_y = numpy.array(0, dtype=float)
        self.y_exp_omega_z = numpy.array(0, dtype=float)

        ii = 0
        t_start = 0
        for _, channel, message, proto_msg in reader.iter_decoded_messages(topics=["/control/output_inertial_expected_plan"]):
            if ii == 0:
                new_shape = stats.channel_message_counts.get(channel.id)  # pyright: ignore[reportOptionalMemberAccess]
                assert type(new_shape) is int

                t_start = message.log_time / 1e9
                self.t.resize((new_shape))
                self.y_exp_f_x.resize((new_shape))
                self.y_exp_f_y.resize((new_shape))
                self.y_exp_f_z.resize((new_shape))
                self.y_exp_omega_x.resize((new_shape))
                self.y_exp_omega_y.resize((new_shape))
                self.y_exp_omega_z.resize((new_shape))

            self.t[ii] = message.log_time / 1e9 - t_start
            self.y_exp_f_x[ii] = proto_msg.signals[0].horizon[0]
            self.y_exp_f_y[ii] = proto_msg.signals[1].horizon[0]
            self.y_exp_f_z[ii] = proto_msg.signals[2].horizon[0]
            self.y_exp_omega_x[ii] = proto_msg.signals[3].horizon[0]
            self.y_exp_omega_y[ii] = proto_msg.signals[4].horizon[0]
            self.y_exp_omega_z[ii] = proto_msg.signals[5].horizon[0]

            ii += 1

    def __read_state_expected_plan(self, reader: mcap.reader.McapReader, stats: mcap.records.Statistics) -> None:

        ii = 0
        t_start = 0
        self.x = numpy.array((0, 0), dtype=float)
        num_samples = 0
        num_signals = 0

        for _, channel, message, proto_msg in reader.iter_decoded_messages(topics=["/control/state_plan"]):

            if ii == 0:
                num_samples = stats.channel_message_counts.get(channel.id)  # pyright: ignore[reportOptionalMemberAccess]
                num_signals = len(proto_msg.signals)
                assert type(num_samples) is int
                assert type(num_signals) is int

                t_start = message.log_time / 1e9
                self.t.resize((num_samples))
                self.x.resize((num_signals, num_samples))

            self.t[ii] = message.log_time / 1e9 - t_start
            for oo in range(num_signals):
                self.x[oo][ii] = proto_msg.signals[oo].horizon[0]

            ii += 1
