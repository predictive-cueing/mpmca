# Python code generation

This python project allows you to generate the required c++ files for your own simulator.
A in-depth tutorial will follow later (let us know if you need it!).

## Running the code generator

To have all the dependencies in place, make sure you run this from within the provided mpmca-dev container.

Navigate to the `python` folder (where this `README.md` is located) and run `python3 -m generate`.