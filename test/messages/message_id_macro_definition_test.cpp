/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/messages/message_id_macro_definition.hpp"

#include <gtest/gtest.h>

constexpr decltype(auto) GetNameAuto()
{
    return "hallo";
}

struct foo
{
    static constexpr const char* foo_a = "a";
    static constexpr const char* foo_string = "boo";
    static constexpr const char* GetFooString() { return "boo"; };
};

static constexpr const char* global_l = "l";

class MessageIdMacroDefinitionTest : public ::testing::Test
{
  protected:
    MessageIdMacroDefinitionTest() {}
};

TEST_F(MessageIdMacroDefinitionTest, CrcTest)
{
    ASSERT_EQ(2136000204, mpmca::messages::identification::GetCrc32Recursive("hallo"));
    ASSERT_EQ(2136000204, mpmca::messages::identification::GetCrc32Recursive(GetNameAuto()));
    ASSERT_EQ(2136000204, mpmca::messages::identification::GetCrc32Recursive("hal", "lo"));
    ASSERT_EQ(2136000204, mpmca::messages::identification::GetCrc32Recursive("ha", "l", "lo"));
    ASSERT_EQ(2136000204, mpmca::messages::identification::GetCrc32Recursive("ha", "l", "lo", ""));
    ASSERT_EQ(2136000204, mpmca::messages::identification::GetCrc32Recursive("h", foo::foo_a, global_l, "lo"));
    ASSERT_EQ(3804703442, mpmca::messages::identification::GetCrc32Recursive("h", foo::foo_a, global_l, "lo", 5));
}
