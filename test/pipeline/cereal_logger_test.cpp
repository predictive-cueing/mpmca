/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/cereal_logger.hpp"

#include <gtest/gtest.h>

#include "mpmca/pipeline/cereal_bus_base_message_logger.hpp"
#include "mpmca/pipeline/cereal_reader.hpp"

namespace mpmca {

struct SineWaveMessage
{
    DEFINE_MPMCA_PIPELINE_MESSAGE_ID("SineWaveMessage");
    int value;

    template <class Archive>
    void serialize(Archive& archive)
    {
        archive(value);
    }
};

template <std::size_t N>
struct SineWaveNumberMessage
{
    DEFINE_MPMCA_PIPELINE_MESSAGE_ID("SineWaveNumberMessage", N);
    int value;

    template <class Archive>
    void serialize(Archive& archive)
    {
        archive(value);
    }
};

struct SineWaveHorizonMessage
{
    DEFINE_MPMCA_PIPELINE_MESSAGE_ID("SineWaveHorizonMessage");
    std::vector<int> values;

    SineWaveHorizonMessage(size_t horizon_length) { values.resize(horizon_length, 0); }

    template <class Archive>
    void serialize(Archive& archive)
    {
        archive(values);
    }
};

std::vector<char> ReadFileToCharVector(const std::string& filename)
{
    std::ifstream file_reader(filename, std::ios::binary | std::ios::ate);
    std::streamsize size = file_reader.tellg();
    file_reader.seekg(0, std::ios::beg);

    std::vector<char> buffer(size);
    file_reader.read(buffer.data(), size);
    return buffer;
}

void PrintBinaryToHex(const std::vector<char> buffer)
{
    for (size_t i = 0; i < buffer.size(); ++i) {
        int z = buffer[i] & 0xff;
        if (i % 2 == 0 && i != 0)
            std::cout << " ";

        if (i % 8 == 0 && i != 0)
            std::cout << std::endl;

        std::cout << std::hex << std::setfill('0') << std::setw(2) << std::nouppercase << z;
    }
    std::cout << std::dec << std::endl;
}

}  //namespace mpmca

namespace mpmca::pipeline::testing {
class CerealLoggerTest : public ::testing::Test
{
  protected:
    CerealLoggerTest() {}
};

TEST_F(CerealLoggerTest, BinaryPrinterTest)
{
    std::vector<char> buf;
    buf.push_back(0);
    buf.push_back(1);
    buf.push_back((char)254);
}

TEST_F(CerealLoggerTest, LogSimpleMessageTest)
{
    std::string filename("out.cereal");

    {
        CerealLogger logger(filename);
        SineWaveMessage message;
        message.value = 10;
        logger.WriteMessage(0, message);

        message.value = 20;
        logger.WriteMessage(1, message);

        message.value = 30;
        logger.WriteMessage(2, message);
    }

    {
        CerealReader reader(filename);

        SineWaveMessage message;

        reader.UpdateMessage(message, 0);
        EXPECT_EQ(10, message.value);

        reader.UpdateMessage(message, 1);
        EXPECT_EQ(20, message.value);

        reader.UpdateMessage(message, 2);
        EXPECT_EQ(30, message.value);

        EXPECT_THROW(reader.UpdateMessage(message, 3), std::runtime_error);

        SineWaveHorizonMessage message_2(10);
        EXPECT_THROW(reader.UpdateMessage(message_2, 2), std::runtime_error);
    }
}

TEST_F(CerealLoggerTest, LogVariableLengthMessageTest)
{
    std::string filename("out.cereal");

    {
        CerealLogger logger(filename);
        {
            SineWaveHorizonMessage message(10);
            std::fill(message.values.begin(), message.values.end(), 10);
            logger.WriteMessage(0, message);
        }

        {
            SineWaveHorizonMessage message(20);
            std::fill(message.values.begin(), message.values.end(), 20);
            logger.WriteMessage(1, message);
        }

        {
            SineWaveHorizonMessage message(30);
            std::fill(message.values.begin(), message.values.end(), 30);
            logger.WriteMessage(2, message);
        }
    }

    {
        CerealReader reader(filename);

        auto timestamps = reader.GetSetOfValidTimestamps<SineWaveHorizonMessage>();

        EXPECT_EQ(1, timestamps.count(0));
        EXPECT_EQ(1, timestamps.count(1));
        EXPECT_EQ(1, timestamps.count(2));
        EXPECT_EQ(0, timestamps.count(3));
        EXPECT_EQ(0, timestamps.count(4));
        EXPECT_EQ(0, timestamps.count(5));

        SineWaveHorizonMessage message(3);
        EXPECT_EQ(3, message.values.size());

        reader.UpdateMessage(message, 0);
        EXPECT_EQ(10, message.values.size());
        EXPECT_EQ(10, message.values.at(0));
        EXPECT_EQ(10, message.values.at(9));

        reader.UpdateMessage(message, 1);
        EXPECT_EQ(20, message.values.size());
        EXPECT_EQ(20, message.values.at(0));
        EXPECT_EQ(20, message.values.at(19));

        reader.UpdateMessage(message, 2);
        EXPECT_EQ(30, message.values.size());
        EXPECT_EQ(30, message.values.at(0));
        EXPECT_EQ(30, message.values.at(29));

        EXPECT_THROW(reader.UpdateMessage(message, 3), std::runtime_error);

        SineWaveMessage message_2;
        EXPECT_THROW(reader.UpdateMessage(message_2, 2), std::runtime_error);
    }
}
TEST_F(CerealLoggerTest, IncludeMessageTypeTest)
{
    {
        CerealBusBaseMessageLogger logger("test.cereal", false);
        logger.AddMessageTypeToInclude(SineWaveMessage::message_type_id);

        EXPECT_TRUE(logger.IsMessageToLog<SineWaveMessage>());
        EXPECT_FALSE(logger.IsMessageToLog<SineWaveHorizonMessage>());

        MessageBus message_bus;
        message_bus.MakeMessage<SineWaveMessage>();
        message_bus.MakeMessage<SineWaveHorizonMessage>(20);

        logger.WriteBusBaseMessages(0, message_bus.GetBusBaseMessages());
    }

    {
        CerealReader reader("test.cereal");
        EXPECT_TRUE(reader.HasMessageType<SineWaveMessage>());
        EXPECT_FALSE(reader.HasMessageType<SineWaveHorizonMessage>());
        EXPECT_TRUE(reader.HasMessageType(SineWaveMessage::message_type_id));
        EXPECT_FALSE(reader.HasMessageType(SineWaveHorizonMessage::message_type_id));

        EXPECT_TRUE(reader.HasMessageOfTypeAtTime<SineWaveMessage>(0));
        EXPECT_FALSE(reader.HasMessageOfTypeAtTime<SineWaveMessage>(1));
        EXPECT_FALSE(reader.HasMessageOfTypeAtTime<SineWaveHorizonMessage>(0));
        EXPECT_FALSE(reader.HasMessageOfTypeAtTime<SineWaveHorizonMessage>(1));
    }
}

TEST_F(CerealLoggerTest, ExcludeMessageTypeTest)
{
    {
        CerealBusBaseMessageLogger logger("test.cereal", true);
        logger.AddMessageTypeToExclude(SineWaveHorizonMessage::message_type_id);

        EXPECT_TRUE(logger.IsMessageToLog<SineWaveMessage>());
        EXPECT_FALSE(logger.IsMessageToLog<SineWaveHorizonMessage>());

        MessageBus message_bus;
        message_bus.MakeMessage<SineWaveMessage>();
        message_bus.MakeMessage<SineWaveHorizonMessage>(20);

        logger.WriteBusBaseMessages(0, message_bus.GetBusBaseMessages());
    }

    {
        CerealReader reader("test.cereal");
        EXPECT_TRUE(reader.HasMessageType<SineWaveMessage>());
        EXPECT_FALSE(reader.HasMessageType<SineWaveHorizonMessage>());
        EXPECT_TRUE(reader.HasMessageType(SineWaveMessage::message_type_id));
        EXPECT_FALSE(reader.HasMessageType(SineWaveHorizonMessage::message_type_id));

        EXPECT_TRUE(reader.HasMessageOfTypeAtTime<SineWaveMessage>(0));
        EXPECT_FALSE(reader.HasMessageOfTypeAtTime<SineWaveMessage>(1));
        EXPECT_FALSE(reader.HasMessageOfTypeAtTime<SineWaveHorizonMessage>(0));
        EXPECT_FALSE(reader.HasMessageOfTypeAtTime<SineWaveHorizonMessage>(1));
    }
}

TEST_F(CerealLoggerTest, ExcludeMessageNameTest)
{
    CerealBusBaseMessageLogger logger("test.cereal", false);
    logger.AddMessageNameToInclude("SineWaveMessage");
    logger.AddMessageNameToInclude("SineWaveNumberMessage3");
    logger.AddMessageNameToInclude("SineWaveNumberMessage10");

    EXPECT_TRUE(logger.IsMessageToLog<SineWaveMessage>());
    EXPECT_TRUE(logger.IsMessageToLog<SineWaveNumberMessage<3>>());
    EXPECT_TRUE(logger.IsMessageToLog<SineWaveNumberMessage<10>>());
    EXPECT_FALSE(logger.IsMessageToLog<SineWaveNumberMessage<1>>());
    EXPECT_FALSE(logger.IsMessageToLog<SineWaveNumberMessage<2>>());
    EXPECT_FALSE(logger.IsMessageToLog<SineWaveNumberMessage<5>>());
}

TEST_F(CerealLoggerTest, IncludeMessageNameTest)
{
    CerealBusBaseMessageLogger logger("test.cereal", true);
    logger.AddMessageNameToExclude("SineWaveMessage");
    logger.AddMessageNameToExclude("SineWaveNumberMessage3");
    logger.AddMessageNameToExclude("SineWaveNumberMessage10");

    EXPECT_FALSE(logger.IsMessageToLog<SineWaveMessage>());
    EXPECT_FALSE(logger.IsMessageToLog<SineWaveNumberMessage<3>>());
    EXPECT_FALSE(logger.IsMessageToLog<SineWaveNumberMessage<10>>());
    EXPECT_TRUE(logger.IsMessageToLog<SineWaveNumberMessage<1>>());
    EXPECT_TRUE(logger.IsMessageToLog<SineWaveNumberMessage<2>>());
    EXPECT_TRUE(logger.IsMessageToLog<SineWaveNumberMessage<5>>());
}
}  //namespace mpmca::pipeline::testing