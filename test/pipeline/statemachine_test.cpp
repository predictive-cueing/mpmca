/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <gtest/gtest.h>

#include "mpmca/pipeline/state_machine.hpp"
namespace mpmca::pipeline {
namespace testing {
class StateMachineTest : public ::testing::Test
{
  protected:
    StateMachineTest() {}

  public:
    static void CheckAllGotoStatesFail(std::vector<StateMachineClient*> clients, StateMachine& machine,
                                       std::vector<State> states)
    {
        for (auto& client : clients) {
            for (auto s : states) {
                client->SetGoToState(s);
                EXPECT_THROW(machine.MainTick(), std::runtime_error) << "No error was thrown for state " << s;
            }
            client->SetGoToState(State::kNone);
        }
    }
};

TEST_F(StateMachineTest, OutputMermaidTest)
{
    StateMachineTester machine;

    std::cout << machine.GetMermaidDiagram() << std::endl;
}

TEST_F(StateMachineTest, CompileTest)
{
    StateMachineTester machine;

    auto client_a = machine.MakeClient("Client A");
    auto client_b = machine.MakeClient("Client B");
    auto client_c = machine.MakeClient("Client C");

    for (int N = 0; N < 10; ++N) {
        if (N == 0) {
            for (int i = 0; i < 10; ++i) {
                EXPECT_EQ(State::kBooting, client_a.GetCurrentState());
                EXPECT_EQ(State::kBooting, client_b.GetCurrentState());
                EXPECT_EQ(State::kBooting, client_c.GetCurrentState());
                machine.MainTick();
            }

            client_a.SetGoToState(State::kBooted);
            client_b.SetGoToState(State::kBooted);
            client_c.SetGoToState(State::kBooted);
            machine.MainTick();
        }

        for (int i = 0; i < 10; ++i) {
            EXPECT_EQ(State::kIdle, client_a.GetCurrentState());
            EXPECT_EQ(State::kIdle, client_b.GetCurrentState());
            EXPECT_EQ(State::kIdle, client_c.GetCurrentState());
            machine.MainTick();
        }

        client_a.SetGoToState(State::kPreparing);

        EXPECT_EQ(State::kIdle, client_a.GetCurrentState());
        EXPECT_EQ(State::kIdle, client_b.GetCurrentState());
        EXPECT_EQ(State::kIdle, client_c.GetCurrentState());

        for (int i = 0; i < 10; ++i) {
            machine.MainTick();
            EXPECT_EQ(State::kPreparing, client_a.GetCurrentState());
            EXPECT_EQ(State::kPreparing, client_b.GetCurrentState());
            EXPECT_EQ(State::kPreparing, client_c.GetCurrentState());
        }

        client_a.SetGoToState(State::kPrepared);
        client_b.SetGoToState(State::kPrepared);
        client_c.SetGoToState(State::kPrepared);
        machine.MainTick();

        EXPECT_EQ(State::kReady, client_a.GetCurrentState());
        EXPECT_EQ(State::kReady, client_b.GetCurrentState());
        EXPECT_EQ(State::kReady, client_c.GetCurrentState());

        client_a.SetGoToState(State::kStarting);

        EXPECT_EQ(State::kReady, client_a.GetCurrentState());
        EXPECT_EQ(State::kReady, client_b.GetCurrentState());
        EXPECT_EQ(State::kReady, client_c.GetCurrentState());

        for (int i = 0; i < 10; ++i) {
            machine.MainTick();
            EXPECT_EQ(State::kStarting, client_a.GetCurrentState());
            EXPECT_EQ(State::kStarting, client_b.GetCurrentState());
            EXPECT_EQ(State::kStarting, client_c.GetCurrentState());
        }

        client_a.SetGoToState(State::kStarted);
        EXPECT_EQ(3, client_a.GetNumberClientsNotInState(State::kStarted));

        machine.MainTick();

        EXPECT_EQ(2, client_a.GetNumberClientsNotInState(State::kStarted));

        // State::kFailing is not in this list, because we can always go to Failing
        CheckAllGotoStatesFail(
            {&client_a, &client_b, &client_c}, machine,
            {State::kIdle, State::kPause, State::kPausing, State::kResumed, State::kRun, State::kStarting,
             State::kStopped, State::kStopping, State::kResuming, State::kPreparing, State::kPrepared, State::kReady,
             State::kFail, State::kFailed, State::kRecovering, State::kRecovered});

        client_b.SetGoToState(State::kStarted);

        machine.MainTick();
        EXPECT_EQ(1, client_a.GetNumberClientsNotInState(State::kStarted));

        EXPECT_EQ(State::kStarting, machine.GetCurrentState());

        CheckAllGotoStatesFail(
            {&client_a, &client_b, &client_c}, machine,
            {State::kIdle, State::kPause, State::kPausing, State::kResumed, State::kRun, State::kStarting,
             State::kStopped, State::kStopping, State::kResuming, State::kPreparing, State::kPrepared, State::kReady,
             State::kFail, State::kFailed, State::kRecovering, State::kRecovered});

        client_a.SetGoToState(State::kStarted);
        client_c.SetGoToState(State::kStarted);

        EXPECT_EQ(1, client_a.GetNumberClientsNotInState(State::kStarted));

        for (int i = 0; i < 10; ++i) {
            machine.MainTick();
            EXPECT_EQ(3, client_a.GetNumberClientsNotInState(State::kStarted));
            EXPECT_EQ(0, client_a.GetNumberClientsNotInState(State::kRun));
            EXPECT_EQ(State::kRun, machine.GetCurrentState());
            EXPECT_EQ(State::kRun, client_a.GetCurrentState());
            EXPECT_EQ(State::kRun, client_b.GetCurrentState());
            EXPECT_EQ(State::kRun, client_c.GetCurrentState());
        }

        client_a.SetGoToState(State::kPausing);
        client_b.SetGoToState(State::kStopping);
        EXPECT_THROW(machine.MainTick(), std::runtime_error);
        client_b.SetGoToState(State::kNone);

        for (int i = 0; i < 10; ++i) {
            machine.MainTick();
            EXPECT_EQ(State::kPausing, machine.GetCurrentState());
            EXPECT_EQ(State::kPausing, client_a.GetCurrentState());
            EXPECT_EQ(State::kPausing, client_b.GetCurrentState());
            EXPECT_EQ(State::kPausing, client_c.GetCurrentState());
        }

        client_a.SetGoToState(State::kPaused);
        client_b.SetGoToState(State::kPaused);
        client_c.SetGoToState(State::kPaused);
        machine.MainTick();

        for (int i = 0; i < 10; ++i) {
            machine.MainTick();
            EXPECT_EQ(State::kPause, machine.GetCurrentState());
            EXPECT_EQ(State::kPause, client_a.GetCurrentState());
            EXPECT_EQ(State::kPause, client_b.GetCurrentState());
            EXPECT_EQ(State::kPause, client_c.GetCurrentState());
        }

        CheckAllGotoStatesFail({&client_a, &client_b, &client_c}, machine,
                               {State::kIdle, State::kPause, State::kPausing, State::kResumed, State::kRun,
                                State::kStarting, State::kStarted, State::kStopped, State::kPreparing, State::kPrepared,
                                State::kReady, State::kFail, State::kFailed, State::kRecovering, State::kRecovered});

        client_a.SetGoToState(State::kResuming);
        client_b.SetGoToState(State::kResuming);

        for (int i = 0; i < 10; ++i) {
            machine.MainTick();
            EXPECT_EQ(State::kResuming, machine.GetCurrentState());
            EXPECT_EQ(State::kResuming, client_a.GetCurrentState());
            EXPECT_EQ(State::kResuming, client_b.GetCurrentState());
            EXPECT_EQ(State::kResuming, client_c.GetCurrentState());
        }

        CheckAllGotoStatesFail(
            {&client_a, &client_b, &client_c}, machine,
            {State::kIdle, State::kPause, State::kPausing, State::kResuming, State::kRun, State::kStarting,
             State::kStarted, State::kStopped, State::kStopping, State::kPreparing, State::kPrepared, State::kReady,
             State::kFail, State::kFailed, State::kRecovering, State::kRecovered});

        client_a.SetGoToState(State::kResumed);
        client_b.SetGoToState(State::kResumed);
        client_c.SetGoToState(State::kResumed);

        for (int i = 0; i < 10; ++i) {
            machine.MainTick();
            EXPECT_EQ(State::kRun, machine.GetCurrentState());
            EXPECT_EQ(State::kRun, client_a.GetCurrentState());
            EXPECT_EQ(State::kRun, client_b.GetCurrentState());
            EXPECT_EQ(State::kRun, client_c.GetCurrentState());
        }

        client_c.SetGoToState(State::kStopping);
        for (int i = 0; i < 10; ++i) {
            machine.MainTick();
            EXPECT_EQ(State::kStopping, machine.GetCurrentState());
            EXPECT_EQ(State::kStopping, client_a.GetCurrentState());
            EXPECT_EQ(State::kStopping, client_b.GetCurrentState());
            EXPECT_EQ(State::kStopping, client_c.GetCurrentState());
        }

        CheckAllGotoStatesFail(
            {&client_a, &client_b, &client_c}, machine,
            {State::kIdle, State::kPause, State::kPausing, State::kPaused, State::kResuming, State::kResumed,
             State::kRun, State::kStarting, State::kStarted, State::kStopping, State::kPreparing, State::kPrepared,
             State::kReady, State::kFail, State::kFailed, State::kRecovering, State::kRecovered});

        client_a.SetGoToState(State::kStopped);
        client_b.SetGoToState(State::kStopped);
        client_c.SetGoToState(State::kStopped);

        for (int i = 0; i < 10; ++i) {
            machine.MainTick();
            EXPECT_EQ(State::kIdle, machine.GetCurrentState());
            EXPECT_EQ(State::kIdle, client_a.GetCurrentState());
            EXPECT_EQ(State::kIdle, client_b.GetCurrentState());
            EXPECT_EQ(State::kIdle, client_c.GetCurrentState());
        }

        CheckAllGotoStatesFail({&client_a, &client_b, &client_c}, machine,
                               {State::kIdle, State::kPause, State::kPausing, State::kPaused, State::kResuming,
                                State::kResumed, State::kRun, State::kStarted, State::kStopping, State::kStopped,
                                State::kReady, State::kFail, State::kFailed, State::kRecovering, State::kRecovered});
    }

    client_a.SetGoToState(State::kTerminating);

    machine.MainTick();
    EXPECT_EQ(State::kTerminating, machine.GetCurrentState());
    EXPECT_EQ(State::kTerminating, client_a.GetCurrentState());
    EXPECT_EQ(State::kTerminating, client_b.GetCurrentState());
    EXPECT_EQ(State::kTerminating, client_c.GetCurrentState());

    client_a.SetGoToState(State::kTerminated);
    client_b.SetGoToState(State::kTerminated);
    client_c.SetGoToState(State::kTerminated);
    machine.MainTick();

    EXPECT_EQ(State::kTerminate, machine.GetCurrentState());
    EXPECT_EQ(State::kTerminate, client_a.GetCurrentState());
    EXPECT_EQ(State::kTerminate, client_b.GetCurrentState());
    EXPECT_EQ(State::kTerminate, client_c.GetCurrentState());

    CheckAllGotoStatesFail(
        {&client_a, &client_b, &client_c}, machine,
        {State::kIdle, State::kPreparing, State::kPrepared, State::kReady, State::kPause, State::kPausing,
         State::kPaused, State::kResuming, State::kResumed, State::kRun, State::kStarting, State::kStarted,
         State::kStopping, State::kStopped, State::kFail, State::kFailed, State::kRecovering, State::kRecovered});
}

TEST_F(StateMachineTest, FailTest)
{
    StateMachineTester machine;

    auto client_a = machine.MakeClient("Client A");
    auto client_b = machine.MakeClient("Client B");
    auto client_c = machine.MakeClient("Client C");

    for (int i = 0; i < 10; ++i) {
        EXPECT_EQ(State::kBooting, client_a.GetCurrentState());
        EXPECT_EQ(State::kBooting, client_b.GetCurrentState());
        EXPECT_EQ(State::kBooting, client_c.GetCurrentState());
        machine.MainTick();
    }

    client_a.SetGoToState(State::kBooted);
    client_b.SetGoToState(State::kBooted);
    client_c.SetGoToState(State::kBooted);
    machine.MainTick();

    for (int i = 0; i < 10; ++i) {
        EXPECT_EQ(State::kIdle, client_a.GetCurrentState());
        EXPECT_EQ(State::kIdle, client_b.GetCurrentState());
        EXPECT_EQ(State::kIdle, client_c.GetCurrentState());
        machine.MainTick();
    }

    client_a.SetGoToState(State::kPreparing);

    machine.MainTick();
    EXPECT_EQ(State::kPreparing, client_a.GetCurrentState());
    EXPECT_EQ(State::kPreparing, client_b.GetCurrentState());
    EXPECT_EQ(State::kPreparing, client_c.GetCurrentState());

    client_b.SetGoToState(State::kPrepared);
    machine.MainTick();
    EXPECT_EQ(State::kPreparing, client_a.GetCurrentState());
    EXPECT_EQ(State::kPrepared, client_b.GetCurrentState());
    EXPECT_EQ(State::kPreparing, client_c.GetCurrentState());

    client_c.SetGoToState(State::kFailing);
    machine.MainTick();
    EXPECT_EQ(State::kFailing, machine.GetCurrentState());
    EXPECT_EQ(State::kFailing, client_a.GetCurrentState());
    EXPECT_EQ(State::kFailing, client_b.GetCurrentState());
    EXPECT_EQ(State::kFailing, client_c.GetCurrentState());

    client_c.SetGoToState(State::kFailed);
    machine.MainTick();
    EXPECT_EQ(State::kFailing, machine.GetCurrentState());
    EXPECT_EQ(State::kFailing, client_a.GetCurrentState());
    EXPECT_EQ(State::kFailing, client_b.GetCurrentState());
    EXPECT_EQ(State::kFailed, client_c.GetCurrentState());

    client_a.SetGoToState(State::kFailed);
    client_b.SetGoToState(State::kFailed);
    machine.MainTick();
    EXPECT_EQ(State::kFail, machine.GetCurrentState());
    EXPECT_EQ(State::kFail, client_a.GetCurrentState());
    EXPECT_EQ(State::kFail, client_b.GetCurrentState());
    EXPECT_EQ(State::kFail, client_c.GetCurrentState());

    client_c.SetGoToState(State::kRecovering);
    machine.MainTick();
    EXPECT_EQ(State::kRecovering, machine.GetCurrentState());
    EXPECT_EQ(State::kRecovering, client_a.GetCurrentState());
    EXPECT_EQ(State::kRecovering, client_b.GetCurrentState());
    EXPECT_EQ(State::kRecovering, client_c.GetCurrentState());

    client_a.SetGoToState(State::kRecovered);
    client_b.SetGoToState(State::kRecovered);
    client_c.SetGoToState(State::kRecovered);

    machine.MainTick();
    EXPECT_EQ(State::kIdle, machine.GetCurrentState());
    EXPECT_EQ(State::kIdle, client_a.GetCurrentState());
    EXPECT_EQ(State::kIdle, client_b.GetCurrentState());
    EXPECT_EQ(State::kIdle, client_c.GetCurrentState());
}

TEST_F(StateMachineTest, FailToTerminateTest)
{
    StateMachineTester machine;

    auto client_a = machine.MakeClient("Client A");
    auto client_b = machine.MakeClient("Client B");
    auto client_c = machine.MakeClient("Client C");

    for (int i = 0; i < 10; ++i) {
        EXPECT_EQ(State::kBooting, client_a.GetCurrentState());
        EXPECT_EQ(State::kBooting, client_b.GetCurrentState());
        EXPECT_EQ(State::kBooting, client_c.GetCurrentState());
        machine.MainTick();
    }

    client_a.SetGoToState(State::kBooted);
    client_b.SetGoToState(State::kBooted);
    client_c.SetGoToState(State::kBooted);
    machine.MainTick();

    for (int i = 0; i < 10; ++i) {
        EXPECT_EQ(State::kIdle, client_a.GetCurrentState());
        EXPECT_EQ(State::kIdle, client_b.GetCurrentState());
        EXPECT_EQ(State::kIdle, client_c.GetCurrentState());
        machine.MainTick();
    }

    client_a.SetGoToState(State::kPreparing);

    machine.MainTick();
    EXPECT_EQ(State::kPreparing, client_a.GetCurrentState());
    EXPECT_EQ(State::kPreparing, client_b.GetCurrentState());
    EXPECT_EQ(State::kPreparing, client_c.GetCurrentState());

    client_b.SetGoToState(State::kPrepared);
    machine.MainTick();
    EXPECT_EQ(State::kPreparing, client_a.GetCurrentState());
    EXPECT_EQ(State::kPrepared, client_b.GetCurrentState());
    EXPECT_EQ(State::kPreparing, client_c.GetCurrentState());

    client_c.SetGoToState(State::kFailing);
    machine.MainTick();
    EXPECT_EQ(State::kFailing, machine.GetCurrentState());
    EXPECT_EQ(State::kFailing, client_a.GetCurrentState());
    EXPECT_EQ(State::kFailing, client_b.GetCurrentState());
    EXPECT_EQ(State::kFailing, client_c.GetCurrentState());

    client_c.SetGoToState(State::kFailed);
    machine.MainTick();
    EXPECT_EQ(State::kFailing, machine.GetCurrentState());
    EXPECT_EQ(State::kFailing, client_a.GetCurrentState());
    EXPECT_EQ(State::kFailing, client_b.GetCurrentState());
    EXPECT_EQ(State::kFailed, client_c.GetCurrentState());

    client_a.SetGoToState(State::kFailed);
    client_b.SetGoToState(State::kFailed);
    machine.MainTick();
    EXPECT_EQ(State::kFail, machine.GetCurrentState());
    EXPECT_EQ(State::kFail, client_a.GetCurrentState());
    EXPECT_EQ(State::kFail, client_b.GetCurrentState());
    EXPECT_EQ(State::kFail, client_c.GetCurrentState());

    client_a.SetGoToState(State::kFailTerminating);

    machine.MainTick();
    EXPECT_EQ(State::kFailTerminating, machine.GetCurrentState());
    EXPECT_EQ(State::kFailTerminating, client_a.GetCurrentState());
    EXPECT_EQ(State::kFailTerminating, client_b.GetCurrentState());
    EXPECT_EQ(State::kFailTerminating, client_c.GetCurrentState());

    client_a.SetGoToState(State::kFailTerminated);
    client_b.SetGoToState(State::kFailTerminated);
    client_c.SetGoToState(State::kFailTerminated);
    machine.MainTick();

    EXPECT_EQ(State::kFailTerminate, machine.GetCurrentState());
    EXPECT_EQ(State::kFailTerminate, client_a.GetCurrentState());
    EXPECT_EQ(State::kFailTerminate, client_b.GetCurrentState());
    EXPECT_EQ(State::kFailTerminate, client_c.GetCurrentState());
}

TEST_F(StateMachineTest, MultipleClientsRequestSameTransitionStateTest)
{
    StateMachineTester machine;

    auto client_a = machine.MakeClient("Client A");
    auto client_b = machine.MakeClient("Client B");

    EXPECT_EQ(State::kBooting, client_a.GetCurrentState());
    EXPECT_EQ(State::kBooting, client_b.GetCurrentState());

    machine.MainTick();

    client_a.SetGoToState(State::kBooted);
    client_b.SetGoToState(State::kBooted);

    machine.MainTick();

    EXPECT_EQ(State::kIdle, client_a.GetCurrentState());
    EXPECT_EQ(State::kIdle, client_b.GetCurrentState());

    client_a.SetGoToState(State::kFailing);
    client_b.SetGoToState(State::kFailing);

    machine.MainTick();

    EXPECT_EQ(State::kFailing, client_a.GetCurrentState());
    EXPECT_EQ(State::kFailing, client_b.GetCurrentState());

    client_a.SetGoToState(State::kFailed);
    client_b.SetGoToState(State::kFailing);

    machine.MainTick();

    EXPECT_EQ(State::kFailing, client_a.GetCurrentState());
    EXPECT_EQ(State::kFailing, client_b.GetCurrentState());

    client_a.SetGoToState(State::kFailed);

    machine.MainTick();

    EXPECT_EQ(State::kFailed, client_a.GetCurrentState());
    EXPECT_EQ(State::kFailing, client_b.GetCurrentState());

    client_b.SetGoToState(State::kFailed);

    machine.MainTick();

    EXPECT_EQ(State::kFail, client_a.GetCurrentState());
    EXPECT_EQ(State::kFail, client_b.GetCurrentState());

    client_b.SetGoToState(State::kRecovering);

    machine.MainTick();

    EXPECT_EQ(State::kRecovering, client_a.GetCurrentState());
    EXPECT_EQ(State::kRecovering, client_b.GetCurrentState());

    client_a.SetGoToState(State::kRecovered);
    client_b.SetGoToState(State::kRecovered);

    machine.MainTick();

    EXPECT_EQ(State::kIdle, client_a.GetCurrentState());
    EXPECT_EQ(State::kIdle, client_b.GetCurrentState());

    client_a.SetGoToState(State::kPreparing);
    client_b.SetGoToState(State::kPreparing);

    machine.MainTick();

    EXPECT_EQ(State::kPreparing, client_a.GetCurrentState());
    EXPECT_EQ(State::kPreparing, client_b.GetCurrentState());

    client_a.SetGoToState(State::kPrepared);
    client_b.SetGoToState(State::kPrepared);

    machine.MainTick();

    EXPECT_EQ(State::kReady, client_a.GetCurrentState());
    EXPECT_EQ(State::kReady, client_b.GetCurrentState());

    client_a.SetGoToState(State::kStarting);
    machine.MainTick();

    EXPECT_EQ(State::kStarting, client_a.GetCurrentState());
    EXPECT_EQ(State::kStarting, client_b.GetCurrentState());

    client_a.SetGoToState(State::kStarted);
    client_b.SetGoToState(State::kStarted);

    machine.MainTick();

    EXPECT_EQ(State::kRun, client_a.GetCurrentState());
    EXPECT_EQ(State::kRun, client_b.GetCurrentState());

    // two state transitions that are valid, as long as they do not happen at the same time.
    client_a.SetGoToState(State::kPausing);
    client_b.SetGoToState(State::kStopping);

    EXPECT_THROW(machine.MainTick(), std::runtime_error);
}
}  //namespace testing
}  //namespace mpmca::pipeline