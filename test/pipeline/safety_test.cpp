/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/safety.hpp"

#include <gtest/gtest.h>

namespace mpmca::pipeline::testing {

class SafetyTest : public ::testing::Test
{
  protected:
    SafetyTest() {}
};

TEST_F(SafetyTest, IntegerTest)
{
    Safety a = Safety::DoSomethingUnsafe("a", false);
    EXPECT_TRUE(a.GetLevelName().compare("UNPREPARED") == 0);
    a.Prepared();
    EXPECT_TRUE(a.GetLevelName().compare("SAFE") == 0);
    a.Fail();
    EXPECT_TRUE(a.GetLevelName().compare("FAIL") == 0);
    a.Critical();
    EXPECT_TRUE(a.GetLevelName().compare("CRITICAL") == 0);
}

TEST_F(SafetyTest, CreateOneParentTest)
{
    Safety a = Safety::DoSomethingUnsafe("a", false);

    Safety b = a.MakeChild("b");
    auto c = b.MakeChild("c");
    auto d = b.MakeChild("d");
    auto e = d.MakeChild("e");
    auto f = d.MakeChild("f");
    auto g = f.MakeChild("g");
    auto h = c.MakeChild("h");

    {
        auto k = h.MakeChild("k");
        auto m = k.MakeChild("l");
    }

    a.ListChildren();
}

TEST_F(SafetyTest, CreateChildTest)
{
    Safety a = Safety::DoSomethingUnsafe("a", false);
    Safety b = a.MakeChild("b");
    auto c = b.MakeChild("c");

    EXPECT_TRUE(a.GetName().compare("a") == 0);
    EXPECT_TRUE(b.GetName().compare("b") == 0);
    EXPECT_TRUE(c.GetName().compare("c") == 0);
    EXPECT_FALSE(a.HasParent());
}

TEST_F(SafetyTest, PrepareTest1)
{
    Safety a = Safety::DoSomethingUnsafe("a", false);
    Safety b = a.MakeChild("b");

    {
        auto c = b.MakeChild("d");
    }

    EXPECT_TRUE(a.GetLevel() == SafetyLevel::kUnprepared);
    EXPECT_TRUE(b.GetLevel() == SafetyLevel::kUnprepared);

    b.Prepared();

    EXPECT_TRUE(a.GetLevel() == SafetyLevel::kUnprepared);
    EXPECT_TRUE(b.GetLevel() == SafetyLevel::kSafe);

    a.Prepared();

    EXPECT_TRUE(a.GetLevel() == SafetyLevel::kSafe);
    EXPECT_TRUE(b.GetLevel() == SafetyLevel::kSafe);
}

TEST_F(SafetyTest, PrepareTest2)
{
    Safety a = Safety::DoSomethingUnsafe("a", false);
    Safety b = a.MakeChild("b");

    EXPECT_TRUE(a.GetLevel() == SafetyLevel::kUnprepared);
    EXPECT_TRUE(b.GetLevel() == SafetyLevel::kUnprepared);

    a.Prepared();

    EXPECT_TRUE(a.GetLevel() == SafetyLevel::kUnprepared);
    EXPECT_TRUE(b.GetLevel() == SafetyLevel::kUnprepared);

    b.Prepared();

    EXPECT_TRUE(a.GetLevel() == SafetyLevel::kUnprepared);
    EXPECT_TRUE(b.GetLevel() == SafetyLevel::kSafe);

    a.Prepared();

    EXPECT_TRUE(a.GetLevel() == SafetyLevel::kSafe);
    EXPECT_TRUE(b.GetLevel() == SafetyLevel::kSafe);
}

TEST_F(SafetyTest, ExceptionTest)
{
    Safety a = Safety::DoSomethingUnsafe("a", false);
    Safety b1 = a.MakeChild("b1");
    Safety b2 = a.MakeChild("b2");
    Safety c = b1.MakeChild("b");

    EXPECT_TRUE(a.GetLevel() == SafetyLevel::kUnprepared);
    EXPECT_TRUE(b1.GetLevel() == SafetyLevel::kUnprepared);
    EXPECT_TRUE(b2.GetLevel() == SafetyLevel::kUnprepared);
    EXPECT_TRUE(c.GetLevel() == SafetyLevel::kUnprepared);

    c.Prepared();
    b1.Prepared();
    b2.Prepared();
    a.Prepared();

    EXPECT_TRUE(a.GetLevel() == SafetyLevel::kSafe);

    c.Fail();

    EXPECT_TRUE(a.GetLevel() == SafetyLevel::kFail);
    EXPECT_TRUE(b1.GetLevel() == SafetyLevel::kFail);
    EXPECT_TRUE(b2.GetLevel() == SafetyLevel::kFail);
    EXPECT_TRUE(c.GetLevel() == SafetyLevel::kFail);
}
/*
 */
}  //namespace mpmca::pipeline::testing