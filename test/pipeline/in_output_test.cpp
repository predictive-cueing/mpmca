/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/in_output.hpp"

#include <gtest/gtest.h>

#include "mpmca/pipeline/pipeline.hpp"

namespace mpmca::pipeline::testing {

class TestInOutput : public InOutput
{
  public:
    TestInOutput(Pipeline& pipeline, const std::string& name)
        : InOutput(pipeline, name, "TestInOutput")
    {
    }

    void Prepare() override { GetSafety().Prepared(); }
    void Tick(MessageBus& in_output_message_bus) override {};
    void SafeTick() override {};
    void TaskCompleted(const DataBucket& data_bucket) override {};
    void MainTick(DataBucket& data_bucket) override {};
};

class InOutputTest : public ::testing::Test
{
  private:
    Pipeline m_pipeline;

  protected:
    InOutputTest()
        : m_pipeline(Pipeline::Options{})
    {
    }

    Pipeline& GetPipeline() { return m_pipeline; }
};

TEST_F(InOutputTest, ConstructorTest)
{
    auto test = GetPipeline().AddInOutput<TestInOutput>("test-name");

    EXPECT_STREQ("TestInOutput", test->GetTypeName().c_str());
    EXPECT_STREQ("test-name", test->GetName().c_str());
    EXPECT_NO_THROW(test->GetSafety());
    EXPECT_NO_THROW(test->GetStateMachineClient());
}
TEST_F(InOutputTest, SetterGetterTest)
{
    auto test = GetPipeline().AddInOutput<TestInOutput>("test");
    EXPECT_EQ(0, test->GetExecutionOrder());

    test->SetMaximumComputationTimeUs(123);
    EXPECT_EQ(std::chrono::microseconds(123), test->GetMaximumComputationTimeUs());

    auto test2 = GetPipeline().AddInOutput<TestInOutput>("test2");
    EXPECT_EQ(1, test2->GetExecutionOrder());
}
}  //namespace mpmca::pipeline::testing