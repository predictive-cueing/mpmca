/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/task.hpp"

#include <gtest/gtest.h>

#include "mpmca/pipeline/pipeline.hpp"

namespace mpmca::pipeline::testing {

class TestStep : public Step
{
  public:
    TestStep(Task& task, const std::string& name)
        : Step(task, name, "TestStep")
    {
    }

    virtual void MainTick(DataBucket& data_bucket) override {};
};

class TaskTest : public ::testing::Test
{
  protected:
    TaskTest() {}
};

TEST_F(TaskTest, ConstructorTest)
{
    Pipeline::Options options;
    Pipeline test(options);

    Pipeline test2(Pipeline::Options{});

    test.GetPipelineReturnCode();
    test2.GetPipelineReturnCode();
}
}  //namespace mpmca::pipeline::testing