/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/message_bus.hpp"

#include <gtest/gtest.h>

#include "mpmca/messages/message_id_macro_definition.hpp"

namespace mpmca::pipeline::testing {

struct TestMessage
{
    DEFINE_MPMCA_PIPELINE_MESSAGE_ID("TestMessage");
    bool test_bool;
    template <class Archive>
    void serialize(Archive& archive)
    {
        archive(test_bool);
    }
};

struct FaultyTestMessage
{
    // this test message is faulty, because the string
    // given to the DEFINE_MPMCA_PIPELINE_MESSAGE_ID
    // is identical to the one for TestMessage.
    // Trying to add these two types to one MessageBus
    // will result in errors.
    DEFINE_MPMCA_PIPELINE_MESSAGE_ID("TestMessage");
    bool test_bool;
    template <class Archive>
    void serialize(Archive& archive)
    {
        archive(test_bool);
    }
};

class MessageBusTest : public ::testing::Test
{
  protected:
    MessageBusTest() {}
};

TEST_F(MessageBusTest, MakeAndDoMessagesTest)
{
    MessageBus bus;

    ASSERT_FALSE(bus.HasMessage<TestMessage>());
    ASSERT_NO_THROW(bus.MakeMessage<TestMessage>());
    ASSERT_TRUE(bus.HasMessage<TestMessage>());
    ASSERT_NO_THROW(bus.MakeMessage<TestMessage>());

    bus.UpdateMessage<TestMessage>([&](TestMessage& message) { message.test_bool = true; });
    bus.ReadUpdatedMessage<TestMessage>([&](const TestMessage& message) { ASSERT_TRUE(message.test_bool); });

    bus.UpdateMessage<TestMessage>([&](TestMessage& message) { message.test_bool = false; });
    bus.ReadUpdatedMessage<TestMessage>([&](const TestMessage& message) { ASSERT_FALSE(message.test_bool); });
}

TEST_F(MessageBusTest, FaultyMessageTest)
{
    MessageBus bus;

    ASSERT_FALSE(bus.HasMessage<TestMessage>());
    ASSERT_FALSE(bus.HasMessage<FaultyTestMessage>());
    bus.MakeMessage<TestMessage>();
    ASSERT_TRUE(bus.HasMessage<TestMessage>());

    ASSERT_THROW(bus.MakeMessage<FaultyTestMessage>(), std::runtime_error);

    ASSERT_THROW(bus.UpdateMessage<FaultyTestMessage>([&](FaultyTestMessage& message) { message.test_bool = true; }),
                 std::runtime_error);
}

}  //namespace mpmca::pipeline::testing