/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/step.hpp"

#include <gtest/gtest.h>

#include "mpmca/pipeline/pipeline.hpp"
#include "mpmca/pipeline/task.hpp"
#include "mpmca/utilities/horizon.hpp"
namespace mpmca::pipeline::testing {

class TestStep : public Step
{
  public:
    TestStep(Task& task, const std::string& name)
        : Step(task, name, "TestStep")
    {
    }
    void Prepare() override { GetSafety().Prepared(); }
    void MainTick(DataBucket& data_bucket) override {};
};

class StepTest : public ::testing::Test
{
  private:
    Pipeline m_pipeline;
    utilities::Horizon m_horizon;
    std::shared_ptr<Task> m_task_ptr;

  protected:
    StepTest()
        : m_pipeline(Pipeline::Options{})
        , m_horizon(10, 10)
    {
        m_task_ptr = m_pipeline.MakeTask("test-task", m_horizon);
    }

    Pipeline& GetPipeline() { return m_pipeline; }
    std::shared_ptr<Task>& GetTaskPtr() { return m_task_ptr; }
};

TEST_F(StepTest, ConstructorTest)
{
    auto test = GetTaskPtr()->AddStep<TestStep>("test-name");

    EXPECT_STREQ("TestStep", test->GetTypeName().c_str());
    EXPECT_STREQ("test-name", test->GetName().c_str());
    EXPECT_NO_THROW(test->GetSafety());
    EXPECT_NO_THROW(test->GetStateMachineClient());
}
TEST_F(StepTest, SetterGetterTest)
{
    auto test = GetTaskPtr()->AddStep<TestStep>("test");
    EXPECT_EQ(0, test->GetExecutionOrder());
    test->SetMaximumComputationTimeUs(123);
    EXPECT_EQ(std::chrono::microseconds(123), test->GetMaximumComputationTimeUs());

    auto test2 = GetTaskPtr()->AddStep<TestStep>("test2");
    EXPECT_EQ(1, test2->GetExecutionOrder());
}
}  //namespace mpmca::pipeline::testing