/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/pipeline/pipeline.hpp"

#include <gtest/gtest.h>

#include "mpmca/pipeline/step.hpp"
#include "mpmca/pipeline/task.hpp"
#include "mpmca/utilities/horizon.hpp"
namespace mpmca::pipeline::testing {

class PipelineTestStep : public Step
{
  private:
    int m_main_tick_counter;

  public:
    PipelineTestStep(Task& task, const std::string& name)
        : Step(task, name, "PipelineTestStep")
        , m_main_tick_counter(0)
    {
    }

    void Prepare() override { GetSafety().Prepared(); }
    void MainTick(DataBucket& data_bucket) override { ++m_main_tick_counter; };

    int GetMainTickCounter() { return m_main_tick_counter; }
};

class PipelineTestInOutput : public InOutput
{
  private:
    int m_tick_counter;
    int m_main_tick_counter;

  public:
    PipelineTestInOutput(Pipeline& pipeline, const std::string& name)
        : InOutput(pipeline, name, "PipelineTestInOutput")
        , m_tick_counter(0)
        , m_main_tick_counter(0)
    {
    }

    void Prepare() override { GetSafety().Prepared(); }
    void Tick(MessageBus& in_output_message_bus) override { ++m_tick_counter; };
    void SafeTick() override {};
    void TaskCompleted(const DataBucket& data_bucket) override {};
    void MainTick(DataBucket& data_bucket) override { ++m_main_tick_counter; };

    int GetTickCounter() { return m_tick_counter; }
    int GetMainTickCounter() { return m_main_tick_counter; }
};

class PipelineTest : public ::testing::Test
{
  protected:
    PipelineTest() {}
};

TEST_F(PipelineTest, PipelineWithoutTaskTest)
{
    Pipeline pipeline(Pipeline::Options{});

    EXPECT_EQ(0, pipeline.GetPipelineReturnCode());
    EXPECT_EQ(1, pipeline.GetBaseStepMs());
    EXPECT_EQ(0, pipeline.GetClock().GetCurrentTime());
    EXPECT_EQ(0, pipeline.GetClock().GetCurrentTimeMs());
    EXPECT_EQ(0.001, pipeline.GetClock().GetDt());
    EXPECT_EQ(1, pipeline.GetClock().GetDtMs());
    EXPECT_EQ(true, pipeline.GetCurrentTickIsMainTick(0));
    EXPECT_EQ(0, pipeline.GetNextExecutionOrder());
    EXPECT_EQ(true, pipeline.GetNextTickIsMainTick(0));
    EXPECT_EQ(0, pipeline.GetNumInOutputs());
    EXPECT_EQ(SafetyLevel::kUnprepared, pipeline.GetSafety().GetLevel());
    EXPECT_STREQ("UNPREPARED", pipeline.GetSafety().GetLevelName().c_str());
    EXPECT_EQ("Pipeline", pipeline.GetSafety().GetName());
    EXPECT_EQ(true, pipeline.GetSafety().GetGuarded());
    EXPECT_EQ(0, pipeline.GetStateClock().GetCurrentTime());
    EXPECT_EQ(0, pipeline.GetStateClock().GetCurrentTimeMs());
    EXPECT_EQ(0.001, pipeline.GetStateClock().GetDt());
    EXPECT_EQ(1, pipeline.GetStateClock().GetDtMs());
    EXPECT_EQ(State::kBooting, pipeline.GetStateMachine().GetCurrentState());
    EXPECT_EQ(false, pipeline.GetStateMachine().GetMainStateChangedOnThisTick());
    EXPECT_EQ(0, pipeline.GetStateMachine().GetNumberClientsNotInState(State::kBooting));
    EXPECT_EQ(false, pipeline.GetStateMachine().GetStatesChangedOnThisTick());
    EXPECT_EQ(nullptr, pipeline.GetTaskPtr(0));
    EXPECT_EQ(nullptr, pipeline.GetTaskPtr(1));
    EXPECT_EQ(nullptr, pipeline.GetTaskPtr(2));
    EXPECT_EQ(nullptr, pipeline.GetTaskPtr(3));
    EXPECT_EQ(-1, pipeline.GetTheoreticalMainTickCycle(0));
    EXPECT_EQ(-1, pipeline.GetTickCycle());
}

TEST_F(PipelineTest, PipelineWithTaskTest)
{
    Pipeline pipeline(Pipeline::Options{});
    utilities::Horizon horizon(10, 10);
    pipeline.MakeTask("Test-Task", horizon);

    EXPECT_EQ(0, pipeline.GetPipelineReturnCode());
    EXPECT_EQ(1, pipeline.GetBaseStepMs());
    EXPECT_EQ(0, pipeline.GetClock().GetCurrentTime());
    EXPECT_EQ(0, pipeline.GetClock().GetCurrentTimeMs());
    EXPECT_EQ(0.001, pipeline.GetClock().GetDt());
    EXPECT_EQ(1, pipeline.GetClock().GetDtMs());
    EXPECT_EQ(false, pipeline.GetCurrentTickIsMainTick(0));
    EXPECT_EQ(0, pipeline.GetNextExecutionOrder());
    EXPECT_EQ(true, pipeline.GetNextTickIsMainTick(0));
    EXPECT_EQ(0, pipeline.GetNumInOutputs());
    EXPECT_EQ(SafetyLevel::kUnprepared, pipeline.GetSafety().GetLevel());
    EXPECT_STREQ("UNPREPARED", pipeline.GetSafety().GetLevelName().c_str());
    EXPECT_EQ("Pipeline", pipeline.GetSafety().GetName());
    EXPECT_EQ(true, pipeline.GetSafety().GetGuarded());
    EXPECT_EQ(0, pipeline.GetStateClock().GetCurrentTime());
    EXPECT_EQ(0, pipeline.GetStateClock().GetCurrentTimeMs());
    EXPECT_EQ(0.001, pipeline.GetStateClock().GetDt());
    EXPECT_EQ(1, pipeline.GetStateClock().GetDtMs());
    EXPECT_EQ(State::kBooting, pipeline.GetStateMachine().GetCurrentState());
    EXPECT_EQ(false, pipeline.GetStateMachine().GetMainStateChangedOnThisTick());
    EXPECT_EQ(0, pipeline.GetStateMachine().GetNumberClientsNotInState(State::kBooting));
    EXPECT_EQ(false, pipeline.GetStateMachine().GetStatesChangedOnThisTick());
    EXPECT_NE(nullptr, pipeline.GetTaskPtr(0));
    EXPECT_EQ(-1, pipeline.GetTheoreticalMainTickCycle(0));
    EXPECT_EQ(-1, pipeline.GetTickCycle());

    EXPECT_EQ(0, pipeline.GetTaskPtr(0)->GetTaskIndex());
    EXPECT_EQ(0, pipeline.GetTaskPtr(0)->GetClock().GetCurrentTime());
    EXPECT_EQ(0, pipeline.GetTaskPtr(0)->GetClock().GetCurrentTimeMs());
    EXPECT_EQ(0.01, pipeline.GetTaskPtr(0)->GetClock().GetDt());
    EXPECT_EQ(10, pipeline.GetTaskPtr(0)->GetClock().GetDtMs());
    EXPECT_EQ(0.01, pipeline.GetTaskPtr(0)->GetHorizon().GetDt());
    EXPECT_EQ(10, pipeline.GetTaskPtr(0)->GetHorizon().GetDtMs());
    EXPECT_STREQ("Test-Task", pipeline.GetTaskPtr(0)->GetName().c_str());
    EXPECT_EQ(true, pipeline.GetConstTaskPtr(0)->GetSafety().GetGuarded());
    EXPECT_EQ(SafetyLevel::kUnprepared, pipeline.GetConstTaskPtr(0)->GetSafety().GetLevel());
    EXPECT_EQ("Test-Task", pipeline.GetConstTaskPtr(0)->GetSafety().GetName());
    EXPECT_EQ(0, pipeline.GetConstTaskPtr(0)->GetStateClock().GetCurrentTime());
    EXPECT_EQ(0, pipeline.GetConstTaskPtr(0)->GetStateClock().GetCurrentTimeMs());
    EXPECT_EQ(0.01, pipeline.GetConstTaskPtr(0)->GetStateClock().GetDt());
    EXPECT_EQ(10, pipeline.GetConstTaskPtr(0)->GetStateClock().GetDtMs());

    EXPECT_EQ(nullptr, pipeline.GetTaskPtr(1));
    EXPECT_EQ(nullptr, pipeline.GetTaskPtr(2));
    EXPECT_EQ(nullptr, pipeline.GetTaskPtr(3));
}

TEST_F(PipelineTest, PipelineBehaviorTest)
{
    Pipeline pipeline(Pipeline::Options{});

    auto test_in_output = pipeline.AddInOutput<PipelineTestInOutput>("Test-InOutput");
    utilities::Horizon horizon(10, 10);
    pipeline.MakeTask("Test-Task", horizon);

    auto test_step = pipeline.GetTaskPtr(0)->AddStep<PipelineTestStep>("Test-Step");

    EXPECT_EQ(1, pipeline.GetNumInOutputs());

    pipeline.Prepare();
    EXPECT_EQ(SafetyLevel::kSafe, test_in_output->GetSafety().GetLevel());
    EXPECT_EQ(SafetyLevel::kSafe, test_step->GetSafety().GetLevel());
    EXPECT_EQ(SafetyLevel::kSafe, pipeline.GetConstTaskPtr(0)->GetSafety().GetLevel());
    EXPECT_EQ(SafetyLevel::kSafe, pipeline.GetSafety().GetLevel());

    EXPECT_EQ(0, test_in_output->GetMainTickCounter());
    EXPECT_EQ(0, test_in_output->GetTickCounter());
    EXPECT_EQ(0, test_step->GetMainTickCounter());

    EXPECT_EQ(false, pipeline.GetCurrentTickIsMainTick(0));
    EXPECT_EQ(true, pipeline.GetNextTickIsMainTick(0));

    // Tick 0
    std::cout << "Calling Tick()" << std::endl;
    pipeline.Tick();  // this call to Tick is the "Next" tick, as referred to by the previous two lines.
    EXPECT_EQ(0, pipeline.GetTickCycle());
    EXPECT_EQ(1, test_in_output->GetMainTickCounter());
    EXPECT_EQ(1, test_in_output->GetTickCounter());
    EXPECT_EQ(0, test_step->GetMainTickCounter());

    std::cout << "Calling MainTick()" << std::endl;
    pipeline.MainTick(0);
    EXPECT_EQ(1, test_in_output->GetMainTickCounter());
    EXPECT_EQ(1, test_in_output->GetTickCounter());
    EXPECT_EQ(1, test_step->GetMainTickCounter());

    for (int tick = 1; tick < 10; ++tick) {
        // Tick 1 through 9
        pipeline.Tick();
        EXPECT_EQ(tick, pipeline.GetTickCycle());
        EXPECT_EQ(1, test_in_output->GetMainTickCounter());
        EXPECT_EQ(1 + tick, test_in_output->GetTickCounter());
        EXPECT_EQ(1, test_step->GetMainTickCounter());
    }

    // Tick 10
    pipeline.Tick();
    EXPECT_EQ(10, pipeline.GetTickCycle());
    EXPECT_EQ(2, test_in_output->GetMainTickCounter());
    EXPECT_EQ(10 + 1, test_in_output->GetTickCounter());
    EXPECT_EQ(1, test_step->GetMainTickCounter());

    pipeline.MainTick(0);
    EXPECT_EQ(SafetyLevel::kSafe, pipeline.GetSafety().GetLevel());

    pipeline.MainTick(0);
    EXPECT_EQ(SafetyLevel::kCritical, pipeline.GetSafety().GetLevel());
}

TEST_F(PipelineTest, PipelineMultipleTasksBehaviorTest)
{
    Pipeline pipeline(Pipeline::Options{});

    auto test_in_output = pipeline.AddInOutput<PipelineTestInOutput>("Test-InOutput");

    utilities::Horizon horizon(10, 10);
    pipeline.MakeTask("Test-Task", horizon);

    utilities::Horizon horizon2(100, 10);
    pipeline.MakeTask("Test-Task", horizon2);

    EXPECT_EQ(0, pipeline.GetTaskPtr(0)->GetTaskIndex());
    EXPECT_EQ(1, pipeline.GetTaskPtr(1)->GetTaskIndex());

    auto test_step1 = pipeline.GetTaskPtr(0)->AddStep<PipelineTestStep>("Test-Step");
    auto test_step2 = pipeline.GetTaskPtr(1)->AddStep<PipelineTestStep>("Test-Step");

    EXPECT_EQ(1, pipeline.GetNumInOutputs());

    pipeline.Prepare();
    EXPECT_EQ(SafetyLevel::kSafe, test_in_output->GetSafety().GetLevel());
    EXPECT_EQ(SafetyLevel::kSafe, test_step1->GetSafety().GetLevel());
    EXPECT_EQ(SafetyLevel::kSafe, test_step2->GetSafety().GetLevel());
    EXPECT_EQ(SafetyLevel::kSafe, pipeline.GetConstTaskPtr(0)->GetSafety().GetLevel());
    EXPECT_EQ(SafetyLevel::kSafe, pipeline.GetConstTaskPtr(1)->GetSafety().GetLevel());
    EXPECT_EQ(SafetyLevel::kSafe, pipeline.GetSafety().GetLevel());

    EXPECT_EQ(0, test_in_output->GetMainTickCounter());
    EXPECT_EQ(0, test_in_output->GetTickCounter());
    EXPECT_EQ(0, test_step1->GetMainTickCounter());
    EXPECT_EQ(0, test_step2->GetMainTickCounter());

    EXPECT_EQ(false, pipeline.GetCurrentTickIsMainTick(0));
    EXPECT_EQ(false, pipeline.GetCurrentTickIsMainTick(1));
    EXPECT_EQ(true, pipeline.GetNextTickIsMainTick(0));
    EXPECT_EQ(true, pipeline.GetNextTickIsMainTick(1));

    for (int tick = 0; tick < 10130; ++tick) {
        pipeline.Tick();

        EXPECT_EQ(tick, pipeline.GetTickCycle());
        EXPECT_EQ((tick / 10 + 1) + (tick / 100 + 1), test_in_output->GetMainTickCounter());
        EXPECT_EQ(tick + 1, test_in_output->GetTickCounter());

        if (pipeline.GetCurrentTickIsMainTick(0)) {
            EXPECT_EQ(tick / 10 + 1 - 1, test_step1->GetMainTickCounter());
            pipeline.MainTick(0);
        }
        EXPECT_EQ(tick / 10 + 1, test_step1->GetMainTickCounter());

        if (pipeline.GetCurrentTickIsMainTick(1)) {
            EXPECT_EQ(tick / 100 + 1 - 1, test_step2->GetMainTickCounter());
            pipeline.MainTick(1);
        }
        EXPECT_EQ(tick / 100 + 1, test_step2->GetMainTickCounter());
    }
}

}  //namespace mpmca::pipeline::testing