/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <gtest/gtest.h>

#include "mpmca/utilities/input_parser.hpp"
#include "mpmca/utilities/test_constants.hpp"

int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);

    mpmca::utilities::CommandLineOptionsParser input_parser(argc, argv);

    if (input_parser.GetCommandOptionExists("--test_data_path")) {
        mpmca::utilities::TestConstants::Instance()->SetTestDataPath(input_parser.GetCommandOption("--test_data_path"));
    }

    return RUN_ALL_TESTS();
}