/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <gtest/gtest.h>

#include "mpmca/utilities/file_tools.hpp"

namespace mpmca::utilities::testing {
class FilePartsTest : public ::testing::Test
{
  protected:
    FilePartsTest() {}
};

TEST_F(FilePartsTest, test)
{
    auto fp = FileTools::GetFileParts("/test/directory/file.ext");

    EXPECT_EQ(0, fp.path.compare("/test/directory/"));
    EXPECT_EQ(0, fp.name.compare("file"));
    EXPECT_EQ(0, fp.ext.compare(".ext"));
}
}  //namespace mpmca::utilities::testing
