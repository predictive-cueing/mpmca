/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/math.hpp"

#include <gtest/gtest.h>

#include <random>

#include "mpmca/constants.hpp"
#include "mpmca/types/pose_rot_mat_vector.hpp"
#include "mpmca/types/pose_vector.hpp"
#include "mpmca/types/transformation_matrix.hpp"

namespace mpmca::utilities::testing {
class MathTest : public ::testing::Test
{
  protected:
    MathTest() {}
};

TEST_F(MathTest, SignTest)
{
    EXPECT_TRUE(Math::Sign(5) == 1);
    EXPECT_TRUE(Math::Sign(-10) == -1);
    EXPECT_TRUE(Math::Sign(0) == 1);
}

TEST_F(MathTest, ClampTest)
{
    // this test tests an std function, which used to be a function in Math
    // the test serves as documentation
    EXPECT_TRUE(std::clamp(5, 1, 3) == 3);
    EXPECT_TRUE(std::clamp(5, -10, 30) == 5);
    EXPECT_TRUE(std::clamp(5, 1, 1) == 1);
    EXPECT_TRUE(std::clamp(5, 1, -1) == -1);
}

TEST_F(MathTest, CosineTest)
{
    double tol = 1e-5;
    EXPECT_NEAR(Math::GetCosineFade(10, 20, 10, -10), 10, tol);
    EXPECT_NEAR(Math::GetCosineFade(10, 20, 10, -5), 10, tol);
    EXPECT_NEAR(Math::GetCosineFade(10, 20, 10, 0), 10, tol);
    EXPECT_NEAR(Math::GetCosineFade(10, 20, 10, 5), 15, tol);
    EXPECT_NEAR(Math::GetCosineFade(10, 20, 10, 10), 20, tol);
    EXPECT_NEAR(Math::GetCosineFade(10, 20, 10, 100), 20, tol);
    EXPECT_NEAR(Math::GetCosineFade(-10, -20, 10, -10), -10, tol);
    EXPECT_NEAR(Math::GetCosineFade(-10, -20, 10, -5), -10, tol);
    EXPECT_NEAR(Math::GetCosineFade(-10, -20, 10, 0), -10, tol);
    EXPECT_NEAR(Math::GetCosineFade(-10, -20, 10, 5), -15, tol);
    EXPECT_NEAR(Math::GetCosineFade(-10, -20, 10, 10), -20, tol);
    EXPECT_NEAR(Math::GetCosineFade(-10, -20, 10, 100), -20, tol);
}

TEST_F(MathTest, WorldToBodyTest)
{
    for (double phi = -0.4 * constants::kPi; phi < 0.4 * constants::kPi; phi += 0.01123) {
        {
            std::array<double, 3> body_vec = Math::WorldToBody<double>({0, 1, 0}, {phi, 0, 0});
            EXPECT_NEAR(0, body_vec[0], 1e-6);
            EXPECT_NEAR(cos(phi), body_vec[1], 1e-6);
            EXPECT_NEAR(-sin(phi), body_vec[2], 1e-6);
        }
        {
            std::array<double, 3> body_vec = Math::WorldToBody<double>({0, 0, 1}, {phi, 0, 0});

            EXPECT_NEAR(0, body_vec[0], 1e-6);
            EXPECT_NEAR(sin(phi), body_vec[1], 1e-6);
            EXPECT_NEAR(cos(phi), body_vec[2], 1e-6);
        }
    }

    for (double theta = -0.4 * constants::kPi; theta < 0.4 * constants::kPi; theta += 0.01123) {
        {
            std::array<double, 3> body_vec = Math::WorldToBody<double>({1, 0, 0}, {0, theta, 0});

            EXPECT_NEAR(cos(theta), body_vec[0], 1e-6);
            EXPECT_NEAR(0, body_vec[1], 1e-6);
            EXPECT_NEAR(sin(theta), body_vec[2], 1e-6);
        }
        {
            std::array<double, 3> body_vec = Math::WorldToBody<double>({0, 0, 1}, {0, theta, 0});

            EXPECT_NEAR(-sin(theta), body_vec[0], 1e-6);
            EXPECT_NEAR(0, body_vec[1], 1e-6);
            EXPECT_NEAR(cos(theta), body_vec[2], 1e-6);
        }
    }

    for (double psi = -2 * constants::kPi; psi < 2 * constants::kPi; psi += 0.01123) {
        {
            std::array<double, 3> body_vec = Math::WorldToBody<double>({1, 0, 0}, {0, 0, psi});
            EXPECT_NEAR(cos(psi), body_vec[0], 1e-6);
            EXPECT_NEAR(-sin(psi), body_vec[1], 1e-6);
            EXPECT_NEAR(0, body_vec[2], 1e-6);
        }
        {
            std::array<double, 3> body_vec = Math::WorldToBody<double>({0, 1, 0}, {0, 0, psi});
            EXPECT_NEAR(sin(psi), body_vec[0], 1e-6);
            EXPECT_NEAR(cos(psi), body_vec[1], 1e-6);
            EXPECT_NEAR(0, body_vec[2], 1e-6);
        }
    }
}

TEST_F(MathTest, WorldToBodyToWorldTest)
{
    std::default_random_engine generator;
    std::uniform_real_distribution<double> pos_dist(-1000, 6000);
    std::uniform_real_distribution<double> angle_dist(-0.5, 0.5);

    for (int i = 0; i < 1000; ++i) {
        std::array<double, 3> world_vec{pos_dist(generator), pos_dist(generator), pos_dist(generator)};

        std::array<double, 3> world_to_body_euler_rotation{angle_dist(generator), angle_dist(generator),
                                                           angle_dist(generator)};

        std::array<double, 3> body_vec = Math::WorldToBody(world_vec, world_to_body_euler_rotation);
        std::array<double, 3> world_vec2 = Math::BodyToWorld(body_vec, world_to_body_euler_rotation);

        ASSERT_NEAR(world_vec[0], world_vec2[0], 1e-10);
        ASSERT_NEAR(world_vec[1], world_vec2[1], 1e-10);
        ASSERT_NEAR(world_vec[2], world_vec2[2], 1e-10);
    }
}

TEST_F(MathTest, QuaternionTest)
{
    for (double roll = -1; roll < 1; roll += 0.2) {
        for (double pitch = -1; pitch < 1; pitch += 0.2) {
            for (double yaw = -1; yaw < 1; yaw += 0.2) {
                auto rot_mat = TransformationMatrix::FromPoseParameters(1, 2, 3, roll, pitch, yaw);
                auto quat_rot_mat = Math::RotationMatrixToQuaternion(rot_mat);
                auto quat_euler = Math::EulerToQuaternion({roll, pitch, yaw});

                ASSERT_TRUE(quat_rot_mat.isApprox(quat_euler) || quat_rot_mat.isApprox(-quat_euler))
                    << quat_rot_mat.transpose() << " <> " << quat_euler.transpose();
            }
        }
    }
}

TEST_F(MathTest, FullPoseVectorTransformationMatrixTest)
{
    const double tol = 0.1;
    const double step = 0.123;
    for (double phi = -constants::kPi / 2 + tol; phi < constants::kPi / 2; phi += step) {
        for (double theta = -constants::kPi / 2 + tol; theta < constants::kPi / 2; theta += step) {
            for (double psi = -constants::kPi / 2 + tol; psi < constants::kPi - tol; psi += step) {
                // x, y, z, phi, theta, psi, xd, yd, zd, p, q, r, xdd, ydd, zdd, pd, qd, rd
                PoseVector pose_vector;
                pose_vector << 1, 2, 3, phi, theta, psi, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15;

                PoseRotMatVector pose_rotation = pose_vector.ToPoseRotMatVector();

                PoseVector pose_back = pose_rotation.ToPoseVector();

                ASSERT_TRUE(pose_vector.isApprox(pose_back)) << phi << " " << theta << " " << psi << std::endl
                                                             << std::endl
                                                             << pose_vector << std::endl
                                                             << std::endl
                                                             << pose_rotation << std::endl
                                                             << std::endl
                                                             << pose_back;
            }
        }
    }
}

TEST_F(MathTest, BodyToWorldTest)
{
    std::array<double, 3> world_vec;

    for (double phi = -0.4 * constants::kPi; phi < 0.4 * constants::kPi; phi += 0.01123) {
        world_vec = Math::BodyToWorld<double>({0, 1, 0}, {phi, 0, 0});

        EXPECT_NEAR(0, world_vec[0], 1e-6);
        EXPECT_NEAR(cos(phi), world_vec[1], 1e-6);
        EXPECT_NEAR(sin(phi), world_vec[2], 1e-6);

        world_vec = Math::BodyToWorld<double>({0, 0, 1}, {phi, 0, 0});

        EXPECT_NEAR(0, world_vec[0], 1e-6);
        EXPECT_NEAR(-sin(phi), world_vec[1], 1e-6);
        EXPECT_NEAR(cos(phi), world_vec[2], 1e-6);
    }

    for (double theta = -0.4 * constants::kPi; theta < 0.4 * constants::kPi; theta += 0.01123) {
        world_vec = Math::BodyToWorld<double>({1, 0, 0}, {0, theta, 0});

        EXPECT_NEAR(cos(theta), world_vec[0], 1e-6);
        EXPECT_NEAR(0, world_vec[1], 1e-6);
        EXPECT_NEAR(-sin(theta), world_vec[2], 1e-6);

        world_vec = Math::BodyToWorld<double>({0, 0, 1}, {0, theta, 0});

        EXPECT_NEAR(sin(theta), world_vec[0], 1e-6);
        EXPECT_NEAR(0, world_vec[1], 1e-6);
        EXPECT_NEAR(cos(theta), world_vec[2], 1e-6);
    }

    for (double psi = -2 * constants::kPi; psi < 2 * constants::kPi; psi += 0.01123) {
        world_vec = Math::BodyToWorld<double>({1, 0, 0}, {0, 0, psi});
        EXPECT_NEAR(cos(psi), world_vec[0], 1e-6);
        EXPECT_NEAR(sin(psi), world_vec[1], 1e-6);
        EXPECT_NEAR(0, world_vec[2], 1e-6);

        world_vec = Math::BodyToWorld<double>({0, 1, 0}, {0, 0, psi});
        EXPECT_NEAR(-sin(psi), world_vec[0], 1e-6);
        EXPECT_NEAR(cos(psi), world_vec[1], 1e-6);
        EXPECT_NEAR(0, world_vec[2], 1e-6);
    }
}

}  //namespace mpmca::utilities::testing
