/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <gtest/gtest.h>

#include "mpmca/utilities/scalar.hpp"

namespace mpmca::utilities::testing {
class ParameterTest : public ::testing::Test
{
  protected:
    ParameterTest() {}
};

TEST_F(ParameterTest, ScalarTest)
{
    Scalar param(4.0);
    EXPECT_TRUE(param.GetValue() == 4.0);
    param.SetValue(3.0);
    EXPECT_TRUE(param.GetValue() == 3.0);
}

}  //namespace mpmca::utilities::testing
