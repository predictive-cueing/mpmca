/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <gtest/gtest.h>

#include "mpmca/utilities/map_2d.hpp"

namespace mpmca::utilities::testing {
class Map2DTest : public ::testing::TestWithParam<double>
{
  protected:
    Map2D m_map;

    Map2DTest()
        : m_map({0.0, 1.0}, {1.0, 2.0, 3.0}, {{1.0, 2.0, 3.0}, {10.0, 20.0, 30.0}})
    {
    }
};

TEST_P(Map2DTest, map2dTest)
{
    double mult = 1.0;
    if (GetParam() > 0.5)
        mult = 10.0;

    EXPECT_DOUBLE_EQ(1.0 * mult, m_map.GetValue(GetParam(), -100.0, Map2D::kNearestNeighbour));
    EXPECT_DOUBLE_EQ(1.0 * mult, m_map.GetValue(GetParam(), 1.0, Map2D::kNearestNeighbour));
    EXPECT_DOUBLE_EQ(1.0 * mult, m_map.GetValue(GetParam(), 1.49999, Map2D::kNearestNeighbour));
    EXPECT_DOUBLE_EQ(2.0 * mult, m_map.GetValue(GetParam(), 1.50001, Map2D::kNearestNeighbour));
    EXPECT_DOUBLE_EQ(2.0 * mult, m_map.GetValue(GetParam(), 2.0, Map2D::kNearestNeighbour));
    EXPECT_DOUBLE_EQ(3.0 * mult, m_map.GetValue(GetParam(), 3.0, Map2D::kNearestNeighbour));
    EXPECT_DOUBLE_EQ(3.0 * mult, m_map.GetValue(GetParam(), 100.0, Map2D::kNearestNeighbour));
}

INSTANTIATE_TEST_CASE_P(ConstructorRange, Map2DTest, ::testing::Values(-10.0, -1.0, 0.0, 0.49999, 0.500001, 1.0, 10));
}  //namespace mpmca::utilities::testing