/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/clock.hpp"

#include <gtest/gtest.h>

namespace mpmca::utilities::testing {
class ClockTest : public ::testing::Test
{
  protected:
    ClockTest() {}
};

TEST_F(ClockTest, clockTest)
{
    using namespace std::chrono_literals;
    Clock clock(10ms);

    EXPECT_EQ(0.01, clock.GetDt());
    EXPECT_EQ(10, clock.GetDtMs());

    EXPECT_EQ(0, clock.GetCurrentTime());
    EXPECT_EQ(0, clock.GetCurrentTimeMs());

    clock.Tick();

    EXPECT_EQ(0.01, clock.GetCurrentTime());
    EXPECT_EQ(10, clock.GetCurrentTimeMs());
}
}  //namespace mpmca::utilities::testing
