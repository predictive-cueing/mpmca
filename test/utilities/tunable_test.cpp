/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/tunable.hpp"

#include <gtest/gtest.h>

#include "mpmca/utilities/scalar.hpp"
#include "mpmca/utilities/tunable_register.hpp"

namespace mpmca::utilities::testing {
class TunableTest : public ::testing::Test
{
  protected:
    TunableTest() {}
};

TEST_F(TunableTest, CompileTest)
{
    Tunable<Scalar> param("BasicName", "ParamCompileTest", Scalar(4.0));
}

TEST_F(TunableTest, NoParameterTest)
{
    Tunable<Scalar> param("BasicName", "ParamCompileTest");
    EXPECT_THROW(param.GetValue(0), std::runtime_error);
}

TEST_F(TunableTest, StepChangeTest)
{
    Tunable<Scalar> param("BasicName", "ParamCompileTest", Scalar(4.0));
    EXPECT_TRUE(param.GetValue(0) == 4.0);
    param.SetNextValue(Scalar(5.0), 50, 0, TunableChange::ChangeMethod::kStep);
    EXPECT_TRUE(param.GetValue(10) == 4.0);
    EXPECT_TRUE(param.GetValue(49) == 4.0);
    EXPECT_TRUE(param.GetValue(50) == 5.0);
    EXPECT_TRUE(param.GetValue(49) == 4.0);
    param.SetNextValue(Scalar(10.0), 100, 0, TunableChange::ChangeMethod::kStep);
    EXPECT_TRUE(param.GetValue(49) == 4.0);
    EXPECT_TRUE(param.GetValue(50) == 5.0);
    EXPECT_TRUE(param.GetValue(99) == 5.0);
    EXPECT_TRUE(param.GetValue(100) == 10.0);
}

TEST_F(TunableTest, LinearChangeStepTest)
{
    Tunable<Scalar> param("BasicName", "ParamCompileTest", Scalar(4.0));
    EXPECT_TRUE(param.GetValue(0) == 4.0);
    param.SetNextValue(Scalar(5.0), 50, 10, TunableChange::ChangeMethod::kLinear);
    EXPECT_TRUE(param.GetValue(10) == 4.0);
    EXPECT_TRUE(param.GetValue(49) == 4.0);
    EXPECT_TRUE(param.GetValue(50) == 4.0);
    EXPECT_TRUE(param.GetValue(51) == 4.1);
    EXPECT_TRUE(param.GetValue(52) == 4.2);
    EXPECT_TRUE(param.GetValue(53) == 4.3);
    EXPECT_TRUE(param.GetValue(54) == 4.4);
    EXPECT_TRUE(param.GetValue(55) == 4.5);
    EXPECT_TRUE(param.GetValue(56) == 4.6);
    EXPECT_TRUE(param.GetValue(57) == 4.7);
    EXPECT_TRUE(param.GetValue(58) == 4.8);
    EXPECT_TRUE(param.GetValue(59) == 4.9);
    EXPECT_TRUE(param.GetValue(60) == 5.0);
    EXPECT_TRUE(param.GetValue(61) == 5.0);
    EXPECT_TRUE(param.GetValue(62) == 5.0);
    param.SetNextValue(Scalar(10.0), 100, 10, TunableChange::ChangeMethod::kLinear);
    EXPECT_TRUE(param.GetValue(49) == 4.0);
    EXPECT_TRUE(param.GetValue(50) == 4.0);
    EXPECT_TRUE(param.GetValue(99) == 5.0);
    EXPECT_TRUE(param.GetValue(100) == 5.0);
    EXPECT_TRUE(param.GetValue(105) == 7.5);
    EXPECT_TRUE(param.GetValue(110) == 10.0);
    EXPECT_TRUE(param.GetValue(0) == 4.0);

    param.Reset();

    EXPECT_TRUE(param.GetValue(0) == 10.0);
    EXPECT_TRUE(param.GetValue(100) == 10.0);
    EXPECT_TRUE(param.GetValue(200) == 10.0);

    param.SetNextValue(Scalar(9.0), 50, 10, TunableChange::ChangeMethod::kLinear);

    EXPECT_TRUE(param.GetValue(51) == 9.9);
}

TEST_F(TunableTest, RegisterTest)
{
    Tunable<Scalar> param("BasicName1", "ParamCompileTest", Scalar(4.0));
    auto p = ITunable::GetTunableRegister().GetParameterByUniqueName("uniqueName");

    if (p != nullptr)
        std::cout << p->GetBasicName() << std::endl;
}
}  //namespace mpmca::utilities::testing
