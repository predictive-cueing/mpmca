/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/horizon.hpp"

#include <gtest/gtest.h>

#include "mpmca/utilities/clock.hpp"
#include "mpmca/utilities/configuration.hpp"
#include "mpmca/utilities/step_count.hpp"
namespace mpmca::utilities::testing {
class HorizonTest : public ::testing::Test
{
  protected:
    ConfigurationPtr m_config1;
    ConfigurationPtr m_config2;
    Clock m_clock;
    Horizon m_horizon0;
    Horizon m_horizon1;
    Horizon m_horizon2;
    Horizon m_horizon3;

    HorizonTest()
        : m_config1(Configuration::CreateConfigFromString("config1",
                                                          "{\"Horizon\" : {\"PredictionMaximumTimeStepMs\" : 10, "
                                                          "\"Steps\" : [ {\"StepMs\": 10, \"Count\": 1000} ] } }"))
        , m_config2(Configuration::CreateConfigFromString(
              "config2",
              "{\"Horizon\" : {\"PredictionMaximumTimeStepMs\" : 10, \"Steps\" : [ "
              "{\"StepMs\": 10, \"Count\": 50}, {\"StepMs\": 50, \"Count\": 10}, {\"StepMs\": 100, \"Count\": 10} ] } "
              "}"))
        , m_clock(std::chrono::milliseconds(10))
        , m_horizon0(StepCount(10, 1000), 10)
        , m_horizon1(m_config1)
        , m_horizon2(m_config2)
        , m_horizon3(10, 1000)
    {
    }
};

TEST_F(HorizonTest, PredictionHorizonTest)
{
    EXPECT_TRUE(m_horizon2.GetNMpc() == m_horizon2.GetPredictionHorizonIndex().size());
    EXPECT_TRUE(m_horizon2.GetNPrediction() == m_horizon2.GetPredictionHorizonDeltaTimeSteps().size());
    EXPECT_TRUE(m_horizon2.GetNPrediction() == m_horizon2.GetPredictionHorizonTime().size());
    EXPECT_TRUE(m_horizon2.GetNPrediction() == m_horizon2.GetPredictionHorizonDeltaTime().size());
    EXPECT_TRUE(m_horizon2.GetNPrediction() == m_horizon2.GetPredictionHorizonTimeMs().size());

    EXPECT_TRUE(m_horizon3.GetNMpc() == m_horizon3.GetPredictionHorizonIndex().size());
    EXPECT_TRUE(m_horizon3.GetNPrediction() == m_horizon3.GetPredictionHorizonDeltaTimeSteps().size());
    EXPECT_TRUE(m_horizon3.GetNPrediction() == m_horizon3.GetPredictionHorizonTime().size());
    EXPECT_TRUE(m_horizon3.GetNPrediction() == m_horizon3.GetPredictionHorizonDeltaTime().size());
    EXPECT_TRUE(m_horizon3.GetNPrediction() == m_horizon3.GetPredictionHorizonTimeMs().size());
}
TEST_F(HorizonTest, PredictionHorizonTest2)
{
    for (int i = 0; i < m_horizon2.GetNPrediction() - 1; ++i)
        EXPECT_TRUE(abs((m_horizon2.GetPredictionHorizonDeltaTimeSteps(i) * 0.01) -
                        m_horizon2.GetPredictionHorizonDeltaTime(i)) < 1e-10);

    for (int i = 0; i < m_horizon2.GetNPrediction(); ++i)
        EXPECT_TRUE(abs(m_horizon2.GetPredictionHorizonTime(i) - (m_horizon2.GetPredictionHorizonTimeMs(i) / 1000.0)) <
                    1e-10);

    for (int i = 0; i < m_horizon3.GetNPrediction() - 1; ++i) {
        EXPECT_TRUE(m_horizon3.GetPredictionHorizonDeltaTimeSteps(i) == 1);
        EXPECT_TRUE(m_horizon3.GetPredictionHorizonDeltaTime(i) == 0.01);
    }

    for (int i = 0; i < m_horizon3.GetNPrediction(); ++i) {
        EXPECT_TRUE(m_horizon3.GetPredictionHorizonTime(i) == i * 0.01);
        EXPECT_TRUE(m_horizon3.GetPredictionHorizonTimeMs(i) == i * 10);
    }
}
TEST_F(HorizonTest, HorizonTest)
{
    EXPECT_EQ(0.01, m_horizon0.GetDt());
    EXPECT_EQ(1000, m_horizon0.GetNMpc());
    EXPECT_EQ(1001, m_horizon0.GetNMpcPred());
    EXPECT_EQ(1001, m_horizon0.GetNPrediction());

    EXPECT_EQ(3000, m_horizon0.GetHorizonTimeMs(300));
    EXPECT_EQ(4000, m_horizon0.GetHorizonTimeMs(400));

    EXPECT_EQ(300, m_horizon0.GetPredictionHorizonIndex(300));
    EXPECT_EQ(400, m_horizon0.GetPredictionHorizonIndex(400));

    EXPECT_EQ(m_horizon1.GetDt(), m_horizon0.GetDt());
    EXPECT_EQ(m_horizon1.GetNPrediction(), m_horizon0.GetNPrediction());
    EXPECT_EQ(m_horizon1.GetNMpcPred(), m_horizon0.GetNMpcPred());
    EXPECT_EQ(m_horizon1.GetNMpc(), m_horizon0.GetNMpc());

    EXPECT_EQ(0.01, m_horizon2.GetDt());
    EXPECT_EQ(50 + 10 + 10, m_horizon2.GetNMpc());
    EXPECT_EQ(50 + 10 + 10 + 1, m_horizon2.GetNMpcPred());
    EXPECT_EQ((50 + 5 * 10 + 10 * 10 + 1), m_horizon2.GetNPrediction());

    EXPECT_EQ(0, m_horizon2.GetPredictionHorizonIndex(0));
    EXPECT_EQ(49, m_horizon2.GetPredictionHorizonIndex(49));
    EXPECT_EQ(50, m_horizon2.GetPredictionHorizonIndex(50));
    EXPECT_EQ(55, m_horizon2.GetPredictionHorizonIndex(51));
    EXPECT_EQ(60, m_horizon2.GetPredictionHorizonIndex(52));
    EXPECT_EQ(65, m_horizon2.GetPredictionHorizonIndex(53));
    EXPECT_EQ(70, m_horizon2.GetPredictionHorizonIndex(54));
    EXPECT_EQ(75, m_horizon2.GetPredictionHorizonIndex(55));
    EXPECT_EQ(80, m_horizon2.GetPredictionHorizonIndex(56));
    EXPECT_EQ(85, m_horizon2.GetPredictionHorizonIndex(57));
    EXPECT_EQ(90, m_horizon2.GetPredictionHorizonIndex(58));
    EXPECT_EQ(95, m_horizon2.GetPredictionHorizonIndex(59));
    EXPECT_EQ(100, m_horizon2.GetPredictionHorizonIndex(60));
    EXPECT_EQ(110, m_horizon2.GetPredictionHorizonIndex(61));
}
}  //namespace mpmca::utilities::testing
