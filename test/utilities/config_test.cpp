/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <gtest/gtest.h>

#include "mpmca/utilities/configuration.hpp"
#include "mpmca/utilities/test_constants.hpp"

namespace mpmca::utilities::testing {
class ConfigTest : public ::testing::Test
{
  protected:
    ConfigurationPtr m_config;

    ConfigTest() { m_config = Configuration::CreateRootConfig("config", true); }
};

TEST_F(ConfigTest, CreateConfigFromString)
{
    ConfigurationPtr config1(Configuration::CreateConfigFromString(
        "config1",
        "{\"Clock\" : {\"t_start\" : 0, \"dt\" : 0.01}, \"Horizon\" : "
        "{\"predictionMaximumTimeStep\" : 1, \"Steps\" : [ {\"step\": 1, \"count\": 1000} ] } }"));
    auto ctime = config1->GetConfigurationObject("Clock");
}

TEST_F(ConfigTest, UpdateTest)
{
    nlohmann::json j;
    j["object_name"]["ip"] = "127.0.0.1";
    j["object_name"]["port"] = 2031;
    j["other_object"]["port"] = 501;

    auto cfg2a = Configuration::CreateRootConfig("cfg2a", j, true);

    nlohmann::json jn;
    jn["object_name"]["ip"] = "127.0.0.1";
    jn["object_name"]["receive_port"] = 2032;

    cfg2a->Update(jn);

    EXPECT_TRUE(cfg2a->GetJson().dump().compare("{\"object_name\":{\"ip\":\"127.0.0.1\",\"port\":2031,\"receive_port\":"
                                                "2032},\"other_object\":{\"port\":501}}") == 0);
}

TEST_F(ConfigTest, GetParentJsonTest)
{
    nlohmann::json j;
    j["first"]["ip"]["local"] = "127.0.0.1";
    j["first"]["ip"]["outside"] = "10.1.2.21";
    j["first"]["port"] = 2031;

    j["second"]["ip"] = "128.2.3.1";
    j["second"]["port"] = 31;

    auto cfg1 = Configuration::CreateRootConfig("cfg1", j, false);

    auto cfg2 = cfg1->GetConfigurationObject("first");
    auto cfg3 = cfg2->GetConfigurationObject("ip");

    EXPECT_TRUE(cfg3->GetJson().dump().compare("{\"local\":\"127.0.0.1\",\"outside\":\"10.1.2.21\"}") == 0);
    EXPECT_TRUE(cfg3->GetGlobalJson().dump().compare(j.dump()) == 0);
}

TEST_F(ConfigTest, ConfigTest1)
{
    double val1 = m_config->GetValue<double>("val", 1.0);
    EXPECT_TRUE(val1 == 1.0);

    ConfigurationPtr cfg1 = m_config->GetArray("val_array");
    EXPECT_TRUE(cfg1->GetJson().dump().compare("[]") == 0);

    double val2 = cfg1->GetValueFromArray<double>(2, std::vector<double>{0., 1., 2.});
    EXPECT_TRUE(val2 == 2.);
    EXPECT_TRUE(cfg1->GetValueFromArray<double>(0, -10) == 0.);
    EXPECT_TRUE(cfg1->GetValueFromArray<double>(1, -10) == 1.);
    EXPECT_TRUE(cfg1->GetValueFromArray<double>(2, -10) == 2.);

    double val3 = cfg1->GetValueFromArray<double>(10, 5);
    EXPECT_TRUE(val3 == 5.);
    EXPECT_TRUE(cfg1->GetValueFromArray<double>(0, -10) == 0.);
    EXPECT_TRUE(cfg1->GetValueFromArray<double>(1, -10) == 1.);
    EXPECT_TRUE(cfg1->GetValueFromArray<double>(2, -10) == 2.);
    EXPECT_TRUE(cfg1->GetValueFromArray<double>(3, -10) == 5.);
    EXPECT_TRUE(cfg1->GetValueFromArray<double>(3, -10) == 5.);
    EXPECT_TRUE(cfg1->GetValueFromArray<double>(4, -10) == 5.);
    EXPECT_TRUE(cfg1->GetValueFromArray<double>(5, -10) == 5.);
    EXPECT_TRUE(cfg1->GetValueFromArray<double>(6, -10) == 5.);
    EXPECT_TRUE(cfg1->GetValueFromArray<double>(7, -10) == 5.);
    EXPECT_TRUE(cfg1->GetValueFromArray<double>(8, -10) == 5.);
    EXPECT_TRUE(cfg1->GetValueFromArray<double>(9, -10) == 5.);
    EXPECT_TRUE(cfg1->GetValueFromArray<double>(10, -10) == 5.);

    EXPECT_TRUE(cfg1->GetValueFromArray<double>(15, -10) == -10.);

    nlohmann::json j;
    j["object_name"]["ip"] = "127.0.0.1";
    j["object_name"]["port"] = 2031;

    auto cfg2a = Configuration::CreateRootConfig("cfg2a", j, true);

    {
        auto cfg2b = cfg2a->GetConfigurationObject("object_name");

        EXPECT_TRUE(cfg2b->GetJson().dump().compare("{\"ip\":\"127.0.0.1\",\"port\":2031}") == 0);

        EXPECT_TRUE(cfg2b->GetValue<std::string>("ip", "192.168.0.1").compare("127.0.0.1") == 0);
        EXPECT_TRUE(cfg2b->GetValue<std::string>("ip", "192.168.0.2").compare("127.0.0.1") == 0);
        EXPECT_TRUE(cfg2b->GetValue<std::string>("ip", "135.0.0.1").compare("127.0.0.1") == 0);
        EXPECT_TRUE(cfg2b->GetValue<std::string>("receive_ip", "192.168.0.1").compare("192.168.0.1") == 0);
        EXPECT_TRUE(cfg2b->GetValue<int>("port", 5501) == 2031);

        EXPECT_TRUE(cfg2b->GetValue<int>("out_port", 5501) == 5501);

        EXPECT_TRUE(cfg2b->GetJson().dump().compare(
                        "{\"ip\":\"127.0.0.1\",\"out_port\":5501,\"port\":2031,\"receive_ip\":\"192.168.0.1\"}") == 0);
        EXPECT_TRUE(cfg2a->GetJson().dump().compare("{\"object_name\":{\"ip\":\"127.0.0.1\",\"port\":2031}}") == 0);
    }

    std::cout << cfg2a->GetJson().dump() << std::endl;
    EXPECT_TRUE(cfg2a->GetJson().dump().compare("{\"object_name\":{\"ip\":\"127.0.0.1\",\"out_port\":5501,\"port\":"
                                                "2031,\"receive_ip\":\"192.168.0.1\"}}") == 0);
}

TEST_F(ConfigTest, ConfigTest2)
{
    auto cfg3a = Configuration::CreateRootConfig("three", true);

    std::string fruit = cfg3a->GetValue<std::string>("fruit", "apple");
    EXPECT_TRUE(fruit.compare("apple") == 0);

    auto cfg3b = cfg3a->GetConfigurationObject("vegetables");
    std::string potato = cfg3b->GetValue<std::string>("potato", "potato");
    auto cfg3c = cfg3b->GetConfigurationObject("broccoli");

    cfg3c->GetValue<int>("leaves", 3);
    auto cfg3d = cfg3c->GetArray("colors");
    cfg3d->GetValueFromArray<double>(2, 3);
    cfg3d->GetValueFromArray<double>(5, 5);

    {
        auto cfg3e = cfg3c->GetConfigurationObjectArray("objects", 2);
        auto cfg3f = cfg3e->GetConfigurationObjectArrayElement(3);
        cfg3f->GetValue<int>("tool", 20);
        EXPECT_TRUE(cfg3f->GetJson().dump().compare("{\"tool\":20}") == 0);
    }

    EXPECT_TRUE(
        cfg3a->GetChildrenJson().dump().compare(
            "{\"fruit\":\"apple\",\"vegetables\":{\"broccoli\":{\"colors\":[3.0,3.0,3.0,5.0,5.0,5.0],\"leaves\":"
            "3,\"objects\":[{},{},{},{\"tool\":20}]},\"potato\":\"potato\"}}") == 0);
}

TEST_F(ConfigTest, ExtraConfigTest)
{
    auto config =
        Configuration::CreateConfigFromString("test", "{\"FileReader\" : {\"Indexing\" : \"time based\"}}", true);
    {
        config->GetConfigurationObject("FileReader")
            ->GetValue<std::string>("Filename", "data/json_playback_example.json");
        config->GetConfigurationObject("FileReader")->GetValue<std::string>("Indexing", "time based");
    }
    EXPECT_EQ(0,
              config->GetChildrenJson().dump().compare(
                  "{\"FileReader\":{\"Filename\":\"data/json_playback_example.json\",\"Indexing\":\"time based\"}}"));

    auto config2 = Configuration::CreateRootConfig("config2", true);

    {
        config2->GetConfigurationObject("FileReader")
            ->GetValue<std::string>("Filename", "data/json_playback_example.json");
    }
    {
        config2->GetConfigurationObject("FileReader")->GetValue<std::string>("Indexing", "time based");
    }

    EXPECT_EQ(0,
              config2->GetChildrenJson().dump().compare(
                  "{\"FileReader\":{\"Filename\":\"data/json_playback_example.json\",\"Indexing\":\"time based\"}}"));
}

TEST_F(ConfigTest, SaveConfigTest)
{
    nlohmann::json j;
    j["object_name"]["ip"] = "127.0.0.1";
    j["object_name"]["port"] = 2031;

    auto cfg2a = Configuration::CreateRootConfig("cfg2a", j, true);

    auto cfg2b = cfg2a->GetConfigurationObject("object_name");
    cfg2b->GetValue<std::string>("ip", "192.168.0.1");
    cfg2b->GetValue<std::string>("ip", "192.168.0.2");
    cfg2b->GetValue<std::string>("ip", "135.0.0.1");
    cfg2b->GetValue<std::string>("receive_ip", "192.168.0.1");
    cfg2b->GetValue<int>("port", 5501);
    cfg2b->GetValue<int>("out_port", 5501);

    // testing saving and overwrite protection
    EXPECT_NO_THROW(cfg2b->SaveJson(TestConstants::TEST_DATA_PATH() + "GOOGLETEST_config.json", true));
    EXPECT_ANY_THROW(cfg2b->SaveJson(TestConstants::TEST_DATA_PATH() + "GOOGLETEST_config.json", false));

    auto cfg3 = Configuration::CreateConfigFromFile("File", TestConstants::TEST_DATA_PATH() + "GOOGLETEST_config.json");

    EXPECT_TRUE(cfg2b->GetJson().dump().compare(cfg3->GetJson().dump()) == 0);
}

TEST_F(ConfigTest, ParentConfigTest)
{
    nlohmann::json j_child1;
    j_child1["ParentConfigFile"] = "GOOGLETEST_parent1.json";
    j_child1["object"]["port"] = 1111;
    j_child1["object"]["send_ip"] = "127.0.0.1";

    nlohmann::json j_child2;
    j_child2["ParentConfigFile"] = "GOOGLETEST_parent2.json";
    j_child2["object"]["port"] = 2222;
    j_child2["object"]["send_ip"] = "127.0.0.2";

    nlohmann::json j_child3;
    j_child3["ParentConfigFile"] = "GOOGLETEST_no_parent.json";
    j_child3["object"]["port"] = 2222;
    j_child3["object"]["send_ip"] = "127.0.0.2";

    nlohmann::json j_parent1;
    j_parent1["object"]["port"] = 11;
    j_parent1["other"]["port"] = 11;
    j_parent1["object"]["receive_ip"] = "11.1.1.111";

    nlohmann::json j_parent2;
    j_parent2["object"]["port"] = 22;
    j_parent2["other"]["port"] = 22;
    j_parent2["object"]["receive_ip"] = "22.2.2.222";

    auto config_parent1 = Configuration::CreateRootConfig("parent", j_parent1, false);
    config_parent1->SaveJson(TestConstants::TEST_DATA_PATH() + "temp/GOOGLETEST_parent1.json", true);

    auto config_parent2 = Configuration::CreateRootConfig("parent", j_parent2, false);
    config_parent2->SaveJson(TestConstants::TEST_DATA_PATH() + "temp/GOOGLETEST_parent2.json", true);

    auto config_child1 = Configuration::CreateRootConfig("child", j_child1, false);
    config_child1->SaveJson(TestConstants::TEST_DATA_PATH() + "temp/GOOGLETEST_child1.json", true);

    auto config_child2 = Configuration::CreateRootConfig("child", j_child2, false);
    config_child2->SaveJson(TestConstants::TEST_DATA_PATH() + "temp/GOOGLETEST_child2.json", true);

    auto config_child3 = Configuration::CreateRootConfig("child", j_child3, false);
    config_child3->SaveJson(TestConstants::TEST_DATA_PATH() + "temp/GOOGLETEST_child3.json", true);

    auto config_loaded1 = Configuration::CreateConfigFromFile(
        "loaded", TestConstants::TEST_DATA_PATH() + "temp/GOOGLETEST_child1.json", false);

    EXPECT_TRUE(
        config_loaded1->GetJson().dump().compare(
            "{\"ParentConfigFile\":\"GOOGLETEST_parent1.json\",\"object\":{\"port\":1111,\"receive_ip\":\"11.1.1."
            "111\",\"send_ip\":\"127.0.0.1\"},\"other\":{\"port\":11}}") == 0);

    auto config_loaded2 = Configuration::CreateConfigFromFile(
        "loaded", TestConstants::TEST_DATA_PATH() + "temp/GOOGLETEST_child2.json", false);
    EXPECT_TRUE(
        config_loaded2->GetJson().dump().compare(
            "{\"ParentConfigFile\":\"GOOGLETEST_parent2.json\",\"object\":{\"port\":2222,\"receive_ip\":\"22.2.2."
            "222\",\"send_ip\":\"127.0.0.2\"},\"other\":{\"port\":22}}") == 0);

    EXPECT_THROW(auto config_loaded3 = Configuration::CreateConfigFromFile(
                     "loaded", TestConstants::TEST_DATA_PATH() + "temp/GOOGLETEST_child3.json", false),
                 std::runtime_error);

    remove(std::string(TestConstants::TEST_DATA_PATH() + "temp/GOOGLETEST_parent1.json").c_str());
    remove(std::string(TestConstants::TEST_DATA_PATH() + "temp/GOOGLETEST_parent2.json").c_str());
    remove(std::string(TestConstants::TEST_DATA_PATH() + "temp/GOOGLETEST_child1.json").c_str());
    remove(std::string(TestConstants::TEST_DATA_PATH() + "temp/GOOGLETEST_child2.json").c_str());
    remove(std::string(TestConstants::TEST_DATA_PATH() + "temp/GOOGLETEST_child3.json").c_str());
}

TEST_F(ConfigTest, RootConfigTest)
{
    nlohmann::json j;
    j["object"]["port"] = 22;
    j["object"]["host"]["Name"]["extension"] = ".com";

    auto config = Configuration::CreateRootConfig("parent", j, false);

    auto config_child =
        config->GetConfigurationObject("object")->GetConfigurationObject("host")->GetConfigurationObject("Name");

    ASSERT_EQ(".com", config_child->GetValue<std::string>("extension", ".nl"));

    auto root_config = config_child->GetRootConfiguration();

    std::cout << root_config->GetJson().dump(4) << std::endl;

    ASSERT_EQ(22, root_config->GetConfigurationObject("object")->GetValue<int>("port", 30));
}

}  //namespace mpmca::utilities::testing
