/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/controllable_clock.hpp"

#include <gtest/gtest.h>

namespace mpmca::utilities::testing {
class ControllableClockTest : public ::testing::Test
{
  protected:
    ControllableClockTest() {}
};

TEST_F(ControllableClockTest, ClockTest)
{
    using namespace std::chrono_literals;
    ControllableClock clock(10ms, 0ms);

    EXPECT_EQ(0.0, clock.GetCurrentTime());
    EXPECT_EQ(0, clock.GetCurrentTimeMs());

    clock.SetCurrentTimeMs(13400);
    EXPECT_EQ(13.4, clock.GetCurrentTime());
    EXPECT_EQ(13400, clock.GetCurrentTimeMs());

    EXPECT_THROW(clock.SetCurrentTimeMs(13411), std::runtime_error);

    clock.Reset();
    EXPECT_EQ(0.0, clock.GetCurrentTime());
    EXPECT_EQ(0, clock.GetCurrentTimeMs());

    clock.Tick();
    EXPECT_EQ(0.01, clock.GetCurrentTime());
    EXPECT_EQ(10, clock.GetCurrentTimeMs());
}
}  //namespace mpmca::utilities::testing
