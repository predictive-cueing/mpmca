/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/data_properties.hpp"

#include <gtest/gtest.h>

namespace mpmca::utilities::testing {
class DataPropertiesTest : public ::testing::Test
{
  protected:
    DataPropertiesTest() {}
};

TEST_F(DataPropertiesTest, ConstructorTest)
{
    ASSERT_NO_THROW(DataProperties test("frame", "name", "units"));
    ASSERT_NO_THROW(DataProperties test("", "name", "units"));
    ASSERT_THROW(DataProperties test("frame", "", "units"), std::runtime_error);
    ASSERT_THROW(DataProperties test("frame", "name", ""), std::runtime_error);
    ASSERT_THROW(DataProperties test("frame", "", ""), std::runtime_error);
}

TEST_F(DataPropertiesTest, AllFieldSetTest)
{
    DataProperties test("frame", "name", "units");

    EXPECT_STRCASEEQ("frame", test.GetFrameOfReference().c_str());
    EXPECT_STRCASEEQ("name", test.GetName().c_str());
    EXPECT_STRCASEEQ("units", test.GetUnits().c_str());
    EXPECT_STRCASEEQ("frame_name_units", test.GetTotalName().c_str());
}

TEST_F(DataPropertiesTest, FrameOfReferenceNotSetTest)
{
    DataProperties test("", "name", "units");

    EXPECT_STRCASEEQ("", test.GetFrameOfReference().c_str());
    EXPECT_STRCASEEQ("name", test.GetName().c_str());
    EXPECT_STRCASEEQ("units", test.GetUnits().c_str());
    EXPECT_STRCASEEQ("name_units", test.GetTotalName().c_str());
}
}  //namespace mpmca::utilities::testing
