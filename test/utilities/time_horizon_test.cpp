/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <gtest/gtest.h>

#include "mpmca/utilities/configuration.hpp"
#include "mpmca/utilities/horizon.hpp"

namespace mpmca::utilities::testing {
class TimeHorizonTest : public ::testing::Test
{
  protected:
    TimeHorizonTest() {}
};

void CheckHorizons(const Horizon& horizon)
{
    int idx_prediction_horizon = 0;
    for (int i = 0; i < horizon.GetHorizonDeltaTimeSteps().size(); ++i) {
        EXPECT_EQ(horizon.GetHorizonTimeMs(i), horizon.GetPredictionHorizonTimeMs(idx_prediction_horizon));
        EXPECT_EQ(i, horizon.GetTimeMsToHorizonIndex(horizon.GetHorizonTimeMs(i)));

        int horizon_steps = horizon.GetHorizonDeltaTimeSteps(i);
        int prediction_steps = 0;

        while ((prediction_steps + horizon.GetPredictionHorizonDeltaTimeSteps(idx_prediction_horizon)) <=
               horizon_steps) {
            prediction_steps += horizon.GetPredictionHorizonDeltaTimeSteps(idx_prediction_horizon);
            EXPECT_EQ(idx_prediction_horizon, horizon.GetTimeMsToPredictionHorizonIndex(
                                                  horizon.GetPredictionHorizonTimeMs(idx_prediction_horizon)));

            ++idx_prediction_horizon;
        }
    }
}

TEST_F(TimeHorizonTest, TimeHorizonTest1)
{
    ConfigurationPtr config(Configuration::CreateConfigFromString("config1",
                                                                  "{\"Horizon\" : { \
																					\"PredictionMaximumTimeStepMs\" : 10, \
																					\"Steps\" : [ {\"StepMs\": 10, \"Count\": 5}, \
																								{\"StepMs\": 30, \"Count\": 3}, \
																								{\"StepMs\": 50, \"Count\": 3}, \
																								{\"StepMs\": 80, \"Count\": 3}, \
																								{\"StepMs\": 110, \"Count\": 3} ] } }"));
    Horizon horizon(config);
    CheckHorizons(horizon);
}

TEST_F(TimeHorizonTest, TimeHorizonTest5)
{
    ConfigurationPtr config(Configuration::CreateConfigFromString("config1",
                                                                  "{\
																		\"Horizon\" : { \
																			\"PredictionMaximumTimeStepMs\" : 50, \
																			\"Steps\" : [ \
																				{ \"Count\": 26, \"StepMs\": 10 },\
																				{ \"Count\":  1, \"StepMs\": 20 },\
																				{ \"Count\":  1, \"StepMs\": 30 },\
																				{ \"Count\":  1, \"StepMs\": 40 },\
																				{ \"Count\":  1, \"StepMs\": 50 },\
																				{ \"Count\":  1, \"StepMs\": 60 },\
																				{ \"Count\":  1, \"StepMs\": 70 },\
																				{ \"Count\":  1, \"StepMs\": 80 },\
																				{ \"Count\":  1, \"StepMs\": 90 },\
																				{ \"Count\":  1, \"StepMs\": 110 },\
																				{ \"Count\":  1, \"StepMs\": 120 },\
																				{ \"Count\":  1, \"StepMs\": 130 },\
																				{ \"Count\":  1, \"StepMs\": 140 },\
																				{ \"Count\":  1, \"StepMs\": 150 },\
																				{ \"Count\":  1, \"StepMs\": 160 },\
																				{ \"Count\":  1, \"StepMs\": 170 },\
																				{ \"Count\":  1, \"StepMs\": 180 },\
																				{ \"Count\":  1, \"StepMs\": 190 },\
																				{ \"Count\":  1, \"StepMs\": 200 },\
																				{ \"Count\":  1, \"StepMs\": 210 },\
																				{ \"Count\":  1, \"StepMs\": 220 },\
																				{ \"Count\":  1, \"StepMs\": 230 },\
																				{ \"Count\":  1, \"StepMs\": 240 },\
																				{ \"Count\":  27, \"StepMs\": 250 } ] \
																			 }\
																		 }"));

    Horizon horizon(config);
    CheckHorizons(horizon);
}

TEST_F(TimeHorizonTest, TimeHorizonTest2)
{
    ConfigurationPtr config(Configuration::CreateConfigFromString("config1",
                                                                  "{\
																	 \"Horizon\" : { \
																		\"PredictionMaximumTimeStepMs\" : 50, \
																		\"Steps\" : [ {\"StepMs\": 10, \"Count\": 5}, \
																				  {\"StepMs\": 30, \"Count\": 3}, \
																				  {\"StepMs\": 50, \"Count\": 3}, \
																				  {\"StepMs\": 80, \"Count\": 3}, \
																				  {\"StepMs\": 110, \"Count\": 3} ] } }"));
    Horizon horizon(config);
    CheckHorizons(horizon);
}

TEST_F(TimeHorizonTest, TimeHorizonTest3)
{
    ConfigurationPtr config(
        Configuration::CreateConfigFromString("config1",
                                              "{\"Horizon\" : {\"PredictionMaximumTimeStepMs\" : 150, \
																		\"Steps\" : [ {\"StepMs\": 10, \"Count\": 5}, \
																					{\"StepMs\": 30, \"Count\": 3}, \
																					{\"StepMs\": 50, \"Count\": 3}, \
																					{\"StepMs\": 80, \"Count\": 3}, \
																					{\"StepMs\": 110, \"Count\": 3} ] } }"));
    Horizon horizon(config);
    CheckHorizons(horizon);

    EXPECT_EQ(horizon.GetTimeMsToPredictionHorizonIndex(0), 0);
    EXPECT_EQ(horizon.GetTimeMsToPredictionHorizonIndex(10), 1);
    EXPECT_EQ(horizon.GetTimeMsToPredictionHorizonIndex(20), 2);
    EXPECT_EQ(horizon.GetTimeMsToPredictionHorizonIndex(30), 3);
    EXPECT_EQ(horizon.GetTimeMsToPredictionHorizonIndex(40), 4);
    EXPECT_EQ(horizon.GetTimeMsToPredictionHorizonIndex(50), 5);
    EXPECT_THROW(horizon.GetTimeMsToPredictionHorizonIndex(60), std::invalid_argument);
    EXPECT_EQ(horizon.GetTimeMsToPredictionHorizonIndex(80), 6);
}

}  //namespace mpmca::utilities::testing
