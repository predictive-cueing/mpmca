/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/utilities/udp_sender_receiver.hpp"

#include <gtest/gtest.h>

namespace mpmca::utilities::testing {
class UdpSenderReceiverTest : public ::testing::Test
{
  protected:
    UdpSenderReceiverTest() {}
};

TEST_F(UdpSenderReceiverTest, IntegerMultipleTest)
{
    ASSERT_TRUE(UdpSenderReceiver::IsIntegerMultiple<uint16_t>(10, 1));
    ASSERT_TRUE(UdpSenderReceiver::IsIntegerMultiple<uint16_t>(20, 10));
    ASSERT_TRUE(UdpSenderReceiver::IsIntegerMultiple<uint16_t>(25, 5));
    ASSERT_FALSE(UdpSenderReceiver::IsIntegerMultiple<uint16_t>(25, 3));
    ASSERT_FALSE(UdpSenderReceiver::IsIntegerMultiple<uint16_t>(19, 8));

    ASSERT_TRUE(UdpSenderReceiver::IsIntegerMultiple<int>(10, 1));
    ASSERT_TRUE(UdpSenderReceiver::IsIntegerMultiple<int>(20, 10));
    ASSERT_TRUE(UdpSenderReceiver::IsIntegerMultiple<int>(25, 5));
    ASSERT_FALSE(UdpSenderReceiver::IsIntegerMultiple<int>(25, 3));
    ASSERT_FALSE(UdpSenderReceiver::IsIntegerMultiple<int>(19, 8));

    ASSERT_TRUE(UdpSenderReceiver::IsIntegerMultiple<double>(10, 1));
    ASSERT_TRUE(UdpSenderReceiver::IsIntegerMultiple<double>(20, 10));
    ASSERT_TRUE(UdpSenderReceiver::IsIntegerMultiple<double>(25, 5));
    ASSERT_FALSE(UdpSenderReceiver::IsIntegerMultiple<double>(25, 3));
    ASSERT_FALSE(UdpSenderReceiver::IsIntegerMultiple<double>(19, 8));
}

}  //namespace mpmca::utilities::testing
