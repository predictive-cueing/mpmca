/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */

#include <gtest/gtest.h>

#include <algorithm>
#include <cmath>

#include "mpmca/utilities/time_measurement.hpp"

namespace mpmca::utilities::testing {
class TimeMeasurementTest : public ::testing::Test
{
  protected:
    TimeMeasurementTest() {}
};

TEST_F(TimeMeasurementTest, MethodsTest)
{
    TimeMeasurement tm;

    ClockIndex apple = tm.RegisterClock("apple");
    ClockIndex orange = tm.RegisterClock("orange");

    EXPECT_TRUE(tm.GetNumClocks() == 2);
    EXPECT_TRUE(tm.GetClockDurationSinceCycleStartNs(apple) == 0);
    EXPECT_TRUE(tm.GetClockDurationSinceCycleStartNs(orange) == 0);

    tm.StartCycle();
    tm.StopClock(apple);
    tm.StopClock(orange);

    EXPECT_TRUE(tm.GetClockDurationSinceCycleStartNs(orange) > tm.GetClockDurationSinceCycleStartNs(apple));

    tm.StopClock(orange);
    tm.StopClock(apple);

    EXPECT_TRUE(tm.GetClockDurationSinceCycleStartNs(apple) > tm.GetClockDurationSinceCycleStartNs(orange));

    tm.StartCycle();
    tm.StopClock(apple);
    tm.StopClock(orange);

    EXPECT_TRUE(tm.GetClockDurationSinceCycleStartNs(orange) > tm.GetClockDurationSinceCycleStartNs(apple));

    tm.StopClock(orange);
    tm.StopClock(apple);

    EXPECT_TRUE(tm.GetClockDurationSinceCycleStartNs(apple) > tm.GetClockDurationSinceCycleStartNs(orange));
}

TEST_F(TimeMeasurementTest, SecondTest)
{
    TimeMeasurement tm;

    ClockIndex apple(tm.RegisterClock("apple"));
    ClockIndex orange(tm.RegisterClock("orange"));
    ClockIndex pear(tm.RegisterClock("pear"));
    ClockIndex strawberry(tm.RegisterClock("strawberry"));
    ClockIndex banana(tm.RegisterClock("banana"));

    EXPECT_TRUE(apple.GetIndex() == 0);
    EXPECT_TRUE(orange.GetIndex() == 1);
    EXPECT_TRUE(pear.GetIndex() == 2);
    EXPECT_TRUE(strawberry.GetIndex() == 3);
    EXPECT_TRUE(banana.GetIndex() == 4);

    EXPECT_TRUE(tm.GetNumClocks() == 5);

    tm.StartCycle();
    tm.StopClock(apple);
    tm.StopClock(orange);
    tm.StopClock(pear);
    tm.StopClock(strawberry);
    tm.StopClock(banana);

    EXPECT_TRUE(tm.GetClockDurationSinceCycleStartNs(banana) > tm.GetClockDurationSinceCycleStartNs(strawberry));
    EXPECT_TRUE(tm.GetClockDurationSinceCycleStartNs(strawberry) > tm.GetClockDurationSinceCycleStartNs(pear));
    EXPECT_TRUE(tm.GetClockDurationSinceCycleStartNs(pear) > tm.GetClockDurationSinceCycleStartNs(orange));
    EXPECT_TRUE(tm.GetClockDurationSinceCycleStartNs(orange) > tm.GetClockDurationSinceCycleStartNs(apple));
    EXPECT_TRUE(tm.GetClockDurationSinceCycleStartNs(apple) > 0);

    EXPECT_TRUE(tm.GetClockDurationSincePreviousNs(banana) > 0);
    EXPECT_TRUE(tm.GetClockDurationSincePreviousNs(strawberry) > 0);
    EXPECT_TRUE(tm.GetClockDurationSincePreviousNs(pear) > 0);
    EXPECT_TRUE(tm.GetClockDurationSincePreviousNs(orange) > 0);
    EXPECT_TRUE(tm.GetClockDurationSincePreviousNs(apple) > 0);

    tm.StartCycle();
    tm.StopClock(apple);
    tm.StopClock(strawberry);
    tm.StopClock(banana);

    EXPECT_TRUE(tm.GetClockDurationSinceCycleStartNs(banana) > tm.GetClockDurationSinceCycleStartNs(strawberry));
    EXPECT_TRUE(tm.GetClockDurationSinceCycleStartNs(strawberry) > tm.GetClockDurationSinceCycleStartNs(pear));
    EXPECT_TRUE(tm.GetClockDurationSinceCycleStartNs(pear) == tm.GetClockDurationSinceCycleStartNs(orange));
    EXPECT_TRUE(tm.GetClockDurationSinceCycleStartNs(orange) == 0);
    EXPECT_TRUE(tm.GetClockDurationSinceCycleStartNs(apple) > 0);

    EXPECT_TRUE(tm.GetClockDurationSincePreviousNs(banana) > 0);
    EXPECT_TRUE(tm.GetClockDurationSincePreviousNs(strawberry) > 0);
    EXPECT_TRUE(tm.GetClockDurationSincePreviousNs(pear) == 0);
    EXPECT_TRUE(tm.GetClockDurationSincePreviousNs(orange) == 0);
    EXPECT_TRUE(tm.GetClockDurationSincePreviousNs(apple) > 0);

    EXPECT_TRUE(tm.GetClockDurationsSincePreviousNs()[banana.GetIndex()] == tm.GetClockDurationSincePreviousNs(banana));
    EXPECT_TRUE(tm.GetClockDurationsSincePreviousNs()[strawberry.GetIndex()] ==
                tm.GetClockDurationSincePreviousNs(strawberry));
    EXPECT_TRUE(tm.GetClockDurationsSincePreviousNs()[pear.GetIndex()] == tm.GetClockDurationSincePreviousNs(pear));
    EXPECT_TRUE(tm.GetClockDurationsSincePreviousNs()[orange.GetIndex()] == tm.GetClockDurationSincePreviousNs(orange));
    EXPECT_TRUE(tm.GetClockDurationsSincePreviousNs()[apple.GetIndex()] == tm.GetClockDurationSincePreviousNs(apple));

    EXPECT_TRUE(tm.GetClockDurationsSincePreviousMs()[banana.GetIndex()] == tm.GetClockDurationSincePreviousMs(banana));
    EXPECT_TRUE(tm.GetClockDurationsSincePreviousMs()[strawberry.GetIndex()] ==
                tm.GetClockDurationSincePreviousMs(strawberry));
    EXPECT_TRUE(tm.GetClockDurationsSincePreviousMs()[pear.GetIndex()] == tm.GetClockDurationSincePreviousMs(pear));
    EXPECT_TRUE(tm.GetClockDurationsSincePreviousMs()[orange.GetIndex()] == tm.GetClockDurationSincePreviousMs(orange));
    EXPECT_TRUE(tm.GetClockDurationsSincePreviousMs()[apple.GetIndex()] == tm.GetClockDurationSincePreviousMs(apple));
}

TEST_F(TimeMeasurementTest, StatisticsTest)
{
    TimeMeasurement tm;

    ClockIndex apple(tm.RegisterClock("apple"));
    ClockIndex orange(tm.RegisterClock("orange"));
    ClockIndex pear(tm.RegisterClock("pear"));
    ClockIndex strawberry(tm.RegisterClock("strawberry"));
    ClockIndex banana(tm.RegisterClock("banana"));

    for (int i = 0; i < 1000000; i++) {
        tm.StartCycle();
        double a = (double)i / 100.0;
        tm.StopClock(apple);
        double b = std::sin(a * 2);
        tm.StopClock(orange);
        double c = std::min(a, b);
        tm.StopClock(pear);
        double d = 0;
        d += std::sqrt(a) + b * b - c * a;
        tm.StopClock(strawberry);
    }

    tm.PrintStatisticsNs();
}
}  //namespace mpmca::utilities::testing
