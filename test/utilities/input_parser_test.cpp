/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <gtest/gtest.h>

#include "mpmca/utilities/command_line_options_parser.hpp"

namespace mpmca::utilities::testing {
class CommandLineOptionsParserTest : public ::testing::Test
{
  protected:
    CommandLineOptionsParserTest() {}
};

TEST_F(CommandLineOptionsParserTest, ArgumentsTest)
{
    char* argv[] = {(char*)("executable_name"),
                    (char*)("argument1"),
                    (char*)("value1"),
                    (char*)("argument2"),
                    (char*)("value2"),
                    (char*)("argument3=value3"),
                    (char*)("argument4=12387a6ca&1241)**12")};
    int argc = sizeof(argv) / sizeof(char*);

    CommandLineOptionsParser parser(argc, argv);

    ASSERT_TRUE(parser.GetCommandOptionExists("argument1"));
    ASSERT_EQ(parser.GetCommandOption("argument1"), "value1");

    ASSERT_TRUE(parser.GetCommandOptionExists("argument3"));
    ASSERT_EQ(parser.GetCommandOption("argument3"), "value3");

    ASSERT_TRUE(parser.GetCommandOptionExists("argument4"));
    ASSERT_EQ(parser.GetCommandOption("argument4"), "12387a6ca&1241)**12");
}

}  //namespace mpmca::utilities::testing
