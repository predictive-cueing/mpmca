/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <gtest/gtest.h>

#include "mpmca/messages/automotive_prediction_path.hpp"
#include "mpmca/messages/mcap_test_message.hpp"
#include "mpmca/pipeline/message_bus.hpp"
#include "mpmca/visualization/automotive_prediction_path_convertor.hpp"
#include "mpmca/visualization/foxglove_message_convertor.hpp"
#include "mpmca/visualization/mcap_test_message_convertor.hpp"

namespace mpmca::testing {

class FoxgloveMessageConversionTest : public ::testing::Test
{
  protected:
    FoxgloveMessageConversionTest() {}
};

TEST_F(FoxgloveMessageConversionTest, LinkerTest)
{
}
TEST_F(FoxgloveMessageConversionTest, MessageBusTest)
{
}
}  //namespace mpmca::testing