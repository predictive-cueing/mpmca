/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/visualization/info_view_convertor.hpp"

#include <gtest/gtest.h>

namespace mpmca::visualization::testing {
struct TestSimulator
{
    static constexpr std::size_t NY_SIMULATOR = 9;
    using ComponentList = control::templates::ComponentList<mpmca::control::components::XyTable,  //
                                                            mpmca::control::components::Hexapod,
                                                            mpmca::control::components::TransformationMatrix,  //
                                                            mpmca::control::components::YawTable,
                                                            mpmca::control::components::YzTracks,  //
                                                            mpmca::control::components::Gimbal>;
    using Dimension = control::templates::DimensionsT<TestSimulator>;
    using AxesVector = Vector<Dimension::NQ>;
    using StateVector = Vector<Dimension::NX>;
    using AlgStateVector = Vector<Dimension::NZ>;
    using InputVector = Vector<Dimension::NU>;
    using ConstraintVector = Vector<Dimension::NC>;
    using ParameterVector = Vector<Dimension::NP>;
    using OutputVector = Vector<Dimension::NY>;
    using InfoVector = Vector<Dimension::NI>;

    using StateStateMatrix = Matrix<Dimension::NX, Dimension::NX>;
    using StateInputMatrix = Matrix<Dimension::NX, Dimension::NU>;
    using InputInputMatrix = Matrix<Dimension::NU, Dimension::NU>;
    using OutputOutputMatrix = Matrix<Dimension::NY, Dimension::NY>;
    using OutputStateMatrix = Matrix<Dimension::NY, Dimension::NX>;
    using OutputInputMatrix = Matrix<Dimension::NY, Dimension::NU>;
    using ConstraintStateMatrix = Matrix<Dimension::NC, Dimension::NX>;
    using ConstraintInputMatrix = Matrix<Dimension::NC, Dimension::NU>;

    using OutputStateStateMatrix = Matrix<Dimension::NX * Dimension::NY, Dimension::NX>;
    using OutputStateInputMatrix = Matrix<Dimension::NX * Dimension::NY, Dimension::NU>;
    using OutputInputInputMatrix = Matrix<Dimension::NU * Dimension::NY, Dimension::NU>;
    using ConstraintStateStateMatrix = Matrix<Dimension::NX * Dimension::NC, Dimension::NX>;
    using ConstraintStateInputMatrix = Matrix<Dimension::NX * Dimension::NC, Dimension::NU>;
    using ConstraintInputInputMatrix = Matrix<Dimension::NU * Dimension::NC, Dimension::NU>;

    using InputMatrix = Eigen::Matrix<double, Dimension::NU, Eigen::Dynamic>;
    using StateMatrix = Eigen::Matrix<double, Dimension::NX, Eigen::Dynamic>;
    using OutputMatrix = Eigen::Matrix<double, Dimension::NY, Eigen::Dynamic>;
};

class InfoViewConvertorTest : public ::testing::Test
{
  protected:
    using d = TestSimulator::Dimension;  // DimensionsT<simulator, 9>;
};

TEST_F(InfoViewConvertorTest, TestDimensions)
{
    TestSimulator::StateVector state_vector = TestSimulator::StateVector::Random();
    TestSimulator::InfoVector info = TestSimulator::InfoVector::Random();
    TestSimulator::StateVector state_lower_bound = TestSimulator::StateVector::Constant(-10);
    TestSimulator::StateVector state_upper_bound = TestSimulator::StateVector::Constant(10);
    TestSimulator::ConstraintVector constraint_lower_bound = TestSimulator::ConstraintVector::Constant(-10);
    TestSimulator::ConstraintVector constraint_upper_bound = TestSimulator::ConstraintVector::Constant(10);
    PoseVector head_pose = PoseVector::Random();
    TransformationMatrix final_transform = TransformationMatrix::Random();

    auto state_view = mpmca::control::MakeStateView<TestSimulator>(state_vector);
    auto info_view = mpmca::control::MakeInfoView<TestSimulator>(info);
    auto state_lb_view = mpmca::control::MakeStateView<TestSimulator>(state_lower_bound);
    auto state_ub_view = mpmca::control::MakeStateView<TestSimulator>(state_upper_bound);
    auto constraint_lb_view = mpmca::control::MakeConstraintView<TestSimulator>(constraint_lower_bound);
    auto constraint_ub_view = mpmca::control::MakeConstraintView<TestSimulator>(constraint_upper_bound);

    auto models = InfoViewConvertor<TestSimulator>::MakeAllModels(info_view, state_lb_view, state_ub_view,
                                                                  constraint_lb_view, constraint_ub_view);
    auto frame_transforms =
        InfoViewConvertor<TestSimulator>::MakeAllFrameTransforms(state_view, info_view, final_transform, head_pose);

    for (auto *model : models)
        std::cout << "----" << std::endl << model->DebugString() << std::endl;
}

}  //namespace mpmca::visualization::testing
