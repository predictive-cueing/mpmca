/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/dokblocks/core/block_diagram.hpp"

#include <gtest/gtest.h>

#include <memory>

#include "mpmca/constants.hpp"
#include "mpmca/dokblocks/components/algebraic_function.hpp"
#include "mpmca/dokblocks/components/state_space.hpp"

namespace mpmca::dokblocks::testing {
class BlockDiagramTest : public ::testing::Test
{
  protected:
    static constexpr int NX = 3;
    static constexpr int NU = 2;
    static constexpr int NY = 2;
    Real dt = 0.1;

    BlockDiagramTest() {}

    template <std::size_t NX, std::size_t NU, std::size_t NY>
    StateSpace<NX, NU, NY> MakeRandomStateSpace()
    {
        Matrix<NX, NX> A = Matrix<NX, NX>::Random();
        Matrix<NX, NU> B = Matrix<NX, NU>::Random();
        Matrix<NY, NX> C = Matrix<NY, NX>::Random();
        Matrix<NY, NU> D = Matrix<NY, NU>::Random();

        return StateSpace("random_state_space", A, B, C, D);
    }

    auto GetTestFunction()
    {
        Vector<2> u_offset = Vector<2>::Constant(1.);
        std::vector<Real> par{1., 2., 3.};

        auto fun = [par = par, u_offset = u_offset](Vector<2> u) {
            u = u - u_offset;
            Real y1 = 2. / constants::kPi * (1. - std::exp(-pow(par[0], u.sum()))) *
                      std::atan(2. * u.prod() / (par[0] * constants::kPi));
            Real y2 = 2. / constants::kPi * (1. - std::exp(-pow(par[1], u.sum()))) *
                      std::atan(2. * u.prod() / (par[1] * constants::kPi));
            Real y3 = 2. / constants::kPi * (1. - std::exp(-pow(par[2], u.sum()))) *
                      std::atan(2. * u.prod() / (par[2] * constants::kPi));
            return (Vector<3>() << y1, y2, y3).finished();
        };

        return fun;
    }

    auto MakeMockStateSpaceMIMO1()
    {
        auto x0 = Vector<NX>::Constant(1.);

        Matrix<NX, NX> A;
        A << 1, 2, 3, 4, 5, 6, 7, 8, 9;

        Matrix<NX, NU> B;
        B << 10, 11, 12, 13, 14, 15;

        Matrix<NY, NX> C;
        C << 16, 17, 18, 19, 20, 21;

        Matrix<NY, NU> D;
        D << 0, 0, 0, 0;

        return StateSpace("mock_state_space_MIMO1", A, B, C, D, x0, dt);
    }

    auto MakeMockStateSpaceMIMO2()
    {
        auto x0 = Vector<NX>::Constant(2.);

        Matrix<NX, NX> A;
        A << 101, 102, 103, 104, 105, 106, 107, 108, 109;

        Matrix<NX, NU> B;
        B << 110, 111, 112, 113, 114, 115;

        Matrix<NY, NX> C;
        C << 116, 117, 118, 119, 120, 121;

        Matrix<NY, NU> D;
        D << 122, 123, 124, 125;

        return StateSpace("mock_state_space_MIMO2", A, B, C, D, x0, dt);
    }

    auto MakeMockStateSpace_gain()
    {
        auto x0 = Vector<1>::Zero();

        Matrix<1, 1> A = Matrix<1, 1>::Zero();
        Matrix<1, NU> B = Matrix<1, NU>::Zero();
        Matrix<NY, 1> C = Matrix<NY, 1>::Zero();
        Matrix<NY, NU> D;
        D << 2, 0, 0, 3;

        return StateSpace("mock_state_space_gain", A, B, C, D, x0, dt);
    }

    auto MakeFunctionGain()
    {
        auto fun = [](Vector<NX> u) {
            Real y1 = u[0] * 2;
            Real y2 = u[1] * 3;
            return (Vector<NY>() << y1, y2).finished();
        };

        return fun;
    }
};

TEST_F(BlockDiagramTest, GivenBlockParallelOfStateSpaces_whileCalculatingOutput_thenEqualToStateSpaceParallel)
{
    auto ss1 = MakeMockStateSpaceMIMO1();
    auto ss2 = MakeMockStateSpaceMIMO2();
    auto ss_blocks = Parallel("mock_parallel", ss1, ss2);

    auto u = Vector<NU>::Constant(3.);
    auto y_blocks = ss_blocks.Simulate(u);

    auto ss_ss = MakeParallel("mock_parallel", ss1, ss2);
    auto y_ss = ss_ss.Simulate(u);
    ASSERT_TRUE(y_blocks.isApprox(y_ss));
}

TEST_F(BlockDiagramTest, GivenBlockParallelOfStateSpaces_whileUpdatingStates_thenEqualToStateSpaceParallel)
{
    auto ss1 = MakeMockStateSpaceMIMO1();
    auto ss2 = MakeMockStateSpaceMIMO2();
    auto ss_blocks = Parallel("mock_parallel", ss1, ss2);
    ss_blocks.SetSimulationStep(dt);

    auto u = Vector<NU>::Constant(3.);
    ss_blocks.UpdateState(u);

    auto ss_ss = MakeParallel("mock_parallel", ss1, ss2);
    ss_ss.SetSimulationStep(dt);
    ss_ss.UpdateState(u);
    ASSERT_TRUE(ss_blocks.GetState().isApprox(ss_ss.GetState()));
}

TEST_F(BlockDiagramTest, GivenBlockParallelOfStateSpaceAndFunction_whileCalculatingOutput_thenEqualToStateSpaceParallel)
{
    auto ss1 = MakeMockStateSpace_gain();
    auto ss2 = MakeMockStateSpaceMIMO2();
    auto ss_blocks = Parallel("mock_parallel", ss1, ss2);

    auto u = Vector<NU>::Constant(3.);
    auto y_blocks = ss_blocks.Simulate(u);

    auto ss_ss = MakeParallel("mock_parallel", ss1, ss2);
    auto y_ss = ss_ss.Simulate(u);
    ASSERT_TRUE(y_blocks.isApprox(y_ss));
}

TEST_F(BlockDiagramTest, GivenBlockParallelOfStateSpaceAndFunction_whileUpdatingStates_thenEqualToStateSpaceParallel)
{
    auto ss1 = MakeMockStateSpace_gain();
    auto ss2 = MakeMockStateSpaceMIMO2();
    auto ss_blocks = Parallel("mock_parallel", ss1, ss2);
    ss_blocks.SetSimulationStep(dt);

    auto u = Vector<NU>::Constant(3.);
    ss_blocks.UpdateState(u);

    auto ss_ss = MakeParallel("mock_parallel", ss1, ss2);
    ss_ss.SetSimulationStep(dt);
    ss_ss.UpdateState(u);
    ASSERT_TRUE(ss_blocks.GetState().isApprox(ss_ss.GetState()));
}

TEST_F(BlockDiagramTest, GivenBlockSeriesOfStateSpaces_whileCalculatingOutput_thenEqualToStateSpaceSeries)
{
    auto ss1 = MakeMockStateSpaceMIMO1();
    auto ss2 = MakeMockStateSpaceMIMO2();
    auto ss_blocks = Series("mock_series", ss1, ss2);

    auto u = Vector<NU>::Constant(3.);
    auto y_blocks = ss_blocks.Simulate(u);

    auto ss_ss = MakeSeries("mock_series", ss1, ss2);
    auto y_ss = ss_ss.Simulate(u);
    ASSERT_TRUE(y_blocks.isApprox(y_ss));
}

TEST_F(BlockDiagramTest, GivenBlockSeriesOfStateSpaces_whileUpdatingStates_thenEqualToStateSpaceSeries)
{
    auto ss1 = MakeMockStateSpaceMIMO1();
    auto ss2 = MakeMockStateSpaceMIMO2();
    auto ss_blocks = Series("mock_series", ss1, ss2);
    ss_blocks.SetSimulationStep(dt);

    auto u = Vector<NU>::Constant(3.);
    ss_blocks.UpdateState(u);

    auto ss_ss = MakeSeries("mock_series", ss1, ss2);
    ss_ss.SetSimulationStep(dt);
    ss_ss.UpdateState(u);
    ASSERT_TRUE(ss_blocks.GetState().isApprox(ss_ss.GetState()));
}

TEST_F(BlockDiagramTest, GivenBlockSeriesOfStateSpaceAndFunction_whileCalculatingOutput_thenEqualToStateSpaceSeries)
{
    auto ss1 = MakeMockStateSpace_gain();
    auto ss2 = MakeMockStateSpaceMIMO2();
    auto ss_blocks = Series("mock_series", ss1, ss2);

    auto u = Vector<NU>::Constant(3.);
    auto y_blocks = ss_blocks.Simulate(u);

    auto ss_ss = MakeSeries("mock_series", ss1, ss2);
    auto y_ss = ss_ss.Simulate(u);
    ASSERT_TRUE(y_blocks.isApprox(y_ss));
}

TEST_F(BlockDiagramTest, GivenBlockSeriesOfStateSpaceAndFunction_whileUpdatingStates_thenEqualToStateSpaceSeries)
{
    auto ss1 = MakeMockStateSpace_gain();
    auto ss2 = MakeMockStateSpaceMIMO2();
    auto ss_blocks = Series("mock_series", ss1, ss2);
    ss_blocks.SetSimulationStep(dt);

    auto u = Vector<NU>::Constant(3.);
    ss_blocks.UpdateState(u);

    auto ss_ss = MakeSeries("mock_series", ss1, ss2);
    ss_ss.SetSimulationStep(dt);
    ss_ss.UpdateState(u);
    ASSERT_TRUE(ss_blocks.GetState().isApprox(ss_ss.GetState()));
}

TEST_F(BlockDiagramTest, GivenBlockFeedbackOfStateSpace_whileCalculatingOutput_thenEqualToStateSpaceFeedback)
{
    auto ss1 = MakeMockStateSpaceMIMO1();
    auto ss2 = MakeMockStateSpaceMIMO2();
    auto ss_blocks = Feedback("mock_feedback", ss1, ss2);

    auto u = Vector<NU>::Constant(3.);
    auto y_blocks = ss_blocks.Simulate(u);

    auto ss_ss = MakeFeedback("mock_feedback", ss1, ss2);
    auto y_ss = ss_ss.Simulate(u);
    ASSERT_TRUE(y_blocks.isApprox(y_ss));
}

TEST_F(BlockDiagramTest, GivenBlockFeedbackOfStateSpace_whileUpdatingStates_thenEqualToStateSpaceFeedback)
{
    auto ss1 = MakeMockStateSpaceMIMO1();
    auto ss2 = MakeMockStateSpaceMIMO2();
    auto ss_blocks = Feedback("mock_feedback", ss1, ss2);
    ss_blocks.SetSimulationStep(dt);

    auto u = Vector<NU>::Constant(3.);
    ss_blocks.UpdateState(u);

    auto ss_ss = MakeFeedback("mock_feedback", ss1, ss2);
    ss_ss.SetSimulationStep(dt);
    ss_ss.UpdateState(u);
    ASSERT_TRUE(ss_blocks.GetState().isApprox(ss_ss.GetState()));
}

TEST_F(BlockDiagramTest, GivenBlockFeedbackOfStateSpaceAndFunction_whileCalculatingOutput_thenEqualToStateSpaceFeedback)
{
    auto ss1 = MakeMockStateSpaceMIMO1();
    auto ss2 = MakeMockStateSpace_gain();
    auto ss_blocks = Feedback("mock_feedback", ss1, ss2);

    auto u = Vector<NU>::Constant(3.);
    auto y_blocks = ss_blocks.Simulate(u);

    auto ss_ss = MakeFeedback("mock_feedback", ss1, ss2);
    auto y_ss = ss_ss.Simulate(u);
    ASSERT_TRUE(y_blocks.isApprox(y_ss));
}

TEST_F(BlockDiagramTest, GivenBlockFeedbackOfStateSpaceAndFunction_whileUpdatingStates_thenEqualToStateSpaceFeedback)
{
    auto ss1 = MakeMockStateSpaceMIMO1();
    auto ss2 = MakeMockStateSpaceMIMO2();
    auto ss_blocks = Feedback("mock_feedback", ss1, ss2);
    ss_blocks.SetSimulationStep(dt);

    auto u = Vector<NU>::Constant(3.);
    ss_blocks.UpdateState(u);

    auto ss_ss = MakeFeedback("mock_feedback", ss1, ss2);
    ss_ss.SetSimulationStep(dt);
    ss_ss.UpdateState(u);
    ASSERT_TRUE(ss_blocks.GetState().isApprox(ss_ss.GetState()));
}

}  //namespace mpmca::dokblocks::testing