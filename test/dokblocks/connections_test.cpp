/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/dokblocks/core/connections.hpp"

#include <gtest/gtest.h>

#include <string>

#include "mpmca/dokblocks/components/state_space.hpp"
#include "mpmca/dokblocks/core/conversions.hpp"
#include "mpmca/dokblocks/core/matrix_types.hpp"

namespace mpmca::dokblocks::testing {
class ConnectionTest : public ::testing::TestWithParam<std::string>
{
  protected:
    ConnectionTest() {}

    template <std::size_t NX, std::size_t NU, std::size_t NY>
    StateSpace<NX, NU, NY> MakeRandomStateSpace()
    {
        Matrix<NX, NX> A = Matrix<NX, NX>::Random();
        Matrix<NX, NU> B = Matrix<NX, NU>::Random();
        Matrix<NY, NX> C = Matrix<NY, NX>::Random();
        Matrix<NY, NU> D = Matrix<NY, NU>::Random();

        return StateSpace("random_state_space", A, B, C, D);
    }

    StateSpace<3, 2, 2> MakeMockStateSpaceMIMO1()
    {
        Matrix<3, 3> A;
        A << 1, 2, 3, 4, 5, 6, 7, 8, 9;

        Matrix<3, 2> B;
        B << 10, 11, 12, 13, 14, 15;

        Matrix<2, 3> C;
        C << 16, 17, 18, 19, 20, 21;

        Matrix<2, 2> D;
        D << 22, 23, 24, 25;

        return StateSpace("test_state_space", A, B, C, D);
    }

    StateSpace<3, 2, 2> MakeMockStateSpaceMIMO2()
    {
        Matrix<3, 3> A;
        A << 101, 102, 103, 104, 105, 106, 107, 108, 109;

        Matrix<3, 2> B;
        B << 110, 111, 112, 113, 114, 115;

        Matrix<2, 3> C;
        C << 116, 117, 118, 119, 120, 121;

        Matrix<2, 2> D;
        D << 122, 123, 124, 125;

        return StateSpace("test_state_space", A, B, C, D);
    }

    StateSpace<6, 4, 2> SumOutputsFromOctave()
    {
        Matrix<6, 6> A;
        A << 1, 2, 3, 0, 0, 0, 4, 5, 6, 0, 0, 0, 7, 8, 9, 0, 0, 0, 0, 0, 0, 101, 102, 103, 0, 0, 0, 104, 105, 106, 0, 0,
            0, 107, 108, 109;

        Matrix<6, 4> B;
        B << 10, 11, 0, 0, 12, 13, 0, 0, 14, 15, 0, 0, 0, 0, 110, 111, 0, 0, 112, 113, 0, 0, 114, 115;

        Matrix<2, 6> C;
        C << 16, 17, 18, 116, 117, 118, 19, 20, 21, 119, 120, 121;

        Matrix<2, 4> D;
        D << 22, 23, 122, 123, 24, 25, 124, 125;

        return StateSpace("test_state_space", A, B, C, D);
    }

    StateSpace<6, 2, 2> SeriesFromOctave()
    {
        Matrix<6, 6> A;
        A << 101, 102, 103, 3869, 4090, 4311, 104, 105, 106, 3939, 4164, 4389, 107, 108, 109, 4009, 4238, 4467, 0, 0, 0,
            1, 2, 3, 0, 0, 0, 4, 5, 6, 0, 0, 0, 7, 8, 9;

        Matrix<6, 2> B;
        B << 5084, 5305, 5176, 5401, 5268, 5497, 10, 11, 12, 13, 14, 15;

        Matrix<2, 6> C;
        C << 116, 117, 118, 4289, 4534, 4779, 119, 120, 121, 4359, 4608, 4857;

        Matrix<2, 2> D;
        D << 5636, 5881, 5728, 5977;
        return StateSpace("test_state_space", A, B, C, D);
    }

    StateSpace<6, 2, 2> ParallelFromOctave()
    {
        Matrix<6, 6> A;
        A << 1, 2, 3, 0, 0, 0, 4, 5, 6, 0, 0, 0, 7, 8, 9, 0, 0, 0, 0, 0, 0, 101, 102, 103, 0, 0, 0, 104, 105, 106, 0, 0,
            0, 107, 108, 109;

        Matrix<6, 2> B;
        B << 10, 11, 12, 13, 14, 15, 110, 111, 112, 113, 114, 115;

        Matrix<2, 6> C;
        C << 16, 17, 18, 116, 117, 118, 19, 20, 21, 119, 120, 121;

        Matrix<2, 2> D;
        D << 144, 146, 148, 150;
        return StateSpace("test_state_space", A, B, C, D);
    }

    StateSpace<6, 2, 2> FeedbackFromOctave()
    {
        Matrix<6, 6> A;
        A << -6.818, -6.265, -5.712, -0.516, -0.5133, -0.5107, -5.308, -4.84, -4.372, -0.5098, -0.5082, -0.5065, -3.798,
            -3.415, -3.032, -0.5036, -0.503, -0.5024, 0.4127, 0.4273, 0.4418, -4.133, -4.027, -3.922, 0.4065, 0.4221,
            0.4377, -3.036, -2.946, -2.857, 0.4003, 0.4169, 0.4336, -1.938, -1.865, -1.792;

        Matrix<6, 2> B;
        B << -0.2778, 0.2752, -0.2348, 0.2332, -0.1918, 0.1912, 0.44, 0.4546, 0.4476, 0.4632, 0.4552, 0.4718;

        Matrix<2, 6> C;
        C << -0.7567, -0.7139, -0.6711, -0.4788, -0.4824, -0.4859, 0.7536, 0.7113, 0.669, -0.4726, -0.4772, -0.4818;

        Matrix<2, 2> D;
        D << -0.01962, 0.02315, 0.02341, -0.01885;

        return StateSpace("test_state_space", A, B, C, D);
    }

    StateSpace<6, 4, 4> AppendFromOctave()
    {
        Matrix<6, 6> A;
        A << 1, 2, 3, 0, 0, 0, 4, 5, 6, 0, 0, 0, 7, 8, 9, 0, 0, 0, 0, 0, 0, 101, 102, 103, 0, 0, 0, 104, 105, 106, 0, 0,
            0, 107, 108, 109;

        Matrix<6, 4> B;
        B << 10, 11, 0, 0, 12, 13, 0, 0, 14, 15, 0, 0, 0, 0, 110, 111, 0, 0, 112, 113, 0, 0, 114, 115;

        Matrix<4, 6> C;
        C << 16, 17, 18, 0, 0, 0, 19, 20, 21, 0, 0, 0, 0, 0, 0, 116, 117, 118, 0, 0, 0, 119, 120, 121;

        Matrix<4, 4> D;
        D << 22, 23, 0, 0, 24, 25, 0, 0, 0, 0, 122, 123, 0, 0, 124, 125;
        return StateSpace("test_state_space", A, B, C, D);
    }
};

TEST_F(ConnectionTest, GivenMockMIMOStateSpaces_whileSumOutputs_thenSameAsOctave)
{
    StateSpace ss1 = MakeMockStateSpaceMIMO1();
    StateSpace ss2 = MakeMockStateSpaceMIMO2();

    StateSpace ss3 = MakeSumOutputs(ss1, ss2);

    StateSpace ss3Check = SumOutputsFromOctave();
    ASSERT_TRUE(ss3.IsApprox(ss3Check));
}

TEST_F(ConnectionTest, GivenMockMIMOStateSpaces_whileSeries_thenSameAsOctave)
{
    StateSpace ss1 = MakeMockStateSpaceMIMO1();
    StateSpace ss2 = MakeMockStateSpaceMIMO2();

    StateSpace ss3 = MakeSeries(ss1, ss2);

    StateSpace ss3Check = SeriesFromOctave();
    ASSERT_TRUE(ss3.IsApprox(ss3Check));
}

TEST_F(ConnectionTest, GivenMockMIMOStateSpaces_whileParallel_thenSameAsOctave)
{
    StateSpace ss1 = MakeMockStateSpaceMIMO1();
    StateSpace ss2 = MakeMockStateSpaceMIMO2();

    StateSpace ss3 = MakeParallel(ss1, ss2);

    StateSpace ss3Check = ParallelFromOctave();
    ASSERT_TRUE(ss3.IsApprox(ss3Check));
}

TEST_F(ConnectionTest, GivenMockMIMOStateSpaces_whileFeedback_thenSameAsOctave)
{
    StateSpace ss1 = MakeMockStateSpaceMIMO1();
    StateSpace ss2 = MakeMockStateSpaceMIMO2();

    StateSpace ss3 = MakeFeedback(ss1, ss2);

    StateSpace ss3Check = FeedbackFromOctave();
    ASSERT_TRUE(ss3.GetA().isApprox(ss3Check.GetA(), 0.0001));
    ASSERT_TRUE(ss3.GetB().isApprox(ss3Check.GetB(), 0.0001));
    ASSERT_TRUE(ss3.GetC().isApprox(ss3Check.GetC(), 0.0001));
    ASSERT_TRUE(ss3.GetD().isApprox(ss3Check.GetD(), 0.001));
}

TEST_F(ConnectionTest, GivenMockMIMOStateSpaces_whileAppend_thenSameAsOctave)
{
    StateSpace ss1 = MakeMockStateSpaceMIMO1();
    StateSpace ss2 = MakeMockStateSpaceMIMO2();

    StateSpace ss3 = MakeAppend(ss1, ss2);

    StateSpace ss3Check = AppendFromOctave();
    ASSERT_TRUE(ss3.IsApprox(ss3Check));

    MakeAppend(ss1, ss2, ss3);
}

TEST_F(ConnectionTest, GivenThreeStateSpaces_whileAppendAll_thenSameAsMultipleAppendTwo)
{
    auto ss1 = MakeRandomStateSpace<3, 2, 1>();
    auto ss2 = MakeRandomStateSpace<6, 5, 4>();
    auto ss3 = MakeRandomStateSpace<9, 8, 7>();

    auto ss_append = MakeAppend(ss1, ss2, ss3);

    auto ss_check_aux = MakeAppend(ss1, ss2);
    auto ss_check = MakeAppend(ss_check_aux, ss3);
    ASSERT_TRUE(ss_append.IsApprox(ss_check));
}
}  //namespace mpmca::dokblocks::testing