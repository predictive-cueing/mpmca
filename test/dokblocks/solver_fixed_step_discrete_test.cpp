/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/dokblocks/core/solver_fixed_step_discrete.hpp"

#include <gtest/gtest.h>

#include <memory>

#include "mpmca/constants.hpp"
#include "mpmca/dokblocks/components/state_space.hpp"
#include "mpmca/dokblocks/core/abstract_block.hpp"

namespace mpmca::dokblocks {
class SolverFixedStepDiscreteTest : public ::testing::Test
{
  protected:
    static constexpr int NX = 3;
    static constexpr int NU = 2;
    static constexpr int NY = 2;

    SolverFixedStepDiscreteTest() {}

    auto GetTestFunction()
    {
        Vector<2> u_offset = Vector<2>::Constant(1.);
        std::vector<Real> par{1., 2., 3.};

        auto fun = [par = par, u_offset = u_offset](Vector<2> u) {
            u = u - u_offset;
            Real y1 = 2. / constants::kPi * (1. - std::exp(-pow(par[0], u.sum()))) *
                      std::atan(2. * u.prod() / (par[0] * constants::kPi));
            Real y2 = 2. / constants::kPi * (1. - std::exp(-pow(par[1], u.sum()))) *
                      std::atan(2. * u.prod() / (par[1] * constants::kPi));
            Real y3 = 2. / constants::kPi * (1. - std::exp(-pow(par[2], u.sum()))) *
                      std::atan(2. * u.prod() / (par[2] * constants::kPi));
            return (Vector<3>() << y1, y2, y3).finished();
        };

        return fun;
    }

    auto MakeMockStateSpaceMIMO1(Vector<NX> const& x0, Real dt)
    {
        Matrix<NX, NX> A;
        A << 1, 2, 3, 4, 5, 6, 7, 8, 9;

        Matrix<NX, NU> B;
        B << 10, 11, 12, 13, 14, 15;

        Matrix<NY, NX> C;
        C << 16, 17, 18, 19, 20, 21;

        Matrix<NY, NU> D;
        D << 22, 23, 24, 25;

        return StateSpace("test_state_space", A, B, C, D, x0, dt);
    }

    auto MakeMockStateSpaceMIMO2(Vector<NX> const& x0, Real dt)
    {
        Matrix<NX, NX> A;
        A << 101, 102, 103, 104, 105, 106, 107, 108, 109;

        Matrix<NX, NU> B;
        B << 110, 111, 112, 113, 114, 115;

        Matrix<NY, NX> C;
        C << 116, 117, 118, 119, 120, 121;

        Matrix<NY, NU> D;
        D << 122, 123, 124, 125;

        return StateSpace("test_state_space", A, B, C, D, x0, dt);
    }
};

TEST_F(SolverFixedStepDiscreteTest, GivenDiscreteStateSpace_whileSimulatingWithSolver_thenEqualAsStateSpaceOutput)
{
    Real dt = 0.1;
    auto x0 = Vector<NX>::Constant(2.);
    auto ss = MakeMockStateSpaceMIMO1(x0, dt);

    auto u = Vector<NU>::Constant(3.);
    SolverFixedStepDiscrete solver{ss, dt};
    auto ySolver = solver.Simulate(u);

    auto ySS = ss.Simulate(u);
    ASSERT_TRUE(ySolver.isApprox(ySS));
}

}  //namespace mpmca::dokblocks