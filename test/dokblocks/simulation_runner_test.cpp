/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/dokblocks/core/simulation_runner.hpp"

#include <gtest/gtest.h>

#include "mpmca/dokblocks/components/constant.hpp"
#include "mpmca/dokblocks/components/gain_coefficient_wise.hpp"
#include "mpmca/dokblocks/core/sum.hpp"

namespace mpmca::dokblocks {
class SimulationRunnerTest : public ::testing::Test
{
  protected:
    const double m_dt = 0.01;
    const size_t m_N = 100;

    SimulationRunnerTest() {}

    auto GetGainModel(double gain) const
    {
        auto model = GainCoefficientWise<1>("test", gain);
        return model;
    }

    auto GetConstantSumModel(double constant_value)
    {
        Constant constant("test_constant", constant_value);
        GainCoefficientWise<1> gain = GainCoefficientWise<1>("unity_gain", 1);

        auto sum = Sum("sum_constant_input", constant, gain);
        return sum;
    }
};

TEST_F(SimulationRunnerTest, DifferentSignalNamesDontConnect)
{
    auto gain = GetGainModel(1.0);
    gain.SetInputSignalNames({"x"});
    gain.SetOutputSignalNames({"y"});

    SimulationRunner runner("test", gain, m_dt, true);

    EXPECT_FALSE(runner.IsSignalConnected("x"));
    EXPECT_FALSE(runner.IsSignalConnected("y"));

    for (size_t i = 0; i < m_N; ++i) {
        runner.GetInput("x").SetSignalAtIndex(i, i * m_dt, i);
    }

    runner.RunSimulation(m_N);

    EXPECT_EQ(m_N, runner.GetOutput("y").GetSignalLength());

    for (size_t i = 0; i < m_N; ++i) {
        std::cout << i << " : " << runner.GetOutput("y").GetTimeAtIndex(i) << ", "
                  << runner.GetOutput("y").GetSignalAtIndex(i) << std::endl;
    }

    EXPECT_EQ(m_N - 1, runner.GetOutput("y").GetSignalAtIndex(m_N - 1));
}

TEST_F(SimulationRunnerTest, SameSignalNamesConnect)
{
    auto gain = GetConstantSumModel(1.0);
    gain.SetInputSignalNames({"x"});
    gain.SetOutputSignalNames({"x"});

    SimulationRunner runner("test", gain, m_dt, true);

    EXPECT_TRUE(runner.IsSignalConnected("x"));
    EXPECT_TRUE(runner.IsSignalConnected("x"));

    runner.GetInput("x").SetSignalAtIndex(0, 0, 0);
    runner.RunSimulation(m_N);
}

}  //namespace mpmca::dokblocks