/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <gtest/gtest.h>

#include "mpmca/dokblocks/classic_washout_algorithm/classic_washout_algorithm.hpp"
#include "mpmca/dokblocks/core/abstract_block.hpp"
#include "mpmca/dokblocks/core/solver_fixed_step_discrete.hpp"
#include "mpmca/dokblocks/utils/block_traits.hpp"
#include "mpmca/dokblocks/utils/compare.hpp"
#include "mpmca/dokblocks/utils/matlab_utils.hpp"

namespace mpmca::dokblocks::classic_washout_algorithm::testing {
class SimulateCwaTest : public ::testing::Test
{
  protected:
    Params m_parameters;

    SimulateCwaTest()
        : m_parameters(TEST_DATA_PATH + std::string("/dokblocks/params_cwa.json"))
    {
    }

    template <class T>
    auto Simulate(T const& block, std::string const& data_input)
    {
        static_assert(utils::is_derived_from_abstract_block<T>::value,
                      "The template parameter must be an AbstractBlock.");

        SolverFixedStepDiscrete solver{block, 0.01};

        auto input = utils::ReadBinary(std::string(TEST_DATA_PATH) + "/dokblocks/" + data_input);
        auto U = FromDynamicMatrixToVectorOfVectors<T::NU>(input);

        std::vector<Vector<T::NY>> Y;

        for (auto u_k : U) {
            Y.emplace_back(solver.Simulate(u_k));
        }

        return Y;
    }

    template <int N>
    auto FromDynamicMatrixToVectorOfVectors(DynamicMatrix mtx)
    {
        std::vector<Vector<N>> v;
        for (auto k = 0; k < mtx.rows(); ++k) {
            Vector<N> v_k = mtx.row(k).tail<N>().transpose();
            v.emplace_back(v_k);
        }

        return v;
    }

    template <int NY>
    void CheckOutput(std::vector<Vector<NY>> const& Y, std::string const& y_check_file_name, Real tol)
    {
        DynamicMatrix output = utils ::ReadBinary(std::string(TEST_DATA_PATH) + "/dokblocks/" + y_check_file_name);
        auto YCheck = FromDynamicMatrixToVectorOfVectors<NY>(output);

        auto it_y_check = YCheck.cbegin();
        auto it_y = Y.cbegin();
        int i = 0;
        for (; it_y_check != YCheck.cend(); ++it_y_check, ++it_y, ++i) {
            ASSERT_TRUE(utils::DifferenceIsWithinPrecision(*it_y_check, *it_y, tol))
                << "Expected: " << (*it_y_check).transpose() << "\nActual : " << (*it_y).transpose() << " on time step "
                << i;
        }
    }
};

TEST_F(SimulateCwaTest, CWACompiles)
{
    auto cwa = classic_washout_algorithm::cwa::GetBlock(m_parameters);
}

TEST_F(SimulateCwaTest, CWAruns)
{
    auto cwa = cwa::GetBlock(m_parameters);
    auto u = Vector<cwa.NU>::Constant(3.);
    SolverFixedStepDiscrete solver{cwa, 0.01};
    auto y_solver = solver.Simulate(u);
}

TEST_F(SimulateCwaTest, GivenAccelerationBody2WorldBlock_whileSimulatingWithSolver_thenSameAsSimulink)
{
    auto block = specific_force_to_acceleration::GetBlock(m_parameters);

    auto Y = Simulate(block, "fH_eul.data");

    CheckOutput(Y, "a_H.data", 0.01);
}

TEST_F(SimulateCwaTest, GivenFromSpecificForceToAcceleration_whileSimulatingWithSolver_thenSameAsSimulink)
{
    auto block = specific_force_to_acceleration::GetBlock(m_parameters);

    auto Y = Simulate(block, "fH_eul.data");

    CheckOutput(Y, "a_H.data", 0.01);
}

TEST_F(SimulateCwaTest, GivenScalingAcceleration_whileSimulatingWithSolver_thenSameAsSimulink)
{
    auto block = acceleration_gain::GetBlock(m_parameters);

    auto Y = Simulate(block, "a_H.data");

    CheckOutput(Y, "ak_H.data", 0.01);
}

TEST_F(SimulateCwaTest, GivenFrequencySplitterHigh_whileSimulatingWithSolver_thenSameAsSimulink)
{
    auto block = acceleration_split_high_pass::GetBlock(m_parameters);

    auto Y = Simulate(block, "ak_H.data");

    CheckOutput(Y, "akHigh_H.data", 0.01);
}

TEST_F(SimulateCwaTest, GivenAccelerationBody2World_whileSimulatingWithSolver_thenSameAsSimulink)
{
    auto block = acceleration_body_to_world::GetBlock(m_parameters);

    auto Y = Simulate(block, "ahH_eul.data");

    CheckOutput(Y, "ah_W.data", 0.01);
}

TEST_F(SimulateCwaTest, GivenWashoutFilter_whileSimulatingWithSolver_thenSameAsSimulink)
{
    auto block = acceleration_washout_high_pass::GetBlock(m_parameters);

    auto Y = Simulate(block, "ah_W.data");

    CheckOutput(Y, "aCwa_W.data", 0.1);  // !SMALL ERRORS (NUMERIC?)
}

TEST_F(SimulateCwaTest, GivenSpecificForcePath_whileSimulatingWithSolver_thenSameAsSimulink)
{
    GTEST_SKIP() << "Skipping test, to be looked into.";
    auto block = specific_force_path::GetBlock(m_parameters);

    auto Y = Simulate(block, "fH_eul.data");

    CheckOutput(Y, "cwa_trans.data", 0.1);  // !DRIFTING (NUMERIC?)
}

TEST_F(SimulateCwaTest, GivenTcFrequencySplitterLow_whileSimulatingWithSolver_thenSameAsSimulink)
{
    auto block = acceleration_split_low_pass::GetBlock(m_parameters);

    auto Y = Simulate(block, "ak_xy.data");

    CheckOutput(Y, "akLow_H.data", 0.01);
}

TEST_F(SimulateCwaTest, GivenTcTiltCoordination_whileSimulatingWithSolver_thenSameAsSimulink)
{
    GTEST_SKIP() << "Skipping test, to be looked into.";
    auto block = tilt_coordination_angles::GetBlock(m_parameters);

    auto Y = Simulate(block, "akLow_H.data");

    CheckOutput(Y, "beta_tc.data", 0.00001);
}

TEST_F(SimulateCwaTest, GivenTcTiltSaturation_whileSimulatingWithSolver_thenSameAsSimulink)
{
    auto block = tilt_coordination_saturation::GetBlock(m_parameters);

    auto Y = Simulate(block, "beta_tc.data");

    CheckOutput(Y, "betaSat_tc.data", 0.01);
}

TEST_F(SimulateCwaTest, GivenTcRateLimiter_whileSimulatingWithSolver_thenSameAsSimulink)
{
    auto block = tilt_coordination_rate_limiter::GetBlock(m_parameters);

    auto Y = Simulate(block, "betaSat_tc.data");

    CheckOutput(Y, "betaRate_tc.data", 0.01);
}

TEST_F(SimulateCwaTest, GivenTcTiltLowPass_whileSimulatingWithSolver_thenSameAsSimulink)
{
    GTEST_SKIP() << "Skipping test, to be looked into.";
    auto block = tilt_coordination_low_pass::GetBlock(m_parameters);

    auto Y = Simulate(block, "betaRate_tc.data");

    CheckOutput(Y, "betaLpf_tc.data", 0.1);  //NUMERIC (?)
}

TEST_F(SimulateCwaTest, GivenTcTiltLowPassSS_whileSimulatingWithSolver_thenSameAsSimulink)
{
    GTEST_SKIP() << "Skipping test, to be looked into.";
    auto block = tilt_coordination_low_pass::GetBlock(m_parameters);

    auto Y = Simulate(block, "betaRate_tc.data");

    CheckOutput(Y, "betaLpf_tc_ss.data", 0.1);  //NUMERIC (?)
}

TEST_F(SimulateCwaTest, GivenTcTiltLowPassDis_whileSimulatingWithSolver_thenSameAsSimulink)
{
    auto block = tilt_coordination_low_pass::GetBlock(m_parameters);

    auto Y = Simulate(block, "betaRate_tc.data");

    CheckOutput(Y, "betaLpf_tc_dis.data", 0.1);  //NUMERIC (?)
}

TEST_F(SimulateCwaTest, GivenTcTiltLowPassDisMy_whileSimulatingWithSolver_thenSameAsSimulink)
{
    GTEST_SKIP() << "Skipping test, to be looked into.";
    auto block = tilt_coordination_low_pass::GetBlock(m_parameters);

    auto Y = Simulate(block, "betaRate_tc.data");

    CheckOutput(Y, "betaLpf_tc_dis_my.data", 0.01);
}

TEST_F(SimulateCwaTest, GivenScalingAngularVelocity_whileSimulatingWithSolver_thenSameAsSimulink)
{
    auto block = angular_velocity_gain::GetBlock(m_parameters);

    auto Y = Simulate(block, "w_H.data");

    CheckOutput(Y, "wk_H.data", 0.01);
}

TEST_F(SimulateCwaTest, GivenFromAngularVelocityToEulerRates_whileSimulatingWithSolver_thenSameAsSimulink)
{
    auto block = angular_velocity_to_euler_rates::GetBlock(m_parameters);

    auto Y = Simulate(block, "wkAndEul.data");

    CheckOutput(Y, "betaDot.data", 0.01);
}

TEST_F(SimulateCwaTest, GivenEulerRatesHighPass_whileSimulatingWithSolver_thenSameAsSimulink)
{
    auto block = euler_rates_washout_high_pass::GetBlock(m_parameters);

    auto Y = Simulate(block, "betaDot.data");

    CheckOutput(Y, "betaDotHp.data", 0.01);
}

TEST_F(SimulateCwaTest, GivenEulerRatesIntegrator_whileSimulatingWithSolver_thenSameAsSimulink)
{
    auto block = euler_rates_integrator::GetBlock(m_parameters);

    auto Y = Simulate(block, "betaDotHp.data");

    CheckOutput(Y, "betaHp.data", 0.01);
}

}  //namespace mpmca::dokblocks::classic_washout_algorithm::testing