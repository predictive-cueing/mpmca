/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/dokblocks/classic_washout_algorithm/params.hpp"

#include <gtest/gtest.h>

namespace mpmca::dokblocks::classic_washout_algorithm::testing {
class ParamsTest : public ::testing::Test
{
  protected:
    Params m_parameters;
    ParamsTest()
        : m_parameters(TEST_DATA_PATH + std::string("/dokblocks/params_cwa.json"))
    {
    }
};

TEST_F(ParamsTest, GivenParametersFromJson_whileLoading_thenCorrect)
{
    ASSERT_TRUE(m_parameters.GetGravityVector().isApprox((Vector<3>() << 0., 0., -9.81).finished()));
    ASSERT_TRUE(m_parameters.GetAccelerationGain().isApprox((Vector<3>() << 0.1, 0.25, 0.25).finished()));
    ASSERT_TRUE(m_parameters.GetAngularVelocityGain().isApprox((Vector<3>() << 0.1, 0.25, 0.25).finished()));
    ASSERT_EQ(m_parameters.GetAccelerationSplitOmegaX(), 0.65);
    ASSERT_EQ(m_parameters.GetAccelerationSplitOmegaY(), 0.65);
    ASSERT_EQ(m_parameters.GetAccelerationSplitOmegaZ(), 1.2);
    ASSERT_EQ(m_parameters.GetAccelerationWashoutHighPassOmegaX(), 0.5);
    ASSERT_EQ(m_parameters.GetAccelerationWashoutHighPassOmegaY(), 0.5);
    ASSERT_EQ(m_parameters.GetAccelerationWashoutHighPassOmegaZ(), 1);
    ASSERT_EQ(m_parameters.GetAccelerationWashoutHighPassZetaX(), 1);
    ASSERT_EQ(m_parameters.GetAccelerationWashoutHighPassZetaY(), 1);
    ASSERT_EQ(m_parameters.GetAccelerationWashoutHighPassZetaZ(), 1);
    ASSERT_EQ(m_parameters.GetTiltCoordinationMaximumRoll(), 0.5);
    ASSERT_EQ(m_parameters.GetTiltCoordinationMaximumPitch(), 0.5);
    ASSERT_EQ(m_parameters.GetTiltCoordinationMaximumRollRate(), 0.5);
    ASSERT_EQ(m_parameters.GetTiltCoordinationMaximumPitchRate(), 0.5);
    ASSERT_EQ(m_parameters.GetTiltCoordinationLowPassOmegaPitch(), 0.5);
    ASSERT_EQ(m_parameters.GetTiltCoordinationLowPassZetaPitch(), 1);
    ASSERT_EQ(m_parameters.GetEulerRatesWashoutHighPassOmegaRoll(), 1.2);
    ASSERT_EQ(m_parameters.GetEulerRatesWashoutHighPassOmegaPitch(), 1.2);
    ASSERT_EQ(m_parameters.GetEulerRatesWashoutHighPassOmegaYaw(), 0.5);
}
}  //namespace mpmca::dokblocks::classic_washout_algorithm::testing