/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/dokblocks/components/algebraic_function.hpp"

#include <gtest/gtest.h>

#include <iostream>
#include <string>

#include "mpmca/constants.hpp"

namespace mpmca::dokblocks::testing {
class AlgebraicFunctionTest : public ::testing::Test
{
  protected:
    static constexpr int NU = 2;
    static constexpr int NY = 3;
    using F = std::function<Vector<NY>(const Vector<NU>&)>;

    AlgebraicFunctionTest() {}

    F GetTestFunction()
    {
        Vector<NU> u_offset = Vector<NU>::Constant(1.);
        std::vector<Real> parameters{1., 2., 3.};

        F fun = [parameters = parameters, u_offset = u_offset](const Vector<NU>& u) {
            Vector<NU> u_minus_offset = u - u_offset;
            Real y1 = 2. / constants::kPi * (1. - std::exp(-pow(parameters[0], u_minus_offset.sum()))) *
                      std::atan(2. * u_minus_offset.prod() / (parameters[0] * constants::kPi));
            Real y2 = 2. / constants::kPi * (1. - std::exp(-pow(parameters[1], u_minus_offset.sum()))) *
                      std::atan(2. * u_minus_offset.prod() / (parameters[1] * constants::kPi));
            Real y3 = 2. / constants::kPi * (1. - std::exp(-pow(parameters[2], u_minus_offset.sum()))) *
                      std::atan(2. * u_minus_offset.prod() / (parameters[2] * constants::kPi));
            return (Vector<NY>() << y1, y2, y3).finished();
        };

        return fun;
    }
};

TEST_F(AlgebraicFunctionTest, GivenTestFunction_whenCalculatingOutput_thenSameAsOctave)
{
    auto fun = AlgebraicFunction<NU, NY>("test_function", GetTestFunction());

    Vector<NU> u = Vector<NU>::Constant(3.);
    auto y = fun.Simulate(u);

    Vector<NY> y_octave{0.4815, 0.5762, 0.4481};
    ASSERT_TRUE(y.isApprox(y_octave, 0.0001));
}

}  //namespace mpmca::dokblocks::testing