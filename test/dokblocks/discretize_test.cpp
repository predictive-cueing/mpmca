/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/dokblocks/core/discretize.hpp"

#include <gtest/gtest.h>

#include "mpmca/dokblocks/core/matrix_types.hpp"

namespace mpmca::dokblocks::testing {
class DiscretizeTest : public ::testing::Test
{
  protected:
    DiscretizeTest() {}

    StateSpace<3, 2, 2> MakeMockStateSpaceMIMO1()
    {
        Matrix<3, 3> A;
        A << 1, 2, 3, 4, 5, 6, 7, 8, 9;

        Matrix<3, 2> B;
        B << 10, 11, 12, 13, 14, 15;

        Matrix<2, 3> C;
        C << 16, 17, 18, 19, 20, 21;

        Matrix<2, 2> D;
        D << 22, 23, 24, 25;

        return StateSpace("test_state_space", A, B, C, D);
    }

    StateSpace<3, 2, 2> GetC2dFromOctave()
    {
        Matrix<3, 3> A;
        A << 1.012, 0.0219, 0.03222, 0.04348, 1.054, 0.06507, 0.07539, 0.08665, 1.098;

        Matrix<3, 2> B;
        B << 0.104, 0.1143, 0.1297, 0.1405, 0.1554, 0.1667;

        Matrix<2, 3> C;
        C << 16, 17, 18, 19, 20, 21;

        Matrix<2, 2> D;
        D << 22, 23, 24, 25;

        Vector<3> x0 = Vector<3>::Zero();

        Real dt = 0.01;

        return StateSpace("test_state_space", A, B, C, D, x0, dt);
    }

    StateSpace<3, 2, 2> GetC2dTustinFromOctave()
    {
        Matrix<3, 3> A;
        A << 1.012, 0.02196, 0.03229, 0.04359, 1.054, 0.06522, 0.07555, 0.08685, 1.098;

        Matrix<3, 2> B;
        B << 0.1042, 0.1145, 0.13, 0.1408, 0.1559, 0.1672;

        Matrix<2, 3> C;
        C << 17.14, 18.42, 19.7, 20.34, 21.66, 22.99;

        Matrix<2, 2> D;
        D << 25.34, 26.62, 27.93, 29.25;

        Vector<3> x0 = Vector<3>::Zero();

        Real dt = 0.01;

        return StateSpace("test_state_space", A, B, C, D, x0, dt);
    }

    StateSpace<3, 2, 2> GetC2dTustin2FromOctave()
    {
        Matrix<3, 3> A;
        A << 1.027, 0.04857, 0.07003, 0.09573, 1.119, 0.1429, 0.1643, 0.19, 1.216;

        Matrix<3, 2> B;
        B << 0.2183, 0.2398, 0.2839, 0.3075, 0.3494, 0.3751;

        Matrix<2, 3> C;
        C << 18.51, 20.11, 21.72, 21.94, 23.65, 25.36;

        Matrix<2, 2> D;
        D << 29.3, 30.91, 32.58, 34.29;

        Vector<3> x0 = Vector<3>::Zero();

        Real dt = 0.02;

        return StateSpace("test_state_space", A, B, C, D, x0, dt);
    }

    StateSpace<3, 2, 2> GetD2dFromOctave()
    {
        Matrix<3, 3> A;
        A << 1.027, 0.04803, 0.06939, 0.09474, 1.118, 0.1414, 0.1628, 0.1881, 1.213;

        Matrix<3, 2> B;
        B << 0.2171, 0.2385, 0.2811, 0.3045, 0.3451, 0.3705;

        Matrix<2, 3> C;
        C << 16, 17, 18, 19, 20, 21;

        Matrix<2, 2> D;
        D << 22, 23, 24, 25;

        Vector<3> x0 = Vector<3>::Zero();

        Real dt = 0.02;

        return StateSpace("test_state_space", A, B, C, D, x0, dt);
    }
};

TEST_F(DiscretizeTest, GivenMockStateSpace_whileConvertingToDiscreteWithTustin_thenSameAsOctave)
{
    auto ss = MakeMockStateSpaceMIMO1();

    auto ssd = c2d(ss, 0.01, DiscretizationMethod::kTustin);

    auto ssd_check = GetC2dTustinFromOctave();
    ASSERT_TRUE(ssd.IsApprox(ssd_check, 0.001));
}

TEST_F(DiscretizeTest, GivenMockStateSpace_whileConvertingToContinuousWithTustin_thenSameAsOctave)
{
    auto ssd = GetC2dTustinFromOctave();

    auto ss = d2c(ssd, DiscretizationMethod::kTustin);

    auto ssCheck = MakeMockStateSpaceMIMO1();
    ASSERT_TRUE(ss.IsApprox(ssCheck, 0.01));
}

TEST_F(DiscretizeTest, GivenMockStateSpace_whileChangingSampleTimeWithTustin_thenSameAsOctave)
{
    auto ssd = GetC2dTustinFromOctave();

    auto ssdNew = d2d(ssd, 0.02, DiscretizationMethod::kTustin);

    auto ssd_check = GetC2dTustin2FromOctave();
    ASSERT_TRUE(ssdNew.IsApprox(ssd_check, 0.001));
}
}  //namespace mpmca::dokblocks::testing