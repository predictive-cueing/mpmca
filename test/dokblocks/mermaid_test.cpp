/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <gtest/gtest.h>

#include "mpmca/dokblocks/classic_washout_algorithm/classic_washout_algorithm.hpp"
#include "mpmca/dokblocks/classic_washout_algorithm/params.hpp"
#include "mpmca/dokblocks/components/constant.hpp"
#include "mpmca/dokblocks/components/integrator.hpp"
#include "mpmca/dokblocks/core/series.hpp"
#include "mpmca/dokblocks/core/sum.hpp"

namespace mpmca::dokblocks::testing {
class MermaidTest : public ::testing::Test
{
  protected:
    MermaidTest() {}

    static void PrintStrings(const std::vector<std::string>& strings)
    {
        std::cout << "flowchart TD" << std::endl;

        for (const std::string& string : strings) {
            std::cout << "  " << string << std::endl;
        }
    }
};

TEST_F(MermaidTest, MermaidTestFlowchart)
{
    Constant constant1("test_constant_1", 32);
    Integrator integrator("integrator1");
    Constant constant2("test_constant_2", 13);

    auto series = MakeSeries("integrate_constant", constant1, integrator, integrator);

    auto sum = Sum("sum", series, constant2);

    PrintStrings(sum.GetMermaidDiagramString());
}

TEST_F(MermaidTest, OmegaScalingFlowchart)
{
    classic_washout_algorithm::Params params;
    auto Kw = classic_washout_algorithm::angular_velocity_gain::GetBlock(params);
    auto K_euler = GainCoefficientWise<3>("K_eul", 1.);
    auto K = MakeAppend("angular_velocity_append", Kw, K_euler);

    auto wk_2_euler_d = classic_washout_algorithm::angular_velocity_to_euler_rates::GetBlock(params);
    auto HP = classic_washout_algorithm::euler_rates_washout_high_pass::GetBlock(params);
    auto INT = classic_washout_algorithm::euler_rates_integrator::GetBlock(params);

    auto wPath = MakeSeries("angular_velocity_path", K, wk_2_euler_d, HP, INT);

    PrintStrings(wPath.GetMermaidDiagramString());
}

TEST_F(MermaidTest, TiltCoordinationPath)
{
    classic_washout_algorithm::Params params;
    auto fB_2_aB = classic_washout_algorithm::specific_force_to_acceleration::GetBlock(params);
    //
    auto ak_B = classic_washout_algorithm::acceleration_gain::GetBlock(params);
    auto select_xy = SignalSelect<0, 2, 3>("select_xy");
    auto ak_xy = MakeSeries("ak_xy", ak_B, select_xy);

    auto roll_pitch_tc = MakeSeries("roll_pitch_tc", fB_2_aB, ak_xy);
    PrintStrings(roll_pitch_tc.GetMermaidDiagramString());
}

TEST_F(MermaidTest, cwaFlowChart)
{
    classic_washout_algorithm::Params params;
    auto cwa = classic_washout_algorithm::cwa::GetBlock(params);

    PrintStrings(cwa.GetMermaidDiagramString());
}

}  //namespace mpmca::dokblocks::testing