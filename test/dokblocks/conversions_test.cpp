/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */

#include "mpmca/dokblocks/core/conversions.hpp"

#include <gtest/gtest.h>

#include "mpmca/dokblocks/core/matrix_types.hpp"

namespace mpmca::dokblocks::testing {
class ConversionsTest : public ::testing::TestWithParam<std::string>
{
  protected:
    ConversionsTest() {}

    std::array<Real, 4> GetMockNumProper()
    {
        std::array<Real, 4> num{1., 2., 3., 4.};
        return num;
    }

    std::array<Real, 3> GetMockNumStrictlyProper()
    {
        std::array<Real, 3> num{2., 3., 4.};
        return num;
    }

    std::array<Real, 4> GetMockDen()
    {
        std::array<Real, 4> den{5., 6., 7., 8.};
        return den;
    }

    StateSpace<3, 1, 1> GetTf2ssProperFromOctave()
    {
        Matrix<3, 3> A;
        A << -1.2000, -1.4000, -1.6000, 1.0000, 0, 0, 0, 1.0000, 0;

        Matrix<3, 1> B;
        B << 1, 0, 0;

        Matrix<1, 3> C;
        C << 0.1600, 0.3200, 0.4800;

        Matrix<1, 1> D;
        D << 0.2;

        return StateSpace("test_state_space", A, B, C, D);
    }

    StateSpace<3, 1, 1> GetTf2ssStrictlyProperFromOctave()
    {
        Matrix<3, 3> A;
        A << -1.2000, -1.4000, -1.6000, 1.0000, 0, 0, 0, 1.0000, 0;

        Matrix<3, 1> B;
        B << 1, 0, 0;

        Matrix<1, 3> C;
        C << 0.4000, 0.6000, 0.8000;

        Matrix<1, 1> D;
        D << 0;

        return StateSpace("test_state_space", A, B, C, D);
    }

    StateSpace<3, 2, 2> MakeMockStateSpaceMIMO1()
    {
        Matrix<3, 3> A;
        A << 1, 2, 3, 4, 5, 6, 7, 8, 9;

        Matrix<3, 2> B;
        B << 10, 11, 12, 13, 14, 15;

        Matrix<2, 3> C;
        C << 16, 17, 18, 19, 20, 21;

        Matrix<2, 2> D;
        D << 22, 23, 24, 25;

        return StateSpace("test_state_space", A, B, C, D);
    }

    StateSpace<3, 2, 2> GetC2dFromOctave()
    {
        Matrix<3, 3> A;
        A << 1.012, 0.0219, 0.03222, 0.04348, 1.054, 0.06507, 0.07539, 0.08665, 1.098;

        Matrix<3, 2> B;
        B << 0.104, 0.1143, 0.1297, 0.1405, 0.1554, 0.1667;

        Matrix<2, 3> C;
        C << 16, 17, 18, 19, 20, 21;

        Matrix<2, 2> D;
        D << 22, 23, 24, 25;

        Vector<3> x0 = Vector<3>::Zero();

        Real dt = 0.01;

        return StateSpace("test_state_space", A, B, C, D, x0, dt);
    }

    StateSpace<3, 2, 2> GetD2dFromOctave()
    {
        Matrix<3, 3> A;
        A << 1.027, 0.04803, 0.06939, 0.09474, 1.118, 0.1414, 0.1628, 0.1881, 1.213;

        Matrix<3, 2> B;
        B << 0.2171, 0.2385, 0.2811, 0.3045, 0.3451, 0.3705;

        Matrix<2, 3> C;
        C << 16, 17, 18, 19, 20, 21;

        Matrix<2, 2> D;
        D << 22, 23, 24, 25;

        Vector<3> x0 = Vector<3>::Zero();

        Real dt = 0.02;

        return StateSpace("test_state_space", A, B, C, D, x0, dt);
    }
};

TEST_F(ConversionsTest, GivenMockNumDenProper_whileUsingTf2ss_thenSameAsOctave)
{
    auto num = GetMockNumProper();
    auto den = GetMockDen();

    StateSpace ss = tf2ss("test_ss", num, den);

    StateSpace ss_check = GetTf2ssProperFromOctave();
    ASSERT_TRUE(ss.GetA().isApprox(ss_check.GetA().colwise().reverse().rowwise().reverse()));
    ASSERT_TRUE(ss.GetB().isApprox(ss_check.GetB().colwise().reverse()));
    ASSERT_TRUE(ss.GetC().isApprox(ss_check.GetC().rowwise().reverse()));
    ASSERT_TRUE(ss.GetD().isApprox(ss_check.GetD()));
}

TEST_F(ConversionsTest, GivenMockNumDenStrictlyProper_whileUsingTf2ss_thenSameAsOctave)
{
    auto num = GetMockNumStrictlyProper();
    auto den = GetMockDen();

    StateSpace ss = tf2ss("test_ss", num, den);

    StateSpace ss_check = GetTf2ssStrictlyProperFromOctave();
    ASSERT_TRUE(ss.GetA().isApprox(ss_check.GetA().colwise().reverse().rowwise().reverse()));
    ASSERT_TRUE(ss.GetB().isApprox(ss_check.GetB().colwise().reverse()));
    ASSERT_TRUE(ss.GetC().isApprox(ss_check.GetC().rowwise().reverse()));
    ASSERT_TRUE(ss.GetD().isApprox(ss_check.GetD()));
}
}  //namespace mpmca::dokblocks::testing