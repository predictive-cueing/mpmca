/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/dokblocks/components/high_pass_first_order.hpp"

#include <gtest/gtest.h>

#include "mpmca/dokblocks/utils/compare.hpp"

namespace mpmca::dokblocks {

TEST(HighPassFirstOrderTest, SteadyStateValueTest)
{
    for (double step = -100; step < 100; step += 0.4) {
        const int N = 100;
        double dt = 0.01;

        HighPassFirstOrder high_pass{"test_hp", 10.};
        high_pass.SetSimulationStep(dt);

        for (int i = 0; i < N; ++i) {
            auto input = HighPassFirstOrder::InputVector::Ones() * step;
            high_pass.Simulate(input);
        }

        auto input = HighPassFirstOrder::InputVector::Ones() * step;
        HighPassFirstOrder::OutputVector output = high_pass.Simulate(input);

        EXPECT_TRUE(utils::DifferenceIsWithinPrecision<1>(output, HighPassFirstOrder::OutputVector::Zero(), 0.1))
            << output;
    }
}

}  //namespace mpmca::dokblocks