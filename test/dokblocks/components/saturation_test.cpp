/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/dokblocks/components/saturation.hpp"

#include <gtest/gtest.h>

namespace mpmca::dokblocks {
TEST(SaturationTest, GivenSaturation_whileCalculatingOutput_thenCorrect)
{
    Saturation sat{"test_saturation", -1., 2.};

    ASSERT_TRUE(sat.Simulate(Vector<1>::Constant(0.5)).isApprox(Vector<1>::Constant(0.5)));
    ASSERT_TRUE(sat.Simulate(Vector<1>::Constant(0.)).isApprox(Vector<1>::Constant(0.)));
    ASSERT_TRUE(sat.Simulate(Vector<1>::Constant(-10.)).isApprox(Vector<1>::Constant(-1.)));
    ASSERT_TRUE(sat.Simulate(Vector<1>::Constant(20.)).isApprox(Vector<1>::Constant(2.)));
    ASSERT_TRUE(sat.Simulate(Vector<1>::Constant(-0.5)).isApprox(Vector<1>::Constant(-0.5)));
}
}  //namespace mpmca::dokblocks