/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/dokblocks/components/integrator.hpp"

#include <gtest/gtest.h>

#include "mpmca/dokblocks/utils/compare.hpp"

namespace mpmca::dokblocks {
TEST(IntegratorTest, SteadyStateValueTest)
{
    for (double gain = -20; gain < 20; gain += 0.3) {
        const int N = 200;
        double dt = 0.01;

        Integrator integrator("test_integrator");
        integrator.SetSimulationStep(dt);

        for (int i = 0; i < N; ++i) {
            auto input = Integrator::InputVector::Ones() * gain;
            integrator.Simulate(input);
        }

        auto input = Integrator::InputVector::Ones() * gain;
        auto output = integrator.Simulate(input);

        EXPECT_TRUE(utils::DifferenceIsWithinPrecision<1>(output, Integrator::OutputVector::Ones() * gain * 2.0, 0.001))
            << output << ", " << gain;
    }
}

}  //namespace mpmca::dokblocks