/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/dokblocks/components/constant.hpp"

#include <gtest/gtest.h>

#include "mpmca/dokblocks/core/matrix_types.hpp"

namespace mpmca::dokblocks {
TEST(ConstantTest, ConstantOutputIsAlwaysTheSame)
{
    Constant constant{"test_constant", 42.};

    constant.UpdateOutput(Vector<0>::Constant(0.));
    ASSERT_TRUE(constant.GetOutput().isApprox(Vector<1>::Constant(42.)));

    constant.UpdateOutput(Vector<0>::Constant(-1.));
    ASSERT_TRUE(constant.GetOutput().isApprox(Vector<1>::Constant(42.)));

    constant.UpdateOutput(Vector<0>::Constant(2.));
    ASSERT_TRUE(constant.GetOutput().isApprox(Vector<1>::Constant(42.)));
}

}  //namespace mpmca::dokblocks