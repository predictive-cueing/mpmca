/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/dokblocks/components/low_pass_second_order.hpp"

#include <gtest/gtest.h>

#include "mpmca/dokblocks/utils/compare.hpp"

namespace mpmca::dokblocks {
TEST(LowPassSecondOrderTest, SteadyStateValueTest)
{
    for (double step = -100; step < 100; step += 0.4) {
        const int N = 100;
        double dt = 0.01;

        LowPassSecondOrder low_pass{"test_lp", 0.7, 10.};
        low_pass.SetSimulationStep(dt);

        for (int i = 0; i < N; ++i) {
            auto input = LowPassSecondOrder::InputVector::Ones() * step;
            low_pass.Simulate(input);
        }

        auto input = LowPassSecondOrder::InputVector::Ones() * step;
        auto output = low_pass.Simulate(input);

        EXPECT_TRUE(
            utils::DifferenceIsWithinPrecision<1>(output, LowPassSecondOrder::OutputVector::Ones() * step, 0.15))
            << step << " " << output;
    }
}

}  //namespace mpmca::dokblocks