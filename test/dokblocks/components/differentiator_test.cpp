/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/dokblocks/components/differentiator.hpp"

#include <gtest/gtest.h>

#include "mpmca/dokblocks/utils/compare.hpp"

namespace mpmca::dokblocks {
TEST(DifferentiatorTest, SteadyStateValueTest)
{
    for (double rate = -20; rate < 20; rate += 0.3) {
        const int N = 100;
        double dt = 0.01;

        Differentiator differentiator{"test_differentiator", 20.};
        differentiator.SetSimulationStep(dt);

        for (int i = 0; i < N; ++i) {
            auto input = Differentiator::InputVector::Ones() * i * dt * rate;
            differentiator.Simulate(input);
        }

        auto input = Differentiator::InputVector::Ones() * N * dt * rate;
        auto output = differentiator.Simulate(input);

        EXPECT_TRUE(utils::DifferenceIsWithinPrecision<1>(output, Differentiator::OutputVector::Ones() * rate, 0.1))
            << output << ", " << rate;
    }
}

}  //namespace mpmca::dokblocks