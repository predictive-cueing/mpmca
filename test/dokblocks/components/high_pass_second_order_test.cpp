/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/dokblocks/components/high_pass_second_order.hpp"

#include <gtest/gtest.h>

#include "mpmca/dokblocks/utils/compare.hpp"

namespace mpmca::dokblocks {

TEST(HighPassSecondOrderTest, SteadyStateValueTest)
{
    for (double step = -100; step < 100; step += 0.4) {
        const int N = 100;
        double dt = 0.01;

        HighPassSecondOrder high_pass{"test_hp", 0.7, 10.};
        high_pass.SetSimulationStep(dt);

        for (int i = 0; i < N; ++i) {
            auto input = HighPassSecondOrder::InputVector::Ones() * step;
            high_pass.Simulate(input);
        }

        auto input = HighPassSecondOrder::InputVector::Ones() * step;
        HighPassSecondOrder::OutputVector output = high_pass.Simulate(input);

        EXPECT_TRUE(utils::DifferenceIsWithinPrecision<1>(output, HighPassSecondOrder::OutputVector::Zero(), 0.1))
            << output;
    }
}

}  //namespace mpmca::dokblocks