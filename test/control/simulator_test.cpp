/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/control/simulator.hpp"

#include <gtest/gtest.h>

#include "generic_test_header.hpp"
#include "mpmca/control/test_data.hpp"

namespace mpmca::control::testing {
class SimulatorTest : public ::testing::Test
{
  protected:
    TestData<TestSimulatorType> m_test_data;
    Simulator<TestSimulatorType> m_simulator;

    SimulatorTest()
        : m_test_data(std::string(TEST_DATA_PATH) + TestSimulatorType::GetSnakeCaseName() + "_data.json")
    {
    }
};

TEST_F(SimulatorTest, GivenSimulator_whenCalculatingOutput_thenResultsMatchWithDataFromCasADi)
{
    TestSimulatorType::OutputVector y = m_simulator.GetOutput(m_test_data.x, m_test_data.u, m_test_data.T_HP);

    EXPECT_TRUE(y.isApprox(m_test_data.y));
}

TEST_F(SimulatorTest, GivenSimulator_whenCalculatingOutputJacobian_thenResultsMatchWithDataFromCasADi)
{
    TestSimulatorType::OutputVector y;
    TestSimulatorType::OutputStateMatrix dy_dx;
    TestSimulatorType::OutputInputMatrix dy_du;
    m_simulator.GetOutputJacobian(m_test_data.x, m_test_data.u, m_test_data.T_HP, y, dy_dx, dy_du);

    EXPECT_TRUE(y.isApprox(m_test_data.y));
    EXPECT_TRUE(dy_dx.isApprox(m_test_data.dy_dx));
    EXPECT_TRUE(dy_du.isApprox(m_test_data.dy_du));
}

TEST_F(SimulatorTest, GivenSimulator_whenIntegratingOneStepWithUnitaryInput_thenResultsAreCorrect)
{
    TestSimulatorType::StateVector xf = m_simulator.Integrate(TestSimulatorType::StateVector::Constant(1.),
                                                              TestSimulatorType::InputVector::Constant(1.), 1.);

    TestSimulatorType::StateVector xf_check;
    xf_check << TestSimulatorType::AxesVector::Constant(2.5), TestSimulatorType::AxesVector::Constant(2.);
    EXPECT_TRUE(xf.isApprox(xf_check));
}

TEST_F(SimulatorTest, GivenSimulator_whenCalculatingConstraints_thenResultsMatchWithDataFromCasADi)
{
    TestSimulatorType::ConstraintVector g = m_simulator.GetConstraint(m_test_data.x, m_test_data.u);

    EXPECT_TRUE(g.isApprox(m_test_data.g));
}

TEST_F(SimulatorTest, GivenSimulator_whenCalculatingHeadPose_thenResultsMatchWithDataFromCasADi)
{
    PoseRotMatVector head_pva = m_simulator.GetHeadPva(m_test_data.x, m_test_data.u, m_test_data.T_HP);
    InertialVector head_inertial = m_simulator.GetHeadInertial(m_test_data.x, m_test_data.u, m_test_data.T_HP);

    EXPECT_TRUE(head_pva.isApprox(m_test_data.head_pva));
    EXPECT_TRUE(head_inertial.isApprox(m_test_data.head_inertial));
}

TEST_F(SimulatorTest, GivenSimulator_thenInfoMatchWithInfoFromCasADi)
{
    TestSimulatorType::InfoVector info = m_simulator.GetInfo();
    EXPECT_TRUE(info.isApprox(m_test_data.info));
}

TEST_F(SimulatorTest, GivenSimulator_thenNameMatchWithNameFromCasADi)
{
    std::string name = m_simulator.GetName();
    EXPECT_EQ(name, m_test_data.name);
}
}  //namespace mpmca::control::testing