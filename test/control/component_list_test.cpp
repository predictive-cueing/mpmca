/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <gtest/gtest.h>

#include "mpmca/control/constraint.hpp"
#include "mpmca/control/info.hpp"
#include "mpmca/control/input.hpp"
#include "mpmca/control/state.hpp"
#include "mpmca/linear_algebra.hpp"

namespace mpmca::control::testing {
struct TestSimulator
{
    static constexpr std::size_t NY_SIMULATOR = 9;
    using ComponentList = templates::ComponentList<components::XyTable,  //
                                                   components::Hexapod,  //
                                                   components::TransformationMatrix,  //
                                                   components::YawTable,  //
                                                   components::YzTracks,  //
                                                   components::Gimbal,  //
                                                   components::RevoluteLink,  //
                                                   components::PrismaticLink,  //
                                                   components::ConstraintFreeBox>;
    using Dimension = templates::DimensionsT<TestSimulator>;
    using AxesVector = Vector<Dimension::NQ>;
    using StateVector = Vector<Dimension::NX>;
    using AlgStateVector = Vector<Dimension::NZ>;
    using InputVector = Vector<Dimension::NU>;
    using ConstraintVector = Vector<Dimension::NC>;
    using ParameterVector = Vector<Dimension::NP>;
    using OutputVector = Vector<Dimension::NY>;
    using InfoVector = Vector<Dimension::NI>;

    using StateStateMatrix = Matrix<Dimension::NX, Dimension::NX>;
    using StateInputMatrix = Matrix<Dimension::NX, Dimension::NU>;
    using InputInputMatrix = Matrix<Dimension::NU, Dimension::NU>;
    using OutputOutputMatrix = Matrix<Dimension::NY, Dimension::NY>;
    using OutputStateMatrix = Matrix<Dimension::NY, Dimension::NX>;
    using OutputInputMatrix = Matrix<Dimension::NY, Dimension::NU>;
    using ConstraintStateMatrix = Matrix<Dimension::NC, Dimension::NX>;
    using ConstraintInputMatrix = Matrix<Dimension::NC, Dimension::NU>;

    using OutputStateStateMatrix = Matrix<Dimension::NX * Dimension::NY, Dimension::NX>;
    using OutputStateInputMatrix = Matrix<Dimension::NX * Dimension::NY, Dimension::NU>;
    using OutputInputInputMatrix = Matrix<Dimension::NU * Dimension::NY, Dimension::NU>;
    using ConstraintStateStateMatrix = Matrix<Dimension::NX * Dimension::NC, Dimension::NX>;
    using ConstraintStateInputMatrix = Matrix<Dimension::NX * Dimension::NC, Dimension::NU>;
    using ConstraintInputInputMatrix = Matrix<Dimension::NU * Dimension::NC, Dimension::NU>;

    using InputMatrix = Eigen::Matrix<double, Dimension::NU, Eigen::Dynamic>;
    using StateMatrix = Eigen::Matrix<double, Dimension::NX, Eigen::Dynamic>;
    using OutputMatrix = Eigen::Matrix<double, Dimension::NY, Eigen::Dynamic>;
};

class ComponentListTest : public ::testing::Test
{
  protected:
    using d = TestSimulator::Dimension;
};

TEST_F(ComponentListTest, TestDimensions)
{
    EXPECT_EQ(22, d::NQ);  // 2 + 6 + 0 + 1 + 2 + 3 +, + 1 + 1  6
    EXPECT_EQ(2, d::NQ_at<0>);
    EXPECT_EQ(6, d::NQ_at<1>);
    EXPECT_EQ(0, d::NQ_at<2>);
    EXPECT_EQ(1, d::NQ_at<3>);
    EXPECT_EQ(2, d::NQ_at<4>);
    EXPECT_EQ(3, d::NQ_at<5>);
    EXPECT_EQ(1, d::NQ_at<6>);
    EXPECT_EQ(1, d::NQ_at<7>);
    EXPECT_EQ(6, d::NQ_at<8>);
    EXPECT_EQ(0, d::NQ_prev<0>);
    EXPECT_EQ(2, d::NQ_prev<1>);
    EXPECT_EQ(8, d::NQ_prev<2>);
    EXPECT_EQ(8, d::NQ_prev<3>);
    EXPECT_EQ(9, d::NQ_prev<4>);
    EXPECT_EQ(11, d::NQ_prev<5>);
    EXPECT_EQ(14, d::NQ_prev<6>);
    EXPECT_EQ(15, d::NQ_prev<7>);
    EXPECT_EQ(16, d::NQ_prev<8>);
    EXPECT_EQ(22, d::NQ_prev<9>);

    EXPECT_EQ(44, d::NX);
    EXPECT_EQ(4, d::NX_at<0>);
    EXPECT_EQ(12, d::NX_at<1>);
    EXPECT_EQ(0, d::NX_at<2>);
    EXPECT_EQ(2, d::NX_at<3>);
    EXPECT_EQ(4, d::NX_at<4>);
    EXPECT_EQ(6, d::NX_at<5>);
    EXPECT_EQ(2, d::NX_at<6>);
    EXPECT_EQ(2, d::NX_at<7>);
    EXPECT_EQ(12, d::NX_at<8>);
    EXPECT_EQ(0, d::NX_prev<0>);
    EXPECT_EQ(4, d::NX_prev<1>);
    EXPECT_EQ(16, d::NX_prev<2>);
    EXPECT_EQ(16, d::NX_prev<3>);
    EXPECT_EQ(18, d::NX_prev<4>);
    EXPECT_EQ(22, d::NX_prev<5>);
    EXPECT_EQ(28, d::NX_prev<6>);
    EXPECT_EQ(30, d::NX_prev<7>);
    EXPECT_EQ(32, d::NX_prev<8>);
    EXPECT_EQ(44, d::NX_prev<9>);

    EXPECT_EQ(22, d::NU);
    EXPECT_EQ(2, d::NU_at<0>);
    EXPECT_EQ(6, d::NU_at<1>);
    EXPECT_EQ(0, d::NU_at<2>);
    EXPECT_EQ(1, d::NU_at<3>);
    EXPECT_EQ(2, d::NU_at<4>);
    EXPECT_EQ(3, d::NU_at<5>);
    EXPECT_EQ(1, d::NU_at<6>);
    EXPECT_EQ(1, d::NU_at<7>);
    EXPECT_EQ(6, d::NU_at<8>);
    EXPECT_EQ(0, d::NU_prev<0>);
    EXPECT_EQ(2, d::NU_prev<1>);
    EXPECT_EQ(8, d::NU_prev<2>);
    EXPECT_EQ(8, d::NU_prev<3>);
    EXPECT_EQ(9, d::NU_prev<4>);
    EXPECT_EQ(11, d::NU_prev<5>);
    EXPECT_EQ(14, d::NU_prev<6>);
    EXPECT_EQ(15, d::NU_prev<7>);
    EXPECT_EQ(16, d::NU_prev<8>);
    EXPECT_EQ(22, d::NU_prev<9>);

    EXPECT_EQ(18, d::NC);
    EXPECT_EQ(0, d::NC_at<0>);
    EXPECT_EQ(18, d::NC_at<1>);
    EXPECT_EQ(0, d::NC_at<2>);
    EXPECT_EQ(0, d::NC_at<3>);
    EXPECT_EQ(0, d::NC_at<4>);
    EXPECT_EQ(0, d::NC_at<5>);
    EXPECT_EQ(0, d::NC_at<6>);
    EXPECT_EQ(0, d::NC_at<7>);
    EXPECT_EQ(0, d::NC_at<8>);
    EXPECT_EQ(0, d::NC_prev<0>);
    EXPECT_EQ(0, d::NC_prev<1>);
    EXPECT_EQ(18, d::NC_prev<2>);
    EXPECT_EQ(18, d::NC_prev<3>);
    EXPECT_EQ(18, d::NC_prev<4>);
    EXPECT_EQ(18, d::NC_prev<5>);
    EXPECT_EQ(18, d::NC_prev<6>);
    EXPECT_EQ(18, d::NC_prev<7>);
    EXPECT_EQ(18, d::NC_prev<8>);
    EXPECT_EQ(18, d::NC_prev<9>);

    EXPECT_EQ(100, d::NI);
    EXPECT_EQ(6, d::NI_at<0>);
    EXPECT_EQ(42, d::NI_at<1>);
    EXPECT_EQ(12, d::NI_at<2>);
    EXPECT_EQ(6, d::NI_at<3>);
    EXPECT_EQ(6, d::NI_at<4>);
    EXPECT_EQ(6, d::NI_at<5>);
    EXPECT_EQ(8, d::NI_at<6>);
    EXPECT_EQ(8, d::NI_at<7>);
    EXPECT_EQ(6, d::NI_at<8>);
    EXPECT_EQ(0, d::NI_prev<0>);
    EXPECT_EQ(6, d::NI_prev<1>);
    EXPECT_EQ(48, d::NI_prev<2>);
    EXPECT_EQ(60, d::NI_prev<3>);
    EXPECT_EQ(66, d::NI_prev<4>);
    EXPECT_EQ(72, d::NI_prev<5>);
    EXPECT_EQ(78, d::NI_prev<6>);
    EXPECT_EQ(86, d::NI_prev<7>);
    EXPECT_EQ(94, d::NI_prev<8>);
    EXPECT_EQ(100, d::NI_prev<9>);
}

TEST_F(ComponentListTest, TestStateView)
{
    // Test vector
    TestSimulator::StateVector x = TestSimulator::StateVector::LinSpaced(1, d::NX);

    // Create viewer
    auto x_view = MakeStateView<TestSimulator>(x);

    // Test
    EXPECT_TRUE(x_view.GetPosition().isApprox(Vector<22>::LinSpaced(1, 22)));
    EXPECT_TRUE(x_view.GetVelocity().isApprox(Vector<22>::LinSpaced(23, 44)));

    EXPECT_TRUE(
        x_view.At<0>().isApprox((Vector<4>() << Vector<2>::LinSpaced(1, 2), Vector<2>::LinSpaced(23, 24)).finished()));
    EXPECT_TRUE(
        x_view.At<1>().isApprox((Vector<12>() << Vector<6>::LinSpaced(3, 8), Vector<6>::LinSpaced(25, 30)).finished()));
    EXPECT_TRUE(x_view.At<2>().isApprox((Vector<0>())));
    EXPECT_TRUE(x_view.At<3>().isApprox((Vector<2>() << 9, 31).finished()));
    EXPECT_TRUE(x_view.At<4>().isApprox(
        (Vector<4>() << Vector<2>::LinSpaced(10, 11), Vector<2>::LinSpaced(32, 33)).finished()));
    EXPECT_TRUE(x_view.At<5>().isApprox(
        (Vector<6>() << Vector<3>::LinSpaced(12, 14), Vector<3>::LinSpaced(34, 36)).finished()));
    EXPECT_TRUE(x_view.At<6>().isApprox((Vector<2>() << 15, 37).finished()));
    EXPECT_TRUE(x_view.At<7>().isApprox((Vector<2>() << 16, 38).finished()));
    EXPECT_TRUE(x_view.At<8>().isApprox(
        (Vector<12>() << Vector<6>::LinSpaced(17, 22), Vector<6>::LinSpaced(39, 44)).finished()));

    EXPECT_TRUE(x_view.GetPositionAt<0>().isApprox(Vector<2>::LinSpaced(1, 2)));
    EXPECT_TRUE(x_view.GetPositionAt<1>().isApprox(Vector<6>::LinSpaced(3, 8)));
    EXPECT_TRUE(x_view.GetPositionAt<2>().isApprox(Vector<0>()));
    EXPECT_TRUE(x_view.GetPositionAt<3>().isApprox(Vector<1>::LinSpaced(9, 9)));
    EXPECT_TRUE(x_view.GetPositionAt<4>().isApprox(Vector<2>::LinSpaced(10, 11)));
    EXPECT_TRUE(x_view.GetPositionAt<5>().isApprox(Vector<3>::LinSpaced(12, 14)));
    EXPECT_TRUE(x_view.GetPositionAt<6>().isApprox(Vector<1>::LinSpaced(15, 15)));
    EXPECT_TRUE(x_view.GetPositionAt<7>().isApprox(Vector<1>::LinSpaced(16, 16)));
    EXPECT_TRUE(x_view.GetPositionAt<8>().isApprox(Vector<6>::LinSpaced(17, 22)));

    EXPECT_TRUE(x_view.GetVelocityAt<0>().isApprox(Vector<2>::LinSpaced(23, 24)));
    EXPECT_TRUE(x_view.GetVelocityAt<1>().isApprox(Vector<6>::LinSpaced(25, 30)));
    EXPECT_TRUE(x_view.GetVelocityAt<2>().isApprox(Vector<0>()));
    EXPECT_TRUE(x_view.GetVelocityAt<3>().isApprox(Vector<1>::LinSpaced(31, 31)));
    EXPECT_TRUE(x_view.GetVelocityAt<4>().isApprox(Vector<2>::LinSpaced(32, 33)));
    EXPECT_TRUE(x_view.GetVelocityAt<5>().isApprox(Vector<3>::LinSpaced(34, 36)));
    EXPECT_TRUE(x_view.GetVelocityAt<6>().isApprox(Vector<1>::LinSpaced(37, 37)));
    EXPECT_TRUE(x_view.GetVelocityAt<7>().isApprox(Vector<1>::LinSpaced(38, 38)));
    EXPECT_TRUE(x_view.GetVelocityAt<8>().isApprox(Vector<6>::LinSpaced(39, 44)));

    // 0 components::XyTable                  2
    // 1 components::Hexapod                  6
    // 2 components::TransformationMatrix     0
    // 3 components::YawTable                 1
    // 4 components::YzTracks                 2
    // 5 components::Gimbal                   3
    // 6 components::RevoluteLink             1
    // 7 components::PrismaticLink            1
    // 8 components::ConstraintFreeBox        6
    auto xy_table_view = x_view.ViewAt<0>();
    EXPECT_EQ(1, xy_table_view.GetX());
    EXPECT_EQ(2, xy_table_view.GetY());
    EXPECT_EQ(23, xy_table_view.GetXd());
    EXPECT_EQ(24, xy_table_view.GetYd());

    auto hexapod_view = x_view.ViewAt<1>();
    EXPECT_EQ(3, hexapod_view.GetX());
    EXPECT_EQ(4, hexapod_view.GetY());
    EXPECT_EQ(5, hexapod_view.GetZ());
    EXPECT_EQ(6, hexapod_view.GetPhi());
    EXPECT_EQ(7, hexapod_view.GetTheta());
    EXPECT_EQ(8, hexapod_view.GetPsi());
    EXPECT_EQ(25, hexapod_view.GetXd());
    EXPECT_EQ(26, hexapod_view.GetYd());
    EXPECT_EQ(27, hexapod_view.GetZd());
    EXPECT_EQ(28, hexapod_view.GetP());
    EXPECT_EQ(29, hexapod_view.GetQ());
    EXPECT_EQ(30, hexapod_view.GetR());

    auto yaw_table_view = x_view.ViewAt<3>();
    EXPECT_EQ(9, yaw_table_view.GetPsi());
    EXPECT_EQ(31, yaw_table_view.GetR());

    auto yz_tracks_view = x_view.ViewAt<4>();
    EXPECT_EQ(10, yz_tracks_view.GetY());
    EXPECT_EQ(11, yz_tracks_view.GetZ());
    EXPECT_EQ(32, yz_tracks_view.GetYd());
    EXPECT_EQ(33, yz_tracks_view.GetZd());

    auto gimbal_view = x_view.ViewAt<5>();
    EXPECT_EQ(12, gimbal_view.GetPhi());
    EXPECT_EQ(13, gimbal_view.GetTheta());
    EXPECT_EQ(14, gimbal_view.GetPsi());
    EXPECT_EQ(34, gimbal_view.GetP());
    EXPECT_EQ(35, gimbal_view.GetQ());
    EXPECT_EQ(36, gimbal_view.GetR());

    auto revolute_link_view = x_view.ViewAt<6>();
    EXPECT_EQ(15, revolute_link_view.GetTheta());
    EXPECT_EQ(37, revolute_link_view.GetQ());

    auto prismatic_link_view = x_view.ViewAt<7>();
    EXPECT_EQ(16, prismatic_link_view.GetD());
    EXPECT_EQ(38, prismatic_link_view.GetDd());

    auto constraint_free_box_view = x_view.ViewAt<8>();
    EXPECT_EQ(17, constraint_free_box_view.GetX());
    EXPECT_EQ(18, constraint_free_box_view.GetY());
    EXPECT_EQ(19, constraint_free_box_view.GetZ());
    EXPECT_EQ(20, constraint_free_box_view.GetPhi());
    EXPECT_EQ(21, constraint_free_box_view.GetTheta());
    EXPECT_EQ(22, constraint_free_box_view.GetPsi());

    EXPECT_EQ(39, constraint_free_box_view.GetXd());
    EXPECT_EQ(40, constraint_free_box_view.GetYd());
    EXPECT_EQ(41, constraint_free_box_view.GetZd());
    EXPECT_EQ(42, constraint_free_box_view.GetP());
    EXPECT_EQ(43, constraint_free_box_view.GetQ());
    EXPECT_EQ(44, constraint_free_box_view.GetR());
}

TEST_F(ComponentListTest, TestConstraintView)
{
    // Test vectors
    TestSimulator::ConstraintVector g = TestSimulator::ConstraintVector::LinSpaced(1, d::NC);

    // Create viewer
    auto g_view = MakeConstraintView<TestSimulator>(g);

    // Test
    EXPECT_TRUE(g_view.At<0>().isApprox(Vector<0>::LinSpaced(1, 1)));
    EXPECT_TRUE(g_view.At<1>().isApprox(Vector<18>::LinSpaced(1, 18)));
    EXPECT_TRUE(g_view.At<2>().isApprox(Vector<0>::LinSpaced(18, 18)));
    EXPECT_TRUE(g_view.At<3>().isApprox(Vector<0>::LinSpaced(18, 18)));
    EXPECT_TRUE(g_view.At<4>().isApprox(Vector<0>::LinSpaced(18, 18)));
    EXPECT_TRUE(g_view.At<5>().isApprox(Vector<0>::LinSpaced(18, 18)));
    EXPECT_TRUE(g_view.ViewAt<1>().GetLegLength().isApprox(Vector<6>::LinSpaced(1, 6)));
    EXPECT_TRUE(g_view.ViewAt<1>().GetLegVelocity().isApprox(Vector<6>::LinSpaced(7, 12)));
    EXPECT_TRUE(g_view.ViewAt<1>().GetLegAcceleration().isApprox(Vector<6>::LinSpaced(13, 18)));

    // Modify constraint vector with At()
    TestSimulator::ConstraintVector g_mod = g;
    MakeConstraintView<TestSimulator>(g_mod).At<1>() = components::Hexapod::ConstraintVector::Constant(1.);

    // Test
    EXPECT_TRUE(g_mod.isApprox(Vector<18>::Constant(1.)));

    // Modify constraint vector with the viewer
    TestSimulator::ConstraintVector g_chk = g;
    MakeConstraintView<TestSimulator>(g_mod).ViewAt<1>().GetLegLength() = Vector<6>::Constant(1.);
    MakeConstraintView<TestSimulator>(g_mod).ViewAt<1>().GetLegVelocity() = Vector<6>::Constant(2.);
    MakeConstraintView<TestSimulator>(g_mod).ViewAt<1>().GetLegAcceleration() = Vector<6>::Constant(3.);
    g_chk << Vector<6>::Constant(1.), Vector<6>::Constant(2.), Vector<6>::Constant(3.);

    // // Test
    EXPECT_TRUE(g_mod.isApprox(g_chk));
}

TEST_F(ComponentListTest, TestInputView)
{
    // Test vectors
    TestSimulator::InputVector u = TestSimulator::InputVector::LinSpaced(1, TestSimulator::Dimension::NU);

    // Create viewer
    auto u_view = MakeInputView<TestSimulator>(u);

    // Test
    EXPECT_TRUE(u_view.At<0>().isApprox(Vector<2>::LinSpaced(1, 2)));
    EXPECT_TRUE(u_view.At<1>().isApprox(Vector<6>::LinSpaced(3, 8)));
    EXPECT_TRUE(u_view.At<2>().isApprox(Vector<0>()));
    EXPECT_TRUE(u_view.At<3>().isApprox(Vector<1>::LinSpaced(9, 9)));
    EXPECT_TRUE(u_view.At<4>().isApprox(Vector<2>::LinSpaced(10, 11)));
    EXPECT_TRUE(u_view.At<5>().isApprox(Vector<3>::LinSpaced(12, 14)));
}

TEST_F(ComponentListTest, TestInfoView)
{
    // Test vectors
    TestSimulator::InfoVector info = TestSimulator::InfoVector::LinSpaced(1, TestSimulator::Dimension::NI);

    // Create viewer
    auto info_view = MakeInfoView<TestSimulator>(info);

    // Test
    EXPECT_TRUE(info_view.At<0>().isApprox(Vector<6>::LinSpaced(1, 6)));
    EXPECT_TRUE(info_view.At<1>().isApprox(Vector<42>::LinSpaced(7, 48)));
    EXPECT_TRUE(info_view.At<2>().isApprox(Vector<12>::LinSpaced(49, 60)));
    EXPECT_TRUE(info_view.At<3>().isApprox(Vector<6>::LinSpaced(61, 66)));
    EXPECT_TRUE(info_view.At<4>().isApprox(Vector<6>::LinSpaced(67, 72)));
    EXPECT_TRUE(info_view.At<5>().isApprox(Vector<6>::LinSpaced(73, 78)));
    EXPECT_TRUE(info_view.ViewAt<0>().GetActiveDoF().isApprox(Vector<6>::LinSpaced(1, 6)));
    EXPECT_TRUE(info_view.ViewAt<1>().GetHexapodBaseCorners().isApprox(Vector<18>::LinSpaced(7, 24)));
    EXPECT_TRUE(info_view.ViewAt<1>().GetHexapodPlatformCorners().isApprox(Vector<18>::LinSpaced(25, 42)));
    EXPECT_TRUE(info_view.ViewAt<1>().GetActiveDoF().isApprox(Vector<6>::LinSpaced(43, 48)));
    EXPECT_TRUE(info_view.ViewAt<2>().GetXyz().isApprox(Vector<3>::LinSpaced(49, 51)));
    EXPECT_TRUE(info_view.ViewAt<2>().GetRollPitchYaw().isApprox(Vector<3>::LinSpaced(52, 54)));
    EXPECT_TRUE(info_view.ViewAt<2>().GetActiveDoF().isApprox(Vector<6>::LinSpaced(55, 60)));
    EXPECT_TRUE(info_view.ViewAt<3>().GetActiveDoF().isApprox(Vector<6>::LinSpaced(61, 66)));
    EXPECT_TRUE(info_view.ViewAt<4>().GetActiveDoF().isApprox(Vector<6>::LinSpaced(67, 72)));
    EXPECT_TRUE(info_view.ViewAt<5>().GetActiveDoF().isApprox(Vector<6>::LinSpaced(73, 78)));

    // Modify info vector with At()
    // 0 components::XyTable                  6
    // 1 components::Hexapod                  42
    // 2 components::TransformationMatrix     12
    // 3 components::YawTable                 6
    // 4 components::YzTracks                 6
    // 5 components::Gimbal                   6
    // 6 components::RevoluteLink             8
    // 7 components::PrismaticLink            8
    // 8 components::ConstraintFreeBox        6
    TestSimulator::InfoVector info_mod = info;
    info_mod = TestSimulator::InfoVector::Random();

    TestSimulator::InfoVector info_chk;
    MakeInfoView<TestSimulator>(info_mod).At<0>() = components::XyTable::InfoVector::Zero();
    MakeInfoView<TestSimulator>(info_mod).At<1>() = components::Hexapod::InfoVector::Constant(1.);
    MakeInfoView<TestSimulator>(info_mod).At<2>() = components::TransformationMatrix::InfoVector::Constant(2.);
    MakeInfoView<TestSimulator>(info_mod).At<3>() = components::YawTable::InfoVector::Constant(3.);
    MakeInfoView<TestSimulator>(info_mod).At<4>() = components::YzTracks::InfoVector::Constant(4.);
    MakeInfoView<TestSimulator>(info_mod).At<5>() = components::Gimbal::InfoVector::Constant(5.);
    MakeInfoView<TestSimulator>(info_mod).At<6>() = components::RevoluteLink::InfoVector::Constant(6.);
    MakeInfoView<TestSimulator>(info_mod).At<7>() = components::PrismaticLink::InfoVector::Constant(7.);
    MakeInfoView<TestSimulator>(info_mod).At<8>() = components::ConstraintFreeBox::InfoVector::Constant(8.);
    info_chk << Vector<6>::Zero(),  //
        Vector<42>::Constant(1.),  //
        Vector<12>::Constant(2.),  //
        Vector<6>::Constant(3.),  //
        Vector<6>::Constant(4.),  //
        Vector<6>::Constant(5.),  //
        Vector<8>::Constant(6.),  //
        Vector<8>::Constant(7.),  //
        Vector<6>::Constant(8.);

    // Test
    EXPECT_TRUE(info_mod.isApprox(info_chk));

    info_mod = TestSimulator::InfoVector::Random();

    // Modify info vector with the viewer
    MakeInfoView<TestSimulator>(info_mod).ViewAt<0>().GetActiveDoF() = Vector<6>::Constant(1.);
    MakeInfoView<TestSimulator>(info_mod).ViewAt<1>().GetHexapodBaseCorners() = Vector<18>::Constant(2.);
    MakeInfoView<TestSimulator>(info_mod).ViewAt<1>().GetHexapodPlatformCorners() = Vector<18>::Constant(3.);
    MakeInfoView<TestSimulator>(info_mod).ViewAt<1>().GetActiveDoF() = Vector<6>::Constant(4.);
    MakeInfoView<TestSimulator>(info_mod).ViewAt<2>().GetXyz() = Vector<3>::Constant(5.);
    MakeInfoView<TestSimulator>(info_mod).ViewAt<2>().GetRollPitchYaw() = Vector<3>::Constant(6.);
    MakeInfoView<TestSimulator>(info_mod).ViewAt<2>().GetActiveDoF() = Vector<6>::Constant(7.);
    MakeInfoView<TestSimulator>(info_mod).ViewAt<3>().GetActiveDoF() = Vector<6>::Constant(8.);
    MakeInfoView<TestSimulator>(info_mod).ViewAt<4>().GetActiveDoF() = Vector<6>::Constant(9.);
    MakeInfoView<TestSimulator>(info_mod).ViewAt<5>().GetActiveDoF() = Vector<6>::Constant(10.);

    MakeInfoView<TestSimulator>(info_mod).ViewAt<6>().GetThetaOffset() = 11.;
    MakeInfoView<TestSimulator>(info_mod).ViewAt<6>().GetD() = 12.;
    MakeInfoView<TestSimulator>(info_mod).ViewAt<6>().GetAlpha() = 13.;
    MakeInfoView<TestSimulator>(info_mod).ViewAt<6>().GetA() = 14.;
    MakeInfoView<TestSimulator>(info_mod).ViewAt<6>().GetActiveDoF() = Vector<4>::Constant(15.);

    MakeInfoView<TestSimulator>(info_mod).ViewAt<7>().GetTheta() = 16.;
    MakeInfoView<TestSimulator>(info_mod).ViewAt<7>().GetDOffset() = 17.;
    MakeInfoView<TestSimulator>(info_mod).ViewAt<7>().GetAlpha() = 18.;
    MakeInfoView<TestSimulator>(info_mod).ViewAt<7>().GetA() = 19.;
    MakeInfoView<TestSimulator>(info_mod).ViewAt<7>().GetActiveDoF() = Vector<4>::Constant(20.);

    MakeInfoView<TestSimulator>(info_mod).ViewAt<8>().GetActiveDoF() = Vector<6>::Constant(21.);

    info_chk << Vector<6>::Constant(1.),  // 0
        Vector<18>::Constant(2.),  // 1
        Vector<18>::Constant(3.),  // 1
        Vector<6>::Constant(4.),  // 3
        Vector<3>::Constant(5.),  // 4
        Vector<3>::Constant(6.),  // 5
        Vector<6>::Constant(7.),  // 6
        Vector<6>::Constant(8.),  // 7
        Vector<6>::Constant(9.),  // 8
        Vector<6>::Constant(10.),  // 9
        11.0, 12.0, 13.0, 14.0, Vector<4>::Constant(15.0),  //
        16.0, 17.0, 18.0, 19.0, Vector<4>::Constant(20.0),  //
        Vector<6>::Constant(21.);

    // Test
    EXPECT_TRUE(info_mod.isApprox(info_chk));
}

TEST_F(ComponentListTest, TestPrintName)
{
    // Get simulator name
    std::vector<std::string> names = TestSimulator::ComponentList::GetName();

    // Modify info vector with At()
    // 0 components::XyTable                  6
    // 1 components::Hexapod                  42
    // 2 components::TransformationMatrix     12
    // 3 components::YawTable                 6
    // 4 components::YzTracks                 6
    // 5 components::Gimbal                   6
    // 6 components::RevoluteLink             8
    // 7 components::PrismaticLink            8
    // 8 components::ConstraintFreeBox        6

    // Test
    EXPECT_EQ(names[0], "XyTable");
    EXPECT_EQ(names[1], "Hexapod");
    EXPECT_EQ(names[2], "TransformationMatrix");
    EXPECT_EQ(names[3], "YawTable");
    EXPECT_EQ(names[4], "YzTracks");
    EXPECT_EQ(names[5], "Gimbal");
    EXPECT_EQ(names[6], "RevoluteLink");
    EXPECT_EQ(names[7], "PrismaticLink");
    EXPECT_EQ(names[8], "ConstraintFreeBox");
}
}  //namespace mpmca::control::testing
