/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/control/ocp.hpp"

#include <gtest/gtest.h>

#include "generic_test_header.hpp"
#include "mpmca/control/ocp_data.hpp"
#include "mpmca/control/templates/component.hpp"
#include "mpmca/control/templates/component_list.hpp"
#include "mpmca/control/test_data.hpp"

namespace mpmca::control::testing {
class OcpTest : public ::testing::Test
{
  protected:
    TestData<TestSimulatorType> m_data;

    OcpTest()
        : m_data(std::string(TEST_DATA_PATH) + TestSimulatorType::GetSnakeCaseName() + "_data.json")
    {
    }
};

TEST_F(OcpTest, GivenHessianFunctions_whileCalculatingCost_thenEqualToPython)
{
    Cost<TestSimulatorType, HessianCalculationMethod::kGaussNewton> hessian_functions_gauss;
    Cost<TestSimulatorType, HessianCalculationMethod::kFull> hessian_functions_full;

    double J_gauss = hessian_functions_gauss.GetCostDt(m_data.x, m_data.u, m_data.y_ref, m_data.x_ref, m_data.T_HP,
                                                       m_data.wx, m_data.wu, m_data.wy);
    double J_full = hessian_functions_full.GetCostDt(m_data.x, m_data.u, m_data.y_ref, m_data.x_ref, m_data.T_HP,
                                                     m_data.wx, m_data.wu, m_data.wy);

    EXPECT_EQ(J_gauss, m_data.J(0, 0));
    EXPECT_EQ(J_full, m_data.J(0, 0));
}

TEST_F(OcpTest, GivenHessianFunctions_whileCalculatingCostJacobian_thenEqualToPython)
{
    Cost<TestSimulatorType, HessianCalculationMethod::kGaussNewton> hessian_functions_gauss;
    Cost<TestSimulatorType, HessianCalculationMethod::kFull> hessian_functions_full;

    TestSimulatorType::StateVector dJ_dx_gauss, dJ_dx_full;
    TestSimulatorType::InputVector dJ_du_gauss, dJ_du_full;
    hessian_functions_gauss.GetCostJacobianDt(m_data.x, m_data.u, m_data.y_ref, m_data.x_ref, m_data.T_HP, m_data.wx,
                                              m_data.wu, m_data.wy, dJ_dx_gauss, dJ_du_gauss);
    hessian_functions_full.GetCostJacobianDt(m_data.x, m_data.u, m_data.y_ref, m_data.x_ref, m_data.T_HP, m_data.wx,
                                             m_data.wu, m_data.wy, dJ_dx_full, dJ_du_full);

    EXPECT_TRUE(dJ_dx_gauss.isApprox(m_data.dJ_dx));
    EXPECT_TRUE(dJ_du_gauss.isApprox(m_data.dJ_du));
    EXPECT_TRUE(dJ_dx_full.isApprox(m_data.dJ_dx));
    EXPECT_TRUE(dJ_du_full.isApprox(m_data.dJ_du));
}

TEST_F(OcpTest, GivenHessianFunctions_whileCalculatingHessianGaussNewtonHessian_thenEqualToPython)
{
    Cost<TestSimulatorType, HessianCalculationMethod::kGaussNewton> hessian_functions;

    TestSimulatorType::StateVector dJ_dx;
    TestSimulatorType::InputVector dJ_du;
    TestSimulatorType::StateStateMatrix dJ2_dx2;
    TestSimulatorType::StateInputMatrix dJ2_dx_du;
    TestSimulatorType::InputInputMatrix dJ2_du2;
    hessian_functions.GetCostHessianDt(m_data.x, m_data.u, m_data.y_ref, m_data.x_ref, m_data.T_HP, m_data.wx,
                                       m_data.wu, m_data.wy, dJ_dx, dJ_du, dJ2_dx2, dJ2_dx_du, dJ2_du2);

    EXPECT_TRUE(dJ_dx.isApprox(m_data.dJ_dx));
    EXPECT_TRUE(dJ_du.isApprox(m_data.dJ_du));
    EXPECT_TRUE(dJ2_dx2.isApprox(m_data.d2L_dx2_gauss_newton));
    EXPECT_TRUE(dJ2_dx_du.isApprox(m_data.d2L_dx_du_gauss_newton));
    EXPECT_TRUE(dJ2_du2.isApprox(m_data.d2L_du2_gauss_newton));
}

TEST_F(OcpTest, GivenHessianFunctions_whileCalculatingFullHessian_thenEqualToPython)
{
    Cost<TestSimulatorType, HessianCalculationMethod::kFull> hessian_functions;

    TestSimulatorType::StateVector dJ_dx;
    TestSimulatorType::InputVector dJ_du;
    TestSimulatorType::StateStateMatrix dJ2_dx2;
    TestSimulatorType::StateInputMatrix dJ2_dx_du;
    TestSimulatorType::InputInputMatrix dJ2_du2;
    hessian_functions.GetCostHessianDt(m_data.x, m_data.u, m_data.y_ref, m_data.x_ref, m_data.T_HP, m_data.wx,
                                       m_data.wu, m_data.wy, dJ_dx, dJ_du, dJ2_dx2, dJ2_dx_du, dJ2_du2);

    EXPECT_TRUE(dJ_dx.isApprox(m_data.dJ_dx));
    EXPECT_TRUE(dJ_du.isApprox(m_data.dJ_du));
    EXPECT_TRUE(dJ2_dx2.isApprox(m_data.d2J_dx2));
    EXPECT_TRUE(dJ2_dx_du.isApprox(m_data.d2J_dx_du));
    EXPECT_TRUE(dJ2_du2.isApprox(m_data.d2J_du2));
}

TEST_F(OcpTest, GivenHessianFunctions_whileAddHessianOfLagrangianMultipliers_thenZero)
{
    ConstraintsInLagrangian<TestSimulatorType, StateStoppabilityOption::kNone, HessianCalculationMethod::kGaussNewton>
        hessian_functions;
    TestSimulatorType::InputVector u_min = TestSimulatorType::InputVector::Random();
    TestSimulatorType::InputVector u_max = TestSimulatorType::InputVector::Random();
    TestSimulatorType::StateStateMatrix d2Lg_dx2 = TestSimulatorType::StateStateMatrix::Zero();
    TestSimulatorType::StateInputMatrix d2Lg_dx_du = TestSimulatorType::StateInputMatrix::Zero();
    TestSimulatorType::InputInputMatrix d2Lg_du2 = TestSimulatorType::InputInputMatrix::Zero();

    hessian_functions.AddHessianOfConstraints(m_data.x, m_data.u, m_data.lam_lbg, m_data.lam_ubg, u_min, u_max,
                                              d2Lg_dx2, d2Lg_dx_du, d2Lg_du2);

    EXPECT_TRUE(d2Lg_dx2.isApprox(TestSimulatorType::StateStateMatrix::Zero()));
    EXPECT_TRUE(d2Lg_dx_du.isApprox(TestSimulatorType::StateInputMatrix::Zero()));
    EXPECT_TRUE(d2Lg_du2.isApprox(TestSimulatorType::InputInputMatrix::Zero()));
}

TEST_F(OcpTest, GivenHessianCalculationMethod_dataWhileAddHessianOfLagrangianMultipliers_thenEqualToPython)
{
    ConstraintsInLagrangian<TestSimulatorType, StateStoppabilityOption::kNone, HessianCalculationMethod::kFull>
        hessian_functions;
    TestSimulatorType::InputVector u_min = TestSimulatorType::InputVector::Random();
    TestSimulatorType::InputVector u_max = TestSimulatorType::InputVector::Random();
    TestSimulatorType::StateStateMatrix d2Lg_dx2 = TestSimulatorType::StateStateMatrix::Zero();
    TestSimulatorType::StateInputMatrix d2Lg_dx_du = TestSimulatorType::StateInputMatrix::Zero();
    TestSimulatorType::InputInputMatrix d2Lg_du2 = TestSimulatorType::InputInputMatrix::Zero();

    hessian_functions.AddHessianOfConstraints(m_data.x, m_data.u, m_data.lam_lbg, m_data.lam_ubg, u_min, u_max,
                                              d2Lg_dx2, d2Lg_dx_du, d2Lg_du2);

    EXPECT_TRUE(d2Lg_dx2.isApprox(m_data.d2Lg_dx2));
    EXPECT_TRUE(d2Lg_dx_du.isApprox(m_data.d2Lg_dx_du));
    EXPECT_TRUE(d2Lg_du2.isApprox(m_data.d2Lg_du2));
}

}  //namespace mpmca::control::testing