/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/control/controller.hpp"

#include <gtest/gtest.h>

#include <cmath>
#include <fstream>

#include "generic_test_header.hpp"
#include "mpmca/constants.hpp"
#include "mpmca/control/controller_options.hpp"
#include "mpmca/control/ocp.hpp"
#include "mpmca/control/simulator.hpp"
#include "mpmca/control/state.hpp"
#include "mpmca/linear_algebra.hpp"

namespace mpmca::control::testing {
class ControllerTest : public ::testing::Test
{
  protected:
    double dt;
    unsigned mpc_horizon;
    unsigned mpc_step;
    TestSimulatorType::StateVector x0;
    TestSimulatorType::InputVector u0;
    std::string qp_solver_name;
    ControllerOptions<TestSimulatorType> options;

    ControllerTest()
        : dt{0.01}
        , mpc_horizon{10}
        , mpc_step{5}
        , x0{TestSimulatorType::StateVector::Zero()}
        , u0{TestSimulatorType::InputVector::Zero()}
        , options{}
    {
    }
};

TEST_F(ControllerTest, ConstructorsTest)
{
    // Parameters
    std::vector<unsigned> mpc_steps(mpc_horizon, mpc_step);

    // Constructors
    Controller<TestSimulatorType> c1(mpc_steps, dt, x0, u0);
    Controller<TestSimulatorType> c2(mpc_horizon, mpc_step, dt, x0, u0);
}

TEST_F(ControllerTest, MpcTimeVectors)
{
    // Parameters
    std::vector<unsigned> mpc_steps_fixed(mpc_horizon, mpc_step);
    std::vector<unsigned> mpc_steps_variable{1, 1, 1, 1, 10, 10, 10, 10};

    // Constructors
    Controller<TestSimulatorType> c1(mpc_horizon, mpc_step, dt, x0, u0);
    Controller<TestSimulatorType> c2(mpc_steps_fixed, dt, x0, u0);
    Controller<TestSimulatorType> c3(mpc_steps_variable, dt, x0, u0);

    // Test time vectors
    EXPECT_TRUE(c1.GetMpcHorizonLength() == mpc_horizon);
    EXPECT_TRUE(c2.GetMpcHorizonLength() == mpc_horizon);
    for (auto k = 0; k < mpc_horizon; k++) {
        EXPECT_TRUE(c1.GetMpcStep(k) == 5);
        EXPECT_TRUE(c1.GetMpcSampleTime(k) == 0.05);
        EXPECT_TRUE(c2.GetMpcStep(k) == 5);
        EXPECT_TRUE(c2.GetMpcSampleTime(k) == 0.05);
    }
    //
    EXPECT_TRUE(c3.GetMpcHorizonLength() == 8);
    for (auto k = 0; k < 8; k++) {
        if (k < 4) {
            EXPECT_TRUE(c3.GetMpcStep(k) == 1);
            EXPECT_TRUE(c3.GetMpcSampleTime(k) == 0.01);
        }
        else {
            EXPECT_TRUE(c3.GetMpcStep(k) == 10);
            EXPECT_TRUE(c3.GetMpcSampleTime(k) == 0.1);
        }
    }
}

TEST_F(ControllerTest, IntegrationTimeVectors)
{
    // Initialize
    ControllerOptions<TestSimulatorType> options_var;
    options_var.integration_method = "variableXY";

    // Constructors
    Controller<TestSimulatorType> c1(mpc_horizon, mpc_step, dt, x0, u0);
    Controller<TestSimulatorType> c2(mpc_horizon, mpc_step, dt, x0, u0, options_var);

    // Test time vectors
    EXPECT_TRUE(c1.GetIntegrationHorizon() == mpc_horizon);
    for (auto k = 0; k < mpc_horizon; k++) {
        EXPECT_TRUE(c1.GetIntegrationPoints(k) == 1);
        EXPECT_TRUE(c1.GetIntegrationSampleTime(k) == 0.05);
    }
    //
    EXPECT_TRUE(c2.GetIntegrationHorizon() == mpc_horizon * mpc_step);
    for (auto k = 0; k < mpc_horizon; k++) {
        EXPECT_TRUE(c2.GetIntegrationPoints(k) == 5);
        EXPECT_TRUE(c2.GetIntegrationSampleTime(k) == 0.01);
    }
}

TEST_F(ControllerTest, DefaultConfig)
{
    // Initialize
    Controller<TestSimulatorType> controller(mpc_horizon, mpc_step, dt, x0, u0);
    Simulator<TestSimulatorType> sim;

    // Reference
    for (auto k = 0; k < mpc_horizon; ++k) {
        EXPECT_TRUE(sim.GetNominalOutput().isApprox(controller.GetReferenceOutput(k)));
        EXPECT_TRUE(sim.GetNominalState().isApprox(controller.GetReferenceState(k)));
    }

    // Bounds
    auto cwiseEqual = [](double el1, double el2) { return el1 == el2; };
    EXPECT_TRUE((sim.GetStateLowerBound().binaryExpr(controller.GetStateLowerBound(), cwiseEqual)).all());
    EXPECT_TRUE((sim.GetStateUpperBound().binaryExpr(controller.GetStateUpperBound(), cwiseEqual)).all());
    EXPECT_TRUE((sim.GetInputLowerBound().binaryExpr(controller.GetInputLowerBound(), cwiseEqual)).all());
    EXPECT_TRUE((sim.GetInputUpperBound().binaryExpr(controller.GetInputUpperBound(), cwiseEqual)).all());
    EXPECT_TRUE((sim.GetConstraintLowerBound().binaryExpr(controller.GetConstraintLowerBound(), cwiseEqual)).all());
    EXPECT_TRUE((sim.GetConstraintUpperBound().binaryExpr(controller.GetConstraintUpperBound(), cwiseEqual)).all());
    EXPECT_TRUE(
        (sim.GetTerminalStateLowerBound().binaryExpr(controller.GetTerminalStateLowerBound(), cwiseEqual)).all());
    EXPECT_TRUE(
        (sim.GetTerminalStateUpperBound().binaryExpr(controller.GetTerminalStateUpperBound(), cwiseEqual)).all());

    // Weights
    EXPECT_TRUE((options.input_error_weight.isApprox(controller.GetInputErrorWeightAt(0))));
    EXPECT_TRUE((options.state_error_weight.isApprox(controller.GetStateErrorWeightAt(0))));
    EXPECT_TRUE((options.terminal_state_error_weight.isApprox(controller.GetTerminalStateErrorWeight())));
    EXPECT_TRUE((options.output_error_weight.isApprox(controller.GetOutputErrorWeightAt(0))));

    // Head to Platform Transformation
    EXPECT_TRUE(sim.GetFinalTransform().isApprox(controller.GetFinalTransform()));

    // Bounds tightening
    EXPECT_EQ(options.bounds_tightening, controller.GetBoundsTightening());
    EXPECT_EQ(options.tightening_horizon, controller.GetTighteningHorizon());

    // Levenberg Marquardt
    EXPECT_EQ(options.levenberg_marquardt, controller.GetLevenbergMarquardt());
}

TEST_F(ControllerTest, SettersTest)
{
    // Initialize
    Controller<TestSimulatorType> controller(mpc_horizon, mpc_step, dt, x0, u0);

    // Reference
    TestSimulatorType::StateVector xref = TestSimulatorType::StateVector::Random();
    controller.SetReferenceState(xref);
    for (auto k = 0; k < mpc_horizon; ++k) {
        EXPECT_TRUE(xref.isApprox(controller.GetReferenceState(k)));
    }
    //
    for (auto k = 0; k < mpc_horizon; ++k) {
        TestSimulatorType::StateVector x_ref_k = TestSimulatorType::StateVector::Random();
        TestSimulatorType::OutputVector y_ref_k = TestSimulatorType::OutputVector::Random();
        controller.SetReferenceState(k, x_ref_k);
        controller.SetReferenceOutput(k, y_ref_k);
        EXPECT_TRUE(x_ref_k.isApprox(controller.GetReferenceState(k)));
        EXPECT_TRUE(y_ref_k.isApprox(controller.GetReferenceOutput(k)));
    }

    // Bounds
    TestSimulatorType::StateVector xb = TestSimulatorType::StateVector::Random().cwiseAbs();
    TestSimulatorType::InputVector ub = TestSimulatorType::InputVector::Random().cwiseAbs();
    TestSimulatorType::ConstraintVector gb = TestSimulatorType::ConstraintVector::Random().cwiseAbs();
    TestSimulatorType::StateVector x_b_N = TestSimulatorType::StateVector::Random().cwiseAbs();
    controller.SetStateBounds(-xb, xb);
    controller.SetInputBounds(-ub, ub);
    controller.SetConstraintBounds(-gb, gb);
    controller.SetTerminalStateBounds(-x_b_N, x_b_N);
    //
    EXPECT_TRUE((-xb).isApprox(controller.GetStateLowerBound()));
    EXPECT_TRUE((xb).isApprox(controller.GetStateUpperBound()));
    EXPECT_TRUE((-ub).isApprox(controller.GetInputLowerBound()));
    EXPECT_TRUE((ub).isApprox(controller.GetInputUpperBound()));
    EXPECT_TRUE((-gb).isApprox(controller.GetConstraintLowerBound()));
    EXPECT_TRUE((gb).isApprox(controller.GetConstraintUpperBound()));
    EXPECT_TRUE((-x_b_N).isApprox(controller.GetTerminalStateLowerBound()));
    EXPECT_TRUE((x_b_N).isApprox(controller.GetTerminalStateUpperBound()));
    //
    controller.SetTerminalVelocityLimits(TestSimulatorType::AxesVector::Constant(-0.01),
                                         TestSimulatorType::AxesVector::Constant(0.01));
    MakeStateView<TestSimulatorType>(x_b_N).GetVelocity() = TestSimulatorType::AxesVector::Constant(0.01);
    EXPECT_TRUE((-x_b_N).isApprox(controller.GetTerminalStateLowerBound()));
    EXPECT_TRUE((x_b_N).isApprox(controller.GetTerminalStateUpperBound()));

    // Weights
    TestSimulatorType::StateVector wx = TestSimulatorType::StateVector::Random().cwiseAbs();
    TestSimulatorType::InputVector wu = TestSimulatorType::InputVector::Random().cwiseAbs();
    TestSimulatorType::OutputVector wy = TestSimulatorType::OutputVector::Random().cwiseAbs();
    TestSimulatorType::StateVector w_x_N = TestSimulatorType::StateVector::Random().cwiseAbs();
    controller.SetStateErrorWeight(wx);
    controller.SetInputErrorWeight(wu);
    controller.SetOutputErrorWeight(wy);
    controller.SetTerminalStateErrorWeight(w_x_N);
    //
    EXPECT_TRUE((wx.isApprox(controller.GetStateErrorWeightAt(0))));
    EXPECT_TRUE((wu.isApprox(controller.GetInputErrorWeightAt(0))));
    EXPECT_TRUE((wy.isApprox(controller.GetOutputErrorWeightAt(0))));
    EXPECT_TRUE((w_x_N.isApprox(controller.GetTerminalStateErrorWeight())));

    // Head to Platform Transformation
    TransformationMatrix T = TransformationMatrix::Random();
    controller.SetFinalTransform(T);
    EXPECT_TRUE(T.isApprox(controller.GetFinalTransform()));

    // Bounds tightening
    controller.SetBoundsTightening(0.1);
    controller.SetTighteningHorizon(5);
    EXPECT_EQ(0.1, controller.GetBoundsTightening());
    EXPECT_EQ(5, controller.GetTighteningHorizon());

    // Levenberg Marquardt
    controller.SetLevenbergMarquardt(0.1);
    EXPECT_EQ(0.1, controller.GetLevenbergMarquardt());
}

TEST_F(ControllerTest, SignalsPlan)
{
    // Initialize
    Controller<TestSimulatorType> controller(mpc_horizon, mpc_step, dt, x0, u0);

    // Set working point and output reference
    TestSimulatorType::StateVector xwp = TestSimulatorType::StateVector::Random();
    TestSimulatorType::InputVector uwp = TestSimulatorType::InputVector::Random();
    TestSimulatorType::OutputVector y_ref = TestSimulatorType::OutputVector::Random();
    controller.SetWorkingPoint(xwp, uwp);
    for (std::size_t i = 0; i < controller.GetIntegrationHorizon(); ++i)
        controller.SetReferenceOutput(i, y_ref);

    // Get state, input, output plans
    TestSimulatorType::StateMatrix X = controller.GetStatePlan();
    TestSimulatorType::InputMatrix U = controller.GetInputPlan();
    TestSimulatorType::OutputMatrix Y = controller.GetReferenceOutput();

    // Check
    EXPECT_EQ(X.cols(), controller.GetMpcHorizonLength() + 1);
    EXPECT_EQ(U.cols(), controller.GetMpcHorizonLength());
    EXPECT_EQ(Y.cols(), controller.GetMpcHorizonLength());

    for (std::size_t i = 0; i < controller.GetMpcHorizonLength(); ++i) {
        EXPECT_TRUE(X.col(i).isApprox(xwp));
        EXPECT_TRUE(U.col(i).isApprox(uwp));
        EXPECT_TRUE(Y.col(i).isApprox(y_ref));
    }
    EXPECT_TRUE(X.col(controller.GetMpcHorizonLength()).isApprox(xwp));
}

TEST_F(ControllerTest, GivenMockTransformationMatrix_whenSettingXYZEuler_thenCorrectMatrix)
{
    Controller<TestSimulatorType> controller(mpc_horizon, mpc_step, dt, x0, u0);
    TransformationMatrix T_exp;
    T_exp << 0., -1., 0., 1., 0., 0., 1., 1., -1., 0., 0., 1., 0., 0., 0., 1.;

    controller.SetFinalTransform(1., 1., 1., constants::kPi_2, constants::kPi_2, constants::kPi);

    auto T = controller.GetFinalTransform();
    EXPECT_TRUE(T.isApprox(T_exp));
}

}  //namespace mpmca::control::testing