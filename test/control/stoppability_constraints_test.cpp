/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "generic_test_header.hpp"
#include "mpmca/control/controller_options.hpp"
#include "mpmca/control/from_tmpc/casadi_interface/generated_function.hpp"
#include "mpmca/control/ocp.hpp"
#include "mpmca/control/ocp_data.hpp"
#include "mpmca/control/state.hpp"

namespace mpmca::control::testing {
class StoppabilityConstraintsTest : public ::testing::Test
{
  protected:
    casadi_interface::GeneratedFunction m_stoppability_constraints;
    casadi_interface::GeneratedFunction m_stoppability_constraints_hessian;
    casadi_interface::GeneratedFunction m_stoppability_constraints_original;
    casadi_interface::GeneratedFunction m_stoppability_constraints_hessian_original;
    OcpData<TestSimulatorType> m_ocp_data;
    typename TestSimulatorType::StateVector m_x;
    typename TestSimulatorType::InputVector m_u;
    typename TestSimulatorType::StateVector m_lam_ubg_stop;
    typename TestSimulatorType::StateVector m_lam_lbg_stop;
    typename TestSimulatorType::AxesVector m_q_min, m_q_max;
    OCP<TestSimulatorType, StateStoppabilityOption::kExpressionWithoutDenominator,
        HessianCalculationMethod::kGaussNewton>
        m_ocp;
    OCP<TestSimulatorType, StateStoppabilityOption::kOriginalExpression, HessianCalculationMethod::kGaussNewton>
        m_ocp_original;

    StoppabilityConstraintsTest()
        : m_stoppability_constraints(TestSimulatorType::StoppabilityConstraints_functions())
        , m_stoppability_constraints_hessian(TestSimulatorType::StoppabilityConstraintsHessian_functions())
        , m_stoppability_constraints_original(TestSimulatorType::StoppabilityConstraintsOriginal_functions())
        , m_stoppability_constraints_hessian_original(
              TestSimulatorType::StoppabilityConstraintsHessianOriginal_functions())
        , m_ocp_data{1, 1, 1, IntegrationMethod::kConstantXY, ControllerOptions<TestSimulatorType>()}
        , m_x{TestSimulatorType::StateVector::Random()}
        , m_u{TestSimulatorType::InputVector::Random()}
        , m_lam_ubg_stop{TestSimulatorType::StateVector::Random()}
        , m_lam_lbg_stop{TestSimulatorType::StateVector::Random()}
        , m_ocp{}
        , m_ocp_original{}
    {
        typename TestSimulatorType::StateVector x_dummy = TestSimulatorType::StateVector::Random();
        typename TestSimulatorType::OutputVector y_dummy = TestSimulatorType::OutputVector::Random();
        typename TestSimulatorType::InputVector u_dummy = TestSimulatorType::InputVector::Random();

        m_ocp_data.SetOutputErrorWeight(y_dummy);
        m_ocp_data.SetStateErrorWeight(x_dummy);
        m_ocp_data.SetInputErrorWeight(u_dummy);
        m_ocp_data.SetLowerBoundState(-TestSimulatorType::StateVector::Random().cwiseAbs());
        m_ocp_data.SetUpperBoundState(TestSimulatorType::StateVector::Random().cwiseAbs());
        m_ocp_data.SetLowerBoundInput(-TestSimulatorType::InputVector::Random().cwiseAbs());
        m_ocp_data.SetUpperBoundInput(TestSimulatorType::InputVector::Random().cwiseAbs());
        m_ocp_data.SetLowerBoundConstraints(-TestSimulatorType::ConstraintVector::Random().cwiseAbs());
        m_ocp_data.SetUpperBoundConstraints(TestSimulatorType::ConstraintVector::Random().cwiseAbs());
        m_ocp_data.SetLevenbergMarquardt(0.);
        m_ocp_data.SetFinalTransform(TransformationMatrix::Identity());
        m_ocp_data.SetAlpha(1.);

        m_q_min = MakeStateView<TestSimulatorType>(m_ocp_data.GetStateLowerBound()).GetPosition();
        m_q_max = MakeStateView<TestSimulatorType>(m_ocp_data.GetStateUpperBound()).GetPosition();
    }
};

TEST_F(StoppabilityConstraintsTest, Cg)
{
    typename TestSimulatorType::StateVector cg_stop;
    typename TestSimulatorType::StateStateMatrix dcg_dx_stop;
    typename TestSimulatorType::StateInputMatrix dcg_du_stop;
    OCP<TestSimulatorType, StateStoppabilityOption::kExpressionWithoutDenominator,
        HessianCalculationMethod::kGaussNewton>::ConstraintVector cg;
    OCP<TestSimulatorType, StateStoppabilityOption::kExpressionWithoutDenominator,
        HessianCalculationMethod::kGaussNewton>::ConstraintInputMatrix dcg_du;
    OCP<TestSimulatorType, StateStoppabilityOption::kExpressionWithoutDenominator,
        HessianCalculationMethod::kGaussNewton>::ConstraintStateMatrix dcg_dx;

    // Calculate with Python generated function
    m_stoppability_constraints({m_x.data(), m_q_min.data(), m_q_max.data(), m_ocp_data.GetInputLowerBound().data(),
                                m_ocp_data.GetInputUpperBound().data()},
                               {cg_stop.data(), dcg_dx_stop.data(), dcg_du_stop.data()});

    // Calculate with m_ocp function
    m_ocp.CalculateConstraintJacobian(m_x, m_u, m_ocp_data, cg, dcg_dx, dcg_du);

    // Check
    EXPECT_TRUE(cg_stop.isApprox(cg.bottomRows<TestSimulatorType::Dimension::NX>()));
    EXPECT_TRUE(dcg_dx_stop.isApprox(dcg_dx.bottomRows<TestSimulatorType::Dimension::NX>()));
    EXPECT_TRUE(dcg_du_stop.isApprox(dcg_du.bottomRows<TestSimulatorType::Dimension::NX>()));
}

TEST_F(StoppabilityConstraintsTest, Hessian)
{
    TestSimulatorType::StateStateMatrix d2L_dx2_py, d2L_dx2_ocp;
    TestSimulatorType::InputInputMatrix d2L_du2_py, d2L_du2_ocp;
    TestSimulatorType::StateInputMatrix d2L_dx_du_py, d2L_dx_du_ocp;

    // Calculate with Python generated function
    m_stoppability_constraints_hessian(
        {m_x.data(), m_q_min.data(), m_q_max.data(), m_ocp_data.GetInputLowerBound().data(),
         m_ocp_data.GetInputUpperBound().data(), m_lam_lbg_stop.data(), m_lam_ubg_stop.data()},
        {d2L_dx2_py.data(), d2L_dx_du_py.data(), d2L_du2_py.data()});

    // Calculate like in m_ocp function
    d2L_dx2_ocp = TestSimulatorType::StateStateMatrix::Zero();
    d2L_du2_ocp = TestSimulatorType::InputInputMatrix::Zero();
    d2L_dx_du_ocp = TestSimulatorType::StateInputMatrix::Zero();
    TestSimulatorType::StateVector lam_stop = 2. * (m_lam_ubg_stop - m_lam_lbg_stop);
    for (auto k = 0; k < TestSimulatorType::Dimension::NQ; ++k)
        d2L_dx2_ocp(TestSimulatorType::Dimension::NQ + k, TestSimulatorType::Dimension::NQ + k) +=
            (lam_stop(k) + lam_stop(TestSimulatorType::Dimension::NQ + k));

    // Check
    EXPECT_TRUE(d2L_dx2_ocp.isApprox(d2L_dx2_py));
    EXPECT_TRUE(d2L_du2_ocp.isApprox(d2L_du2_py));
    EXPECT_TRUE(d2L_dx_du_ocp.isApprox(d2L_dx_du_py));
}

TEST_F(StoppabilityConstraintsTest, CgOriginal)
{
    TestSimulatorType::StateVector cg_stop;
    TestSimulatorType::StateStateMatrix dcg_dx_stop;
    TestSimulatorType::StateInputMatrix dcg_du_stop;
    OCP<TestSimulatorType, StateStoppabilityOption::kOriginalExpression,
        HessianCalculationMethod::kGaussNewton>::ConstraintVector cg;
    OCP<TestSimulatorType, StateStoppabilityOption::kOriginalExpression,
        HessianCalculationMethod::kGaussNewton>::ConstraintInputMatrix dcg_du;
    OCP<TestSimulatorType, StateStoppabilityOption::kOriginalExpression,
        HessianCalculationMethod::kGaussNewton>::ConstraintStateMatrix dcg_dx;

    // Calculate with Python generated function
    m_stoppability_constraints_original(
        {m_x.data(), m_q_min.data(), m_q_max.data(), m_ocp_data.GetInputLowerBound().data(),
         m_ocp_data.GetInputUpperBound().data()},
        {cg_stop.data(), dcg_dx_stop.data(), dcg_du_stop.data()});

    // Calculate with m_ocp function
    m_ocp_original.CalculateConstraintJacobian(m_x, m_u, m_ocp_data, cg, dcg_dx, dcg_du);

    // Check
    EXPECT_TRUE(cg_stop.isApprox(cg.bottomRows<TestSimulatorType::Dimension::NX>()));
    EXPECT_TRUE(dcg_dx_stop.isApprox(dcg_dx.bottomRows<TestSimulatorType::Dimension::NX>()));
    EXPECT_TRUE(dcg_du_stop.isApprox(dcg_du.bottomRows<TestSimulatorType::Dimension::NX>()));
}

TEST_F(StoppabilityConstraintsTest, HessianOriginal)
{
    TestSimulatorType::StateStateMatrix d2L_dx2_py, d2L_dx2_ocp;
    TestSimulatorType::InputInputMatrix d2L_du2_py, d2L_du2_ocp;
    TestSimulatorType::StateInputMatrix d2L_dx_du_py, d2L_dx_du_ocp;

    // Calculate with Python generated function
    m_stoppability_constraints_hessian_original(
        {m_x.data(), m_q_min.data(), m_q_max.data(), m_ocp_data.GetInputLowerBound().data(),
         m_ocp_data.GetInputUpperBound().data(), m_lam_lbg_stop.data(), m_lam_ubg_stop.data()},
        {d2L_dx2_py.data(), d2L_dx_du_py.data(), d2L_du2_py.data()});

    // Calculate like in m_ocp function
    d2L_dx2_ocp = TestSimulatorType::StateStateMatrix::Zero();
    d2L_du2_ocp = TestSimulatorType::InputInputMatrix::Zero();
    d2L_dx_du_ocp = TestSimulatorType::StateInputMatrix::Zero();
    TestSimulatorType::StateVector lam_stop = (m_lam_ubg_stop - m_lam_lbg_stop);
    for (auto k = 0; k < TestSimulatorType::Dimension::NQ; ++k) {
        TestSimulatorType::InputVector ulb = m_ocp_data.GetInputLowerBound();
        TestSimulatorType::InputVector uub = m_ocp_data.GetInputUpperBound();
        d2L_dx2_ocp(TestSimulatorType::Dimension::NQ + k, TestSimulatorType::Dimension::NQ + k) +=
            (-lam_stop(k) / ulb(k) + lam_stop(TestSimulatorType::Dimension::NQ + k) / uub(k));
    }

    // Check
    EXPECT_TRUE(d2L_dx2_ocp.isApprox(d2L_dx2_py));
    EXPECT_TRUE(d2L_du2_ocp.isApprox(d2L_du2_py));
    EXPECT_TRUE(d2L_dx_du_ocp.isApprox(d2L_dx_du_py));
}
}  //namespace mpmca::control::testing