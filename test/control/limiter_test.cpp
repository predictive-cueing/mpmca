/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/control/limiter.hpp"

#include <algorithm>
#include <fstream>
#include <tmpc/qp/QpSolverException.hpp>

#include "generic_test_header.hpp"
#include "mpmca/control/controller.hpp"
#include "mpmca/control/ocp.hpp"
#include "mpmca/control/simulator.hpp"
#include "mpmca/control/state.hpp"
#include "mpmca/linear_algebra.hpp"
#include "mpmca/utilities/mpmca_gtest.hpp"

namespace mpmca::control::testing {
template <typename T>
class LimiterTest : public ::testing::Test
{
  protected:
    TestSimulatorType::StateVector x_max, x_min;
    TestSimulatorType::InputVector u_max, u_min;
    std::array<bool, TestSimulatorType::Dimension::NQ> m_axes_to_limit;

    LimiterTest()
        : x_max{TestSimulatorType::StateVector::Constant(1.5)}
        , x_min{-TestSimulatorType::StateVector::Constant(1.5)}
        , u_max{TestSimulatorType::InputVector::Constant(1.)}
        , u_min{-TestSimulatorType::InputVector::Constant(1.)}
    {
        m_axes_to_limit.fill(true);
    }

    bool IsWithinBounds(TestSimulatorType::StateVector const& x)
    {
        return ((x_max - x).array() >= 0.0).all() && ((x - x_min).array() >= 0.0).all();
    }
    bool IsStoppable(TestSimulatorType::StateVector const& x)
    {
        auto p = MakeStateView<TestSimulatorType>(x).GetPosition();
        auto v = MakeStateView<TestSimulatorType>(x).GetVelocity();
        auto p_max = MakeStateView<TestSimulatorType>(x_max).GetPosition();
        auto p_min = MakeStateView<TestSimulatorType>(x_min).GetPosition();

        bool is_stoppable = true;
        for (auto k = 0; k < p.size(); ++k) {
            if (v[k] < 0) {
                if ((p[k] - v[k] * v[k] / (2. * u_max[k]) - p_min[k]) < 0) {
                    is_stoppable = false;
                    break;
                }
            }
            else if (v[k] > 0) {
                if ((p[k] - v[k] * v[k] / (2. * u_min[k]) - p_max[k]) > 0) {
                    is_stoppable = false;
                    break;
                }
            }
        }

        return is_stoppable;
    }

    TestSimulatorType::StateVector GenerateRandomNotFeasibleState()
    {
        auto p_max = MakeStateView<TestSimulatorType>(x_max).GetPosition();
        auto p_min = -p_max;
        auto uMin = -u_max;
        (void)uMin;

        TestSimulatorType::AxesVector v = TestSimulatorType::AxesVector::Random();
        TestSimulatorType::AxesVector pNotFeasible =
            v.cwiseAbs2().cwiseQuotient(2. * u_max) + p_min - TestSimulatorType::AxesVector::Constant(1.);

        TestSimulatorType::StateVector xNotFeasible;
        xNotFeasible << pNotFeasible, v;

        return xNotFeasible;
    }
};

template <StateSubsetToModify s>
struct StateSubsetType
{
    static constexpr StateSubsetToModify value = s;
};

using TestTypes =
    ::testing::Types<StateSubsetType<StateSubsetToModify::kNone>, StateSubsetType<StateSubsetToModify::kPosition>,
                     StateSubsetType<StateSubsetToModify::kVelocity> >;

TYPED_TEST_CASE(LimiterTest, TestTypes);

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsuggest-override"
TYPED_TEST(LimiterTest, GivenNotFeasibleState_onlyWhileLimitingState_thenStateIsFeasible)
{
    static constexpr StateSubsetToModify s = TypeParam::value;

    TestSimulatorType::StateVector x = this->GenerateRandomNotFeasibleState();

    Limiter<TestSimulatorType, s> limiter{this->m_axes_to_limit};
    limiter.MakeFeasible(x, this->x_min, this->x_max, this->u_min, this->u_max);

    if (s == StateSubsetToModify::kNone) {
        ASSERT_FALSE(this->IsWithinBounds(x));
        ASSERT_FALSE(this->IsStoppable(x));
    }
    else {
        ASSERT_TRUE(this->IsWithinBounds(x));
        ASSERT_TRUE(this->IsStoppable(x));
    }
}
#pragma GCC diagnostic pop

}  //namespace mpmca::control::testing
