/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <gtest/gtest.h>

#include <fstream>
#include <tmpc/qp/QpSolverException.hpp>

#include "generic_test_header.hpp"
#include "mpmca/control/controller.hpp"
#include "mpmca/control/simulator.hpp"

namespace mpmca::control::testing {
class MpcTest : public ::testing::Test
{
  protected:
    Simulator<MpmcaHexapod> simulator;
    double dt;
    std::vector<unsigned> mpc_steps;
    MpmcaHexapod::StateVector const x0;
    MpmcaHexapod::InputVector const u0;
    MpmcaHexapod::OutputVector y_ref;
    MpmcaHexapod::OutputVector wy;

    MpcTest()
        : dt{0.01}
        , x0{simulator.GetNominalState()}
        , u0{MpmcaHexapod::InputVector::Zero()}
    {
        // Init reference output
        SetReferenceOutput();

        // Initialize mpc_steps
        unsigned N = 100;
        for (int i = 0; i < N; ++i) {
            if (i < 25)
                mpc_steps.push_back(1);

            if (i >= 25 && i < 50)
                mpc_steps.push_back(1 + 9 * (i - 25) / 25);

            if (i >= 50)
                mpc_steps.push_back(10);
        }

        // Init output error weight
        SetOutputErrorWeight();
    }

    void SetReferenceOutput()
    {
        y_ref.topRows<3>() = simulator.GetGravity() + (Vector<3>() << 10., 10., 0.).finished();
        y_ref.bottomRows<6>() = (Vector<6>() << 1., 1., 1., 0., 0., 0.).finished();
    }

    void SetOutputErrorWeight() { wy << 1., 1., 1., 10., 10., 10., 0., 0., 0.; }

    Controller<TestSimulatorType> MakeController()
    {
        Controller<TestSimulatorType> controller{mpc_steps, dt, x0, u0};
        controller.SetLevenbergMarquardt(1.e-6);
        controller.SetTighteningHorizon(8);
        controller.SetBoundsTightening(0.10);
        controller.SetOutputErrorWeight(wy);
        for (std::size_t i = 0; i < controller.GetIntegrationHorizon(); ++i)
            controller.SetReferenceOutput(i, y_ref);

        return controller;
    }

    void Simulate(Controller<TestSimulatorType>& controller, MpmcaHexapod::InputVector& u, MpmcaHexapod::StateVector& x,
                  MpmcaHexapod::OutputVector& y)
    {
        unsigned const nt = 5;
        x = x0;
        try {
            for (int i = 0; i < nt; ++i) {
                controller.Preparation();
                u = controller.Feedback(x);
                x = simulator.Integrate(x, u, dt);
            }
            y = simulator.GetOutput(x, u, controller.GetFinalTransform());
        }
        catch (tmpc::QpSolverException const& ex) {
            std::cerr << "QpSolverException: " << ex.what() << std::endl;
            std::cerr << "Dumping failed QP to failed_qp.m" << std::endl;

            std::ofstream failed_qp("failed_qp.m");
            controller.PrintQp(failed_qp);

            throw;
        }
    }

    bool AreSimulatedSignalsCorrect(MpmcaHexapod::InputVector u, MpmcaHexapod::StateVector x,
                                    MpmcaHexapod::OutputVector y)
    {
        MpmcaHexapod::InputVector u_check;
        MpmcaHexapod::StateVector x_check;
        MpmcaHexapod::OutputVector yChk;

        u_check << 0.204919, -1.16112, 1.57389, 8.14247, -2.45247, -6.81043;
        x_check << 0.00113077, -0.00077579, 0.00375355, 0.0148249, -0.00929576, -0.00170326, 0.0324796, -0.0419128,
            0.128301, 0.576987, -0.291317, -0.105188;
        yChk << -0.130328, 1.28276, 8.21957, 0.576009, -0.292844, -0.100854, 8.04852, -2.61135, -6.60378;

        return u.isApprox(u_check, 0.00001) && x.isApprox(x_check, 0.00001) && y.isApprox(yChk, 0.00001);
    }
};

TEST_F(MpcTest, ControlInputCorrectlyCalculated)
{
    Controller<TestSimulatorType> controller = MakeController();

    MpmcaHexapod::InputVector u;
    MpmcaHexapod::StateVector x;
    MpmcaHexapod::OutputVector y;
    Simulate(controller, u, x, y);

    try {
        EXPECT_TRUE(AreSimulatedSignalsCorrect(u, x, y));
    }
    catch (std::invalid_argument& e) {
        EXPECT_TRUE(false) << e.what();
    }
}

TEST_F(MpcTest, ParallelPreparationGivesSameControlInput)
{
    // Init controllers
    Controller c1{MakeController()};
    Controller c2{MakeController()};

    // Simulate and check
    unsigned const nt = 5;
    MpmcaHexapod::InputVector u1, u2;
    MpmcaHexapod::StateVector x{x0};
    for (int i = 0; i < nt; ++i) {
        c1.Preparation();
        c2.Preparation();
        u1 = c1.Feedback(x);
        u2 = c2.Feedback(x);

        EXPECT_TRUE(u1.isApprox(u2));

        x = simulator.Integrate(x, u1, dt);
    }
}

TEST_F(MpcTest, GivenControllerAfterEvolution_whenResettingWorkingPoint_thenSameEvolution)
{
    // Init controllers
    Controller c1{MakeController()};
    Controller c2{MakeController()};

    // Simulate and reset c1
    MpmcaHexapod::InputVector u;
    MpmcaHexapod::StateVector x;
    MpmcaHexapod::OutputVector y;
    Simulate(c1, u, x, y);
    c1.SetWorkingPoint(x0, u0);

    // Simulate and check
    unsigned const nt = 5;
    MpmcaHexapod::InputVector u1, u2;
    x = x0;
    for (int i = 0; i < nt; ++i) {
        c1.Preparation();
        c2.Preparation();
        u1 = c1.Feedback(x);
        u2 = c2.Feedback(x);

        EXPECT_TRUE(u1.isApprox(u2));

        x = simulator.Integrate(x, u1, dt);
    }
}
}  //namespace mpmca::control::testing
