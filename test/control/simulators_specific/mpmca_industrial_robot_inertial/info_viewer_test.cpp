/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <cmath>
#include <iostream>

#include "generic_test_header.hpp"
#include "mpmca/constants.hpp"
#include "mpmca/control/info.hpp"
#include "mpmca/control/simulator.hpp"
#include "mpmca/control/templates/component.hpp"
#include "mpmca/linear_algebra.hpp"

namespace mpmca::control::testing {
class InfoTest : public ::testing::Test
{
  protected:
    Simulator<TestSimulatorType> sim;
    InfoTest()
        : sim()
    {
    }
};

TEST_F(InfoTest, InfoViewers)
{
    TestSimulatorType::InfoVector info = sim.GetInfo();
    auto info_viewer = MakeInfoView<TestSimulatorType>(info);
    Vector<4> active_dofs;
    active_dofs << 1, 0, 0, 0;

    EXPECT_EQ(info_viewer.ViewAt<0>().GetThetaOffset(), 0.);
    EXPECT_EQ(info_viewer.ViewAt<0>().GetD(), -.75);
    EXPECT_EQ(info_viewer.ViewAt<0>().GetAlpha(), constants::kPi_2);
    EXPECT_EQ(info_viewer.ViewAt<0>().GetA(), .45);
    EXPECT_TRUE(active_dofs.isApprox(info_viewer.ViewAt<0>().GetActiveDoF(), 0.00001));

    EXPECT_EQ(info_viewer.ViewAt<1>().GetThetaOffset(), 0.);
    EXPECT_EQ(info_viewer.ViewAt<1>().GetD(), 0.);
    EXPECT_EQ(info_viewer.ViewAt<1>().GetAlpha(), 0.);
    EXPECT_EQ(info_viewer.ViewAt<1>().GetA(), 1.3);
    EXPECT_TRUE(active_dofs.isApprox(info_viewer.ViewAt<1>().GetActiveDoF(), 0.00001));

    EXPECT_EQ(info_viewer.ViewAt<2>().GetThetaOffset(), -90. * constants::kPi / 180);
    EXPECT_EQ(info_viewer.ViewAt<2>().GetD(), 0.);
    EXPECT_EQ(info_viewer.ViewAt<2>().GetAlpha(), -90. * constants::kPi / 180.);
    EXPECT_EQ(info_viewer.ViewAt<2>().GetA(), -0.05);
    EXPECT_TRUE(active_dofs.isApprox(info_viewer.ViewAt<2>().GetActiveDoF(), 0.00001));

    EXPECT_EQ(info_viewer.ViewAt<3>().GetThetaOffset(), 0.);
    EXPECT_EQ(info_viewer.ViewAt<3>().GetD(), 1.);
    EXPECT_EQ(info_viewer.ViewAt<3>().GetAlpha(), 90. * constants::kPi / 180.);
    EXPECT_EQ(info_viewer.ViewAt<3>().GetA(), 0.);
    EXPECT_TRUE(active_dofs.isApprox(info_viewer.ViewAt<3>().GetActiveDoF(), 0.00001));

    EXPECT_EQ(info_viewer.ViewAt<4>().GetThetaOffset(), 0.);
    EXPECT_EQ(info_viewer.ViewAt<4>().GetD(), 0.);
    EXPECT_EQ(info_viewer.ViewAt<4>().GetAlpha(), -90. * constants::kPi / 180.);
    EXPECT_EQ(info_viewer.ViewAt<4>().GetA(), 0.);
    EXPECT_TRUE(active_dofs.isApprox(info_viewer.ViewAt<4>().GetActiveDoF(), 0.00001));

    EXPECT_EQ(info_viewer.ViewAt<5>().GetThetaOffset(), 0.);
    EXPECT_EQ(info_viewer.ViewAt<5>().GetD(), 0.3);
    EXPECT_EQ(info_viewer.ViewAt<5>().GetAlpha(), 0.);
    EXPECT_EQ(info_viewer.ViewAt<5>().GetA(), 0.);
    EXPECT_TRUE(active_dofs.isApprox(info_viewer.ViewAt<5>().GetActiveDoF(), 0.00001));

    Vector<6> active_dofs_transformation_matrix = Vector<6>::Zero();
    Vector<3> xyz = Vector<3>::Zero();
    Vector<3> roll_pitch_yaw;
    roll_pitch_yaw << 0., -constants::kPi_2, 0.;

    EXPECT_TRUE(xyz.isApprox(info_viewer.ViewAt<6>().GetXyz(), 0.00001));
    EXPECT_TRUE(roll_pitch_yaw.isApprox(info_viewer.ViewAt<6>().GetRollPitchYaw(), 0.00001));
    EXPECT_TRUE(active_dofs_transformation_matrix.isApprox(info_viewer.ViewAt<6>().GetActiveDoF(), 0.00001));
}
}  //namespace mpmca::control::testing