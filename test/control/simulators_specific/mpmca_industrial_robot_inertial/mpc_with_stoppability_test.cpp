/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <gtest/gtest.h>

#include <fstream>
#include <tmpc/qp/QpSolverException.hpp>

#include "generic_test_header.hpp"
#include "mpmca/control/controller.hpp"
#include "mpmca/control/controller_options.hpp"
#include "mpmca/control/ocp.hpp"
#include "mpmca/control/simulator.hpp"

namespace mpmca::control::testing {
class MpcWithStoppabilityTest : public ::testing::Test
{
  protected:
    Simulator<TestSimulatorType> sim;
    double dt;
    std::vector<unsigned> mpc_steps;
    TestSimulatorType::StateVector x0;
    TestSimulatorType::InputVector u0;
    TestSimulatorType::OutputVector y_ref;

    TestSimulatorType::InputVector u;
    TestSimulatorType::OutputVector y;

    TestSimulatorType::OutputVector wy;

    Simulator<TestSimulatorType> simulator;

    MpcWithStoppabilityTest()
        : dt{0.01}
        , u0{TestSimulatorType::InputVector::Zero()}
    {
        // Init initial state
        x0 = sim.GetNominalState();

        // Init yRef
        SetReferenceOutput();

        // Initialize mpc_steps
        unsigned N = 100;
        for (int i = 0; i < N; ++i) {
            if (i < 25)
                mpc_steps.push_back(1);

            if (i >= 25 && i < 50)
                mpc_steps.push_back(1 + 9 * (i - 25) / 25);

            if (i >= 50)
                mpc_steps.push_back(10);
        }

        // Init error weight
        SetOutputErrorWeight();
    }

    void SetReferenceOutput()
    {
        y_ref.topRows<3>() = simulator.GetGravity() + (Vector<3>() << 10., 10., 0.).finished();
        y_ref.bottomRows<6>() = (Vector<6>() << 1., 1., 1., 0., 0., 0.).finished();
    }

    void SetOutputErrorWeight() { wy << 1., 1., 1., 10., 10., 10., 0., 0., 0.; }

    template <typename T>
    void Simulate(T& controller)
    {
        unsigned const nt = 5;

        try {
            for (int i = 0; i < nt; ++i) {
                controller.Preparation();
                u = controller.Feedback(x0);
                x0 = simulator.Integrate(x0, u, dt);
            }
            y = simulator.GetOutput(x0, u, controller.GetFinalTransform());
        }
        catch (tmpc::QpSolverException const& ex) {
            std::cerr << "QpSolverException: " << ex.what() << std::endl;
            std::cerr << "Dumping failed QP to failed_qp.m" << std::endl;

            std::ofstream failed_qp("failed_qp.m");
            controller.PrintQp(failed_qp);

            throw;
        }
    }

    bool AreSimulatedSignalsCorrect(const TestSimulatorType::InputVector& u, const TestSimulatorType::StateVector& x,
                                    const TestSimulatorType::OutputVector& y)
    {
        TestSimulatorType::InputVector u_check;
        TestSimulatorType::StateVector x_check;
        TestSimulatorType::OutputVector y_check;

        u_check << 1.91986, -1.02501, 1.91986, 1.39626, -1.22172, 2.09439;
        x_check << 0.00215732, -1.57029, 1.5732, 0.00174533, 0.000183238, 0.00261799, 0.0890643, -0.00678476, 0.0959929,
            0.0698131, -0.0122181, 0.10472;
        y_check << 1.431, -3.32003, 9.02459, 0.174808, -0.0766007, 0.0894075, 3.50344, 0.350869, 1.93534;

        return u.isApprox(u_check, 0.00001) && x.isApprox(x_check, 0.00001) && y.isApprox(y_check, 0.00001);
    }
};

TEST_F(MpcWithStoppabilityTest, GivenControllerWithStoppabilityOptions_whenSimulating_thenResultsAreCorrect)
{
    Controller<TestSimulatorType, StateStoppabilityOption::kOriginalExpression> controller{
        mpc_steps, dt, x0, u0, ControllerOptions<TestSimulatorType>()};
    controller.SetLevenbergMarquardt(1.e-6);
    controller.SetTighteningHorizon(8);
    controller.SetBoundsTightening(0.10);
    controller.SetOutputErrorWeight(wy);
    for (std::size_t i = 0; i < controller.GetIntegrationHorizon(); ++i)
        controller.SetReferenceOutput(i, y_ref);

    Simulate(controller);

    try {
        EXPECT_TRUE(AreSimulatedSignalsCorrect(u, x0, y));
    }
    catch (std::invalid_argument& e) {
        EXPECT_TRUE(false) << e.what();
    }
}

}  //namespace mpmca::control::testing