/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <gtest/gtest.h>

#include <fstream>
#include <tmpc/qp/QpSolverException.hpp>

#include "generic_test_header.hpp"
#include "mpmca/control/controller.hpp"
#include "mpmca/control/simulator.hpp"

namespace mpmca::control::testing {
class MpcTest : public ::testing::Test
{
  protected:
    Simulator<TestSimulatorType> simulator;
    double dt;
    std::vector<unsigned> mpc_steps;
    TestSimulatorType::StateVector const x0;
    TestSimulatorType::InputVector const u0;
    TestSimulatorType::OutputVector y_ref;
    TestSimulatorType::OutputVector wy;

    MpcTest()
        : dt{0.01}
        , x0{simulator.GetNominalState()}
        , u0{TestSimulatorType::InputVector::Zero()}
    {
        // Init reference output
        SetReferenceOutput();

        // Initialize mpc_steps
        unsigned N = 100;
        for (int i = 0; i < N; ++i) {
            if (i < 25)
                mpc_steps.push_back(1);

            if (i >= 25 && i < 50)
                mpc_steps.push_back(1 + 9 * (i - 25) / 25);

            if (i >= 50)
                mpc_steps.push_back(10);
        }

        // Init output error weight
        SetOutputErrorWeight();
    }

    void SetReferenceOutput()
    {
        y_ref.topRows<3>() = simulator.GetGravity() + (Vector<3>() << 10., 10., 0.).finished();
        y_ref.bottomRows<6>() = (Vector<6>() << 1., 1., 1., 0., 0., 0.).finished();
    }

    void SetOutputErrorWeight() { wy << 1., 1., 1., 10., 10., 10., 0., 0., 0.; }

    Controller<TestSimulatorType> MakeController()
    {
        Controller<TestSimulatorType> controller{mpc_steps, dt, x0, u0};
        controller.SetLevenbergMarquardt(1.e-6);
        controller.SetTighteningHorizon(8);
        controller.SetBoundsTightening(0.10);
        controller.SetOutputErrorWeight(wy);
        for (std::size_t i = 0; i < controller.GetIntegrationHorizon(); ++i)
            controller.SetReferenceOutput(i, y_ref);

        return controller;
    }

    void Simulate(Controller<TestSimulatorType>& controller, TestSimulatorType::InputVector& u,
                  TestSimulatorType::StateVector& x, TestSimulatorType::OutputVector& y)
    {
        unsigned const nt = 5;
        x = x0;
        try {
            for (int i = 0; i < nt; ++i) {
                controller.Preparation();
                u = controller.Feedback(x);
                x = simulator.Integrate(x, u, dt);
            }
            y = simulator.GetOutput(x, u, controller.GetFinalTransform());
        }
        catch (tmpc::QpSolverException const& ex) {
            std::cerr << "QpSolverException: " << ex.what() << std::endl;
            std::cerr << "Dumping failed QP to failed_qp.m" << std::endl;

            std::ofstream failed_qp("failed_qp.m");
            controller.PrintQp(failed_qp);

            throw;
        }
    }

    bool AreSimulatedSignalsCorrect(TestSimulatorType::InputVector u, TestSimulatorType::StateVector x,
                                    TestSimulatorType::OutputVector y)
    {
        TestSimulatorType::InputVector u_check;
        TestSimulatorType::StateVector x_check;
        TestSimulatorType::OutputVector y_check;

        u_check << 1.91986, -1.00601, 1.91986, 1.39626, -1.22173, 2.0944;
        x_check << 0.00220308, -1.57031, 1.5732, 0.00174533, 0.000183256, 0.00261799, 0.0903717, -0.00717718, 0.095993,
            0.0698131, -0.0122174, 0.10472;
        y_check << 1.40744, -3.31998, 8.99994, 0.174811, -0.0762034, 0.0907132, 3.50348, 0.332101, 1.93535;

        return u.isApprox(u_check, 0.00001) && x.isApprox(x_check, 0.00001) && y.isApprox(y_check, 0.00001);
    }
};

TEST_F(MpcTest, ControlInputCorrectlyCalculated)
{
    Controller<TestSimulatorType> controller{MakeController()};

    TestSimulatorType::InputVector u;
    TestSimulatorType::StateVector x;
    TestSimulatorType::OutputVector y;
    Simulate(controller, u, x, y);

    try {
        EXPECT_TRUE(AreSimulatedSignalsCorrect(u, x, y));
    }
    catch (std::invalid_argument& e) {
        EXPECT_TRUE(false) << e.what();
    }
}

TEST_F(MpcTest, ParallelPreparationGivesSameControlInput)
{
    // Init controllers
    Controller c1{MakeController()};
    Controller c2{MakeController()};

    // Simulate and check
    unsigned const nt = 5;
    TestSimulatorType::InputVector u1, u2;
    TestSimulatorType::StateVector x{x0};
    for (int i = 0; i < nt; ++i) {
        c1.Preparation();
        c2.Preparation();
        u1 = c1.Feedback(x);
        u2 = c2.Feedback(x);

        EXPECT_TRUE(u1.isApprox(u2));

        x = simulator.Integrate(x, u1, dt);
    }
}

TEST_F(MpcTest, GivenControllerAfterEvolution_whenResettingWorkingPoint_thenSameEvolution)
{
    // Init controllers
    Controller c1{MakeController()};
    Controller c2{MakeController()};

    // Simulate and reset c1
    TestSimulatorType::InputVector u;
    TestSimulatorType::StateVector x;
    TestSimulatorType::OutputVector y;
    Simulate(c1, u, x, y);
    c1.SetWorkingPoint(x0, u0);

    // Simulate and check
    unsigned const nt = 5;
    TestSimulatorType::InputVector u1, u2;
    x = x0;
    for (int i = 0; i < nt; ++i) {
        c1.Preparation();
        c2.Preparation();
        u1 = c1.Feedback(x);
        u2 = c2.Feedback(x);

        EXPECT_TRUE(u1.isApprox(u2));

        x = simulator.Integrate(x, u1, dt);
    }
}
}  //namespace mpmca::control::testing
