/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <gtest/gtest.h>

#include <fstream>
#include <tmpc/qp/QpSolverException.hpp>

#include "generic_test_header.hpp"
#include "mpmca/control/controller.hpp"
#include "mpmca/control/controller_options.hpp"
#include "mpmca/control/ocp.hpp"
#include "mpmca/control/simulator.hpp"

namespace mpmca::control::testing {
class MpcWithStoppabilityTest : public ::testing::Test
{
  protected:
    Simulator<TestSimulatorType> sim;
    double dt;
    std::vector<unsigned> mpc_steps;
    TestSimulatorType::StateVector x0;
    TestSimulatorType::InputVector u0;
    TestSimulatorType::OutputVector y_ref;

    TestSimulatorType::InputVector u;
    TestSimulatorType::OutputVector y;

    TestSimulatorType::OutputVector wy;

    Simulator<TestSimulatorType> simulator;

    MpcWithStoppabilityTest()
        : dt{0.01}
        , u0{TestSimulatorType::InputVector::Zero()}
        , u{TestSimulatorType::InputVector::Zero()}
    {
        // Init initial state
        x0 = sim.GetNominalState();

        // Init yRef
        SetReferenceOutput();

        // Initialize mpc_steps
        unsigned N = 100;
        for (int i = 0; i < N; ++i) {
            if (i < 25)
                mpc_steps.push_back(1);

            if (i >= 25 && i < 50)
                mpc_steps.push_back(1 + 9 * (i - 25) / 25);

            if (i >= 50)
                mpc_steps.push_back(10);
        }

        // Init error weight
        SetOutputErrorWeight();
    }

    void SetReferenceOutput() { y_ref = sim.GetNominalOutput(); }

    void SetOutputErrorWeight() { wy = ControllerOptions<TestSimulatorType>().output_error_weight; }

    template <typename T>
    void Simulate(T& controller)
    {
        unsigned const nt = 5;

        try {
            for (int i = 0; i < nt; ++i) {
                controller.Preparation();
                u = controller.Feedback(x0);
                x0 = simulator.Integrate(x0, u, dt);
            }
            y = simulator.GetOutput(x0, u, controller.GetFinalTransform());
        }
        catch (tmpc::QpSolverException const& ex) {
            std::cerr << "QpSolverException: " << ex.what() << std::endl;
            std::cerr << "Dumping failed QP to failed_qp.m" << std::endl;

            std::ofstream failed_qp("failed_qp.m");
            controller.PrintQp(failed_qp);

            throw;
        }
    }

    template <int N>
    mpmca::Vector<N> round(const mpmca::Vector<N>& inout, double threshold)
    {
        return (threshold < inout.array().abs()).select(inout, 0.0f);
    }

    bool AreSimulatedSignalsCorrect(const TestSimulatorType::InputVector& u, const TestSimulatorType::StateVector& x,
                                    const TestSimulatorType::OutputVector& y, StateStoppabilityOption opt)
    {
        TestSimulatorType::InputVector u_check;
        TestSimulatorType::StateVector x_check;
        TestSimulatorType::OutputVector y_check;

        u_check << 0, 0, 0, 0, 0, 0;
        x_check << 0, -1.5708, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;
        y_check << 1.75, 0, -2, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0.000, 0, 0, 0, 0, 0, 0.000, 0, 0, 0, 0, 0;

        double tol = 0.00001;

        return (u - u_check).cwiseAbs2().sum() < tol && x.isApprox(x_check, tol) && y.isApprox(y_check, tol);
    }
};

TEST_F(MpcWithStoppabilityTest, GivenControllerWithStoppabilityOptions_whenSimulating_thenResultsAreCorrectNone)
{
    Controller<TestSimulatorType, StateStoppabilityOption::kNone> controller{mpc_steps, dt, x0, u0,
                                                                             ControllerOptions<TestSimulatorType>()};
    controller.SetLevenbergMarquardt(1.e-6);
    controller.SetTighteningHorizon(8);
    controller.SetBoundsTightening(0.10);
    controller.SetOutputErrorWeight(wy);
    for (std::size_t i = 0; i < controller.GetIntegrationHorizon(); ++i)
        controller.SetReferenceOutput(i, y_ref);

    Simulate(controller);

    try {
        EXPECT_TRUE(AreSimulatedSignalsCorrect(u, x0, y, StateStoppabilityOption::kNone));
    }
    catch (std::invalid_argument& e) {
        EXPECT_TRUE(false) << e.what();
    }
}

TEST_F(MpcWithStoppabilityTest, GivenControllerWithStoppabilityOptions_whenSimulating_thenResultsAreCorrectWoDenom)
{
    Controller<TestSimulatorType, StateStoppabilityOption::kExpressionWithoutDenominator> controller{
        mpc_steps, dt, x0, u0, ControllerOptions<TestSimulatorType>()};
    controller.SetLevenbergMarquardt(1.e-6);
    controller.SetTighteningHorizon(8);
    controller.SetBoundsTightening(0.10);
    controller.SetOutputErrorWeight(wy);
    for (std::size_t i = 0; i < controller.GetIntegrationHorizon(); ++i)
        controller.SetReferenceOutput(i, y_ref);

    Simulate(controller);

    try {
        EXPECT_TRUE(AreSimulatedSignalsCorrect(u, x0, y, StateStoppabilityOption::kExpressionWithoutDenominator));
    }
    catch (std::invalid_argument& e) {
        EXPECT_TRUE(false) << e.what();
    }
}

TEST_F(MpcWithStoppabilityTest, GivenControllerWithStoppabilityOptions_whenSimulating_thenResultsAreCorrectOriginal)
{
    Controller<TestSimulatorType, StateStoppabilityOption::kOriginalExpression> controller{
        mpc_steps, dt, x0, u0, ControllerOptions<TestSimulatorType>()};
    controller.SetLevenbergMarquardt(1.e-6);
    controller.SetTighteningHorizon(8);
    controller.SetBoundsTightening(0.10);
    controller.SetOutputErrorWeight(wy);
    for (std::size_t i = 0; i < controller.GetIntegrationHorizon(); ++i)
        controller.SetReferenceOutput(i, y_ref);

    Simulate(controller);

    try {
        EXPECT_TRUE(AreSimulatedSignalsCorrect(u, x0, y, StateStoppabilityOption::kOriginalExpression));
    }
    catch (std::invalid_argument& e) {
        EXPECT_TRUE(false) << e.what();
    }
}
}  //namespace mpmca::control::testing