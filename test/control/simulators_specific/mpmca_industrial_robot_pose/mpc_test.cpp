/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <gtest/gtest.h>

#include <fstream>
#include <tmpc/qp/QpSolverException.hpp>

#include "generic_test_header.hpp"
#include "mpmca/control/controller.hpp"
#include "mpmca/control/simulator.hpp"

namespace mpmca::control::testing {
class MpcTest : public ::testing::Test
{
  protected:
    Simulator<TestSimulatorType> simulator;
    double dt;
    std::vector<unsigned> mpc_steps;
    TestSimulatorType::StateVector const x0;
    TestSimulatorType::InputVector const u0;
    TestSimulatorType::OutputVector y_ref;
    TestSimulatorType::OutputVector wy;

    MpcTest()
        : dt{0.01}
        , x0{simulator.GetNominalState()}
        , u0{TestSimulatorType::InputVector::Zero()}
    {
        // Init reference output
        SetReferenceOutput();

        // Initialize mpc_steps
        unsigned N = 100;
        for (int i = 0; i < N; ++i) {
            if (i < 25)
                mpc_steps.push_back(1);

            if (i >= 25 && i < 50)
                mpc_steps.push_back(1 + 9 * (i - 25) / 25);

            if (i >= 50)
                mpc_steps.push_back(10);
        }

        // Init output error weight
        SetOutputErrorWeight();
    }

    void SetReferenceOutput() { y_ref = simulator.GetNominalOutput(); }

    void SetOutputErrorWeight() { wy = ControllerOptions<TestSimulatorType>().output_error_weight; }

    Controller<TestSimulatorType> MakeController()
    {
        Controller<TestSimulatorType> controller{mpc_steps, dt, x0, u0};
        controller.SetLevenbergMarquardt(1.e-6);
        controller.SetTighteningHorizon(8);
        controller.SetBoundsTightening(0.10);
        controller.SetOutputErrorWeight(wy);
        for (std::size_t i = 0; i < controller.GetIntegrationHorizon(); ++i)
            controller.SetReferenceOutput(i, y_ref);

        return controller;
    }

    void Simulate(Controller<TestSimulatorType>& controller, TestSimulatorType::InputVector& u,
                  TestSimulatorType::StateVector& x, TestSimulatorType::OutputVector& y)
    {
        unsigned const nt = 5;
        x = x0;
        try {
            for (int i = 0; i < nt; ++i) {
                controller.Preparation();
                u = controller.Feedback(x);
                x = simulator.Integrate(x, u, dt);
            }
            y = simulator.GetOutput(x, u, controller.GetFinalTransform());
        }
        catch (tmpc::QpSolverException const& ex) {
            std::cerr << "QpSolverException: " << ex.what() << std::endl;
            std::cerr << "Dumping failed QP to failed_qp.m" << std::endl;

            std::ofstream failed_qp("failed_qp.m");
            controller.PrintQp(failed_qp);

            throw;
        }
    }

    bool AreSimulatedSignalsCorrect(TestSimulatorType::InputVector u, TestSimulatorType::StateVector x,
                                    TestSimulatorType::OutputVector y)
    {
        TestSimulatorType::InputVector uChk;
        TestSimulatorType::StateVector xChk;
        TestSimulatorType::OutputVector yChk;
        std::cout << u.transpose() << std::endl;
        std::cout << x.transpose() << std::endl;
        std::cout << y.transpose() << std::endl;

        uChk << -5.62063e-28, 9.4054e-12, -2.23669e-12, 2.83528e-30, -6.97702e-12, 9.51283e-31;
        xChk << 0, -1.5708, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;
        yChk << 1.75, 0, -2, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;

        return u.isApprox(uChk, 0.01) && x.isApprox(xChk, 0.0001) && y.isApprox(yChk, 0.0001);
    }
};

TEST_F(MpcTest, ControlInputCorrectlyCalculated)
{
    Controller<TestSimulatorType> controller{MakeController()};

    TestSimulatorType::InputVector u;
    TestSimulatorType::StateVector x;
    TestSimulatorType::OutputVector y;
    Simulate(controller, u, x, y);

    try {
        EXPECT_TRUE(AreSimulatedSignalsCorrect(u, x, y));
    }
    catch (std::invalid_argument& e) {
        EXPECT_TRUE(false) << e.what();
    }
}

TEST_F(MpcTest, ParallelPreparationGivesSameControlInput)
{
    // Init controllers
    Controller c1{MakeController()};
    Controller c2{MakeController()};

    // Simulate and check
    unsigned const nt = 5;
    TestSimulatorType::InputVector u1, u2;
    TestSimulatorType::StateVector x{x0};
    for (int i = 0; i < nt; ++i) {
        c1.Preparation();
        c2.Preparation();
        u1 = c1.Feedback(x);
        u2 = c2.Feedback(x);

        EXPECT_TRUE(u1.isApprox(u2));

        x = simulator.Integrate(x, u1, dt);
    }
}

TEST_F(MpcTest, GivenControllerAfterEvolution_whenResettingWorkingPoint_thenSameEvolution)
{
    // Init controllers
    Controller c1{MakeController()};
    Controller c2{MakeController()};

    // Simulate and reset c1
    TestSimulatorType::InputVector u;
    TestSimulatorType::StateVector x;
    TestSimulatorType::OutputVector y;
    Simulate(c1, u, x, y);
    c1.SetWorkingPoint(x0, u0);

    // Simulate and check
    unsigned const nt = 5;
    TestSimulatorType::InputVector u1, u2;
    x = x0;
    for (int i = 0; i < nt; ++i) {
        c1.Preparation();
        c2.Preparation();
        u1 = c1.Feedback(x);
        u2 = c2.Feedback(x);

        EXPECT_TRUE(u1.isApprox(u2));

        x = simulator.Integrate(x, u1, dt);
    }
}
}  //namespace mpmca::control::testing