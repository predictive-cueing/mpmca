/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <gtest/gtest.h>

#include <cmath>
#include <iostream>

#include "generic_test_header.hpp"
#include "mpmca/constants.hpp"
#include "mpmca/control/info.hpp"
#include "mpmca/control/simulator.hpp"
#include "mpmca/control/templates/component.hpp"
#include "mpmca/linear_algebra.hpp"
#include "mpmca/utilities/math.hpp"

namespace mpmca::control::testing {
class DenavitHartenbergTest : public ::testing::Test
{
  protected:
    Simulator<TestSimulatorType> sim;
    DenavitHartenbergTest()
        : sim()
    {
    }
};

TransformationMatrix GetDenavitHartenbergMatrixSlow(double theta, double d, double alpha, double a)
{
    Matrix<4, 4> rotation_z = Matrix<4, 4>::Identity();
    rotation_z(0, 0) = rotation_z(1, 1) = std::cos(theta);
    rotation_z(1, 0) = std::sin(theta);
    rotation_z(0, 1) = -rotation_z(1, 0);

    Matrix<4, 4> translation_z = Matrix<4, 4>::Identity();
    translation_z(2, 3) = d;

    Matrix<4, 4> rotation_x = Matrix<4, 4>::Identity();
    rotation_x(1, 1) = rotation_x(2, 2) = std::cos(alpha);
    rotation_x(2, 1) = std::sin(alpha);
    rotation_x(1, 2) = -rotation_x(2, 1);

    Matrix<4, 4> translation_x = Matrix<4, 4>::Identity();
    translation_z(0, 3) = a;

    return rotation_z * translation_z * rotation_x * translation_x;
}

template <std::size_t index>
TransformationMatrix GetDenavitHartenbergMatrixSlow(
    TestSimulatorType::InfoVector& info,
    TestSimulatorType::StateVector& x_current = TestSimulatorType::StateVector::Zero())
{
    auto info_viewer = MakeInfoView<TestSimulatorType>(info);
    return GetDenavitHartenbergMatrixSlow(info_viewer.ViewAt<index>().GetThetaOffset() + x_current(index),
                                          info_viewer.ViewAt<index>().GetD(),  //
                                          info_viewer.ViewAt<index>().GetAlpha(),  //
                                          info_viewer.ViewAt<index>().GetA());
}

template <std::size_t index>
void TestDenavitHartenbergMatrix(TestSimulatorType::InfoVector& info)
{
    auto info_viewer = MakeInfoView<TestSimulatorType>(info);

    auto dh_a = TransformationMatrix::FromDenavitHartenbergParameters(
        info_viewer.ViewAt<index>().GetThetaOffset(), info_viewer.ViewAt<index>().GetD(),
        info_viewer.ViewAt<index>().GetAlpha(), info_viewer.ViewAt<index>().GetA());
    (void)dh_a;
    auto dh_b =
        GetDenavitHartenbergMatrixSlow(info_viewer.ViewAt<index>().GetThetaOffset(), info_viewer.ViewAt<index>().GetD(),
                                       info_viewer.ViewAt<index>().GetAlpha(), info_viewer.ViewAt<index>().GetA());

    EXPECT_TRUE(dh_a.isApprox(dh_b, 0.0001));
}

TEST_F(DenavitHartenbergTest, InfoViewers)
{
    TestSimulatorType::InfoVector info = sim.GetInfo();

    TestSimulatorType::StateVector x_current = TestSimulatorType::StateVector::Zero();
    x_current(1) = -constants::kPi_2;

    TestDenavitHartenbergMatrix<0>(info);
    TestDenavitHartenbergMatrix<1>(info);
    TestDenavitHartenbergMatrix<2>(info);
    TestDenavitHartenbergMatrix<3>(info);
    TestDenavitHartenbergMatrix<4>(info);
    TestDenavitHartenbergMatrix<5>(info);

    std::array<TransformationMatrix, 6> dh;
    dh[0] = GetDenavitHartenbergMatrixSlow<0>(info, x_current);
    dh[1] = GetDenavitHartenbergMatrixSlow<1>(info, x_current);
    dh[2] = GetDenavitHartenbergMatrixSlow<2>(info, x_current);
    dh[3] = GetDenavitHartenbergMatrixSlow<3>(info, x_current);
    dh[4] = GetDenavitHartenbergMatrixSlow<4>(info, x_current);
    dh[5] = GetDenavitHartenbergMatrixSlow<5>(info, x_current);

    Vector<3> x = Vector<3>::Zero();
    Matrix<3, 3> r = Matrix<3, 3>::Identity();

    for (size_t i = 0; i < 6; ++i) {
        auto dx = dh[i].block(0, 3, 3, 1);
        x = x + r * dx;
        auto r_current = dh[i].block(0, 0, 3, 3);
        r = r * r_current;

        auto quat = utilities::Math::RotationMatrixToQuaternion(dh[i]);

        std::cout << quat.transpose() << " " << dx.transpose() << std::endl;
    }
}
}  //namespace mpmca::control::testing