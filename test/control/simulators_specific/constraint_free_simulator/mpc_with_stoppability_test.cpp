/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <gtest/gtest.h>

#include <fstream>
#include <tmpc/qp/QpSolverException.hpp>

#include "generic_test_header.hpp"
#include "mpmca/control/controller.hpp"
#include "mpmca/control/ocp.hpp"
#include "mpmca/control/simulator.hpp"

namespace mpmca::control::testing {
class MpcWithStoppabilityTest : public ::testing::Test
{
  protected:
    double dt;
    std::vector<unsigned> mpc_steps;
    TestSimulatorType::StateVector x0;
    TestSimulatorType::InputVector u0;
    TestSimulatorType::OutputVector y_ref;

    TestSimulatorType::InputVector u;
    TestSimulatorType::OutputVector y;

    TestSimulatorType::OutputVector wy;

    Simulator<TestSimulatorType> simulator;

    MpcWithStoppabilityTest()
        : dt{0.01}
        , u0{TestSimulatorType::InputVector::Zero()}
    {
        // Init initial state
        x0 = simulator.GetNominalState();

        // Init y_ref
        SetReferenceOutput();

        // Initialize mpc_steps
        unsigned N = 100;
        for (int i = 0; i < N; ++i) {
            if (i < 25)
                mpc_steps.push_back(1);

            if (i >= 25 && i < 50)
                mpc_steps.push_back(1 + 9 * (i - 25) / 25);

            if (i >= 50)
                mpc_steps.push_back(10);
        }

        // Init error weight
        SetOutputErrorWeight();
    }

    void SetReferenceOutput()
    {
        y_ref.topRows<3>() = simulator.GetGravity() + (Vector<3>() << 10., 10., 0.).finished();
        y_ref.bottomRows<6>() = (Vector<6>() << 1., 1., 1., 0., 0., 0.).finished();
    }

    void SetOutputErrorWeight() { wy << 1., 1., 1., 10., 10., 10., 0., 0., 0.; }

    template <typename T>
    void Simulate(T& controller)
    {
        unsigned const nt = 5;

        try {
            for (int i = 0; i < nt; ++i) {
                controller.Preparation();
                u = controller.Feedback(x0);
                x0 = simulator.Integrate(x0, u, dt);
            }
            y = simulator.GetOutput(x0, u, controller.GetFinalTransform());
        }
        catch (tmpc::QpSolverException const& ex) {
            std::cerr << "QpSolverException: " << ex.what() << std::endl;
            std::cerr << "Dumping failed QP to failed_qp.m" << std::endl;

            std::ofstream failed_qp("failed_qp.m");
            controller.PrintQp(failed_qp);

            throw;
        }
    }

    bool AreSimulatedSignalsCorrect(TestSimulatorType::InputVector u, TestSimulatorType::StateVector x,
                                    TestSimulatorType::OutputVector y, StateStoppabilityOption opt)
    {
        TestSimulatorType::InputVector uChk;
        TestSimulatorType::StateVector xChk;
        TestSimulatorType::OutputVector yChk;

        if (opt == StateStoppabilityOption::kNone) {
            uChk << -2.06466, -3.29111, 3.38393, 0.764696, -1.3305, 3.83835;
            xChk << -0.00303939, -0.00362166, 0.00314452, 0.0640324, -0.00912727, 0.029146, -0.114065, -0.153908,
                0.149247, 1.56006, 0.0394921, 0.673281;
            yChk << 2.21825, 3.63285, 6.18631, 1.5662, 0.0824917, 0.669346, 0.773141, -0.0379405, 3.78702;
        }
        else if (opt == StateStoppabilityOption::kOriginalExpression) {
            uChk << -2.06466, -3.29111, 3.38393, 0.764694, -1.3305, 3.83835;
            xChk << -0.00303939, -0.00362166, 0.00314452, 0.0640323, -0.00912692, 0.029146, -0.114065, -0.153908,
                0.149247, 1.56006, 0.0394918, 0.673281;
            yChk << 2.21825, 3.63284, 6.18631, 1.5662, 0.0824913, 0.669346, 0.773138, -0.037941, 3.78702;
        }
        else if (opt == StateStoppabilityOption::kExpressionWithoutDenominator) {
            uChk << -2.06466, -3.29111, 3.38393, 0.764694, -1.3305, 3.83835;
            xChk << -0.00303939, -0.00362166, 0.00314452, 0.0640323, -0.00912692, 0.029146, -0.114065, -0.153908,
                0.149247, 1.56006, 0.0394918, 0.673281;
            yChk << 2.21825, 3.63284, 6.18631, 1.5662, 0.0824913, 0.669346, 0.773138, -0.037941, 3.78702;
        }

        return u.isApprox(uChk, 0.00001) && x.isApprox(xChk, 0.00001) && y.isApprox(yChk, 0.00001);
    }
};

TEST_F(MpcWithStoppabilityTest, GivenControllerWithStoppabilityOptions_whenSimulating_thenResultsAreCorrectNone)
{
    Controller<TestSimulatorType, StateStoppabilityOption::kNone> controller{mpc_steps, dt, x0, u0,
                                                                             ControllerOptions<TestSimulatorType>()};
    controller.SetLevenbergMarquardt(1.e-6);
    controller.SetTighteningHorizon(8);
    controller.SetBoundsTightening(0.10);
    controller.SetOutputErrorWeight(wy);
    for (std::size_t i = 0; i < controller.GetIntegrationHorizon(); ++i)
        controller.SetReferenceOutput(i, y_ref);

    Simulate(controller);

    try {
        EXPECT_TRUE(AreSimulatedSignalsCorrect(u, x0, y, StateStoppabilityOption::kNone));
    }
    catch (std::invalid_argument& e) {
        EXPECT_TRUE(false) << e.what();
    }
}

TEST_F(MpcWithStoppabilityTest, GivenControllerWithStoppabilityOptions_whenSimulating_thenResultsAreCorrectWoDenom)
{
    Controller<TestSimulatorType, StateStoppabilityOption::kExpressionWithoutDenominator> controller{
        mpc_steps, dt, x0, u0, ControllerOptions<TestSimulatorType>()};
    controller.SetLevenbergMarquardt(1.e-6);
    controller.SetTighteningHorizon(8);
    controller.SetBoundsTightening(0.10);
    controller.SetOutputErrorWeight(wy);
    for (std::size_t i = 0; i < controller.GetIntegrationHorizon(); ++i)
        controller.SetReferenceOutput(i, y_ref);

    Simulate(controller);

    try {
        EXPECT_TRUE(AreSimulatedSignalsCorrect(u, x0, y, StateStoppabilityOption::kExpressionWithoutDenominator));
    }
    catch (std::invalid_argument& e) {
        EXPECT_TRUE(false) << e.what();
    }
}

TEST_F(MpcWithStoppabilityTest, GivenControllerWithStoppabilityOptions_whenSimulating_thenResultsAreCorrectOriginal)
{
    Controller<TestSimulatorType, StateStoppabilityOption::kOriginalExpression> controller{
        mpc_steps, dt, x0, u0, ControllerOptions<TestSimulatorType>()};
    controller.SetLevenbergMarquardt(1.e-6);
    controller.SetTighteningHorizon(8);
    controller.SetBoundsTightening(0.10);
    controller.SetOutputErrorWeight(wy);
    for (std::size_t i = 0; i < controller.GetIntegrationHorizon(); ++i)
        controller.SetReferenceOutput(i, y_ref);

    Simulate(controller);

    try {
        EXPECT_TRUE(AreSimulatedSignalsCorrect(u, x0, y, StateStoppabilityOption::kOriginalExpression));
    }
    catch (std::invalid_argument& e) {
        EXPECT_TRUE(false) << e.what();
    }
}

}  //namespace mpmca::control::testing