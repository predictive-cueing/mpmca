/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <gtest/gtest.h>

#include <fstream>
#include <tmpc/qp/QpSolverException.hpp>

#include "generic_test_header.hpp"
#include "mpmca/control/controller.hpp"
#include "mpmca/control/ocp.hpp"
#include "mpmca/control/simulator.hpp"

namespace mpmca::control::testing {
class MpcWithStoppabilityTest : public ::testing::Test
{
  protected:
    Simulator<TestSimulatorType> sim;
    double dt;
    std::vector<unsigned> mpc_steps;
    TestSimulatorType::StateVector x0;
    TestSimulatorType::InputVector u0;
    TestSimulatorType::OutputVector y_ref;

    TestSimulatorType::InputVector u;
    TestSimulatorType::OutputVector y;

    TestSimulatorType::OutputVector wy;

    Simulator<TestSimulatorType> simulator;

    MpcWithStoppabilityTest()
        : dt{0.01}
        , u0{TestSimulatorType::InputVector::Zero()}
    {
        // Init initial state
        x0 = sim.GetNominalState();

        // Init yRef
        SetReferenceOutput();

        // Initialize mpc_steps
        unsigned N = 100;
        for (int i = 0; i < N; ++i) {
            if (i < 25)
                mpc_steps.push_back(1);

            if (i >= 25 && i < 50)
                mpc_steps.push_back(1 + 9 * (i - 25) / 25);

            if (i >= 50)
                mpc_steps.push_back(10);
        }

        // Init error weight
        SetOutputErrorWeight();
    }

    void SetReferenceOutput()
    {
        y_ref.topRows<3>() = sim.GetGravity() + (Vector<3>() << 10., 10., 0.).finished();
        y_ref.bottomRows<6>() = (Vector<6>() << 1., 1., 1., 0., 0., 0.).finished();
    }

    void SetOutputErrorWeight() { wy << 1., 1., 1., 10., 10., 10., 0., 0., 0.; }

    template <typename T>
    void Simulate(T& controller)
    {
        unsigned const nt = 5;

        try {
            for (int i = 0; i < nt; ++i) {
                controller.Preparation();
                u = controller.Feedback(x0);
                x0 = simulator.Integrate(x0, u, dt);
            }
            y = simulator.GetOutput(x0, u, controller.GetFinalTransform());
        }
        catch (tmpc::QpSolverException const& ex) {
            std::cerr << "QpSolverException: " << ex.what() << std::endl;
            std::cerr << "Dumping failed QP to failed_qp.m" << std::endl;

            std::ofstream failed_qp("failed_qp.m");
            controller.PrintQp(failed_qp);

            throw;
        }
    }

    bool AreSimulatedSignalsCorrect(TestSimulatorType::InputVector u, TestSimulatorType::StateVector x,
                                    TestSimulatorType::OutputVector y, StateStoppabilityOption opt)
    {
        TestSimulatorType::InputVector uChk;
        TestSimulatorType::StateVector xChk;
        TestSimulatorType::OutputVector yChk;

        if (opt == StateStoppabilityOption::kNone) {
            uChk << 0.2871508, 2.632699, -0.71647143, -4.4403028, 3.0212676, 6.1415812, -2.7601382, -0.68664621,
                0.52359868;
            xChk << 0.0020673486, 0.0017446959, -0.0033807429, -0.0034908017, 0.0038515853, 0.0071615238, -0.0044669735,
                0.00049767083, 0.00013090086, 0.051826949, 0.082455374, -0.092739425, -0.15465525, 0.1562422,
                0.29659839, -0.16392118, 0.0014638583, 0.0052360543;
            yChk << 0.4607838, 1.8558863, 6.7736253, 0.29658347, -0.16394532, 0.0078737759, 6.1375336, -2.7665589,
                -0.094643114;
        }
        else if (opt == StateStoppabilityOption::kOriginalExpression) {
            uChk << 0.23041059, 2.7954912, -0.70748585, -4.7348607, 2.9931182, 6.126881, -2.7350635, -0.75529854,
                0.52359858;
            xChk << 0.0020523299, 0.0017766438, -0.0033774423, -0.0035392307, 0.0038355966, 0.0071589132, -0.0044527122,
                0.00048676753, 0.00013090266, 0.050520438, 0.085834863, -0.092436385, -0.16002276, 0.15506687,
                0.29632997, -0.16285732, 0.00022379504, 0.0052360827;
            yChk << 0.50862821, 1.9878061, 6.8006292, 0.29630964, -0.16289033, 0.0066257412, 6.1223427, -2.7423417,
                -0.16383557;
        }
        else if (opt == StateStoppabilityOption::kExpressionWithoutDenominator) {
            uChk << 0.23040343, 2.7952934, -0.70745998, -4.7346718, 2.9931062, 6.1268809, -2.7350693, -0.75526621,
                0.52359857;
            xChk << 0.0020522138, 0.0017764078, -0.0033773287, -0.0035389709, 0.0038355286, 0.0071588874, -0.0044527137,
                0.00048677902, 0.00013090244, 0.050515681, 0.085822819, -0.092431513, -0.16000975, 0.15506361,
                0.29632898, -0.16285703, 0.00022476692, 0.0052360755;
            yChk << 0.5086096, 1.9878149, 6.8006412, 0.29630866, -0.16289004, 0.0066266996, 6.1223429, -2.7423469,
                -0.16380353;
        }

        return u.isApprox(uChk, 0.00001) && x.isApprox(xChk, 0.00001) && y.isApprox(yChk, 0.00001);
    }
};

TEST_F(MpcWithStoppabilityTest, GivenControllerWithStoppabilityOptions_whenSimulating_thenResultsAreCorrectNone)
{
    Controller<TestSimulatorType, StateStoppabilityOption::kNone> controller{mpc_steps, dt, x0, u0,
                                                                             ControllerOptions<TestSimulatorType>()};
    controller.SetLevenbergMarquardt(1.e-6);
    controller.SetTighteningHorizon(8);
    controller.SetBoundsTightening(0.10);
    controller.SetOutputErrorWeight(wy);
    for (std::size_t i = 0; i < controller.GetIntegrationHorizon(); ++i)
        controller.SetReferenceOutput(i, y_ref);

    Simulate(controller);

    try {
        EXPECT_TRUE(AreSimulatedSignalsCorrect(u, x0, y, StateStoppabilityOption::kNone));
    }
    catch (std::invalid_argument& e) {
        EXPECT_TRUE(false) << e.what();
    }
}

TEST_F(MpcWithStoppabilityTest, GivenControllerWithStoppabilityOptions_whenSimulating_thenResultsAreCorrectWoDenom)
{
    Controller<TestSimulatorType, StateStoppabilityOption::kExpressionWithoutDenominator> controller{
        mpc_steps, dt, x0, u0, ControllerOptions<TestSimulatorType>()};
    controller.SetLevenbergMarquardt(1.e-6);
    controller.SetTighteningHorizon(8);
    controller.SetBoundsTightening(0.10);
    controller.SetOutputErrorWeight(wy);
    for (std::size_t i = 0; i < controller.GetIntegrationHorizon(); ++i)
        controller.SetReferenceOutput(i, y_ref);

    Simulate(controller);

    try {
        EXPECT_TRUE(AreSimulatedSignalsCorrect(u, x0, y, StateStoppabilityOption::kExpressionWithoutDenominator));
    }
    catch (std::invalid_argument& e) {
        EXPECT_TRUE(false) << e.what();
    }
}

TEST_F(MpcWithStoppabilityTest, GivenControllerWithStoppabilityOptions_whenSimulating_thenResultsAreCorrectOriginal)
{
    Controller<TestSimulatorType, StateStoppabilityOption::kOriginalExpression> controller{
        mpc_steps, dt, x0, u0, ControllerOptions<TestSimulatorType>()};
    controller.SetLevenbergMarquardt(1.e-6);
    controller.SetTighteningHorizon(8);
    controller.SetBoundsTightening(0.10);
    controller.SetOutputErrorWeight(wy);
    for (std::size_t i = 0; i < controller.GetIntegrationHorizon(); ++i)
        controller.SetReferenceOutput(i, y_ref);

    Simulate(controller);

    try {
        EXPECT_TRUE(AreSimulatedSignalsCorrect(u, x0, y, StateStoppabilityOption::kOriginalExpression));
    }
    catch (std::invalid_argument& e) {
        EXPECT_TRUE(false) << e.what();
    }
}

}  //namespace mpmca::control::testing