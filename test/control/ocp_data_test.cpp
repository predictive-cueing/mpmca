/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/control/ocp_data.hpp"

#include <gtest/gtest.h>

#include <fstream>

#include "generic_test_header.hpp"
#include "mpmca/control/controller_options.hpp"

namespace mpmca::control::testing {
class OcpDataTest : public ::testing::Test
{
  protected:
    double m_dt;
    std::vector<unsigned> m_mpc_sample_times;
    unsigned mpc_horizon;
    unsigned mpc_step;
    typename TestSimulatorType::StateVector x0;
    typename TestSimulatorType::InputVector u0;
    std::string qp_solver_name;

    OcpDataTest()
        : m_dt{0.01}
        , m_mpc_sample_times(10, 1)
        , mpc_horizon{10}
        , mpc_step{5}
        , x0{TestSimulatorType::StateVector::Zero()}
        , u0{TestSimulatorType::InputVector::Zero()}
    {
    }
};

TEST_F(OcpDataTest, ConstructorTest)
{
    ControllerOptions<TestSimulatorType> options = ControllerOptions<TestSimulatorType>();
    OcpData<TestSimulatorType> d1(this->m_dt, this->m_mpc_sample_times, IntegrationMethod::kConstantXY, options);
    OcpData<TestSimulatorType> d2(this->m_dt, this->mpc_horizon, this->mpc_step, IntegrationMethod::kConstantXY,
                                  options);
}

TEST_F(OcpDataTest, MpcTimeVectors)
{
    // Parameters
    std::vector<unsigned> mpc_steps_fixed(this->mpc_horizon, this->mpc_step);
    std::vector<unsigned> mpc_steps_variable{1, 1, 1, 1, 10, 10, 10, 10};
    ControllerOptions<TestSimulatorType> options = ControllerOptions<TestSimulatorType>();

    // Constructors
    OcpData<TestSimulatorType> d1(this->m_dt, mpc_steps_fixed, IntegrationMethod::kConstantXY, options);
    OcpData<TestSimulatorType> d2(this->m_dt, mpc_steps_variable, IntegrationMethod::kConstantXY, options);
    OcpData<TestSimulatorType> d3(this->m_dt, this->mpc_horizon, this->mpc_step, IntegrationMethod::kConstantXY,
                                  options);
}

}  //namespace mpmca::control::testing