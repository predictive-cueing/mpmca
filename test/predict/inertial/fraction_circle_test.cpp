/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <gtest/gtest.h>

#include "mpmca/constants.hpp"
#include "mpmca/predict/inertial/path.hpp"
#include "mpmca/predict/inertial/path_factory.hpp"

namespace mpmca::predict::inertial::testing {
class FractionCircleTest : public ::testing::Test
{
  protected:
    FractionCircleTest() {}
};

TEST_F(FractionCircleTest, ConstructorTest)
{
    std::vector<PathPointE> vector;
    int N = 100;

    for (int i = 0; i < N; ++i)
        vector.push_back(PathPointE(sin((double)i), std::cos((double)i), 0, 0, 0, 0));

    PathFactory::ProcessVector(vector, false);
}
TEST_F(FractionCircleTest, AngleBetweenTest)
{
    for (double offset = -10 * constants::kPi; offset < 90 * constants::kPi; offset += 0.0123) {
        for (double offset_x = 0; offset_x < 3 * constants::kPi; offset_x += 2 * constants::kPi) {
            double offset2 = offset + offset_x;
            EXPECT_TRUE(FractionCircle::GetAngleIsBetween(0.2 + offset, 0.4 + offset2, 0.25 + offset));
            EXPECT_TRUE(FractionCircle::GetAngleIsBetween(-0.2 + offset, -0.4 + offset2, -0.25 + offset));
            EXPECT_TRUE(FractionCircle::GetAngleIsBetween(3.0 + offset, -3.0 + offset2, 3.10 + offset));
            EXPECT_TRUE(FractionCircle::GetAngleIsBetween(3.0 + offset, -3.0 + offset2, 3.20 + offset));

            EXPECT_FALSE(FractionCircle::GetAngleIsBetween(3.0 + offset, -3.0 + offset2, 3.50 + offset));
            EXPECT_FALSE(FractionCircle::GetAngleIsBetween(3.0 + offset, -3.0 + offset2, 2.90 + offset));
            EXPECT_FALSE(FractionCircle::GetAngleIsBetween(0.2 + offset, 0.4 + offset2, -0.25 + offset));
            EXPECT_FALSE(FractionCircle::GetAngleIsBetween(0.2 + offset, 1.4 + offset2, -0.25 + offset));
            EXPECT_FALSE(FractionCircle::GetAngleIsBetween(3.0 + offset, -3.0 + offset2, -0.25 + offset));

            for (double t = -1.4; t < 1.4; t += 0.0123)
                EXPECT_TRUE(FractionCircle::GetAngleIsBetween(-1.4 + offset, 1.4 + offset2, t + offset));

            double tol = 1e-10;
            for (double t = tol; t < constants::kPi; t += 0.0123) {
                EXPECT_TRUE(FractionCircle::GetAngleIsBetween(0 + offset, constants::kPi - tol + offset2, t + offset));
                EXPECT_FALSE(
                    FractionCircle::GetAngleIsBetween(0 + offset, constants::kPi - tol + offset2, -t + offset));
            }

            for (double t = tol; t < constants::kPi; t += 0.0123) {
                EXPECT_FALSE(FractionCircle::GetAngleIsBetween(0 + offset, constants::kPi + tol + offset2, t + offset));
                EXPECT_TRUE(FractionCircle::GetAngleIsBetween(0 + offset, constants::kPi + tol + offset2, -t + offset));
            }
        }
    }
}
TEST_F(FractionCircleTest, DetailedCircleTest)
{
    std::vector<PathPointE> vector;

    double x0 = 0.0;
    double y0 = 0.0;
    double angle1 = 3.1;
    double angle2 = -0.1;
    double l1 = 0.1;
    double l2 = 0.1;

    vector.push_back({x0, y0 + 0.0, 0.0, 0.0, 0.0, 0.0});
    vector.push_back({vector.back().GetX() + l1 * std::cos(angle1), vector.back().GetY() + l1 * std::sin(angle1), 0.0,
                      0.0, 0.0, 0.0});
    vector.push_back({vector.back().GetX() + l2 * std::cos(angle1 + angle2),
                      vector.back().GetY() + l2 * std::sin(angle1 + angle2), 0, 0, 0, 0});

    PathFactory::ProcessVector(vector, false);
    int index = 1;

    EXPECT_TRUE(vector[index].HasCircle());
    EXPECT_TRUE(vector[index].HasNext());
    EXPECT_TRUE(vector[index].HasPrevious());

    for (double f = 0.0; f < 1.0; f += 0.1) {
        {
            auto ds = vector[index - 1].GetDistance(vector[index]);
            auto p = vector[index - 1].MoveDistance(vector[index], f * ds);
            auto frac = vector[index].GetFraction(p);
            EXPECT_NEAR(f - 1, frac, 1e-3);
        }
    }
    for (double f = 0.0; f < 1.0; f += 0.1) {
        {
            auto ds = vector[index].GetDistance(vector[index + 1]);
            auto p = vector[index].MoveDistance(vector[index + 1], f * ds);
            auto frac = vector[index].GetFraction(p);
            EXPECT_NEAR(f, frac, 1e-3);
        }
    }
}

TEST_F(FractionCircleTest, ExtremelyDetailedCircleTest)
{
    long N = 0;
    double tol = 0.0002;

    for (double x0 = -1000.0; x0 < 3000.0; x0 += 923.3451) {
        for (double y0 = -1223.124; y0 < 928.148; y0 += 499.646) {
            for (double angle1 = -5 * constants::kPi; angle1 < 3 * constants::kPi; angle1 += 0.81958) {
                for (double angle2 = -0.1; angle2 < 0.1; angle2 += 0.01958) {
                    for (double l1 = 0.1; l1 < 2.0; l1 += 0.15309) {
                        for (double l2 = 0.1; l2 < 2.0; l2 += 0.35309) {
                            std::vector<PathPointE> vector;
                            vector.push_back({x0, y0, 0.0, 0.0, 0.0, 0.0});
                            vector.push_back({vector.back().GetX() + l1 * std::cos(angle1),
                                              vector.back().GetY() + l1 * std::sin(angle1), 0.0, 0.0, 0.0, 0.0});
                            vector.push_back({vector.back().GetX() + l2 * std::cos(angle1 + angle2),
                                              vector.back().GetY() + l2 * std::sin(angle1 + angle2), 0, 0, 0, 0});

                            PathFactory::ProcessVector(vector, false);
                            ++N;
                            int index = 1;

                            for (double f = 0.0; f < 1.0; f += 0.1) {
                                {
                                    auto ds = vector[index - 1].GetDistance(vector[index]);
                                    auto p = vector[index - 1].MoveDistance(vector[index], f * ds);
                                    auto frac = vector[index].FractionCircle::GetFraction(p);
                                    EXPECT_NEAR(f - 1, frac, tol);
                                }
                                {
                                    auto ds = vector[index].GetDistance(vector[index + 1]);
                                    auto p = vector[index].MoveDistance(vector[index + 1], f * ds);
                                    auto frac = vector[index].FractionCircle::GetFraction(p);
                                    EXPECT_NEAR(f, frac, tol);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
}  //namespace mpmca::predict::inertial::testing
