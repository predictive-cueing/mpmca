/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/model.hpp"

#include <gtest/gtest.h>

#include "mpmca/predict/inertial/signal.hpp"
#include "mpmca/utilities/clock.hpp"

namespace mpmca::predict::inertial::testing {

constexpr int T_NHist = 5;

class TestModel : public Model<3, T_NHist>
{
  public:
    TestModel(const utilities::Horizon& t)
        : Model("TestModel", t, false)
        , x(this, "x")
        , y(this, "y")
        , z(this, "z")
    {
    }

    Signal<TestModel> x;  //0
    Signal<TestModel> y;  //1
    Signal<TestModel> z;  //2
};

class ModelTest : public ::testing::Test
{
  public:
    utilities::Clock m_clock;
    utilities::Horizon m_horizon;
    TestModel m_test_model;

  public:
    ModelTest()
        : m_clock(std::chrono::milliseconds(10))
        , m_horizon(m_clock, 100)
        , m_test_model(m_horizon)
    {
    }
};

TEST_F(ModelTest, SizesTest)
{
    EXPECT_TRUE(m_test_model.GetNPrediction() == 101);
    EXPECT_TRUE(m_test_model.GetNHistory() == T_NHist);
    EXPECT_TRUE(m_test_model.GetNTotal() == 101 + T_NHist);
    EXPECT_TRUE(m_test_model.GetNSignals() == 3);
}

TEST_F(ModelTest, InitTestEigen)
{
    TestModel::SignalVector init_vec;
    init_vec << 1.0, 2.0, 3.0;
    m_test_model.Init(init_vec);

    ASSERT_DOUBLE_EQ(1.0, m_test_model.x.GetInitializedValue());
    ASSERT_DOUBLE_EQ(2.0, m_test_model.y.GetInitializedValue());
    ASSERT_DOUBLE_EQ(3.0, m_test_model.z.GetInitializedValue());
}

TEST_F(ModelTest, FirstStepTest)
{
    EXPECT_FALSE(m_test_model.IsFirstStepInHorizon(T_NHist - 1));
    EXPECT_TRUE(m_test_model.IsFirstStepInHorizon(T_NHist));
    EXPECT_FALSE(m_test_model.IsFirstStepInHorizon(T_NHist + 1));
}

TEST_F(ModelTest, GetTakeoverDistanceTest)
{
    m_test_model.CalculateTakeoverDistance(T_NHist, 10);
    EXPECT_TRUE(m_test_model.GetTakeoverDistance() == 0);

    for (int i = 0; i < 100; ++i) {
        m_test_model.CalculateTakeoverDistance(T_NHist + i, 10);
        EXPECT_TRUE(abs(m_test_model.GetTakeoverDistance() - (double)i * 0.1) < 1e-10);
    }

    m_test_model.CalculateTakeoverDistance(T_NHist, 10);
    EXPECT_TRUE(m_test_model.GetTakeoverDistance() == 0);
}

TEST_F(ModelTest, GetTakeoverTimeTest)
{
    m_test_model.CalculateTakeoverTime(T_NHist);
    EXPECT_TRUE(m_test_model.GetTakeoverTime() == 0);

    for (int i = 0; i < 100; ++i) {
        m_test_model.CalculateTakeoverTime(T_NHist + i);
        EXPECT_TRUE(abs(m_test_model.GetTakeoverTime() - (double)i * 0.01) < 1e-10);
    }

    m_test_model.CalculateTakeoverTime(T_NHist);
    EXPECT_TRUE(m_test_model.GetTakeoverTime() == 0);
}

TEST_F(ModelTest, CorrectTest)
{
    TestModel::SignalVector init_vec{10.0, 20.0, 30.0};
    m_test_model.Init(init_vec);

    double s_fade = 5;

    for (int i = 0; i < m_test_model.GetNPrediction(); ++i) {
        m_test_model.CalculateTakeoverDistance(T_NHist + i, 10);
        m_test_model.x.SetValue(T_NHist + i, 30);
        m_test_model.x.Correct(T_NHist + i, s_fade, m_test_model.GetTakeoverDistance());

        EXPECT_DOUBLE_EQ(m_test_model.x.GetValue(T_NHist + i),
                         utilities::Math::GetCosineFade(10, 30, s_fade, m_test_model.GetTakeoverDistance()));
    }
}

TEST_F(ModelTest, InitSignalsTest)
{
    m_test_model.x.Init(10);
    m_test_model.y.Init(20);
    m_test_model.z.Init(30);

    double s_fade = 5;

    for (int i = 0; i < m_test_model.GetNPrediction(); ++i) {
        m_test_model.CalculateTakeoverDistance(T_NHist + i, 10);
        m_test_model.x.SetValue(T_NHist + i, 30);
        m_test_model.x.Correct(T_NHist + i, s_fade, m_test_model.GetTakeoverDistance());

        EXPECT_DOUBLE_EQ(m_test_model.x.GetValue(T_NHist + i),
                         utilities::Math::GetCosineFade(10, 30, s_fade, m_test_model.GetTakeoverDistance()));
    }
}
}  //namespace mpmca::predict::inertial::testing