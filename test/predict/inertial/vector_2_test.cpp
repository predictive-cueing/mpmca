/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/vector_2.hpp"

#include <gtest/gtest.h>

#include "mpmca/constants.hpp"
#include "mpmca/predict/inertial/vector_3.hpp"
#include "mpmca/utilities/math.hpp"

namespace mpmca::predict::inertial::testing {

class Vector2Test : public ::testing::Test
{
  protected:
    Vector2Test() {}
};

TEST_F(Vector2Test, DistanceTest)
{
    Vector2 p0(0, 0);
    Vector2 p1(1, 1);
    Vector2 p2(-10, -20);

    //Vector3!
    Vector3 p3(-5, 9, 1);

    EXPECT_EQ(sqrt(2), p0.GetDistance(p1));
    EXPECT_EQ(sqrt(500), p0.GetDistance(p2));

    EXPECT_EQ(2, p0.GetDistanceSquared(p1));
    EXPECT_EQ(500, p0.GetDistanceSquared(p2));

    EXPECT_EQ(sqrt(6 * 6 + 8 * 8), p1.GetDistance(p3));
}

TEST_F(Vector2Test, SumAndDifferenceTest)
{
    Vector2 p0(-3, -2);
    Vector2 p1(-1, 0);
    Vector2 p2(1, 2);

    auto d1 = p2 - p1;
    EXPECT_EQ(2, d1.GetX());
    EXPECT_EQ(2, d1.GetY());

    auto d2 = p2 - p0;
    EXPECT_EQ(4, d2.GetX());
    EXPECT_EQ(4, d2.GetY());

    auto s1 = p0 + p1;
    EXPECT_EQ(-4, s1.GetX());
    EXPECT_EQ(-2, s1.GetY());

    auto s2 = p1 + p0;
    EXPECT_EQ(-4, s2.GetX());
    EXPECT_EQ(-2, s2.GetY());

    auto m1 = p0.GetMeanWith(p1);
    EXPECT_EQ(-2, m1.GetX());
    EXPECT_EQ(-1, m1.GetY());
}
TEST_F(Vector2Test, LengthTest)
{
    Vector2 p0(10, 10);
    Vector2 p1(-11, -12);

    EXPECT_NEAR(std::sqrt(200.0), p0.GetLength(), 1e-10);
    EXPECT_NEAR(std::sqrt(11.0 * 11.0 + 12.0 * 12.0), p1.GetLength(), 1e-10);
}
TEST_F(Vector2Test, UnitVectorTest)
{
    Vector2 p0(10, 10);
    Vector2 p1(-11, -12);

    auto p0_unit = p0.GetUnitVector();
    auto p1_unit = p1.GetUnitVector();

    EXPECT_NEAR(1.0, p0_unit.GetLength(), 1e-10);
    EXPECT_NEAR(1.0, p1_unit.GetLength(), 1e-10);

    EXPECT_NEAR(10.0 / std::sqrt(200.0), p0_unit.GetX(), 1e-10);
    EXPECT_NEAR(10.0 / std::sqrt(200.0), p0_unit.GetY(), 1e-10);

    EXPECT_NEAR(-11.0 / std::sqrt(11.0 * 11.0 + 12.0 * 12.0), p1_unit.GetX(), 1e-10);
    EXPECT_NEAR(-12.0 / std::sqrt(11.0 * 11.0 + 12.0 * 12.0), p1_unit.GetY(), 1e-10);
}
TEST_F(Vector2Test, MoveDistanceTowardTest)
{
    Vector2 p0(10, 10);
    Vector2 p1(11, 10);
    Vector2 p2(10, 11);
    Vector2 p3(11, 11);
    Vector2 p4(34102, 34102);

    EXPECT_NEAR(12.5, p0.MoveDistance(p1, 2.5).GetX(), 1e-10);
    EXPECT_NEAR(10, p0.MoveDistance(p2, 2.5).GetX(), 1e-10);
    EXPECT_NEAR(10 + std::sqrt(2) / 2 * 2.5, p0.MoveDistance(p3, 2.5).GetX(), 1e-10);
    EXPECT_NEAR(10 + std::sqrt(2) / 2 * 2.5, p0.MoveDistance(p4, 2.5).GetX(), 1e-10);

    EXPECT_NEAR(10.0, p0.MoveDistance(p1, 2.5).GetY(), 1e-10);
    EXPECT_NEAR(12.5, p0.MoveDistance(p2, 2.5).GetY(), 1e-10);
    EXPECT_NEAR(10 + std::sqrt(2) / 2 * 2.5, p0.MoveDistance(p3, 2.5).GetY(), 1e-10);
    EXPECT_NEAR(10 + std::sqrt(2) / 2 * 2.5, p0.MoveDistance(p4, 2.5).GetY(), 1e-10);
}
TEST_F(Vector2Test, HeadingTest)
{
    double R = 30;
    Vector2 p0(-5, 10);

    for (double psi = -2 * constants::kPi; psi < 2 * constants::kPi; psi += 0.0112) {
        Vector2 p(R * std::cos(psi), R * std::sin(psi));
        EXPECT_NEAR(utilities::Math::GetAngleDiff(psi, 0), p.GetHeading(), 1e-10);

        Vector2 p2 = p + p0;
        EXPECT_NEAR(utilities::Math::GetAngleDiff(psi, 0), p0.GetHeadingFrom(p2), 1e-10);
    }
}
TEST_F(Vector2Test, DotTest)
{
    Vector2 p0(0, 10);
    Vector2 p1(4, 4);

    EXPECT_NEAR(4, p0.GetDotProductWith(p1) / p0.GetLength(), 1e-10);
    EXPECT_NEAR(5 * std::sqrt(2), p1.GetDotProductWith(p0) / p1.GetLength(), 1e-10);
}
}  //namespace mpmca::predict::inertial::testing