/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <gtest/gtest.h>

#include "mpmca/predict/inertial/car_minimal.hpp"
#include "mpmca/predict/inertial/driver_minimal.hpp"
#include "mpmca/predict/inertial/inertial_output.hpp"
#include "mpmca/predict/inertial/path_factory.hpp"
#include "mpmca/predict/inertial/simulation.hpp"
#include "mpmca/utilities/clock.hpp"
namespace mpmca::predict::inertial::testing {

class SimulationTTest : public ::testing::Test
{
  protected:
    utilities::ConfigurationPtr m_config;
    utilities::Clock m_clock;
    utilities::Horizon m_horizon;
    SimulationTTest()
        : m_config(utilities::Configuration::CreateRootConfig("simulation_test"))
        , m_clock(std::chrono::milliseconds(10))
        , m_horizon(m_clock, 1000)
    {
    }
};

class ModelA;
class ModelB;
class ModelC;
class ModelD;

template <typename... DependedModels>
class BaseModel
{
  protected:
    std::tuple<DependedModels*...> m_depended_model_ptr;

    template <typename Tuple, std::size_t... Is>
    void InitializeWithNullptr(Tuple& tuple, std::index_sequence<Is...>)
    {
        ((std::get<Is>(tuple) = nullptr), ...);
    }

    BaseModel() { InitializeWithNullptr(m_depended_model_ptr, std::make_index_sequence<sizeof...(DependedModels)>{}); }
    virtual std::string GetName() const = 0;

    template <std::size_t... Is>
    void HasAllModelsInternal(std::vector<bool>& result, std::index_sequence<Is...>) const
    {
        ((result.push_back(std::get<Is>(m_depended_model_ptr) != nullptr)), ...);
    }

  public:
    template <typename T1, typename T2>
    void SaveModelPtrIfTypeEqual(T1* ptr1, T2*& ptr2)
    {
        if constexpr (std::is_same<T1, T2>::value) {
            ptr2 = ptr1;
        }
        (void)ptr1;
        (void)ptr2;
    }

    template <typename T1, typename Tuple, std::size_t... Is>
    void SavePtrs(T1* ptr, Tuple& tuple, std::index_sequence<Is...>)
    {
        ((SaveModelPtrIfTypeEqual(ptr, std::get<Is>(tuple))), ...);
    }

    template <typename Tuple, std::size_t... Ls>
    void SaveOtherModelPtr(Tuple& tuple, std::index_sequence<Ls...>)
    {
        ((SavePtrs(&(std::get<Ls>(tuple)), m_depended_model_ptr,
                   std::make_index_sequence<sizeof...(DependedModels)>{})),
         ...);
    }

    bool HasAllModels() const
    {
        std::vector<bool> result;
        HasAllModelsInternal(result, std::make_index_sequence<sizeof...(DependedModels)>{});
        return std::all_of(result.cbegin(), result.cend(), [](bool r) { return r; });
    }

    template <std::size_t N>
    inline auto GetModel() const
    {
        return std::get<N>(m_depended_model_ptr);
    }

    template <std::size_t N>
    bool HasModel() const
    {
        return std::get<N>(m_depended_model_ptr) != nullptr;
    }
};

class ModelA : public BaseModel<ModelB>
{
  public:
    ModelA(utilities::ConfigurationPtr config, const utilities::Horizon& horizon, const std::string& name,
           SimulationControlOption for_control = SimulationControlOption::kForPrediction)
    {
    }

    void PrintAllDependentModelNames();

    std::string GetName() const override { return "ModelA"; }

    void DoCalculation();
};

class ModelB : public BaseModel<ModelA, ModelD>
{
  public:
    ModelB(utilities::ConfigurationPtr config, const utilities::Horizon& horizon, const std::string& name,
           SimulationControlOption for_control = SimulationControlOption::kForPrediction)
    {
    }
    std::string GetName() const override { return "ModelB"; }

    double GetValue() const { return 5; }
};

void ModelA::DoCalculation()
{
    std::cout << "3 + ModelB.GetValue() = " << (3 + GetModel<0>()->GetValue()) << std::endl;
}

void ModelA::PrintAllDependentModelNames()
{
    std::cout << std::get<0>(m_depended_model_ptr)->GetName() << std::endl;
}

class ModelC : public BaseModel<ModelA>
{
  public:
    ModelC(utilities::ConfigurationPtr config, const utilities::Horizon& horizon, const std::string& name,
           SimulationControlOption for_control = SimulationControlOption::kForPrediction)
    {
    }
    std::string GetName() const override { return "ModelC"; }
};

class ModelD : public BaseModel<ModelA>
{
  public:
    ModelD(utilities::ConfigurationPtr config, const utilities::Horizon& horizon, const std::string& name,
           SimulationControlOption for_control = SimulationControlOption::kForPrediction)
    {
    }
    std::string GetName() const override { return "ModelD"; }
};

template <typename... ModelTypes>
class SimulationT
{
    std::tuple<ModelTypes...> m_models;

    template <typename Tuple, std::size_t... Is>
    void CallSaveOtherModelPtr(Tuple& tpl, std::index_sequence<Is...>)
    {
        ((std::get<Is>(tpl).SaveOtherModelPtr(m_models, std::make_index_sequence<sizeof...(ModelTypes)>{})), ...);
    }

    template <typename Tuple, std::size_t... Is>
    void CallGetName(std::vector<std::string>& vector, Tuple& tpl, std::index_sequence<Is...>)
    {
        ((vector.push_back(std::get<Is>(tpl).GetName())), ...);
    }

    template <typename Tuple, std::size_t... Is>
    void CallHasAllModels(std::vector<bool>& vector, Tuple& tpl, std::index_sequence<Is...>)
    {
        ((vector.push_back(std::get<Is>(tpl).HasAllModels())), ...);
    }

    template <typename Tuple, std::size_t... Is>
    void CallHasAllModels(Tuple& tpl, std::index_sequence<Is...>)
    {
        std::vector<bool> all_models;
        CallHasAllModels(all_models, m_models, std::make_index_sequence<sizeof...(ModelTypes)>{});

        std::vector<std::string> names;
        CallGetName(names, m_models, std::make_index_sequence<sizeof...(ModelTypes)>{});

        if (!std::all_of(all_models.cbegin(), all_models.cend(), [](bool res) { return res; })) {
            throw std::runtime_error("Not all dependencies met");
        }
    }

  public:
    SimulationT(utilities::ConfigurationPtr config, const utilities::Horizon& horizon, const std::string& name,
                SimulationControlOption for_control = SimulationControlOption::kForPrediction)
        : m_models{ModelTypes(config, horizon, name, for_control)...}
    {
        CallSaveOtherModelPtr(m_models, std::make_index_sequence<sizeof...(ModelTypes)>{});
        CallHasAllModels(m_models, std::make_index_sequence<sizeof...(ModelTypes)>{});
    };
    //

    template <std::size_t N>
    auto& GetModel()
    {
        return std::get<N>(m_models);
    }
};

class TestClass
{
  public:
    std::string m_name;
    TestClass(const std::string& name)
        : m_name(name)
    {
    }
};

TEST_F(SimulationTTest, TupleTest)
{
    SimulationT<ModelA, ModelB, ModelD> simulation(m_config, m_horizon, "Test", SimulationControlOption::kForControl);

    // simulation.GetModel<0>().PrintAllDependentModelNames();
    // simulation.GetModel<0>().DoCalculation();

    // std::cout << "Model A in simulation has model B: " << simulation.GetModel<0>().HasModel<0>() << std::endl;
    // std::cout << "Model A has all models " << simulation.GetModel<0>().HasAllModels() << std::endl;

    // std::cout << simulation.GetModel<0>().GetModel<0>()->GetName() << std::endl;

    // std::cout << simulation.GetModel<1>().GetModel<0>()->GetName() << std::endl;
    // std::cout << simulation.GetModel<1>().GetModel<1>()->GetName() << std::endl;

    // std::cout << simulation.GetModel<2>().GetModel<0>()->GetName() << std::endl;

    // std::array<std::string, 2> test{"hallo", "boo"};

    // std::cout << test[0] << " " << test[1] << std::endl;

    // TestClass* a = new TestClass("A");
    // TestClass* b = new TestClass("B");

    // a = b;

    // std::cout << a->m_name << std::endl;
}
}  //namespace mpmca::predict::inertial::testing