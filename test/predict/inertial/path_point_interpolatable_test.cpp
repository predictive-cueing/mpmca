/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/path_point_interpolatable.hpp"

#include <gtest/gtest.h>

#include "mpmca/constants.hpp"
#include "mpmca/predict/inertial/interpolatable_cubic_spline.hpp"
#include "mpmca/predict/inertial/interpolatable_linear.hpp"
#include "mpmca/predict/inertial/path_factory.hpp"
#include "mpmca/predict/inertial/path_searcher_segmented.hpp"
#include "mpmca/predict/inertial/vector_6.hpp"
#include "mpmca/utilities/clock.hpp"

namespace mpmca::predict::inertial::testing {
class PathPointInterpolatableTest : public ::testing::Test
{
  protected:
    PathPointInterpolatableTest() {}
};
TEST_F(PathPointInterpolatableTest, VectorProcessorTest)
{
    std::vector<PathPointE> vector;
    int N = 100;

    for (int i = 0; i < N; ++i)
        vector.push_back(PathPointE(sin((double)i), std::cos((double)i), 0, 0, 0, 0));

    PathFactory::ProcessVector(vector, false);
}
TEST_F(PathPointInterpolatableTest, SymmetricCubicSplineTest)
{
    /**
     * @brief This test evaluates a symmetric 'mountain' that is interpolated by a cubic spline:
     *         --2--
     *    --1--     --3--
     * 0-- 				 --4
     *
     * the skip parameter modifies the shape to something like this:
     *          	   --2--
     *    	  ------1--     --3------
     * 0------ 				 		 ------4
     *
     * For all shapes like this, it should be true that:
     * the interpolation from two adjacent points along the same segment should result in equal values,
     * the interpolation in forward direction at points 0 and 1 should equal that in backward direction at 3 and 4,
     * the interpolation in backward direction at point 2 should equal that in forward direction at point 2.
     */

    for (int skip = 0; skip < 10; ++skip) {
        std::vector<PathPointE> vector;
        vector.push_back(PathPointE(0 + 0 * skip, 0, 0, 0, 0, 0));
        vector.push_back(PathPointE(1 + 1 * skip, 0, 0, 1, 1, 1));
        vector.push_back(PathPointE(2 + 1 * skip, 0, 0, 2, 2, 2));
        vector.push_back(PathPointE(3 + 1 * skip, 0, 0, 1, 1, 1));
        vector.push_back(PathPointE(4 + 2 * skip, 0, 0, 0, 0, 0));

        PathFactory::ProcessVector(vector, false);

        for (double f = 0; f < 100; f += 0.123) {
            // beyond the end-points the value should remain equal to the value at the endpoint.
            EXPECT_NEAR(0, vector[0].GetInterpolated(-f).GetZ(), 1e-10);
            EXPECT_NEAR(0, vector[4].GetInterpolated(f).GetZ(), 1e-10);
        }

        for (int i = 0; i < 4; ++i) {
            for (double f = 0.0; f < 1.0; f += 0.0123) {
                EXPECT_NEAR(vector[i].GetInterpolated(f).GetZ(), vector[i + 1].GetInterpolated(f - 1).GetZ(), 1e-10);
                EXPECT_NEAR(vector[i].GetInterpolated(f).GetPhi(), vector[i + 1].GetInterpolated(f - 1).GetPhi(),
                            1e-10);
                EXPECT_NEAR(vector[i].GetInterpolated(f).GetDzDs(), vector[i + 1].GetInterpolated(f - 1).GetDzDs(),
                            1e-10);
            }
        }

        for (double f = 0.0; f < 1.0; f += 0.0123)
            EXPECT_NEAR(vector[2].GetInterpolated(f).GetZ(), vector[2].GetInterpolated(-f).GetZ(), 1e-10);

        for (double f = 0.0; f < 1.0; f += 0.0123)
            EXPECT_NEAR(vector[0].GetInterpolated(f).GetZ(), vector[4].GetInterpolated(-f).GetZ(), 1e-10);
    }
}
TEST_F(PathPointInterpolatableTest, AsymmetricCubicSplineTest)
{
    /**
     * @brief This test evaluates a symmetric 'mountain' that is interpolated by a cubic spline:
     *         --2--
     *    --1--     --3--
     * 0-- 				 --4
     *
     * the skip parameter modifies the shape to something like this:
     *          	   --2--
     *    	  ------1--     --3------
     * 0------ 				 		 ------4
     *
     * For all shapes like this, it should be true that:
     * the interpolation from two adjacent points along the same segment should result in equal values,
     * the interpolation in forward direction at points 0 and 1 should equal that in backward direction at 3 and 4,
     * the interpolation in backward direction at point 2 should equal that in forward direction at point 2.
     */

    for (int skip = 0; skip < 10; ++skip) {
        std::vector<PathPointE> vector;
        vector.push_back(PathPointE(0 + 0 * skip, 0, 0, 0, 0, 0));
        vector.push_back(PathPointE(1 + 1 * skip, 0, 1, 1, 1, 1));
        vector.push_back(PathPointE(2 + 1 * skip, 0, 2, 2, 2, 2));
        vector.push_back(PathPointE(3 + 1 * skip, 0, 1, 1, 1, 1));
        vector.push_back(PathPointE(4 + 2 * skip, 0, 0, 0, 0, 0));

        PathFactory::ProcessVector(vector, false);

        for (double f = 0; f < 100; f += 0.123) {
            // beyond the end-points the value should remain equal to the value at the endpoint.
            EXPECT_NEAR(0, vector[0].GetInterpolated(-f).GetZ(), 1e-10);
            EXPECT_NEAR(0, vector[4].GetInterpolated(f).GetZ(), 1e-10);
        }

        for (int i = 0; i < 4; ++i) {
            for (double f = 0.0; f <= 1.0; f += 0.022) {
                EXPECT_NEAR(vector[i].GetInterpolated(f).GetZ(), vector[i + 1].GetInterpolated(f - 1).GetZ(), 1e-10)
                    << "index: " << i << " frac: " << f;
                EXPECT_NEAR(vector[i].GetInterpolated(f).GetPhi(), vector[i + 1].GetInterpolated(f - 1).GetPhi(), 1e-10)
                    << "index: " << i << " frac: " << f;
                EXPECT_NEAR(vector[i].GetInterpolated(f).GetDzDs(), vector[i + 1].GetInterpolated(f - 1).GetDzDs(),
                            1e-10)
                    << "index: " << i << " frac: " << f;
            }
        }

        for (double f = 0.0; f < 1.0; f += 0.0123)
            EXPECT_NEAR(vector[2].GetInterpolated(f).GetZ(), vector[2].GetInterpolated(-f).GetZ(), 1e-10);

        for (double f = 0.0; f < 1.0; f += 0.0123)
            EXPECT_NEAR(vector[0].GetInterpolated(f).GetZ(), vector[4].GetInterpolated(-f).GetZ(), 1e-10);
    }
}
TEST_F(PathPointInterpolatableTest, StraightLineCubicSplineTest)
{
    /**
     * @brief This test evaluates whether a linearly increasing segment of length 5 indeed sees linear increase along
     * all segments.
     */

    std::vector<PathPointE> vector;
    vector.push_back(PathPointE(0, 0, 0, 0, 0, 0));
    vector.push_back(PathPointE(1, 0, 1, 1, 1, 1));
    vector.push_back(PathPointE(2, 0, 2, 2, 2, 2));
    vector.push_back(PathPointE(3, 0, 3, 3, 3, 3));
    vector.push_back(PathPointE(4, 0, 4, 4, 4, 4));
    PathFactory::ProcessVector(vector, false);

    for (double f = 0; f < 100; f += 0.123) {
        // beyond the end-points the value should remain equal to the value at the endpoint.
        EXPECT_NEAR(0, vector[0].GetInterpolated(-f).GetZ(), 1e-10);
        EXPECT_NEAR(4, vector[4].GetInterpolated(f).GetZ(), 1e-10);
        EXPECT_NEAR(0, vector[0].GetInterpolated(-f).GetPhi(), 1e-10);
        EXPECT_NEAR(4, vector[4].GetInterpolated(f).GetPhi(), 1e-10);
        EXPECT_NEAR(0, vector[0].GetInterpolated(-f).GetTheta(), 1e-10);
        EXPECT_NEAR(4, vector[4].GetInterpolated(f).GetTheta(), 1e-10);
        EXPECT_NEAR(0, vector[0].GetInterpolated(-f).GetPsi(), 1e-10);
        EXPECT_NEAR(4, vector[4].GetInterpolated(f).GetPsi(), 1e-10);
    }

    for (int i = 0; i < 4; ++i) {
        for (double f = 0.0; f < 1.0; f += 0.0123) {
            EXPECT_NEAR(vector[i].GetInterpolated(f).GetZ(), vector[i].GetInterpolated(f).GetZ(), 1e-10);
            EXPECT_NEAR(vector[i].GetInterpolated(f).GetZ(), vector[i].GetInterpolated(f).GetPhi(), 1e-10);
            EXPECT_NEAR(vector[i].GetInterpolated(f).GetZ(), vector[i].GetInterpolated(f).GetTheta(), 1e-10);
            EXPECT_NEAR(vector[i].GetInterpolated(f).GetZ(), vector[i].GetInterpolated(f).GetPsi(), 1e-10);

            EXPECT_NEAR(vector[i].GetInterpolated(f).GetZ(), vector[i + 1].GetInterpolated(f - 1).GetZ(), 1e-10);
            EXPECT_NEAR(vector[i].GetInterpolated(f).GetDzDs(), vector[i + 1].GetInterpolated(f - 1).GetDzDs(), 1e-10);
            EXPECT_NEAR(vector[i].GetInterpolated(f).GetPhi(), vector[i + 1].GetInterpolated(f - 1).GetPhi(), 1e-10);
            EXPECT_NEAR(vector[i].GetInterpolated(f).GetDPhiDs(), vector[i + 1].GetInterpolated(f - 1).GetDPhiDs(),
                        1e-10);
            EXPECT_NEAR(vector[i].GetInterpolated(f).GetTheta(), vector[i + 1].GetInterpolated(f - 1).GetTheta(),
                        1e-10);
            EXPECT_NEAR(vector[i].GetInterpolated(f).GetDThetaDs(), vector[i + 1].GetInterpolated(f - 1).GetDThetaDs(),
                        1e-10);
            EXPECT_NEAR(vector[i].GetInterpolated(f).GetPsi(), vector[i + 1].GetInterpolated(f - 1).GetPsi(), 1e-10);
            EXPECT_NEAR(vector[i].GetInterpolated(f).GetDPsiDs(), vector[i + 1].GetInterpolated(f - 1).GetDPsiDs(),
                        1e-10);
        }
    }

    for (double s = 0.0; s < 4.0; s += 0.0123) {
        int i = (int)s;
        EXPECT_NEAR(s, vector[i].GetInterpolated(s - (double)i).GetZ(), 1e-10);
    }
}
TEST_F(PathPointInterpolatableTest, SineCubicSplineTest)
{
    std::vector<PathPointE> vector;
    int N = 100;
    double A = 0.5;

    for (int i = 0; i < N; ++i) {
        vector.push_back(
            PathPointE(0, (double)i, 0, std::sin(A * (double)i), std::sin(A * (double)i), std::sin(A * (double)i)));
    }

    PathFactory::ProcessVector(vector, false);

    for (double s = constants::kPi / A; s < 3 * constants::kPi / A; s += 0.1) {
        int i = (int)s;
        EXPECT_NEAR(sin(A * s), vector[i].GetInterpolated(s - (double)i).GetPhi(), 0.03);
        EXPECT_NEAR(A * std::cos(A * s), vector[i].GetInterpolated(s - (double)i).GetDPhiDs(), 0.03);
    }
}
TEST_F(PathPointInterpolatableTest, DeletedFunctionTest)
{
    PathPointE p(2.0, 3);

    EXPECT_EQ(p.GetX(), 2.0);
}

TEST_F(PathPointInterpolatableTest, ClosedPathInterpolation)
{
    double ds = 0.1;
    double R = 5;
    PathPtr path = PathFactory::CreatePathCircle(ds, 0, 2 * constants::kPi, R, 0, 0, true);

    for (double f = -1.0; f < 1.0; f += 0.1) {
        EXPECT_NEAR(path->GetPathPointAt(0).GetInterpolated(0).GetDzDs(),
                    path->GetPathPointAt(0).GetInterpolated(f).GetDzDs(), 1e-5);
        EXPECT_NEAR(path->GetPathPointAt(0).GetInterpolated(0).GetDPhiDs(),
                    path->GetPathPointAt(0).GetInterpolated(f).GetDPhiDs(), 1e-5);
        EXPECT_NEAR(path->GetPathPointAt(0).GetInterpolated(0).GetDThetaDs(),
                    path->GetPathPointAt(0).GetInterpolated(f).GetDThetaDs(), 1e-5);
        EXPECT_NEAR(path->GetPathPointAt(0).GetInterpolated(0).GetDPsiDs(),
                    path->GetPathPointAt(0).GetInterpolated(f).GetDPsiDs(), 1e-5);

        EXPECT_NEAR(2 * constants::kPi / (2 * constants::kPi * R),
                    path->GetPathPointAt(0).GetInterpolated(f).GetDPsiDs(), 1e-5);
    }
}
}  //namespace mpmca::predict::inertial::testing
