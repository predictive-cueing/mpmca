/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/vector_3.hpp"

#include <gtest/gtest.h>

#include <cmath>

namespace mpmca::predict::inertial::testing {

class Vector3Test : public ::testing::Test
{
  protected:
    Vector3Test() {}
};

TEST_F(Vector3Test, DistanceTest)
{
    Vector3 p0(0, 0, 0);
    Vector3 p1(1, 1, 1);
    Vector3 p2(-10, -20, -30);

    //Vector3!
    Vector3 p3(-5, 9, 1);

    EXPECT_EQ(std::sqrt(3), p0.GetDistance(p1));
    EXPECT_EQ(std::sqrt(100 + 400 + 900), p0.GetDistance(p2));

    EXPECT_EQ(3, p0.GetDistanceSquared(p1));
    EXPECT_EQ(1400, p0.GetDistanceSquared(p2));

    EXPECT_EQ(std::sqrt(11.0 * 11.0 + 21.0 * 21.0 + 31.0 * 31.0), p1.GetDistance(p2));
}

TEST_F(Vector3Test, SumAndDifferenceTest)
{
    Vector3 p0(-3, -2, 10);
    Vector3 p1(-1, 0, 19);
    Vector3 p2(1, 2, 31);

    auto d1 = p2 - p1;
    EXPECT_EQ(2, d1.GetX());
    EXPECT_EQ(2, d1.GetY());
    EXPECT_EQ(31 - 19, d1.GetZ());

    auto d2 = p2 - p0;
    EXPECT_EQ(4, d2.GetX());
    EXPECT_EQ(4, d2.GetY());
    EXPECT_EQ(31 - 10, d2.GetZ());

    auto s1 = p0 + p1;
    EXPECT_EQ(-4, s1.GetX());
    EXPECT_EQ(-2, s1.GetY());
    EXPECT_EQ(29, s1.GetZ());

    auto s2 = p1 + p0;
    EXPECT_EQ(-4, s2.GetX());
    EXPECT_EQ(-2, s2.GetY());
    EXPECT_EQ(29, s2.GetZ());

    auto m1 = p0.GetMeanWith(p1);
    EXPECT_EQ(-2, m1.GetX());
    EXPECT_EQ(-1, m1.GetY());
    EXPECT_EQ(14.5, m1.GetZ());
}
}  //namespace mpmca::predict::inertial::testing