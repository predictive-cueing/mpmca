/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/vehicle_state.hpp"

#include <gtest/gtest.h>

#include <cmath>

#include "mpmca/constants.hpp"

namespace mpmca::predict::inertial::testing {

class VehicleStateTest : public ::testing::Test
{
  protected:
    VehicleStateTest() {}
};

TEST_F(VehicleStateTest, PredictTest)
{
    double dt = 1.0;
    VehicleState x;
    x.SetU(10.0);
    x.SetV(1.0);

    for (double psi = 0; psi < constants::kPi * 2.0; psi += 0.1) {
        x.SetPsi(psi);

        EXPECT_NEAR((x.GetU() * std::cos(x.GetPsi()) - x.GetV() * std::sin(x.GetPsi())) * dt,
                    x.PredictPosition(dt).GetX(), 1e-5);
        EXPECT_NEAR((x.GetU() * std::sin(x.GetPsi()) + x.GetV() * std::cos(x.GetPsi())) * dt,
                    x.PredictPosition(dt).GetY(), 1e-5);
    }
}

TEST_F(VehicleStateTest, PredictZeroVelocityTest)
{
    double dt = 1.0;
    VehicleState x;

    for (double psi = 0; psi < constants::kPi * 2.0; psi += 0.1) {
        x.SetPsi(psi);

        EXPECT_NEAR(0.0, x.PredictPosition(dt).GetX(), 1e-15);
        EXPECT_NEAR(0.0, x.PredictPosition(dt).GetY(), 1e-15);
        EXPECT_NEAR(0.0, x.PredictPosition(dt).GetZ(), 1e-15);
        EXPECT_NEAR(0.0, x.PredictPosition(dt).GetPhi(), 1e-15);
        EXPECT_NEAR(0.0, x.PredictPosition(dt).GetTheta(), 1e-15);
        EXPECT_NEAR(x.GetPsi(), x.PredictPosition(dt).GetPsi(), 1e-15);
    }
}
}  //namespace mpmca::predict::inertial::testing