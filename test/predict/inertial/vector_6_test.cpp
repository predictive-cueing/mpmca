/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/vector_6.hpp"

#include <gtest/gtest.h>

#include <cmath>

#include "mpmca/constants.hpp"

namespace mpmca::predict::inertial::testing {

class Vector6Test : public ::testing::Test
{
  protected:
    Vector6Test() {}
};

TEST_F(Vector6Test, DistanceTest)
{
    Vector6 p0(0, 0);
    Vector6 p1(1, 1);

    EXPECT_TRUE(p0.GetDistance(p1) == std::sqrt(2));
}

TEST_F(Vector6Test, HeadingTest)
{
    Vector6 p0(0, 0);
    Vector6 p1(1, 0);
    Vector6 p2(0, 1);
    Vector6 p3(0, -1);

    EXPECT_TRUE(p0.GetHeadingFrom(p1) == 0);
    EXPECT_TRUE(p0.GetHeadingFrom(p2) == constants::kPi_2);
    EXPECT_TRUE(p0.GetHeadingFrom(p3) == -constants::kPi_2);
}
}  //namespace mpmca::predict::inertial::testing