/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/interpolatable.hpp"

#include <gtest/gtest.h>

namespace mpmca::predict::inertial::testing {

class InterpolatableTest : public ::testing::Test
{
  protected:
    InterpolatableTest() {}
};
TEST_F(InterpolatableTest, InterpolatableBaseClassTest)
{
    Interpolatable test;
    test.SetValue(5.);
    EXPECT_EQ(5.0, test.GetValue());
    EXPECT_EQ(0, test.GetNumPointsRequiredBehind());
    EXPECT_EQ(0, test.GetNumPointsRequiredAhead());
    EXPECT_THROW(test.GetInterpolated(1.0), std::runtime_error);
    EXPECT_THROW(test.Derivative(1.0), std::runtime_error);
    EXPECT_THROW(test.GetAdjacentPoint(0, 0.0, {5.0}), std::runtime_error);
    EXPECT_THROW(test.Finalize(), std::runtime_error);
}
}  //namespace mpmca::predict::inertial::testing
