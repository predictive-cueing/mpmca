/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/simulation.hpp"

#include <gtest/gtest.h>

#include "mpmca/constants.hpp"
#include "mpmca/predict/inertial/car_minimal.hpp"
#include "mpmca/predict/inertial/driver_minimal.hpp"
#include "mpmca/predict/inertial/inertial_output.hpp"
#include "mpmca/predict/inertial/path_factory.hpp"
#include "mpmca/utilities/clock.hpp"

namespace mpmca::predict::inertial::testing {

class SimulationTest : public ::testing::Test
{
  protected:
    using Car = CarMinimal;
    using Human = DriverMinimal;
    using Output = InertialOutput;

    utilities::ConfigurationPtr m_config;
    utilities::Clock m_clock;
    utilities::Horizon m_horizon;
    PathPtr m_path;
    Simulation<Human, Car, InertialOutput> m_sim;

    Car::SignalVector m_car_init;
    Human::SignalVector m_human_init;

    SimulationTest()
        : m_config(utilities::Configuration::CreateRootConfig("simulation_test"))
        , m_clock(std::chrono::milliseconds(10))
        , m_horizon(m_clock, 1000)
        , m_path(GetPath())
        , m_sim(m_config, m_horizon, "TestSimulation")
        , m_car_init(Car::SignalVector::Zero())
        , m_human_init(Human::SignalVector::Zero())
    {
        m_sim.SetPathPtr(m_path);

        m_sim.GetCarModel().car_C_distance_rear_axle_m.SetNextValue(1.76125);

        m_sim.GetHumanModel().gravity_m_s2.SetNextValue(9.81);
        m_sim.GetHumanModel().vehicle_mass_kg.SetNextValue(1894.0);
        m_sim.GetHumanModel().car_S_aero_force_kg_m.SetNextValue(0.3777);
        m_sim.GetHumanModel().car_S_brake_force_N_fraction.SetNextValue(43377.2);
        m_sim.GetHumanModel().car_M1D_gear_ratio_1_m.SetNextValue(
            {{-1.0, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0},
             {0.0, 0.0, 42.90, 27.45, 18.38, 14.75, 11.27, 8.58, 7.05, 5.49}});
        m_sim.GetHumanModel().car_M2D_engine_torque_Nm.SetNextValue(
            {{0, 0.1, 1.0},
             {104.72, 130.90, 209.44, 418.88, 462.55, 523.60, 544.54, 575.96, 607.38, 628.32, 654.50, 680.68, 706.86,
              733.04},
             {{-24.2, -26.4, -25.2, -28.8, -30.3, -31.8, -33.0, -34.2, -35.5, -36.8, -38.1, -39.4, -39.4, -39.40},
              {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
              {250.0, 350.0, 400.0, 400.0, 400.0, 353.0, 340.0, 321.0, 305.0, 294.0, 280.0, 260.0, 240.0, 218.0}}});

        m_sim.GetCarModel().SetSignalValue("x", m_car_init, 0);
        m_sim.GetCarModel().SetSignalValue("y", m_car_init, 0);
        m_sim.GetCarModel().SetSignalValue("u", m_car_init, 7.5);
        m_sim.GetCarModel().SetSignalValue("psi", m_car_init, constants::kPi_2);
        m_sim.GetCarModel().SetSignalValue("gear", m_car_init, 3);

        m_sim.SetPathPtr(m_path);
    }

    PathPtr GetPath()
    {
        return PathFactory::CreatePathCircle(0.1, 0, 3, 2000, 0, 0, false, "SimulationCircleTestPath");
    }
};

TEST_F(SimulationTest, CompileTest)
{
    m_sim.GetCarModel().Init(m_car_init);
    m_sim.Simulate();
}
}  //namespace mpmca::predict::inertial::testing