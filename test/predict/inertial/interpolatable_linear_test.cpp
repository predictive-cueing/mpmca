/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/interpolatable_linear.hpp"

#include <gtest/gtest.h>

#include <cmath>

namespace mpmca::predict::inertial::testing {

class InterpolatableLinearTest : public ::testing::Test
{
  protected:
    InterpolatableLinearTest() {}
};
TEST_F(InterpolatableLinearTest, SingleInterpolatableLinearTest)
{
    InterpolatableLinear test(5.0);
    EXPECT_EQ(5.0, test.GetValue());

    test.GetAdjacentPoint(-1, 0.1, {-5.0});
    test.GetAdjacentPoint(1, 0.1, {10.0});
    test.Finalize();

    /**
     * @brief
     *
     * s      		-0.1 	0.0     0.1
     * value  		-5.0    5.0    10.0
     * difference       10.0    5.0
     * dv/ds           100.0   50.0
     */

    EXPECT_NEAR(test.GetInterpolated(-1.0), -5.0, 1e-10);
    EXPECT_NEAR(test.GetInterpolated(-0.5), 0.0, 1e-10);
    EXPECT_NEAR(test.GetInterpolated(0.0), 5.0, 1e-10);
    EXPECT_NEAR(test.GetInterpolated(0.5), 7.5, 1e-10);
    EXPECT_NEAR(test.GetInterpolated(1.0), 10.0, 1e-10);

    EXPECT_NEAR(test.Derivative(-1.0), 100.0, 1e-10);
    EXPECT_NEAR(test.Derivative(-0.5), 100.0, 1e-10);
    EXPECT_NEAR(test.Derivative(0.0), 50.0, 1e-10);
    EXPECT_NEAR(test.Derivative(0.5), 50.0, 1e-10);
    EXPECT_NEAR(test.Derivative(1.0), 50.0, 1e-10);
}
TEST_F(InterpolatableLinearTest, MultipleInterpolatableLinearTest)
{
    int N = 10;
    double ds = 0.1;
    std::vector<InterpolatableLinear> vector;
    vector.reserve(N);

    for (int i = 0; i < N; ++i)
        vector.push_back(InterpolatableLinear(std::sin((double)i)));

    vector[0].GetAdjacentPoint(1, ds, vector[1]);
    vector.back().GetAdjacentPoint(-1, ds, vector[N - 2]);
    for (int i = 1; i < N - 1; ++i) {
        vector[i].GetAdjacentPoint(-1, ds, vector[i - 1]);
        vector[i].GetAdjacentPoint(1, ds, vector[i + 1]);
    }

    for (auto& vec : vector)
        vec.Finalize();

    for (int i = 0; i < N - 1; ++i) {
        for (double fraction = 0.0; fraction <= 1.0; fraction += 0.001) {
            EXPECT_NEAR(vector[i].GetInterpolated(fraction), vector[i + 1].GetInterpolated(-1 + fraction), 1e-8);
            EXPECT_NEAR(vector[i].Derivative(fraction), vector[i + 1].Derivative(-1 + fraction), 1e-8);
        }
    }
}
}  //namespace mpmca::predict::inertial::testing
