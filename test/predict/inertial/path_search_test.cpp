/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <gtest/gtest.h>

#include "mpmca/constants.hpp"
#include "mpmca/predict/inertial/path.hpp"
#include "mpmca/predict/inertial/path_factory.hpp"
#include "mpmca/predict/inertial/path_searcher_segmented.hpp"

namespace mpmca::predict::inertial::testing {

class PathSearchTest : public ::testing::Test
{
  protected:
    PathPtr m_path;

    PathSearchTest()
        : m_path(GetPath())
    {
    }

    PathPtr GetPath()
    {
        // we create a u-shaped path
        std::vector<PathPoint> track_pose;
        int N_long = 2000;
        int N_long2 = 1420;
        int N_short = 301;
        double ds = 0.1;

        for (int i = 0; i < N_long; ++i)
            track_pose.push_back({(N_long - i) * ds, (N_short - 1) * ds / 2., 0.0, 0.0, 0.0, -constants::kPi});

        for (int i = 0; i < N_short; ++i)
            track_pose.push_back({0., ((N_short - 1) / 2 - i) * ds, 0.0, 0.0, 0.0, -constants::kPi_2});

        for (int i = 0; i < N_long2; ++i)
            track_pose.push_back({(i + 1) * ds, -(N_short - 1) * ds / 2., 0.0, 0.0, 0.0, 0.0});

        PathFactory factory(track_pose, false);
        factory.SetOmitHeadingCheck(true);

        return factory.GetPreparedPath("PathSearchTest.FindClosestPointTest");
    }
};

TEST_F(PathSearchTest, FindClosestPointTest)
{
    PathSearcherSegmented searcher;
    for (int i = 0; i < 10; ++i) {
        Vector6 point = {50. - 5 + i, -1. * i + 5.};
        auto pi = searcher.FindIndexGlobal(m_path, point);
        double distance = m_path->GetPathPointAt(pi).GetDistance(point);
        EXPECT_TRUE(distance <= 15.);
    }
}

}  //namespace mpmca::predict::inertial::testing