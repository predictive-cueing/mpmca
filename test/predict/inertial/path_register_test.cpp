/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/path_register.hpp"

#include <gtest/gtest.h>

#include "mpmca/predict/inertial/path.hpp"
#include "mpmca/predict/inertial/path_factory.hpp"

namespace mpmca::predict::inertial::testing {

class PathRegisterTest : public ::testing::Test
{
  protected:
    PathRegisterTest() {}
};

TEST_F(PathRegisterTest, ConstructorTest)
{
    PathPtr direct_pointer = PathFactory::CreatePathLine(1, 0, 0, 10, 10, 1, 1, "FrankPath");
    PathPtr register_pointer = PathRegister::Instance()->GetPath("FrankPath");

    std::cout << direct_pointer->GetName() << std::endl;
    std::cout << register_pointer->GetName() << std::endl;
}
}  //namespace mpmca::predict::inertial::testing