/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/path_segment.hpp"

#include <gtest/gtest.h>

namespace mpmca::predict::inertial::testing {

class PathSegmentTest : public ::testing::Test
{
  protected:
    PathSegmentTest() {}
};
TEST_F(PathSegmentTest, ConstructorTest)
{
    PathSegment ps(0, 1000);
    EXPECT_EQ(1001, ps.GetNumberOfPoints());
}
TEST_F(PathSegmentTest, SortTest)
{
    std::vector<PathSegment> ps_vector;

    double min_dist = 60.0;
    int vector_index_minimum = 8;
    int segment_length = 100;

    for (int i = 0; i < 15; ++i) {
        ps_vector.push_back(PathSegment(segment_length * i, segment_length * (i + 1)));
        ps_vector.back().IsMinimumPointFound(
            segment_length / 2 * i, min_dist + (double)(i - vector_index_minimum) * (i - vector_index_minimum));
    }

    sort(ps_vector.begin(), ps_vector.end());

    EXPECT_NEAR(min_dist, ps_vector.front().GetMinimumDistance(), 1e-15);
    EXPECT_EQ(vector_index_minimum * (segment_length / 2), ps_vector.front().GetMinimumIndex());
}
}  //namespace mpmca::predict::inertial::testing
