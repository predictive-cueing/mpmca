/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <gtest/gtest.h>

#include "mpmca/predict/inertial/car_minimal.hpp"
#include "mpmca/utilities/clock.hpp"
namespace mpmca::predict::inertial::testing {

class SignalsTest : public ::testing::Test
{
  protected:
    utilities::ConfigurationPtr m_config;
    utilities::Clock m_clock;
    utilities::Horizon m_horizon;
    CarMinimal m_car_model;

    SignalsTest()
        : m_config(utilities::Configuration::CreateRootConfig("signal_test"))
        , m_clock(std::chrono::milliseconds(10))
        , m_horizon(m_clock, 1000)
        , m_car_model(m_config, m_horizon, "test_car")
    {
    }
};

TEST_F(SignalsTest, CompileTest)
{
    m_car_model.x.SetValue(4, 3.0);
    m_car_model.x.SetValue(2, 15.0);

    CarMinimal::SignalVector y = CarMinimal::SignalVector::Zero();

    y[2] = 1.4;
    m_car_model.SetSignalValue("psi", y, 3);
}
}  //namespace mpmca::predict::inertial::testing