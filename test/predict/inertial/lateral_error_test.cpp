/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <gtest/gtest.h>

#include "mpmca/constants.hpp"
#include "mpmca/predict/inertial/path_factory.hpp"
#include "mpmca/predict/inertial/path_searcher_grid_search.hpp"
#include "mpmca/predict/inertial/path_searcher_segmented.hpp"

namespace mpmca::predict::inertial::testing {

class LateralErrorTest : public ::testing::Test
{
  protected:
    LateralErrorTest() {}
};

TEST_F(LateralErrorTest, SingleTest)
{
    PathPtr path =
        PathFactory("StraightPath", PathFactory::GetStraightPathPointVector({0.0, 0.0}, {10.0, 0.0}, 0.1), false)
            .GetPreparedPath("LateralErrorTest.SingleTest");

    VehicleState state;
    state.SetX(1.0);
    state.SetY(-0.54);
    state.SetU(10);
    state.SetV(-1);
    state.SetPsi(constants::kPi);

    PathSearcherSegmented searcher;
    searcher.FindIndexGlobal(path, state);
}
TEST_F(LateralErrorTest, LateralErrorTest)
{
    double ds = 0.1;
    double l = 10.0;
    int kk = 0;
    for (double t = -3; t < 3; t += 0.5) {
        for (double psi = 0; psi < 4 * constants::kPi; psi += 0.1) {
            PathPtr path = PathFactory("StraightPath",
                                       PathFactory::GetStraightPathPointVector(
                                           {0.0, 0.0}, {l * std::cos(psi), l * std::sin(psi)}, ds),
                                       false)
                               .GetPreparedPath("LateralErrorTest" + std::to_string(kk++));
            PathSearcherSegmented se;

            for (double d = 1e-5; d < l * 0.999999; d += 0.1) {
                double xd = d * std::cos(psi) - t * std::sin(psi);
                double yd = d * std::sin(psi) + t * std::cos(psi);
                se.FindIndexGlobal(path, {xd, yd});

                EXPECT_NEAR(t,
                            path->GetPathPointAt(se.GetLastPathIndex())
                                .GetLateralError(se.GetLastPathIndex().GetFraction(), {xd, yd}),
                            1e-9);
                EXPECT_NEAR(t, path->GetPathPointAt(se.GetLastPathIndex()).GetLateralError({xd, yd}), 1e-9);
            }
        }
    }
}
TEST_F(LateralErrorTest, LateralErrorDotTest)
{
    double ds = 0.1;
    double l = 10.0;

    int kk = 0;
    for (double t = -3; t < 3; t += 0.8) {
        for (double psi = 0; psi < 2 * constants::kPi; psi += 0.4) {
            PathPtr path = PathFactory("StraightPath",
                                       PathFactory::GetStraightPathPointVector(
                                           {0.0, 0.0}, {l * std::cos(psi), l * std::sin(psi)}, ds),
                                       false)
                               .GetPreparedPath("LateralErrorTest.lateralErrorDotTest." + std::to_string(kk++));
            PathSearcherSegmented searcher;

            for (double d = 0.0001; d < l * 0.9999; d += 0.9) {
                for (double d_psi = -0.4; d_psi < 0.4; d_psi += 0.1) {
                    for (double u = 0.0; u < 50; u += 5) {
                        for (double v = -1; v < 1; v += 0.4) {
                            double xd = d * std::cos(psi) - t * std::sin(psi);
                            double yd = d * std::sin(psi) + t * std::cos(psi);
                            VehicleState x_state;
                            x_state.SetX(xd);
                            x_state.SetY(yd);
                            x_state.SetPsi(psi + d_psi);
                            x_state.SetU(u);
                            x_state.SetV(v);

                            searcher.FindIndexGlobal(path, x_state);

                            double lat_e = path->GetPathPointAt(searcher.GetLastPathIndex()).GetLateralError(x_state);
                            double lat_e_dot =
                                path->GetPathPointAt(searcher.GetLastPathIndex()).GetLateralErrorDot(x_state);
                            double lat_e_dot2 =
                                path->GetPathPointAt(searcher.GetLastPathIndex())
                                    .GetLateralErrorDot(searcher.GetLastPathIndex().GetFraction(), x_state);

                            double lat_e_dot_exp = u * std::sin(d_psi) + v * std::cos(d_psi);

                            EXPECT_NEAR(t, lat_e, 1e-9);
                            EXPECT_NEAR(lat_e_dot_exp, lat_e_dot, 1e-3);
                            EXPECT_NEAR(lat_e_dot_exp, lat_e_dot2, 1e-3);
                        }
                    }
                }
            }
        }
    }
}
TEST_F(LateralErrorTest, SingleLateralErrorDotTest)
{
    double ds = 0.1;
    double l = 10.0;
    double lat_e = 0;
    double lat_e_dot = 0;
    double t = 0.2;
    double psi = 0;
    double d = 0.001;
    double d_psi = -0.4;
    double u = 0;
    double v = -1;

    PathPtr path =
        PathFactory("StraightPath",
                    PathFactory::GetStraightPathPointVector({0.0, 0.0}, {l * std::cos(psi), l * std::sin(psi)}, ds),
                    false)
            .GetPreparedPath("LateralErrorTest.singleLateralErrorDotTest");
    PathSearcherSegmented searcher;

    double xd = d * std::cos(psi) - t * std::sin(psi);
    double yd = d * std::sin(psi) + t * std::cos(psi);

    VehicleState x_state;
    x_state.SetX(xd);
    x_state.SetY(yd);
    x_state.SetPsi(psi + d_psi);
    x_state.SetU(u);
    x_state.SetV(v);

    searcher.FindIndexGlobal(path, x_state);

    lat_e = path->GetPathPointAt(searcher.GetLastPathIndex()).GetLateralError(x_state);
    lat_e_dot = path->GetPathPointAt(searcher.GetLastPathIndex()).GetLateralErrorDot(x_state);

    double lat_e_dot_exp = u * std::sin(d_psi) + v * std::cos(d_psi);

    EXPECT_NEAR(t, lat_e, 1e-9);
    EXPECT_NEAR(lat_e_dot_exp, lat_e_dot, 1e-2);
}
TEST_F(LateralErrorTest, LateralErrorCircleTest)
{
    double ds = 0.1;
    double R = 10;
    int N = (int)ceil(2 * constants::kPi * R / ds);
    PathPtr path = PathFactory("StraightPath", PathFactory::GetCirclePathPointVector(N, N + 1, R), true)
                       .GetPreparedPath("LateralErrorTest.lateralErrorCircleTest");
    PathSearcherGridSearch searcher;

    for (double t = -0.5; t < 0.5; t += 0.04) {
        double R_test = R - t;  // must be R minus t, because t is defined positive in positive y direction

        for (double psi = -4 * constants::kPi; psi < 4 * constants::kPi; psi += 0.2) {
            Vector6 point = {R_test * std::cos(psi), R_test * std::sin(psi)};
            auto pi = searcher.FindIndexGlobal(path, point);
            EXPECT_GE(pi.GetFraction(), -0.5);
            EXPECT_LE(pi.GetFraction(), 0.5);
            EXPECT_NEAR(t, path->GetPathPointAt(pi).GetLateralError(point), 1e-3)
                << "Distance is " << path->GetInterpolated(pi).GetDistance(point) << " for psi " << psi;
        }
    }
}
TEST_F(LateralErrorTest, LateralErrorDotCircleTest)
{
    double ds = 0.1;
    double R = 3;
    int N = (int)ceil(2 * constants::kPi * R / ds);
    PathPtr path = PathFactory("StraightPath", PathFactory::GetCirclePathPointVector(N, N + 1, R), true)
                       .GetPreparedPath("LateralErrorTest.lateralErrorDotCircleTest");
    PathSearcherSegmented searcher;

    double lat_e;
    double lat_e_dot;

    for (double t = -0.5; t < 0.5; t += 0.04) {
        double R_test = R - t;  // must be R minus t, because t is defined positive in positive y direction

        for (double psi = -4 * constants::kPi; psi < 4 * constants::kPi; psi += 0.2) {
            for (double d_psi = -0.4; d_psi < 0.4; d_psi += 0.1) {
                for (double u = 0.0; u < 50; u += 5) {
                    for (double v = -1; v < 1; v += 0.4) {
                        VehicleState x_state;
                        x_state.SetX(R_test * std::cos(psi));
                        x_state.SetY(R_test * std::sin(psi));
                        x_state.SetPsi(psi + constants::kPi_2 + d_psi);
                        x_state.SetU(u);
                        x_state.SetV(v);

                        searcher.FindIndexGlobal(path, x_state);

                        lat_e = path->GetPathPointAt(searcher.GetLastPathIndex()).GetLateralError(x_state);
                        lat_e_dot = path->GetPathPointAt(searcher.GetLastPathIndex()).GetLateralErrorDot(x_state);

                        double lat_e_dot_exp = u * std::sin(d_psi) + v * std::cos(d_psi);

                        EXPECT_NEAR(t, lat_e, 1e-3);
                        EXPECT_NEAR(lat_e_dot_exp, lat_e_dot, 1e-2);
                    }
                }
            }
        }
    }
}
TEST_F(LateralErrorTest, LateralErrorContinuityTest)
{
    double ds = 0.1;
    double R = 3;
    int N = (int)ceil(2 * constants::kPi * R / ds);
    PathPtr path = PathFactory("StraightPath", PathFactory::GetCirclePathPointVector(N, N + 1, R), true)
                       .GetPreparedPath("LateralErrorTest.lateralErrorContinuityTest");
    PathSearcherSegmented searcher;

    double lat_e;
    double lat_e_dot;
    double t = 0;
    double R_test = R - t;  // must be R minus t, because t is defined positive in positive y direction
    double psi = 1 * constants::kPi_4;
    double d_psi = 0;
    double u = 10;
    double v = 0;

    double lat_e_prev = 0;
    (void)lat_e_prev;
    double lat_e_dot_prev = 0;
    (void)lat_e_dot_prev;
    for (double d = -0.1; d < 0.1; d += 0.01) {
        VehicleState x_state;
        x_state.SetX(R_test * std::cos(psi) + d * std::cos(psi + constants::kPi_2));
        x_state.SetY(R_test * std::sin(psi) + d * std::sin(psi + constants::kPi_2));
        x_state.SetPsi(psi + constants::kPi_2 + d_psi);
        x_state.SetU(u);
        x_state.SetV(v);

        searcher.FindIndexGlobal(path, x_state);
        lat_e = path->GetPathPointAt(searcher.GetLastPathIndex()).GetLateralError(x_state);
        lat_e_dot = path->GetPathPointAt(searcher.GetLastPathIndex()).GetLateralErrorDot(x_state);

        lat_e_prev = lat_e;
        lat_e_dot_prev = lat_e_dot;
    }
}
}  //namespace mpmca::predict::inertial::testing
