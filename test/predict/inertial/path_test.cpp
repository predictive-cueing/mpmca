/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/path.hpp"

#include <gtest/gtest.h>

#include "mpmca/constants.hpp"
#include "mpmca/predict/inertial/path_factory.hpp"

namespace mpmca::predict::inertial::testing {

class PathTest : public ::testing::Test
{
  protected:
    PathTest() {}
};

TEST_F(PathTest, ConstructorTest)
{
    /** Just checking that the constructor properties are actually used in the object
     *
     */

    std::vector<PathPoint> path_points;
    int N = 100;

    for (int i = 0; i < N; ++i)
        path_points.push_back(PathPoint((double)i / 10.0, 0.0, 0, 0, 0, 0));

    PathPtr path = PathFactory("SomeRandomFactoryName", path_points, false).GetPreparedPath("SomeRandomName");

    EXPECT_TRUE(path->GetName().compare("SomeRandomName") == 0);
    EXPECT_FALSE(path->IsClosed());

    EXPECT_EQ(0, path->GetStartIndex());
    EXPECT_EQ(N - 1, path->GetEndIndex());
    EXPECT_EQ(N, path->GetNumPathPoints());

    EXPECT_NEAR(0.1, path->GetSmallestApproximatePointToPointDistance(), 1e-8);
    // there is no point in checking the unique_id, because it will depend on how many paths were created before this
    // path EXPECT_EQ(0, path->GetUniqueId());
}

TEST_F(PathTest, OpenPathPointGettersTest)
{
    /** Check that for an open path, you can just get the points that are between 0 and the length of the path
     */

    std::vector<PathPoint> path_points;
    int N = 100;

    for (int i = 0; i < N; ++i)
        path_points.push_back(PathPoint((double)i / 10.0, 0.0, 0, 0, 0, 0));

    PathPtr path = PathFactory("openPathPointGettersTestPath", path_points, false).GetPreparedPath();

    EXPECT_THROW(path->GetPathPointAt(-100), std::runtime_error);
    EXPECT_THROW(path->GetPathPointAt(-1), std::runtime_error);
    EXPECT_NO_THROW(path->GetPathPointAt(0));
    EXPECT_NO_THROW(path->GetPathPointAt(99));
    EXPECT_THROW(path->GetPathPointAt(100), std::runtime_error);
    EXPECT_NEAR(0.1, path->GetSmallestApproximatePointToPointDistance(), 1e-8);

    for (int i = 0; i < N; ++i)
        EXPECT_EQ(i, path->GetPathPointAt(i).GetIndex());
}

TEST_F(PathTest, FaultyClosedPathPointGettersTest)
{
    // Check that if you claim the path is closed, but actually the first and last point do not overlap, an error is
    // thrown.
    EXPECT_THROW(PathFactory("faultyClosedPath", PathFactory::GetCirclePathPointVector(1000, 999, 40.0), true)
                     .GetPreparedPath("faultyClosedPath"),
                 std::runtime_error);

    PathFactory("correctlyClosedPath", PathFactory::GetCirclePathPointVector(1000, 1001, 40.0), true)
        .GetPreparedPath("correctlyClosedPath");
    // EXPECT_NO_THROW(Path * path = PathFactory("SomeRandomName", PathFactory::GetCirclePathPointVector(1000,
    // 1001, 40.0), true).GetPreparedPath());
}

TEST_F(PathTest, ClosedPathPointGettersTest)
{
    // check that for a closed path, you indeed get the point you expect to get, if you call At() with a number outside
    // the range (0,N).
    int N = 1000;
    double R = 40.0;
    auto path_points = PathFactory::GetCirclePathPointVector(N, N + 1, R);

    PathPtr path = PathFactory("SomeRandomName", path_points, true).GetPreparedPath("closedPathPointGettersTest");

    for (int i = -10 * N; i < 10 * N; i += 10)
        EXPECT_NO_THROW(path->GetPathPointAt(i));

    EXPECT_EQ(N - 1, path->GetPathPointAt(-1).GetIndex());
    EXPECT_NEAR(2 * constants::kPi * R / N, path->GetSmallestApproximatePointToPointDistance(), 1e-6);

    for (int i = 0; i < N; ++i) {
        EXPECT_EQ(i, path->GetPathPointAt(i).GetIndex());
        EXPECT_EQ(i, path->GetPathPointAt(i + N).GetIndex());  // check the modulus
    }
}
TEST_F(PathTest, StraightLineCircleCalculationTest)
{
    // check that points on a straight line do not have a circle. Check this for many headings.
    int N = 100;
    double ds = 0.1;
    double l = N * ds;

    for (double j = 0; j < 4 * constants::kPi; j += 0.01) {
        std::vector<PathPoint> path_points;

        for (int i = 0; i < N; ++i)
            path_points.push_back(
                PathPoint((double)i / l * std::cos(j), (double)i / l * std::sin(j), 0.0, 0.0, 0.0, j));

        PathPtr path = PathFactory("SomeRandomName", path_points, false)
                           .GetPreparedPath("straightLineCircleCalculationTest" + std::to_string(j));

        for (int i = 0; i < path->GetNumPathPoints(); ++i)
            EXPECT_FALSE(path->GetPathPointAt(i).HasCircle());
    }
}
TEST_F(PathTest, VeryLargeCircleTest)
{
    double R = 1e8;
    double x_center = 3.124;
    double y_center = -1.12882;
    double ds = 0.1;
    int N = 10;

    PathPtr path =
        PathFactory("smallCircle", PathFactory::GetCircularPathPointVector(N, ds, R, x_center, y_center), false)
            .GetPreparedPath("veryLargeCircleTest");

    for (int i = 0; i < N; ++i)
        EXPECT_FALSE(path->GetPathPointAt(i).HasCircle()) << "Failed for " << i;
}

TEST_F(PathTest, ClosedPathCircleCalculationTest)
{
    double R = 3.0;
    double x_center = 31243252.124;
    double y_center = -123414.12882;

    double ds = 0.1;
    int N = 10;

    PathPtr path =
        PathFactory("smallCircle", PathFactory::GetCircularPathPointVector(N, ds, R, x_center, y_center), false)
            .GetPreparedPath("closedPathCircleCalculationTest");

    EXPECT_FALSE(path->GetPathPointAt(0).HasCircle());
    EXPECT_FALSE(path->GetPathPointAt(N - 1).HasCircle());

    for (int i = 1; i < N - 1; ++i)
        EXPECT_TRUE(path->GetPathPointAt(i).HasCircle());
}

}  //namespace mpmca::predict::inertial::testing
