/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <gtest/gtest.h>

#include "mpmca/predict/inertial/path_factory.hpp"
#include "mpmca/predict/inertial/path_searcher_grid_search.hpp"
#include "mpmca/predict/inertial/path_searcher_segmented.hpp"
namespace mpmca::predict::inertial::testing {

class PathContinuityTest : public ::testing::Test
{
  protected:
    PathContinuityTest() {}
};
TEST_F(PathContinuityTest, ContinuousPsiTest)
{
    double R = 10;
    double ds = 0.1;

    PathPtr path = PathFactory("circle", PathFactory::CreatePathPointVectorCircle(ds, -0.1, 0.1, R, 0.0, 0.0), false)
                       .GetPreparedPath("circle");

    double start = path->GetStartIndex();
    double end = path->GetEndIndex();
    double step = 0.01;
    double psi_previous = 0;

    for (double at = start; at < end; at += step) {
        PathIndex pi((int)round(at), at - std::round(at));
        double psi_current = path->GetPathPointAt(pi).GetInterpolated(pi.GetFraction()).GetPsi();
        psi_previous = psi_current;
        (void)psi_previous;
        // this is not really a test, but we might turn it into a test later.
    }
}
}  //namespace mpmca::predict::inertial::testing
