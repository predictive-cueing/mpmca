/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/inertial/interpolatable_cubic_spline.hpp"

#include <gtest/gtest.h>

#include "mpmca/constants.hpp"
#include "mpmca/utilities/math.hpp"

namespace mpmca::predict::inertial::testing {
class InterpolatableCubicSplineTest : public ::testing::Test
{
  protected:
    InterpolatableCubicSplineTest() {}
};
TEST_F(InterpolatableCubicSplineTest, SingleInterpolatableCubicSplineTest)
{
    InterpolatableCubicSpline test(2.0);
    EXPECT_EQ(2.0, test.GetValue());

    test.GetAdjacentPoint(-2, 2, {0.0});
    test.GetAdjacentPoint(-1, 1, {1.0});
    test.GetAdjacentPoint(1, 1, {1.0});
    test.GetAdjacentPoint(2, 2, {0.0});
    // we set a previously set point again
    EXPECT_THROW(test.GetAdjacentPoint(-1, 2, {0.0}), std::runtime_error);
    test.Finalize();

    /**
     * @brief
     *
     * s      		 -2.0 	-1.0   0.0   1.0    2.0
     * value  		  0.0    1.0   2.0   1.0    0.0
     * derivative     	     1.0   0.0   -1.0
     *
     * fraction >= 0
     * p0 = 2.0
     * m0 = 0.0
     * p1 = 1.0
     * m1 = -1.0
     *
     * h00  =  2*t^3 - 3*t^2 +     + 1
     * h10  =    t^3 - 2*t^2 + 1*t
     * h01  = -2*t^3 + 3*t^2
     * h11  =    t^3 -   t^2
     *
     * h00' =          6*t^2 - 6*t
     * h10' =          3*t^2 - 4*t + 1
     * h01' =         -6*t^2 + 6*t
     * h11' =          3*t^2 - 2*t
     *
     * v    = p0*h00  + m0*h10  + p1*h01  + m1*h11
     * v'   = p0*h00' + m0*h10' + p1*h01' + m1*h11'
     */

    auto h00d = [](double t) { return 6.0 * t * t - 6.0 * t; };
    auto h10d = [](double t) { return 3.0 * t * t - 4.0 * t + 1.0; };
    auto h01d = [](double t) { return -6.0 * t * t + 6.0 * t; };
    auto h11d = [](double t) { return 3.0 * t * t - 2.0 * t; };
    auto vd = [h00d, h10d, h01d, h11d](double p0, double m0, double p1, double m1, double t) {
        return h00d(t) * p0 + h10d(t) * m0 + h01d(t) * p1 + h11d(t) * m1;
    };

    EXPECT_NEAR(test.GetInterpolated(-1.0), 1.0, 1e-10);
    EXPECT_NEAR(test.GetInterpolated(-0.5), 1.625, 1e-10);
    EXPECT_NEAR(test.GetInterpolated(0.0), 2.0, 1e-10);
    EXPECT_NEAR(test.GetInterpolated(0.5), 1.625, 1e-10);
    EXPECT_NEAR(test.GetInterpolated(1.0), 1.0, 1e-10);

    EXPECT_NEAR(test.Derivative(-1.00), 1.0, 1e-10);
    EXPECT_NEAR(test.Derivative(-0.75), vd(1.0, 1.0, 2.0, 0.0, 0.25), 1e-10);
    EXPECT_NEAR(test.Derivative(-0.50), vd(1.0, 1.0, 2.0, 0.0, 0.50), 1e-10);
    EXPECT_NEAR(test.Derivative(0.00), 0.0, 1e-10);
    EXPECT_NEAR(test.Derivative(0.50), vd(2.0, 0.0, 1.0, -1.0, 0.50), 1e-10);
    EXPECT_NEAR(test.Derivative(0.75), vd(2.0, 0.0, 1.0, -1.0, 0.75), 1e-10);
    EXPECT_NEAR(test.Derivative(1.00), -1.0, 1e-10);
}
TEST_F(InterpolatableCubicSplineTest, DifferenceFunctionTest)
{
    InterpolatableCubicSpline test(0.0);

    test.GetAdjacentPoint(-2, 2, {2 * constants::kPi});
    test.GetAdjacentPoint(-1, 1, {2 * constants::kPi});
    test.GetAdjacentPoint(1, 1, {0 * constants::kPi});
    test.GetAdjacentPoint(2, 2, {0 * constants::kPi});

    test.Finalize();
    // just 1 pi: the derivative is calculated between the current point and 2 points behind
    EXPECT_NEAR(-constants::kPi, test.Derivative(0), 1e-8);
    test.Finalize(utilities::Math::GetAngleDiff);
    EXPECT_NEAR(0, test.Derivative(0), 1e-8);
}
}  //namespace mpmca::predict::inertial::testing
