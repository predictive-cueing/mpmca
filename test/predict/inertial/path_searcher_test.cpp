/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <gtest/gtest.h>

#include "mpmca/constants.hpp"
#include "mpmca/predict/inertial/path_factory.hpp"
#include "mpmca/predict/inertial/path_searcher_grid_search.hpp"
#include "mpmca/predict/inertial/path_searcher_segmented.hpp"
#include "mpmca/utilities/math.hpp"

namespace mpmca::predict::inertial::testing {

class PathSearcherTest : public ::testing::Test
{
  protected:
    PathSearcherTest() {}
    static double PositiveModuloDoubleLocal(double x, double y) { return x - (int)(floor(x / y)) * y; }
};
TEST_F(PathSearcherTest, PathSearcherGridSearchTest)
{
    PathPtr path =
        PathFactory("StraightPath", PathFactory::GetStraightPathPointVector({0.0, 0.0}, {1000.0, 0.0}, 0.1), false)
            .GetPreparedPath("PathSearcherGridSearchTest");

    PathSearcherGridSearch searcher;
    searcher.FindIndexGlobal(path, {4.0, 2.1});
    searcher.FindIndexLocal(path, {4.5, 2.1});
}
TEST_F(PathSearcherTest, SearchOutsidePathTest)
{
    PathPtr path =
        PathFactory("StraightPath", PathFactory::GetStraightPathPointVector({0.0, 0.0}, {10.0, 0.0}, 0.1), false)
            .GetPreparedPath("SearchOutsidePathTest");

    PathSearcherGridSearch searcher;

    EXPECT_THROW(searcher.FindIndexGlobal(path, {-0.4, 2.1}), std::runtime_error);
    EXPECT_THROW(searcher.FindIndexGlobal(path, {-0.0004, 2.1}), std::runtime_error);
    EXPECT_NO_THROW(searcher.FindIndexGlobal(path, {0.0004, 2.1}));
}
TEST_F(PathSearcherTest, PathSearcherSegmentedTest)
{
    for (int oo = 0; oo < 2; ++oo) {
        double length;

        if (oo == 0)
            length = 100.0;
        else
            length = 100000.0;

        double A = 100.0;
        double ds = 0.1;
        PathPtr path = PathFactory(PathFactory::GetStraightPathPointVector({0.0, 0.0}, {length, 0.0}, ds), false)
                           .GetPreparedPath("pathSearcherSegmentedTest" + std::to_string(oo));

        PathSearcher* searcher;

        if (oo == 0)
            searcher = new PathSearcherGridSearch();
        else
            searcher = new PathSearcherSegmented();

        for (double x = -10.0; x < length + 10.0; x += 13.2848341) {
            double y = A * std::sin(x);

            if (x > 0 && x < length) {
                searcher->FindIndexGlobal(path, {x, y});
                int expectedIndex = std::clamp((int)std::round(x / ds), path->GetStartIndex(), path->GetEndIndex());
                double expectedFraction = (x - (double)expectedIndex * ds) / ds;

                EXPECT_EQ(expectedIndex, searcher->GetLastPathIndex().GetIndex());
                EXPECT_NEAR(expectedFraction, searcher->GetLastPathIndex().GetFraction(), 1e-7);
            }
            else {
                EXPECT_THROW(searcher->FindIndexGlobal(path, {x, y}), std::runtime_error);
            }

            if (oo > 0)  // the grid searcher is inefficient, so do not expect few iterations.
            {
                EXPECT_LT(searcher->GetNumIterations(), 200);
            }
        }
    }
}
TEST_F(PathSearcherTest, CircularPathSearcherSegmentedTest)
{
    for (int ii = 1; ii < 5; ++ii) {
        double length = std::pow(10, ii);
        double ds = 0.1;
        int N = (int)round(length / ds);
        length = N * ds;

        double R = length / 2 / constants::kPi;
        double A = std::pow(10, ii - 1);

        PathPtr path = PathFactory("CircularPath", PathFactory::GetCirclePathPointVector(N, N + 1, R), true)
                           .GetPreparedPath("circularPathSearcherSegmentedTest" + std::to_string(ii));

        PathSearcherSegmented searcher;

        for (double theta = -10.0; theta < 20.0 * constants::kPi; theta += 0.001234) {
            double r = R + A * std::sin(20 * theta + 21.66);
            double x = r * std::cos(theta);
            double y = r * std::sin(theta);
            searcher.FindIndexGlobal(path, {x, y});

            EXPECT_LT(searcher.GetNumIterations(), 200);
            double d_theta = 2.0 * constants::kPi / (double)N;
            double circleFraction =
                utilities::Math::PositiveModuloDouble(theta, 2.0 * constants::kPi) / 2.0 / constants::kPi;

            int expectedIndex = (int)std::round(circleFraction * 2 * constants::kPi / d_theta);
            int expectedIndexWrapped = path->GetPathPointAt(expectedIndex).GetIndex();

            double expectedFraction =
                (circleFraction * 2.0 * constants::kPi - (double)expectedIndex * d_theta) / d_theta;

            EXPECT_EQ(expectedIndexWrapped, searcher.GetLastPathIndex().GetIndex());
            EXPECT_NEAR(expectedFraction, searcher.GetLastPathIndex().GetFraction(), 1e-5);
        }
    }
}
}  //namespace mpmca::predict::inertial::testing
