/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <gtest/gtest.h>

#include "mpmca/predict/pose/trajectory.hpp"
#include "mpmca/predict/pose/trajectory_creator_t.hpp"
#include "mpmca/predict/pose/trajectory_t.hpp"
#include "mpmca/utilities/math.hpp"

namespace mpmca::predict::pose::testing {

class SegmentTest : public ::testing::Test
{
  protected:
    Trajectory m_trajectory;
    Segment* m_start_segment;
    Segment* m_hold_segment;
    Segment* m_move_segment;
    Segment* m_free_segment;
    Segment* m_goto_segment;
    Segment* m_end_segment;

    SegmentTest()
        : m_trajectory(utilities::Horizon(10, 1000))
        , m_start_segment(m_trajectory.AddSegment<SegmentStart>("Frank", 1000, PoseVector::Constant(5.0),
                                                                PoseRotMatVector::Constant(1.0)))
        , m_hold_segment(m_trajectory.AddSegment<SegmentHold>("Federico", 300, PoseRotMatVector::Constant(2.0)))
        , m_move_segment(m_trajectory.AddSegment<SegmentLinearMove>(
              "Mario", 3000, PoseVector::Constant(0.0), PoseVector::Constant(1.0), PoseRotMatVector::Constant(10.0)))
        , m_free_segment(m_trajectory.AddSegment<SegmentFreeMove>("Misha", 400))
        , m_goto_segment(m_trajectory.AddSegment<SegmentGoto>("Carlo", "Mario", 300))
        , m_end_segment(m_trajectory.AddSegment<SegmentEnd>("Stefano", 1000, PoseVector::Constant(5.0),
                                                            PoseRotMatVector::Constant(1.0)))
    {
    }
};

TEST_F(SegmentTest, TypenameTest)
{
    EXPECT_EQ(m_start_segment->GetType(), "Start");
    EXPECT_EQ(m_hold_segment->GetType(), "Hold");
    EXPECT_EQ(m_move_segment->GetType(), "LinearMove");
    EXPECT_EQ(m_free_segment->GetType(), "FreeMove");
    EXPECT_EQ(m_goto_segment->GetType(), "GoTo");
    EXPECT_EQ(m_end_segment->GetType(), "End");

    EXPECT_EQ(m_start_segment->GetName(), "Frank");
    EXPECT_EQ(m_hold_segment->GetName(), "Federico");
    EXPECT_EQ(m_move_segment->GetName(), "Mario");
    EXPECT_EQ(m_free_segment->GetName(), "Misha");
    EXPECT_EQ(m_goto_segment->GetName(), "Carlo");
    EXPECT_EQ(m_end_segment->GetName(), "Stefano");
}

TEST_F(SegmentTest, CannotAppend)
{
    EXPECT_THROW(m_trajectory.AddSegment<SegmentFreeMove>("boo", 500), std::runtime_error);
}

TEST_F(SegmentTest, SegmentStartTest)
{
    EXPECT_NO_THROW(m_start_segment->Calculate(nullptr));

    EXPECT_TRUE(m_start_segment->GetDtMs() == 10);
    EXPECT_TRUE(m_start_segment->GetTimeDurationMs() == 1000);

    EXPECT_TRUE(m_start_segment->GetFinalReferencePose().isApprox(PoseVector::Constant(5.0)));

    EXPECT_THROW(m_start_segment->GetReferencePoseAtLocalTimeMs(-10), std::invalid_argument);
    EXPECT_THROW(m_start_segment->GetReferencePoseAtLocalTimeMs(1010), std::invalid_argument);
    EXPECT_THROW(m_start_segment->GetReferencePoseAtLocalTimeMs(1001), std::invalid_argument);

    for (int t = 0; t < m_start_segment->GetTimeDurationMs(); t += m_start_segment->GetDtMs()) {
        EXPECT_TRUE(m_start_segment->GetReferencePoseAtLocalTimeMs(t).isApprox(PoseVector::Constant(5.0)))
            << m_start_segment->GetReferencePoseAtLocalTimeMs(t).transpose();

        double alpha = (double)(t) / (double)(m_start_segment->GetTimeDurationMs() - m_start_segment->GetDtMs());

        EXPECT_TRUE(m_start_segment->GetOutputErrorWeightAtLocalTimeMs(t).isApprox(PoseRotMatVector::Constant(alpha)))
            << "Expected " << alpha << " found " << m_start_segment->GetOutputErrorWeightAtLocalTimeMs(t).GetX();
    }
}

TEST_F(SegmentTest, SegmentHoldTest)
{
    EXPECT_NO_THROW(m_start_segment->Calculate(nullptr));
    EXPECT_NO_THROW(m_hold_segment->Calculate(m_start_segment));

    EXPECT_TRUE(m_hold_segment->GetDtMs() == 10);
    EXPECT_TRUE(m_hold_segment->GetTimeDurationMs() == 300);

    for (int t = 0; t < m_hold_segment->GetTimeDurationMs(); t = t + m_hold_segment->GetDtMs()) {
        EXPECT_TRUE(m_hold_segment->GetReferencePoseAtLocalTimeMs(t).isApprox(PoseVector::Constant(5.0)))
            << m_hold_segment->GetReferencePoseAtLocalTimeMs(t).transpose();

        EXPECT_TRUE(m_hold_segment->GetOutputErrorWeightAtLocalTimeMs(t).isApprox(PoseRotMatVector::Constant(2.0)))
            << m_hold_segment->GetOutputErrorWeightAtLocalTimeMs(t).transpose();
    }
}

TEST_F(SegmentTest, SegmentMoveTest)
{
    EXPECT_NO_THROW(m_start_segment->Calculate(nullptr));
    EXPECT_NO_THROW(m_hold_segment->Calculate(m_start_segment));
    EXPECT_NO_THROW(m_move_segment->Calculate(m_hold_segment));

    EXPECT_TRUE(m_move_segment->GetDtMs() == 10);
    EXPECT_TRUE(m_move_segment->GetTimeDurationMs() == 3000);

    for (int t = 0; t < m_move_segment->GetTimeDurationMs(); t = t + m_move_segment->GetDtMs()) {
        double alpha = (double)(t) / (double)(m_move_segment->GetTimeDurationMs() - m_move_segment->GetDtMs());

        EXPECT_TRUE(m_move_segment->GetReferencePoseAtLocalTimeMs(t).isApprox(PoseVector::Constant(alpha)))
            << "Expected " << alpha << " found " << m_move_segment->GetReferencePoseAtLocalTimeMs(t).GetX();

        EXPECT_TRUE(m_move_segment->GetOutputErrorWeightAtLocalTimeMs(t).isApprox(PoseRotMatVector::Constant(10.0)))
            << m_move_segment->GetOutputErrorWeightAtLocalTimeMs(t).transpose();
    }
}

TEST_F(SegmentTest, SegmentFreeMoveTest)
{
    EXPECT_NO_THROW(m_start_segment->Calculate(nullptr));
    EXPECT_NO_THROW(m_hold_segment->Calculate(m_start_segment));
    EXPECT_NO_THROW(m_move_segment->Calculate(m_hold_segment));
    EXPECT_NO_THROW(m_free_segment->Calculate(m_move_segment));

    EXPECT_TRUE(m_free_segment->GetDtMs() == 10);
    EXPECT_TRUE(m_free_segment->GetTimeDurationMs() == 400);

    for (int t = 0; t < m_free_segment->GetTimeDurationMs(); t = t + m_free_segment->GetDtMs()) {
        EXPECT_TRUE(m_free_segment->GetReferencePoseAtLocalTimeMs(t).isApprox(PoseVector::Constant(1.0)));
        EXPECT_TRUE(m_free_segment->GetOutputErrorWeightAtLocalTimeMs(t).isApprox(PoseRotMatVector::Zero()));
    }
}
TEST_F(SegmentTest, SegmentEndTest)
{
    EXPECT_NO_THROW(m_start_segment->Calculate(nullptr));
    EXPECT_NO_THROW(m_hold_segment->Calculate(m_start_segment));
    EXPECT_NO_THROW(m_move_segment->Calculate(m_hold_segment));
    EXPECT_NO_THROW(m_free_segment->Calculate(m_move_segment));
    EXPECT_NO_THROW(m_end_segment->Calculate(m_free_segment));

    EXPECT_TRUE(m_end_segment->GetDtMs() == 10);
    EXPECT_TRUE(m_end_segment->GetTimeDurationMs() == 1000);

    EXPECT_FALSE(m_end_segment->TimeIsWithinBounds(-100));
    EXPECT_TRUE(m_end_segment->TimeIsWithinBounds(1010));
    EXPECT_TRUE(m_end_segment->TimeIsWithinBounds(5690));
    EXPECT_TRUE(m_end_segment->TimeIsWithinBounds(8000));

    for (int t = 0; t < m_end_segment->GetTimeDurationMs(); t = t + m_end_segment->GetDtMs()) {
        double alpha = (double)(t) / (double)(m_end_segment->GetTimeDurationMs() - m_end_segment->GetDtMs());

        EXPECT_TRUE(m_end_segment->GetReferencePoseAtLocalTimeMs(t).isApprox(PoseVector::Constant(5.0)));
        EXPECT_TRUE(m_end_segment->GetOutputErrorWeightAtLocalTimeMs(t).isApprox(PoseRotMatVector::Constant(alpha)));
    }

    EXPECT_TRUE(m_end_segment
                    ->GetReferencePoseAtLocalTimeMs(m_end_segment->GetTimeDurationMs() + 300 * m_end_segment->GetDtMs())
                    .isApprox(PoseVector::Constant(5.0)));
    EXPECT_TRUE(
        m_end_segment
            ->GetOutputErrorWeightAtLocalTimeMs(m_end_segment->GetTimeDurationMs() + 300 * m_end_segment->GetDtMs())
            .isApprox(PoseRotMatVector::Constant(1.0)));
}

TEST_F(SegmentTest, PoseOutputDifferenceTest)
{
    PoseVector A, B;

    A << 1, 1, 1, 0.00, 0.00, 0.00, 0.0000, 0, 0, 0, 0, 0, 0.0000, 0, 0, 0, 0, 0;
    B << 1, 1, 1, 1.57, 1.57, 1.57, 0.0000, 0, 0, 0, 0, 0, 0.0000, 0, 0, 0, 0, 0;

    PoseRotMatVector o_A = A.ToPoseRotMatVector();
    PoseRotMatVector o_B = B.ToPoseRotMatVector();

    int N = 5;

    for (int i = 0; i < N; ++i) {
        double alpha = (double)i / (double)(N - 1);
        PoseRotMatVector morph_pose = PoseVector((1 - alpha) * A + alpha * B).ToPoseRotMatVector();
        PoseRotMatVector morph_output = (1 - alpha) * o_A + alpha * o_B;

        Eigen::Matrix<double, 24, 2> vec_joined;
        vec_joined << morph_pose, morph_output;
        std::cout << vec_joined.transpose() << std::endl << std::endl;
    }
}
}  //namespace mpmca::predict::pose::testing