/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <gtest/gtest.h>

#include "mpmca/constants.hpp"
#include "mpmca/predict/pose/segment_end.hpp"
#include "mpmca/predict/pose/segment_free_move.hpp"
#include "mpmca/predict/pose/segment_hold.hpp"
#include "mpmca/predict/pose/segment_linear_move.hpp"
#include "mpmca/predict/pose/segment_start.hpp"
#include "mpmca/predict/pose/trajectory.hpp"
#include "mpmca/predict/pose/trajectory_creator_t.hpp"
#include "mpmca/predict/pose/trajectory_t.hpp"
#include "mpmca/predict/pose/waypoint.hpp"
#include "mpmca/predict/pose/waypoint_register.hpp"

namespace mpmca::predict::pose::testing {
class TrajectoryWaypointTest : public ::testing::Test
{
  protected:
    PoseVector m_p1, m_p2;
    PoseRotMatVector m_o1, m_o2;

    TrajectoryWaypointTest()
    {
        m_p1 << 1, 2, 3, 0.0, 0.0, 0.0000, 0.0000, 0, 0, 0, 0, 0, 0.0000, 0, 0, 0, 0, 0;
        m_p2 << 1, 2, 3, 0.0, 0.0, constants::kPi_2, 0.0000, 0, 0, 0, 0, 0, 0.0000, 0, 0, 0, 0, 0;

        m_o1 << 1, 2, 3.000, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0.0000, 0, 0, 0, 0, 0, 0.0000, 0, 0, 0, 0, 0;
        m_o2 << 1, 2, 3.000, 0, 1, 0, -1, 0, 0, 0, 0, 1, 0.0000, 0, 0, 0, 0, 0, 0.0000, 0, 0, 0, 0, 0;
    }
};

TEST_F(TrajectoryWaypointTest, SegmentCreateTest)
{
    Waypoint point1("test_name", m_p1);
    EXPECT_EQ(point1.GetName(), "test_name");
    EXPECT_TRUE(point1.GetPoseVector().isApprox(m_p1));
    EXPECT_TRUE(point1.GetOutputVector().isApprox(m_o1));

    Waypoint point2("test_name", m_p2);
    EXPECT_EQ(point2.GetName(), "test_name");
    EXPECT_TRUE(point2.GetPoseVector().isApprox(m_p2));
    EXPECT_TRUE(point2.GetOutputVector().isApprox(m_o2));
}

TEST_F(TrajectoryWaypointTest, RegisterTest)
{
    WaypointRegister reg;
    EXPECT_NO_THROW(reg.MakePoint("test_name", m_p1));
    EXPECT_THROW(reg.MakePoint("test_name", m_p1), std::invalid_argument);
    EXPECT_THROW(reg.MakePoint("test_name", m_p2), std::invalid_argument);
    EXPECT_NO_THROW(reg.MakePoint("test_name2", m_p2));

    EXPECT_TRUE(reg.GetOutputVector("test_name").isApprox(m_o1));
    EXPECT_TRUE(reg.GetOutputVector("test_name2").isApprox(m_o2));
    EXPECT_TRUE(reg.GetOutputVector("test_name").isApprox(m_o1));

    EXPECT_TRUE(reg.GetPoseVector("test_name").isApprox(m_p1));
    EXPECT_TRUE(reg.GetPoseVector("test_name2").isApprox(m_p2));
    EXPECT_TRUE(reg.GetPoseVector("test_name").isApprox(m_p1));

    EXPECT_FALSE(reg.GetOutputVector("test_name2").isApprox(m_o1));
    EXPECT_THROW(reg.GetOutputVector("boo"), std::invalid_argument);
}

TEST_F(TrajectoryWaypointTest, TrajectoryTest)
{
    Trajectory trajectory(utilities::Horizon(10, 1000));

    trajectory.AddWaypoint("A", m_p1);
    trajectory.AddWaypoint("B", m_p2);

    auto conf1 = utilities::Configuration::CreateConfigFromString("config1",
                                                                  "{\"Type\" : \"Start\", \
								 \"Name\" : \"boo1\", \
								 \"TimeMoveMs\" : 2000, \
                                 \"ReferencePose\" : \"A\",  \
                                 \"OutputErrorWeight\" : [0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, \
                                 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01] \
								 }");

    auto conf2 = utilities::Configuration::CreateConfigFromString("config2",
                                                                  "{\"Type\" : \"LinearMove\", \
								   \"Name\" : \"boo1\", \
								   \"TimeMoveMs\" : 5000, \
                                   \"ReferencePoseStart\" : \"A\", \
                                   \"ReferencePoseEnd\" : \"B\",  \
                                   \"OutputErrorWeight\" : [100.0, 100.0, 100.0, 100.0, 100.0, 100.0, \
								   								100.0, 100.0, 100.0, 100.0, 100.0, 100.0, \
                                                                100.0, 100.0, 100.0, 100.0, 100.0, 100.0, \
								   								100.0, 100.0, 100.0, 100.0, 100.0, 100.0] \
									}");

    auto conf3 = utilities::Configuration::CreateConfigFromString("config3",
                                                                  "{\"Type\" : \"LinearMove\", \
								   \"TimeMoveMs\" : 5000, \
								   \"Name\" : \"boo1\", \
                                   \"ReferencePoseEnd\" : \"A\",  \
                                   \"OutputErrorWeight\" : [100.0, 100.0, 100.0, 100.0, 100.0, 100.0, \
								   								100.0, 100.0, 100.0, 100.0, 100.0, 100.0, \
                                                                100.0, 100.0, 100.0, 100.0, 100.0, 100.0, \
								   								100.0, 100.0, 100.0, 100.0, 100.0, 100.0] \
									}");

    auto conf4 = utilities::Configuration::CreateConfigFromString("config3",
                                                                  "{\"Type\" : \"LinearMove\", \
								   \"TimeMoveMs\" : 5000, \
								   \"Name\" : \"boo1\", \
                                   \"ReferencePoseEnd\" : \"C\",  \
                                   \"OutputErrorWeight\" : [100.0, 100.0, 100.0, 100.0, 100.0, 100.0, \
								   								100.0, 100.0, 100.0, 100.0, 100.0, 100.0, \
                                                                100.0, 100.0, 100.0, 100.0, 100.0, 100.0, \
								   								100.0, 100.0, 100.0, 100.0, 100.0, 100.0] \
									}");

    auto i1 = trajectory.AddSegment<SegmentStart>(conf1);
    auto i2 = trajectory.AddSegment<SegmentLinearMove>(conf2);
    auto i3 = trajectory.AddSegment<SegmentLinearMove>(conf3);
    EXPECT_THROW(trajectory.AddSegment<SegmentLinearMove>(conf4), std::invalid_argument);

    EXPECT_TRUE(static_cast<const SegmentStart *>(i1)->GetReferencePose().isApprox(m_p1));
    EXPECT_TRUE(static_cast<const SegmentLinearMove *>(i2)->GetStartReferencePose().isApprox(m_p1));
    EXPECT_TRUE(static_cast<const SegmentLinearMove *>(i3)->GetStartReferencePose().isApprox(PoseVector::Zero()));
    EXPECT_TRUE(static_cast<const SegmentLinearMove *>(i3)->GetEndReferencePose().isApprox(m_p1));
}
}  //namespace mpmca::predict::pose::testing