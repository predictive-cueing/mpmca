/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include "mpmca/predict/pose/trajectory.hpp"

#include <gtest/gtest.h>

#include "mpmca/predict/pose/segment_end.hpp"
#include "mpmca/predict/pose/segment_free_move.hpp"
#include "mpmca/predict/pose/segment_goto.hpp"
#include "mpmca/predict/pose/segment_hold.hpp"
#include "mpmca/predict/pose/segment_linear_move.hpp"
#include "mpmca/predict/pose/segment_start.hpp"
#include "mpmca/predict/pose/trajectory.hpp"
#include "mpmca/predict/pose/trajectory_creator_t.hpp"

namespace mpmca::predict::pose::testing {

class TrajectoryTest : public ::testing::Test
{
  protected:
    TrajectoryTest() {}
};

TEST_F(TrajectoryTest, SegmentCreateTest)
{
    Trajectory trajectory(utilities::Horizon(10, 1000));

    EXPECT_THROW(trajectory.AddSegment<SegmentHold>("boo0", 300, PoseRotMatVector::Constant(2.0)), std::runtime_error);
    EXPECT_THROW(
        trajectory.AddSegment<SegmentEnd>("boo1", 1000, PoseVector::Constant(5.0), PoseRotMatVector::Constant(1.0)),
        std::runtime_error);

    trajectory.AddSegment<SegmentStart>("boo2", 1000, PoseVector::Constant(5.0), PoseRotMatVector::Constant(1.0));

    EXPECT_THROW(
        trajectory.AddSegment<SegmentStart>("boo3", 1000, PoseVector::Constant(5.0), PoseRotMatVector::Constant(1.0)),
        std::runtime_error);

    trajectory.AddSegment<SegmentEnd>("boo4", 1000, PoseVector::Constant(5.0), PoseRotMatVector::Constant(1.0));

    EXPECT_THROW(
        trajectory.AddSegment<SegmentEnd>("boo5", 1000, PoseVector::Constant(5.0), PoseRotMatVector::Constant(1.0)),
        std::runtime_error);

    EXPECT_THROW(
        trajectory.AddSegment<SegmentStart>("boo6", 1000, PoseVector::Constant(5.0), PoseRotMatVector::Constant(1.0)),
        std::runtime_error);

    EXPECT_THROW(trajectory.AddSegment<SegmentHold>("boo7", 300, PoseRotMatVector::Constant(2.0)), std::runtime_error);
}

TEST_F(TrajectoryTest, SegmentTest)
{
    Trajectory trajectory(utilities::Horizon(10, 1800));

    Segment* start =
        trajectory.AddSegment<SegmentStart>("boo8", 1000, PoseVector::Constant(5.0), PoseRotMatVector::Constant(1.0));
    Segment* hold = trajectory.AddSegment<SegmentHold>("boo9", 300, PoseRotMatVector::Constant(2.0));
    Segment* linear_move = trajectory.AddSegment<SegmentLinearMove>(
        "boo10", 3000, PoseVector::Constant(0.0), PoseVector::Constant(1.0), PoseRotMatVector::Constant(10.0));
    Segment* free_move = trajectory.AddSegment<SegmentFreeMove>("boo11", 400);
    Segment* end_segment =
        trajectory.AddSegment<SegmentEnd>("boo12", 1000, PoseVector::Constant(5.0), PoseRotMatVector::Constant(1.0));

    trajectory.Calculate();

    EXPECT_EQ(trajectory.GetNextSegmentInDeque(start), hold);
    EXPECT_EQ(trajectory.GetNextSegmentInDeque(hold), linear_move);
    EXPECT_EQ(trajectory.GetNextSegmentInDeque(linear_move), free_move);
    EXPECT_EQ(trajectory.GetNextSegmentInDeque(free_move), end_segment);
    EXPECT_THROW(trajectory.GetNextSegmentInDeque(end_segment), std::runtime_error);

    double tol = 1e-10;
    // start
    EXPECT_TRUE(trajectory.GetOutputErrorWeightAtHorizonTimeMs(0).isApprox(PoseRotMatVector::Constant(0.0), tol));
    EXPECT_TRUE(trajectory.GetOutputErrorWeightAtHorizonTimeMs(990).isApprox(PoseRotMatVector::Constant(1.0), tol));

    // hold
    EXPECT_TRUE(trajectory.GetOutputErrorWeightAtHorizonTimeMs(1000).isApprox(PoseRotMatVector::Constant(2.0), tol));
    EXPECT_TRUE(trajectory.GetOutputErrorWeightAtHorizonTimeMs(1290).isApprox(PoseRotMatVector::Constant(2.0), tol));

    // linear move
    EXPECT_TRUE(trajectory.GetOutputErrorWeightAtHorizonTimeMs(1300).isApprox(PoseRotMatVector::Constant(10.0), tol));
    EXPECT_TRUE(trajectory.GetOutputErrorWeightAtHorizonTimeMs(3000).isApprox(PoseRotMatVector::Constant(10.0), tol));
    EXPECT_TRUE(trajectory.GetOutputErrorWeightAtHorizonTimeMs(4290).isApprox(PoseRotMatVector::Constant(10.0), tol));

    // free move
    EXPECT_TRUE(trajectory.GetOutputErrorWeightAtHorizonTimeMs(4300).isApprox(PoseRotMatVector::Constant(0.0), tol));
    EXPECT_TRUE(trajectory.GetOutputErrorWeightAtHorizonTimeMs(4690).isApprox(PoseRotMatVector::Constant(0.0), tol));

    // end_segment
    EXPECT_TRUE(trajectory.GetOutputErrorWeightAtHorizonTimeMs(4700).isApprox(PoseRotMatVector::Constant(0.0), tol));
    EXPECT_TRUE(trajectory.GetOutputErrorWeightAtHorizonTimeMs(5690).isApprox(PoseRotMatVector::Constant(1.0), tol));
    EXPECT_TRUE(trajectory.GetOutputErrorWeightAtHorizonTimeMs(15000).isApprox(PoseRotMatVector::Constant(1.0), tol));

    // outside bounds
    EXPECT_THROW(trajectory.GetOutputErrorWeightAtHorizonTimeMs(-10), std::invalid_argument);
    EXPECT_THROW(trajectory.GetOutputErrorWeightAtHorizonTimeMs(1), std::invalid_argument);
    EXPECT_THROW(trajectory.GetReferencePoseAtHorizonTimeMs(-10), std::invalid_argument);
    EXPECT_THROW(trajectory.GetReferencePoseAtHorizonTimeMs(1), std::invalid_argument);
    EXPECT_THROW(trajectory.GetReferencePoseAtHorizonTimeMs(998), std::invalid_argument);
    EXPECT_THROW(trajectory.GetReferencePoseAtHorizonTimeMs(15001), std::invalid_argument);

    // start
    EXPECT_TRUE(trajectory.GetReferencePoseAtHorizonTimeMs(0).isApprox(PoseVector::Constant(5.0), tol));
    EXPECT_TRUE(trajectory.GetReferencePoseAtHorizonTimeMs(990).isApprox(PoseVector::Constant(5.0), tol));

    // hold
    EXPECT_TRUE(trajectory.GetReferencePoseAtHorizonTimeMs(1000).isApprox(PoseVector::Constant(5.0), tol));
    EXPECT_TRUE(trajectory.GetReferencePoseAtHorizonTimeMs(1290).isApprox(PoseVector::Constant(5.0), tol));

    // linear move
    EXPECT_TRUE(trajectory.GetReferencePoseAtHorizonTimeMs(1300).isApprox(PoseVector::Constant(0.0), tol));
    EXPECT_TRUE(trajectory.GetReferencePoseAtHorizonTimeMs(4290).isApprox(PoseVector::Constant(1.0), tol));

    // free move
    EXPECT_TRUE(trajectory.GetReferencePoseAtHorizonTimeMs(4300).isApprox(PoseVector::Constant(1.0), tol));
    EXPECT_TRUE(trajectory.GetReferencePoseAtHorizonTimeMs(4690).isApprox(PoseVector::Constant(1.0), tol));

    // end_segment
    EXPECT_TRUE(trajectory.GetReferencePoseAtHorizonTimeMs(4700).isApprox(PoseVector::Constant(5.0), tol));
    EXPECT_TRUE(trajectory.GetReferencePoseAtHorizonTimeMs(5690).isApprox(PoseVector::Constant(5.0), tol));
    EXPECT_TRUE(trajectory.GetReferencePoseAtHorizonTimeMs(15000).isApprox(PoseVector::Constant(5.0), tol));
}

TEST_F(TrajectoryTest, SegmentGotoTest)
{
    Trajectory trajectory(utilities::Horizon(10, 1000));

    Segment* start =
        trajectory.AddSegment<SegmentStart>("Start", 1000, PoseVector::Constant(5.0), PoseRotMatVector::Constant(1.0));
    Segment* linear_move = trajectory.AddSegment<SegmentLinearMove>(
        "linear_move", 3000, PoseVector::Constant(0.0), PoseVector::Constant(1.0), PoseRotMatVector::Constant(10.0));
    Segment* goto_segment = trajectory.AddSegment<SegmentGoto>("GoTo", "linear_move", 500);
    Segment* end_segment =
        trajectory.AddSegment<SegmentEnd>("boo12", 1000, PoseVector::Constant(5.0), PoseRotMatVector::Constant(1.0));

    EXPECT_NO_THROW(trajectory.Calculate());

    EXPECT_EQ(trajectory.GetNextSegmentInDeque(start), linear_move);
    EXPECT_EQ(trajectory.GetNextSegmentInDeque(linear_move), goto_segment);
    EXPECT_EQ(trajectory.GetNextSegmentInDeque(goto_segment), end_segment);
    EXPECT_THROW(trajectory.GetNextSegmentInDeque(end_segment), std::runtime_error);

    double tol = 1e-10;
    // start
    EXPECT_TRUE(trajectory.GetOutputErrorWeightAtHorizonTimeMs(0).isApprox(PoseRotMatVector::Constant(0.0), tol));
    EXPECT_TRUE(trajectory.GetOutputErrorWeightAtHorizonTimeMs(990).isApprox(PoseRotMatVector::Constant(1.0), tol));

    // linear move
    EXPECT_TRUE(trajectory.GetOutputErrorWeightAtHorizonTimeMs(1000).isApprox(PoseRotMatVector::Constant(10.0), tol));
    EXPECT_TRUE(trajectory.GetOutputErrorWeightAtHorizonTimeMs(3990).isApprox(PoseRotMatVector::Constant(10.0), tol));

    // goto
    EXPECT_TRUE(trajectory.GetOutputErrorWeightAtHorizonTimeMs(4000).isApprox(PoseRotMatVector::Constant(0.0), tol));
    EXPECT_TRUE(trajectory.GetOutputErrorWeightAtHorizonTimeMs(4490).isApprox(PoseRotMatVector::Constant(0.0), tol));

    // linear move
    EXPECT_TRUE(trajectory.GetOutputErrorWeightAtHorizonTimeMs(4500).isApprox(PoseRotMatVector::Constant(10.0), tol));
    EXPECT_TRUE(trajectory.GetOutputErrorWeightAtHorizonTimeMs(7490).isApprox(PoseRotMatVector::Constant(10.0), tol));

    // goto
    EXPECT_TRUE(trajectory.GetOutputErrorWeightAtHorizonTimeMs(7500).isApprox(PoseRotMatVector::Constant(0.0), tol));
    EXPECT_TRUE(trajectory.GetOutputErrorWeightAtHorizonTimeMs(7990).isApprox(PoseRotMatVector::Constant(0.0), tol));

    // outside bounds
    EXPECT_THROW(trajectory.GetOutputErrorWeightAtHorizonTimeMs(-10), std::invalid_argument);
    EXPECT_THROW(trajectory.GetOutputErrorWeightAtHorizonTimeMs(1), std::invalid_argument);
    EXPECT_THROW(trajectory.GetReferencePoseAtHorizonTimeMs(-10), std::invalid_argument);
    EXPECT_THROW(trajectory.GetReferencePoseAtHorizonTimeMs(1), std::invalid_argument);
    EXPECT_THROW(trajectory.GetReferencePoseAtHorizonTimeMs(998), std::invalid_argument);
    EXPECT_THROW(trajectory.GetReferencePoseAtHorizonTimeMs(15001), std::invalid_argument);

    // start
    EXPECT_TRUE(trajectory.GetReferencePoseAtHorizonTimeMs(0).isApprox(PoseVector::Constant(5.0), tol));
    EXPECT_TRUE(trajectory.GetReferencePoseAtHorizonTimeMs(990).isApprox(PoseVector::Constant(5.0), tol));

    // linear move
    EXPECT_TRUE(trajectory.GetReferencePoseAtHorizonTimeMs(1000).isApprox(PoseVector::Constant(0.0), tol));
    EXPECT_TRUE(trajectory.GetReferencePoseAtHorizonTimeMs(3990).isApprox(PoseVector::Constant(1.0), tol));

    // goto
    EXPECT_TRUE(trajectory.GetReferencePoseAtHorizonTimeMs(4000).isApprox(PoseVector::Constant(1.0), tol));
    EXPECT_TRUE(trajectory.GetReferencePoseAtHorizonTimeMs(4490).isApprox(PoseVector::Constant(1.0), tol));

    // linear move
    EXPECT_TRUE(trajectory.GetReferencePoseAtHorizonTimeMs(4500).isApprox(PoseVector::Constant(0.0), tol));
    EXPECT_TRUE(trajectory.GetReferencePoseAtHorizonTimeMs(7490).isApprox(PoseVector::Constant(1.0), tol));

    // goto
    EXPECT_TRUE(trajectory.GetReferencePoseAtHorizonTimeMs(7500).isApprox(PoseVector::Constant(1.0), tol));
    EXPECT_TRUE(trajectory.GetReferencePoseAtHorizonTimeMs(7990).isApprox(PoseVector::Constant(1.0), tol));
}
}  //namespace mpmca::predict::pose::testing