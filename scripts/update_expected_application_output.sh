#!/bin/bash
set -e
./build/bin/demo_classic_washout -c applications/demo_classic_washout/config_test.json --max_iter 10000 --free | sed "s/{.*}//g" > data/application_output/demo_classic_washout/config.txt
./build/bin/demo_classic_washout -c applications/demo_classic_washout/config_xy_hexapod_yaw_test.json --max_iter 10000 --free | sed "s/{.*}//g" > data/application_output/demo_classic_washout/config_xy_hexapod_yaw.txt
./build/bin/demo_hello_pipeline --run_example1  | sed "s/{.*}//g" > data/application_output/demo_hello_pipeline/example1.txt
./build/bin/demo_hello_pipeline --run_example2  | sed "s/{.*}//g" > data/application_output/demo_hello_pipeline/example2.txt
./build/bin/demo_hello_pipeline --run_example3  | sed "s/{.*}//g" > data/application_output/demo_hello_pipeline/example3.txt
./build/bin/demo_industrial_robot_pose -c applications/demo_industrial_robot_pose/config_test.json --max_iter 10000 --free | sed "s/{.*}//g" > data/application_output/demo_industrial_robot_pose/config.txt
./build/bin/demo_mermaid > data/application_output/demo_mermaid/log.txt
./build/bin/demo_minimal_controller > data/application_output/demo_minimal_controller/log.txt
./build/bin/demo_oracle_predictor -c applications/demo_oracle_predictor/config_test.json --max_iter 10000 --free | sed "s/{.*}//g" > data/application_output/demo_oracle_predictor/config.txt
./build/bin/demo_pipeline_config -c applications/demo_pipeline_config/config_test.json --max_iter 10000 --free | sed "s/{.*}//g" > data/application_output/demo_pipeline_config/config.txt
./build/bin/demo_pipeline_config -c applications/demo_pipeline_config/config_extended_test.json --max_iter 10000 --free | sed "s/{.*}//g" > data/application_output/demo_pipeline_config/config_extended.txt
./build/bin/demo_cascaded -c applications/demo_cascaded/config_xy_test.json --max_iter 10000 --free | sed "s/{.*}//g" > data/application_output/demo_cascaded/config_xy.txt
./build/bin/demo_cascaded -c applications/demo_cascaded/config_hexapod_test.json --max_iter 10000 --free | sed "s/{.*}//g" > data/application_output/demo_cascaded/config_hexapod.txt