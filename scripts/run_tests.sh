#!/bin/bash
set -e
./build/bin/mpmca_test --gtest_output="xml:junit-mpmca_test-report.xml"
./build/bin/dokblocks_test  --gtest_output="xml:junit-dokblocks_test-report.xml"
./build/mpmca_constraint_free_simulator/bin/mpmca_constraint_free_simulator_test --gtest_output="xml:junit-mpmca_constraint_free_simulator-report.xml"
./build/mpmca_hexapod_pose/bin/mpmca_hexapod_pose_test --gtest_output="xml:junit-mpmca_hexapod_pose-report.xml"
./build/mpmca_hexapod/bin/mpmca_hexapod_test --gtest_output="xml:junit-mpmca_hexapod-report.xml"
./build/mpmca_industrial_robot_inertial/bin/mpmca_industrial_robot_inertial_test --gtest_output="xml:junit-mpmca_industrial_robot_inertial-report.xml"
./build/mpmca_industrial_robot_pose/bin/mpmca_industrial_robot_pose_test --gtest_output="xml:junit-mpmca_industrial_robot_pose-report.xml"
./build/mpmca_xy_hexapod_yaw_pose/bin/mpmca_xy_hexapod_yaw_pose_test --gtest_output="xml:junit-mpmca_xy_hexapod_yaw_pose-report.xml"
./build/mpmca_xy_hexapod_yaw/bin/mpmca_xy_hexapod_yaw_test --gtest_output="xml:junit-mpmca_xy_hexapod_yaw-report.xml"
cd data/a2d2/ && ./download_unpack_data.sh && cd ../../
./build/bin/demo_pipeline_config -c applications/demo_pipeline_config/config_extended_test.json --max_iter 0 --free
./build/bin/demo_pipeline_config -c applications/demo_pipeline_config/config.json --max_iter 0 --free
./build/bin/demo_classic_washout -c applications/demo_classic_washout/config_xy_hexapod_yaw.json --max_iter 0 --free
./build/bin/demo_industrial_robot_pose -c applications/demo_industrial_robot_pose/config.json --max_iter 0 --free
./build/bin/demo_classic_washout -c applications/demo_classic_washout/config.json --max_iter 0 --free
./build/bin/demo_classic_washout -c applications/demo_classic_washout/config_test.json --max_iter 10000 --free | sed "s/{.*}//g" > log.txt
diff data/application_output/demo_classic_washout/config.txt log.txt
./build/bin/demo_classic_washout -c applications/demo_classic_washout/config_xy_hexapod_yaw_test.json --max_iter 10000 --free | sed "s/{.*}//g" > log.txt
diff data/application_output/demo_classic_washout/config_xy_hexapod_yaw.txt log.txt
./build/bin/demo_hello_pipeline --run_example1  | sed "s/{.*}//g" > log.txt
diff data/application_output/demo_hello_pipeline/example1.txt log.txt
./build/bin/demo_hello_pipeline --run_example2  | sed "s/{.*}//g" > log.txt
diff data/application_output/demo_hello_pipeline/example2.txt log.txt
./build/bin/demo_hello_pipeline --run_example3  | sed "s/{.*}//g" > log.txt
diff data/application_output/demo_hello_pipeline/example3.txt log.txt
./build/bin/demo_industrial_robot_pose -c applications/demo_industrial_robot_pose/config_test.json --max_iter 10000 --free | sed "s/{.*}//g" > log.txt
diff data/application_output/demo_industrial_robot_pose/config.txt log.txt
./build/bin/demo_mermaid > log.txt
diff data/application_output/demo_mermaid/log.txt log.txt
./build/bin/demo_minimal_controller > log.txt
diff data/application_output/demo_minimal_controller/log.txt log.txt
./build/bin/demo_pipeline_config -c applications/demo_pipeline_config/config_test.json --max_iter 10000 --free | sed "s/{.*}//g" > log.txt
diff data/application_output/demo_pipeline_config/config.txt log.txt
./build/bin/demo_pipeline_config -c applications/demo_pipeline_config/config_extended_test.json --max_iter 10000 --free | sed "s/{.*}//g" > log.txt
diff data/application_output/demo_pipeline_config/config_extended.txt log.txt
./build/bin/demo_oracle_predictor -c applications/demo_oracle_predictor/config.json --max_iter 0 --free
./build/bin/demo_oracle_predictor -c applications/demo_oracle_predictor/config_test.json --max_iter 10000 --free | sed "s/{.*}//g" > log.txt
diff data/application_output/demo_oracle_predictor/config.txt log.txt
./build/bin/demo_cascaded -c applications/demo_cascaded/config_cascaded_xy.json --max_iter 0 --free
./build/bin/demo_cascaded -c applications/demo_cascaded/config_cascaded_hexapod.json --max_iter 0 --free
./build/bin/demo_cascaded -c applications/demo_cascaded/config_xy_test.json --max_iter 10000 --free | sed "s/{.*}//g" > log.txt
diff data/application_output/demo_cascaded/config_xy.txt log.txt
./build/bin/demo_cascaded -c applications/demo_cascaded/config_hexapod_test.json --max_iter 10000 --free | sed "s/{.*}//g" > log.txt
diff data/application_output/demo_cascaded/config_hexapod.txt log.txt