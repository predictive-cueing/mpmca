#!/bin/bash
set -e
export CCACHE_BASEDIR="$PWD"
export CCACHE_DIR="$PWD/ccache"
export CCACHE_COMPILERCHECK=content
ccache --zero-stats || true
ccache --show-stats || true
mkdir -p build
cd build
cmake .. -DCMAKE_CXX_COMPILER_LAUNCHER=ccache -DCMAKE_C_COMPILER_LAUNCHER=ccache
cmake --build . --parallel 4
cd ..
export CCACHE_DIR="$PWD/ccache"
ccache --show-stats
