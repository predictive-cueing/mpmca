/**
 * Copyright Frank Drop and Mario Olivari ("Predictive cueing").
 * Please read carefully the terms and conditions (root/LICENCE.md) and
 * any accompanying documentation before you download and/or use
 * the Model Predictive Motion Cueing Algorithm (MPMCA) software.
 */
#include <benchmark/benchmark.h>

#include <fstream>
#include <vector>

#include "generic_test_header.hpp"
#include "mpmca/control/controller.hpp"
#include "mpmca/control/controller_options.hpp"
#include "mpmca/control/simulator.hpp"

namespace mpmca::control::testing {

template <StateStoppabilityOption STOP_OPT, HessianCalculationMethod HM>
class ControllerTemplatedFixture : public ::benchmark::Fixture
{
  protected:
    double m_dt;
    std::vector<unsigned> mpc_steps;
    Simulator<TestSimulatorType> m_sim;
    TestSimulatorType::StateVector m_x0;
    TestSimulatorType::StateVector m_x;
    TestSimulatorType::InputVector m_u0;
    TestSimulatorType::OutputVector m_y_ref;
    TestSimulatorType::OutputVector m_w_y;

    Controller<TestSimulatorType, STOP_OPT, HM> m_controller;

  public:
    ControllerTemplatedFixture()
        : m_dt(0.01)
        , mpc_steps(GetMpcSteps())
        , m_x0(m_sim.GetNominalState())
        , m_u0(m_sim.GetNominalInput())
        , m_controller(mpc_steps, m_dt, m_x0, m_u0, GetOptions())
    {
        if (TestSimulatorType::NY_SIMULATOR == 24) {
            m_y_ref = m_sim.GetNominalOutput();
            m_w_y = ControllerOptions<TestSimulatorType>().output_error_weight;
        }
        else if (TestSimulatorType::NY_SIMULATOR == 9) {
            // Init reference output
            m_y_ref.topRows<3>() = m_sim.GetGravity() + (Vector<3>() << 10., 10., 0.).finished();
            m_y_ref.bottomRows<6>() = (Vector<6>() << 1., 1., 1., 0., 0., 0.).finished();
            m_w_y << 1., 1., 1., 10., 10., 10., 0., 0., 0.;
        }
        else {
            throw std::runtime_error("Unsupported simulator.");
        }

        m_controller.SetLevenbergMarquardt(1.e-6);
    }

    void SetUp(const ::benchmark::State& st) override
    {
        m_x = m_x0;
        m_controller.SetWorkingPoint(m_x0, m_u0);
        m_controller.SetOutputErrorWeight(m_w_y);

        for (std::size_t i = 0; i < m_controller.GetIntegrationHorizon(); ++i)
            m_controller.SetReferenceOutput(i, m_y_ref);
    }

    ControllerOptions<TestSimulatorType> GetOptions()
    {
        ControllerOptions<TestSimulatorType> options;
        options.integration_method = "variableXY";
        return options;
    }

    static std::vector<unsigned> GetMpcSteps()
    {
        std::vector<unsigned> steps;
        unsigned N = 100;
        for (int i = 0; i < N; ++i) {
            if (i < 25)
                steps.push_back(1);

            if (i >= 25 && i < 50)
                steps.push_back(1 + 9 * (i - 25) / 25);

            if (i >= 50)
                steps.push_back(10);
        }
        return steps;
    }

    void FeedbackBench(benchmark::State& state)
    {
        for (auto _ : state) {
            state.PauseTiming();
            m_controller.Preparation();
            state.ResumeTiming();

            auto const u = m_controller.Feedback(m_x);

            state.PauseTiming();
            m_x = m_sim.Integrate(m_x, u, m_dt);
            state.ResumeTiming();
        }
    }
    void PreparationBench(benchmark::State& state)
    {
        for (auto _ : state)
            m_controller.Preparation();
    }
    void ControllerBench(benchmark::State& state)
    {
        for (auto _ : state) {
            m_controller.Preparation();
            auto const u = m_controller.Feedback(m_x);
            m_x = m_sim.Integrate(m_x, u, m_dt);
        }
    }
};

// Prepare

BENCHMARK_TEMPLATE_F(ControllerTemplatedFixture, Prepare0, StateStoppabilityOption::kNone,
                     HessianCalculationMethod::kGaussNewton)
(benchmark::State& state)
{
    PreparationBench(state);
}

BENCHMARK_TEMPLATE_F(ControllerTemplatedFixture, Prepare2, StateStoppabilityOption::kOriginalExpression,
                     HessianCalculationMethod::kGaussNewton)
(benchmark::State& state)
{
    PreparationBench(state);
}

BENCHMARK_TEMPLATE_F(ControllerTemplatedFixture, Prepare4, StateStoppabilityOption::kExpressionWithoutDenominator,
                     HessianCalculationMethod::kGaussNewton)
(benchmark::State& state)
{
    PreparationBench(state);
}

// Feedback

BENCHMARK_TEMPLATE_F(ControllerTemplatedFixture, Feedback0, StateStoppabilityOption::kNone,
                     HessianCalculationMethod::kGaussNewton)
(benchmark::State& state)
{
    FeedbackBench(state);
}

BENCHMARK_TEMPLATE_F(ControllerTemplatedFixture, Feedback2, StateStoppabilityOption::kOriginalExpression,
                     HessianCalculationMethod::kGaussNewton)
(benchmark::State& state)
{
    FeedbackBench(state);
}

BENCHMARK_TEMPLATE_F(ControllerTemplatedFixture, Feedback4, StateStoppabilityOption::kExpressionWithoutDenominator,
                     HessianCalculationMethod::kGaussNewton)
(benchmark::State& state)
{
    FeedbackBench(state);
}

// Controller

BENCHMARK_TEMPLATE_F(ControllerTemplatedFixture, Controller0, StateStoppabilityOption::kNone,
                     HessianCalculationMethod::kGaussNewton)
(benchmark::State& state)
{
    ControllerBench(state);
}

BENCHMARK_TEMPLATE_F(ControllerTemplatedFixture, Controller2, StateStoppabilityOption::kOriginalExpression,
                     HessianCalculationMethod::kGaussNewton)
(benchmark::State& state)
{
    ControllerBench(state);
}

BENCHMARK_TEMPLATE_F(ControllerTemplatedFixture, Controller4, StateStoppabilityOption::kExpressionWithoutDenominator,
                     HessianCalculationMethod::kGaussNewton)
(benchmark::State& state)
{
    ControllerBench(state);
}

}  //namespace mpmca::control::testing

BENCHMARK_MAIN();